/*
    V_GOAL_RESULT_ANALY_ACC
    
*/
-- CREATE 
-- ALTER
-- VIEW V_GOAL_RESULT_ANALY_ACC AS
    SELECT 
        YG.C_YEAR AS C_YEAR,
        LPAD(MG.C_MON, 2, '0') AS C_MON,
        YG.ID_SITE AS ID_SITE,
        SG.ID_ENG_POINT AS ID_ENG_POINT,
        VP.S_POINT_NM AS S_POINT_NM,
        VP.ID_ENG_CEF AS ID_ENG_CEF,
        VP.S_ENG_CD_NM AS S_ENG_CD_NM,
        MG.N_MON_ENG_GOAL AS N_MON_ENG_GOAL, -- 월별 목표  - 배출원별(kW,L,M3,...), 사용자 입력값.
        0 AS N_MON_GAS_GOAL, -- 사용실적
        GA.N_ENG AS N_ENG,
        GA.N_GJ AS N_GJ,
        GA.N_TOE AS N_TOE,
        GA.N_CO2 AS N_CO2, 
        GA.N_CH4 AS N_CH4,
        GA.N_N2O AS N_N2O,
        GA.N_TCO2EQ AS N_TCO2EQ
    FROM
        ((((T_SITE_YEAR_GOAL YG
        JOIN T_SITE_MON_SUM_GOAL SG ON (YG.ID_SITE_YEAR_GOAL = SG.ID_SITE_YEAR_GOAL))
        JOIN T_SITE_MON_GOAL MG ON (SG.ID_SITE_MON_SUM_GOAL = MG.ID_SITE_MON_SUM_GOAL))
        JOIN V_ENG_POINT VP ON (SG.ID_ENG_POINT = VP.ID_ENG_POINT))
        LEFT JOIN T_ENG_GHG_AMT GA ON (GA.ID_ENG_POINT = SG.ID_ENG_POINT
            AND YG.C_YEAR = GA.C_YEAR
            AND MG.C_MON = GA.C_MON))
;

SELECT 
        YG.C_YEAR AS C_YEAR,
        LPAD(MG.C_MON, 2, '0') AS C_MON,
        YG.ID_SITE AS ID_SITE,
        SG.ID_ENG_POINT AS ID_ENG_POINT,
        VP.S_POINT_NM AS S_POINT_NM,
        VP.ID_ENG_CEF AS ID_ENG_CEF,
        VP.S_ENG_CD_NM AS S_ENG_CD_NM,
        MG.N_MON_ENG_GOAL AS N_MON_ENG_GOAL,
        -- 0 AS N_MON_GAS_GOAL,
        FN_ENG_CONVERSION('CO2',VP.ID_ENG_CEF,'',MG.N_MON_ENG_GOAL) N_MON_GAS_GOAL,
        -- MG.N_MON_ENG_GOAL AS N_MON_ENG_GOAL,
        GA.N_ENG AS N_ENG,
        GA.N_GJ AS N_GJ,
        GA.N_TOE AS N_TOE,
        GA.N_CO2 AS N_CO2,
        GA.N_CH4 AS N_CH4,
        GA.N_N2O AS N_N2O,
        GA.N_TCO2EQ AS N_TCO2EQ
    FROM
        ((((T_SITE_YEAR_GOAL YG -- 년간사업장 목표
        JOIN T_SITE_MON_SUM_GOAL SG -- 월별 사업장 목표 합
            ON (YG.ID_SITE_YEAR_GOAL = SG.ID_SITE_YEAR_GOAL))
        JOIN T_SITE_MON_GOAL MG -- 사업장 배출원별 목표 월별
            ON (SG.ID_SITE_MON_SUM_GOAL = MG.ID_SITE_MON_SUM_GOAL))
        JOIN V_ENG_POINT VP  -- 배출시설
            ON (SG.ID_ENG_POINT = VP.ID_ENG_POINT))
        LEFT JOIN T_ENG_GHG_AMT GA -- 배출원온실가스 계산(집계)
            ON (GA.ID_ENG_POINT = SG.ID_ENG_POINT
            AND YG.C_YEAR = GA.C_YEAR
            AND MG.C_MON = GA.C_MON))
;

# 20191205 kdh.
# 20191210 kdh. 수정. EP와 GOAL, EGA 조인간 ID_SITE 조건 추가
-- CREATE 
ALTER
VIEW V_GOAL_RESULT_ANALY_ACC AS
SELECT YM.YYYY as C_YEAR
	, YM.MM as C_MON
    , EP.ID_SITE		-- 사업장ID
	, EP.ID_ENG_POINT	-- 배출시설ID
	, EP.S_POINT_NM		-- 배출시설명
	, EP.ID_ENG_CEF		-- 배출원ID
	, EP.S_ENG_CD		-- 배출원코드
    , EP.S_ENG_CD_NM	-- 배출원명
        
	, GOAL.N_MON_ENG_GOAL	-- 월별 목표  - 배출원별(kW,L,M3,...)
    , GOAL.N_GAS_GOAL	-- 월별 목표 - tCO2 환산 목표량
    , GOAL.N_ENG_GOAL	-- 월별 목표 - GJ 환산 목표량
        
	, EGA.N_GJ	-- GJ 환산 목표량
	, EGA.N_CO2	-- tCO2 환산 실적
	, EGA.N_ENG
	, EGA.N_TOE
	, EGA.N_CH4
	, EGA.N_N2O
	, EGA.N_TCO2EQ
FROM V_YEAR_MONTH as YM
cross join V_ENG_POINT as EP
LEFT OUTER JOIN (
	SELECT 
		YG.C_YEAR,	-- 년
		LPAD(MG.C_MON, 2, '0') AS C_MON, -- 월
		YG.ID_SITE,		-- 사업장ID
		SG.ID_ENG_POINT,-- 배출시설ID
		MG.N_MON_ENG_GOAL,	-- 월별 목표  - 배출원별(kW,L,M3,...)
        MG.N_GAS_GOAL,	-- 월별 목표 - tCO2 환산 목표량
        MG.N_ENG_GOAL	-- 월별 목표 - GJ 환산 목표량
	FROM T_SITE_YEAR_GOAL as YG -- 년간사업장 목표
	INNER JOIN T_SITE_MON_SUM_GOAL as SG -- 월별 배출원 목표 합(구,월별 사업장 목표 합)
		ON (YG.ID_SITE_YEAR_GOAL = SG.ID_SITE_YEAR_GOAL)
	INNER JOIN T_SITE_MON_GOAL as MG -- 사업장 배출시설별 목표 월별
		ON (SG.ID_SITE_MON_SUM_GOAL = MG.ID_SITE_MON_SUM_GOAL)
) as GOAL
	ON (YM.YYYY = GOAL.C_YEAR AND YM.MM = GOAL.C_MON AND EP.ID_ENG_POINT = GOAL.ID_ENG_POINT AND GOAL.ID_SITE= EP.ID_SITE)
LEFT OUTER JOIN T_ENG_GHG_AMT as EGA
	ON (YM.YYYY = EGA.C_YEAR AND YM.MM = EGA.C_MON AND EP.ID_ENG_POINT = EGA.ID_ENG_POINT AND GOAL.ID_SITE= EP.ID_SITE)
where EP.S_INV_CD != '11'	-- 용차는 제외
ORDER BY YM.YYYY, YM.MM, EP.ID_SITE, EP.ID_ENG_CEF

;


SELECT YG.C_YEAR, MG.C_MON
	, '||년간사업장 목표||' as '', YG.*
    , '||월별 배출원 목표 합||' as '', SG.*
    , '||사업장 배출시설별 목표 월별||' as '', MG.*
FROM T_SITE_YEAR_GOAL as YG -- 년간사업장 목표
INNER JOIN T_SITE_MON_SUM_GOAL as SG -- 월별 배출원 목표 합(구,월별 사업장 목표 합)	select * from T_SITE_MON_SUM_GOAL
	ON (YG.ID_SITE_YEAR_GOAL = SG.ID_SITE_YEAR_GOAL)
INNER JOIN T_SITE_MON_GOAL MG -- 사업장 배출시설별 목표 월별		-- select * from T_SITE_MON_GOAL
	ON (SG.ID_SITE_MON_SUM_GOAL = MG.ID_SITE_MON_SUM_GOAL)
ORDER BY YG.C_YEAR, YG.ID_SITE, MG.c_mon, SG.ID_ENG_POINT
        
;
/* T_SITE_MON_SUM_GOAL을 기준으로 T_SITE_MON_GOAL에 임시 환산데이터 입력 쿼리*/
#SET SQL_SAFE_UPDATES = 0;
update T_SITE_MON_GOAL MG, T_SITE_MON_SUM_GOAL SG
set MG.N_GAS_GOAL = case when SG.N_GOAL_SUM !=0 then SG.N_GAS_GOAL * (MG.N_MON_ENG_GOAL / SG.N_GOAL_SUM) else 0 end
	, MG.N_ENG_GOAL = case when SG.N_GOAL_SUM !=0 then SG.N_ENG_GOAL * (MG.N_MON_ENG_GOAL / SG.N_GOAL_SUM) else 0 end
where MG.ID_SITE_MON_SUM_GOAL = SG.ID_SITE_MON_SUM_GOAL
;

select * from T_SITE_MON_SUM_GOAL where ID_ENG_POINT = 4;
select * from V_ENG_POINT where ID_ENG_POINT = 4;

select EGA.*
FROM V_YEAR_MONTH as YM
cross join V_ENG_POINT as EP
left join T_ENG_GHG_AMT as EGA
	on YM.yyyy = EGA.c_year
    and YM.mm = EGA.c_mon
    and EP.ID_ENG_POINT = EGA.ID_ENG_POINT
where YM.yyyy = 2019 and YM.mm = '03';


select * from V_GOAL_RESULT_ANALY_ACC where c_year = 2019 and c_mon = '03';