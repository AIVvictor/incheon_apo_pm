/*
	SP_MAKE_LOGIS_TRANS_MON_EFF_INFO:
    
	효율화 지표용 data 생성
        
	1~12 월달 pivot 하여 데이터를 넣기 위해
    SQL을 입력된 월에 따라 작성하여 실행한다(다른 달의 간섭을 막기 위함)
    
    2019-12-12 kdh.
    
*/
select * from T_LOGIS_TRANS_MON_EFF_INFO;
call SP_MAKE_LOGIS_TRANS_MON_EFF_INFO('201911');

DELIMITER $$
drop procedure SP_MAKE_LOGIS_TRANS_MON_EFF_INFO;
create procedure SP_MAKE_LOGIS_TRANS_MON_EFF_INFO(
  IN IN_YYYYMM VARCHAR(255)
) 
BEGIN
	declare YYYY varchar(4);
    declare MM varchar(2);
    declare YYYYMM varchar(6);
    
    -- 어제 기준의 Year Month를 구한다.
	set YYYY=substring(IN_YYYYMM,1,4);
	set MM=substring(IN_YYYYMM,5,2);
    set @M=CONVERT(MM, INT);
    
	if IN_YYYYMM is null
	then
		set YYYY=DATE_FORMAT( DATE_ADD(now(), INTERVAL -1 DAY)/*yesterday*/,'%Y');
		set MM=DATE_FORMAT( DATE_ADD(now(), INTERVAL -1 DAY)/*yesterday*/,'%m');
		set @M=CONVERT(MM, INT);
        set YYYYMM=DATE_FORMAT( DATE_ADD(now(), INTERVAL -1 DAY)/*yesterday*/,'%Y%m');
	end if;

	set YYYYMM= concat(YYYY,MM);
	select  YYYYMM,YYYY,MM;

	-- logging
	INSERT INTO T_SCHEDULER_LOG(SP_NM) VALUES ( concat('SP_MAKE_LOGIS_TRANS_MON_EFF_INFO:',YYYY,',',MM));
    
	-- data insert/update
	SET @qry = (
		SELECT CONCAT(
			'insert into T_LOGIS_TRANS_MON_EFF_INFO(
				ID_SITE, LINE_CD, S_SELFVRN_CD, LOGI_WEI_CD, CAR_TYPE, TRANS_TYPE_CD, C_YEAR
                , M', @M, '_TON_KM_SUM
                , M', @M, '_GCO2_SUM
                , M', @M, '_MJ_SUM
                , M', @M, '_ENG_VOL_SUM
			)
            select *
			from (
				select 
					SD.ID_SITE	-- 사이트ID
					, V.S_LINE_CD as LINE_CD -- 노선구분(분정기/비정기)
                    , V.S_SELFVRN_CD as S_SELFVRN_CD -- 자차구분(자차/영차)
					, V.S_LOGI_WEI_CD as LOGI_WEI_CD -- 톤수
					, V.S_CAR_TYPE as CAR_TYPE	-- 차종
					, M.TRANS_TYPE_CD	-- 운송수단
					, substring(M.DATE_MON,1,4) as C_YEAR -- 날짜: YYYY
					, IFNULL(sum(case when substring(M.DATE_MON,5,2) = "', MM, '" then M.TON_KM_SUM else null end), 0) as  M', @M, '_TON_KM_SUM -- 톤킬로, 온실가스원단위 n월
					, IFNULL(sum(case when substring(M.DATE_MON,5,2) = "', MM, '" then M.GCO2_SUM else null end), 0) as  M', @M, '_GCO2_SUM -- 온실가스배출량(gCO2) n월
					, IFNULL(sum(case when substring(M.DATE_MON,5,2) = "', MM, '" then M.MJ_SUM else null end), 0) as  M', @M, '_MJ_SUM -- 에너지사용량(MJ) n월
					, IFNULL(sum(case when substring(M.DATE_MON,5,2) = "', MM, '" then M.ENG_VOL_SUM else null end), 0) as  M', @M, '_ENG_VOL_SUM -- 연료사용량 n월
                    
                    from V_SITE_DEPT as SD
					inner join T_VEHICLE as V
						on SD.ID_ERP_DEPT = V.ID_ERP_DEPT
					INNER JOIN T_LOGIS_TRANS_MON_INFO as M
						ON V.ID_VEHICLE = M.ID_VEHICLE
					where V.C_DEL_YN != "Y"
						and M.DATE_MON = "', YYYYMM, '"
					group by SD.ID_SITE
						, V.S_LINE_CD, V.S_LOGI_WEI_CD, V.S_CAR_TYPE
						, M.TRANS_TYPE_CD, substring(M.DATE_MON,1,4)
				) T
				ON DUPLICATE KEY UPDATE
					M', @M, '_TON_KM_SUM = T.M', @M, '_TON_KM_SUM
					, M', @M, '_GCO2_SUM = T.M', @M, '_GCO2_SUM
					, M', @M, '_MJ_SUM = T.M', @M, '_MJ_SUM
					, M', @M, '_ENG_VOL_SUM = T.M', @M, '_ENG_VOL_SUM
            ')
	);
    select @qry;
	PREPARE stmt FROM @qry;
	EXECUTE stmt;
	DEALLOCATE PREPARE stmt;
    
    /*
    insert into T_LOGIS_TRANS_MON_EFF_INFO(
		ID_SITE, LINE_CD, LOGI_WEI_CD, CAR_TYPE, TRANS_TYPE_CD, C_YEAR
		, M1_TON_KM_SUM, M2_TON_KM_SUM, M3_TON_KM_SUM, M4_TON_KM_SUM, M5_TON_KM_SUM, M6_TON_KM_SUM, M7_TON_KM_SUM, M8_TON_KM_SUM, M9_TON_KM_SUM, M10_TON_KM_SUM, M11_TON_KM_SUM, M12_TON_KM_SUM
		, M1_GCO2_SUM, M2_GCO2_SUM, M3_GCO2_SUM, M4_GCO2_SUM, M5_GCO2_SUM, M6_GCO2_SUM, M7_GCO2_SUM, M8_GCO2_SUM, M9_GCO2_SUM, M10_GCO2_SUM, M11_GCO2_SUM, M12_GCO2_SUM
		, M1_MJ_SUM, M2_MJ_SUM, M3_MJ_SUM, M4_MJ_SUM, M5_MJ_SUM, M6_MJ_SUM, M7_MJ_SUM, M8_MJ_SUM, M9_MJ_SUM, M10_MJ_SUM, M11_MJ_SUM, M12_MJ_SUM
		, M1_ENG_VOL_SUM, M2_ENG_VOL_SUM, M3_ENG_VOL_SUM, M4_ENG_VOL_SUM, M5_ENG_VOL_SUM, M6_ENG_VOL_SUM, M7_ENG_VOL_SUM, M8_ENG_VOL_SUM, M9_ENG_VOL_SUM, M10_ENG_VOL_SUM, M11_ENG_VOL_SUM, M12_ENG_VOL_SUM
	)
	select *
	from (
		select 
			SD.ID_SITE	-- 사이트ID
			, V.S_LINE_CD as LINE_CD -- 노선구분(분정기/비정기)
			, V.S_LOGI_WEI_CD as LOGI_WEI_CD -- 톤수
			, V.S_CAR_TYPE as CAR_TYPE	-- 차종
			, M.TRANS_TYPE_CD	-- 운송수단
			, substring(M.DATE_MON,1,4) as C_YEAR -- 날짜: YYYY
			
			, IFNULL(sum(case when substring(M.DATE_MON,5,2) = '01' then M.TON_KM_SUM else null end), 0) as  M1_TON_KM_SUM -- 톤킬로, 온실가스원단위 01월
			, IFNULL(sum(case when substring(M.DATE_MON,5,2) = '02' then M.TON_KM_SUM else null end), 0) as  M2_TON_KM_SUM -- 톤킬로, 온실가스원단위 02월
			, IFNULL(sum(case when substring(M.DATE_MON,5,2) = '03' then M.TON_KM_SUM else null end), 0) as  M3_TON_KM_SUM -- 톤킬로, 온실가스원단위 03월
			, IFNULL(sum(case when substring(M.DATE_MON,5,2) = '04' then M.TON_KM_SUM else null end), 0) as  M4_TON_KM_SUM -- 톤킬로, 온실가스원단위 04월
			, IFNULL(sum(case when substring(M.DATE_MON,5,2) = '05' then M.TON_KM_SUM else null end), 0) as  M5_TON_KM_SUM -- 톤킬로, 온실가스원단위 05월
			, IFNULL(sum(case when substring(M.DATE_MON,5,2) = '06' then M.TON_KM_SUM else null end), 0) as  M6_TON_KM_SUM -- 톤킬로, 온실가스원단위 06월
			, IFNULL(sum(case when substring(M.DATE_MON,5,2) = '07' then M.TON_KM_SUM else null end), 0) as  M7_TON_KM_SUM -- 톤킬로, 온실가스원단위 07월
			, IFNULL(sum(case when substring(M.DATE_MON,5,2) = '08' then M.TON_KM_SUM else null end), 0) as  M8_TON_KM_SUM -- 톤킬로, 온실가스원단위 08월
			, IFNULL(sum(case when substring(M.DATE_MON,5,2) = '09' then M.TON_KM_SUM else null end), 0) as  M9_TON_KM_SUM -- 톤킬로, 온실가스원단위 09월
			, IFNULL(sum(case when substring(M.DATE_MON,5,2) = '10' then M.TON_KM_SUM else null end), 0) as M10_TON_KM_SUM -- 톤킬로, 온실가스원단위 10월
			, IFNULL(sum(case when substring(M.DATE_MON,5,2) = '11' then M.TON_KM_SUM else null end), 0) as M11_TON_KM_SUM -- 톤킬로, 온실가스원단위 11월
			, IFNULL(sum(case when substring(M.DATE_MON,5,2) = '12' then M.TON_KM_SUM else null end), 0) as M12_TON_KM_SUM -- 톤킬로, 온실가스원단위 12월
			
			, IFNULL(sum(case when substring(M.DATE_MON,5,2) = '01' then M.GCO2_SUM else null end), 0) as  M1_GCO2_SUM -- 온실가스배출량(gCO2) 01월
			, IFNULL(sum(case when substring(M.DATE_MON,5,2) = '02' then M.GCO2_SUM else null end), 0) as  M2_GCO2_SUM -- 온실가스배출량(gCO2) 02월
			, IFNULL(sum(case when substring(M.DATE_MON,5,2) = '03' then M.GCO2_SUM else null end), 0) as  M3_GCO2_SUM -- 온실가스배출량(gCO2) 03월
			, IFNULL(sum(case when substring(M.DATE_MON,5,2) = '04' then M.GCO2_SUM else null end), 0) as  M4_GCO2_SUM -- 온실가스배출량(gCO2) 04월
			, IFNULL(sum(case when substring(M.DATE_MON,5,2) = '05' then M.GCO2_SUM else null end), 0) as  M5_GCO2_SUM -- 온실가스배출량(gCO2) 05월
			, IFNULL(sum(case when substring(M.DATE_MON,5,2) = '06' then M.GCO2_SUM else null end), 0) as  M6_GCO2_SUM -- 온실가스배출량(gCO2) 06월
			, IFNULL(sum(case when substring(M.DATE_MON,5,2) = '07' then M.GCO2_SUM else null end), 0) as  M7_GCO2_SUM -- 온실가스배출량(gCO2) 07월
			, IFNULL(sum(case when substring(M.DATE_MON,5,2) = '08' then M.GCO2_SUM else null end), 0) as  M8_GCO2_SUM -- 온실가스배출량(gCO2) 08월
			, IFNULL(sum(case when substring(M.DATE_MON,5,2) = '09' then M.GCO2_SUM else null end), 0) as  M9_GCO2_SUM -- 온실가스배출량(gCO2) 09월
			, IFNULL(sum(case when substring(M.DATE_MON,5,2) = '10' then M.GCO2_SUM else null end), 0) as M10_GCO2_SUM -- 온실가스배출량(gCO2) 10월
			, IFNULL(sum(case when substring(M.DATE_MON,5,2) = '11' then M.GCO2_SUM else null end), 0) as M11_GCO2_SUM -- 온실가스배출량(gCO2) 11월
			, IFNULL(sum(case when substring(M.DATE_MON,5,2) = '12' then M.GCO2_SUM else null end), 0) as M12_GCO2_SUM -- 온실가스배출량(gCO2) 12월
			
			, IFNULL(sum(case when substring(M.DATE_MON,5,2) = '01' then M.MJ_SUM else null end), 0) as  M1_MJ_SUM -- 에너지사용량(MJ) 01월
			, IFNULL(sum(case when substring(M.DATE_MON,5,2) = '02' then M.MJ_SUM else null end), 0) as  M2_MJ_SUM -- 에너지사용량(MJ) 02월
			, IFNULL(sum(case when substring(M.DATE_MON,5,2) = '03' then M.MJ_SUM else null end), 0) as  M3_MJ_SUM -- 에너지사용량(MJ) 03월
			, IFNULL(sum(case when substring(M.DATE_MON,5,2) = '04' then M.MJ_SUM else null end), 0) as  M4_MJ_SUM -- 에너지사용량(MJ) 04월
			, IFNULL(sum(case when substring(M.DATE_MON,5,2) = '05' then M.MJ_SUM else null end), 0) as  M5_MJ_SUM -- 에너지사용량(MJ) 05월
			, IFNULL(sum(case when substring(M.DATE_MON,5,2) = '06' then M.MJ_SUM else null end), 0) as  M6_MJ_SUM -- 에너지사용량(MJ) 06월
			, IFNULL(sum(case when substring(M.DATE_MON,5,2) = '07' then M.MJ_SUM else null end), 0) as  M7_MJ_SUM -- 에너지사용량(MJ) 07월
			, IFNULL(sum(case when substring(M.DATE_MON,5,2) = '08' then M.MJ_SUM else null end), 0) as  M8_MJ_SUM -- 에너지사용량(MJ) 08월
			, IFNULL(sum(case when substring(M.DATE_MON,5,2) = '09' then M.MJ_SUM else null end), 0) as  M9_MJ_SUM -- 에너지사용량(MJ) 09월
			, IFNULL(sum(case when substring(M.DATE_MON,5,2) = '10' then M.MJ_SUM else null end), 0) as M10_MJ_SUM -- 에너지사용량(MJ) 10월
			, IFNULL(sum(case when substring(M.DATE_MON,5,2) = '11' then M.MJ_SUM else null end), 0) as M11_MJ_SUM -- 에너지사용량(MJ) 11월
			, IFNULL(sum(case when substring(M.DATE_MON,5,2) = '12' then M.MJ_SUM else null end), 0) as M12_MJ_SUM -- 에너지사용량(MJ) 12월
			
			, IFNULL(sum(case when substring(M.DATE_MON,5,2) = '01' then M.ENG_VOL_SUM else null end), 0) as  M1_ENG_VOL_SUM -- 연료사용량 01월
			, IFNULL(sum(case when substring(M.DATE_MON,5,2) = '02' then M.ENG_VOL_SUM else null end), 0) as  M2_ENG_VOL_SUM -- 연료사용량 02월
			, IFNULL(sum(case when substring(M.DATE_MON,5,2) = '03' then M.ENG_VOL_SUM else null end), 0) as  M3_ENG_VOL_SUM -- 연료사용량 03월
			, IFNULL(sum(case when substring(M.DATE_MON,5,2) = '04' then M.ENG_VOL_SUM else null end), 0) as  M4_ENG_VOL_SUM -- 연료사용량 04월
			, IFNULL(sum(case when substring(M.DATE_MON,5,2) = '05' then M.ENG_VOL_SUM else null end), 0) as  M5_ENG_VOL_SUM -- 연료사용량 05월
			, IFNULL(sum(case when substring(M.DATE_MON,5,2) = '06' then M.ENG_VOL_SUM else null end), 0) as  M6_ENG_VOL_SUM -- 연료사용량 06월
			, IFNULL(sum(case when substring(M.DATE_MON,5,2) = '07' then M.ENG_VOL_SUM else null end), 0) as  M7_ENG_VOL_SUM -- 연료사용량 07월
			, IFNULL(sum(case when substring(M.DATE_MON,5,2) = '08' then M.ENG_VOL_SUM else null end), 0) as  M8_ENG_VOL_SUM -- 연료사용량 08월
			, IFNULL(sum(case when substring(M.DATE_MON,5,2) = '09' then M.ENG_VOL_SUM else null end), 0) as  M9_ENG_VOL_SUM -- 연료사용량 09월
			, IFNULL(sum(case when substring(M.DATE_MON,5,2) = '10' then M.ENG_VOL_SUM else null end), 0) as M10_ENG_VOL_SUM -- 연료사용량 10월
			, IFNULL(sum(case when substring(M.DATE_MON,5,2) = '11' then M.ENG_VOL_SUM else null end), 0) as M11_ENG_VOL_SUM -- 연료사용량 11월
			, IFNULL(sum(case when substring(M.DATE_MON,5,2) = '12' then M.ENG_VOL_SUM else null end), 0) as M12_ENG_VOL_SUM -- 연료사용량 12월
			
		from V_SITE_DEPT as SD
		inner join T_VEHICLE as V -- 차량 master	-- select * from T_VEHICLE
			on SD.ID_ERP_DEPT = V.ID_ERP_DEPT
		INNER JOIN T_LOGIS_TRANS_MON_INFO as M -- 월운송정보	-- select * from T_LOGIS_TRANS_MON_INFO
			ON V.ID_VEHICLE = M.ID_VEHICLE
		where V.C_DEL_YN != 'Y'
			and M.DATE_MON = YYYYMM
		group by SD.ID_SITE
			, V.S_LINE_CD, V.S_LOGI_WEI_CD, V.S_CAR_TYPE
			, M.TRANS_TYPE_CD, substring(M.DATE_MON,1,4)
	) T
	ON DUPLICATE KEY UPDATE
		M1_TON_KM_SUM = T.M1_TON_KM_SUM
		, M2_TON_KM_SUM = T.M2_TON_KM_SUM
		, M3_TON_KM_SUM = T.M3_TON_KM_SUM
		, M4_TON_KM_SUM = T.M4_TON_KM_SUM
		, M5_TON_KM_SUM = T.M5_TON_KM_SUM
		, M6_TON_KM_SUM = T.M6_TON_KM_SUM
		, M7_TON_KM_SUM = T.M7_TON_KM_SUM
		, M8_TON_KM_SUM = T.M8_TON_KM_SUM
		, M9_TON_KM_SUM = T.M9_TON_KM_SUM
		, M10_TON_KM_SUM = T.M10_TON_KM_SUM
		, M11_TON_KM_SUM = T.M11_TON_KM_SUM
		, M12_TON_KM_SUM = T.M12_TON_KM_SUM
		, M1_GCO2_SUM = T.M1_GCO2_SUM
		, M2_GCO2_SUM = T.M2_GCO2_SUM
		, M3_GCO2_SUM = T.M3_GCO2_SUM
		, M4_GCO2_SUM = T.M4_GCO2_SUM
		, M5_GCO2_SUM = T.M5_GCO2_SUM
		, M6_GCO2_SUM = T.M6_GCO2_SUM
		, M7_GCO2_SUM = T.M7_GCO2_SUM
		, M8_GCO2_SUM = T.M8_GCO2_SUM
		, M9_GCO2_SUM = T.M9_GCO2_SUM
		, M10_GCO2_SUM = T.M10_GCO2_SUM
		, M11_GCO2_SUM = T.M11_GCO2_SUM
		, M12_GCO2_SUM = T.M12_GCO2_SUM
		, M1_MJ_SUM = T.M1_MJ_SUM
		, M2_MJ_SUM = T.M2_MJ_SUM
		, M3_MJ_SUM = T.M3_MJ_SUM
		, M4_MJ_SUM = T.M4_MJ_SUM
		, M5_MJ_SUM = T.M5_MJ_SUM
		, M6_MJ_SUM = T.M6_MJ_SUM
		, M7_MJ_SUM = T.M7_MJ_SUM
		, M8_MJ_SUM = T.M8_MJ_SUM
		, M9_MJ_SUM = T.M9_MJ_SUM
		, M10_MJ_SUM = T.M10_MJ_SUM
		, M11_MJ_SUM = T.M11_MJ_SUM
		, M12_MJ_SUM = T.M12_MJ_SUM
		, M1_ENG_VOL_SUM = T.M1_ENG_VOL_SUM
		, M2_ENG_VOL_SUM = T.M2_ENG_VOL_SUM
		, M3_ENG_VOL_SUM = T.M3_ENG_VOL_SUM
		, M4_ENG_VOL_SUM = T.M4_ENG_VOL_SUM
		, M5_ENG_VOL_SUM = T.M5_ENG_VOL_SUM
		, M6_ENG_VOL_SUM = T.M6_ENG_VOL_SUM
		, M7_ENG_VOL_SUM = T.M7_ENG_VOL_SUM
		, M8_ENG_VOL_SUM = T.M8_ENG_VOL_SUM
		, M9_ENG_VOL_SUM = T.M9_ENG_VOL_SUM
		, M10_ENG_VOL_SUM = T.M10_ENG_VOL_SUM
		, M11_ENG_VOL_SUM = T.M11_ENG_VOL_SUM
		, M12_ENG_VOL_SUM = T.M12_ENG_VOL_SUM
	;
    */

END $$
DELIMITER ;

