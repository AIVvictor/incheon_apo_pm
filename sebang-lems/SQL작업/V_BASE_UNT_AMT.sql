/*
  V_BASE_UNT_AMT
  
  2016/11/6
  
  화면: 온실가스 에너지/ 온실가스 에너지 / 원단위 분석


*/
select * from T_ENG_GHG_AMT;

select * from V_BASE_UNT_AMT;

alter view V_BASE_UNT_AMT as 
SELECT 
  1 ID_SITE -- 사업장 ID
  ,'사업장' S_SITE_NM -- 사업장명
  ,1 YCT_CD -- 노선구분
  ,'YCT_NM' YCT_NM -- 노선구분 이름
  ,'TCO2' SCU_CD -- 분석조회단위코드
  ,'TCO2' SCU_NM -- 분석조회단위명
  ,'BASE' BASE_UNT_CD -- 원단위 기준 코드
  ,'BASE' BASE_UNT_NM -- 원단위 기준명
  ,C_YEAR -- 연도
  ,1 M1_GJ
  ,1 M1_GJ_BASE
  ,1 M1_BASE_UNIT 
  ,1 M2_GJ
  ,1 M2_GJ_BASE
  ,1 M2_BASE_UNIT 
  ,1 M3_GJ
  ,1 M3_GJ_BASE
  ,1 M3_BASE_UNIT 
  ,1 M4_GJ
  ,1 M4_GJ_BASE
  ,1 M4_BASE_UNIT 
  ,1 M5_GJ
  ,1 M5_GJ_BASE
  ,1 M5_BASE_UNIT 
  ,1 M6_GJ
  ,1 M6_GJ_BASE
  ,1 M6_BASE_UNIT 
  ,1 M7_GJ
  ,1 M7_GJ_BASE
  ,1 M7_BASE_UNIT 
  ,1 M8_GJ
  ,1 M8_GJ_BASE
  ,1 M8_BASE_UNIT 
  ,1 M9_GJ
  ,1 M9_GJ_BASE
  ,1 M9_BASE_UNIT 
  ,1 M10_GJ
  ,1 M10_GJ_BASE
  ,1 M10_BASE_UNIT 
  ,1 M11_GJ
  ,1 M11_GJ_BASE
  ,1 M11_BASE_UNIT 
  ,1 M12_GJ
  ,1 M12_GJ_BASE
  ,1 M12_BASE_UNIT 

FROM T_ENG_GHG_AMT;


where C_YEAR in (2015,2016);


select * from T_CODE where S_CAT='UNT';

select * from T_CODE where S_CAT='BASE_UNT';

select * from T_CAT;


-- INSERT INTO T_CODE(  S_CAT  ,S_CD  ,S_DESC  ,C_DEL_YN) VALUES (  'BASE_UNT'  ,'1' ,'ton.km' ,'N');
-- INSERT INTO T_CODE(  S_CAT  ,S_CD  ,S_DESC  ,C_DEL_YN) VALUES (  'BASE_UNT'  ,'2' ,'대수' ,'N');
-- INSERT INTO T_CODE(  S_CAT  ,S_CD  ,S_DESC  ,C_DEL_YN) VALUES (  'BASE_UNT'  ,'3' ,'적재량(kg)' ,'N');
-- INSERT INTO T_CODE(  S_CAT  ,S_CD  ,S_DESC  ,C_DEL_YN) VALUES (  'BASE_UNT'  ,'4' ,'면적(m2)' ,'N');
-- INSERT INTO T_CODE(  S_CAT  ,S_CD  ,S_ESC  ,C_DEL_YN) VALUES (  'BASE_UNT'  ,'5' ,'가동시간(hour)' ,'N');

-- ==========================================================================
-- 2019/11/11 1차 수정

-- alter view V_BASE_UNT_AMT as 
SELECT 
  1 ID_SITE -- 사업장 ID
  ,'사업장' S_SITE_NM -- 사업장명
  ,1 YCT_CD -- 노선구분
  ,'YCT_NM' YCT_NM -- 노선구분 이름
  ,1 SCU_CD -- 분석조회단위코드
  ,'TCO2' SCU_NM -- 분석조회단위명
  ,'BASE' BASE_UNT_CD -- 원단위 기준 코드
  ,'BASE' BASE_UNT_NM -- 원단위 기준명
  ,C_YEAR -- 연도
  ,1 S_LOGI_TYPE_CD -- 배출시설 물류구분
  
  ,1 M1_GJ  -- 에너지사용량 GJ
  ,1 M1_GJ_BASE -- 원단위 GJ(면적당)
  ,1 M2_GJ  -- 에너지사용량 GJ
  ,1 M2_GJ_BASE -- 원단위 GJ(면적당)
  ,1 M3_GJ  -- 에너지사용량 GJ
  ,1 M3_GJ_BASE -- 원단위 GJ(면적당)
  ,1 M4_GJ  -- 에너지사용량 GJ
  ,1 M4_GJ_BASE -- 원단위 GJ(면적당)
  ,1 M5_GJ  -- 에너지사용량 GJ
  ,1 M5_GJ_BASE -- 원단위 GJ(면적당)
  ,1 M6_GJ  -- 에너지사용량 GJ
  ,1 M6_GJ_BASE -- 원단위 GJ(면적당)
  ,1 M7_GJ  -- 에너지사용량 GJ
  ,1 M7_GJ_BASE -- 원단위 GJ(면적당)
  ,1 M8_GJ  -- 에너지사용량 GJ
  ,1 M8_GJ_BASE -- 원단위 GJ(면적당)
  ,1 M9_GJ  -- 에너지사용량 GJ
  ,1 M9_GJ_BASE -- 원단위 GJ(면적당)
  ,1 M10_GJ  -- 에너지사용량 GJ
  ,1 M10_GJ_BASE -- 원단위 GJ(면적당)
  ,1 M11_GJ  -- 에너지사용량 GJ
  ,1 M11_GJ_BASE -- 원단위 GJ(면적당)
  ,1 M12_GJ  -- 에너지사용량 GJ
  ,1 M12_GJ_BASE -- 원단위 GJ(면적당)
  
  ,1 M1_TOE  -- 에너지사용량 TOE
  ,1 M1_TOE_BASE -- 원단위 TOE(면적당)
  ,1 M2_TOE  -- 에너지사용량 TOE
  ,1 M2_TOE_BASE -- 원단위 TOE(면적당)
  ,1 M3_TOE  -- 에너지사용량 TOE
  ,1 M3_TOE_BASE -- 원단위 TOE(면적당)
  ,1 M4_TOE  -- 에너지사용량 TOE
  ,1 M4_TOE_BASE -- 원단위 TOE(면적당)
  ,1 M5_TOE  -- 에너지사용량 TOE
  ,1 M5_TOE_BASE -- 원단위 TOE(면적당)
  ,1 M6_TOE  -- 에너지사용량 TOE
  ,1 M6_TOE_BASE -- 원단위 TOE(면적당)
  ,1 M7_TOE  -- 에너지사용량 TOE
  ,1 M7_TOE_BASE -- 원단위 TOE(면적당)
  ,1 M8_TOE  -- 에너지사용량 TOE
  ,1 M8_TOE_BASE -- 원단위 TOE(면적당)
  ,1 M9_TOE  -- 에너지사용량 TOE
  ,1 M9_TOE_BASE -- 원단위 TOE(면적당)
  ,1 M10_TOE  -- 에너지사용량 TOE
  ,1 M10_TOE_BASE -- 원단위 TOE(면적당)
  ,1 M11_TOE  -- 에너지사용량 TOE
  ,1 M11_TOE_BASE -- 원단위 TOE(면적당)
  ,1 M12_TOE  -- 에너지사용량 TOE
  ,1 M12_TOE_BASE -- 원단위 TOE(면적당)
  
  ,1 M1_CO2  -- 에너지사용량 CO2
  ,1 M1_CO2_BASE -- 원단위 CO2(면적당)
  ,1 M2_CO2  -- 에너지사용량 CO2
  ,1 M2_CO2_BASE -- 원단위 CO2(면적당)
  ,1 M3_CO2  -- 에너지사용량 CO2
  ,1 M3_CO2_BASE -- 원단위 CO2(면적당)
  ,1 M4_CO2  -- 에너지사용량 CO2
  ,1 M4_CO2_BASE -- 원단위 CO2(면적당)
  ,1 M5_CO2  -- 에너지사용량 CO2
  ,1 M5_CO2_BASE -- 원단위 CO2(면적당)
  ,1 M6_CO2  -- 에너지사용량 CO2
  ,1 M6_CO2_BASE -- 원단위 CO2(면적당)
  ,1 M7_CO2  -- 에너지사용량 CO2
  ,1 M7_CO2_BASE -- 원단위 CO2(면적당)
  ,1 M8_CO2  -- 에너지사용량 CO2
  ,1 M8_CO2_BASE -- 원단위 CO2(면적당)
  ,1 M9_CO2  -- 에너지사용량 CO2
  ,1 M9_CO2_BASE -- 원단위 CO2(면적당)
  ,1 M10_CO2  -- 에너지사용량 CO2
  ,1 M10_CO2_BASE -- 원단위 CO2(면적당)
  ,1 M11_CO2  -- 에너지사용량 CO2
  ,1 M11_CO2_BASE -- 원단위 CO2(면적당)
  ,1 M12_CO2  -- 에너지사용량 CO2
  ,1 M12_CO2_BASE -- 원단위 CO2(면적당)
  
  ,1 M1_BASE_VAL -- 면적,톤킬로
  ,1 M2_BASE_VAL -- 면적,톤킬로
  ,1 M3_BASE_VAL -- 면적,톤킬로
  ,1 M4_BASE_VAL -- 면적,톤킬로
  ,1 M5_BASE_VAL -- 면적,톤킬로
  ,1 M6_BASE_VAL -- 면적,톤킬로
  ,1 M7_BASE_VAL -- 면적,톤킬로
  ,1 M8_BASE_VAL -- 면적,톤킬로
  ,1 M9_BASE_VAL -- 면적,톤킬로
  ,1 M10_BASE_VAL -- 면적,톤킬로
  ,1 M11_BASE_VAL -- 면적,톤킬로
  ,1 M12_BASE_VAL -- 면적,톤킬로

FROM T_ENG_GHG_AMT;



-- ==========================================================================
-- 2019/11/11 - 권선임 작업 분
-- 2019/12/13 - 노선구분 -> 자차구분으로 변경. kdh.

-- alter view V_BASE_UNT_AMT as 
select M.ID_SITE -- 사업장 ID
        , M.S_SITE_NM -- 사업장명
        , M.ID_ENG_POINT -- 배출시설ID
        , M.S_POINT_NM -- 배출시설명
        #, M.YCT_CD -- 노선구분
        #, M.YCT_NM -- 노선구분 이름
        , M.S_INV_CD	-- 배출시설구분
        , M.SELFVRN_CD -- 자차구분
        , M.SELFVRN_NM -- 자차구분 이름
        , M.BASE_UNT_CD -- 원단위 기준 코드
        , M.BASE_UNT_NM -- 원단위 기준명
        , M.S_LOGI_TYPE_CD -- 배출시설 물류구분
        , D.C_YEAR -- 연도

        , sum( case when D.C_MON = 1  then D.N_GJ             else 0 end ) as M1_GJ                -- 에너지사용량 GJ
        , sum( case when D.C_MON = 1  then D.N_GJ_BASE        else 0 end ) as M1_GJ_BASE        -- 원단위 GJ(면적당)
        , sum( case when D.C_MON = 2  then D.N_GJ             else 0 end ) as M2_GJ                -- 에너지사용량 GJ
        , sum( case when D.C_MON = 2  then D.N_GJ_BASE        else 0 end ) as M2_GJ_BASE        -- 원단위 GJ(면적당)
        , sum( case when D.C_MON = 3  then D.N_GJ             else 0 end ) as M3_GJ                -- 에너지사용량 GJ
        , sum( case when D.C_MON = 3  then D.N_GJ_BASE        else 0 end ) as M3_GJ_BASE        -- 원단위 GJ(면적당)
        , sum( case when D.C_MON = 4  then D.N_GJ             else 0 end ) as M4_GJ                -- 에너지사용량 GJ
        , sum( case when D.C_MON = 4  then D.N_GJ_BASE        else 0 end ) as M4_GJ_BASE        -- 원단위 GJ(면적당)
        , sum( case when D.C_MON = 5  then D.N_GJ             else 0 end ) as M5_GJ                -- 에너지사용량 GJ
        , sum( case when D.C_MON = 5  then D.N_GJ_BASE        else 0 end ) as M5_GJ_BASE        -- 원단위 GJ(면적당)
        , sum( case when D.C_MON = 6  then D.N_GJ             else 0 end ) as M6_GJ                -- 에너지사용량 GJ
        , sum( case when D.C_MON = 6  then D.N_GJ_BASE        else 0 end ) as M6_GJ_BASE        -- 원단위 GJ(면적당)
        , sum( case when D.C_MON = 7  then D.N_GJ             else 0 end ) as M7_GJ                -- 에너지사용량 GJ
        , sum( case when D.C_MON = 7  then D.N_GJ_BASE        else 0 end ) as M7_GJ_BASE        -- 원단위 GJ(면적당)
        , sum( case when D.C_MON = 8  then D.N_GJ             else 0 end ) as M8_GJ                -- 에너지사용량 GJ
        , sum( case when D.C_MON = 8  then D.N_GJ_BASE        else 0 end ) as M8_GJ_BASE        -- 원단위 GJ(면적당)
        , sum( case when D.C_MON = 9  then D.N_GJ             else 0 end ) as M9_GJ                -- 에너지사용량 GJ
        , sum( case when D.C_MON = 9  then D.N_GJ_BASE        else 0 end ) as M9_GJ_BASE        -- 원단위 GJ(면적당)
        , sum( case when D.C_MON = 10 then D.N_GJ             else 0 end ) as M10_GJ                -- 에너지사용량 GJ
        , sum( case when D.C_MON = 10 then D.N_GJ_BASE        else 0 end ) as M10_GJ_BASE        -- 원단위 GJ(면적당)
        , sum( case when D.C_MON = 11 then D.N_GJ             else 0 end ) as M11_GJ                -- 에너지사용량 GJ
        , sum( case when D.C_MON = 11 then D.N_GJ_BASE        else 0 end ) as M11_GJ_BASE        -- 원단위 GJ(면적당)
        , sum( case when D.C_MON = 12 then D.N_GJ             else 0 end ) as M12_GJ                -- 에너지사용량 GJ
        , sum( case when D.C_MON = 12 then D.N_GJ_BASE        else 0 end ) as M12_GJ_BASE        -- 원단위 GJ(면적당)

        , sum( case when D.C_MON = 1  then D.N_TOE      else 0 end ) as M1_TOE                -- 에너지사용량 TOE
        , sum( case when D.C_MON = 1  then D.N_TOE_BASE else 0 end ) as M1_TOE_BASE        -- 원단위 TOE(면적당)
        , sum( case when D.C_MON = 2  then D.N_TOE      else 0 end ) as M2_TOE                -- 에너지사용량 TOE
        , sum( case when D.C_MON = 2  then D.N_TOE_BASE else 0 end ) as M2_TOE_BASE        -- 원단위 TOE(면적당)
        , sum( case when D.C_MON = 3  then D.N_TOE      else 0 end ) as M3_TOE                -- 에너지사용량 TOE
        , sum( case when D.C_MON = 3  then D.N_TOE_BASE else 0 end ) as M3_TOE_BASE        -- 원단위 TOE(면적당)
        , sum( case when D.C_MON = 4  then D.N_TOE      else 0 end ) as M4_TOE                -- 에너지사용량 TOE
        , sum( case when D.C_MON = 4  then D.N_TOE_BASE else 0 end ) as M4_TOE_BASE        -- 원단위 TOE(면적당)
        , sum( case when D.C_MON = 5  then D.N_TOE             else 0 end ) as M5_TOE                -- 에너지사용량 TOE
        , sum( case when D.C_MON = 5  then D.N_TOE_BASE else 0 end ) as M5_TOE_BASE        -- 원단위 TOE(면적당)
        , sum( case when D.C_MON = 6  then D.N_TOE             else 0 end ) as M6_TOE                -- 에너지사용량 TOE
        , sum( case when D.C_MON = 6  then D.N_TOE_BASE else 0 end ) as M6_TOE_BASE        -- 원단위 TOE(면적당)
        , sum( case when D.C_MON = 7  then D.N_TOE             else 0 end ) as M7_TOE                -- 에너지사용량 TOE
        , sum( case when D.C_MON = 7  then D.N_TOE_BASE else 0 end ) as M7_TOE_BASE        -- 원단위 TOE(면적당)
        , sum( case when D.C_MON = 8  then D.N_TOE             else 0 end ) as M8_TOE                -- 에너지사용량 TOE
        , sum( case when D.C_MON = 8  then D.N_TOE_BASE else 0 end ) as M8_TOE_BASE        -- 원단위 TOE(면적당)
        , sum( case when D.C_MON = 9  then D.N_TOE             else 0 end ) as M9_TOE                -- 에너지사용량 TOE
        , sum( case when D.C_MON = 9  then D.N_TOE_BASE else 0 end ) as M9_TOE_BASE        -- 원단위 TOE(면적당)
        , sum( case when D.C_MON = 10 then D.N_TOE             else 0 end ) as M10_TOE                -- 에너지사용량 TOE
        , sum( case when D.C_MON = 10 then D.N_TOE_BASE else 0 end ) as M10_TOE_BASE -- 원단위 TOE(면적당)
        , sum( case when D.C_MON = 11 then D.N_TOE             else 0 end ) as M11_TOE                -- 에너지사용량 TOE
        , sum( case when D.C_MON = 11 then D.N_TOE_BASE else 0 end ) as M11_TOE_BASE -- 원단위 TOE(면적당)
        , sum( case when D.C_MON = 12 then D.N_TOE             else 0 end ) as M12_TOE                -- 에너지사용량 TOE
        , sum( case when D.C_MON = 12 then D.N_TOE_BASE else 0 end ) as M12_TOE_BASE -- 원단위 TOE(면적당)

        , sum( case when D.C_MON = 1  then D.N_CO2             else 0 end ) as M1_CO2                        -- 에너지사용량 CO2
        , sum( case when D.C_MON = 1  then D.N_CO2_BASE else 0 end ) as M1_CO2_BASE                -- 원단위 CO2(면적당)
        , sum( case when D.C_MON = 2  then D.N_CO2             else 0 end ) as M2_CO2                        -- 에너지사용량 CO2
        , sum( case when D.C_MON = 2  then D.N_CO2_BASE else 0 end ) as M2_CO2_BASE                -- 원단위 CO2(면적당)
        , sum( case when D.C_MON = 3  then D.N_CO2             else 0 end ) as M3_CO2                        -- 에너지사용량 CO2
        , sum( case when D.C_MON = 3  then D.N_CO2_BASE else 0 end ) as M3_CO2_BASE                -- 원단위 CO2(면적당)
        , sum( case when D.C_MON = 4  then D.N_CO2             else 0 end ) as M4_CO2                        -- 에너지사용량 CO2
        , sum( case when D.C_MON = 4  then D.N_CO2_BASE else 0 end ) as M4_CO2_BASE                -- 원단위 CO2(면적당)
        , sum( case when D.C_MON = 5  then D.N_CO2             else 0 end ) as M5_CO2                        -- 에너지사용량 CO2
        , sum( case when D.C_MON = 5  then D.N_CO2_BASE else 0 end ) as M5_CO2_BASE                -- 원단위 CO2(면적당)
        , sum( case when D.C_MON = 6  then D.N_CO2             else 0 end ) as M6_CO2                        -- 에너지사용량 CO2
        , sum( case when D.C_MON = 6  then D.N_CO2_BASE else 0 end ) as M6_CO2_BASE                -- 원단위 CO2(면적당)
        , sum( case when D.C_MON = 7  then D.N_CO2      else 0 end ) as M7_CO2                        -- 에너지사용량 CO2
        , sum( case when D.C_MON = 7  then D.N_CO2_BASE else 0 end ) as M7_CO2_BASE                -- 원단위 CO2(면적당)
        , sum( case when D.C_MON = 8  then D.N_CO2      else 0 end ) as M8_CO2                        -- 에너지사용량 CO2
        , sum( case when D.C_MON = 8  then D.N_CO2_BASE else 0 end ) as M8_CO2_BASE                -- 원단위 CO2(면적당)
        , sum( case when D.C_MON = 9  then D.N_CO2      else 0 end ) as M9_CO2                        -- 에너지사용량 CO2
        , sum( case when D.C_MON = 9  then D.N_CO2_BASE else 0 end ) as M9_CO2_BASE                -- 원단위 CO2(면적당)
        , sum( case when D.C_MON = 10 then D.N_CO2      else 0 end ) as M10_CO2                        -- 에너지사용량 CO2
        , sum( case when D.C_MON = 10 then D.N_CO2_BASE else 0 end ) as M10_CO2_BASE        -- 원단위 CO2(면적당)
        , sum( case when D.C_MON = 11 then D.N_CO2      else 0 end ) as M11_CO2                        -- 에너지사용량 CO2
        , sum( case when D.C_MON = 11 then D.N_CO2_BASE else 0 end ) as M11_CO2_BASE        -- 원단위 CO2(면적당)
        , sum( case when D.C_MON = 12 then D.N_CO2      else 0 end ) as M12_CO2                        -- 에너지사용량 CO2
        , sum( case when D.C_MON = 12 then D.N_CO2_BASE else 0 end ) as M12_CO2_BASE        -- 원단위 CO2(면적당)

        , sum( case when D.C_MON = 1  then D.S_BASE_VAL else 0 end ) as M1_BASE_VAL          -- 면적,톤킬로
        , sum( case when D.C_MON = 2  then D.S_BASE_VAL else 0 end ) as M2_BASE_VAL          -- 면적,톤킬로
        , sum( case when D.C_MON = 3  then D.S_BASE_VAL else 0 end ) as M3_BASE_VAL          -- 면적,톤킬로
        , sum( case when D.C_MON = 4  then D.S_BASE_VAL else 0 end ) as M4_BASE_VAL          -- 면적,톤킬로
        , sum( case when D.C_MON = 5  then D.S_BASE_VAL else 0 end ) as M5_BASE_VAL          -- 면적,톤킬로
        , sum( case when D.C_MON = 6  then D.S_BASE_VAL else 0 end ) as M6_BASE_VAL          -- 면적,톤킬로
        , sum( case when D.C_MON = 7  then D.S_BASE_VAL else 0 end ) as M7_BASE_VAL          -- 면적,톤킬로
        , sum( case when D.C_MON = 8  then D.S_BASE_VAL else 0 end ) as M8_BASE_VAL          -- 면적,톤킬로
        , sum( case when D.C_MON = 9  then D.S_BASE_VAL else 0 end ) as M9_BASE_VAL          -- 면적,톤킬로
        , sum( case when D.C_MON = 10 then D.S_BASE_VAL else 0 end ) as M10_BASE_VAL         -- 면적,톤킬로
        , sum( case when D.C_MON = 11 then D.S_BASE_VAL else 0 end ) as M11_BASE_VAL         -- 면적,톤킬로
        , sum( case when D.C_MON = 12 then D.S_BASE_VAL else 0 end ) as M12_BASE_VAL         -- 면적,톤킬로
from (
        select SI.ID_SITE -- 사업장 ID
                , SI.S_SITE_NM -- 사업장명
                , EP.ID_ENG_POINT
                , EP.S_POINT_NM
                , EP.S_LOGI_TYPE_CD
                , EP.S_INV_CD	-- 배출시설구분
                #, T5.S_VAL as YCT_CD -- 노선구분
                #, T3.S_DESC as YCT_NM -- 노선구분 이름
                #, T5.S_VAL as SELFVRN_CD -- 자차구분
                #, T3.S_DESC as SELFVRN_NM -- 자차구분 이름
                , V.S_SELFVRN_CD as SELFVRN_CD -- 자차구분
                , CD2.S_DESC as SELFVRN_NM -- 자차구분명
                #, V.S_DESC as SELFVRN_NM -- 자차구분 이름
                , CD1.S_CD as BASE_UNT_CD -- 원단위 기준 코드
                , CD1.S_DESC as BASE_UNT_NM -- 원단위 기준명
        FROM T_SITE_INFO as SI
        inner join T_ENG_POINT as EP on SI.id_site = EP.id_site
        left outer join (
			SELECT EP.ID_ENG_POINT, V.S_SELFVRN_CD
			FROM T_VEHICLE V
			INNER JOIN V_SITE_DEPT SD
			  ON V.ID_ERP_DEPT=SD.ID_ERP_DEPT
			INNER JOIN T_ENG_POINT EP
			  ON EP.ID_SITE=SD.ID_SITE 
				 and 
					((   EP.S_INV_CD = 10 and V.S_SELFVRN_CD = '1' ) 
					or (EP.S_INV_CD =11 and V.S_SELFVRN_CD = '2'))
			where V.C_DEL_YN<>'Y' and EP.C_DEL_YN<>'Y'
			group by EP.ID_ENG_POINT, V.S_SELFVRN_CD
		) V ON EP.ID_ENG_POINT = V.ID_ENG_POINT
        left outer join T_CODE as CD1 on CD1.S_CAT = 'BASE_UNT' and EP.S_BASE_UNT = CD1.S_CD and CD1.S_CD IN (1, 4)	-- 1=ton.km 4=면적(m2)
        left outer join T_CODE as CD2 on CD2.S_CAT = 'SELFVRN'  and V.S_SELFVRN_CD = CD2.S_CD
        
        where 
			SI.C_DEL_YN != 'Y' and -- T_SITE_INFO 삭제 여부 확인
            EP.C_DEL_YN != 'Y' -- T_ENG_POINT 삭제 여부 확인
) as M
inner join (
        select T1.ID_ENG_POINT, T1.C_YEAR
                , T1.C_MON
                , T1.S_BASE_VAL, T1.S_BASE_UNT
                , T2.S_DESC
                , T1.N_ENG, T1.N_ENG_ETC, T1.N_ENG_ETC_UNT_CD
                , T1.N_GJ, T1.N_TOE, N_CO2, T1.N_CH4, T1.N_N2O, T1.N_TCO2EQ
                , T1.N_TOE_BASE, T1.N_GJ_BASE, T1.N_CO2_BASE
        from T_ENG_GHG_AMT as T1
        left outer join T_CODE as T2 on T2.S_CAT = 'BASE_UNT' and T1.S_BASE_UNT = T2.S_CD and T2.C_DEL_YN != 'Y'
) as D
        on M.ID_ENG_POINT = D.ID_ENG_POINT -- and M.BASE_UNT_CD = D.S_BASE_UNT
group by M.ID_SITE
        , M.S_SITE_NM
        , M.ID_ENG_POINT
        #, M.YCT_CD
        #, M.YCT_NM
        , M.S_INV_CD
        , M.SELFVRN_CD
        , M.SELFVRN_NM
        , M.BASE_UNT_CD
        , M.BASE_UNT_NM
        , M.S_LOGI_TYPE_CD
        , D.C_YEAR
   ;



