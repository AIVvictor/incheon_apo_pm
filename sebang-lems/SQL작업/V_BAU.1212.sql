/*
  V_BAU_SUB
  V_BAU
  2019/12/12 kdh.
  계획 및 목표/ 목표관리 / BAU분석
*/
-- create
-- alter
-- view V_BAU_SUB as
	select EGA.C_DATE_MON
		#, FN_CALC_ADD_YM(C_DATE_MON, -12*1+1) as C_DATE_MON1	-- 1년 기준 년월 
		#, FN_CALC_ADD_YM(C_DATE_MON, -12*2+1) as C_DATE_MON2
		#, FN_CALC_ADD_YM(C_DATE_MON, -12*3+1) as C_DATE_MON3
		#, FN_CALC_ADD_YM(C_DATE_MON, -12*4+1) as C_DATE_MON4
		#, FN_CALC_ADD_YM(C_DATE_MON, -12*5+1) as C_DATE_MON5
		, SUM(EGA.N_GJ_BASE ) as N_GJ_BASE
		, SUM(EGA.N_TOE_BASE) as N_TOE_BASE
		, SUM(EGA.N_CO2_BASE) as N_CO2_BASE
	FROM T_ENG_GHG_AMT AS EGA
	INNER JOIN T_ENG_POINT as EP
		ON EGA.ID_ENG_POINT = EP.ID_ENG_POINT
	INNER JOIN T_SITE_INFO AS SI
		ON EP.ID_SITE = SI.ID_SITE
	WHERE EP.C_DEL_YN != 'Y'
		AND SI.C_DEL_YN != 'Y'
        AND EP.S_INV_CD != '11'	-- 용차 제외
	GROUP BY EGA.C_YEAR, EGA.C_MON, EGA.C_DATE_MON
;

-- create
-- alter
-- view V_BAU as
explain 
SELECT YM.YYYYMM as C_YEAR					-- 년월(YYYYMM)
	, SUM(Y0.N_GJ_BASE) as N_GJ				-- 사용실적(GJ)
	, SUM(Y1.N_GJ_BASE) as N_1_YEAR_GJ		-- 전년도 원단위 활용(GJ)
	, SUM(Y2.N_GJ_BASE) as N_2_YEAR_GJ		-- 2년평균 원단위 활용(GJ)
	, SUM(Y3.N_GJ_BASE) as N_3_YEAR_GJ		-- 3년평균 원단위 활용(GJ)
	, SUM(Y4.N_GJ_BASE) as N_4_YEAR_GJ		-- 4년평균 원단위 활용(GJ)
	, SUM(Y5.N_GJ_BASE) as N_5_YEAR_GJ		-- 5년평균 원단위 활용(GJ)
    , SUM(Y0.N_TOE_BASE) as N_TOE			-- 사용실적(TOE)
    , SUM(Y1.N_TOE_BASE) as N_1_YEAR_TOE	-- 전년도 원단위 활용(TOE)
    , SUM(Y2.N_TOE_BASE) as N_2_YEAR_TOE	-- 2년평균 원단위 활용(TOE)
    , SUM(Y3.N_TOE_BASE) as N_3_YEAR_TOE	-- 3년평균 원단위 활용(TOE)
    , SUM(Y4.N_TOE_BASE) as N_4_YEAR_TOE	-- 4년평균 원단위 활용(TOE)
    , SUM(Y5.N_TOE_BASE) as N_5_YEAR_TOE	-- 5년평균 원단위 활용(TOE)
    , SUM(Y0.N_CO2_BASE) as N_CO2			-- 사용실적(CO2)
    , SUM(Y1.N_CO2_BASE) as N_1_YEAR_CO2	-- 전년도 원단위 활용(CO2)
    , SUM(Y2.N_CO2_BASE) as N_2_YEAR_CO2	-- 2년평균 원단위 활용(CO2)
    , SUM(Y3.N_CO2_BASE) as N_3_YEAR_CO2	-- 3년평균 원단위 활용(CO2)
    , SUM(Y4.N_CO2_BASE) as N_4_YEAR_CO2	-- 4년평균 원단위 활용(CO2)
    , SUM(Y5.N_CO2_BASE) as N_5_YEAR_CO2	-- 5년평균 원단위 활용(CO2)
FROM V_YEAR_MONTH as YM
LEFT OUTER JOIN V_BAU_SUB as Y0	/* 실적 */
	on YM.YYYYMM = Y0.C_DATE_MON
LEFT OUTER JOIN (		/* 1년평균원단위 */
	SELECT YYYYMM
		, case when CNT < (12*1) then null else N_GJ_BASE end as N_GJ_BASE		-- 계산에 필요한 12*1개월만큼의 분모데이터가 없다면 null처리
        , case when CNT < (12*1) then null else N_TOE_BASE end as N_TOE_BASE	-- 계산에 필요한 12*1개월만큼의 분모데이터가 없다면 null처리
        , case when CNT < (12*1) then null else N_CO2_BASE end as N_CO2_BASE	-- 계산에 필요한 12*1개월만큼의 분모데이터가 없다면 null처리
	FROM (
		SELECT YM.YYYYMM
			, count(1) as CNT
			, AVG(BS.N_GJ_BASE) as N_GJ_BASE
			, AVG(BS.N_TOE_BASE) as N_TOE_BASE
			, AVG(BS.N_CO2_BASE) as N_CO2_BASE
		FROM V_YEAR_MONTH as YM
		LEFT OUTER JOIN V_BAU_SUB as BS
			on BS.C_DATE_MON between FN_CALC_ADD_YM(YM.YYYYMM, -12*1+1) and YM.YYYYMM	-- 1년치: 11개월 전 ~ 현재 까지 평균을 계산
		GROUP BY YM.YYYYMM
	) T1
) as Y1	on YM.YYYYMM = Y1.YYYYMM
LEFT OUTER JOIN (		/* 2년평균원단위 */
	SELECT YYYYMM
		, case when CNT < (12*2) then null else N_GJ_BASE end as N_GJ_BASE		-- 계산에 필요한 12*2개월만큼의 분모데이터가 없다면 null처리
        , case when CNT < (12*2) then null else N_TOE_BASE end as N_TOE_BASE	-- 계산에 필요한 12*2개월만큼의 분모데이터가 없다면 null처리
        , case when CNT < (12*2) then null else N_CO2_BASE end as N_CO2_BASE	-- 계산에 필요한 12*2개월만큼의 분모데이터가 없다면 null처리
	FROM (
		SELECT YM.YYYYMM
			, count(1) as CNT
			, AVG(BS.N_GJ_BASE) as N_GJ_BASE
			, AVG(BS.N_TOE_BASE) as N_TOE_BASE
			, AVG(BS.N_CO2_BASE) as N_CO2_BASE
		FROM V_YEAR_MONTH as YM
		LEFT OUTER JOIN V_BAU_SUB as BS
			on BS.C_DATE_MON between FN_CALC_ADD_YM(YM.YYYYMM, -12*2+1) and YM.YYYYMM	-- 2년치: 23개월 전 ~ 현재 까지 평균을 계산
		GROUP BY YM.YYYYMM
	) T1
) as Y2	on YM.YYYYMM = Y2.YYYYMM
LEFT OUTER JOIN (		/* 3년평균원단위 */
	SELECT YYYYMM
		, case when CNT < (12*3) then null else N_GJ_BASE end as N_GJ_BASE		-- 계산에 필요한 12*3개월만큼의 분모데이터가 없다면 null처리
        , case when CNT < (12*3) then null else N_TOE_BASE end as N_TOE_BASE	-- 계산에 필요한 12*3개월만큼의 분모데이터가 없다면 null처리
        , case when CNT < (12*3) then null else N_CO2_BASE end as N_CO2_BASE	-- 계산에 필요한 12*3개월만큼의 분모데이터가 없다면 null처리
	FROM (
		SELECT YM.YYYYMM
			, count(1) as CNT
			, AVG(BS.N_GJ_BASE) as N_GJ_BASE
			, AVG(BS.N_TOE_BASE) as N_TOE_BASE
			, AVG(BS.N_CO2_BASE) as N_CO2_BASE
		FROM V_YEAR_MONTH as YM
		LEFT OUTER JOIN V_BAU_SUB as BS
			on BS.C_DATE_MON between FN_CALC_ADD_YM(YM.YYYYMM, -12*3+1) and YM.YYYYMM	-- 1년치: 35개월 전 ~ 현재 까지 평균을 계산
		GROUP BY YM.YYYYMM
	) T1
) as Y3	on YM.YYYYMM = Y3.YYYYMM
LEFT OUTER JOIN (		/* 4년평균원단위 */
	SELECT YYYYMM
		, case when CNT < (12*4) then null else N_GJ_BASE end as N_GJ_BASE		-- 계산에 필요한 12*4개월만큼의 분모데이터가 없다면 null처리
        , case when CNT < (12*4) then null else N_TOE_BASE end as N_TOE_BASE	-- 계산에 필요한 12*4개월만큼의 분모데이터가 없다면 null처리
        , case when CNT < (12*4) then null else N_CO2_BASE end as N_CO2_BASE	-- 계산에 필요한 12*4개월만큼의 분모데이터가 없다면 null처리
	FROM (
		SELECT YM.YYYYMM
			, count(1) as CNT
			, AVG(BS.N_GJ_BASE) as N_GJ_BASE
			, AVG(BS.N_TOE_BASE) as N_TOE_BASE
			, AVG(BS.N_CO2_BASE) as N_CO2_BASE
		FROM V_YEAR_MONTH as YM
		LEFT OUTER JOIN V_BAU_SUB as BS
			on BS.C_DATE_MON between FN_CALC_ADD_YM(YM.YYYYMM, -12*4+1) and YM.YYYYMM	-- 1년치: 47개월 전 ~ 현재 까지 평균을 계산
		GROUP BY YM.YYYYMM
	) T1
) as Y4	on YM.YYYYMM = Y4.YYYYMM
LEFT OUTER JOIN (		/* 5년평균원단위 */
	SELECT YYYYMM
		, case when CNT < (12*5) then null else N_GJ_BASE end as N_GJ_BASE		-- 계산에 필요한 12*5개월만큼의 분모데이터가 없다면 null처리
        , case when CNT < (12*5) then null else N_TOE_BASE end as N_TOE_BASE	-- 계산에 필요한 12*5개월만큼의 분모데이터가 없다면 null처리
        , case when CNT < (12*5) then null else N_CO2_BASE end as N_CO2_BASE	-- 계산에 필요한 12*5개월만큼의 분모데이터가 없다면 null처리
	FROM (
		SELECT YYYYMM
			, count(1) as CNT
			, AVG(BS.N_GJ_BASE) as N_GJ_BASE
			, AVG(BS.N_TOE_BASE) as N_TOE_BASE
			, AVG(BS.N_CO2_BASE) as N_CO2_BASE
		FROM V_YEAR_MONTH as YM
		LEFT OUTER JOIN V_BAU_SUB as BS
			on BS.C_DATE_MON between FN_CALC_ADD_YM(YM.YYYYMM, -12*5+1) and YM.YYYYMM	-- 1년치: 59개월 전 ~ 현재 까지 평균을 계산
		GROUP BY YM.YYYYMM
	) T1
) as Y5	on YM.YYYYMM = Y5.YYYYMM
GROUP BY YM.YYYYMM
order by YM.YYYYMM DESC

;