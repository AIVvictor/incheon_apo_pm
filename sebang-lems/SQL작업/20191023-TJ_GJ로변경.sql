/*
	10/23 schema 변경 내역
	by richard
*/

ALTER TABLE `tlems`.`T_ENG_GHG_AMT` 
CHANGE COLUMN `N_TJ` `N_GJ` FLOAT NOT NULL COMMENT '에너지사용량 GJ 단위' ;
-- --------------------------------------------------------------------------
ALTER 
VIEW `V_GOAL_RESULT_ANALY_ACC` AS
    SELECT 
        `YG`.`C_YEAR` AS `C_YEAR`,
        `MG`.`C_MON` AS `C_MON`,
        `YG`.`ID_SITE` AS `ID_SITE`,
        `SG`.`ID_ENG_POINT` AS `ID_ENG_POINT`,
        `VP`.`S_POINT_NM` AS `S_POINT_NM`,
        `VP`.`ID_ENG_CEF` AS `ID_ENG_CEF`,
        `VP`.`S_ENG_CD_NM` AS `S_ENG_CD_NM`,
        `MG`.`N_MON_ENG_GOAL` AS `N_MON_ENG_GOAL`,
        0 AS `N_MON_GAS_GOAL`,
        `GA`.`N_ENG` AS `N_ENG`,
        `GA`.`N_GJ` AS `N_GJ`,
        `GA`.`N_TOE` AS `N_TOE`,
        `GA`.`N_CO2` AS `N_CO2`,
        `GA`.`N_CH4` AS `N_CH4`,
        `GA`.`N_N2O` AS `N_N2O`,
        `GA`.`N_TCO2EQ` AS `N_TCO2EQ`
    FROM
        ((((`T_SITE_YEAR_GOAL` `YG`
        JOIN `T_SITE_MON_SUM_GOAL` `SG` ON (`YG`.`ID_SITE_YEAR_GOAL` = `SG`.`ID_SITE_YEAR_GOAL`))
        JOIN `T_SITE_MON_GOAL` `MG` ON (`SG`.`ID_SITE_MON_SUM_GOAL` = `MG`.`ID_SITE_MON_SUM_GOAL`))
        JOIN `V_ENG_POINT` `VP` ON (`SG`.`ID_ENG_POINT` = `VP`.`ID_ENG_POINT`))
        LEFT JOIN `T_ENG_GHG_AMT` `GA` ON (`GA`.`ID_ENG_POINT` = `SG`.`ID_ENG_POINT`
            AND `YG`.`C_YEAR` = `GA`.`C_YEAR`
            AND `MG`.`C_MON` = `GA`.`C_MON`));
-- --------------------------------------------------------------------------
ALTER
VIEW `V_MON_ENG_CLOSE_SUB` AS
    SELECT 
        `SI`.`ID_SITE` AS `ID_SITE`,
        `SI`.`S_SITE_ID` AS `S_SITE_ID`,
        `SI`.`S_SITE_NM` AS `S_SITE_NM`,
        `SI`.`N_ORDER` AS `N_ORDER`,
        `SI`.`C_DEL_YN` AS `SI_DEL_YN`,
        `EGA`.`C_YEAR` AS `C_YEAR`,
        `EGA`.`C_MON` AS `C_MON`,
        IFNULL(SUM(`EGA`.`N_GJ`), 0) AS `GJ_SUM`,
        IFNULL(SUM(`EGA`.`N_TOE`), 0) AS `TOE_SUM`,
        IFNULL(SUM(`EGA`.`N_CO2`), 0) AS `CO2_SUM`,
        IFNULL(SUM(`EGA`.`N_CH4`), 0) AS `CH4_SUM`,
        IFNULL(SUM(`EGA`.`N_N2O`), 0) AS `N2O_SUM`,
        IFNULL(SUM(`EGA`.`N_TCO2EQ`), 0) AS `TCO2EQ_SUM`
    FROM
        (((`T_SITE_INFO` `SI`
        LEFT JOIN `T_ENG_POINT` `EP` ON (`EP`.`ID_SITE` = `SI`.`ID_SITE`))
        LEFT JOIN `T_ENG_GHG_AMT` `EGA` ON (`EP`.`ID_ENG_POINT` = `EGA`.`ID_ENG_POINT`))
        LEFT JOIN `T_MON_ENG_CLOSE` `MC` ON (`SI`.`ID_SITE` = `MC`.`ID_SITE`))
    GROUP BY `SI`.`ID_SITE` , `SI`.`S_SITE_ID` , `SI`.`N_ORDER` , `SI`.`C_DEL_YN` , `EGA`.`C_YEAR` , `EGA`.`C_MON`;
-- --------------------------------------------------------------------------
ALTER
VIEW `V_SUM_AMT` AS
    SELECT 
        `EP`.`ID_ENG_POINT` AS `ID_ENG_POINT`,
        `EP`.`ID_SITE` AS `ID_SITE`,
        `SI`.`S_SITE_ID` AS `S_SITE_ID`,
        `SI`.`S_SITE_NM` AS `S_SITE_NM`,
        `EP`.`ID_ENG_CEF` AS `ID_ENG_CEF`,
        `EP`.`S_POINT_ID` AS `S_POINT_ID`,
        `EP`.`S_POINT_NM` AS `S_POINT_NM`,
        `EP`.`C_POINT_YN` AS `C_POINT_YN`,
        `EP`.`N_ORDER` AS `N_ORDER`,
        `EP`.`S_INV_CD` AS `S_INV_CD`,
        `EP`.`S_INV_SEQ` AS `S_INV_SEQ`,
        `EP`.`N_CAPA` AS `N_CAPA`,
        `EP`.`S_UNT_CAPA` AS `S_UNT_CAPA`,
        `EP`.`N_CAPA2` AS `N_CAPA2`,
        `EP`.`N_UNT_CAPA2` AS `N_UNT_CAPA2`,
        `EP`.`S_LOGI_TYPE_CD` AS `S_LOGI_TYPE_CD`,
        (SELECT 
                `tlems`.`T_CODE`.`S_DESC`
            FROM
                `tlems`.`T_CODE`
            WHERE
                `tlems`.`T_CODE`.`S_CAT` = 'LOGI_TYPE'
                    AND `tlems`.`T_CODE`.`S_CD` = `EP`.`S_LOGI_TYPE_CD`) AS `LOGI_TYPE_NM`,
        `EP`.`S_REG_ID` AS `S_REG_ID`,
        `EP`.`S_BASE_VAL` AS `S_BASE_VAL`,
        `EP`.`S_BASE_UNT` AS `S_BASE_UNT`,
        (SELECT 
                `tlems`.`T_CODE`.`S_DESC`
            FROM
                `tlems`.`T_CODE`
            WHERE
                `tlems`.`T_CODE`.`S_CAT` = 'UNT'
                    AND `tlems`.`T_CODE`.`S_CD` = `EP`.`S_BASE_UNT`) AS `BASE_UNT_NM`,
        `EP`.`S_FUEL_EFF` AS `S_FUEL_EFF`,
        `EP`.`C_DEL_YN` AS `EP_DEL_YN`,
        `EC`.`S_CEF_CD` AS `S_CEF_CD`,
        (SELECT 
                `tlems`.`T_CODE`.`S_DESC`
            FROM
                `tlems`.`T_CODE`
            WHERE
                `tlems`.`T_CODE`.`S_CAT` = 'CEF'
                    AND `tlems`.`T_CODE`.`S_CD` = `EC`.`S_CEF_CD`) AS `CEF_NM`,
        `EC`.`S_ENG_CD` AS `S_ENG_CD`,
        (SELECT 
                `tlems`.`T_CODE`.`S_DESC`
            FROM
                `tlems`.`T_CODE`
            WHERE
                `tlems`.`T_CODE`.`S_CAT` = 'ENG'
                    AND `tlems`.`T_CODE`.`S_CD` = `EC`.`S_ENG_CD`) AS `ENG_NM`,
        `EC`.`S_MRVENG_CD` AS `S_MRVENG_CD`,
        (SELECT 
                `tlems`.`T_CODE`.`S_DESC`
            FROM
                `tlems`.`T_CODE`
            WHERE
                `tlems`.`T_CODE`.`S_CAT` = 'MRVENG'
                    AND `tlems`.`T_CODE`.`S_CD` = `EC`.`S_MRVENG_CD`) AS `MRVENG_NM`,
        `EC`.`S_EMT_CD` AS `S_EMT_CD`,
        (SELECT 
                `tlems`.`T_CODE`.`S_DESC`
            FROM
                `tlems`.`T_CODE`
            WHERE
                `tlems`.`T_CODE`.`S_CAT` = 'EMT'
                    AND `tlems`.`T_CODE`.`S_CD` = `EC`.`S_EMT_CD`) AS `ENT_NM`,
        `EC`.`S_IN_UNT_CD` AS `S_IN_UNT_CD`,
        (SELECT 
                `tlems`.`T_CODE`.`S_DESC`
            FROM
                `tlems`.`T_CODE`
            WHERE
                `tlems`.`T_CODE`.`S_CAT` = 'UNT'
                    AND `tlems`.`T_CODE`.`S_CD` = `EC`.`S_IN_UNT_CD`) AS `S_IN_UNT_NM`,
        `EC`.`S_CALC_UNT_CD` AS `S_CALC_UNT_CD`,
        (SELECT 
                `tlems`.`T_CODE`.`S_DESC`
            FROM
                `tlems`.`T_CODE`
            WHERE
                `tlems`.`T_CODE`.`S_CAT` = 'UNT'
                    AND `tlems`.`T_CODE`.`S_CD` = `EC`.`S_CALC_UNT_CD`) AS `S_CALC_UNT_NM`,
        `T`.`C_YEAR` AS `C_YEAR`,
        `T`.`M1_GJ` AS `M1_GJ`,
        `T`.`M1_CO2` AS `M1_CO2`,
        `T`.`M1_CH4` AS `M1_CH4`,
        `T`.`M1_N2O` AS `M1_N2O`,
        `T`.`M1_TCO2EQ` AS `M1_TCO2EQ`,
        `T`.`M2_GJ` AS `M2_GJ`,
        `T`.`M2_CO2` AS `M2_CO2`,
        `T`.`M2_CH4` AS `M2_CH4`,
        `T`.`M2_N2O` AS `M2_N2O`,
        `T`.`M2_TCO2EQ` AS `M2_TCO2EQ`,
        `T`.`M3_GJ` AS `M3_GJ`,
        `T`.`M3_CO2` AS `M3_CO2`,
        `T`.`M3_CH4` AS `M3_CH4`,
        `T`.`M3_N2O` AS `M3_N2O`,
        `T`.`M3_TCO2EQ` AS `M3_TCO2EQ`,
        `T`.`M4_GJ` AS `M4_GJ`,
        `T`.`M4_CO2` AS `M4_CO2`,
        `T`.`M4_CH4` AS `M4_CH4`,
        `T`.`M4_N2O` AS `M4_N2O`,
        `T`.`M4_TCO2EQ` AS `M4_TCO2EQ`,
        `T`.`M5_GJ` AS `M5_GJ`,
        `T`.`M5_CO2` AS `M5_CO2`,
        `T`.`M5_CH4` AS `M5_CH4`,
        `T`.`M5_N2O` AS `M5_N2O`,
        `T`.`M5_TCO2EQ` AS `M5_TCO2EQ`,
        `T`.`M6_GJ` AS `M6_GJ`,
        `T`.`M6_CO2` AS `M6_CO2`,
        `T`.`M6_CH4` AS `M6_CH4`,
        `T`.`M6_N2O` AS `M6_N2O`,
        `T`.`M6_TCO2EQ` AS `M6_TCO2EQ`,
        `T`.`M7_GJ` AS `M7_GJ`,
        `T`.`M7_CO2` AS `M7_CO2`,
        `T`.`M7_CH4` AS `M7_CH4`,
        `T`.`M7_N2O` AS `M7_N2O`,
        `T`.`M7_TCO2EQ` AS `M7_TCO2EQ`,
        `T`.`M8_GJ` AS `M8_GJ`,
        `T`.`M8_CO2` AS `M8_CO2`,
        `T`.`M8_CH4` AS `M8_CH4`,
        `T`.`M8_N2O` AS `M8_N2O`,
        `T`.`M8_TCO2EQ` AS `M8_TCO2EQ`,
        `T`.`M9_CO2` AS `M9_CO2`,
        `T`.`M9_GJ` AS `M9_GJ`,
        `T`.`M9_CH4` AS `M9_CH4`,
        `T`.`M9_N2O` AS `M9_N2O`,
        `T`.`M9_TCO2EQ` AS `M9_TCO2EQ`,
        `T`.`M10_GJ` AS `M10_GJ`,
        `T`.`M10_CO2` AS `M10_CO2`,
        `T`.`M10_CH4` AS `M10_CH4`,
        `T`.`M10_N2O` AS `M10_N2O`,
        `T`.`M10_TCO2EQ` AS `M10_TCO2EQ`,
        `T`.`M11_GJ` AS `M11_GJ`,
        `T`.`M11_CO2` AS `M11_CO2`,
        `T`.`M11_CH4` AS `M11_CH4`,
        `T`.`M11_N2O` AS `M11_N2O`,
        `T`.`M11_TCO2EQ` AS `M11_TCO2EQ`,
        `T`.`M12_GJ` AS `M12_GJ`,
        `T`.`M12_CO2` AS `M12_CO2`,
        `T`.`M12_CH4` AS `M12_CH4`,
        `T`.`M12_N2O` AS `M12_N2O`,
        `T`.`M12_TCO2EQ` AS `M12_TCO2EQ`
    FROM
        (((((`tlems`.`T_ENG_POINT` `EP`
        LEFT JOIN `tlems`.`T_ENG_CEF` `EC` ON (`EP`.`ID_ENG_CEF` = `EC`.`ID_ENG_CEF`))
        LEFT JOIN `tlems`.`T_MON_ENG_CLOSE` `MC` ON (`MC`.`ID_SITE` = `EP`.`ID_SITE`
            AND `MC`.`C_CLOSE_YN` = 'Y'))
        LEFT JOIN `tlems`.`T_SITE_INFO` `SI` ON (`EP`.`ID_SITE` = `SI`.`ID_SITE`))
        LEFT JOIN `tlems`.`T_USERS` `U` ON (`EP`.`S_REG_ID` = `U`.`S_REG_ID`))
        LEFT JOIN (SELECT 
            `T`.`ID_ENG_POINT` AS `ID_ENG_POINT`,
                `T`.`C_YEAR` AS `C_YEAR`,
                MAX(`T`.`M1_GJ`) AS `M1_GJ`,
                MAX(`T`.`M1_CO2`) AS `M1_CO2`,
                MAX(`T`.`M1_CH4`) AS `M1_CH4`,
                MAX(`T`.`M1_N2O`) AS `M1_N2O`,
                MAX(`T`.`M1_TCO2EQ`) AS `M1_TCO2EQ`,
                MAX(`T`.`M2_GJ`) AS `M2_GJ`,
                MAX(`T`.`M2_CO2`) AS `M2_CO2`,
                MAX(`T`.`M2_CH4`) AS `M2_CH4`,
                MAX(`T`.`M2_N2O`) AS `M2_N2O`,
                MAX(`T`.`M2_TCO2EQ`) AS `M2_TCO2EQ`,
                MAX(`T`.`M3_GJ`) AS `M3_GJ`,
                MAX(`T`.`M3_CO2`) AS `M3_CO2`,
                MAX(`T`.`M3_CH4`) AS `M3_CH4`,
                MAX(`T`.`M3_N2O`) AS `M3_N2O`,
                MAX(`T`.`M3_TCO2EQ`) AS `M3_TCO2EQ`,
                MAX(`T`.`M4_GJ`) AS `M4_GJ`,
                MAX(`T`.`M4_CO2`) AS `M4_CO2`,
                MAX(`T`.`M4_CH4`) AS `M4_CH4`,
                MAX(`T`.`M4_N2O`) AS `M4_N2O`,
                MAX(`T`.`M4_TCO2EQ`) AS `M4_TCO2EQ`,
                MAX(`T`.`M5_GJ`) AS `M5_GJ`,
                MAX(`T`.`M5_CO2`) AS `M5_CO2`,
                MAX(`T`.`M5_CH4`) AS `M5_CH4`,
                MAX(`T`.`M5_N2O`) AS `M5_N2O`,
                MAX(`T`.`M5_TCO2EQ`) AS `M5_TCO2EQ`,
                MAX(`T`.`M6_GJ`) AS `M6_GJ`,
                MAX(`T`.`M6_CO2`) AS `M6_CO2`,
                MAX(`T`.`M6_CH4`) AS `M6_CH4`,
                MAX(`T`.`M6_N2O`) AS `M6_N2O`,
                MAX(`T`.`M6_TCO2EQ`) AS `M6_TCO2EQ`,
                MAX(`T`.`M7_GJ`) AS `M7_GJ`,
                MAX(`T`.`M7_CO2`) AS `M7_CO2`,
                MAX(`T`.`M7_CH4`) AS `M7_CH4`,
                MAX(`T`.`M7_N2O`) AS `M7_N2O`,
                MAX(`T`.`M7_TCO2EQ`) AS `M7_TCO2EQ`,
                MAX(`T`.`M8_GJ`) AS `M8_GJ`,
                MAX(`T`.`M8_CO2`) AS `M8_CO2`,
                MAX(`T`.`M8_CH4`) AS `M8_CH4`,
                MAX(`T`.`M8_N2O`) AS `M8_N2O`,
                MAX(`T`.`M8_TCO2EQ`) AS `M8_TCO2EQ`,
                MAX(`T`.`M9_GJ`) AS `M9_GJ`,
                MAX(`T`.`M9_CO2`) AS `M9_CO2`,
                MAX(`T`.`M9_CH4`) AS `M9_CH4`,
                MAX(`T`.`M9_N2O`) AS `M9_N2O`,
                MAX(`T`.`M9_TCO2EQ`) AS `M9_TCO2EQ`,
                MAX(`T`.`M10_GJ`) AS `M10_GJ`,
                MAX(`T`.`M10_CO2`) AS `M10_CO2`,
                MAX(`T`.`M10_CH4`) AS `M10_CH4`,
                MAX(`T`.`M10_N2O`) AS `M10_N2O`,
                MAX(`T`.`M10_TCO2EQ`) AS `M10_TCO2EQ`,
                MAX(`T`.`M11_GJ`) AS `M11_GJ`,
                MAX(`T`.`M11_CO2`) AS `M11_CO2`,
                MAX(`T`.`M11_CH4`) AS `M11_CH4`,
                MAX(`T`.`M11_N2O`) AS `M11_N2O`,
                MAX(`T`.`M11_TCO2EQ`) AS `M11_TCO2EQ`,
                MAX(`T`.`M12_GJ`) AS `M12_GJ`,
                MAX(`T`.`M12_CO2`) AS `M12_CO2`,
                MAX(`T`.`M12_CH4`) AS `M12_CH4`,
                MAX(`T`.`M12_N2O`) AS `M12_N2O`,
                MAX(`T`.`M12_TCO2EQ`) AS `M12_TCO2EQ`
        FROM
            (SELECT 
            `tlems`.`T_ENG_GHG_AMT`.`ID_ENG_POINT` AS `ID_ENG_POINT`,
                `tlems`.`T_ENG_GHG_AMT`.`C_YEAR` AS `C_YEAR`,
                CASE
                    WHEN `tlems`.`T_ENG_GHG_AMT`.`C_MON` = 1 THEN `tlems`.`T_ENG_GHG_AMT`.`N_GJ`
                    ELSE NULL
                END AS `M1_GJ`,
                CASE
                    WHEN `tlems`.`T_ENG_GHG_AMT`.`C_MON` = 1 THEN `tlems`.`T_ENG_GHG_AMT`.`N_CO2`
                    ELSE NULL
                END AS `M1_CO2`,
                CASE
                    WHEN `tlems`.`T_ENG_GHG_AMT`.`C_MON` = 1 THEN `tlems`.`T_ENG_GHG_AMT`.`N_CH4`
                    ELSE NULL
                END AS `M1_CH4`,
                CASE
                    WHEN `tlems`.`T_ENG_GHG_AMT`.`C_MON` = 1 THEN `tlems`.`T_ENG_GHG_AMT`.`N_N2O`
                    ELSE NULL
                END AS `M1_N2O`,
                CASE
                    WHEN `tlems`.`T_ENG_GHG_AMT`.`C_MON` = 1 THEN `tlems`.`T_ENG_GHG_AMT`.`N_TCO2EQ`
                    ELSE NULL
                END AS `M1_TCO2EQ`,
                CASE
                    WHEN `tlems`.`T_ENG_GHG_AMT`.`C_MON` = 2 THEN `tlems`.`T_ENG_GHG_AMT`.`N_GJ`
                    ELSE NULL
                END AS `M2_GJ`,
                CASE
                    WHEN `tlems`.`T_ENG_GHG_AMT`.`C_MON` = 2 THEN `tlems`.`T_ENG_GHG_AMT`.`N_CO2`
                    ELSE NULL
                END AS `M2_CO2`,
                CASE
                    WHEN `tlems`.`T_ENG_GHG_AMT`.`C_MON` = 2 THEN `tlems`.`T_ENG_GHG_AMT`.`N_CH4`
                    ELSE NULL
                END AS `M2_CH4`,
                CASE
                    WHEN `tlems`.`T_ENG_GHG_AMT`.`C_MON` = 2 THEN `tlems`.`T_ENG_GHG_AMT`.`N_N2O`
                    ELSE NULL
                END AS `M2_N2O`,
                CASE
                    WHEN `tlems`.`T_ENG_GHG_AMT`.`C_MON` = 2 THEN `tlems`.`T_ENG_GHG_AMT`.`N_TCO2EQ`
                    ELSE NULL
                END AS `M2_TCO2EQ`,
                CASE
                    WHEN `tlems`.`T_ENG_GHG_AMT`.`C_MON` = 3 THEN `tlems`.`T_ENG_GHG_AMT`.`N_GJ`
                    ELSE NULL
                END AS `M3_GJ`,
                CASE
                    WHEN `tlems`.`T_ENG_GHG_AMT`.`C_MON` = 3 THEN `tlems`.`T_ENG_GHG_AMT`.`N_CO2`
                    ELSE NULL
                END AS `M3_CO2`,
                CASE
                    WHEN `tlems`.`T_ENG_GHG_AMT`.`C_MON` = 3 THEN `tlems`.`T_ENG_GHG_AMT`.`N_CH4`
                    ELSE NULL
                END AS `M3_CH4`,
                CASE
                    WHEN `tlems`.`T_ENG_GHG_AMT`.`C_MON` = 3 THEN `tlems`.`T_ENG_GHG_AMT`.`N_N2O`
                    ELSE NULL
                END AS `M3_N2O`,
                CASE
                    WHEN `tlems`.`T_ENG_GHG_AMT`.`C_MON` = 3 THEN `tlems`.`T_ENG_GHG_AMT`.`N_TCO2EQ`
                    ELSE NULL
                END AS `M3_TCO2EQ`,
                CASE
                    WHEN `tlems`.`T_ENG_GHG_AMT`.`C_MON` = 4 THEN `tlems`.`T_ENG_GHG_AMT`.`N_GJ`
                    ELSE NULL
                END AS `M4_GJ`,
                CASE
                    WHEN `tlems`.`T_ENG_GHG_AMT`.`C_MON` = 4 THEN `tlems`.`T_ENG_GHG_AMT`.`N_CO2`
                    ELSE NULL
                END AS `M4_CO2`,
                CASE
                    WHEN `tlems`.`T_ENG_GHG_AMT`.`C_MON` = 4 THEN `tlems`.`T_ENG_GHG_AMT`.`N_CH4`
                    ELSE NULL
                END AS `M4_CH4`,
                CASE
                    WHEN `tlems`.`T_ENG_GHG_AMT`.`C_MON` = 4 THEN `tlems`.`T_ENG_GHG_AMT`.`N_N2O`
                    ELSE NULL
                END AS `M4_N2O`,
                CASE
                    WHEN `tlems`.`T_ENG_GHG_AMT`.`C_MON` = 4 THEN `tlems`.`T_ENG_GHG_AMT`.`N_TCO2EQ`
                    ELSE NULL
                END AS `M4_TCO2EQ`,
                CASE
                    WHEN `tlems`.`T_ENG_GHG_AMT`.`C_MON` = 5 THEN `tlems`.`T_ENG_GHG_AMT`.`N_GJ`
                    ELSE NULL
                END AS `M5_GJ`,
                CASE
                    WHEN `tlems`.`T_ENG_GHG_AMT`.`C_MON` = 5 THEN `tlems`.`T_ENG_GHG_AMT`.`N_CO2`
                    ELSE NULL
                END AS `M5_CO2`,
                CASE
                    WHEN `tlems`.`T_ENG_GHG_AMT`.`C_MON` = 5 THEN `tlems`.`T_ENG_GHG_AMT`.`N_CH4`
                    ELSE NULL
                END AS `M5_CH4`,
                CASE
                    WHEN `tlems`.`T_ENG_GHG_AMT`.`C_MON` = 5 THEN `tlems`.`T_ENG_GHG_AMT`.`N_N2O`
                    ELSE NULL
                END AS `M5_N2O`,
                CASE
                    WHEN `tlems`.`T_ENG_GHG_AMT`.`C_MON` = 5 THEN `tlems`.`T_ENG_GHG_AMT`.`N_TCO2EQ`
                    ELSE NULL
                END AS `M5_TCO2EQ`,
                CASE
                    WHEN `tlems`.`T_ENG_GHG_AMT`.`C_MON` = 6 THEN `tlems`.`T_ENG_GHG_AMT`.`N_GJ`
                    ELSE NULL
                END AS `M6_GJ`,
                CASE
                    WHEN `tlems`.`T_ENG_GHG_AMT`.`C_MON` = 6 THEN `tlems`.`T_ENG_GHG_AMT`.`N_CO2`
                    ELSE NULL
                END AS `M6_CO2`,
                CASE
                    WHEN `tlems`.`T_ENG_GHG_AMT`.`C_MON` = 6 THEN `tlems`.`T_ENG_GHG_AMT`.`N_CH4`
                    ELSE NULL
                END AS `M6_CH4`,
                CASE
                    WHEN `tlems`.`T_ENG_GHG_AMT`.`C_MON` = 6 THEN `tlems`.`T_ENG_GHG_AMT`.`N_N2O`
                    ELSE NULL
                END AS `M6_N2O`,
                CASE
                    WHEN `tlems`.`T_ENG_GHG_AMT`.`C_MON` = 6 THEN `tlems`.`T_ENG_GHG_AMT`.`N_TCO2EQ`
                    ELSE NULL
                END AS `M6_TCO2EQ`,
                CASE
                    WHEN `tlems`.`T_ENG_GHG_AMT`.`C_MON` = 7 THEN `tlems`.`T_ENG_GHG_AMT`.`N_GJ`
                    ELSE NULL
                END AS `M7_GJ`,
                CASE
                    WHEN `tlems`.`T_ENG_GHG_AMT`.`C_MON` = 7 THEN `tlems`.`T_ENG_GHG_AMT`.`N_CO2`
                    ELSE NULL
                END AS `M7_CO2`,
                CASE
                    WHEN `tlems`.`T_ENG_GHG_AMT`.`C_MON` = 7 THEN `tlems`.`T_ENG_GHG_AMT`.`N_CH4`
                    ELSE NULL
                END AS `M7_CH4`,
                CASE
                    WHEN `tlems`.`T_ENG_GHG_AMT`.`C_MON` = 7 THEN `tlems`.`T_ENG_GHG_AMT`.`N_N2O`
                    ELSE NULL
                END AS `M7_N2O`,
                CASE
                    WHEN `tlems`.`T_ENG_GHG_AMT`.`C_MON` = 7 THEN `tlems`.`T_ENG_GHG_AMT`.`N_TCO2EQ`
                    ELSE NULL
                END AS `M7_TCO2EQ`,
                CASE
                    WHEN `tlems`.`T_ENG_GHG_AMT`.`C_MON` = 8 THEN `tlems`.`T_ENG_GHG_AMT`.`N_GJ`
                    ELSE NULL
                END AS `M8_GJ`,
                CASE
                    WHEN `tlems`.`T_ENG_GHG_AMT`.`C_MON` = 8 THEN `tlems`.`T_ENG_GHG_AMT`.`N_CO2`
                    ELSE NULL
                END AS `M8_CO2`,
                CASE
                    WHEN `tlems`.`T_ENG_GHG_AMT`.`C_MON` = 8 THEN `tlems`.`T_ENG_GHG_AMT`.`N_CH4`
                    ELSE NULL
                END AS `M8_CH4`,
                CASE
                    WHEN `tlems`.`T_ENG_GHG_AMT`.`C_MON` = 8 THEN `tlems`.`T_ENG_GHG_AMT`.`N_N2O`
                    ELSE NULL
                END AS `M8_N2O`,
                CASE
                    WHEN `tlems`.`T_ENG_GHG_AMT`.`C_MON` = 8 THEN `tlems`.`T_ENG_GHG_AMT`.`N_TCO2EQ`
                    ELSE NULL
                END AS `M8_TCO2EQ`,
                CASE
                    WHEN `tlems`.`T_ENG_GHG_AMT`.`C_MON` = 9 THEN `tlems`.`T_ENG_GHG_AMT`.`N_GJ`
                    ELSE NULL
                END AS `M9_GJ`,
                CASE
                    WHEN `tlems`.`T_ENG_GHG_AMT`.`C_MON` = 9 THEN `tlems`.`T_ENG_GHG_AMT`.`N_CO2`
                    ELSE NULL
                END AS `M9_CO2`,
                CASE
                    WHEN `tlems`.`T_ENG_GHG_AMT`.`C_MON` = 9 THEN `tlems`.`T_ENG_GHG_AMT`.`N_CH4`
                    ELSE NULL
                END AS `M9_CH4`,
                CASE
                    WHEN `tlems`.`T_ENG_GHG_AMT`.`C_MON` = 9 THEN `tlems`.`T_ENG_GHG_AMT`.`N_N2O`
                    ELSE NULL
                END AS `M9_N2O`,
                CASE
                    WHEN `tlems`.`T_ENG_GHG_AMT`.`C_MON` = 9 THEN `tlems`.`T_ENG_GHG_AMT`.`N_TCO2EQ`
                    ELSE NULL
                END AS `M9_TCO2EQ`,
                CASE
                    WHEN `tlems`.`T_ENG_GHG_AMT`.`C_MON` = 10 THEN `tlems`.`T_ENG_GHG_AMT`.`N_GJ`
                    ELSE NULL
                END AS `M10_GJ`,
                CASE
                    WHEN `tlems`.`T_ENG_GHG_AMT`.`C_MON` = 10 THEN `tlems`.`T_ENG_GHG_AMT`.`N_CO2`
                    ELSE NULL
                END AS `M10_CO2`,
                CASE
                    WHEN `tlems`.`T_ENG_GHG_AMT`.`C_MON` = 10 THEN `tlems`.`T_ENG_GHG_AMT`.`N_CH4`
                    ELSE NULL
                END AS `M10_CH4`,
                CASE
                    WHEN `tlems`.`T_ENG_GHG_AMT`.`C_MON` = 10 THEN `tlems`.`T_ENG_GHG_AMT`.`N_N2O`
                    ELSE NULL
                END AS `M10_N2O`,
                CASE
                    WHEN `tlems`.`T_ENG_GHG_AMT`.`C_MON` = 10 THEN `tlems`.`T_ENG_GHG_AMT`.`N_TCO2EQ`
                    ELSE NULL
                END AS `M10_TCO2EQ`,
                CASE
                    WHEN `tlems`.`T_ENG_GHG_AMT`.`C_MON` = 11 THEN `tlems`.`T_ENG_GHG_AMT`.`N_GJ`
                    ELSE NULL
                END AS `M11_GJ`,
                CASE
                    WHEN `tlems`.`T_ENG_GHG_AMT`.`C_MON` = 11 THEN `tlems`.`T_ENG_GHG_AMT`.`N_CO2`
                    ELSE NULL
                END AS `M11_CO2`,
                CASE
                    WHEN `tlems`.`T_ENG_GHG_AMT`.`C_MON` = 11 THEN `tlems`.`T_ENG_GHG_AMT`.`N_CH4`
                    ELSE NULL
                END AS `M11_CH4`,
                CASE
                    WHEN `tlems`.`T_ENG_GHG_AMT`.`C_MON` = 11 THEN `tlems`.`T_ENG_GHG_AMT`.`N_N2O`
                    ELSE NULL
                END AS `M11_N2O`,
                CASE
                    WHEN `tlems`.`T_ENG_GHG_AMT`.`C_MON` = 11 THEN `tlems`.`T_ENG_GHG_AMT`.`N_TCO2EQ`
                    ELSE NULL
                END AS `M11_TCO2EQ`,
                CASE
                    WHEN `tlems`.`T_ENG_GHG_AMT`.`C_MON` = 12 THEN `tlems`.`T_ENG_GHG_AMT`.`N_GJ`
                    ELSE NULL
                END AS `M12_GJ`,
                CASE
                    WHEN `tlems`.`T_ENG_GHG_AMT`.`C_MON` = 12 THEN `tlems`.`T_ENG_GHG_AMT`.`N_CO2`
                    ELSE NULL
                END AS `M12_CO2`,
                CASE
                    WHEN `tlems`.`T_ENG_GHG_AMT`.`C_MON` = 12 THEN `tlems`.`T_ENG_GHG_AMT`.`N_CH4`
                    ELSE NULL
                END AS `M12_CH4`,
                CASE
                    WHEN `tlems`.`T_ENG_GHG_AMT`.`C_MON` = 12 THEN `tlems`.`T_ENG_GHG_AMT`.`N_N2O`
                    ELSE NULL
                END AS `M12_N2O`,
                CASE
                    WHEN `tlems`.`T_ENG_GHG_AMT`.`C_MON` = 12 THEN `tlems`.`T_ENG_GHG_AMT`.`N_TCO2EQ`
                    ELSE NULL
                END AS `M12_TCO2EQ`
        FROM
            `tlems`.`T_ENG_GHG_AMT`
        WHERE
            1 = 1) `T`
        GROUP BY `T`.`ID_ENG_POINT` , `T`.`C_YEAR`) `T` ON (`T`.`ID_ENG_POINT` = `EP`.`ID_ENG_POINT`))	