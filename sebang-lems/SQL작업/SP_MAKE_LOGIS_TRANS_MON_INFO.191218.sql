/*
  SP_MAKE_LOGIS_TRANS_MON_INFO(월운송정보 생성)
  
  2019/11/11: richard
  2019/12/18: kdh
  
  IN:
    T_LOGIS_TRANS_INFO(운송정보)
  OUT:
    T_LOGIS_TRANS_MON_INFO(월운송정보 )
    
  Note:
    차량단위,선박단위,사업장별철도 단위로 생성.C_DATE을 기준으로 해당 월단위로 생성.
    구분
      1. 정기,비정기
      2. 사업장
      3. 연월
      
      
  */
 

-- procedure source: 2019/11/12
-- procedure source: 2019/12/18	kdh.
/*
  SP_MAKE_LOGIS_TRANS_MON_INFO
*/
DELIMITER $$
drop procedure SP_MAKE_LOGIS_TRANS_MON_INFO;
create procedure SP_MAKE_LOGIS_TRANS_MON_INFO(
  IN IN_YYYYMM VARCHAR(255)
) 
BEGIN
  /*
  Created at 2019/11/12 By Richard
  
  @DESCRIPTION
  	T_LOGIS_TRANS_INFO(운송정보)로부터 T_LOGIS_TRANS_MON_INFO(월운송정보)을 생성한다.
  @PARAM
  	IN_YYYYMM: 처리하고자 하는 YYYYMM 값. null 인 경우 default 자동으로 오늘을 기준으로 동작함
  @RETURN
  	없음
  */

	-- 어제 기준의 Year Month를 구한다.
	set @monthOfYesterday=IN_YYYYMM;
	if IN_YYYYMM is null
	then
		set @monthOfYesterday=DATE_FORMAT( DATE_ADD(now(), INTERVAL -1 DAY)/*yesterday*/,'%Y%m');
	end if;

	set @YYYYMM=@monthOfYesterday;
	set @YYYY=substring(@YYYYMM,1,4);
	set @MM=substring(@YYYYMM,5,2);

	select @YYYYMM,@YYYY,@MM;
  

	-- logging
	INSERT INTO T_SCHEDULER_LOG(SP_NM) VALUES (concat('SP_MAKE_LOGIS_TRANS_MON_INFO:',@YYYYMM));
    
/*
화물차 data 생성
*/

insert into 
	T_LOGIS_TRANS_MON_INFO
		(
		-- ID_LOGIS_TRANS_MON_INFO
		ID_TRANS_COM  -- 운수사ID
		,ID_ENG_POINT  -- 배출시설ID
		,ID_ERP_DEPT  -- EPR부서ID
		,ID_VEHICLE -- 차량ID
		,DATE_MON -- 날짜
		,SELFVRN_CD -- 자차여부
		,VRN -- 차량VRN
		,TRANS_TYPE_CD  -- 운송수단1=화물차
		,LOGI_YCT_CD -- 노선구분,정기/비정기
		,LOGI_WEI_CD -- 톤수
		,FARE_SUM -- 운임료
		,LOADAGE_SUM  -- 적재량
		-- ,WEIGHT_SUM -- 총중량
		,VACANT_DISTANCE_SUM -- 공차운행거리
		,DISTANCE_SUM -- 영차운행거리
		,REG_MOD -- 등록/수정일시,YYYYMMDDHHmiSS
		,FUEL_EFF -- 연비
		,ENG_VOL_SUM -- 연료사용량
		,MJ_SUM -- 에너지사용량(MJ)
		,GCO2_SUM -- 온실가스배출량(gCO2)
		,TON_KM_SUM -- 톤킬로, 온실가스원단위
		,TOE_SUM -- 환산톤
		,BASE_VAL -- 원단위 값
		-- ,BAS_UNT -- 원단위 
		-- ,S_REG_ID -- 등록자ID
		-- ,S_MOD_ID -- 수정자ID
		-- ,C_DEL_YN
	)
select * from (
	select 
		ID_TRANS_COM -- 운수사ID
		, ID_ENG_POINT -- 배출시설ID
		, ID_ERP_DEPT -- EPR부서ID
		, ID_VEHICLE -- 차량ID
		, YM_DATE -- 날짜
		, S_SELFVRN_CD -- 자차여부
		, S_VRN  -- 차량VRN
		, S_TRANS_TYPE_CD -- 운송수단1=화물차
		, S_LINE_CD -- 노선구분,정기/비정기
		, S_LOGI_WEI_CD -- 톤수
		, SUM_FARE -- 운임료
		, SUM_LOADAGE -- 적재량
		, SUM_VACANT_DISTANCE -- 공차운행거리
		, SUM_DISTANCE -- 영차운행거리
		, REG_MOD -- 등록/수정일시,YYYYMMDDHHmiSS
		, S_FUEL_EFF -- 연비
		, SUM_ENG_VOL -- 연료사용량
		, FN_ENG_CONVERSION('GJ',  ID_ENG_CEF, DATE_FORMAT(now(), '%Y%m%d'), SUM_ENG_VOL) as SUM_MJ	-- 에너지사용량(GJ)
		, FN_ENG_CONVERSION('CO2', ID_ENG_CEF, DATE_FORMAT(now(), '%Y%m%d'), SUM_ENG_VOL) as SUM_GCO2 -- 온실가스배출량(TCO2)
		, SUM_TON_KM -- ton.km
		, FN_ENG_CONVERSION('TOE', ID_ENG_CEF, DATE_FORMAT(now(), '%Y%m%d'), SUM_ENG_VOL) as SUM_TOE -- 환산톤
		, case 	when SUM_TON_KM=0 then 0 
				else FN_ENG_CONVERSION('CO2', ID_ENG_CEF, DATE_FORMAT(now(), '%Y%m%d'), SUM_ENG_VOL) / SUM_TON_KM end as BASE_VAL -- 온실 가스원단위 값
	FROM (
		SELECT
			V.ID_TRANS_COM -- 운수사ID
			, E.ID_ENG_POINT -- 배출시설ID
			, E.ID_ENG_CEF	-- 배출원ID
			, V.ID_ERP_DEPT -- EPR부서ID
			, V.ID_VEHICLE -- 차량ID
			, concat(YM.YYYY,YM.MM) as YM_DATE -- 날짜
			, V.S_SELFVRN_CD -- 자차여부
			, V.S_VRN  -- 차량VRN
			, 1 as S_TRANS_TYPE_CD -- 운송수단1=화물차
			, V.S_LINE_CD -- 노선구분,정기/비정기
			, V.S_LOGI_WEI_CD -- 톤수
			, ifnull(sum(T.S_FARE),0) SUM_FARE -- 운임료
			, ifnull(sum(T.S_LOADAGE),0) SUM_LOADAGE -- 적재량
			, ifnull(sum(T.N_VACANT_DISTANCE),0) SUM_VACANT_DISTANCE -- 공차운행거리
			, ifnull(sum(T.S_DISTANCE),0) SUM_DISTANCE -- 영차운행거리
			, DATE_FORMAT(NOW(),'%Y%m%d%H%i%s') REG_MOD -- 등록/수정일시,YYYYMMDDHHmiSS
			, ifnull(V.S_FUEL_EFF,0) S_FUEL_EFF -- 연비
			, ifnull(V.S_FUEL_EFF,0) * (ifnull(sum(T.S_DISTANCE),0) + ifnull(sum(T.N_VACANT_DISTANCE),0)) SUM_ENG_VOL -- 연료사용량
			, ifnull(sum(T.S_LOADAGE),0)/1000 * (ifnull(sum(T.S_DISTANCE),0) + ifnull(sum(T.N_VACANT_DISTANCE),0)) SUM_TON_KM -- 톤키로
		FROM 
			T_VEHICLE V -- 차량Master
			Cross join V_YEAR_MONTH YM
			LEFT OUTER JOIN V_SITE_DEPT S # 사업장과부서
				ON S.ID_ERP_DEPT = V.ID_ERP_DEPT -- ERP부서ID
			INNER JOIN T_ENG_POINT E -- 배출원
				ON S.ID_SITE = E.ID_SITE # 사업장ID
                and (
					(E.S_INV_CD = 10 and V.S_SELFVRN_CD = '1') or (E.S_INV_CD =11 and V.S_SELFVRN_CD = '2')
				)
				and E.S_INV_CD in (10,11) # 배출시설구분.  T_CODE.INV_CD,1=창고 2=선박(비정기) 3=철도(정기) 4=화물차(정기) 5=화물차(비정기)   6=사무실, 10=화물차(자차), 11=화물차(용차)
			LEFT OUTER JOIN T_LOGIS_TRANS_INFO T  -- 운송정보	-- select * from T_LOGIS_TRANS_INFO where C_DATE like '201912%'
				ON V.ID_VEHICLE=T.ID_VEHICLE -- 차량ID
				-- and T.S_TRANS_TYPE_CD = 1
				and substring(T.C_DATE,1,6)=@YYYYMM -- 집계할 연월 지정
		where
			V.C_DEL_YN = 'N' 
			and V.ID_ERP_DEPT != 0
            and V.ID_ERP_DEPT is not null
			and E.ID_ENG_POINT is not null
			and YM.YYYY=@YYYY and YM.MM=@MM
		group by
			V.ID_TRANS_COM -- 운수사ID
			, E.ID_ENG_POINT -- 배출시설ID
			, E.ID_ENG_CEF	-- 배출원ID
			, V.ID_ERP_DEPT -- EPR부서ID
			, V.ID_VEHICLE -- 차량ID
			, concat(YM.YYYY,YM.MM) -- 날짜
			, V.S_SELFVRN_CD -- 자차여부
			, V.S_VRN  -- 차량VRN
			, V.S_LINE_CD -- 노선구분,정기/비정기
			, V.S_LOGI_WEI_CD -- 톤수
	) U1
) U
ON DUPLICATE KEY UPDATE
	ID_TRANS_COM = U.ID_TRANS_COM -- 운수사ID
	, ID_ENG_POINT = U.ID_ENG_POINT -- 배출시설ID
	, ID_ERP_DEPT = U.ID_ERP_DEPT -- EPR부서ID
	, ID_VEHICLE = U.ID_VEHICLE -- 차량ID
	, DATE_MON = U.YM_DATE -- 날짜
	, SELFVRN_CD = U.S_SELFVRN_CD -- 자차여부
	, VRN = U.S_VRN  -- 차량VRN
	, TRANS_TYPE_CD = U.S_TRANS_TYPE_CD -- 운송수단1=화물차
	, LOGI_YCT_CD = U.S_LINE_CD -- 노선구분, 정기/비정기
	, LOGI_WEI_CD = U.S_LOGI_WEI_CD -- 톤수
	, FARE_SUM = U.SUM_FARE -- 운임료
	, LOADAGE_SUM = U.SUM_LOADAGE -- 적재량
	, VACANT_DISTANCE_SUM = U.SUM_VACANT_DISTANCE -- 공차운행거리
	, DISTANCE_SUM = U.SUM_DISTANCE -- 영차운행거리
	, REG_MOD = U.REG_MOD -- 등록/수정일시,YYYYMMDDHHmiSS
	, FUEL_EFF = U.S_FUEL_EFF -- 연비
	, ENG_VOL_SUM = U.SUM_ENG_VOL -- 연료사용량
	, MJ_SUM = U.SUM_MJ	-- 에너지사용량(GJ)
	, GCO2_SUM = U.SUM_GCO2 -- 온실가스배출량(TCO2)
	, TON_KM_SUM = U.SUM_TON_KM -- ton.km
	, TOE_SUM = U.SUM_TOE -- 환산톤
	, BASE_VAL = U.BASE_VAL -- 온실 가스원단위 값
;

  /*
    해송 data 생성
  */
  insert into 
    T_LOGIS_TRANS_MON_INFO
        (
          -- ID_LOGIS_TRANS_MON_INFO
         ID_TRANS_COM  -- 운수사ID
         ,ID_VEHICLE -- 차량ID
         ,ID_ENG_POINT  -- 배출시설ID
         ,ID_ERP_DEPT  -- EPR부서ID
         ,DATE_MON -- 날짜
         ,SELFVRN_CD -- 자차여부
         ,VRN -- 차량VRN
         ,TRANS_TYPE_CD  -- 운송수단1=화물차
         ,LOGI_YCT_CD -- 노선구분,정기/비정기
         ,LOGI_WEI_CD -- 톤수
         ,FARE_SUM -- 운임료
         ,LOADAGE_SUM  -- 적재량
         -- ,WEIGHT_SUM -- 총중량
         ,VACANT_DISTANCE_SUM -- 공차운행거리
         ,DISTANCE_SUM -- 영차운행거리
         ,REG_MOD -- 등록/수정일시,YYYYMMDDHHmiSS
         ,FUEL_EFF -- 연비
         ,ENG_VOL_SUM -- 연료사용량
         ,MJ_SUM -- 에너지사용량(MJ)
         ,GCO2_SUM -- 온실가스배출량(gCO2)
         ,TON_KM_SUM -- 톤킬로, 온실가스원단위
         ,TOE_SUM -- 환산톤
		 ,BASE_VAL -- 원단위 값
         -- ,BAS_UNT -- 온실가스원단위(gCO2eq/ton-km)
         -- ,S_REG_ID -- 등록자ID
         -- ,S_MOD_ID -- 수정자ID
         -- ,C_DEL_YN
         )
  select * from (    
    SELECT
      null ID_TRANS_COM -- 운수사ID
      ,-2 ID_VEHICLE -- 차량ID(-2:선박, -3:철송)
      ,E.ID_ENG_POINT -- 배출시설ID unique index
      ,null ID_ERP_DEPT -- EPR부서ID
      ,@YYYYMM YM_DATE -- 날짜 unique index
      ,2 S_SELFVRN_CD -- 자차여부:용차
      ,null S_VRN  -- 차량VRN
      -- ,T.S_TRANS_TYPE_CD -- 운송수단
      ,2 S_TRANS_TYPE_CD  -- 운송수단 2=선박
      ,2 S_LINE_CD -- 노선구분,정기(1)/비정기(2)
      ,0 S_LOGI_WEI_CD -- 톤수
      -- ,T.S_START_PNT -- 출발지
      -- ,T.S_DEST_PNT -- 도착지
      ,0 SUM_LOADAGE -- 적재량
      ,0 SUM_FARE -- 운임료
      ,0 SUM_VACANT_DISTANCE -- 공차운행거리
      ,0 SUM_DISTANCE -- 영차운행거리
      ,DATE_FORMAT(NOW(),'%Y%m%d%H%i%s') REG_MOD -- 등록/수정일시,YYYYMMDDHHmiSS
      ,E.S_FUEL_EFF -- 연비
      ,0 SUM_ENG_VOL -- 연료사용량
      ,0 SUM_MJ -- 에너지사용량(MJ)
      ,0 SUM_GCO2 -- 온실가스배출량(gCO2)
      ,0 SUM_TON_KM -- 톤킬로, 온실가스원단위
      ,0 SUM_TOE -- 환산톤
      ,0 -- 온실가스원단위(gCO2eq/ton-km)
    FROM
      T_ENG_POINT E -- 배출시설
      LEFT OUTER JOIN V_SITE_DEPT S -- 사업장과부서
        ON E.ID_SITE = S.ID_SITE -- 사업장
    where
      E.S_INV_CD = 2 -- 배출시설 선박
	group by E.ID_ENG_POINT
  ) U  
  ON DUPLICATE KEY UPDATE
	 ID_TRANS_COM = U.ID_TRANS_COM -- 운수사ID
	 , ID_VEHICLE = U.ID_VEHICLE -- 차량ID(-2:선박, -3:철송)
	 , ID_ENG_POINT = U.ID_ENG_POINT -- 배출시설ID unique index
	 , ID_ERP_DEPT = U.ID_ERP_DEPT -- EPR부서ID
	 , DATE_MON = U.YM_DATE -- 날짜 unique index
	 , SELFVRN_CD = U.S_SELFVRN_CD -- 자차여부:용차
	 , VRN = U.S_VRN  -- 차량VRN
	 , TRANS_TYPE_CD = U.S_TRANS_TYPE_CD  -- 운송수단 2=선박
	 , LOGI_YCT_CD = U.S_LINE_CD -- 노선구분,정기(1)/비정기(2)
	 , LOGI_WEI_CD = U.S_LOGI_WEI_CD -- 톤수
	 , FARE_SUM = U.SUM_LOADAGE -- 적재량
	 , LOADAGE_SUM  = U.SUM_FARE -- 운임료
	 , VACANT_DISTANCE_SUM = U.SUM_VACANT_DISTANCE -- 공차운행거리
	 , DISTANCE_SUM = U.SUM_DISTANCE -- 영차운행거리
	 , REG_MOD = U.REG_MOD -- 등록/수정일시,YYYYMMDDHHmiSS
	 , FUEL_EFF = U.S_FUEL_EFF -- 연비
	 , ENG_VOL_SUM = U.SUM_ENG_VOL -- 연료사용량
	 , MJ_SUM = U.SUM_MJ -- 에너지사용량(MJ)
	 , GCO2_SUM = U.SUM_GCO2 -- 온실가스배출량(gCO2)
	 , TON_KM_SUM = U.SUM_TON_KM -- 톤킬로, 온실가스원단위
	 , TOE_SUM = U.SUM_TOE -- 환산톤
	 , BASE_VAL = 0 -- 온실가스원단위(gCO2eq/ton-km)
  ;

  /*
    철송 data 생성
  */
insert into 
	T_LOGIS_TRANS_MON_INFO
      (
       ID_TRANS_COM  -- 운수사ID
       ,ID_VEHICLE -- 차량ID
       ,ID_ENG_POINT  -- 배출시설ID
       ,ID_ERP_DEPT  -- EPR부서ID
       ,DATE_MON -- 날짜
       ,SELFVRN_CD -- 자차여부
       ,VRN -- 차량VRN
       ,TRANS_TYPE_CD  -- 운송수단1=화물차
       ,LOGI_YCT_CD -- 노선구분,정기/비정기
       ,LOGI_WEI_CD -- 톤수
       ,FARE_SUM -- 운임료
       ,LOADAGE_SUM  -- 적재량
       -- ,WEIGHT_SUM -- 총중량
       ,VACANT_DISTANCE_SUM -- 공차운행거리
       ,DISTANCE_SUM -- 영차운행거리
       ,REG_MOD -- 등록/수정일시,YYYYMMDDHHmiSS
       ,FUEL_EFF -- 연비
       ,ENG_VOL_SUM -- 연료사용량
       ,MJ_SUM -- 에너지사용량(MJ)
       ,GCO2_SUM -- 온실가스배출량(gCO2)
       ,TON_KM_SUM -- 톤킬로, 온실가스원단위
       ,TOE_SUM -- 환산톤
       ,BASE_VAL -- 온실가스원단위(gCO2eq/ton-km)
       -- ,S_REG_ID -- 등록자ID
       -- ,S_MOD_ID -- 수정자ID
       -- ,C_DEL_YN
       )
select * from (  
    select ID_TRANS_COM -- 운수사ID
		, ID_VEHICLE -- 차량ID
		, ID_ENG_POINT -- 배출시설ID
		, ID_ERP_DEPT -- ERP부서ID
		, YM_DATE -- 날짜
		, S_SELFVRN_CD -- 자차여부:용차
		, null S_VRN  -- 차량VRN
		, 3 S_TRANS_TYPE_CD  -- 운송수단 3=철도
		, 1 S_LINE_CD -- 노선구분,정기(1)/비정기(2)
		, 0 S_LOGI_WEI_CD -- 톤수
		, SUM_FARE -- 운임료
		, SUM_LOADAGE -- 적재량
		, SUM_VACANT_DISTANCE -- 공차운행거리
		, SUM_DISTANCE -- 영차운행거리
		, DATE_FORMAT(NOW(),'%Y%m%d%H%i%s') REG_MOD -- 등록/수정일시,YYYYMMDDHHmiSS
		, S_FUEL_EFF -- 연비
		, SUM_ENG_VOL -- 연료사용량
		, FN_ENG_CONVERSION('GJ',  T.ID_ENG_CEF, DATE_FORMAT(now(), '%Y%m%d'), SUM_ENG_VOL) as SUM_MJ	-- 에너지사용량(GJ)
		, FN_ENG_CONVERSION('CO2', T.ID_ENG_CEF, DATE_FORMAT(now(), '%Y%m%d'), SUM_ENG_VOL) as SUM_GCO2 -- 온실가스배출량(TCO2)
		, SUM_TON_KM
		, FN_ENG_CONVERSION('TOE', T.ID_ENG_CEF, DATE_FORMAT(now(), '%Y%m%d'), SUM_ENG_VOL) as SUM_TOE -- 환산톤
		, case 	when SUM_TON_KM=0 then 0 
				else FN_ENG_CONVERSION('CO2', T.ID_ENG_CEF, DATE_FORMAT(now(), '%Y%m%d'), SUM_ENG_VOL) / SUM_TON_KM end as BASE_VAL -- 온실 가스원단위 값
	from (
		select
			null ID_TRANS_COM -- 운수사ID
            ,-3 ID_VEHICLE -- 차량ID(-2:선박, -3:철송)
			, E.ID_ENG_POINT -- 배출시설ID
			, E.ID_ENG_CEF	-- 배출원ID
			, null ID_ERP_DEPT -- ERP부서ID
			, concat(YM.YYYY,YM.MM) YM_DATE -- 날짜
			, 2 as S_SELFVRN_CD -- 자차여부:용차
			, null S_VRN  -- 차량VRN
			, 3 S_TRANS_TYPE_CD  -- 운송수단 3=철도
			, 1 S_LINE_CD -- 노선구분,정기(1)/비정기(2)
			, 0 S_LOGI_WEI_CD -- 톤수
			, IFNULL(sum(T.S_FARE), 0) SUM_FARE -- 운임료
			, IFNULL(sum(T.S_LOADAGE), 0) SUM_LOADAGE -- 적재량
			, IFNULL(sum(T.N_VACANT_DISTANCE), 0) SUM_VACANT_DISTANCE -- 공차운행거리
			, IFNULL(sum(T.S_DISTANCE), 0) SUM_DISTANCE -- 영차운행거리
			, DATE_FORMAT(NOW(),'%Y%m%d%H%i%s') REG_MOD -- 등록/수정일시,YYYYMMDDHHmiSS
			, E.S_FUEL_EFF -- 연비
			, ifnull(E.S_FUEL_EFF,0) * (ifnull(sum(T.S_DISTANCE),0) + ifnull(sum(T.N_VACANT_DISTANCE),0)) SUM_ENG_VOL -- 연료사용량
			-- , IFNULL(sum(T.N_MJ), 0) SUM_MJ -- 에너지사용량(MJ)
			-- , IFNULL(sum(T.N_GCO2), 0) SUM_GCO2 -- 온실가스배출량(gCO2)
			, ifnull(sum(T.S_LOADAGE),0)/1000 * (ifnull(sum(T.S_DISTANCE),0) + ifnull(sum(T.N_VACANT_DISTANCE),0)) SUM_TON_KM -- 톤키로, 온실가스원단위
			-- , IFNULL(sum(T.N_TOE), 0) SUM_TOE -- 환산톤
			-- , 0 as BASE_VAL -- 온실가스원단위(gCO2eq/ton-km)
			  
		FROM T_ENG_POINT as E -- 배출시설
		Cross join V_YEAR_MONTH as YM	-- 연월
		left join V_SITE_DEPT as S -- 사업장과부서	-- select * from V_SITE_DEPT
			on E.ID_SITE = S.ID_SITE
		LEFT OUTER JOIN T_LOGIS_TRANS_INFO as T  -- 운송정보	-- select * from T_LOGIS_TRANS_INFO where S_TRANS_TYPE_CD = 3
			ON S.ID_ERP_DEPT = T.ID_ERP_DEPT -- ERP부서ID
			and substring(T.C_DATE,1,6) = concat(YM.YYYY, YM.MM) -- 집계할 연월 지정
			and T.S_TRANS_TYPE_CD = 3 -- 철도(3)
		where E.S_INV_CD = 3 -- 철도(3)
			and YM.yyyy = @YYYY
			and YM.mm = @MM
		group by E.ID_ENG_POINT
			, E.ID_ENG_CEF
			, YM.YYYY, YM.MM
			, E.S_FUEL_EFF
	) T
  ) U      
  ON DUPLICATE KEY UPDATE
	ID_TRANS_COM = U.ID_TRANS_COM -- 운수사ID
       , ID_VEHICLE = U.ID_VEHICLE -- 차량ID
       , ID_ENG_POINT = U.ID_ENG_POINT -- 배출시설ID
       , ID_ERP_DEPT = U.ID_ERP_DEPT -- ERP부서ID
       , DATE_MON = U.YM_DATE -- 날짜
       , SELFVRN_CD = U.S_SELFVRN_CD -- 자차여부:용차
       , VRN = U.S_VRN  -- 차량VRN
       , TRANS_TYPE_CD = U.S_TRANS_TYPE_CD  -- 운송수단 3=철도
       , LOGI_YCT_CD = U.S_LINE_CD -- 노선구분,정기(1)/비정기(2)
       , LOGI_WEI_CD = U.S_LOGI_WEI_CD -- 톤수
       , FARE_SUM = U.SUM_FARE -- 운임료
       , LOADAGE_SUM = U.SUM_LOADAGE -- 적재량
       , VACANT_DISTANCE_SUM = U.SUM_VACANT_DISTANCE -- 공차운행거리
       , DISTANCE_SUM = U.SUM_DISTANCE -- 영차운행거리
       , REG_MOD = U.REG_MOD -- 등록/수정일시,YYYYMMDDHHmiSS
       , FUEL_EFF = U.S_FUEL_EFF -- 연비
       , ENG_VOL_SUM = U.SUM_ENG_VOL -- 연료사용량
       , MJ_SUM = U.SUM_MJ	-- 에너지사용량(GJ)
       , GCO2_SUM = U.SUM_GCO2 -- 온실가스배출량(TCO2)
       , TON_KM_SUM = U.SUM_TON_KM
       , TOE_SUM = U.SUM_TOE -- 환산톤
       , BASE_VAL = U.BASE_VAL -- 온실 가스원단위 값
  ;

END $$
DELIMITER ;

call SP_MAKE_LOGIS_TRANS_MON_INFO('201904');
call SP_MAKE_LOGIS_TRANS_MON_INFO(null);

