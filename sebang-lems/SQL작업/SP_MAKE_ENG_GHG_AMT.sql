/*
    SP_MAKE_ENG_GHG_AMT
*/

delimiter $$

DROP PROCEDURE `SP_MAKE_ENG_GHG_AMT`;

CREATE PROCEDURE SP_MAKE_ENG_GHG_AMT(
  IN IN_YYYY VARCHAR(255),
  IN IN_MM VARCHAR(255)
)
BEGIN
  /*
  Created at 2019/11/12 By Richard
  
  @DESCRIPTION
  	T_LOGIS_TRANS_INFO(운송정보)로부터 T_ENG_POINT_AMT을 생성한다.
  @PARAM
  	IN_YYYY: 처리하고자 하는 YYYY값. null 인 경우 default 자동으로 어제를 기준으로 동작함
    IN_MM: 처리하고자 하는 YYYY값. null 인 경우 default 자동으로 어제를 기준으로 동작함
  @RETURN
  	없음
  */

  -- 어제 기준의 Year Month를 구한다.
  set @yearOfYesterday=IN_YYYY;
  set @monthOfYesterday=LPAD(IN_MM,2,'0');
  if IN_YYYY is null
  then
    set @yearOfYesterday=DATE_FORMAT( DATE_ADD(now(), INTERVAL -1 DAY)/*yesterday*/,'%Y');
  end if;
  
  if IN_MM is null
  then
    set @monthOfYesterday=DATE_FORMAT( DATE_ADD(now(), INTERVAL -1 DAY)/*yesterday*/,'%m');
  end if;
  
  set @YYYYMMOfYesterday = concat(@yearOfYesterday,@monthOfYesterday);
  select  @yearOfYesterday, @monthOfYesterday, @YYYYMMOfYesterday;

	-- logging
	INSERT INTO T_SCHEDULER_LOG(SP_NM) VALUES ( concat('SP_MAKE_ENG_GHG_AMT:',@yearOfYesterday,',',@monthOfYesterday));
    




insert into T_ENG_GHG_AMT(
	ID_ENG_POINT, C_YEAR, C_MON, C_DATE_MON, N_ENG, N_ENG_ETC, N_ENG_ETC_UNT_CD, N_GJ, N_TOE, N_CO2, N_CH4, N_N2O, N_TCO2EQ, S_BASE_VAL, S_BASE_UNT, N_TOE_BASE, N_GJ_BASE, N_CO2_BASE
)
select *
from (
	select
		ID_ENG_POINT, C_YEAR, C_MON, C_DATE_MON, N_ENG, N_ENG_ETC, N_ENG_ETC_UNT_CD, N_GJ, N_TOE, N_TCO2, N_CH4, N_N2O, N_TCO2EQ, S_BASE_VAL, S_BASE_UNT,
		FN_CALC_BASE_VAL(N_TOE,  T1.S_BASE_VAL) as N_TOE_BASE,
        FN_CALC_BASE_VAL(N_GJ,   T1.S_BASE_VAL) as N_GJ_BASE,
        FN_CALC_BASE_VAL(N_TCO2, T1.S_BASE_VAL) as N_CO2_BASE        
	FROM (
		SELECT 
			TEP.ID_ENG_POINT,
			TEPA.C_YEAR,	-- yyyy
			TEPA.C_MON,	-- month
			CONCAT(TEPA.C_YEAR, LPAD(TEPA.C_MON, 2, '0')) as C_DATE_MON,	-- yyyy-mm
			TEP.S_POINT_NM,
			TEPA.N_USE_AMT as N_ENG,	-- 원사용량
			TEPA.N_ENG_ETC as N_ENG_ETC,	-- 기타사용량
			TEPA.N_ENG_ETC_UNT_CD as N_ENG_ETC_UNT_CD,	-- 기타사용량 단위,  S_IN_UNT_CD: 입력단위, S_CALC_UNT_CD:계산단위 -- (select S_DESC from T_CODE TC where TEC.S_CALC_UNT_CD=TC.S_CD AND TC.S_CAT='UNT')
			FN_ENG_CONVERSION('GJ',  TEP.ID_ENG_CEF, DATE_FORMAT(now(), '%Y%m%d'), TEPA.N_USE_AMT) as N_GJ,	-- 단위 환산을 마친 데이터
			FN_ENG_CONVERSION('TOE', TEP.ID_ENG_CEF, DATE_FORMAT(now(), '%Y%m%d'), TEPA.N_USE_AMT) as N_TOE,
			FN_ENG_CONVERSION('CO2', TEP.ID_ENG_CEF, DATE_FORMAT(now(), '%Y%m%d'), TEPA.N_USE_AMT) as N_TCO2, -- 정은봉 확인 사항 <===== 11/25 S_CALC_UNT_CD , ID_ENG_CEF
            -- FN_ENG_CONVERSION('CO2', TEC.ID_ENG_CEF, DATE_FORMAT(now(), '%Y%m%d'), TEPA.N_USE_AMT) as N_TCO2, -- 정은봉 확인 사항 <===== 11/25
            
            -- FN_ENG_CONVERSION('CO2',VP.ID_ENG_CEF,'',`MG`.`N_MON_ENG_GOAL`) `N_MON_GAS_GOAL`,
            
			null as N_CH4,
			null as N_N2O,
			null as N_TCO2EQ
			
            /* if 운송, 해송, 철송 THEN T_ENG_POINT_AMT.S_BASE_VAL ELSE T_ENG_POINT.S_BASE_VAL */
            /*
			CASE	-- S_INV_CD IN (2,3,10,11)	-- 배출시설구분.  T_CODE.INV_CD,1=창고 2=선박(비정기) 3=철도(정기) 4=화물차(정기) 5=화물차(비정기)   6=사무실
				WHEN TEP.S_INV_CD IN (2,3,10,11) THEN TEPA.S_BASE_VAL
				ELSE TEP.S_BASE_VAL
				END AS S_BASE_VAL,
			*/
			, TEPA.N_ENG_ETC as S_BASE_VAL	-- SP_MAKE_ENG_POINT_AMT 에서 TEPA에 미리 넣어주므로 그대로 가져다 쓴다.
            
            /*
			CASE	-- S_INV_CD IN (2,3,10,11)	-- 배출시설구분.  T_CODE.INV_CD,1=창고 2=선박(비정기) 3=철도(정기) 4=화물차(정기) 5=화물차(비정기)   6=사무실
				WHEN TEP.S_INV_CD IN (2,3,10,11) THEN '1'	-- [1 = tonkm] 안들어가있을 수 있으므로 강제로 넣어준다.
				ELSE TEP.S_BASE_UNT
				END AS S_BASE_UNT,
			*/
            , TEPA.N_ENG_ETC_UNT_CD as S_BASE_UNT	-- SP_MAKE_ENG_POINT_AMT 에서 TEPA에 미리 넣어주므로 그대로 가져다 쓴다.
			
			, null as N_ENG_UNT_CD	-- 작성 필요
		FROM T_ENG_POINT_AMT as TEPA	-- select * from T_ENG_POINT_AMT
		INNER JOIN T_ENG_POINT as TEP	-- select * from T_ENG_POINT
			ON TEPA.ID_ENG_POINT = TEP.ID_ENG_POINT
		INNER JOIN T_ENG_CEF as TEC
			ON TEP.ID_ENG_CEF = TEC.ID_ENG_CEF
		where TEP.C_DEL_YN != 'Y'
			and C_YEAR = @yearOfYesterday
			and C_MON = @monthOfYesterday
	) AS T1
) AS T1
ON DUPLICATE KEY UPDATE
    ID_ENG_POINT       = T1.ID_ENG_POINT
	, C_YEAR           = T1.C_YEAR
	, C_MON            = T1.C_MON
	, C_DATE_MON       = T1.C_DATE_MON
	, N_ENG            = T1.N_ENG
	, N_ENG_ETC        = T1.N_ENG_ETC
	, N_ENG_ETC_UNT_CD = T1.N_ENG_ETC_UNT_CD
	, N_GJ             = T1.N_GJ
	, N_TOE            = T1.N_TOE
	, N_CO2            = T1.N_TCO2
	, N_CH4            = T1.N_CH4
	, N_N2O            = T1.N_N2O
	, N_TCO2EQ         = T1.N_TCO2EQ
	, S_BASE_VAL       = T1.S_BASE_VAL
	, S_BASE_UNT       = T1.S_BASE_UNT
	, N_TOE_BASE       = T1.N_TOE_BASE
	, N_GJ_BASE        = T1.N_GJ_BASE
	, N_CO2_BASE       = T1.N_CO2_BASE
;
END $$
delimiter ;

/*
ALTER TABLE `tlems`.`T_ENG_GHG_AMT` 
ADD UNIQUE INDEX `ID_ENG_GHG_AMT_UNIQUE2` (`ID_ENG_POINT` ASC, `C_YEAR` ASC, `C_MON` ASC)
*/
;



set @yearOfYesterday='2019';
set @monthOfYesterday='11';

select *
from (
	select
		ID_ENG_POINT, 
        S_POINT_NM,
        C_YEAR, C_MON, C_DATE_MON, N_ENG, N_ENG_ETC, N_ENG_ETC_UNT_CD, 
            S_CALC_UNT_CD,
            ID_ENG_CEF,
        N_GJ, N_TOE, N_TCO2, 
        N_GJ2, N_TOE2, N_TCO22, 
        N_CH4, N_N2O, N_TCO2EQ, S_BASE_VAL, S_BASE_UNT,
		(case when T1.S_BASE_VAL !=0 then N_TOE/T1.S_BASE_VAL  else 0 end) as N_TOE_BASE,
		(case when T1.S_BASE_VAL !=0 then N_GJ/T1.S_BASE_VAL   else 0 end) as N_GJ_BASE,
		(case when T1.S_BASE_VAL !=0 then N_TCO2/T1.S_BASE_VAL else 0 end) as N_CO2_BASE
	FROM (
		SELECT 
			TEP.ID_ENG_POINT,
			TEPA.C_YEAR,	-- yyyy
			TEPA.C_MON,	-- month
			CONCAT(TEPA.C_YEAR, LPAD(TEPA.C_MON, 2, '0')) as C_DATE_MON,	-- yyyy-mm
			TEP.S_POINT_NM,
			TEPA.N_USE_AMT as N_ENG,	-- 원사용량
			null as N_ENG_ETC,	-- 
			null as N_ENG_ETC_UNT_CD,	-- S_IN_UNT_CD: 입력단위, S_CALC_UNT_CD:계산단위 -- (select S_DESC from T_CODE TC where TEC.S_CALC_UNT_CD=TC.S_CD AND TC.S_CAT='UNT')
            TEC.S_CALC_UNT_CD,
            TEC.ID_ENG_CEF,
			FN_ENG_CONVERSION('GJ',  TEC.S_CALC_UNT_CD, DATE_FORMAT(now(), '%Y%m%d'), TEPA.N_USE_AMT) as N_GJ,	-- 단위 환산을 마친 데이터
			FN_ENG_CONVERSION('TOE', TEC.S_CALC_UNT_CD, DATE_FORMAT(now(), '%Y%m%d'), TEPA.N_USE_AMT) as N_TOE,
			FN_ENG_CONVERSION('CO2', TEC.S_CALC_UNT_CD, DATE_FORMAT(now(), '%Y%m%d'), TEPA.N_USE_AMT) as N_TCO2,
            
			FN_ENG_CONVERSION('GJ',  TEC.ID_ENG_CEF, DATE_FORMAT(now(), '%Y%m%d'), TEPA.N_USE_AMT) as N_GJ2,	-- 단위 환산을 마친 데이터
			FN_ENG_CONVERSION('TOE', TEC.ID_ENG_CEF, DATE_FORMAT(now(), '%Y%m%d'), TEPA.N_USE_AMT) as N_TOE2,
            FN_ENG_CONVERSION('CO2', TEC.ID_ENG_CEF, DATE_FORMAT(now(), '%Y%m%d'), TEPA.N_USE_AMT) as N_TCO22,
           
            -- FN_ENG_CONVERSION('CO2',VP.ID_ENG_CEF,'',`MG`.`N_MON_ENG_GOAL`) `N_MON_GAS_GOAL`,
            
			null as N_CH4,
			null as N_N2O,
			null as N_TCO2EQ
            
			/* if 운송, 해송, 철송 THEN T_ENG_POINT_AMT.S_BASE_VAL ELSE T_ENG_POINT.S_BASE_VAL */
            /*
			CASE	-- S_INV_CD IN (2,3,10,11)	-- 배출시설구분.  T_CODE.INV_CD,1=창고 2=선박(비정기) 3=철도(정기) 4=화물차(정기) 5=화물차(비정기)   6=사무실
				WHEN TEP.S_INV_CD IN (2,3,10,11) THEN TEPA.S_BASE_VAL
				ELSE TEP.S_BASE_VAL
				END AS S_BASE_VAL,
			*/
			, TEPA.N_ENG_ETC as S_BASE_VAL
            
            /*
			CASE	-- S_INV_CD IN (2,3,10,11)	-- 배출시설구분.  T_CODE.INV_CD,1=창고 2=선박(비정기) 3=철도(정기) 4=화물차(정기) 5=화물차(비정기)   6=사무실
				WHEN TEP.S_INV_CD IN (2,3,10,11) THEN '1'	-- [1 = tonkm] 안들어가있을 수 있으므로 강제로 넣어준다.
				ELSE TEP.S_BASE_UNT
				END AS S_BASE_UNT,
			*/
            , TEPA.N_ENG_ETC_UNT_CD as S_BASE_UNT
            
			, null as N_ENG_UNT_CD	-- 작성 필요
		FROM T_ENG_POINT_AMT as TEPA	-- select * from T_ENG_POINT_AMT
		INNER JOIN T_ENG_POINT as TEP	-- select * from T_ENG_POINT
			ON TEPA.ID_ENG_POINT = TEP.ID_ENG_POINT
		INNER JOIN T_ENG_CEF as TEC
			ON TEP.ID_ENG_CEF = TEC.ID_ENG_CEF
		where TEP.C_DEL_YN != 'Y'
			and C_YEAR = @yearOfYesterday
			and C_MON = @monthOfYesterday
	) AS T1
) AS T1
;

select * from T_ENG_POINT 
;