CREATE 
    ALGORITHM = UNDEFINED 
    DEFINER = `lems`@`%` 
    SQL SECURITY DEFINER
VIEW `V_CEF_MOD` AS
    SELECT 
        `CM`.`ID_CEF_MOD` AS `ID_CEF_MOD`,
        `CM`.`ID_ENG_CEF` AS `ID_ENG_CEF`,
        `CM`.`C_YEAR` AS `C_YEAR`,
        `CM`.`N_HEAT` AS `N_HEAT`,
        `CM`.`N_TOT_HEAT` AS `N_TOT_HEAT`,
        `CM`.`N_CO2_CEF` AS `N_CO2_CEF`,
        `CM`.`N_CH4_CEF` AS `N_CH4_CEF`,
        `CM`.`N_N2O_CEF` AS `N_N2O_CEF`,
        `CM`.`N_MOD_CAL` AS `N_MOD_CAL`,
        `CM`.`S_GHG_CD` AS `S_GHG_CD`,
        `CM`.`C_PER` AS `C_PER`,
        `CM`.`S_REG_ID` AS `S_REG_ID`,
        `CM`.`D_REG_DATE` AS `D_REG_DATE`,
        `CM`.`S_MOD_ID` AS `S_MOD_ID`,
        `CM`.`D_MOD_DATE` AS `D_MOD_DATE`,
        `CM`.`C_DEL_YN` AS `C_DEL_YN`,
        `CM`.`N_TOE_CEF` AS `N_TOE_CEF`,
        (SELECT 
                `T_CODE`.`S_DESC`
            FROM
                `T_CODE`
            WHERE
                `T_CODE`.`S_CAT` = 'GHG'
                    AND `T_CODE`.`S_CD` = `CM`.`S_GHG_CD`) AS `S_GHG_CD_NM`
    FROM
        `T_CEF_MOD` `CM`
    WHERE
        `CM`.`C_DEL_YN` = 'N'