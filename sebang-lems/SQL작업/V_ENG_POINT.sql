
;
-- CREATE 
-- ALTER
-- VIEW V_ENG_POINT AS
    SELECT 
        p.ID_SITE AS ID_SITE,               -- 사업장ID
        s.S_SITE_ID AS S_SITE_ID,           -- S사업장ID ('S'+9(7).S0000003,S+ID_SITE(7))
        s.S_SITE_NM AS S_SITE_NM,           -- 사업장명
        p.ID_ENG_POINT AS ID_ENG_POINT,     -- 배출시설ID
        p.ID_ENG_CEF AS ID_ENG_CEF,         -- 배출원ID
        
        c.S_ENG_CD AS S_ENG_CD,             -- 사용 에너지원 코드
        CD1.S_DESC AS S_ENG_CD_NM,          -- 사용 에너지원 코드 명
        CD1.N_ORDER AS N_ENG_ORDER,         -- 사용 에너지원 정렬순서
        
        c.S_MRVENG_CD AS S_MRVENG_CD,       -- 정부보고 에너지원
        CD2.S_DESC AS S_MRVENG_CD_NM,       -- 정부보고 에너지원
        CD2.N_ORDER AS N_MRVENG_ORDER,      -- 정부보고 에너지원 정렬순서
        
        c.S_EMT_CD AS S_EMT_CD,             -- 배출원 구분 코드
        CD3.S_DESC AS S_EMT_CD_NM,          -- 배출원 구분 코드 명
        CD3.N_ORDER AS N_EMT_ORDER,         -- 배출원 구분 정렬순서
        
        c.S_IN_UNT_CD AS S_IN_UNT_CD,       -- 입력단위 코드
        CD4.S_DESC AS S_IN_UNT_CD_NM,       -- 입력단위 코드 명
        CD4.N_ORDER AS N_IN_UNT_ORDER,      -- 입력단위 정렬순서
        
        c.S_CALC_UNT_CD AS S_CALC_UNT_CD,   -- 계산단위 코드
        CD5.S_DESC AS S_CALC_UNT_NM,        -- 계산단위 코드 명
        CD5.N_ORDER AS N_CALC_UNT_ORDER,    -- 계산단위 정렬순서
        
		c.S_OTH_CEF_NM AS S_OTH_CEF_NM,     -- 배출원.기타온실가스
        c.S_OTH_PURPOSE AS S_OTH_PURPOSE,   -- 배출원.사용목적
        
        p.SUB_SEQ AS SUB_SEQ,               -- 배출시설 SUB SEQ    
        p.S_POINT_ID AS S_POINT_ID,         -- 배출시설 ID (배출시설ID.S_SITE_ID(8)+SUB SEQ(6))
        p.S_POINT_NM AS S_POINT_NM,         -- 배출시설 명
        p.C_POINT_YN AS C_POINT_YN,         -- 배출시설 사용여부
        p.N_ORDER AS N_ORDER,               -- 배출시설 정렬순서
        
        p.S_INV_CD AS S_INV_CD,             -- 배출시설구분 코드
        CD6.S_DESC AS S_INV_CD_NM,          -- 배출시설구분 코드 명
        CD6.N_ORDER AS N_INV_ORDER,         -- 배출시설구분 코드 정렬순서
        
        p.S_INV_SEQ AS S_INV_SEQ,           -- 배출시설 일련번호
        p.C_S_YEAR AS C_S_YEAR,             -- 시작년도
        p.C_E_YEAR AS C_E_YEAR,             -- 종료년도
        
        p.S_LOGI_TYPE_CD AS S_LOGI_TYPE_CD, -- 물류구분 코드
        CD7.S_DESC AS S_LOGI_TYPE_CD_NM,    -- 물류구분 코드 명
        CD7.N_ORDER AS N_LOGI_TYPE_ORDER,   -- 물류구분 정렬순서

        p.N_CAPA AS N_CAPA,                 -- 용량
        p.S_UNT_CAPA AS S_UNT_CAPA,         -- 용량단위
        CD8.S_DESC AS S_UNT_CAPA_CD_NM,     -- 용량단위 코드 명
        CD8.N_ORDER AS N_UNT_CAPA_ORDER,    -- 용량단위 정렬순서
                
        p.N_CAPA2 AS N_CAPA2,               -- 세부시설용량
        p.N_UNT_CAPA2 AS N_UNT_CAPA2,       -- 세부시설용량단위
        CD9.S_DESC AS N_UNT_CAPA2_CD_NM,    -- 세부시설용량단위 코드 명
        CD9.N_ORDER AS N_UNT_CAPA2_ORDER,   -- 세부시설용량단위 정렬순서
        
        p.S_REG_ID AS S_REG_ID,             -- 등록자
        p.D_REG_DATE AS D_REG_DATE,         -- 등록일
        p.S_MOD_ID AS S_MOD_ID,             -- 변경자
        p.D_MOD_DATE AS D_MOD_DATE,         -- 변경일
        p.C_DEL_YN AS C_DEL_YN,             -- 삭제여부
        
        p.S_BASE_VAL AS S_BASE_VAL,         -- 원단위 기준 값
        p.S_BASE_UNT AS S_BASE_UNT,         -- 원단위 기준 단위
        CD10.S_DESC AS S_BASE_UNT_NM,       -- 원단위 기준 단위 명
        CD10.N_ORDER AS N_BASE_UNT_ORDER,   -- 원단위 기준 단위 정렬순서
        
        p.S_FUEL_EFF AS S_FUEL_EFF,         -- 공인연비
        s.N_ORDER AS N_SITE_ORDER           -- 사업장 정렬순서
        
    FROM T_ENG_POINT as p
    INNER JOIN T_SITE_INFO as s ON (p.ID_SITE = s.ID_SITE)
    LEFT JOIN T_ENG_CEF as c ON (c.ID_ENG_CEF = p.ID_ENG_CEF AND c.C_DEL_YN = 'N')
    LEFT JOIN T_CODE as CD1  ON (CD1.S_CAT = 'ENG'        AND CD1.S_CD = c.S_ENG_CD)
    LEFT JOIN T_CODE as CD2  ON (CD2.S_CAT = 'MRVENG'     AND CD2.S_CD = c.S_MRVENG_CD)
    LEFT JOIN T_CODE as CD3  ON (CD3.S_CAT = 'EMT'        AND CD3.S_CD = c.S_EMT_CD)
    LEFT JOIN T_CODE as CD4  ON (CD4.S_CAT = 'UNT'        AND CD4.S_CD = c.S_IN_UNT_CD)
    LEFT JOIN T_CODE as CD5  ON (CD5.S_CAT = 'UNT'        AND CD5.S_CD = c.S_CALC_UNT_CD)
    LEFT JOIN T_CODE as CD6  ON (CD6.S_CAT = 'INV'        AND CD6.S_CD = p.S_INV_CD)
    LEFT JOIN T_CODE as CD7  ON (CD7.S_CAT = 'LOGI_TYPE'  AND CD7.S_CD = p.S_LOGI_TYPE_CD)
    LEFT JOIN T_CODE as CD8  ON (CD8.S_CAT = 'UNT'        AND CD8.S_CD = p.S_UNT_CAPA)
    LEFT JOIN T_CODE as CD9  ON (CD9.S_CAT = 'UNT'        AND CD9.S_CD = p.N_UNT_CAPA2)
    LEFT JOIN T_CODE as CD10 ON (CD10.S_CAT = 'UNT'       AND CD10.S_CD = p.S_BASE_UNT)
    
    WHERE p.C_DEL_YN = 'N'
        AND s.C_DEL_YN = 'N'
;

select * from T_ENG_POINT;
sGhgCdNm (온실가스 코드 이름값)