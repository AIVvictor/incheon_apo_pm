/*
  SP_MAKE_ENG_POINT_AMT: 월 실적정보생성 (화물차,철송,해송)
  화면: 온실가스 에너지/ 에너지 실적등록 / 사용실적
  
  IN
    T_LOGIS_TRANS_MON_INFO 월운송정보
    
    T_ENG_POINT 배출시설
    
  OUT
    T_ENG_POINT_AMT
*/
;

select * from T_LOGIS_TRANS_INFO;
select * from T_LOGIS_TRANS_MON_INFO order by TRANS_TYPE_CD desc; -- 월운송정보
select * from T_LOGIS_TRANS_MON_INFO; -- 월운송정보
select * from T_ENG_POINT; -- 배출시설
select * from T_ENG_POINT_AMT; -- 에너지사용량 실적관리
select * from T_ENG_CEF; -- 배출원 관리
select * from V_SITE_DEPT;
select * from T_SITE_INFO;

select * from T_LOGIS_TRANS_INFO;

select * from T_ENG_POINT_AMT -- 에너지사용량 실적관리
where ID_ENG_POINT_AMT='980'
;

select * from T_ENG_POINT
;
select * from T_ENG_POINT_AMT -- 에너지사용량 실적관리
where C_YEAR=2019 and C_MON=11
;

-- delete from T_ENG_POINT_AMT -- 에너지사용량 실적관리
where C_YEAR=2019 and C_MON=11
;

select * from T_ENG_POINT_AMT EPA -- 에너지사용량 실적관리
left join T_ENG_POINT EP
	ON EPA.ID_ENG_POINT = EP.ID_ENG_POINT
		and EP.ID_ENG_POINT is not null
where C_YEAR=2019 and C_MON=11
;

select * from V_LOGIS_TRANS_YEAR_SHIP
;
select * from V_LOGIS_TRANS_YEAR_TRAIN
;
select * from V_LOGIS_TRANS_YEAR_VHCL
;

-- 1. 배출시설의 갯수만큼 빈 record를 생성한다.
insert into T_ENG_POINT_AMT
  (ID_ENG_POINT -- 배출시설ID
  ,ID_SITE -- 사업장ID
  ,C_YEAR -- 연도
  ,C_MON -- 월
  ,N_USE_AMT -- 사용량
  ,N_COST -- 금액
  ,N_ENG_ETC -- 가동시간 등 기준 data 의 입력 값  
  ,N_ENG_ETC_UNT_CD -- 기타사용량의 단위. T_CODE.UNT 참고(예:20=시간)
  -- ,G_BILL_ID  -- 고지서ID
  -- ,S_REG_ID  -- 등록자ID
  -- ,D_REG_DATE
  -- ,S_MOD_ID
  -- ,D_MOD_DATE
  -- ,C_DEL_YN
  ,N_GJ   -- 에너지(GJ), 사용량 입력후 서버에서 등록전에 계산
  ,N_TCO2 -- 온실가스(tCO2), 사용량 입력후 서버에서 등록전에 계산
  ,N_TOE   -- 에너지사용량_TOE. 사용량 입력수 서에서 등록전에 계산
  ,S_BASE_VAL -- 원단위 기준값
  )
select * from (  
  select 
    E.ID_ENG_POINT -- 배출시설ID
    ,S.ID_SITE -- 사업장ID
    -- ,S.S_SITE_ID -- 사업장ID
    -- ,S.S_SITE_NM -- 사업장명
    ,DATE_FORMAT(NOW(),'%Y') YEAR -- 연도
    ,DATE_FORMAT(NOW(),'%m') MON -- 월
    ,0 USE_AMT        -- 사용량
    ,0 COST           -- 금액
    ,0 ENG_ETC        -- 가동시간 등 기준 data 의 입력 값        
    ,0 ENG_ETC_UNT_CD -- 기타사용량의 단위. T_CODE.UNT 참고(예:20=시간)
    -- ,G_BILL_ID        -- 고지서ID
    -- ,S_REG_ID         -- 등록자ID
    ,0 GJ             -- 에너지(GJ), 사용량 입력후 서버에서 등록전에 계산
    ,0 TCO2           -- 온실가스(tCO2), 사용량 입력후 서버에서 등록전에 계산
    ,0 TOE            -- 에너지사용량_TOE. 사용량 입력수 서에서 등록전에 계산
    ,0 BASE_VAL       -- 원단위 기준값
  from 
    T_ENG_POINT E -- 배출시설
    -- LEFT OUTER JOIN T_ENG_CEF C -- 배출원 관리
    --   ON E.ID_ENG_CEF = C.ID_ENG_CEF
    LEFT OUTER JOIN T_SITE_INFO S -- 사업장과 부서
      ON E.ID_SITE = S.ID_SITE
      ) U
ON DUPLICATE KEY UPDATE
    ID_ENG_POINT=U.ID_ENG_POINT
  ;
  
  
select 
*
from 
  T_ENG_POINT E -- 배출시설
  -- LEFT OUTER JOIN T_ENG_CEF C -- 배출원 관리
  --   ON E.ID_ENG_CEF = C.ID_ENG_CEF
  LEFT OUTER JOIN T_SITE_INFO S -- 사업장과 부서
    ON E.ID_SITE = S.ID_SITE
  ;
  
  
set @yearOfYesterday='2019';
set @monthOfYesterday='11';
select 
		  E.ID_ENG_POINT -- 배출시설ID
		  ,S.ID_SITE -- 사업장ID
		   ,S.S_SITE_ID -- 사업장ID
		   ,S.S_SITE_NM -- 사업장명
		  ,@yearOfYesterday YEAR -- 연도
		  ,@monthOfYesterday MON -- 월
		  ,0 USE_AMT        -- 사용량
		  ,0 COST           -- 금액
		  ,0 ENG_ETC        -- 가동시간 등 기준 data 의 입력 값        
		  ,0 ENG_ETC_UNT_CD -- 기타사용량의 단위. T_CODE.UNT 참고(예:20=시간)
		  -- ,G_BILL_ID        -- 고지서ID
		  -- ,S_REG_ID         -- 등록자ID
		  ,0 GJ             -- 에너지(GJ), 사용량 입력후 서버에서 등록전에 계산
		  ,0 TCO2           -- 온실가스(tCO2), 사용량 입력후 서버에서 등록전에 계산
		  ,0 TOE            -- 에너지사용량_TOE. 사용량 입력수 서에서 등록전에 계산
		  ,case when E.S_BASE_VAL is not null then  E.S_BASE_VAL else 0 end as BASE_VAL       -- 원단위 기준값
		from 
		  T_ENG_POINT E -- 배출시설
		  -- LEFT OUTER JOIN T_ENG_CEF C -- 배출원 관리
		  --   ON E.ID_ENG_CEF = C.ID_ENG_CEF
		  LEFT OUTER JOIN T_SITE_INFO S -- 사업장과 부서
			ON E.ID_SITE = S.ID_SITE
				and S.C_DEL_YN='N'
				and E.C_DEL_YN='N'
		where
			S.ID_SITE is not null
;


select * from T_ENG_POINT E
;
/*
 2. 화물차,철도,선박의 경우 
 
    T_LOGIS_TRANS_MON_INFO(월운송정보)로부터 그 합을 가지고 온다.
      MJ_SUM      에너지사용량(MJ)
      GCO2_SUM    온실가스배출량(gCO2)
      TOE_SUM     온실가스배출량(gCO2)
      TON_KM_SUM  톤킬로, 온실가스원단위
      
    T_ENG_POINT_AMT 에 update 한다.
      N_USE_AMT   사용량
      N_COST      사용금액
      
      N_GJ        에너지(GJ), 사용량 입력후 서버에서 등록전에 계산
      N_TCO2      온실가스(tCO2), 사용량 입력후 서버에서 등록전에 계산
      N_TOE       에너지사용량_TOE. 사용량 입력수 서에서 등록전에 계산
      S_BASE_VAL  원단위기준 값(면적값,톤키로값)
    
    배출시설이 2=선박(비정기) 3=철도(정기) 4=화물차(정기) 5=화물차(비정기) 인 에너지사용량 실적관리 record list를 구한다.   
*/
select 
  EA.ID_ENG_POINT_AMT -- ID
  ,EA.ID_ENG_POINT  -- 배출시설ID
  ,EA.N_USE_AMT   -- 사용량
  ,EA.N_COST      -- 사용금액
  ,EA.N_GJ        -- 에너지(GJ), 사용량 입력후 서버에서 등록전에 계산
  ,EA.N_TCO2      -- 온실가스(tCO2), 사용량 입력후 서버에서 등록전에 계산
  ,EA.N_TOE       -- 에너지사용량_TOE. 사용량 입력수 서에서 등록전에 계산
  ,EA.S_BASE_VAL  -- 원단위기준 값(먄적값,톤키로값)
  ,E.S_BASE_UNT   -- 원단위 기준 단위. T_CODE에 BASE_UNT 참조(m3,ton.km,hour,...등)
  -- ,EA.*
   ,E.*
from 
  T_ENG_POINT_AMT EA -- 에너지사용량 실적관리
  LEFT OUTER JOIN T_ENG_POINT E -- 배출시설
    ON EA.ID_ENG_POINT = E.ID_ENG_POINT  
where
  C_YEAR='2019' and C_MON='11' 
  and E.S_INV_CD in (2,3,10,11) -- 배출시설구분. 2=선박(비정기) 3=철도(정기) 4=화물차(정기) 5=화물차(비정기)   
;

select 
  ID_ENG_POINT, DATE_MON, TRANS_TYPE_CD, LOGI_YCT_CD 
  ,MJ_SUM     -- 에너지사용량(MJ)
  ,GCO2_SUM    -- 온실가스배출량(gCO2)
  ,TOE_SUM    -- 온실가스배출량(gCO2)
  ,TON_KM_SUM -- 톤킬로, 온실가스원단위
from 
  T_LOGIS_TRANS_MON_INFO M -- 월운송정보
where 
  -- ID_ENG_POINT=1 and 
  DATE_MON='201911'
 ;
 
 select * from T_LOGIS_TRANS_MON_INFO;
 
select 
  ID_ENG_POINT, DATE_MON, TRANS_TYPE_CD, LOGI_YCT_CD 
  ,sum(FARE_SUM) FARE_SUM -- 요금
  ,sum(ENG_VOL_SUM)  ENG_VOL_SUM -- 연료사용량
  ,sum(MJ_SUM    ) MJ_SUM -- 에너지사용량(MJ)
  ,sum(GCO2_SUM  ) GCO2_SUM -- 온실가스배출량(gCO2)
  ,sum(TOE_SUM   ) TOE_SUM -- 온실가스배출량(gCO2)
  ,sum(TON_KM_SUM) TON_KM_SUM -- 톤킬로, 온실가스원단위
from 
  T_LOGIS_TRANS_MON_INFO M -- 월운송정보
where 
  -- ID_ENG_POINT=1 and 
  DATE_MON='201911'
group by
  ID_ENG_POINT, DATE_MON, TRANS_TYPE_CD, LOGI_YCT_CD
;

select * from T_ENG_POINT_AMT where C_YEAR='2019' and C_MON='11';

update T_ENG_POINT_AMT D
      ,(select 
          ID_ENG_POINT, DATE_MON, TRANS_TYPE_CD, LOGI_YCT_CD 
          ,sum(MJ_SUM    ) MJ_SUM -- 에너지사용량(MJ)
          ,sum(GCO2_SUM  ) GCO2_SUM -- 온실가스배출량(gCO2)
          ,sum(TOE_SUM   ) TOE_SUM -- 온실가스배출량(gCO2)
          ,sum(TON_KM_SUM) TON_KM_SUM -- 톤킬로, 온실가스원단위
        from 
          T_LOGIS_TRANS_MON_INFO M -- 월운송정보
        where 
          -- ID_ENG_POINT=1 and 
          DATE_MON='201911'
        group by
          ID_ENG_POINT, DATE_MON, TRANS_TYPE_CD, LOGI_YCT_CD
        ) S
SET 
  --  D.N_USE_AMT  =  0 -- 사용량
  -- ,D.N_COST     =  0-- 사용금액
  D.N_GJ       =  S.MJ_SUM -- 에너지(GJ), 사용량 입력후 서버에서 등록전에 계산
  ,D.N_TCO2     =  S.GCO2_SUM -- 온실가스(tCO2), 사용량 입력후 서버에서 등록전에 계산
  ,D.N_TOE      =  S.TOE_SUM -- 에너지사용량_TOE. 사용량 입력수 서에서 등록전에 계산
  ,D.S_BASE_VAL =  S.TON_KM_SUM -- 원단위기준 값(먄적값,톤키로값)
  -- ,D.BASE_UNT   =  1-- 원단위 기준 단위. T_CODE에 BASE_UNT 참조(m3,ton.km,hour,...등)

WHERE D.ID_ENG_POINT = S.ID_ENG_POINT
  and C_YEAR='2019' and C_MON='11'
;

select 
  EA.ID_ENG_POINT_AMT
  ,EA.ID_ENG_POINT
  ,EA.C_YEAR
  ,EA.C_MON
  ,S.S_SITE_NM
  ,EP.S_POINT_NM
  ,EP.S_INV_CD
  ,C.S_DESC
from 
  T_ENG_POINT_AMT EA
  LEFT JOIN T_ENG_POINT EP
    ON EA.ID_ENG_POINT = EP.ID_ENG_POINT
  LEFT JOIN T_SITE_INFO S
    ON EA.ID_SITE = S.ID_SITE
  LEFT JOIN T_CODE C
    ON C.S_CAT='INV' and EP.S_INV_CD=C.S_CD
where
  EA.C_YEAR='2019' and EA.C_MON='11'
  and EP.S_INV_CD in (2,3,10,11)
    ;

select 
  E.ID_ENG_POINT -- 배출시설ID
  ,S.ID_SITE -- 사업장ID
  -- ,S.S_SITE_ID -- 사업장ID
  -- ,S.S_SITE_NM -- 사업장명
  ,DATE_FORMAT(NOW(),'%Y') YEAR -- 연도
  ,DATE_FORMAT(NOW(),'%m') MON -- 월
  ,0 USE_AMT        -- 사용량
  ,0 COST           -- 금액
  ,0 ENG_ETC        -- 가동시간 등 기준 data 의 입력 값        
  ,0 ENG_ETC_UNT_CD -- 기타사용량의 단위. T_CODE.UNT 참고(예:20=시간)
  -- ,G_BILL_ID        -- 고지서ID
  -- ,S_REG_ID         -- 등록자ID
  ,0 GJ             -- 에너지(GJ), 사용량 입력후 서버에서 등록전에 계산
  ,0 TCO2           -- 온실가스(tCO2), 사용량 입력후 서버에서 등록전에 계산
  ,0 TOE            -- 에너지사용량_TOE. 사용량 입력수 서에서 등록전에 계산
  ,0 BASE_VAL       -- 원단위 기준값
from 
  T_ENG_POINT_AMT EA -- 에너지사용량 실적관리
  -- T_ENG_POINT E -- 배출시설
  -- LEFT OUTER JOIN T_ENG_CEF C -- 배출원 관리
  --   ON E.ID_ENG_CEF = C.ID_ENG_CEF
  LEFT OUTER JOIN T_SITE_INFO S -- 사업장과 부서
    ON E.ID_SITE = S.ID_SITE
;

-- 검증용 query
select S.S_SITE_NM,P.S_POINT_NM,A.* from T_ENG_POINT_AMT A
left join T_ENG_POINT P
    ON A.ID_ENG_POINT=P.ID_ENG_POINT
left JOIN T_SITE_INFO S
    ON A.ID_SITE = S.ID_SITE
where c_year=2019 and c_mon=12
    and A.S_BASE_VAL is not null and A.S_BASE_VAL <> 0
order by S.s_site_nm   
;


-- procedure source: 2019/11/12
call SP_MAKE_ENG_POINT_AMT('2019','11');
call SP_MAKE_ENG_POINT_AMT(null,null);
/*
  SP_MAKE_ENG_POINT_AMT
*/
DELIMITER $$
drop procedure SP_MAKE_ENG_POINT_AMT;
create procedure SP_MAKE_ENG_POINT_AMT(
  IN IN_YYYY VARCHAR(255),
  IN IN_MM VARCHAR(255)
) 
BEGIN
  /*
  Created at 2019/11/12 By Richard
  
  @DESCRIPTION
  	T_LOGIS_TRANS_INFO(운송정보)로부터 T_ENG_POINT_AMT을 생성한다.
  @PARAM
  	IN_YYYY: 처리하고자 하는 YYYY값. null 인 경우 default 자동으로 어제를 기준으로 동작함
    IN_MM: 처리하고자 하는 YYYY값. null 인 경우 default 자동으로 어제를 기준으로 동작함
  @RETURN
  	없음
  */

  -- 어제 기준의 Year Month를 구한다.
  set @yearOfYesterday=IN_YYYY;
  set @monthOfYesterday=LPAD(IN_MM,2,'0');
  if IN_YYYY is null
  then
    set @yearOfYesterday=DATE_FORMAT( DATE_ADD(now(), INTERVAL -1 DAY)/*yesterday*/,'%Y');
  end if;
  
  if IN_MM is null
  then
    set @monthOfYesterday=DATE_FORMAT( DATE_ADD(now(), INTERVAL -1 DAY)/*yesterday*/,'%m');
  end if;
  
  set @YYYYMMOfYesterday = concat(@yearOfYesterday,@monthOfYesterday);
  select  @yearOfYesterday, @monthOfYesterday, @YYYYMMOfYesterday;

	-- logging
	INSERT INTO T_SCHEDULER_LOG(SP_NM) VALUES ( concat('SP_MAKE_ENG_POINT_AMT:',@yearOfYesterday,',',@monthOfYesterday));
    
  -- 1. 배출시설의 갯수만큼 빈 record를 생성한다.
  insert into T_ENG_POINT_AMT
    (ID_ENG_POINT -- 배출시설ID
    ,ID_SITE -- 사업장ID
    ,C_YEAR -- 연도
    ,C_MON -- 월
    ,N_USE_AMT -- 사용량
    ,N_COST -- 금액
    ,N_ENG_ETC -- 가동시간 등 기준 data 의 입력 값  
    ,N_ENG_ETC_UNT_CD -- 기타사용량의 단위. T_CODE.UNT 참고(예:20=시간)
    -- ,G_BILL_ID  -- 고지서ID
    -- ,S_REG_ID  -- 등록자ID
    -- ,D_REG_DATE
    -- ,S_MOD_ID
    -- ,D_MOD_DATE
    -- ,C_DEL_YN
    ,N_GJ   -- 에너지(GJ), 사용량 입력후 서버에서 등록전에 계산
    ,N_TCO2 -- 온실가스(tCO2), 사용량 입력후 서버에서 등록전에 계산
    ,N_TOE   -- 에너지사용량_TOE. 사용량 입력수 서에서 등록전에 계산
    ,S_BASE_VAL -- 원단위 기준값
    )
	select * from (  
		select 
		  E.ID_ENG_POINT -- 배출시설ID
		  ,S.ID_SITE -- 사업장ID
		  -- ,S.S_SITE_ID -- 사업장ID
		  -- ,S.S_SITE_NM -- 사업장명
		  ,@yearOfYesterday YEAR -- 연도
		  ,@monthOfYesterday MON -- 월
		  ,0 USE_AMT        -- 사용량
		  ,0 COST           -- 금액
		  ,0 ENG_ETC        -- 가동시간 등 기준 data 의 입력 값        
		  ,0 ENG_ETC_UNT_CD -- 기타사용량의 단위. T_CODE.UNT 참고(예:20=시간)
		  -- ,G_BILL_ID        -- 고지서ID
		  -- ,S_REG_ID         -- 등록자ID
		  ,0 GJ             -- 에너지(GJ), 사용량 입력후 서버에서 등록전에 계산
		  ,0 TCO2           -- 온실가스(tCO2), 사용량 입력후 서버에서 등록전에 계산
		  ,0 TOE            -- 에너지사용량_TOE. 사용량 입력수 서에서 등록전에 계산
		  ,case when E.S_BASE_VAL is not null then  E.S_BASE_VAL else 0 end as BASE_VAL       -- 원단위 기준값
		from 
		  T_ENG_POINT E -- 배출시설
		  -- LEFT OUTER JOIN T_ENG_CEF C -- 배출원 관리
		  --   ON E.ID_ENG_CEF = C.ID_ENG_CEF
		  LEFT OUTER JOIN T_SITE_INFO S -- 사업장과 부서
			ON E.ID_SITE = S.ID_SITE
				and S.C_DEL_YN='N'
				and E.C_DEL_YN='N'
		where
			S.ID_SITE is not null
        ) U
  ON DUPLICATE KEY UPDATE
      ID_ENG_POINT=U.ID_ENG_POINT	-- 아무것도 안한것과 같음.
    ;
  
  -- 2. 화물차,철도,선박의 경우 T_LOGIS_TRANS_MON_INFO(월운송정보)로부터 그 합을 가지고 온다.
  update T_ENG_POINT_AMT D
      ,(select 
          ID_ENG_POINT, DATE_MON, TRANS_TYPE_CD, LOGI_YCT_CD 
			,sum(FARE_SUM) FARE_SUM -- 요금
			,sum(ENG_VOL_SUM)  ENG_VOL_SUM -- 연료사용량
          ,sum(MJ_SUM    ) MJ_SUM -- 에너지사용량(MJ)
          ,sum(GCO2_SUM  ) GCO2_SUM -- 온실가스배출량(gCO2)
          ,sum(TOE_SUM   ) TOE_SUM -- 온실가스배출량(gCO2)
          ,sum(TON_KM_SUM) TON_KM_SUM -- 톤킬로, 온실가스원단위
        from 
          T_LOGIS_TRANS_MON_INFO M -- 월운송정보
        where 
          -- ID_ENG_POINT=1 and 
          DATE_MON=@YYYYMMOfYesterday
        group by
          ID_ENG_POINT, DATE_MON, TRANS_TYPE_CD, LOGI_YCT_CD
        ) S
  SET 
    D.N_USE_AMT  =  S.ENG_VOL_SUM -- 사용량
    ,D.N_COST     =  S.FARE_SUM -- 사용금액
    ,D.N_GJ       =  S.MJ_SUM -- 에너지(GJ), 사용량 입력후 서버에서 등록전에 계산
    ,D.N_TCO2     =  S.GCO2_SUM -- 온실가스(tCO2), 사용량 입력후 서버에서 등록전에 계산
    ,D.N_TOE      =  S.TOE_SUM -- 에너지사용량_TOE. 사용량 입력수 서에서 등록전에 계산
    ,D.S_BASE_VAL =  S.TON_KM_SUM -- 원단위기준 값(면적값,톤키로값)			-- 함수로 바꿔주어야 함
    -- ,D.S_BASE_UNT   =  1 -- 원단위 기준 단위. T_CODE에 BASE_UNT 참조(m3,ton.km,hour,...등)
    , D.N_ENG_ETC =  S.TON_KM_SUM -- 기타사용량, 가동시간 등 기준 data 의 입력 값
    , D.N_ENG_ETC_UNT_CD = 1 -- 원단위 기준 단위. T_CODE에 BASE_UNT 참조(m3,ton.km,hour,...등)
  WHERE D.ID_ENG_POINT = S.ID_ENG_POINT
    and C_YEAR=@yearOfYesterday and C_MON=@monthOfYesterday
  ;
  
  -- 3. 이동체가 아닌(운송정보테이블에 없는) 배출시설물의 기타사용량과 기타사용량 단위를 update 한다.
  update T_ENG_POINT_AMT D
      ,(select ID_ENG_POINT, S_BASE_VAL, S_BASE_UNT, S_INV_CD
        from T_ENG_POINT EP -- 배출시설정보
        where c_del_yn != 'Y'
      ) S
  SET 
    D.N_ENG_ETC =  S.S_BASE_VAL -- 기타사용량, 가동시간 등 기준 data 의 입력 값
    , D.N_ENG_ETC_UNT_CD =  S.S_BASE_UNT -- 기타사용량 단위, 기타사용량의 단위. T_CODE.BASE_UNT 참고(예:20=시간)
    
  WHERE D.ID_ENG_POINT = S.ID_ENG_POINT
    and D.C_YEAR=@yearOfYesterday and D.C_MON=@monthOfYesterday
    and S.S_INV_CD not in (2,3,10,11)	-- 이동체 제외
  ;
  # select * from T_ENG_POINT_AMT where c_year = 2017 and c_mon = '05'
END $$
DELIMITER ;

set @yearOfYesterday ='2019';
set @monthOfYesterday='11';

select 
      E.ID_ENG_POINT -- 배출시설ID
      ,E.S_POINT_NM
      -- ,E.ID_SITE -- 사업장ID
      ,S.ID_SITE -- 사업장ID
      -- ,S.S_SITE_ID -- 사업장ID
      ,S.S_SITE_NM -- 사업장명
      ,@yearOfYesterday YEAR -- 연도
      ,@monthOfYesterday MON -- 월
      ,0 USE_AMT        -- 사용량
      ,0 COST           -- 금액
      ,0 ENG_ETC        -- 가동시간 등 기준 data 의 입력 값        
      ,0 ENG_ETC_UNT_CD -- 기타사용량의 단위. T_CODE.UNT 참고(예:20=시간)
      -- ,G_BILL_ID        -- 고지서ID
      -- ,S_REG_ID         -- 등록자ID
      ,0 GJ             -- 에너지(GJ), 사용량 입력후 서버에서 등록전에 계산
      ,0 TCO2           -- 온실가스(tCO2), 사용량 입력후 서버에서 등록전에 계산
      ,0 TOE            -- 에너지사용량_TOE. 사용량 입력수 서에서 등록전에 계산
      ,0 BASE_VAL       -- 원단위 기준값
    from 
      T_ENG_POINT E -- 배출시설
      -- LEFT OUTER JOIN T_ENG_CEF C -- 배출원 관리
      --   ON E.ID_ENG_CEF = C.ID_ENG_CEF
      LEFT OUTER JOIN T_SITE_INFO S -- 사업장과 부서
        ON E.ID_SITE = S.ID_SITE
			-- and S.ID_SITE is not null
			and S.C_DEL_YN='N'
			and E.C_DEL_YN='N'
where 1=1
		and S.ID_SITE is not null
order by E.id_site            
;

select * from T_ENG_POINT;
select * from T_SITE_INFO;
