/*
# 316.html 이동체가 아닌 항목들 전체 값입력&셀렉트박스 클릭
var _cYear = PAGE.params.cYear.kText().substr(2,2);
var _cMon = PAGE.params.cMon.kValue();
var _value = _cYear * 100 + _cMon * 100;
console.log(_value);
$(PAGE.grid.main.self.dataSource.view()).each(function(i, row){
 if ( row.sInvCd != 2 && row.sInvCd != 3 && row.sInvCd != 4 && row.sInvCd !=5 ) {
  PAGE.grid.main.find('tr').eq(i+1).find('td:first input').click()
  row.nUseAmt = _value;
  row.nCost = _value;
 }
});
*/

# select * from T_ENG_POINT_AMT where c_year = '2017' and c_mon = '07';
/*

# 배출시설 백업
DROP TABLE T_ENG_POINT_BAK_20191211;
CREATE TABLE T_ENG_POINT_BAK_20191211
SELECT * FROM T_ENG_POINT;

# 배출시설 사용량 백업
DROP TABLE T_ENG_POINT_AMT_BAK_20191211;
CREATE TABLE T_ENG_POINT_AMT_BAK_20191211
SELECT * FROM T_ENG_POINT_AMT;

# 배출원 온실가스 집계 백업
DROP TABLE T_ENG_GHG_AMT_BAK_191211;
CREATE TABLE T_ENG_GHG_AMT_BAK_191211
SELECT * FROM T_ENG_GHG_AMT;

# id로만 업데이트 될 경우 설정
SET SQL_SAFE_UPDATES = 0;

# delete from T_ENG_POINT_AMT where c_year = '2017' and c_mon = '07';
insert into T_ENG_POINT_AMT(ID_ERP_DEPT, ID_ENG_POINT, ID_SITE, C_YEAR, C_MON, N_USE_AMT, N_COST, N_ENG_ETC, N_ENG_ETC_UNT_CD, G_BILL_ID, S_REG_ID, D_REG_DATE, S_MOD_ID, D_MOD_DATE, C_DEL_YN, N_GJ, N_TCO2, S_BASE_VAL, N_TOE)
select ID_ERP_DEPT, ID_ENG_POINT, ID_SITE, C_YEAR, C_MON, N_USE_AMT, N_COST, N_ENG_ETC, N_ENG_ETC_UNT_CD, G_BILL_ID, S_REG_ID, D_REG_DATE, S_MOD_ID, D_MOD_DATE, C_DEL_YN, N_GJ, N_TCO2, S_BASE_VAL, N_TOE
from T_ENG_POINT_AMT_TEST_2019
where c_year = '2017' and c_mon = '07';


# 면적인 배출시설은 원단위값을 100m2 로 update 한다.
select *
from T_ENG_POINT
where C_DEL_YN != 'Y'
	and S_BASE_UNT = 4	-- 4:면적
    and S_BASE_VAL = 0	-- 면적=0
;
-- update T_ENG_POINT
set S_BASE_VAL = 100
where C_DEL_YN != 'Y'
	and S_BASE_UNT = 4	-- 4:면적
    and S_BASE_VAL = 0	-- 면적=0
;
    
# 창고, 이동체 = 물류(LOGI_TYPE:1) / 그 외 = 물류외(LOGI_TYPE:2)
select *
from T_ENG_POINT
where S_INV_CD not in (1, 2, 3, 10,11, 9)
;
-- update T_ENG_POINT
set S_LOGI_TYPE_CD = 1
where S_INV_CD in (1, 2, 3, 10,11, 9)
;
-- update T_ENG_POINT
set S_LOGI_TYPE_CD = 2
where S_INV_CD not in (1, 2, 3, 10,11, 9)
;




*/

set @YYYY = '2017';
set @MM = '04';
set @YYYYMM = concat(@YYYY, @MM);

/* 0. 사용실적에 기본 데이터를 insert 하도록 실행 */
call SP_LEMS_DAILY_BATCH(@YYYYMM);


/* 1. 배출시설의 S_BASE_UNT가 없으면 이후의 원단위계산이 안되므로 확인 필요 */
select S_BASE_UNT, '||', T1.* from T_ENG_POINT T1 where S_BASE_UNT is null;	# 0 row(s) returned 이어야 함.
# select * from T_CODE where S_CAT = 'INV';
# select * from T_CODE where S_CAT = 'BASE_UNT';
# S_INV_CD 가 고정체면 S_BASE_UNT는 면적	# S_INV_CD in (1,6) => S_BASE_UNT = 4
# S_INV_CD 가 이동체면 S_BASE_UNT는 tonkm	#  S_INV_CD in (2,3,10,11) => S_BASE_UNT = 1


/* 2.1. UI에서 년월을 임의로 선택하여(ex:2017년 07월) 값을 입력 */
/* 2.2. UI에서 입력한 항목들의 입력값(N_USE_AMT, N_COST)과 환산값(N_GJ, N_TCO2, N_TOE, S_BASE_VAL)이 잘 들어갔는지 확인*/
/* 2.2. T_ENG_POINT_AMT의 환산값(N_GJ, N_TCO2, N_TOE, S_BASE_VAL)과 함수로 계산한 *_CALC의 값이 같은지 확인*/
SELECT T1.C_YEAR, T1.C_MON, T2.S_POINT_NM
	, T1.N_USE_AMT, T1.N_COST
    , '||'
    , T1.N_GJ, T1.N_TCO2, T1.N_TOE, T1.S_BASE_VAL
    , '||'
    , FN_ENG_CONVERSION('GJ',T2.ID_ENG_CEF, @YYYY,T1.N_USE_AMT) as N_GJ_CALC
    , FN_ENG_CONVERSION('CO2',T2.ID_ENG_CEF, @YYYY,T1.N_USE_AMT) as N_TCO2_CALC
	, FN_ENG_CONVERSION('TOE',T2.ID_ENG_CEF, @YYYY,T1.N_USE_AMT) as N_TOE_CALC
    , CASE
		WHEN T2.S_BASE_UNT = 4	/* 1:ton.km, 4:면적 */
			THEN FN_CALC_BASE_VAL(
				FN_ENG_CONVERSION('CO2',T2.ID_ENG_CEF, @YYYY,T1.N_USE_AMT)
			, T2.S_BASE_VAL)
		ELSE
			null
	END AS S_BASE_VAL_CALC
FROM T_ENG_POINT_AMT as T1
inner join T_ENG_POINT as T2
	on T1.ID_ENG_POINT = T2.ID_ENG_POINT
where c_year = @YYYY and c_mon = @MM;	# 환산값은 엑셀을 이용하여 검증


/* 3. 사용실적의 데이터들이 분석데이터로 들어가도록 프로시저 실행 */
call SP_LEMS_DAILY_BATCH(@YYYYMM);


/* 4. 분석데이터가 정상적으로 들어갔는지 확인 */
select EP.S_POINT_NM, EP.ID_ENG_CEF
    , FN_ENG_CONVERSION('GJ',  EP.ID_ENG_CEF, DATE_FORMAT(now(), '%Y%m%d'), EPA.N_USE_AMT) as N_GJ	-- 단위 환산을 마친 데이터
	, FN_ENG_CONVERSION('TOE', EP.ID_ENG_CEF, DATE_FORMAT(now(), '%Y%m%d'), EPA.N_USE_AMT) as N_TOE
	, FN_ENG_CONVERSION('CO2', EP.ID_ENG_CEF, DATE_FORMAT(now(), '%Y%m%d'), EPA.N_USE_AMT) as N_TCO2
    , EPA.N_ENG_ETC	-- = S_BASE_VAL
    , '||'
    , EGA.N_GJ, EGA.N_TOE, EGA.N_CO2
    , EGA.S_BASE_VAL
    , EGA.N_GJ_BASE, EGA.N_TOE_BASE, EGA.N_CO2_BASE
    , '||'
    , EGA.*
FROM T_ENG_POINT as EP
INNER JOIN T_ENG_CEF as TEC	-- select * from V_ENG_CEF where id_eng_cef = 10
	ON EP.ID_ENG_CEF = TEC.ID_ENG_CEF
inner join T_ENG_POINT_AMT as EPA
	on EP.ID_ENG_POINT = EPA.ID_ENG_POINT
left outer join T_ENG_GHG_AMT as EGA
	on EPA.ID_ENG_POINT = EGA.ID_ENG_POINT
    and EPA.c_year = EGA.c_year
    and EPA.c_mon = EGA.c_mon
where EPA.c_year = @YYYY and EPA.c_mon = @MM
;	# 환산값은 엑셀을 이용하여 검증




;
# END #

############################################################################################################################################################

-- SET SQL_SAFE_UPDATES = 0;
call SP_LEMS_DAILY_BATCH('201401');call SP_LEMS_DAILY_BATCH('201402');call SP_LEMS_DAILY_BATCH('201403');call SP_LEMS_DAILY_BATCH('201404');
call SP_LEMS_DAILY_BATCH('201405');call SP_LEMS_DAILY_BATCH('201406');call SP_LEMS_DAILY_BATCH('201407');call SP_LEMS_DAILY_BATCH('201408');
call SP_LEMS_DAILY_BATCH('201409');call SP_LEMS_DAILY_BATCH('201410');call SP_LEMS_DAILY_BATCH('201411');call SP_LEMS_DAILY_BATCH('201412');

call SP_LEMS_DAILY_BATCH('201501');call SP_LEMS_DAILY_BATCH('201502');call SP_LEMS_DAILY_BATCH('201503');call SP_LEMS_DAILY_BATCH('201504');
call SP_LEMS_DAILY_BATCH('201505');call SP_LEMS_DAILY_BATCH('201506');call SP_LEMS_DAILY_BATCH('201507');call SP_LEMS_DAILY_BATCH('201508');
call SP_LEMS_DAILY_BATCH('201509');call SP_LEMS_DAILY_BATCH('201510');call SP_LEMS_DAILY_BATCH('201511');call SP_LEMS_DAILY_BATCH('201512');

call SP_LEMS_DAILY_BATCH('201601');call SP_LEMS_DAILY_BATCH('201602');call SP_LEMS_DAILY_BATCH('201603');call SP_LEMS_DAILY_BATCH('201604');
call SP_LEMS_DAILY_BATCH('201605');call SP_LEMS_DAILY_BATCH('201606');call SP_LEMS_DAILY_BATCH('201607');call SP_LEMS_DAILY_BATCH('201608');
call SP_LEMS_DAILY_BATCH('201609');call SP_LEMS_DAILY_BATCH('201610');call SP_LEMS_DAILY_BATCH('201611');call SP_LEMS_DAILY_BATCH('201612');

call SP_LEMS_DAILY_BATCH('201701');call SP_LEMS_DAILY_BATCH('201702');call SP_LEMS_DAILY_BATCH('201703');call SP_LEMS_DAILY_BATCH('201704');
call SP_LEMS_DAILY_BATCH('201705');call SP_LEMS_DAILY_BATCH('201706');call SP_LEMS_DAILY_BATCH('201707');call SP_LEMS_DAILY_BATCH('201708');
call SP_LEMS_DAILY_BATCH('201709');call SP_LEMS_DAILY_BATCH('201710');call SP_LEMS_DAILY_BATCH('201711');call SP_LEMS_DAILY_BATCH('201712');

call SP_LEMS_DAILY_BATCH('201801');call SP_LEMS_DAILY_BATCH('201802');call SP_LEMS_DAILY_BATCH('201803');call SP_LEMS_DAILY_BATCH('201804');
call SP_LEMS_DAILY_BATCH('201805');call SP_LEMS_DAILY_BATCH('201806');call SP_LEMS_DAILY_BATCH('201807');call SP_LEMS_DAILY_BATCH('201808');
call SP_LEMS_DAILY_BATCH('201809');call SP_LEMS_DAILY_BATCH('201810');call SP_LEMS_DAILY_BATCH('201811');call SP_LEMS_DAILY_BATCH('201812');

call SP_LEMS_DAILY_BATCH('201901');call SP_LEMS_DAILY_BATCH('201902');call SP_LEMS_DAILY_BATCH('201903');call SP_LEMS_DAILY_BATCH('201904');
call SP_LEMS_DAILY_BATCH('201905');call SP_LEMS_DAILY_BATCH('201906');call SP_LEMS_DAILY_BATCH('201907');call SP_LEMS_DAILY_BATCH('201908');
call SP_LEMS_DAILY_BATCH('201909');call SP_LEMS_DAILY_BATCH('201910');call SP_LEMS_DAILY_BATCH('201911');call SP_LEMS_DAILY_BATCH('201912');

############################################################################################################################################################

show processlist;

select
		FN_ENG_CONVERSION('GJ',  10, DATE_FORMAT(now(), '%Y%m%d'), 10000) as N_GJ,	-- 단위 환산을 마친 데이터
		FN_ENG_CONVERSION('TOE', 10, DATE_FORMAT(now(), '%Y%m%d'), 10000) as N_TOE,
		FN_ENG_CONVERSION('CO2', 10, DATE_FORMAT(now(), '%Y%m%d'), 10000) as N_TCO2
;

select * from T_ENG_GHG_AMT order by ID_ENG_GHG_AMT desc;


select S_UNT_CAPA, A.* FROM T_ENG_POINT as A
;
SELECT FN_ENG_CONVERSION('CO2',ID_ENG_CEF, @YYYY,10000) as N_TCO2, 
	FN_ENG_CONVERSION('GJ',ID_ENG_CEF, @YYYY,10000) as N_GJ,
	FN_ENG_CONVERSION('TOE',ID_ENG_CEF, @YYYY,10000) as N_TOE,
    (select S_DESC FROM T_CODE where S_CAT = 'BASE_UNT' and S_CD = T1.S_BASE_UNT),
    case
		when T1.S_BASE_UNT = 4	-- 1:ton.km, 4:면적
			then FN_CALC_BASE_VAL(4.662504, T1.S_BASE_VAL)	-- 원단위 기준 값(면적값 only)
        else null	-- ton.km, 대수, 면적(m2),...
	end as S_BASE_VAL
			
FROM T_ENG_POINT as T1
where id_eng_point in (
	select id_eng_point from T_ENG_POINT_AMT where c_year = @YYYY and c_mon = @MM
);

select * FROM T_ENG_POINT as T1
where id_eng_point in (
	select id_eng_point from T_ENG_POINT_AMT where c_year = @YYYY and c_mon = @MM
)
;
select *from T_ENG_POINT_AMT where c_year = @YYYY and c_mon = @MM ;