/*

V_MON_SITE_ENG_CEF_AMT

T_SITE_INFO     -- 사업장
T_MON_ENG_CLOSE -- 월마감 
T_ENG_POINT     -- 배출시설
T_ENG_POINT_AMT -- 에너지사용량 실적관리
T_ENG_CEF       -- 배출원관리
T_CODE

select * from T_MON_ENG_CLOSE; -- 월마감 
select * from T_SITE_INFO    ; -- 사업장
select * from T_ENG_POINT    ; -- 배출시설
select * from T_ENG_POINT_AMT; -- 에너지사용량 실적관리
select * from T_ENG_CEF      ; -- 배출원관리
select * from T_CODE         ;

*/

-- alter 
-- create
-- VIEW V_MON_SITE_ENG_CEF_AMT AS

SELECT
	T1.ID_SITE, T1.S_SITE_ID, T1.N_ORDER, T1.S_SITE_NM
	, T1.C_YEAR, T1.C_MON
	, '' as S_POINT_ID, '' as S_POINT_NM
	, T1.S_CEF_CD, T1.CEF_NM, T1.S_EMT_CD, T1.ENT_NM, T1.S_ENG_CD, T1.ENG_NM
	, T1.S_IN_UNT_CD, T1.S_IN_UNT_NM
	, SUM(T1.N_USE_AMT) as N_USE_AMT
    , SUM(T1.N_COST) as N_COST
FROM (
	SELECT
	  SI.ID_SITE  -- 사업장ID
	  , SI.S_SITE_ID -- 사업장ID
	  , SI.N_ORDER   -- 정렬순서
	  , SI.S_SITE_NM -- 사업장명
	  
	  , YM.YYYY AS C_YEAR -- 연도
	  , YM.MM AS C_MON -- 월
	  
	  -- T_ENG_POINT EP 배출시설
	  , EP.S_POINT_ID -- 배출시설ID.S_SITE_ID(8)+SUB SEQ(6)
	  , EP.S_POINT_NM -- 배출시설명
	  
	  -- T_ENG_CEF 배출원관리
	  , EC.S_CEF_CD -- 배출구분, T_CODE에 CEF 참조.1	직접배출2	간접배출3	기타배출
	  , (SELECT S_DESC from T_CODE where S_CAT='CEF' and S_CD=EC.S_CEF_CD) CEF_NM -- 배출구분 한글명
	  , EC.S_EMT_CD -- 배출원 구분, T_CODE에 EMT 참조.1=고정.2=이동.3=간접-전기.4=간접-스팀.5=간접-온수.6=공정.9=기타.
	  , (SELECT S_DESC from T_CODE where S_CAT='EMT' and S_CD=EC.S_EMT_CD) ENT_NM -- 배출원구분 한글명
	  , EC.S_ENG_CD -- 사용 에너지원, T_CODE에 ENG 참조.1=경유(ℓ).2=휘발유(ℓ).3=LNG_도시가스(㎥).4=LPG_프로판(Kg).5=LPG_프로판(N㎥).6=전기(Kwh).7=스팀(톤).8=LPG_부탄(Kg).9=LPG_부탄(ℓ).10=등유(ℓ).11=온수(Gcal).
	  , (SELECT S_DESC from T_CODE where S_CAT='ENG' and S_CD=EC.S_ENG_CD) ENG_NM -- 사용에너지원 한글명
	  , EC.S_IN_UNT_CD -- 단위.입력단위, T_CODE에 UNT 참조
	  , (SELECT S_DESC from T_CODE where S_CAT='UNT' and S_CD=EC.S_IN_UNT_CD) S_IN_UNT_NM -- 입력단위 표기
	  
	  -- T_ENG_POINT_AMT 에너지사용량 실적관리
	  , ifnull(EPA.N_USE_AMT,0)  N_USE_AMT -- 사용량
	  , ifnull(EPA.N_COST,0) N_COST -- 사용금액
	FROM T_SITE_INFO  SI   -- 사업장
	CROSS JOIN V_YEAR_MONTH AS YM
	LEFT JOIN T_MON_ENG_CLOSE MEC -- 월마감 
		ON SI.ID_SITE = MEC.ID_SITE
		AND YM.YYYY = MEC.C_YEAR
		AND YM.MM = MEC.C_MON
        AND MEC.C_CLOSE_YN='Y'
	LEFT JOIN T_ENG_POINT EP    -- 배출시설
		ON SI.ID_SITE = EP.ID_SITE
			and EP.C_DEL_YN='N'
	LEFT JOIN T_ENG_POINT_AMT EPA -- 에너지사용량 실적관리
		ON EPA.ID_ENG_POINT = EP.ID_ENG_POINT 
			-- and EP.C_DEL_YN='N'
			and YM.YYYY = EPA.C_YEAR
			and YM.MM = EPA.C_MON
	LEFT JOIN T_ENG_CEF EC      -- 배출원관리
		ON EP.ID_ENG_CEF = EC.ID_ENG_CEF
			and EC.C_DEL_YN='N'
	where SI.C_DEL_YN<>'Y' 
) as T1
-- where T1.ID_SITE=2 and T1.C_YEAR=2019 and T1.C_MON=12
	GROUP BY
		T1.ID_SITE, T1.S_SITE_ID, T1.N_ORDER, T1.S_SITE_NM
        , T1.C_YEAR, T1.C_MON
        -- , T1.S_POINT_ID, T1.S_POINT_NM
        , T1.S_CEF_CD, T1.CEF_NM, T1.S_EMT_CD, T1.ENT_NM, T1.S_ENG_CD, T1.ENG_NM
		, T1.S_IN_UNT_CD, T1.S_IN_UNT_NM
        -- , T1.N_USE_AMT, T1.N_COST
	ORDER BY T1.ID_SITE, T1.C_YEAR, T1.C_MON, T1.S_ENG_CD, T1.S_IN_UNT_CD    
;

-- sample
select * from V_MON_SITE_ENG_CEF_AMT where ID_SITE=2 and C_YEAR=2019 and C_MON=12;
select * from V_MON_SITE_ENG_CEF_AMT where C_YEAR=2019 and C_MON=11;