/*

V_VEHICLE

T_VEHICLE
T_TRANS_COM
T_SITE_INFO
T_GAS_DEPT_MAPPING
T_CODE
"컬럼:
차량ID,운송수단,차량번호,노선구분,운수회사,톤수,자차여부,공인연비"
*/

-- create
-- alter
-- view V_VEHICLE as
select 
  V.ID_VEHICLE -- 차량ID
  ,1 TRANS_TYPE -- 운송수단: 1=화물차,2=선박,3=철도
  -- ,'화물차' TRANS_TYPE_NM  -- 운송수단 명
  ,(select S_DESC from T_CODE where S_CAT='TRANS_TYPE' and S_CD=1) TRANS_TYPE_NM  -- 운송수단 명
  ,V.S_VRN -- 차량번호
  ,V.S_LINE_CD -- 노선구분 코드
  ,(select S_DESC from T_CODE where S_CAT='LOGI_YCT' and S_CD=V.S_LINE_CD) LOGI_YCT_NM  -- 노선구분 한글명
  ,V.S_LOGI_WEI_CD -- 톤수 코드
  ,(select S_DESC from T_CODE where S_CAT='LOGI_WEI' and S_CD=V.S_LOGI_WEI_CD) LOGI_WEI_NM  -- 톤수 한글명
  ,V.S_CAR_TYPE -- 차종
  ,(select S_DESC from T_CODE where S_CAT='CAR_TYPE' and S_CD=V.S_CAR_TYPE) CAR_TYPE_NM  -- 차종 한글명
  ,V.S_VIN -- 차대번호
  ,V.S_FUEL_TYPE_CD -- 유종 코
  ,(select S_DESC from T_CODE where S_CAT='FUEL_TYPE' and S_CD=V.S_FUEL_TYPE_CD) FUEL_TYPE_NM  -- 유종 한글명
  ,V.S_CAR_MADE -- 연식
  ,V.S_USE_TYPE_CD -- 용도 
  ,(select S_DESC from T_CODE where S_CAT='USE_TYPE' and S_CD=V.S_USE_TYPE_CD) USE_TYPE_NM  -- 용도 한글명
  ,V.S_CAR_STATUS_CD -- 차량상태
  ,(select S_DESC from T_CODE where S_CAT='CAR_STATUS' and S_CD=V.S_CAR_STATUS_CD) CAR_STATUS_NM  -- 차량상태 한글명
  ,V.S_CAR_COMPANY -- 제조사
  ,V.S_CAR_MODEL -- 차량모델
  ,V.D_REG_DATE -- 등록일시
  ,V.D_MOD_DATE -- 수정일시
  ,V.S_CAR_LOAN_DATE -- 대차일시
  ,V.S_REG_ID -- 등록자
  ,V.S_MOD_ID -- 변경자
  ,V.C_DEL_YN VHCL_DEL_YN -- 차량 삭제여부
  ,V.S_SELFVRN_CD -- 자차여부
  ,(select S_DESC from T_CODE where S_CAT='SELFVRN' and S_CD=V.S_SELFVRN_CD) SELFVRN_NM  -- 자차여부 한글명
  ,V.S_FUEL_EFF -- 공인연비

  ,V.ID_TRANS_COM -- 운수회사ID
  -- ,'000' 운수회사ID(ERP)
  ,C.S_TRANS_COM_NM -- 운수회사명
  ,C.S_COM_STATUS_CD -- 운수회사 운영상태
  
  ,SI.ID_SITE -- 사업장ID
  ,SI.S_SITE_ID -- 사업장ID
  ,SI.S_SITE_NM -- 사업장 명
  ,SI.C_DEL_YN SITE_DEL_YN -- 사업장 삭제여부
  
  ,ED.ID_ERP_DEPT -- 부서ID 
  ,ED.S_DEPT_ID -- 부서ID(ERP)
  ,ED.S_DEPT_NM -- 부서명
  ,ED.S_U_DEPT_ID -- 상위부서ID
  ,ED.S_FULL_DEPT_NM -- 전체부서명
  ,ED.C_DEL_YN DEPT_DEL_YN -- ERP 조직 삭제여부
from 
T_VEHICLE V -- 차량 마스터
LEFT JOIN T_TRANS_COM C -- 운수사 마스터
  ON V.ID_TRANS_COM = C.ID_TRANS_COM
LEFT JOIN T_ERP_DEPT ED -- ERP 부서 조직
  ON V.ID_ERP_DEPT = ED.ID_ERP_DEPT
LEFT JOIN T_GAS_DEPT_MAPPING DM -- ERP,사업장 mapping
  ON V.ID_ERP_DEPT = DM.ID_ERP_DEPT
LEFT JOIN T_SITE_INFO SI  -- 사업장
  ON SI.ID_SITE = DM.ID_SITE
where V.C_DEL_YN='N'
  ;





select * from T_SITE_INFO;
select * from T_ERP_DEPT;
select * from T_VEHICLE;

select * from 
T_SITE_INFO SI
LEFT JOIN T_GAS_DEPT_MAPPING DM
  ON SI.ID_SITE = DM.ID_SITE
LEFT JOIN T_ERP_DEPT ED
  ON DM.ID_ERP_DEPT = ED.ID_ERP_DEPT
where SI.C_DEL_YN <> 'Y'
  
  
  ;
  select * from V_VEHICLE