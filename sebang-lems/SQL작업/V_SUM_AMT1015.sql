/*

V_SUM_AMT

연도별 실적

T_ENG_POINT     -- 배출시설
T_ENG_GHG_AMT   -- 배출원온실가스 계산(집계)
T_ENG_CEF       -- 배출원관리
T_MON_ENG_CLOSE -- 월마감 
T_SITE_INFO     -- 사업장
T_USER          -- 사용자
T_CODE

컬럼:연도,조회구분(전체,사업장,물류/외),조회단위(J,TOE,CO2),J,TOE,CO2


select * from T_CODE;
select * from T_ENG_POINT;      -- 배출시설
select * from T_ENG_CEF;        -- 배출원관리
select * from T_ENG_GHG_AMT;    -- 배출원온실가스 계산(집계)
select * from T_ENG_POINT_AMT;  -- 에너지사용량 실적관리
select * from T_MON_ENG_CLOSE;  -- 월마감

*/
  
-- 1차 작성
select 
  EP.ID_ENG_POINT -- 배출시설ID
  ,EP.ID_SITE -- 사업장ID
  ,SI.S_SITE_ID -- 사업장ID
  ,SI.S_SITE_NM -- 사업장이름
  ,EP.ID_ENG_CEF -- 배출원ID
  ,EP.S_POINT_ID -- 배출시설ID
  ,EP.S_POINT_NM -- 배출시설이름
  ,EP.C_POINT_YN -- 배출시설 사용여부
  ,EP.N_ORDER -- 정렬순서
  ,EP.S_INV_CD -- 배출시설구분
  ,EP.S_INV_SEQ -- 배출시설일련번호
  ,EP.N_CAPA -- 용량
  ,EP.S_UNT_CAPA -- 용량단위
  ,EP.N_CAPA2 -- 세부시설용량
  ,EP.N_UNT_CAPA2 -- 세부시설용량 단위
  ,EP.S_LOGI_TYPE_CD -- 물류구분,물류/물류외
  ,(SELECT S_DESC from T_CODE where S_CAT='LOGI_TYPE' and S_CD=EP.S_LOGI_TYPE_CD) LOGI_TYPE_NM -- 물류구분 한글명,물류/물류외
  ,EP.S_REG_ID -- 등록자
  ,EP.S_BASE_VAL -- 원단위 기준값
  ,EP.S_BASE_UNT -- 원단위 기준 단위
  ,(SELECT S_DESC from T_CODE where S_CAT='UNT' and S_CD=EP.S_BASE_UNT) BASE_UNT_NM -- 원단위 기준 단위 한글명
  ,EP.S_FUEL_EFF -- 공인연비
  ,EP.C_DEL_YN EP_DEL_YN -- 삭제제여부
  ,EC.S_CEF_CD -- 배출구분. T_CODE CEF 참고
  ,(SELECT S_DESC from T_CODE where S_CAT='CEF' and S_CD=EC.S_CEF_CD) CEF_NM -- 배출구분 한글명
  ,EC.S_ENG_CD -- 사용에너지원
  ,(SELECT S_DESC from T_CODE where S_CAT='ENG' and S_CD=EC.S_ENG_CD) ENG_NM -- 사용에너지원 한글명
  ,EC.S_MRVENG_CD -- 정부보고 에너지원
  ,(SELECT S_DESC from T_CODE where S_CAT='MRVENG' and S_CD=EC.S_MRVENG_CD) MRVENG_NM -- 정부보고 에너지원 한글명
  ,EC.S_EMT_CD -- 배출원구분
  ,(SELECT S_DESC from T_CODE where S_CAT='EMT' and S_CD=EC.S_EMT_CD) ENT_NM -- 배출원구분 한글명
  ,EC.S_IN_UNT_CD -- 입력단위
  ,(SELECT S_DESC from T_CODE where S_CAT='UNT' and S_CD=EC.S_IN_UNT_CD) S_IN_UNT_NM -- 입력단위 표기
  ,EC.S_CALC_UNT_CD -- 계산단위
  ,(SELECT S_DESC from T_CODE where S_CAT='UNT' and S_CD=EC.S_CALC_UNT_CD) S_CALC_UNT_NM -- 계산단위 표기
  -- ,EGA.ID_ENG_POINT -- 배출시설ID
  ,EGA.ID_ENG_GHG_AMT -- 배출원온실가스 계산(집계)ID
  ,EGA.C_YEAR -- 연도
  ,EGA.C_MON -- 월
  ,EGA.N_ENG -- 에너지사용량_고유단위
  ,(SELECT S_DESC from T_CODE where S_CAT='UNT' and S_CD=EGA.N_ENG) ENG_UNT_NM -- 에너지사용량_고유단위 표기
  ,EGA.N_GJ -- 에너지사용량_TJ
  ,EGA.N_TOE -- 에너지사용량_TOE
  ,EGA.N_CO2 -- 온실가스배출량_CO2
  ,EGA.N_CH4 -- 온실가스배출량_CH4
  ,EGA.N_N2O -- 온실가스배출량_N2O
  ,EGA.N_TCO2EQ -- 온실가스배출량_tCO2eq
from
  T_ENG_POINT EP -- 배출시설
  LEFT JOIN T_ENG_CEF EC -- 배출원관리
    ON EP.ID_ENG_CEF = EC.ID_ENG_CEF
  LEFT JOIN T_ENG_GHG_AMT EGA -- 배출원온실가스 계산(집계)
    ON EP.ID_ENG_POINT = EGA.ID_ENG_POINT
  LEFT JOIN T_MON_ENG_CLOSE MC -- 월마감
    ON MC.ID_SITE = EP.ID_SITE and MC.C_CLOSE_YN='Y'  -- 마감여부
  LEFT JOIN T_SITE_INFO SI
    ON EP.ID_SITE = SI.ID_SITE
  LEFT JOIN T_USERS U
    ON EP.S_REG_ID = U.S_REG_ID;

-- sample
select * from V_SUM_AMT where C_YEAR in (2018,2019) and S_LOGI_TYPE_CD=1; -- 물류에너지

-- ==================================

-- 테스트

select ID_ENG_POINT, C_YEAR, 
      max(M1_GJ) M1_GJ, max(M1_CO2) M1_CO2, max(M1_CH4) M1_CH4, max(M1_N2O) M1_N2O, max(M1_TCO2EQ) M1_TCO2EQ
      ,max(M2_GJ) M2_GJ, max(M2_CO2) M2_CO2, max(M2_CH4) M2_CH4, max(M2_N2O) M2_N2O, max(M2_TCO2EQ) M2_TCO2EQ
      ,max(M3_GJ) M3_GJ, max(M3_CO2) M3_CO2, max(M3_CH4) M3_CH4, max(M3_N2O) M3_N2O, max(M3_TCO2EQ) M3_TCO2EQ
  from (
    select 
      ID_ENG_POINT,
      C_YEAR,
    
      case when (C_MON=1) then N_GJ else null end     as M1_GJ,
      case when (C_MON=1) then N_CO2 else null end    as M1_CO2,
      case when (C_MON=1) then N_CH4 else null end    as M1_CH4,
      case when (C_MON=1) then N_N2O else null end    as M1_N2O,
      case when (C_MON=1) then N_TCO2EQ else null end as M1_TCO2EQ,
      
      case when (C_MON=2) then N_GJ else null end as     M2_GJ,
      case when (C_MON=2) then N_CO2 else null end as    M2_CO2,
      case when (C_MON=2) then N_CH4 else null end as    M2_CH4,
      case when (C_MON=2) then N_N2O else null end as    M2_N2O,
      case when (C_MON=2) then N_TCO2EQ else null end as M2_TCO2EQ,


      case when (C_MON=3) then N_GJ else null end as     M3_GJ,
      case when (C_MON=3) then N_CO2 else null end as    M3_CO2,
      case when (C_MON=3) then N_CH4 else null end as    M3_CH4,
      case when (C_MON=3) then N_N2O else null end as    M3_N2O,
      case when (C_MON=3) then N_TCO2EQ else null end as M3_TCO2EQ

    from T_ENG_GHG_AMT
    where 1=1 
      -- ID_ENG_POINT=T.ID_ENG_POINT 
      and C_YEAR = '2019'
  ) as T2
  group by C_YEAR
  ;
  
-- ----------------------------------------------------
-- 2차. 피봇 적용 작성
select 
  EP.ID_ENG_POINT -- 배출시설ID
  ,EP.ID_SITE -- 사업장ID
  ,SI.S_SITE_ID -- 사업장ID
  ,SI.S_SITE_NM -- 사업장이름
  ,EP.ID_ENG_CEF -- 배출원ID
  ,EP.S_POINT_ID -- 배출시설ID
  ,EP.S_POINT_NM -- 배출시설이름
  ,EP.C_POINT_YN -- 배출시설 사용여부
  ,EP.N_ORDER -- 정렬순서
  ,EP.S_INV_CD -- 배출시설구분
  ,EP.S_INV_SEQ -- 배출시설일련번호
  ,EP.N_CAPA -- 용량
  ,EP.S_UNT_CAPA -- 용량단위
  ,EP.N_CAPA2 -- 세부시설용량
  ,EP.N_UNT_CAPA2 -- 세부시설용량 단위
  ,EP.S_LOGI_TYPE_CD -- 물류구분,물류/물류외
  ,(SELECT S_DESC from T_CODE where S_CAT='LOGI_TYPE' and S_CD=EP.S_LOGI_TYPE_CD) LOGI_TYPE_NM -- 물류구분 한글명,물류/물류외
  ,EP.S_REG_ID -- 등록자
  ,EP.S_BASE_VAL -- 원단위 기준값
  ,EP.S_BASE_UNT -- 원단위 기준 단위
  ,(SELECT S_DESC from T_CODE where S_CAT='UNT' and S_CD=EP.S_BASE_UNT) BASE_UNT_NM -- 원단위 기준 단위 한글명
  ,EP.S_FUEL_EFF -- 공인연비
  ,EP.C_DEL_YN EP_DEL_YN -- 삭제제여부
  ,EC.S_CEF_CD -- 배출구분. T_CODE CEF 참고
  ,(SELECT S_DESC from T_CODE where S_CAT='CEF' and S_CD=EC.S_CEF_CD) CEF_NM -- 배출구분 한글명
  ,EC.S_ENG_CD -- 사용에너지원
  ,(SELECT S_DESC from T_CODE where S_CAT='ENG' and S_CD=EC.S_ENG_CD) ENG_NM -- 사용에너지원 한글명
  ,EC.S_MRVENG_CD -- 정부보고 에너지원
  ,(SELECT S_DESC from T_CODE where S_CAT='MRVENG' and S_CD=EC.S_MRVENG_CD) MRVENG_NM -- 정부보고 에너지원 한글명
  ,EC.S_EMT_CD -- 배출원구분
  ,(SELECT S_DESC from T_CODE where S_CAT='EMT' and S_CD=EC.S_EMT_CD) ENT_NM -- 배출원구분 한글명
  ,EC.S_IN_UNT_CD -- 입력단위
  ,(SELECT S_DESC from T_CODE where S_CAT='UNT' and S_CD=EC.S_IN_UNT_CD) S_IN_UNT_NM -- 입력단위 표기
  ,EC.S_CALC_UNT_CD -- 계산단위
  ,(SELECT S_DESC from T_CODE where S_CAT='UNT' and S_CD=EC.S_CALC_UNT_CD) S_CALC_UNT_NM -- 계산단위 표기
  ,T.C_YEAR -- 연도
  ,T.M1_GJ,   T.M1_TOE,  T.M1_CO2, T.M1_CH4, T.M1_N2O, T.M1_TCO2EQ -- 1월
  ,T.M2_GJ,   T.M2_TOE,  T.M2_CO2, T.M2_CH4, T.M2_N2O, T.M2_TCO2EQ -- 2월
  ,T.M3_GJ,   T.M3_TOE,  T.M3_CO2, T.M3_CH4, T.M3_N2O, T.M3_TCO2EQ -- 3월
  ,T.M4_GJ,   T.M4_TOE,  T.M4_CO2, T.M4_CH4, T.M4_N2O, T.M4_TCO2EQ -- 4월
  ,T.M5_GJ,   T.M5_TOE,  T.M5_CO2, T.M5_CH4, T.M5_N2O, T.M5_TCO2EQ -- 5월
  ,T.M6_GJ,   T.M6_TOE,  T.M6_CO2, T.M6_CH4, T.M6_N2O, T.M6_TCO2EQ -- 6월
  ,T.M7_GJ,   T.M7_TOE,  T.M7_CO2, T.M7_CH4, T.M7_N2O, T.M7_TCO2EQ -- 7월
  ,T.M8_GJ,   T.M8_TOE,  T.M8_CO2, T.M8_CH4, T.M8_N2O, T.M8_TCO2EQ -- 8월
  ,T.M9_GJ,   T.M9_TOE,  T.M9_CO2, T.M9_CH4, T.M9_N2O, T.M9_TCO2EQ -- 9월
  ,T.M10_GJ, T.M10_TOE, T.M10_CO2, T.M10_CH4, T.M10_N2O, T.M10_TCO2EQ -- 10월
  ,T.M11_GJ, T.M11_TOE, T.M11_CO2, T.M11_CH4, T.M11_N2O, T.M11_TCO2EQ -- 11월
  ,T.M12_GJ, T.M12_TOE, T.M12_CO2, T.M12_CH4, T.M12_N2O, T.M12_TCO2EQ -- 2월
from
  T_ENG_POINT EP -- 배출시설
  LEFT JOIN T_ENG_CEF EC -- 배출원관리
    ON EP.ID_ENG_CEF = EC.ID_ENG_CEF
    AND EC.C_DEL_YN != 'Y'
  -- LEFT JOIN T_ENG_GHG_AMT EGA -- 배출원온실가스 계산(집계)
  --  ON EP.ID_ENG_POINT = EGA.ID_ENG_POINT
  -- LEFT JOIN T_MON_ENG_CLOSE MC -- 월마감
  --  ON MC.ID_SITE = EP.ID_SITE and MC.C_CLOSE_YN='Y'  -- 마감여부
  LEFT JOIN T_SITE_INFO SI
    ON EP.ID_SITE = SI.ID_SITE
		and SI.C_DEL_YN!='Y'
  LEFT JOIN T_USERS U	-- select * from T_USERS
    ON EP.S_REG_ID = U.S_REG_ID
    AND U.C_DEL_YN != 'Y'
  LEFT JOIN (
      select ID_ENG_POINT, C_YEAR, 
            max(M1_GJ) M1_GJ,    max(M1_TOE)   M1_TOE,  max(M1_CO2)  M1_CO2,  max(M1_CH4) M1_CH4, max(M1_N2O) M1_N2O, max(M1_TCO2EQ) M1_TCO2EQ
            ,max(M2_GJ) M2_GJ,   max(M2_TOE)   M2_TOE,  max(M2_CO2)  M2_CO2,  max(M2_CH4) M2_CH4, max(M2_N2O) M2_N2O, max(M2_TCO2EQ) M2_TCO2EQ
            ,max(M3_GJ) M3_GJ,   max(M3_TOE)   M3_TOE,  max(M3_CO2)  M3_CO2,  max(M3_CH4) M3_CH4, max(M3_N2O) M3_N2O, max(M3_TCO2EQ) M3_TCO2EQ
            ,max(M4_GJ) M4_GJ,   max(M4_TOE)   M4_TOE,  max(M4_CO2)  M4_CO2,  max(M4_CH4) M4_CH4, max(M4_N2O) M4_N2O, max(M4_TCO2EQ) M4_TCO2EQ
            ,max(M5_GJ) M5_GJ,   max(M5_TOE)   M5_TOE,  max(M5_CO2)  M5_CO2,  max(M5_CH4) M5_CH4, max(M5_N2O) M5_N2O, max(M5_TCO2EQ) M5_TCO2EQ
            ,max(M6_GJ) M6_GJ,   max(M6_TOE)   M6_TOE,  max(M6_CO2)  M6_CO2,  max(M6_CH4) M6_CH4, max(M6_N2O) M6_N2O, max(M6_TCO2EQ) M6_TCO2EQ
            ,max(M7_GJ) M7_GJ,   max(M7_TOE)   M7_TOE,  max(M7_CO2)  M7_CO2,  max(M7_CH4) M7_CH4, max(M7_N2O) M7_N2O, max(M7_TCO2EQ) M7_TCO2EQ
            ,max(M8_GJ) M8_GJ,   max(M8_TOE)   M8_TOE,  max(M8_CO2)  M8_CO2,  max(M8_CH4) M8_CH4, max(M8_N2O) M8_N2O, max(M8_TCO2EQ) M8_TCO2EQ
            ,max(M9_GJ) M9_GJ,   max(M9_TOE)   M9_TOE,  max(M9_CO2)  M9_CO2,  max(M9_CH4) M9_CH4, max(M9_N2O) M9_N2O, max(M9_TCO2EQ) M9_TCO2EQ
            ,max(M10_GJ) M10_GJ, max(M10_TOE) M10_TOE, max(M10_CO2) M10_CO2, max(M10_CH4) M10_CH4, max(M10_N2O) M10_N2O, max(M10_TCO2EQ) M10_TCO2EQ
            ,max(M11_GJ) M11_GJ, max(M11_TOE) M11_TOE, max(M11_CO2) M11_CO2, max(M11_CH4) M11_CH4, max(M11_N2O) M11_N2O, max(M11_TCO2EQ) M11_TCO2EQ
            ,max(M12_GJ) M12_GJ, max(M12_TOE) M12_TOE, max(M12_CO2) M12_CO2, max(M12_CH4) M12_CH4, max(M12_N2O) M12_N2O, max(M12_TCO2EQ) M12_TCO2EQ
        from (
          select 
            ID_ENG_POINT,
            C_YEAR,
          
            ifnull(case when (C_MON=1) then N_GJ else null end, 0)     as M1_GJ,
            ifnull(case when (C_MON=1) then N_TOE else null end, 0)    as M1_TOE,
            ifnull(case when (C_MON=1) then N_CO2 else null end, 0)    as M1_CO2,
            ifnull(case when (C_MON=1) then N_CH4 else null end, 0)    as M1_CH4,
            ifnull(case when (C_MON=1) then N_N2O else null end, 0)    as M1_N2O,
            ifnull(case when (C_MON=1) then N_TCO2EQ else null end, 0) as M1_TCO2EQ,
            
            ifnull(case when (C_MON=2) then N_GJ else null end, 0) as     M2_GJ,
            ifnull(case when (C_MON=2) then N_TOE else null end, 0)    as M2_TOE,
            ifnull(case when (C_MON=2) then N_CO2 else null end, 0) as    M2_CO2,
            ifnull(case when (C_MON=2) then N_CH4 else null end, 0) as    M2_CH4,
            ifnull(case when (C_MON=2) then N_N2O else null end, 0) as    M2_N2O,
            ifnull(case when (C_MON=2) then N_TCO2EQ else null end, 0) as M2_TCO2EQ,

            ifnull(case when (C_MON=3) then N_GJ else null end, 0) as     M3_GJ,
            ifnull(case when (C_MON=3) then N_TOE else null end, 0) as    M3_TOE,
            ifnull(case when (C_MON=3) then N_CO2 else null end, 0) as    M3_CO2,
            ifnull(case when (C_MON=3) then N_CH4 else null end, 0) as    M3_CH4,
            ifnull(case when (C_MON=3) then N_N2O else null end, 0) as    M3_N2O,
            ifnull(case when (C_MON=3) then N_TCO2EQ else null end, 0) as M3_TCO2EQ,

            ifnull(case when (C_MON=4) then N_GJ else null end, 0) as     M4_GJ,
            ifnull(case when (C_MON=4) then N_TOE else null end, 0) as    M4_TOE,
            ifnull(case when (C_MON=4) then N_CO2 else null end, 0) as    M4_CO2,
            ifnull(case when (C_MON=4) then N_CH4 else null end, 0) as    M4_CH4,
            ifnull(case when (C_MON=4) then N_N2O else null end, 0) as    M4_N2O,
            ifnull(case when (C_MON=4) then N_TCO2EQ else null end, 0) as M4_TCO2EQ,

            ifnull(case when (C_MON=5) then N_GJ else null end, 0) as     M5_GJ,
            ifnull(case when (C_MON=5) then N_TOE else null end, 0) as    M5_TOE,
            ifnull(case when (C_MON=5) then N_CO2 else null end, 0) as    M5_CO2,
            ifnull(case when (C_MON=5) then N_CH4 else null end, 0) as    M5_CH4,
            ifnull(case when (C_MON=5) then N_N2O else null end, 0) as    M5_N2O,
            ifnull(case when (C_MON=5) then N_TCO2EQ else null end, 0) as M5_TCO2EQ,

            ifnull(case when (C_MON=6) then N_GJ else null end, 0) as     M6_GJ,
            ifnull(case when (C_MON=6) then N_TOE else null end, 0) as    M6_TOE,
            ifnull(case when (C_MON=6) then N_CO2 else null end, 0) as    M6_CO2,
            ifnull(case when (C_MON=6) then N_CH4 else null end, 0) as    M6_CH4,
            ifnull(case when (C_MON=6) then N_N2O else null end, 0) as    M6_N2O,
            ifnull(case when (C_MON=6) then N_TCO2EQ else null end, 0) as M6_TCO2EQ,

            ifnull(case when (C_MON=7) then N_GJ else null end, 0) as     M7_GJ,
            ifnull(case when (C_MON=7) then N_TOE else null end, 0) as    M7_TOE,
            ifnull(case when (C_MON=7) then N_CO2 else null end, 0) as    M7_CO2,
            ifnull(case when (C_MON=7) then N_CH4 else null end, 0) as    M7_CH4,
            ifnull(case when (C_MON=7) then N_N2O else null end, 0) as    M7_N2O,
            ifnull(case when (C_MON=7) then N_TCO2EQ else null end, 0) as M7_TCO2EQ,

            ifnull(case when (C_MON=8) then N_GJ else null end, 0) as     M8_GJ,
            ifnull(case when (C_MON=8) then N_TOE else null end, 0) as    M8_TOE,
            ifnull(case when (C_MON=8) then N_CO2 else null end, 0) as    M8_CO2,
            ifnull(case when (C_MON=8) then N_CH4 else null end, 0) as    M8_CH4,
            ifnull(case when (C_MON=8) then N_N2O else null end, 0) as    M8_N2O,
            ifnull(case when (C_MON=8) then N_TCO2EQ else null end, 0) as M8_TCO2EQ,

            ifnull(case when (C_MON=9) then N_GJ else null end, 0) as     M9_GJ,
            ifnull(case when (C_MON=9) then N_TOE else null end, 0) as    M9_TOE,
            ifnull(case when (C_MON=9) then N_CO2 else null end, 0) as    M9_CO2,
            ifnull(case when (C_MON=9) then N_CH4 else null end, 0) as    M9_CH4,
            ifnull(case when (C_MON=9) then N_N2O else null end, 0) as    M9_N2O,
            ifnull(case when (C_MON=9) then N_TCO2EQ else null end, 0) as M9_TCO2EQ,

            ifnull(case when (C_MON=10) then N_GJ else null end, 0) as     M10_GJ,
            ifnull(case when (C_MON=10) then N_TOE else null end, 0) as    M10_TOE,
            ifnull(case when (C_MON=10) then N_CO2 else null end, 0) as    M10_CO2,
            ifnull(case when (C_MON=10) then N_CH4 else null end, 0) as    M10_CH4,
            ifnull(case when (C_MON=10) then N_N2O else null end, 0) as    M10_N2O,
            ifnull(case when (C_MON=10) then N_TCO2EQ else null end, 0) as M10_TCO2EQ,

            ifnull(case when (C_MON=11) then N_GJ else null end, 0) as     M11_GJ,
            ifnull(case when (C_MON=11) then N_TOE else null end, 0) as    M11_TOE,
            ifnull(case when (C_MON=11) then N_CO2 else null end, 0) as    M11_CO2,
            ifnull(case when (C_MON=11) then N_CH4 else null end, 0) as    M11_CH4,
            ifnull(case when (C_MON=11) then N_N2O else null end, 0) as    M11_N2O,
            ifnull(case when (C_MON=11) then N_TCO2EQ else null end, 0) as M11_TCO2EQ,

            ifnull(case when (C_MON=12) then N_GJ else null end, 0) as     M12_GJ,
            ifnull(case when (C_MON=12) then N_TOE else null end, 0) as    M12_TOE,
            ifnull(case when (C_MON=12) then N_CO2 else null end, 0) as    M12_CO2,
            ifnull(case when (C_MON=12) then N_CH4 else null end, 0) as    M12_CH4,
            ifnull(case when (C_MON=12) then N_N2O else null end, 0) as    M12_N2O,
            ifnull(case when (C_MON=12) then N_TCO2EQ else null end, 0) as M12_TCO2EQ

          from T_ENG_GHG_AMT
          where 1=1 
            -- ID_ENG_POINT=T.ID_ENG_POINT 
            -- and C_YEAR = '2019'
        ) T
        group by ID_ENG_POINT, C_YEAR
  ) T
  ON T.ID_ENG_POINT = EP.ID_ENG_POINT 
  where EP.C_DEL_YN != 'Y'
;

-- ----------------------------------------------------
-- 3차. JOIN절 등 일부 구문 수정
-- create 
-- alter view V_SUM_AMT as
select 
  EP.ID_ENG_POINT -- 배출시설ID
  ,EP.ID_SITE -- 사업장ID
  ,SI.S_SITE_ID -- 사업장ID
  ,SI.S_SITE_NM -- 사업장이름
  ,EP.ID_ENG_CEF -- 배출원ID
  ,EP.S_POINT_ID -- 배출시설ID
  ,EP.S_POINT_NM -- 배출시설이름
  ,EP.C_POINT_YN -- 배출시설 사용여부
  ,EP.N_ORDER -- 정렬순서
  ,EP.S_INV_CD -- 배출시설구분
  ,EP.S_INV_SEQ -- 배출시설일련번호
  ,EP.N_CAPA -- 용량
  ,EP.S_UNT_CAPA -- 용량단위
  ,EP.N_CAPA2 -- 세부시설용량
  ,EP.N_UNT_CAPA2 -- 세부시설용량 단위
  ,EP.S_LOGI_TYPE_CD -- 물류구분,물류/물류외
  ,(SELECT S_DESC from T_CODE where S_CAT='LOGI_TYPE' and S_CD=EP.S_LOGI_TYPE_CD) LOGI_TYPE_NM -- 물류구분 한글명,물류/물류외
  ,EP.S_REG_ID -- 등록자
  ,EP.S_BASE_VAL -- 원단위 기준값
  ,EP.S_BASE_UNT -- 원단위 기준 단위
  ,(SELECT S_DESC from T_CODE where S_CAT='UNT' and S_CD=EP.S_BASE_UNT) BASE_UNT_NM -- 원단위 기준 단위 한글명
  ,EP.S_FUEL_EFF -- 공인연비
  ,EP.C_DEL_YN EP_DEL_YN -- 삭제제여부
  ,EC.S_CEF_CD -- 배출구분. T_CODE CEF 참고
  ,(SELECT S_DESC from T_CODE where S_CAT='CEF' and S_CD=EC.S_CEF_CD) CEF_NM -- 배출구분 한글명
  ,EC.S_ENG_CD -- 사용에너지원
  ,(SELECT S_DESC from T_CODE where S_CAT='ENG' and S_CD=EC.S_ENG_CD) ENG_NM -- 사용에너지원 한글명
  ,EC.S_MRVENG_CD -- 정부보고 에너지원
  ,(SELECT S_DESC from T_CODE where S_CAT='MRVENG' and S_CD=EC.S_MRVENG_CD) MRVENG_NM -- 정부보고 에너지원 한글명
  ,EC.S_EMT_CD -- 배출원구분
  ,(SELECT S_DESC from T_CODE where S_CAT='EMT' and S_CD=EC.S_EMT_CD) ENT_NM -- 배출원구분 한글명
  ,EC.S_IN_UNT_CD -- 입력단위
  ,(SELECT S_DESC from T_CODE where S_CAT='UNT' and S_CD=EC.S_IN_UNT_CD) S_IN_UNT_NM -- 입력단위 표기
  ,EC.S_CALC_UNT_CD -- 계산단위
  ,(SELECT S_DESC from T_CODE where S_CAT='UNT' and S_CD=EC.S_CALC_UNT_CD) S_CALC_UNT_NM -- 계산단위 표기
  ,T.C_YEAR -- 연도
  ,T.M1_GJ,   T.M1_TOE,  T.M1_CO2, T.M1_CH4, T.M1_N2O, T.M1_TCO2EQ -- 1월
  ,T.M2_GJ,   T.M2_TOE,  T.M2_CO2, T.M2_CH4, T.M2_N2O, T.M2_TCO2EQ -- 2월
  ,T.M3_GJ,   T.M3_TOE,  T.M3_CO2, T.M3_CH4, T.M3_N2O, T.M3_TCO2EQ -- 3월
  ,T.M4_GJ,   T.M4_TOE,  T.M4_CO2, T.M4_CH4, T.M4_N2O, T.M4_TCO2EQ -- 4월
  ,T.M5_GJ,   T.M5_TOE,  T.M5_CO2, T.M5_CH4, T.M5_N2O, T.M5_TCO2EQ -- 5월
  ,T.M6_GJ,   T.M6_TOE,  T.M6_CO2, T.M6_CH4, T.M6_N2O, T.M6_TCO2EQ -- 6월
  ,T.M7_GJ,   T.M7_TOE,  T.M7_CO2, T.M7_CH4, T.M7_N2O, T.M7_TCO2EQ -- 7월
  ,T.M8_GJ,   T.M8_TOE,  T.M8_CO2, T.M8_CH4, T.M8_N2O, T.M8_TCO2EQ -- 8월
  ,T.M9_GJ,   T.M9_TOE,  T.M9_CO2, T.M9_CH4, T.M9_N2O, T.M9_TCO2EQ -- 9월
  ,T.M10_GJ, T.M10_TOE, T.M10_CO2, T.M10_CH4, T.M10_N2O, T.M10_TCO2EQ -- 10월
  ,T.M11_GJ, T.M11_TOE, T.M11_CO2, T.M11_CH4, T.M11_N2O, T.M11_TCO2EQ -- 11월
  ,T.M12_GJ, T.M12_TOE, T.M12_CO2, T.M12_CH4, T.M12_N2O, T.M12_TCO2EQ -- 2월
from
  T_ENG_POINT EP -- 배출시설
  INNER JOIN T_SITE_INFO SI
    ON EP.ID_SITE = SI.ID_SITE
		and SI.C_DEL_YN!='Y'
  LEFT JOIN T_ENG_CEF EC -- 배출원관리
    ON EP.ID_ENG_CEF = EC.ID_ENG_CEF
    AND EC.C_DEL_YN != 'Y'
  -- LEFT JOIN T_ENG_GHG_AMT EGA -- 배출원온실가스 계산(집계)
  --  ON EP.ID_ENG_POINT = EGA.ID_ENG_POINT
  -- LEFT JOIN T_MON_ENG_CLOSE MC -- 월마감
  --  ON MC.ID_SITE = EP.ID_SITE and MC.C_CLOSE_YN='Y'  -- 마감여부
  LEFT JOIN T_USERS U
    ON EP.S_REG_ID = U.S_REG_ID
    AND U.C_DEL_YN != 'Y'
  LEFT JOIN (
      select ID_ENG_POINT, C_YEAR, 
            max(M1_GJ) M1_GJ,    max(M1_TOE)   M1_TOE,  max(M1_CO2)  M1_CO2,  max(M1_CH4) M1_CH4, max(M1_N2O) M1_N2O, max(M1_TCO2EQ) M1_TCO2EQ
            ,max(M2_GJ) M2_GJ,   max(M2_TOE)   M2_TOE,  max(M2_CO2)  M2_CO2,  max(M2_CH4) M2_CH4, max(M2_N2O) M2_N2O, max(M2_TCO2EQ) M2_TCO2EQ
            ,max(M3_GJ) M3_GJ,   max(M3_TOE)   M3_TOE,  max(M3_CO2)  M3_CO2,  max(M3_CH4) M3_CH4, max(M3_N2O) M3_N2O, max(M3_TCO2EQ) M3_TCO2EQ
            ,max(M4_GJ) M4_GJ,   max(M4_TOE)   M4_TOE,  max(M4_CO2)  M4_CO2,  max(M4_CH4) M4_CH4, max(M4_N2O) M4_N2O, max(M4_TCO2EQ) M4_TCO2EQ
            ,max(M5_GJ) M5_GJ,   max(M5_TOE)   M5_TOE,  max(M5_CO2)  M5_CO2,  max(M5_CH4) M5_CH4, max(M5_N2O) M5_N2O, max(M5_TCO2EQ) M5_TCO2EQ
            ,max(M6_GJ) M6_GJ,   max(M6_TOE)   M6_TOE,  max(M6_CO2)  M6_CO2,  max(M6_CH4) M6_CH4, max(M6_N2O) M6_N2O, max(M6_TCO2EQ) M6_TCO2EQ
            ,max(M7_GJ) M7_GJ,   max(M7_TOE)   M7_TOE,  max(M7_CO2)  M7_CO2,  max(M7_CH4) M7_CH4, max(M7_N2O) M7_N2O, max(M7_TCO2EQ) M7_TCO2EQ
            ,max(M8_GJ) M8_GJ,   max(M8_TOE)   M8_TOE,  max(M8_CO2)  M8_CO2,  max(M8_CH4) M8_CH4, max(M8_N2O) M8_N2O, max(M8_TCO2EQ) M8_TCO2EQ
            ,max(M9_GJ) M9_GJ,   max(M9_TOE)   M9_TOE,  max(M9_CO2)  M9_CO2,  max(M9_CH4) M9_CH4, max(M9_N2O) M9_N2O, max(M9_TCO2EQ) M9_TCO2EQ
            ,max(M10_GJ) M10_GJ, max(M10_TOE) M10_TOE, max(M10_CO2) M10_CO2, max(M10_CH4) M10_CH4, max(M10_N2O) M10_N2O, max(M10_TCO2EQ) M10_TCO2EQ
            ,max(M11_GJ) M11_GJ, max(M11_TOE) M11_TOE, max(M11_CO2) M11_CO2, max(M11_CH4) M11_CH4, max(M11_N2O) M11_N2O, max(M11_TCO2EQ) M11_TCO2EQ
            ,max(M12_GJ) M12_GJ, max(M12_TOE) M12_TOE, max(M12_CO2) M12_CO2, max(M12_CH4) M12_CH4, max(M12_N2O) M12_N2O, max(M12_TCO2EQ) M12_TCO2EQ
        from (
          select 
            ID_ENG_POINT,
            C_YEAR,
          
            ifnull(case when (C_MON='01') then N_GJ else null end, 0)     as M1_GJ,
            ifnull(case when (C_MON='01') then N_TOE else null end, 0)    as M1_TOE,
            ifnull(case when (C_MON='01') then N_CO2 else null end, 0)    as M1_CO2,
            ifnull(case when (C_MON='01') then N_CH4 else null end, 0)    as M1_CH4,
            ifnull(case when (C_MON='01') then N_N2O else null end, 0)    as M1_N2O,
            ifnull(case when (C_MON='01') then N_TCO2EQ else null end, 0) as M1_TCO2EQ,
            
            ifnull(case when (C_MON='02') then N_GJ else null end, 0) as     M2_GJ,
            ifnull(case when (C_MON='02') then N_TOE else null end, 0)    as M2_TOE,
            ifnull(case when (C_MON='02') then N_CO2 else null end, 0) as    M2_CO2,
            ifnull(case when (C_MON='02') then N_CH4 else null end, 0) as    M2_CH4,
            ifnull(case when (C_MON='02') then N_N2O else null end, 0) as    M2_N2O,
            ifnull(case when (C_MON='02') then N_TCO2EQ else null end, 0) as M2_TCO2EQ,

            ifnull(case when (C_MON='03') then N_GJ else null end, 0) as     M3_GJ,
            ifnull(case when (C_MON='03') then N_TOE else null end, 0) as    M3_TOE,
            ifnull(case when (C_MON='03') then N_CO2 else null end, 0) as    M3_CO2,
            ifnull(case when (C_MON='03') then N_CH4 else null end, 0) as    M3_CH4,
            ifnull(case when (C_MON='03') then N_N2O else null end, 0) as    M3_N2O,
            ifnull(case when (C_MON='03') then N_TCO2EQ else null end, 0) as M3_TCO2EQ,

            ifnull(case when (C_MON='04') then N_GJ else null end, 0) as     M4_GJ,
            ifnull(case when (C_MON='04') then N_TOE else null end, 0) as    M4_TOE,
            ifnull(case when (C_MON='04') then N_CO2 else null end, 0) as    M4_CO2,
            ifnull(case when (C_MON='04') then N_CH4 else null end, 0) as    M4_CH4,
            ifnull(case when (C_MON='04') then N_N2O else null end, 0) as    M4_N2O,
            ifnull(case when (C_MON='04') then N_TCO2EQ else null end, 0) as M4_TCO2EQ,

            ifnull(case when (C_MON='05') then N_GJ else null end, 0) as     M5_GJ,
            ifnull(case when (C_MON='05') then N_TOE else null end, 0) as    M5_TOE,
            ifnull(case when (C_MON='05') then N_CO2 else null end, 0) as    M5_CO2,
            ifnull(case when (C_MON='05') then N_CH4 else null end, 0) as    M5_CH4,
            ifnull(case when (C_MON='05') then N_N2O else null end, 0) as    M5_N2O,
            ifnull(case when (C_MON='05') then N_TCO2EQ else null end, 0) as M5_TCO2EQ,

            ifnull(case when (C_MON='06') then N_GJ else null end, 0) as     M6_GJ,
            ifnull(case when (C_MON='06') then N_TOE else null end, 0) as    M6_TOE,
            ifnull(case when (C_MON='06') then N_CO2 else null end, 0) as    M6_CO2,
            ifnull(case when (C_MON='06') then N_CH4 else null end, 0) as    M6_CH4,
            ifnull(case when (C_MON='06') then N_N2O else null end, 0) as    M6_N2O,
            ifnull(case when (C_MON='06') then N_TCO2EQ else null end, 0) as M6_TCO2EQ,

            ifnull(case when (C_MON='07') then N_GJ else null end, 0) as     M7_GJ,
            ifnull(case when (C_MON='07') then N_TOE else null end, 0) as    M7_TOE,
            ifnull(case when (C_MON='07') then N_CO2 else null end, 0) as    M7_CO2,
            ifnull(case when (C_MON='07') then N_CH4 else null end, 0) as    M7_CH4,
            ifnull(case when (C_MON='07') then N_N2O else null end, 0) as    M7_N2O,
            ifnull(case when (C_MON='07') then N_TCO2EQ else null end, 0) as M7_TCO2EQ,

            ifnull(case when (C_MON='08') then N_GJ else null end, 0) as     M8_GJ,
            ifnull(case when (C_MON='08') then N_TOE else null end, 0) as    M8_TOE,
            ifnull(case when (C_MON='08') then N_CO2 else null end, 0) as    M8_CO2,
            ifnull(case when (C_MON='08') then N_CH4 else null end, 0) as    M8_CH4,
            ifnull(case when (C_MON='08') then N_N2O else null end, 0) as    M8_N2O,
            ifnull(case when (C_MON='08') then N_TCO2EQ else null end, 0) as M8_TCO2EQ,

            ifnull(case when (C_MON='09') then N_GJ else null end, 0) as     M9_GJ,
            ifnull(case when (C_MON='09') then N_TOE else null end, 0) as    M9_TOE,
            ifnull(case when (C_MON='09') then N_CO2 else null end, 0) as    M9_CO2,
            ifnull(case when (C_MON='09') then N_CH4 else null end, 0) as    M9_CH4,
            ifnull(case when (C_MON='09') then N_N2O else null end, 0) as    M9_N2O,
            ifnull(case when (C_MON='09') then N_TCO2EQ else null end, 0) as M9_TCO2EQ,

            ifnull(case when (C_MON='10') then N_GJ else null end, 0) as     M10_GJ,
            ifnull(case when (C_MON='10') then N_TOE else null end, 0) as    M10_TOE,
            ifnull(case when (C_MON='10') then N_CO2 else null end, 0) as    M10_CO2,
            ifnull(case when (C_MON='10') then N_CH4 else null end, 0) as    M10_CH4,
            ifnull(case when (C_MON='10') then N_N2O else null end, 0) as    M10_N2O,
            ifnull(case when (C_MON='10') then N_TCO2EQ else null end, 0) as M10_TCO2EQ,

            ifnull(case when (C_MON='11') then N_GJ else null end, 0) as     M11_GJ,
            ifnull(case when (C_MON='11') then N_TOE else null end, 0) as    M11_TOE,
            ifnull(case when (C_MON='11') then N_CO2 else null end, 0) as    M11_CO2,
            ifnull(case when (C_MON='11') then N_CH4 else null end, 0) as    M11_CH4,
            ifnull(case when (C_MON='11') then N_N2O else null end, 0) as    M11_N2O,
            ifnull(case when (C_MON='11') then N_TCO2EQ else null end, 0) as M11_TCO2EQ,

            ifnull(case when (C_MON='12') then N_GJ else null end, 0) as     M12_GJ,
            ifnull(case when (C_MON='12') then N_TOE else null end, 0) as    M12_TOE,
            ifnull(case when (C_MON='12') then N_CO2 else null end, 0) as    M12_CO2,
            ifnull(case when (C_MON='12') then N_CH4 else null end, 0) as    M12_CH4,
            ifnull(case when (C_MON='12') then N_N2O else null end, 0) as    M12_N2O,
            ifnull(case when (C_MON='12') then N_TCO2EQ else null end, 0) as M12_TCO2EQ

          from T_ENG_GHG_AMT
          where 1=1 
            -- ID_ENG_POINT=T.ID_ENG_POINT 
            -- and C_YEAR = '2019'
        ) T
        group by ID_ENG_POINT, C_YEAR
  ) T
  ON T.ID_ENG_POINT = EP.ID_ENG_POINT 
WHERE EP.C_DEL_YN != 'Y' 
	and EP.S_INV_CD != 11	-- 배출 시설 11=화물차(용차)
;

select * from T_CODE where S_CAT='INV';


    
select N_GJ, N_TOE, N_CO2, N_CH4, N_N2O, N_TCO2EQ from T_ENG_GHG_AMT where ID_ENG_POINT=1 and C_YEAR=2019 and C_MON=1;

SELECT * FROM T_ENG_GHG_AMT;

--  배출시설ID YEAR M1_A M1_B M2_A ...
--  '0001' '2019' 1 null null
--  '0001' '2019' null 2 null
--  '0001' '2019' null null 3
--  ----
--  '0001' '2019' 1 2 3

-- 예제:
select * from V_SUM_AMT;
select * from V_SUM_AMT where C_YEAR='2019';
select * from V_SUM_AMT where C_YEAR in (2018,2019) and S_LOGI_TYPE_CD=1; -- 물류에너지
select * from T_SCHEDULER_LOG;

call SP_LEMS_DAILY_BATCH(null);
