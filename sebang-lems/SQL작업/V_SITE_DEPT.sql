/*

  V_SITE_INFO
  
  화면 : 

  2019/11/8 by richard

T_SITE_INFO -- 사업장
T_GAS_DEPT_MAPPING -- 온실가스 관리용 조직도
T_ERP_DEPT -- ERP조직도

*/
select * from T_SITE_INFO; -- 사업장
select * from T_GAS_DEPT_MAPPING; -- 온실가스 관리용 조직도
select * from T_ERP_DEPT; -- ERP조직도

select * from V_SITE_DEPT;

create 
-- alter 
view V_SITE_DEPT as
select 
  S.ID_SITE -- ID,SEQ.AI
  ,S.S_SITE_ID -- 사업장ID,'S'+9(7).S0000003,S+ID_SITE(7)
  ,S.N_ORDER -- 정렬순서
  ,S.S_SITE_NM -- 사업장명
  -- ,S.S_SITE_REG_NUM -- 사업자등록번호
  -- ,S.S_SITE_ADDR -- 사업장소재지
  -- ,S.S_OWNER -- 대표자명
  -- ,S.S_SITE_CAT -- 업종
  -- ,S.S_SITE_TEL -- 사업장대표전화
  ,S.S_SITE_PARTY_CD -- 사업장구분(공장, 사무실, 창고 등)
  ,S.C_CLOSE_YEAR -- 폐쇄연도
  -- ,S.S_PIC_NM -- 담당자이름
  
  ,D.ID_ERP_DEPT -- ID,SEQ.AI
  ,D.S_DEPT_ID -- 부서ID.ERP에서 생성
  ,D.S_DEPT_NM -- 부서명
  ,D.S_U_DEPT_ID -- 상위부서ID
  ,D.S_FULL_DEPT_NM -- 전체부서명
  
from 
  T_SITE_INFO S
  LEFT JOIN  T_GAS_DEPT_MAPPING M
    ON S.ID_SITE = M.ID_SITE
  LEFT JOIN T_ERP_DEPT D
    ON M.ID_ERP_DEPT = D.ID_ERP_DEPT;
    
    