# SET SQL_SAFE_UPDATES = 0;		# Error Code: 1175 의 경우 설정
call SP_DO_CLOSE_MONTH('201801', @result);
call SP_DO_CLOSE_MONTH(null, @result);
select @result;

select * from T_SCHEDULER_LOG order by SEQ DESC;


DELIMITER $$
drop procedure SP_DO_CLOSE_MONTH;
create procedure SP_DO_CLOSE_MONTH(
	IN IN_YYYYMM VARCHAR(255),
	OUT OUT_RESULT INT(11)		# 1:성공, -1:실패
) 
BEGIN
	/*
	Created at 2019/12/18 By kdh

	@DESCRIPTION
		마감시 년월을 입력받아 월운송정보를 제외한 이하 집계테이블을 최종 계산하여 적재한다.
		# 월 운송정보 - 효율화지표 생성
		- T_LOGIS_TRANS_MON_EFF_INFO
		# 월 실적정보생성 (화물차,철송,해송): 온실가스 에너지/ 에너지 실적등록 / 사용실적
		- T_ENG_POINT_AMT
		# 배출시설별 원단위 생성
		- T_ENG_GHG_AMT
		# 온실가스/에너지현황 data 생성
		- T_LOGIS_TRANS_YEAR
      
	@PARAM
		IN_YYYYMM: 처리하고자 하는 YYYYMM 값. null 인 경우 default 자동으로 오늘을 기준으로 동작함
        
	@RETURN
		OUT_RESULT: -1:실패, 1:성공
      
	*/
  
	set @YYYYMMOfYesterday=IN_YYYYMM;
	if IN_YYYYMM is null
	then
		/* IN_YYYYMM이 null일 경우 이하 SP 실행불가 */
		set OUT_RESULT = -1;
        select OUT_RESULT;
        
	else
		/* START: IN_YYYYMM이 존재할 경우 이하 SP 실행시작 */

		set @YYYYOfYesterday=substring(@YYYYMMOfYesterday,1,4);
		set @MMOfYesterday=substring(@YYYYMMOfYesterday,5,2);
	  
	  
		-- SP 시작시간 저장
		SET @SP_START_SEC = TIME_TO_SEC(now());


		/*
		월 운송정보 생성: 운송정보
		- 마감에 의해서 실행되는 SP_DO_CLOSE_MONTH에서는 월운송정보를 새로 갱신하지 않는다
		- 사용자 입력에 의해 변경되었을 수도 있는 T_MAKE_LOGIS_TRANS_MON_INFO의 데이터가 T_MAKE_LOGIS_TRANS_INFO 기준으로 다시 update 될 수 있기 때문이다.
		*/
		#call SP_MAKE_LOGIS_TRANS_MON_INFO(@YYYYMMOfYesterday); # call SP_MAKE_LOGIS_TRANS_MON_INFO('201802');
	  
	  
		/*
		월 운송정보 - 효율화지표 생성: T_LOGIS_TRANS_MON_EFF_INFO
		*/
		call SP_MAKE_LOGIS_TRANS_MON_EFF_INFO(@YYYYMMOfYesterday); # call SP_MAKE_LOGIS_TRANS_MON_EFF_INFO('201802');

		/*
		월 실적정보생성 (화물차,철송,해송): 온실가스 에너지/ 에너지 실적등록 / 사용실적
		*/
		call SP_MAKE_ENG_POINT_AMT(@YYYYOfYesterday,@MMOfYesterday); # call SP_MAKE_ENG_POINT_AMT('2018', '02');
	  
	  
		/*
		배출시설별 원단위 생성
		*/
		call SP_MAKE_ENG_GHG_AMT(@YYYYOfYesterday,@MMOfYesterday); # call SP_MAKE_ENG_GHG_AMT('2018', '02');
	  
	  
		/*
		온실가스/에너지현황 data 생성
		*/
		call SP_MAKE_LOGIS_TRANS_YEAR(@YYYYMMOfYesterday); # call SP_MAKE_LOGIS_TRANS_YEAR('201802');
	  
	  
		/*
		마감에 따른 SP실행이 끝나면
		월마감 테이블의 G:마감중 코드를 Y:마감완료 로 변경해준다.
		*/
		update T_MON_ENG_CLOSE
		set C_CLOSE_YN = 'Y'
		where C_YEAR = @YYYYOfYesterday
			and C_MON = @MMOfYesterday;
	
		set OUT_RESULT = 1;	 # 1:성공
        
		/* END: IN_YYYYMM이 존재할 경우 이하 SP 실행종료 */
	end if;


	-- SP 종료시간 저장
	SET @SP_END_SEC = TIME_TO_SEC(now());
	-- logging
	INSERT INTO T_SCHEDULER_LOG (SP_NM) VALUES ( concat('SP_DO_CLOSE_MONTH:',@YYYYMMOfYesterday, ', Result: ', OUT_RESULT, ', Duration: ', @SP_END_SEC-@SP_START_SEC, ' s') );
    
    select OUT_RESULT;
  
END $$
DELIMITER ;