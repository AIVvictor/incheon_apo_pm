/*

V_LOGIS_TRANS_MON / 온실가스 에너지 현황

화면: 물류에너지 관리 / 온실가스 에너지 / 온실가스 에너지 현황

select * from T_CODE                 ;
select * from T_LOGIS_TRANS_MON_INFO ; -- 월운송정보
select * from T_SITE_INFO            ; -- 사업장
select * from T_VEHICLE              ; -- 차량 마스터
select * from V_VEHICLE              ; -- 차량 마스터
select * from T_ERP_DEPT             ; -- ERP 조직도

select * from T_LOGIS_TRANS_INFO     ; -- 운송정보


*/

-- 차량 온실가스 에너지 현황
-- create 
-- alter 
-- view V_LOGIS_TRANS_MON_VHCL as
select
  TMI.TRANS_TYPE_CD -- 운송수단
  ,(SELECT S_DESC FROM T_CODE WHERE S_CAT='TRANS_TYPE' and S_CD=TMI.TRANS_TYPE_CD ) TRANS_TYPE_NM -- 운송수단 한글명
  ,V.S_LINE_CD -- 노선구분
  ,V.LOGI_YCT_NM -- 노선구분 한글명
  ,V.S_LOGI_WEI_CD -- 톤수
  ,V.LOGI_WEI_NM -- 톤수 한글명
  ,V.S_CAR_TYPE -- 차종
  ,(SELECT S_DESC FROM T_CODE WHERE S_CAT='CAR_TYPE' and S_CD=V.S_CAR_TYPE ) CAR_TYPE_NM -- 차종 한글명
  ,V.ID_SITE -- 사업장ID
  ,V.S_SITE_NM -- 사업장명
  ,sum(TMI.LOADAGE_SUM) LOADAGE_SUM -- 적재량 합
  ,sum(TMI.FARE_SUM) FARE_SUM -- 운임료 합
  ,sum(TMI.VACANT_DISTANCE_SUM) VACANT_DISTANCE_SUM -- 공차운행거리 합
  ,sum(TMI.DISTANCE_SUM) DISTANCE_SUM -- 영차운행거리
  ,sum(TMI.MJ_SUM) MJ_SUM -- 에너지 사용량(MJ) 합
  ,sum(TMI.GCO2_SUM) GCO2_SUM -- 온실가스배출량(gCO2) 합
  ,sum(TMI.ENG_VOL_SUM) ENG_VOL_SUM -- 연료사용량 합
  ,sum(TMI.TOE_SUM) TOE_SUM -- 환상톤 합
  ,sum(TMI.TON_KM_SUM) TON_KM_SUM -- 톤킬로 합
  ,TMI.DATE_MON -- 날짜,YYYYMM
  ,substring(TMI.DATE_MON,1,4) YEAR -- 연
from 
  T_LOGIS_TRANS_MON_INFO TMI  -- 월운송정보
  LEFT JOIN V_VEHICLE V       -- 차량 마스터
    ON TMI.ID_VEHICLE = V.ID_VEHICLE -- 차량ID
  -- LEFT JOIN T_SITE_INFO SI    -- 사업장
  --  ON SI.ID_SITE=V.ID_SITE   -- 사업장ID
  where TMI.TRANS_TYPE_CD=1   -- 화물차
  GROUP BY
    V.TRANS_TYPE -- 운송수단
    ,V.TRANS_TYPE_NM -- 운송수단 한글명
    ,V.S_LINE_CD -- 노선구분
    ,V.LOGI_YCT_NM -- 노선구분 한글명
    ,V.S_LOGI_WEI_CD -- 톤수
    ,V.LOGI_WEI_NM -- 톤수 한글명
    ,V.ID_SITE -- 사업장ID
    ,V.S_SITE_NM -- 사업장명
    ,TMI.DATE_MON -- 날짜,YYYYMM
  ;

-- 철송 온실가스 에너지 현황
-- create 
-- alter 
-- view V_LOGIS_TRANS_MON_TRAIN as
select
  TMI.TRANS_TYPE_CD -- 운송수단
  ,(SELECT S_DESC FROM T_CODE WHERE S_CAT='TRANS_TYPE' and S_CD=TMI.TRANS_TYPE_CD ) TRANS_TYPE_NM -- 운송수단 한글명
  ,null S_LINE_CD -- 노선구분
  ,null LOGI_YCT_NM -- 노선구분 한글명
  ,null S_LOGI_WEI_CD -- 톤수
  ,null LOGI_WEI_NM -- 톤수 한글명
  ,null S_CAR_TYPE -- 차종
  ,null CAR_TYPE_NM -- 차종 한글명
  ,SI.ID_SITE -- 사업장ID
  ,SI.S_SITE_NM -- 사업장명
  ,sum(TMI.LOADAGE_SUM) LOADAGE_SUM -- 적재량 합
  ,sum(TMI.FARE_SUM) FARE_SUM -- 운임료 합
  ,sum(TMI.VACANT_DISTANCE_SUM) VACANT_DISTANCE_SUM -- 공차운행거리 합
  ,sum(TMI.DISTANCE_SUM) DISTANCE_SUM -- 영차운행거리
  ,sum(TMI.MJ_SUM) MJ_SUM -- 에너지 사용량(MJ) 합
  ,sum(TMI.GCO2_SUM) GCO2_SUM -- 온실가스배출량(gCO2) 합
  ,sum(TMI.ENG_VOL_SUM) ENG_VOL_SUM -- 연료사용량 합
  ,sum(TMI.TOE_SUM) TOE_SUM -- 환상톤 합
  ,sum(TMI.TON_KM_SUM) TON_KM_SUM -- 톤킬로 합
  ,TMI.DATE_MON -- 날짜,YYYYMM
  ,substring(TMI.DATE_MON,1,4) YEAR -- 연
from 
  T_LOGIS_TRANS_MON_INFO TMI  -- 월운송정보
  LEFT JOIN T_GAS_DEPT_MAPPING GD
    ON TMI.ID_ERP_DEPT = GD.ID_ERP_DEPT
  LEFT JOIN T_ERP_DEPT ED -- ERP조직도
    ON GD.ID_ERP_DEPT=ED.ID_ERP_DEPT
  LEFT JOIN T_SITE_INFO SI    -- 사업장
    ON SI.ID_SITE=GD.ID_SITE   -- 사업장ID
  where TMI.TRANS_TYPE_CD=3   -- 철도
  GROUP BY
    TMI.TRANS_TYPE_CD -- 운송수단
    ,SI.ID_SITE -- 사업장ID
    ,SI.S_SITE_NM -- 사업장명
    ,TMI.DATE_MON -- 날짜,YYYYMM
;

-- 해송 온실가스 에너지 현황
-- create 
-- alter 
-- view V_LOGIS_TRANS_MON_SHIP as
select
  TMI.TRANS_TYPE_CD -- 운송수단
  ,(SELECT S_DESC FROM T_CODE WHERE S_CAT='TRANS_TYPE' and S_CD=TMI.TRANS_TYPE_CD ) TRANS_TYPE_NM -- 운송수단 한글명
  ,null S_LINE_CD -- 노선구분
  ,null LOGI_YCT_NM -- 노선구분 한글명
  ,null S_LOGI_WEI_CD -- 톤수
  ,null LOGI_WEI_NM -- 톤수 한글명
  ,null S_CAR_TYPE -- 차종
  ,null CAR_TYPE_NM -- 차종 한글명
  ,SI.ID_SITE -- 사업장ID
  ,SI.S_SITE_NM -- 사업장명
  ,sum(TMI.LOADAGE_SUM) LOADAGE_SUM -- 적재량 합
  ,sum(TMI.FARE_SUM) FARE_SUM -- 운임료 합
  ,sum(TMI.VACANT_DISTANCE_SUM) VACANT_DISTANCE_SUM -- 공차운행거리 합
  ,sum(TMI.DISTANCE_SUM) DISTANCE_SUM -- 영차운행거리
  ,sum(TMI.MJ_SUM) MJ_SUM -- 에너지 사용량(MJ) 합
  ,sum(TMI.GCO2_SUM) GCO2_SUM -- 온실가스배출량(gCO2) 합
  ,sum(TMI.ENG_VOL_SUM) ENG_VOL_SUM -- 연료사용량 합
  ,sum(TMI.TOE_SUM) TOE_SUM -- 환상톤 합
  ,sum(TMI.TON_KM_SUM) TON_KM_SUM -- 톤킬로 합
  ,TMI.DATE_MON -- 날짜,YYYYMM
  ,substring(TMI.DATE_MON,1,4) YEAR -- 연
from 
  T_LOGIS_TRANS_MON_INFO TMI  -- 월운송정보
  LEFT JOIN T_GAS_DEPT_MAPPING GD
    ON TMI.ID_ERP_DEPT = GD.ID_ERP_DEPT
  LEFT JOIN T_ERP_DEPT ED -- ERP조직도
    ON GD.ID_ERP_DEPT=ED.ID_ERP_DEPT
  LEFT JOIN T_SITE_INFO SI    -- 사업장
    ON SI.ID_SITE=GD.ID_SITE   -- 사업장ID
  where TMI.TRANS_TYPE_CD=2   -- 해송
  GROUP BY
    TMI.TRANS_TYPE_CD -- 운송수단
    ,SI.ID_SITE -- 사업장ID
    ,SI.S_SITE_NM -- 사업장명
    ,TMI.DATE_MON -- 날짜,YYYYMM
    ;

-- 예제
select * from V_LOGIS_TRANS_MON_VHCL 
  where
    ID_SITE=10
    and DATE_MON BETWEEN '201901' and '201907'
union
select * from V_LOGIS_TRANS_MON_TRAIN 
  where
    ID_SITE=10
    and DATE_MON BETWEEN '201901' and '201907'
union 
select * from V_LOGIS_TRANS_MON_SHIP
  where
    ID_SITE=10
    and DATE_MON BETWEEN '201901' and '201907'
;
