/*

  V_BOARD

*/


-- CREATE 
alter 
VIEW V_BOARD AS
    SELECT 
        B.ID_BOARD AS ID_BOARD,
        B.S_BOARD_TYPE AS S_BOARD_TYPE,
        B.S_TITLE AS S_TITLE,
        B.S_CONTENT AS S_CONTENT,
        B.S_REG_EMP_ID AS S_REG_EMP_ID,
        U.S_EMP_NM AS S_EMP_NM,
        B.D_REG_DATE AS D_REG_DATE,
        B.G_FILE_ID AS G_FILE_ID,
        F.S_FILE_PATH AS S_FILE_PATH,
        F.S_FILE_NM AS S_FILE_NM
    FROM
        T_BOARD B
        LEFT JOIN T_USERS U ON (B.S_REG_EMP_ID = U.S_EMP_ID)
        LEFT JOIN V_FILES F ON (B.G_FILE_ID = F.G_ID)
    order by B.D_REG_DATE desc
;


alter 
-- CREATE 
VIEW V_BOARD_DIC AS
select 
  B.ID_BOARD -- 게시물ID
  ,B.S_BOARD_TYPE -- 게시판 type
  ,B.S_TITLE -- 제목
  ,B.S_CONTENT -- 문서 body
  ,B.S_REG_EMP_ID -- 등록한 사용자 사번
  ,U.S_EMP_NM -- 사용자이름
  ,B.D_REG_DATE -- 등록일
  ,B.G_FILE_ID -- 첨부파일 GUID
  ,F.S_FILE_PATH -- 파일경로
  ,F.S_FILE_NM -- 원본 파일명
from 
  T_BOARD B
  LEFT JOIN T_USERS U
    ON B.S_REG_EMP_ID=U.S_EMP_ID
  LEFT JOIN V_FILES F
    ON B.G_FILE_ID=F.G_ID
WHERE B.S_BOARD_TYPE='4' -- 4 = 용어사전
order by B.D_REG_DATE desc
;

alter
-- CREATE 
VIEW V_BOARD_MAT AS
select 
  B.ID_BOARD -- 게시물ID
  ,B.S_BOARD_TYPE -- 게시판 type
  ,B.S_TITLE -- 제목
  ,B.S_CONTENT -- 문서 body
  ,B.S_REG_EMP_ID -- 등록한 사용자 사번
  ,U.S_EMP_NM -- 사용자이름
  ,B.D_REG_DATE -- 등록일
  ,B.G_FILE_ID -- 첨부파일 GUID
  ,F.S_FILE_PATH -- 파일경로
  ,F.S_FILE_NM -- 원본 파일명
from 
  T_BOARD B
  LEFT JOIN T_USERS U
    ON B.S_REG_EMP_ID=U.S_EMP_ID
  LEFT JOIN V_FILES F
    ON B.G_FILE_ID=F.G_ID
WHERE B.S_BOARD_TYPE='3' -- 3 = 자료실
order by B.D_REG_DATE desc
;

-- CREATE 
alter VIEW V_BOARD_QA AS
select 
  B.ID_BOARD -- 게시물ID
  ,B.S_BOARD_TYPE -- 게시판 type
  ,B.S_TITLE -- 제목
  ,B.S_CONTENT -- 문서 body
  ,B.S_REG_EMP_ID -- 등록한 사용자 사번
  ,U.S_EMP_NM -- 사용자이름
  ,B.D_REG_DATE -- 등록일
  ,B.G_FILE_ID -- 첨부파일 GUID
  ,F.S_FILE_PATH -- 파일경로
  ,F.S_FILE_NM -- 원본 파일명
from 
  T_BOARD B
  LEFT JOIN T_USERS U
    ON B.S_REG_EMP_ID=U.S_EMP_ID
  LEFT JOIN V_FILES F
    ON B.G_FILE_ID=F.G_ID
WHERE B.S_BOARD_TYPE='2' -- 2 = Q&A
order by B.D_REG_DATE desc
;

alter 
-- CREATE 
VIEW V_BOARD_NOTICE AS
select 
  B.ID_BOARD -- 게시물ID
  ,B.S_BOARD_TYPE -- 게시판 type
  ,B.S_TITLE -- 제목
  ,B.S_CONTENT -- 문서 body
  ,B.S_REG_EMP_ID -- 등록한 사용자 사번
  ,U.S_EMP_NM -- 사용자이름
  ,B.D_REG_DATE -- 등록일
  ,B.G_FILE_ID -- 첨부파일 GUID
  ,F.S_FILE_PATH -- 파일경로
  ,F.S_FILE_NM -- 원본 파일명
from 
  T_BOARD B
  LEFT JOIN T_USERS U
    ON B.S_REG_EMP_ID=U.S_EMP_ID
  LEFT JOIN V_FILES F
    ON B.G_FILE_ID=F.G_ID
WHERE B.S_BOARD_TYPE='1' -- 1 = 공지사항
order by B.D_REG_DATE desc
;


select * from V_BOARD;


-- =========================================
/*
  10/24 수정
*/
select S_BOARD_TYPE, GROUP_CONCAT(S_TITLE SEPARATOR ', ')
from T_BOARD
where S_BOARD_TYPE=1;

alter VIEW V_BOARD as
select 
  ID_BOARD
  ,S_BOARD_TYPE
  ,S_TITLE
  ,S_CONTENT
  ,B.S_REG_EMP_ID AS S_REG_EMP_ID
  ,U.S_EMP_NM AS S_EMP_NM
  ,B.D_REG_DATE
  ,B.G_FILE_ID 
  ,F.ID_FILE
  ,F.S_FILE_ID
  ,F.S_SCREEN_ID
  ,F.S_FILE_SECT
  ,F.S_FILE_PATH
  ,F.S_FILE_NM
  ,F.S_TEMP_COL
from 
  T_BOARD B
  LEFT JOIN T_USERS U ON (B.S_REG_EMP_ID = U.S_EMP_ID)
  LEFT JOIN (
    select 
        G_ID
        ,GROUP_CONCAT(ID_FILE SEPARATOR ',') ID_FILE
        ,GROUP_CONCAT(S_FILE_ID SEPARATOR ',') S_FILE_ID
        ,GROUP_CONCAT(S_SCREEN_ID SEPARATOR ',') S_SCREEN_ID
        ,GROUP_CONCAT(S_FILE_SECT SEPARATOR ',') S_FILE_SECT
        ,GROUP_CONCAT(S_FILE_PATH SEPARATOR ',') S_FILE_PATH
        ,GROUP_CONCAT(S_FILE_NM SEPARATOR ',') S_FILE_NM
        ,GROUP_CONCAT(S_TEMP_COL SEPARATOR ',') S_TEMP_COL
    from V_FILES
    group by G_ID
  ) F
  ON F.G_ID=B.G_FILE_ID
  ;

select * from T_BOARD;
select * from V_BOARD;
select * from V_FILES;
select * from T_FILES;
select * from T_BOARD where ID_BOARD=56;
select * from V_BOARD where ID_BOARD=71;
select * from V_FILES where G_ID='f49b02553a064a45bda9fc916e00b786';
select * from V_FILES where G_ID='5597dfd3dea9447a965e7ec6b6f2efba';
select 
    G_ID
    ,GROUP_CONCAT(ID_FILE SEPARATOR ',') ID_FILE
    ,GROUP_CONCAT(S_FILE_ID SEPARATOR ',') S_FILE_ID
    ,GROUP_CONCAT(S_SCREEN_ID SEPARATOR ',') S_SCREEN_ID
    ,GROUP_CONCAT(S_FILE_SECT SEPARATOR ',') S_FILE_SECT
    ,GROUP_CONCAT(S_FILE_PATH SEPARATOR ',') S_FILE_PATH
    ,GROUP_CONCAT(S_FILE_NM SEPARATOR ',') S_FILE_NM
    ,GROUP_CONCAT(S_TEMP_COL SEPARATOR ',') S_TEMP_COL
from V_FILES
where G_ID='f49b02553a064a45bda9fc916e00b786'
group by G_ID
;

select 
  B.*,
  F.*
from 
  T_BOARD B
  -- LEFT JOIN T_USERS U ON (B.S_REG_EMP_ID = U.S_EMP_ID)
  LEFT JOIN (
    select 
        G_ID
        ,GROUP_CONCAT(ID_FILE SEPARATOR ',') ID_FILE
        ,GROUP_CONCAT(S_FILE_ID SEPARATOR ',') S_FILE_ID
        ,GROUP_CONCAT(S_SCREEN_ID SEPARATOR ',') S_SCREEN_ID
        ,GROUP_CONCAT(S_FILE_SECT SEPARATOR ',') S_FILE_SECT
        ,GROUP_CONCAT(S_FILE_PATH SEPARATOR ',') S_FILE_PATH
        ,GROUP_CONCAT(S_FILE_NM SEPARATOR ',') S_FILE_NM
        ,GROUP_CONCAT(S_TEMP_COL SEPARATOR ',') S_TEMP_COL
    from V_FILES
    group by G_ID
  ) F
  ON F.G_ID=B.G_FILE_ID;
  
select * from V_FILES where ID_FILE in (96,97); -- 25838d842d9b4725aee1a0af84eba9bb
select * from T_FILE_MAPPING where ID_FILE in (95,96,97);

-- update T_FILES set S_FILE_NM=null where ID_FILE=97;  -- TEST.png