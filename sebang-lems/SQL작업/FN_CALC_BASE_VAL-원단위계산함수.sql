/*
	원단위 계산 함수
    
    IN PARAM:
		IN_A_VAL DECIMAL(20,6)	-- CO2,TOE,GJ
		IN_B_VAL DECIMAL(20,6) -- ton.km, 대수, 면적(m2),...
        
	OUT
		DECIMAL(20,6)
*/

delimiter $$
drop function FN_CALC_BASE_VAL;
CREATE FUNCTION `FN_CALC_BASE_VAL`(
    IN_A_VAL DECIMAL(40,6)	-- tCO2,TOE,GJ
    , IN_B_VAL DECIMAL(40,6) -- ton.km, 대수, 면적(m2),...
) RETURNS DECIMAL(40,6)
BEGIN
	/*
		return A / B
    */
    DECLARE ret DECIMAL(40,6);
	  
    IF IN_B_VAL = 0 THEN
		set ret = 0;
    ELSE
		set ret = IN_A_VAL / IN_B_VAL;
    END IF;
    return ret;
END $$
delimiter ;


select FN_CALC_BASE_VAL(100/1.5,1);