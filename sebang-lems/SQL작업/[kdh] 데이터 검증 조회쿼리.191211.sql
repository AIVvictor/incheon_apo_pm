select * from T_CODE where s_cat = 'INV_YCT'
;

#전체실적조회-합계
SELECT count(1) as cnt
    , EGA.C_YEAR
    , EGA.C_MON
    , sum(EGA.N_ENG) as N_ENG
    , sum(EGA.N_ENG_ETC) as N_ENG_ETC
    , sum(EGA.N_GJ) as N_GJ
    , sum(EGA.N_TOE) as N_TOE
    , sum(EGA.N_CO2) as N_CO2
    , sum(EGA.N_CH4) as N_CH4
    , sum(EGA.N_N2O) as N_N2O
    , sum(EGA.N_TCO2EQ) as N_TCO2EQ
    , sum(EGA.S_BASE_VAL) as S_BASE_VAL
    , sum(EGA.N_TOE_BASE) as N_TOE_BASE
    , sum(EGA.N_GJ_BASE) as N_GJ_BASE
    , sum(EGA.N_CO2_BASE) as N_CO2_BASE
FROM T_SITE_INFO as SI
inner join T_ENG_POINT as EP
    on SI.ID_SITE = EP.ID_SITE
inner join T_ENG_GHG_AMT as EGA
    on EP.ID_ENG_POINT = EGA.ID_ENG_POINT
where SI.C_DEL_YN != 'Y'
    and EP.C_DEL_YN != 'Y'
    and EP.S_INV_CD != '11' -- 용차 제외
group by EGA.c_year, EGA.C_MON
;


#전체실적조회-물류
SELECT count(1) as cnt
    , EGA.C_YEAR
    , EGA.C_MON
    , sum(EGA.N_ENG) as N_ENG
    , sum(EGA.N_ENG_ETC) as N_ENG_ETC
    , sum(EGA.N_GJ) as N_GJ
    , sum(EGA.N_TOE) as N_TOE
    , sum(EGA.N_CO2) as N_CO2
    , sum(EGA.N_CH4) as N_CH4
    , sum(EGA.N_N2O) as N_N2O
    , sum(EGA.N_TCO2EQ) as N_TCO2EQ
    , sum(EGA.S_BASE_VAL) as S_BASE_VAL
    , sum(EGA.N_TOE_BASE) as N_TOE_BASE
    , sum(EGA.N_GJ_BASE) as N_GJ_BASE
    , sum(EGA.N_CO2_BASE) as N_CO2_BASE
FROM T_SITE_INFO as SI
inner join T_ENG_POINT as EP
    on SI.ID_SITE = EP.ID_SITE
inner join T_ENG_GHG_AMT as EGA
    on EP.ID_ENG_POINT = EGA.ID_ENG_POINT
where SI.C_DEL_YN != 'Y'
    and EP.C_DEL_YN != 'Y'
    and EP.S_INV_CD != '11' -- 용차 제외
    and EP.S_LOGI_TYPE_CD = 1 # 물류만
group by EGA.c_year, EGA.C_MON

;
#전체실적조회-물류 외
SELECT count(1) as cnt
    , EGA.C_YEAR
    , EGA.C_MON
    , sum(EGA.N_ENG) as N_ENG
    , sum(EGA.N_ENG_ETC) as N_ENG_ETC
    , sum(EGA.N_GJ) as N_GJ
    , sum(EGA.N_TOE) as N_TOE
    , sum(EGA.N_CO2) as N_CO2
    , sum(EGA.N_CH4) as N_CH4
    , sum(EGA.N_N2O) as N_N2O
    , sum(EGA.N_TCO2EQ) as N_TCO2EQ
    , sum(EGA.S_BASE_VAL) as S_BASE_VAL
    , sum(EGA.N_TOE_BASE) as N_TOE_BASE
    , sum(EGA.N_GJ_BASE) as N_GJ_BASE
    , sum(EGA.N_CO2_BASE) as N_CO2_BASE
FROM T_SITE_INFO as SI
inner join T_ENG_POINT as EP
    on SI.ID_SITE = EP.ID_SITE
inner join T_ENG_GHG_AMT as EGA
    on EP.ID_ENG_POINT = EGA.ID_ENG_POINT
where SI.C_DEL_YN != 'Y'
    and EP.C_DEL_YN != 'Y'
    and EP.S_INV_CD != '11' -- 용차 제외
    and EP.S_LOGI_TYPE_CD = 2 # 물류 외
group by EGA.c_year, EGA.C_MON
;

#실적 - 태양광
select C_YEAR
	, sum(N_POWER)
from T_SUN_POWER
where C_DEL_YN != 'Y'
group by C_YEAR
order by C_YEAR


;


#목표 - 전사,사업장 - 연간
select T1.C_YEAR	-- 년도
	, T1.S_SITE_NM
    
	, T1.N_YEAR_GAS_GOAL	-- 연간 사내목표 가스
	, T1.N_YEAR_ENG_GOAL	-- 연간 사내목표 에너지
	, T1.N_GAS_GOAL_SUM	-- 연간 월별목표 합 가스
	, T1.N_ENG_GOAL_SUM	-- 연간 월별목표 합 에너지
	
	, SUM(T1.N_GOAL_SUM_MONSUM)	-- 월별합 - 목표 배출원
	, SUM(T1.N_GAS_GOAL_MONSUM)	-- 월별합 - 월 가스 환산
	, SUM(T1.N_ENG_GOAL_MONSUM)	-- 월별합 - 월 에너지 환산
	
	#, LPAD(T1.C_MON, 2, '0') as C_MON			-- 월
	, SUM(T1.N_MON_ENG_GOAL_MON)	-- 월별목표-배출원별
	, SUM(T1.N_GAS_GOAL_MON)			-- 월 가스 환산
	, SUM(T1.N_ENG_GOAL_MON)			-- 월 에너지 환산
from (
	select 
		SI.ID_SITE
		, SI.S_SITE_NM
        , EP.ID_ENG_POINT		-- ID  배출시설
		
		, SYG.C_YEAR	-- 년도
		, SYG.N_YEAR_GAS_GOAL	-- 연간 사내목표 가스
		, SYG.N_YEAR_ENG_GOAL	-- 연간 사내목표 에너지
		, SYG.N_GAS_GOAL_SUM	-- 연간 월별목표 합 가스
		, SYG.N_ENG_GOAL_SUM	-- 연간 월별목표 합 에너지
		
		, SMSG.N_GOAL_SUM as N_GOAL_SUM_MONSUM	-- 월별합 - 목표 배출원
		, SMSG.N_GAS_GOAL as N_GAS_GOAL_MONSUM	-- 월별합 - 월 가스 환산
		, SMSG.N_ENG_GOAL as N_ENG_GOAL_MONSUM	-- 월별합 - 월 에너지 환산
		
        , LPAD(SMG.C_MON, 2, '0') as C_MON			-- 월
        , SMG.N_MON_ENG_GOAL as N_MON_ENG_GOAL_MON	-- 월별목표-배출원별
        , SMG.N_GAS_GOAL as N_GAS_GOAL_MON			-- 월 가스 환산
        , SMG.N_ENG_GOAL as N_ENG_GOAL_MON			-- 월 에너지 환산
	from T_SITE_INFO as SI
    inner join T_ENG_POINT as EP
		on SI.ID_SITE = EP.ID_SITE
	left outer join T_SITE_YEAR_GOAL as SYG
		on SI.ID_SITE = SYG.ID_SITE
	left outer join T_SITE_MON_SUM_GOAL as SMSG
		on SYG.ID_SITE_YEAR_GOAL = SMSG.ID_SITE_YEAR_GOAL
		and EP.ID_ENG_POINT = SMSG.ID_ENG_POINT
	left outer join T_SITE_MON_GOAL as SMG
		on SMSG.ID_SITE_MON_SUM_GOAL = SMG.ID_SITE_MON_SUM_GOAL
	where SI.C_DEL_YN != 'Y'
		and EP.C_DEL_YN != 'Y'
		-- and SYG.C_YEAR = 2019
	order by  SI.ID_SITE, SYG.C_YEAR
) T1
group by T1.ID_SITE
	, T1.S_SITE_NM
	, T1.C_YEAR	-- 년도
	, T1.N_YEAR_GAS_GOAL	-- 사내년목표 가스
	, T1.N_YEAR_ENG_GOAL	-- 사내년목표에너지
	, T1.N_GAS_GOAL_SUM	-- 월별목표 합 가스
	, T1.N_ENG_GOAL_SUM	-- 월별목표 합 에너지
	#, T1.C_MON
order by T1.C_YEAR, T1.ID_SITE, T1.C_MON

;
#목표 - 전사,사업장 - 월별
select T1.C_YEAR	-- 년도
	, LPAD(T1.C_MON, 2, '0') as C_MON			-- 월
	, T1.S_SITE_NM
    
	, T1.N_YEAR_GAS_GOAL	-- 연간 사내목표 가스
	, T1.N_YEAR_ENG_GOAL	-- 연간 사내목표 에너지
	, T1.N_GAS_GOAL_SUM	-- 연간 월별목표 합 가스
	, T1.N_ENG_GOAL_SUM	-- 연간 월별목표 합 에너지
	
	, (T1.N_GOAL_SUM_MONSUM)	-- 월별합 - 목표 배출원
	, (T1.N_GAS_GOAL_MONSUM)	-- 월별합 - 월 가스 환산
	, (T1.N_ENG_GOAL_MONSUM)	-- 월별합 - 월 에너지 환산
	
	, (T1.N_MON_ENG_GOAL_MON)	-- 월별목표-배출원별
	, (T1.N_GAS_GOAL_MON)			-- 월 가스 환산
	, (T1.N_ENG_GOAL_MON)			-- 월 에너지 환산
from (
	select 
		SI.ID_SITE
		, SI.S_SITE_NM
        , EP.ID_ENG_POINT		-- ID  배출시설
		
		, SYG.C_YEAR	-- 년도
		, SYG.N_YEAR_GAS_GOAL	-- 연간 사내목표 가스
		, SYG.N_YEAR_ENG_GOAL	-- 연간 사내목표 에너지
		, SYG.N_GAS_GOAL_SUM	-- 연간 월별목표 합 가스
		, SYG.N_ENG_GOAL_SUM	-- 연간 월별목표 합 에너지
		
		, SMSG.N_GOAL_SUM as N_GOAL_SUM_MONSUM	-- 월별합 - 목표 배출원
		, SMSG.N_GAS_GOAL as N_GAS_GOAL_MONSUM	-- 월별합 - 월 가스 환산
		, SMSG.N_ENG_GOAL as N_ENG_GOAL_MONSUM	-- 월별합 - 월 에너지 환산
		
        , LPAD(SMG.C_MON, 2, '0') as C_MON			-- 월
        , SMG.N_MON_ENG_GOAL as N_MON_ENG_GOAL_MON	-- 월별목표-배출원별
        , SMG.N_GAS_GOAL as N_GAS_GOAL_MON			-- 월 가스 환산
        , SMG.N_ENG_GOAL as N_ENG_GOAL_MON			-- 월 에너지 환산
	from T_SITE_INFO as SI
    inner join T_ENG_POINT as EP
		on SI.ID_SITE = EP.ID_SITE
	left outer join T_SITE_YEAR_GOAL as SYG
		on SI.ID_SITE = SYG.ID_SITE
	left outer join T_SITE_MON_SUM_GOAL as SMSG
		on SYG.ID_SITE_YEAR_GOAL = SMSG.ID_SITE_YEAR_GOAL
		and EP.ID_ENG_POINT = SMSG.ID_ENG_POINT
	left outer join T_SITE_MON_GOAL as SMG
		on SMSG.ID_SITE_MON_SUM_GOAL = SMG.ID_SITE_MON_SUM_GOAL
	where SI.C_DEL_YN != 'Y'
		and EP.C_DEL_YN != 'Y'
		-- and SYG.C_YEAR = 2019
	order by  SI.ID_SITE, SYG.C_YEAR
) T1
order by T1.C_YEAR, T1.ID_SITE, T1.C_MON
;


#목표 - 월별2
select T1.ID_SITE
	, T1.S_SITE_NM
	
	, T1.C_YEAR	-- 년도
	#, T1.N_YEAR_GAS_GOAL	-- 사내년목표 가스
	#, T1.N_YEAR_ENG_GOAL	-- 사내년목표에너지
	#, T1.N_GAS_GOAL_SUM	-- 월별목표 합 가스
	#, T1.N_ENG_GOAL_SUM	-- 월별목표 합 에너지
	
	, T1.C_MON			-- 월
	, SUM(T1.N_MON_ENG_GOAL) as N_MON_ENG_GOAL	-- 월별목표-배출원별
	, SUM(T1.N_GAS_GOAL) as N_GAS_GOAL			-- 월 가스 환산
	, SUM(T1.N_ENG_GOAL) as N_ENG_GOAL			-- 월 에너지 환산
	, SUM(T1.N_CO2) as N_CO2 				-- 월 가스 실적
	, SUM(T1.N_GJ) as N_GJ				-- 월 에너지 실적
from (
	select 
		SI.ID_SITE
		, SI.S_SITE_NM
        , EP.ID_ENG_POINT		-- ID  배출시설
		
		#, '||'
		, SYG.C_YEAR	-- 년도
		, SYG.N_YEAR_GAS_GOAL	-- 사내년목표 가스
		, SYG.N_YEAR_ENG_GOAL	-- 사내년목표에너지
		, SYG.N_GAS_GOAL_SUM	-- 월별목표 합 가스
		, SYG.N_ENG_GOAL_SUM	-- 월별목표 합 에너지
		
		#, '||'
		#, SMSG.N_GOAL_SUM		-- 목표-월별합계
		#, SMSG.N_GAS_GOAL		-- 월 가스 환산
		#, SMSG.N_ENG_GOAL		-- 월 에너지 환산
		
        , LPAD(SMG.C_MON, 2, '0') as C_MON				-- 월
        , SMG.N_MON_ENG_GOAL	-- 월별목표-배출원별
        , SMG.N_GAS_GOAL		-- 월 가스 환산
        , SMG.N_ENG_GOAL		-- 월 에너지 환산
        
        , EGA.N_CO2				-- 월 가스 실적
        , EGA.N_GJ				-- 월 에너지 실적
	from T_SITE_INFO as SI
    inner join T_ENG_POINT as EP
		on SI.ID_SITE = EP.ID_SITE
	left outer join T_SITE_YEAR_GOAL as SYG
		on SI.ID_SITE = SYG.ID_SITE
	left outer join T_SITE_MON_SUM_GOAL as SMSG
		on SYG.ID_SITE_YEAR_GOAL = SMSG.ID_SITE_YEAR_GOAL
		and EP.ID_ENG_POINT = SMSG.ID_ENG_POINT
	left outer join T_SITE_MON_GOAL as SMG
		on SMSG.ID_SITE_MON_SUM_GOAL = SMG.ID_SITE_MON_SUM_GOAL
	LEFT OUTER JOIN T_ENG_GHG_AMT as EGA	-- select * from T_ENG_GHG_AMT
		ON (SYG.C_YEAR = EGA.C_YEAR AND LPAD(SMG.C_MON, 2, '0') = EGA.C_MON AND EP.ID_ENG_POINT = EGA.ID_ENG_POINT AND SI.ID_SITE= EP.ID_SITE)
	where SI.C_DEL_YN != 'Y'
		and EP.C_DEL_YN != 'Y'
		-- and SYG.C_YEAR = 2019
	order by  SI.ID_SITE, SYG.C_YEAR
) T1
group by T1.ID_SITE
	, T1.S_SITE_NM
	, T1.C_YEAR	-- 년도
	, T1.N_YEAR_GAS_GOAL	-- 사내년목표 가스
	, T1.N_YEAR_ENG_GOAL	-- 사내년목표에너지
	, T1.N_GAS_GOAL_SUM	-- 월별목표 합 가스
	, T1.N_ENG_GOAL_SUM	-- 월별목표 합 에너지
	, T1.C_MON
order by T1.ID_SITE, T1.C_YEAR, T1.C_MON
;

# 사업장별 목표
select SYG.c_year, SI.S_SITE_NM
	, SYG.ID_SITE_YEAR_GOAL
    , SYG.N_YEAR_GAS_GOAL
    , SYG.N_YEAR_ENG_GOAL
    , SYG.N_GAS_GOAL_SUM
    , SYG.N_ENG_GOAL_SUM
from T_SITE_INFO as SI	-- select * from T_SITE_INFO
left outer join T_SITE_YEAR_GOAL as SYG
	on SI.ID_SITE = SYG.ID_SITE
where SI.C_DEL_YN != 'Y'
order by SYG.c_year, SI.N_ORDER

;

# 물류원단위조회 - 자차/용차, tonkm/m2
select count(1) as cnt
    , EGA.C_YEAR, EGA.C_MON
    , sum(EGA.N_ENG) as N_ENG
    , sum(EGA.N_ENG_ETC) as N_ENG_ETC
    , sum(EGA.N_GJ) as N_GJ
    , sum(EGA.N_TOE) as N_TOE
    , sum(EGA.N_CO2) as N_CO2
    , sum(EGA.N_CH4) as N_CH4
    , sum(EGA.N_N2O) as N_N2O
    , sum(EGA.N_TCO2EQ) as N_TCO2EQ
    , sum(EGA.S_BASE_VAL) as S_BASE_VAL
    , sum(EGA.N_TOE_BASE) as N_TOE_BASE
    , sum(EGA.N_GJ_BASE) as N_GJ_BASE
    , sum(EGA.N_CO2_BASE) as N_CO2_BASE
from T_SITE_INFO as SI
inner join T_ENG_POINT as EP	-- select * from T_ENG_POINT
	on SI.ID_SITE = EP.ID_SITE
left join T_ENG_GHG_AMT as EGA
	on EP.ID_ENG_POINT = EGA.ID_ENG_POINT
where SI.C_DEL_YN != 'Y'
	and EP.C_DEL_YN != 'Y'
	and EP.S_LOGI_TYPE_CD = '1'	# 1:'물류' / 2:'물류 외'
    AND EP.S_INV_CD = '11'	# 10:자차 / 11:용차
    and EP.S_BASE_UNT = '4'	# 1:ton.km / 2:대수 / 3:적재량(kg) / 4:면적(m2) / 5:가동시간(hour)
group by EGA.C_YEAR, EGA.C_MON
;

#BAU
select count(1) as cnt
    , EGA.C_YEAR, EGA.C_MON
    , sum(EGA.N_GJ_BASE) as N_GJ_BASE
    , sum(EGA.N_TOE_BASE) as N_TOE_BASE
    , sum(EGA.N_CO2_BASE) as N_CO2_BASE
from T_SITE_INFO as SI
inner join T_ENG_POINT as EP	-- select * from T_ENG_POINT
	on SI.ID_SITE = EP.ID_SITE
left join T_ENG_GHG_AMT as EGA
	on EP.ID_ENG_POINT = EGA.ID_ENG_POINT
where SI.C_DEL_YN != 'Y'
	and EP.C_DEL_YN != 'Y'
    AND EP.S_INV_CD != '11'	# 10:자차 / 11:용차
group by EGA.C_YEAR, EGA.C_MON
order by EGA.C_YEAR DESC, EGA.C_MON DESC
;


# 온실가스/에너지에너지 > 실적등록 > 사용실적
SELECT EP.S_SITE_NM, YM.YYYY, YM.MM, EP.S_EMT_CD_NM
    , EP.S_POINT_NM, EP.S_ENG_CD_NM, EP.S_IN_UNT_CD_NM
    , EPA.N_USE_AMT, EPA.N_COST, EPA.N_ENG_ETC
    , (SELECT S_DESC FROM T_CODE WHERE S_CAT = 'BASE_UNT' AND S_CD = EP.S_BASE_UNT) AS N_ENG_ETC_UNT_CD_NM
FROM V_ENG_POINT as EP 
CROSS JOIN V_YEAR_MONTH as YM 
LEFT JOIN T_ENG_POINT_AMT as EPA on EP.ID_ENG_POINT = EPA.ID_ENG_POINT AND YM.YYYY = EPA.C_YEAR AND YM.MM = EPA.C_MON 
WHERE YM.YYYY = '2019' AND YM.MM = '12'
ORDER BY EP.N_SITE_ORDER, YM.YYYY, YM.MM, EP.N_EMT_ORDER, EP.S_POINT_NM
;

# 온실가스/에너지 > 에너지 실적등록 > 월마감 - 사업장별 마감
SELECT SI.S_SITE_NM AS S_SITE_NM,  -- 사업장 이름
       YM.YYYY as C_YEAR,      -- 연도
       YM.MM as C_MON,         -- 월
       ifnull(MC.C_CLOSE_YN, 'N') AS C_CLOSE_YN,  -- 마감여부:'Y'=마감'N'=마감전
       MC.S_MOD_DATE, -- 실적수정 종료일
       
       ifnull(sum(EGA.N_GJ), 0) AS GJ_SUM,        -- 에너지사용량_TJ
       ifnull(sum(EGA.N_TOE), 0) AS TOE_SUM,      -- 에너지사용량_TOE
       ifnull(sum(EGA.N_CO2), 0) AS CO2_SUM,      -- 온실가스배출량_CO2
       ifnull(sum(EGA.N_CH4), 0) AS CH4_SUM,      -- 온실가스배출량_CH4
       ifnull(sum(EGA.N_N2O), 0) AS N2O_SUM,      -- 온실가스배출량_N2O
       ifnull(sum(EGA.N_TCO2EQ), 0) AS TCO2EQ_SUM -- 온실가스배출량_tCO2eq
FROM 
    T_SITE_INFO SI  -- 사업장
    CROSS JOIN V_YEAR_MONTH as YM
    LEFT JOIN T_ENG_POINT EP    -- 배출시설
      ON (EP.ID_SITE = SI.ID_SITE)
    LEFT JOIN T_ENG_GHG_AMT EGA -- 배출원온실가스 계산(집계)
      ON (EP.ID_ENG_POINT = EGA.ID_ENG_POINT AND YM.YYYY = EGA.C_YEAR AND YM.MM = EGA.C_MON)
    LEFT JOIN T_MON_ENG_CLOSE MC  -- 월마감 
      ON (SI.ID_SITE = MC.ID_SITE AND YM.YYYY = MC.C_YEAR AND YM.MM = MC.C_MON)
where SI.C_DEL_YN != 'Y'
GROUP BY SI.ID_SITE, SI.S_SITE_ID,SI.N_ORDER, SI.C_DEL_YN, YM.YYYY, YM.MM
ORDER BY SI.N_ORDER, YM.YYYY, YM.MM
;

      
# 온실가스/에너지 > 에너지 실적등록 > 월마감 - 사업장 배출 실적 현황
SELECT T1.S_SITE_NM
 , T1.C_YEAR, T1.C_MON
 , T1.CEF_NM, T1.ENT_NM, T1.ENG_NM
 , T1.S_IN_UNT_NM
 , SUM(T1.N_USE_AMT) as N_USE_AMT
 , SUM(T1.N_COST) as N_COST
FROM (
 SELECT SI.N_ORDER   -- 정렬순서
   , SI.S_SITE_NM -- 사업장명
   , YM.YYYY AS C_YEAR -- 연도
   , YM.MM AS C_MON -- 월
   , (SELECT S_DESC from T_CODE where S_CAT='CEF' and S_CD=EC.S_CEF_CD) CEF_NM -- 배출구분 한글명
   , (SELECT S_DESC from T_CODE where S_CAT='EMT' and S_CD=EC.S_EMT_CD) ENT_NM -- 배출원구분 한글명
   , (SELECT S_DESC from T_CODE where S_CAT='ENG' and S_CD=EC.S_ENG_CD) ENG_NM -- 사용에너지원 한글명
   , (SELECT S_DESC from T_CODE where S_CAT='UNT' and S_CD=EC.S_IN_UNT_CD) S_IN_UNT_NM -- 입력단위 표기
   , ifnull(EPA.N_USE_AMT,0)  N_USE_AMT -- 사용량
   , ifnull(EPA.N_COST,0) N_COST -- 사용금액
 FROM T_SITE_INFO  SI   -- 사업장
 CROSS JOIN V_YEAR_MONTH AS YM
 LEFT JOIN T_MON_ENG_CLOSE MEC -- 월마감 
  ON SI.ID_SITE = MEC.ID_SITE
  AND YM.YYYY = MEC.C_YEAR
  AND YM.MM = MEC.C_MON
        AND MEC.C_CLOSE_YN='Y'
 LEFT JOIN T_ENG_POINT EP    -- 배출시설
  ON SI.ID_SITE = EP.ID_SITE
   and EP.C_DEL_YN != 'Y'
 LEFT JOIN T_ENG_POINT_AMT EPA -- 에너지사용량 실적관리
  ON EPA.ID_ENG_POINT = EP.ID_ENG_POINT 
   and YM.YYYY = EPA.C_YEAR
   and YM.MM = EPA.C_MON
 LEFT JOIN T_ENG_CEF EC      -- 배출원관리
  ON EP.ID_ENG_CEF = EC.ID_ENG_CEF
   and EC.C_DEL_YN='N'
 where SI.C_DEL_YN != 'Y' 
) as T1
GROUP BY T1.N_ORDER, T1.S_SITE_NM
 , T1.C_YEAR, T1.C_MON
 , T1.CEF_NM, T1.ENT_NM, T1.ENG_NM
 , T1.S_IN_UNT_NM
ORDER BY T1.N_ORDER, T1.C_YEAR, T1.C_MON
;

#온실가스/에너지 > 온실가스/에너지 > 연도별분석 - 사업장전체
SELECT 
	EGA.C_YEAR AS C_YEAR,
	C.S_EMT_CD AS S_EMT_CD,
	(SELECT T_CODE.S_DESC FROM T_CODE WHERE T_CODE.S_CAT = 'EMT' AND T_CODE.S_CD = C.S_EMT_CD) AS S_DESC,
	SUM(EGA.N_GJ) AS GJ,
	SUM(EGA.N_TOE) AS TOE,
	SUM(EGA.N_CO2) AS CO2,
	SUM(EGA.N_CH4) AS CH4,
	SUM(EGA.N_N2O) AS N2O,
	SUM(EGA.N_TCO2EQ) AS TCO2EQ
FROM T_ENG_GHG_AMT EGA
INNER JOIN T_ENG_POINT EP ON EGA.ID_ENG_POINT = EP.ID_ENG_POINT
INNER JOIN T_SITE_INFO SI ON EP.ID_SITE = SI.ID_SITE
INNER JOIN T_ENG_CEF C ON C.ID_ENG_CEF = EP.ID_ENG_CEF 
WHERE EP.C_DEL_YN != 'Y' AND C.C_DEL_YN != 'Y' and SI.C_DEL_YN !='Y'
	AND EP.S_INV_CD != 11    -- 배출 시설 11=화물차(용차) 제외
GROUP BY EGA.C_YEAR, C.S_EMT_CD
ORDER BY EGA.C_YEAR DESC, C.S_EMT_CD
;

#온실가스/에너지 > 온실가스/에너지 > 연도별분석 - 사업장n
SELECT 
	EGA.C_YEAR AS C_YEAR,
    SI.ID_SITE,
    SI.S_SITE_NM,
	SUM(EGA.N_GJ) AS GJ,
	SUM(EGA.N_TOE) AS TOE,
	SUM(EGA.N_CO2) AS CO2,
	SUM(EGA.N_CH4) AS CH4,
	SUM(EGA.N_N2O) AS N2O,
	SUM(EGA.N_TCO2EQ) AS TCO2EQ
FROM T_ENG_GHG_AMT EGA
INNER JOIN T_ENG_POINT EP ON EGA.ID_ENG_POINT = EP.ID_ENG_POINT
INNER JOIN T_SITE_INFO SI ON EP.ID_SITE = SI.ID_SITE
INNER JOIN T_ENG_CEF C ON C.ID_ENG_CEF = EP.ID_ENG_CEF 
WHERE EP.C_DEL_YN != 'Y' AND C.C_DEL_YN != 'Y' and SI.C_DEL_YN !='Y'
	AND EP.S_INV_CD != 11    -- 배출 시설 11=화물차(용차) 제외
    AND EGA.C_YEAR between '2015' and '2019'
GROUP BY EGA.C_YEAR, SI.ID_SITE, SI.S_SITE_NM
ORDER BY EGA.C_YEAR DESC, SI.ID_SITE
;

#온실가스/에너지 > 온실가스/에너지 > 연도별분석 - 사업장1 배출시설n
SELECT 
	EGA.C_YEAR AS C_YEAR,
    SI.ID_SITE,
    SI.S_SITE_NM,
    EP.ID_ENG_POINT,
    EP.S_POINT_NM, 
	C.S_EMT_CD AS S_EMT_CD,
	(SELECT T_CODE.S_DESC FROM T_CODE WHERE T_CODE.S_CAT = 'EMT' AND T_CODE.S_CD = C.S_EMT_CD) AS S_DESC,
	SUM(EGA.N_GJ) AS GJ,
	SUM(EGA.N_TOE) AS TOE,
	SUM(EGA.N_CO2) AS CO2,
	SUM(EGA.N_CH4) AS CH4,
	SUM(EGA.N_N2O) AS N2O,
	SUM(EGA.N_TCO2EQ) AS TCO2EQ
FROM T_ENG_GHG_AMT EGA
INNER JOIN T_ENG_POINT EP ON EGA.ID_ENG_POINT = EP.ID_ENG_POINT
INNER JOIN T_SITE_INFO SI ON EP.ID_SITE = SI.ID_SITE
INNER JOIN T_ENG_CEF C ON C.ID_ENG_CEF = EP.ID_ENG_CEF 
WHERE EP.C_DEL_YN != 'Y' AND C.C_DEL_YN != 'Y' and SI.C_DEL_YN !='Y'
	AND EP.S_INV_CD != 11    -- 배출 시설 11=화물차(용차) 제외
    AND EGA.C_YEAR between '2015' and '2019'
GROUP BY EGA.C_YEAR, SI.ID_SITE, SI.S_SITE_NM, EP.ID_ENG_POINT, EP.S_POINT_NM, C.S_EMT_CD
ORDER BY EGA.C_YEAR DESC, SI.ID_SITE, EP.ID_ENG_POINT, C.S_EMT_CD
;


#온실가스/에너지 > 온실가스/에너지 > 월별분석 - 사업장전체
SELECT 
	EGA.C_YEAR,
	EGA.C_MON,
	C.S_EMT_CD,
	(SELECT T_CODE.S_DESC FROM T_CODE WHERE T_CODE.S_CAT = 'EMT' AND T_CODE.S_CD = C.S_EMT_CD) AS S_DESC,
	SUM(EGA.N_GJ) AS GJ,
	SUM(EGA.N_TOE) AS TOE,
	SUM(EGA.N_CO2) AS CO2,
	SUM(EGA.N_CH4) AS CH4,
	SUM(EGA.N_N2O) AS N2O,
	SUM(EGA.N_TCO2EQ) AS TCO2EQ
FROM T_ENG_GHG_AMT EGA
INNER JOIN T_ENG_POINT EP ON EGA.ID_ENG_POINT = EP.ID_ENG_POINT
INNER JOIN T_SITE_INFO SI ON EP.ID_SITE = SI.ID_SITE
INNER JOIN T_ENG_CEF C ON C.ID_ENG_CEF = EP.ID_ENG_CEF 
WHERE EP.C_DEL_YN != 'Y' AND C.C_DEL_YN != 'Y' and SI.C_DEL_YN !='Y'
	AND EP.S_INV_CD != 11    -- 배출 시설 11=화물차(용차) 제외
GROUP BY EGA.C_YEAR, EGA.C_MON, C.S_EMT_CD
ORDER BY EGA.C_YEAR DESC, EGA.C_MON DESC, C.S_EMT_CD
;



#온실가스/에너지 > 온실가스/에너지 > 월별분석 - 사업장n
SELECT 
	EGA.C_YEAR,
	EGA.C_MON,
    SI.ID_SITE,
    SI.S_SITE_NM,
	SUM(EGA.N_GJ) AS GJ,
	SUM(EGA.N_TOE) AS TOE,
	SUM(EGA.N_CO2) AS CO2,
	SUM(EGA.N_CH4) AS CH4,
	SUM(EGA.N_N2O) AS N2O,
	SUM(EGA.N_TCO2EQ) AS TCO2EQ
FROM T_ENG_GHG_AMT EGA
INNER JOIN T_ENG_POINT EP ON EGA.ID_ENG_POINT = EP.ID_ENG_POINT
INNER JOIN T_SITE_INFO SI ON EP.ID_SITE = SI.ID_SITE
INNER JOIN T_ENG_CEF C ON C.ID_ENG_CEF = EP.ID_ENG_CEF 
WHERE EP.C_DEL_YN != 'Y' AND C.C_DEL_YN != 'Y' and SI.C_DEL_YN !='Y'
	AND EP.S_INV_CD != 11    -- 배출 시설 11=화물차(용차) 제외
    AND EGA.C_YEAR between '2015' and '2019'
GROUP BY EGA.C_YEAR, EGA.C_MON, SI.ID_SITE, SI.S_SITE_NM
ORDER BY EGA.C_YEAR DESC, EGA.C_MON DESC, SI.ID_SITE
;


#온실가스/에너지 > 온실가스/에너지 > 월별분석 - 사업장1 배출시설n
SELECT 
	EGA.C_YEAR,
	EGA.C_MON,
    SI.ID_SITE,
    SI.S_SITE_NM,
    EP.ID_ENG_POINT,
    EP.S_POINT_NM, 
	C.S_EMT_CD AS S_EMT_CD,
	(SELECT T_CODE.S_DESC FROM T_CODE WHERE T_CODE.S_CAT = 'EMT' AND T_CODE.S_CD = C.S_EMT_CD) AS S_DESC,
	SUM(EGA.N_GJ) AS GJ,
	SUM(EGA.N_TOE) AS TOE,
	SUM(EGA.N_CO2) AS CO2,
	SUM(EGA.N_CH4) AS CH4,
	SUM(EGA.N_N2O) AS N2O,
	SUM(EGA.N_TCO2EQ) AS TCO2EQ
FROM T_ENG_GHG_AMT EGA
INNER JOIN T_ENG_POINT EP ON EGA.ID_ENG_POINT = EP.ID_ENG_POINT
INNER JOIN T_SITE_INFO SI ON EP.ID_SITE = SI.ID_SITE
INNER JOIN T_ENG_CEF C ON C.ID_ENG_CEF = EP.ID_ENG_CEF 
WHERE EP.C_DEL_YN != 'Y' AND C.C_DEL_YN != 'Y' and SI.C_DEL_YN !='Y'
	AND EP.S_INV_CD != 11    -- 배출 시설 11=화물차(용차) 제외
    AND EGA.C_YEAR between '2015' and '2019'
GROUP BY EGA.C_YEAR, EGA.C_MON, SI.ID_SITE, SI.S_SITE_NM, EP.ID_ENG_POINT, EP.S_POINT_NM, C.S_EMT_CD
ORDER BY EGA.C_YEAR DESC, EGA.C_MON DESC, SI.ID_SITE, EP.ID_ENG_POINT, C.S_EMT_CD
;



#온실가스/에너지 > 온실가스/에너지 > 목표대비 실적 분석
SELECT YM.YYYY
	, YM.MM
    , EP.ID_SITE
	, EP.ID_ENG_POINT
	, EP.S_POINT_NM
	, EP.ID_ENG_CEF
	, EP.S_ENG_CD
    , EP.S_ENG_CD_NM
	, GOAL.N_MON_ENG_GOAL
    , GOAL.N_GAS_GOAL
    , GOAL.N_ENG_GOAL
	, EGA.N_GJ
	, EGA.N_CO2
	, EGA.N_ENG
	, EGA.N_TOE
	, EGA.N_CH4
	, EGA.N_N2O
	, EGA.N_TCO2EQ
FROM V_YEAR_MONTH as YM
CROSS JOIN V_ENG_POINT as EP
LEFT OUTER JOIN (
	SELECT 
		YG.C_YEAR,	-- 년
		LPAD(MG.C_MON, 2, '0') AS C_MON, -- 월
		YG.ID_SITE,		-- 사업장ID
		SG.ID_ENG_POINT,-- 배출시설ID
		MG.N_MON_ENG_GOAL,	-- 월별 목표  - 배출원별(kW,L,M3,...)
        MG.N_GAS_GOAL,	-- 월별 목표 - tCO2 환산 목표량
        MG.N_ENG_GOAL	-- 월별 목표 - GJ 환산 목표량
	FROM T_SITE_YEAR_GOAL as YG -- 년간사업장 목표
	INNER JOIN T_SITE_MON_SUM_GOAL as SG -- 월별 배출원 목표 합(구,월별 사업장 목표 합)
		ON (YG.ID_SITE_YEAR_GOAL = SG.ID_SITE_YEAR_GOAL)
	INNER JOIN T_SITE_MON_GOAL as MG -- 사업장 배출시설별 목표 월별
		ON (SG.ID_SITE_MON_SUM_GOAL = MG.ID_SITE_MON_SUM_GOAL)
) as GOAL
	ON (YM.YYYY = GOAL.C_YEAR AND YM.MM = GOAL.C_MON AND EP.ID_ENG_POINT = GOAL.ID_ENG_POINT AND GOAL.ID_SITE= EP.ID_SITE)
LEFT OUTER JOIN T_ENG_GHG_AMT as EGA
	ON (YM.YYYY = EGA.C_YEAR AND YM.MM = EGA.C_MON AND EP.ID_ENG_POINT = EGA.ID_ENG_POINT AND GOAL.ID_SITE= EP.ID_SITE)
where EP.S_INV_CD != '11'	-- 용차는 제외
	-- and EP.ID_ENG_CEF = 6
ORDER BY YM.YYYY, YM.MM, EP.ID_SITE, EP.ID_ENG_CEF

;


#온실가스/에너지 > 온실가스/에너지 > 원단위 분석
select count(1) as cnt
    , EGA.C_YEAR, EGA.C_MON
    , SI.ID_SITE
    , SI.S_SITE_NM
    , sum(EGA.N_ENG) as N_ENG
    , sum(EGA.N_ENG_ETC) as N_ENG_ETC
    , sum(EGA.N_GJ) as N_GJ
    , sum(EGA.N_TOE) as N_TOE
    , sum(EGA.N_CO2) as N_CO2
    , sum(EGA.N_CH4) as N_CH4
    , sum(EGA.N_N2O) as N_N2O
    , sum(EGA.N_TCO2EQ) as N_TCO2EQ
    , sum(EGA.S_BASE_VAL) as S_BASE_VAL
    , sum(EGA.N_GJ_BASE) as N_GJ_BASE
    , sum(EGA.N_TOE_BASE) as N_TOE_BASE
    , sum(EGA.N_CO2_BASE) as N_CO2_BASE
from T_SITE_INFO as SI
inner join T_ENG_POINT as EP
	on SI.ID_SITE = EP.ID_SITE
left join T_ENG_GHG_AMT as EGA
	on EP.ID_ENG_POINT = EGA.ID_ENG_POINT
where SI.C_DEL_YN != 'Y'
	and EP.C_DEL_YN != 'Y'
	# and EP.S_LOGI_TYPE_CD = '1'	# 1:'물류' / 2:'물류 외'
    AND EP.S_INV_CD != '11'	# 10:자차 / 11:용차
    and EP.S_BASE_UNT = '1'	# 1:ton.km / 2:대수 / 3:적재량(kg) / 4:면적(m2) / 5:가동시간(hour)
group by EGA.C_YEAR, EGA.C_MON, SI.ID_SITE, SI.S_SITE_NM
order by EGA.C_YEAR, EGA.C_MON, SI.ID_SITE, SI.S_SITE_NM
;

#물류에너지관리 > 운송정보 - 화물차/철도운송/해상운송
SELECT 
	SI.S_SITE_NM		-- 부서명
	, EP.S_POINT_NM		-- 배출시설명(선박명,철도명)
	, V.ID_ERP_DEPT		-- ERP부서ID
	, V.S_VRN			-- 차량번호
	, V.S_LOGI_WEI_CD	-- 톤수
	, V.S_SELFVRN_CD	-- 자차구분
	, TC.S_TRANS_COM_NM -- 운수회사
	, LTM.DATE_MON 		-- YYYYMM
	, LTM.FUEL_EFF 		-- 공인연비
	, LTM.TRANS_TYPE_CD -- 운송구분 1:화물차, 2:?, 3:?
	, LTM.FARE_SUM		-- 운임료
	, LTM.LOADAGE_SUM	-- 적재량
	, LTM.DISTANCE_SUM	-- 영차운행거리
	, LTM.VACANT_DISTANCE_SUM	-- 공차운행거리
	, ifnull(LTM.DISTANCE_SUM,0) + ifnull(LTM.VACANT_DISTANCE_SUM,0) AS DISTANCE_SUM -- 운행거리
    , LTM.MJ_SUM		-- GJ
    , LTM.TOE_SUM		-- TOE
	, LTM.GCO2_SUM		-- CO2
    , LTM.ENG_VOL_SUM	-- 연료사용량
	, LTM.BAS_UNT		-- 원단위
	, LTM.BASE_VAL		-- 원단위 값
FROM T_SITE_INFO as SI
INNER join T_ENG_POINT as EP
	on SI.ID_SITE = EP.ID_SITE
INNER JOIN T_LOGIS_TRANS_MON_INFO as LTM
	on EP.ID_ENG_POINT = LTM.ID_ENG_POINT
LEFT JOIN T_VEHICLE V
	ON LTM.ID_VEHICLE = V.ID_VEHICLE
	and V.C_DEL_YN != 'Y'
LEFT JOIN T_TRANS_COM TC
	ON LTM.ID_TRANS_COM = TC.ID_TRANS_COM
	and TC.C_DEL_YN != 'Y'
where SI.C_DEL_YN != 'Y'
	and EP.C_DEL_YN != 'Y'
;


#물류에너지관리 > 차량정보
SELECT ID_TRANS_COM, ID_ERP_DEPT, S_VRN, S_LINE_CD, S_LOGI_WEI_CD, S_CAR_TYPE, S_VIN
	, S_FUEL_TYPE_CD, S_CAR_MADE, S_USE_TYPE_CD, S_CAR_STATUS_CD, S_CAR_COMPANY
    , S_CAR_MODEL, S_REG_DATE, S_MOD_DATE, S_CAR_LOAN_DATE, S_REG_ID, D_REG_DATE
    , S_MOD_ID, D_MOD_DATE, C_DEL_YN, S_SELFVRN_CD, S_FUEL_EFF, S_TRANS_COM_CD, S_ERP_DEPT_CD
FROM T_VEHICLE
where C_DEL_YN != 'Y'
order by s_vrn
limit 0, 100
;

#물류에너지관리 > 온실가스/에너지 > 온실가스/에너지현황
SELECT SI.ID_SITE, SI.S_SITE_NM
	, D.C_YEAR
    , D.QRY_OBJ_TYPE
    , D.S_SELFVRN_CD
    , D.S_LOGI_WEI_CD
    , D.S_CAR_TYPE
	, IFNULL(SUM(D.MJ_SUM), 0) as MJ_SUM
	, IFNULL(SUM(D.GCO2_SUM), 0) as GCO2_SUM
	, IFNULL(SUM(D.ENG_VOL_SUM), 0) as ENG_VOL_SUM
FROM T_SITE_INFO as SI
LEFT OUTER JOIN (
	/* 화물 */
	SELECT C_YEAR, ID_SITE, S_SITE_NM, S_SELFVRN_CD, S_LOGI_WEI_CD, S_CAR_TYPE
		, SUM(MJ_SUM) as MJ_SUM
		, SUM(GCO2_SUM) as GCO2_SUM
		, SUM(ENG_VOL_SUM) as ENG_VOL_SUM
		, 1 as QRY_OBJ_TYPE
	FROM T_LOGIS_TRANS_YEAR_VHCL
	where 1=1
	#		and S_LOGI_WEI_CD = #
	#		and S_CAR_TYPE = #
	GROUP BY C_YEAR, ID_SITE, S_SITE_NM, S_SELFVRN_CD, S_LOGI_WEI_CD, S_CAR_TYPE
	
	UNION ALL

	/* 철도 */
	SELECT C_YEAR, ID_SITE, S_SITE_NM, S_SELFVRN_CD, S_LOGI_WEI_CD, S_CAR_TYPE
		, SUM(MJ_SUM) as MJ_SUM
		, SUM(GCO2_SUM) as GCO2_SUM
		, SUM(ENG_VOL_SUM) as ENG_VOL_SUM
		, 3 as QRY_OBJ_TYPE
	FROM T_LOGIS_TRANS_YEAR_TRAIN   
	GROUP BY C_YEAR, ID_SITE, S_SITE_NM, S_SELFVRN_CD, S_LOGI_WEI_CD, S_CAR_TYPE
 
	UNION ALL

	/* 해송 */
	SELECT C_YEAR, ID_SITE, S_SITE_NM, S_SELFVRN_CD, S_LOGI_WEI_CD, S_CAR_TYPE
		, SUM(MJ_SUM) as MJ_SUM
		, SUM(GCO2_SUM) as GCO2_SUM
		, SUM(ENG_VOL_SUM) as ENG_VOL_SUM
		, 2 as QRY_OBJ_TYPE
	FROM T_LOGIS_TRANS_YEAR_SHIP   
	GROUP BY C_YEAR, ID_SITE, S_SITE_NM, S_SELFVRN_CD, S_LOGI_WEI_CD, S_CAR_TYPE
	
	UNION ALL

	/* 기타(고정체) */
	SELECT C_YEAR, ID_SITE, S_SITE_NM, S_SELFVRN_CD, S_LOGI_WEI_CD, S_CAR_TYPE
		, SUM(MJ_SUM) as MJ_SUM
		, SUM(GCO2_SUM) as GCO2_SUM
		, SUM(ENG_VOL_SUM) as ENG_VOL_SUM
		, 4 as QRY_OBJ_TYPE
	FROM T_LOGIS_TRANS_YEAR_FACIL
	GROUP BY C_YEAR, ID_SITE, S_SITE_NM, S_SELFVRN_CD, S_LOGI_WEI_CD, S_CAR_TYPE

) as D
ON SI.ID_SITE = D.ID_SITE
		#and D.QRY_OBJ_TYPE = ''
where SI.c_del_yn != 'Y'
GROUP BY SI.ID_SITE, SI.S_SITE_NM, D.C_YEAR, D.QRY_OBJ_TYPE, D.S_SELFVRN_CD, D.S_LOGI_WEI_CD, D.S_CAR_TYPE
ORDER BY SI.ID_SITE, SI.S_SITE_NM, D.C_YEAR, D.QRY_OBJ_TYPE, D.S_SELFVRN_CD, D.S_LOGI_WEI_CD, D.S_CAR_TYPE
;


#물류에너지관리 > 온실가스/에너지 > 효율화지표
SELECT *
	, (M1_TON_KM_SUM + M2_TON_KM_SUM + M3_TON_KM_SUM) as Q1_TON_KM
	, (M4_TON_KM_SUM + M5_TON_KM_SUM + M6_TON_KM_SUM) as Q2_TON_KM
	, (M7_TON_KM_SUM + M8_TON_KM_SUM + M9_TON_KM_SUM) as Q3_TON_KM
	, (M10_TON_KM_SUM + M11_TON_KM_SUM + M12_TON_KM_SUM) as Q4_TON_KM
	, (M1_GCO2_SUM + M2_GCO2_SUM + M3_GCO2_SUM) as Q1_GCO2
	, (M4_GCO2_SUM + M5_GCO2_SUM + M6_GCO2_SUM) as Q2_GCO2
	, (M7_GCO2_SUM + M8_GCO2_SUM + M9_GCO2_SUM) as Q3_GCO2
	, (M10_GCO2_SUM + M11_GCO2_SUM + M12_GCO2_SUM) as Q4_GCO2
	, (M1_MJ_SUM + M2_MJ_SUM + M3_MJ_SUM) as Q1_MJ
	, (M4_MJ_SUM + M5_MJ_SUM + M6_MJ_SUM) as Q2_MJ
	, (M7_MJ_SUM + M8_MJ_SUM + M9_MJ_SUM) as Q3_MJ
	, (M10_MJ_SUM + M11_MJ_SUM + M12_MJ_SUM) as Q4_MJ
	, (M1_ENG_VOL_SUM + M2_ENG_VOL_SUM + M3_ENG_VOL_SUM) as Q1_ENG_VOL
	, (M4_ENG_VOL_SUM + M5_ENG_VOL_SUM + M6_ENG_VOL_SUM) as Q2_ENG_VOL
	, (M7_ENG_VOL_SUM + M8_ENG_VOL_SUM + M9_ENG_VOL_SUM) as Q3_ENG_VOL
	, (M10_ENG_VOL_SUM + M11_ENG_VOL_SUM + M12_ENG_VOL_SUM) as Q4_ENG_VOL
FROM (
	select ID_SITE, LINE_CD, S_SELFVRN_CD, LOGI_WEI_CD, CAR_TYPE, TRANS_TYPE_CD
		, C_YEAR
        , M1_TON_KM_SUM, M2_TON_KM_SUM, M3_TON_KM_SUM, M4_TON_KM_SUM, M5_TON_KM_SUM, M6_TON_KM_SUM, M7_TON_KM_SUM, M8_TON_KM_SUM, M9_TON_KM_SUM, M10_TON_KM_SUM, M11_TON_KM_SUM, M12_TON_KM_SUM
        , M1_GCO2_SUM, M2_GCO2_SUM, M3_GCO2_SUM, M4_GCO2_SUM, M5_GCO2_SUM, M6_GCO2_SUM, M7_GCO2_SUM, M8_GCO2_SUM, M9_GCO2_SUM, M10_GCO2_SUM, M11_GCO2_SUM, M12_GCO2_SUM
        , M1_MJ_SUM, M2_MJ_SUM, M3_MJ_SUM, M4_MJ_SUM, M5_MJ_SUM, M6_MJ_SUM, M7_MJ_SUM, M8_MJ_SUM, M9_MJ_SUM, M10_MJ_SUM, M11_MJ_SUM, M12_MJ_SUM
        , M1_ENG_VOL_SUM, M2_ENG_VOL_SUM, M3_ENG_VOL_SUM, M4_ENG_VOL_SUM, M5_ENG_VOL_SUM, M6_ENG_VOL_SUM, M7_ENG_VOL_SUM, M8_ENG_VOL_SUM, M9_ENG_VOL_SUM, M10_ENG_VOL_SUM, M11_ENG_VOL_SUM, M12_ENG_VOL_SUM
	FROM  T_LOGIS_TRANS_MON_EFF_INFO as EI	-- select * from T_LOGIS_TRANS_MON_EFF_INFO
	GROUP BY C_YEAR
) T
ORDER BY C_YEAR
;








select c_del_yn, count(1) from T_VEHICLE group by c_del_yn;				-- 차량 마스터		Y:19366 / N:18199
select c_del_yn, count(1) from T_TRANS_COM group by c_del_yn;			-- 운수사 마스터	Y:0 / N:18529
select c_del_yn, count(1) from T_ERP_DEPT group by c_del_yn;			-- ERP 부서 조직	Y:156 /  N:238
select c_del_yn, count(1) from T_GAS_DEPT_MAPPING group by c_del_yn;	-- ERP,사업장 mapping
select c_del_yn, count(1) from T_SITE_INFO group by c_del_yn;  			-- 사업장	Y:13, N:12
;









SELECT count(1) FROM T_ENG_POINT_BAK_20191210;
SELECT count(1) FROM T_ENG_POINT_BAK_20191211;


SELECT count(1), sum(n_use_amt), sum(n_cost), sum(n_gj), sum(n_tco2), sum(n_toe), sum(s_base_val)
#FROM T_ENG_POINT_AMT_BAK_20191210;	# 10226	18985784.819200	31261202.100000	534761.240000	523176481.628271	192825.118497	6011827027.234744
FROM T_ENG_POINT_AMT_BAK_20191211;	# 10226	18985784.819200	31261202.100000	534761.240000	523176481.628271	192863.750277	6011827077.240544


SELECT count(1), sum(n_gj), sum(n_co2), sum(n_toe), sum(s_base_val), sum(n_gj_base), sum(n_co2_base), sum(n_toe_base)
#FROM T_ENG_GHG_AMT_BAK_191210;	# 10226	317131.927164	19054.653298	7573.110472	6018145080.466008	2755.314350	154.378730	65.796581
FROM T_ENG_GHG_AMT_BAK_191211;	# 10226	317131.927164	19054.653298	7705.740312	6018145080.466008	2755.314350	154.378730	65.887236


####################
select *
from (
	select T1.ID_ENG_GHG_AMT, T2.N_GJ
	from T_ENG_GHG_AMT_BAK_191211 as T1
	left outer join T_ENG_GHG_AMT_BAK_191210 as T2
		on T1.ID_ENG_GHG_AMT = T2.ID_ENG_GHG_AMT
) T
where T.N_GJ is null

;