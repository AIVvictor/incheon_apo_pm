/*
  SP_ADD_T_CODE_NEW_YEAR: 신규년도 코드 추가
  
  IN
    
  OUT
    
  
*/
DELIMITER $$
drop procedure SP_ADD_T_CODE_NEW_YEAR;
create procedure SP_ADD_T_CODE_NEW_YEAR(
  IN IN_YYYY VARCHAR(255)
) 
BEGIN
  /*
  Created at 2019/11/13 By Richard
  
  @DESCRIPTION
  	매년 1월1일0시 동작할 scheule task
    T_CODE에 신규년도 코드 추가
  @PARAM
	  IN IN_YYYY VARCHAR(255): 연도. null 인 경우 today 기준 연도 코드를 추가한다.
  @RETURN
  	없음
  */
  
  -- 어제 기준의 Year Month를 구한다.
  set @YYYY=IN_YYYY;
  if IN_YYYY is null
  then
    set @YYYY=DATE_FORMAT( now(),'%Y');
  end if;
  
  IF NOT EXISTS(select (1) from T_CODE where S_CAT='YYYY' and S_CD=@YYYY)
  then
	-- select 'insert';
    select @NEW_ORDER := max(N_ORDER)+1
	from T_CODE
    where S_CAT = 'YYYY'
		and C_DEL_YN != 'Y';
    
    INSERT INTO T_CODE(
		S_CAT
        , S_CD
        , S_DESC
        , S_EXP
        , C_DEL_YN
        , N_ORDER
	) VALUES (
		'YYYY'
        , @YYYY
        , @YYYY
        , @YYYY
        , 'N'
        , @NEW_ORDER
    );
  -- else
	-- select 'end';
  end if;

	-- logging
	INSERT INTO T_SCHEDULER_LOG (SP_NM) VALUES ('SP_ADD_T_CODE_NEW_YEAR');

END $$
DELIMITER ;

select * from T_CODE where S_CAT='YYYY'; -- and S_CD='2019';
call SP_ADD_T_CODE_NEW_YEAR(2023);

select * from T_SCHEDULER_LOG order by seq desc;

-- scheduler ON 여부 확인
show variables like 'event%' ;
-- scheduler ON 
SET GLOBAL event_scheduler = ON ;


-- 이벤트 등록
DELIMITER $$
-- create 
ALTER 
event event_SP_ADD_T_CODE_NEW_YEAR
    ON SCHEDULE
      EVERY 1 YEAR             -- 매년
	STARTS '2019-01-01 0:0:0' -- 0시에
      ENABLE  
    DO
    BEGIN
      call SP_ADD_T_CODE_NEW_YEAR(null);
	END$$
DELIMITER ;

--이벤트 목록 보기 
SELECT * FROM information_schema.EVENTS;

-- 이벤트 수정
-- ALTER EVENT `이벤트명` .....;
