﻿/*

V_SITE_GOAL / 사업장별 연 목표

정부목표

*/
;
-- CREATE 
-- alter VIEW V_SITE_GOAL AS
select 
  YM.YYYY C_YEAR 
  -- ,SG.C_YEAR -- 연도
  ,SI.ID_SITE
  ,SI.S_SITE_ID
  ,SI.S_SITE_NM -- 사업장ID,사업장이름
  ,SG.N_YEAR_GAS_GOAL
  ,SG.N_YEAR_ENG_GOAL 
  ,SI.C_DEL_YN
	from
		T_SITE_INFO SI -- 사업장
		CROSS JOIN (
			SELECT DISTINCT YYYY FROM V_YEAR_MONTH   -- 연도만 필요하기 때문에 DISTINCT 해줌
		) as YM
	LEFT JOIN T_SITE_YEAR_GOAL SG -- 년간사업장 목표
    ON SG.ID_SITE = SI.ID_SITE 
      and YM.YYYY = SG.C_YEAR
      and SI.C_DEL_YN='N'    
where -- YM.YYYY=2019  and 
	SI.C_DEL_YN='N'      
;
    
select * from V_SITE_GOAL where C_YEAR='2019';    

select * from V_YEAR_MONTH 
;
T_SITE_INFO 
;
    
    
    
    