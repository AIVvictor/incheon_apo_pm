/*
	SP_LOG()
	RETURN VOID
*/
DELIMITER $$
drop procedure SP_LOG;
create procedure SP_LOG (
  IN IN_SP_NAME VARCHAR(255),	-- 프로시져 네임
  IN IN_MSG VARCHAR(5000)	-- 메시지명
) 
BEGIN
    insert into T_DEBUG_LOG(MSG) value( concat(IN_SP_NAME, ' => ', IN_MSG) );
END $$
delimiter ;

call SP_LOG('test', 'TEST MSG');

select * from T_DEBUG_LOG order by seq desc;