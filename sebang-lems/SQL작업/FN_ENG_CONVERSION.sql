/*
	FN_ENG_CONVERSION

	RETURN
		CO2: tCO2 단위
        J: GJ 단위
        TOE: 1 TOE 단위
*/
delimiter $$
drop function FN_ENG_CONVERSION;
CREATE FUNCTION `FN_ENG_CONVERSION`(
    IN_ENG_TYPE VARCHAR(255) -- 구분: 'CO2','GJ','TOE'
    , IN_ID_ENG_CEF VARCHAR(255) -- 배출원ID
    , IN_DATE VARCHAR(255) -- YYYYMMDD
    , USE_VALUE DECIMAL(20,6) -- 사용실적의 사용량
) RETURNS DECIMAL(20,6)
BEGIN
    DECLARE nReturn   DECIMAL(20,6);
    -- DECLARE sum_time  VARCHAR(10);
    DECLARE eng_type  VARCHAR(8);
    DECLARE factor    DECIMAL(20,6);
    DECLARE heat      DECIMAL(20,6);
    DECLARE tot_heat  DECIMAL(20,6);
    DECLARE co2_cef   DECIMAL(20,6);
    DECLARE ch4_cef   DECIMAL(20,6);
    DECLARE n2O_cef   DECIMAL(20,6);
    DECLARE toe_cef	  DECIMAL(20,6);
    DECLARE co2_gwp   INT;
    DECLARE ch4_gwp   INT;
    DECLARE n2O_gwp   INT;
    DECLARE data_load  CHAR(1);
    DECLARE date_part  INT;
    DECLARE value_cost DECIMAL(20,6);
    
    -- 일자 계산
    -- IF ( IN_ENG_TYPE IN ('CO2', 'GJ', 'TOE') ) THEN
    --     set sum_time = substring(IN_DATE, 1, 6);	-- YYYYMM
    -- ELSE
    --     set sum_time = substring(IN_DATE, 1, 10);	-- YYYYMMDDHH
    -- END IF;

	-- insert into T_DEBUG_LOG (MSG) VALUE ( concat("IN_ENG_TYPE=",IN_ENG_TYPE ) );
	-- insert into T_DEBUG_LOG (MSG) VALUE ( concat("USE_VALUE=",USE_VALUE ) );
    
    -- 구분에 따른 계산
    IF (IN_ENG_TYPE='CO2') THEN -- 온실가스
		-- *******************************************************************************************************************
		-- 온실가스 배출량 계산
		SELECT 
			B.S_EMT_CD, -- 배출구분 1=고정연소,2=이동연소,3=간접-전기,4=간접-스팀,5=간접-온수,6=공정,9=기타
			ifnull(B.N_FACTOR, 1),
			ifnull(A.N_HEAT, 0), -- 순발열량
			ifnull(A.N_CO2_CEF, 0), -- CO2 배출계수
			ifnull(A.N_CH4_CEF, 0), -- 
			ifnull(A.N_N2O_CEF, 0)
		INTO
			eng_type 
			,factor   
			,heat 
			,co2_cef  
			,ch4_cef  
			,n2o_cef  
		FROM T_CEF_MOD A -- 배출계수
        LEFT OUTER JOIN T_ENG_CEF B  -- 배출원 관리
			ON B.ID_ENG_CEF = A.ID_ENG_CEF
        WHERE
			A.C_DEL_YN != 'Y'
            AND B.C_DEL_YN != 'Y'
            AND A.ID_ENG_CEF = IN_ID_ENG_CEF
            AND A.C_YEAR = (
						SELECT MAX(C_YEAR)
                        FROM T_CEF_MOD
                        WHERE C_DEL_YN != 'Y'
							and ID_ENG_CEF = IN_ID_ENG_CEF
							and C_YEAR <= date_format(now(),'%Y')
						);
		set co2_gwp = 1; -- 온난화 지수값
		set ch4_gwp = 21; -- 
		set n2o_gwp = 310; -- 
	
		-- insert into T_DEBUG_LOG (MSG) VALUE ( concat("factor:",factor," heat:",heat ," co2_cef:",co2_cef  ,"ch4_cef :",ch4_cef  ," n2o_cef:",n2o_cef  ) );
    
		-- insert into T_DEBUG_LOG (MSG) VALUE ( concat("eng_type:",eng_type ) );
        
		IF eng_type='3' THEN -- 1=간접전기
			set nReturn = USE_VALUE*(co2_cef*co2_gwp+ch4_cef*0.001*ch4_gwp+n2o_cef*0.001*n2o_gwp); -- KgCO2 단위
		ELSE -- 이외 동일
			set nReturn = USE_VALUE*heat*co2_cef;   
			-- insert into T_DEBUG_LOG (MSG) VALUE ( concat("USE_VALUE*heat*co2_cef=",USE_VALUE*heat*co2_cef) );
			set nReturn = nReturn / 1000000; -- KgCO2 단위
		END IF;
		
        -- set nReturn = nReturn / 1000000; -- KgCO2 단위
        set nReturn = nReturn / 1000; -- tCO2 단위
        
    ELSEIF (IN_ENG_TYPE='GJ') THEN
		-- *******************************************************************************************************************
		-- TJ 계산
		SELECT -- A.ID_ENG_CEF,
			ifnull(B.N_FACTOR, 1)
			,ifnull(A.N_TOT_HEAT, 0)
		INTO
			factor  
			,tot_heat
		FROM T_CEF_MOD A -- 배출계수
		LEFT OUTER JOIN T_ENG_CEF B  -- 배출원 관리
			ON B.ID_ENG_CEF = A.ID_ENG_CEF
		WHERE
			A.C_DEL_YN != 'Y'
            AND B.C_DEL_YN != 'Y'
			AND A.ID_ENG_CEF = IN_ID_ENG_CEF
            AND A.C_YEAR = (
						SELECT MAX(C_YEAR)
						FROM T_CEF_MOD
						WHERE C_DEL_YN != 'Y'
							and ID_ENG_CEF = IN_ID_ENG_CEF
                            and C_YEAR <= date_format(now(),'%Y')
						);
                   
		set nReturn = USE_VALUE*factor*tot_heat*0.001; -- GJ 단위
        -- set nReturn = nReturn * 1000; -- MJ 단위로 변환
        
    ELSEIF (IN_ENG_TYPE='TOE') THEN
		-- *******************************************************************************************************************
		-- TOE 계산
		SELECT -- A.ID_ENG_CEF,
			ifnull(B.N_FACTOR, 1)
			,ifnull(A.N_TOT_HEAT, 0)
            ,ifnull(A.N_TOE_CEF, 0)
		INTO
			factor  
			,tot_heat
            ,toe_cef
		FROM T_CEF_MOD A -- 배출계수
		LEFT OUTER JOIN T_ENG_CEF B  -- 배출원 관리
		ON B.ID_ENG_CEF = A.ID_ENG_CEF
		WHERE
			A.C_DEL_YN != 'Y'
            AND B.C_DEL_YN != 'Y'
            AND A.ID_ENG_CEF = IN_ID_ENG_CEF
			AND A.C_YEAR = (
						SELECT MAX(C_YEAR)
						FROM T_CEF_MOD
						WHERE C_DEL_YN != 'Y'
							AND ID_ENG_CEF = IN_ID_ENG_CEF
                            AND C_YEAR <= date_format(now(),'%Y')
						);
                        
		set nReturn = USE_VALUE*toe_cef*0.001;	-- TOE 단위
        -- set nReturn = nReturn / 1000;	-- KgOE로 변환
        -- set nReturn = nReturn / 1000;	-- TOE로 변환
    END IF;

	-- insert into T_DEBUG_LOG (MSG) VALUE ( concat("nReturn:",nReturn ) );

    RETURN nReturn; -- 결과값 리턴

END $$
delimiter ;


-- ======================

select * from T_CEF_MOD 
where ID_ENG_CEF=11
;
SELECT -- A.ID_ENG_CEF,
    A.C_YEAR
	,ifnull(B.N_FACTOR, 1)
	,ifnull(A.N_TOT_HEAT, 0)
-- INTO
-- 	factor  
-- 	,tot_heat
FROM T_CEF_MOD A -- 배출계수
LEFT OUTER JOIN T_ENG_CEF B  -- 배출원 관리
	ON B.ID_ENG_CEF = A.ID_ENG_CEF
WHERE
   A.ID_ENG_CEF = 11 -- IN_ID_ENG_CEF
    AND A.C_YEAR = (SELECT MAX(C_YEAR)
                 FROM T_CEF_MOD
                 WHERE ID_ENG_CEF = A.ID_ENG_CEF and
                 C_YEAR <= date_format(now(),'%Y'));    
                 
                 
-- ====

delimiter $$
drop function FN_ENG_CONVERSION_TEST;
CREATE FUNCTION `FN_ENG_CONVERSION_TEST`(
    IN_ENG_TYPE VARCHAR(255) -- 구분: 'CO2','GJ','TOE'
    , IN_ID_ENG_CEF VARCHAR(255) -- 배출원ID
    , IN_DATE VARCHAR(255) -- YYYYMMDD
    , USE_VALUE DECIMAL(20,5) -- 사용실적의 사용량
) RETURNS DECIMAL(20,5)
BEGIN
    DECLARE nReturn   DECIMAL(20,5);
    -- DECLARE sum_time  VARCHAR(10);
    DECLARE eng_type  VARCHAR(8);
    DECLARE factor    DECIMAL(20,5);
    DECLARE heat      DECIMAL(20,5);
    DECLARE tot_heat  DECIMAL(20,5);
    DECLARE co2_cef   DECIMAL(20,5);
    DECLARE ch4_cef   DECIMAL(20,5);
    DECLARE n2O_cef   DECIMAL(20,5);
    DECLARE co2_gwp   INT;
    DECLARE ch4_gwp   INT;
    DECLARE n2O_gwp   INT;
    DECLARE data_load  CHAR(1);
    DECLARE date_part  INT;
    DECLARE value_cost DECIMAL(20,5);
    
    -- 일자 계산
    -- IF ( IN_ENG_TYPE IN ('CO2', 'GJ', 'TOE') ) THEN
    --     set sum_time = substring(IN_DATE, 1, 6);	-- YYYYMM
    -- ELSE
    --     set sum_time = substring(IN_DATE, 1, 10);	-- YYYYMMDDHH
    -- END IF;

	insert into T_DEBUG_LOG (MSG) VALUE ( concat("IN_ENG_TYPE=",IN_ENG_TYPE ) );
	insert into T_DEBUG_LOG (MSG) VALUE ( concat("USE_VALUE=",USE_VALUE ) );
    
    -- 구분에 따른 계산
    IF (IN_ENG_TYPE='CO2') THEN -- 온실가스
		-- *******************************************************************************************************************
		-- 온실가스 배출량 계산
		SELECT 
			B.S_EMT_CD, -- 배출구분 1=고정연소,2=이동연소,3=간접-전기,4=간접-스팀,5=간접-온수,6=공정,9=기타
			ifnull(B.N_FACTOR, 1),
			ifnull(A.N_HEAT, 0), -- 순발열량
			ifnull(A.N_CO2_CEF, 0), -- CO2 배출계수
			ifnull(A.N_CH4_CEF, 0), -- 
			ifnull(A.N_N2O_CEF, 0)
		INTO
			eng_type 
			,factor   
			,heat 
			,co2_cef  
			,ch4_cef  
			,n2o_cef  
		FROM T_CEF_MOD A -- 배출계수
        LEFT OUTER JOIN T_ENG_CEF B  -- 배출원 관리
			ON B.ID_ENG_CEF = A.ID_ENG_CEF
        WHERE
            A.ID_ENG_CEF = IN_ID_ENG_CEF
            AND A.C_YEAR = (SELECT MAX(C_YEAR)
                         FROM T_CEF_MOD
                         WHERE ID_ENG_CEF = A.ID_ENG_CEF and
                         C_YEAR <= date_format(now(),'%Y'));    
		set co2_gwp = 1; -- 온난화 지수값
		set ch4_gwp = 21; -- 
		set n2o_gwp = 310; -- 
	
		insert into T_DEBUG_LOG (MSG) VALUE ( concat("factor=",factor," heat=",heat ," co2_cef=",co2_cef  ," ch4_cef=",ch4_cef  ," n2o_cef=",n2o_cef  ) );
    
		insert into T_DEBUG_LOG (MSG) VALUE ( concat("eng_type=",eng_type ) );
        
		IF eng_type='3' THEN -- 1=간접전기
			set nReturn = USE_VALUE*(co2_cef*co2_gwp+ch4_cef*0.001*ch4_gwp+n2o_cef*0.001*n2o_gwp); 
            insert into T_DEBUG_LOG (MSG) VALUE ( concat("USE_VALUE*(co2_cef*co2_gwp+ch4_cef*0.001*ch4_gwp+n2o_cef*0.001*n2o_gwp)=",USE_VALUE*(co2_cef*co2_gwp+ch4_cef*0.001*ch4_gwp+n2o_cef*0.001*n2o_gwp)) );
		ELSE -- 이외 동일
			set nReturn = USE_VALUE*heat*co2_cef;   
			insert into T_DEBUG_LOG (MSG) VALUE ( concat("USE_VALUE*heat*co2_cef=",USE_VALUE*heat*co2_cef) );
		END IF;
		
    ELSEIF (IN_ENG_TYPE='GJ') THEN
		-- *******************************************************************************************************************
		-- TJ 계산
		SELECT -- A.ID_ENG_CEF,
			ifnull(B.N_FACTOR, 1)
			,ifnull(A.N_TOT_HEAT, 0)
		INTO
			factor  
			,tot_heat
		FROM T_CEF_MOD A -- 배출계수
		LEFT OUTER JOIN T_ENG_CEF B  -- 배출원 관리
			ON B.ID_ENG_CEF = A.ID_ENG_CEF
		WHERE
           A.ID_ENG_CEF = IN_ID_ENG_CEF
            AND A.C_YEAR = (SELECT MAX(C_YEAR)
                         FROM T_CEF_MOD
                         WHERE ID_ENG_CEF = A.ID_ENG_CEF and
                         C_YEAR <= date_format(now(),'%Y'));       

		insert into T_DEBUG_LOG (MSG) VALUE ( concat("IN_ENG_TYPE=",IN_ENG_TYPE," factor=",factor ," tot_heat=",tot_heat ) );
                                      
		set nReturn=USE_VALUE*factor*tot_heat*0.001;

		insert into T_DEBUG_LOG (MSG) VALUE ( concat("USE_VALUE*factor*tot_heat*0.001=",USE_VALUE*factor*tot_heat*0.001) );

    ELSEIF (IN_ENG_TYPE='TOE') THEN
		-- *******************************************************************************************************************
		-- TOE 계산
		SELECT -- A.ID_ENG_CEF,
			ifnull(B.N_FACTOR, 1)
			,ifnull(A.N_TOT_HEAT, 0)
		INTO
			factor  
			,tot_heat
		FROM T_CEF_MOD A -- 배출계수
		LEFT OUTER JOIN T_ENG_CEF B  -- 배출원 관리
		ON B.ID_ENG_CEF = A.ID_ENG_CEF
		WHERE
			A.ID_ENG_CEF = IN_ID_ENG_CEF
				AND A.C_YEAR = (SELECT MAX(C_YEAR)
							FROM T_CEF_MOD
							WHERE ID_ENG_CEF = A.ID_ENG_CEF and
                            C_YEAR <= date_format(now(),'%Y')); 

		insert into T_DEBUG_LOG (MSG) VALUE ( concat("IN_ENG_TYPE=",IN_ENG_TYPE," factor=",factor ," tot_heat=",tot_heat ) );
                        
		set nReturn=USE_VALUE*factor*tot_heat*23.88;
        
     		insert into T_DEBUG_LOG (MSG) VALUE ( concat("USE_VALUE*factor*tot_heat*23.88=",USE_VALUE*factor*tot_heat*23.88) );

    END IF;

	-- insert into T_DEBUG_LOG (MSG) VALUE ( concat("nReturn:",nReturn ) );

    RETURN nReturn; -- 결과값 리턴

END $$
delimiter ;



-- =====================
-- TEST
select * from T_ENG_CEF;
-- ID_ENG_CEF=10,전기

select FN_ENG_CONVERSION('CO2',10,'',100) KgCO2;
set @ID_ENG_CEF=1;
set @VAL=300;
select FN_ENG_CONVERSION('CO2',@ID_ENG_CEF,'',@VAL) tCO2, FN_ENG_CONVERSION('GJ',@ID_ENG_CEF,'',@VAL) GJ, FN_ENG_CONVERSION('TOE',@ID_ENG_CEF,'',@VAL) TOE;

select FN_ENG_CONVERSION('CO2',@ID_ENG_CEF,'',@VAL) tCO2;
select FN_ENG_CONVERSION('GJ',@ID_ENG_CEF,'',@VAL) GJ;
select FN_ENG_CONVERSION('TOE',@ID_ENG_CEF,'',@VAL) TOE;

select FN_ENG_CONVERSION('TOE',  1, DATE_FORMAT(now(), '%Y%m%d'), 300) -- 전기
;
-- select ;

select FN_ENG_CONVERSION('CO2',6,'',1000) tCO2, FN_ENG_CONVERSION('GJ',6,'',1000) GJ, FN_ENG_CONVERSION('TOE',6,'',1000) TOE;