/*
	SP_MAKE_LOGIS_TRANS_YEAR:
    
	온실가스/에너지현황용 data 생성
	
    T_ENG_GHG_AMT의 데이터를
    
    T_LOGIS_TRANS_YEAR_VHCL		-- 차량 온실가스 에너지 현황
    T_LOGIS_TRANS_YEAR_TRAIN	-- 철송 온실가스 에너지 현황
    T_LOGIS_TRANS_YEAR_SHIP		-- 해송 온실가스 에너지 현황
    T_LOGIS_TRANS_YEAR_FACIL	-- 고정체(시설) 에너지 현황
	안에 시설분류에 따라 연도, 사업장별, ...별로 적재함
    
    2019-12-13 kdh.
    
*/
SELECT * FROM SP_MAKE_LOGIS_TRANS_YEAR;
call SP_MAKE_LOGIS_TRANS_YEAR('201802');

DELIMITER $$
drop procedure SP_MAKE_LOGIS_TRANS_YEAR;
create procedure SP_MAKE_LOGIS_TRANS_YEAR(
  IN IN_YYYYMM VARCHAR(255)
) 
BEGIN
	declare YYYY varchar(4);
    declare MM varchar(2);
    declare YYYYMM varchar(6);
    
    -- 어제 기준의 Year Month를 구한다.
	set YYYY=substring(IN_YYYYMM,1,4);
	set MM=substring(IN_YYYYMM,5,2);
    set @M=CONVERT(MM, INT);
    
	if IN_YYYYMM is null
	then
		set YYYY=DATE_FORMAT( DATE_ADD(now(), INTERVAL -1 DAY)/*yesterday*/,'%Y');
		set MM=DATE_FORMAT( DATE_ADD(now(), INTERVAL -1 DAY)/*yesterday*/,'%m');
		set @M=CONVERT(MM, INT);
        set YYYYMM=DATE_FORMAT( DATE_ADD(now(), INTERVAL -1 DAY)/*yesterday*/,'%Y%m');
	end if;

	set YYYYMM= concat(YYYY,MM);
	SELECT  YYYYMM,YYYY,MM;

	-- logging
	INSERT INTO T_SCHEDULER_LOG(SP_NM) VALUES ( concat('SP_MAKE_LOGIS_TRANS_YEAR:',YYYY,',',MM));
    
	-- data insert/update
    
    -- 차량 온실가스 에너지 현황
    -- T_LOGIS_TRANS_YEAR_VHCL
    INSERT INTO T_LOGIS_TRANS_YEAR_VHCL (
		C_YEAR, TRANS_TYPE_CD, TRANS_TYPE_NM, ID_SITE, S_SITE_NM, S_LINE_CD, S_SELFVRN_CD, LOGI_YCT_NM, S_LOGI_WEI_CD, LOGI_WEI_NM, S_CAR_TYPE, CAR_TYPE_NM
        , LOADAGE_SUM, FARE_SUM, VACANT_DISTANCE_SUM, DISTANCE_SUM, MJ_SUM, GCO2_SUM, ENG_VOL_SUM, TOE_SUM, TON_KM_SUM
    )
    SELECT *
    FROM (
		SELECT
			substring(TMI.DATE_MON,1,4) as C_YEAR -- 연
			, TMI.TRANS_TYPE_CD -- 운송수단
			,(SELECT S_DESC FROM T_CODE WHERE S_CAT='TRANS_TYPE' and S_CD=TMI.TRANS_TYPE_CD ) TRANS_TYPE_NM -- 운송수단 한글명
			,V.ID_SITE -- 사업장ID
			,V.S_SITE_NM -- 사업장명
			,V.S_LINE_CD -- 노선구분
			,V.S_SELFVRN_CD -- 자차여부
			,V.LOGI_YCT_NM -- 노선구분 한글명
			,V.S_LOGI_WEI_CD -- 톤수
			,V.LOGI_WEI_NM -- 톤수 한글명
			,V.S_CAR_TYPE -- 차종
			,(SELECT S_DESC FROM T_CODE WHERE S_CAT='CAR_TYPE' and S_CD=V.S_CAR_TYPE ) CAR_TYPE_NM -- 차종 한글명
			,sum(TMI.LOADAGE_SUM) LOADAGE_SUM -- 적재량 합
			,sum(TMI.FARE_SUM) FARE_SUM -- 운임료 합
			,sum(TMI.VACANT_DISTANCE_SUM) VACANT_DISTANCE_SUM -- 공차운행거리 합
			,sum(TMI.DISTANCE_SUM) DISTANCE_SUM -- 영차운행거리
			,sum(TMI.MJ_SUM) MJ_SUM -- 에너지 사용량(MJ) 합
			,sum(TMI.GCO2_SUM) GCO2_SUM -- 온실가스배출량(gCO2) 합
			,sum(TMI.ENG_VOL_SUM) ENG_VOL_SUM -- 연료사용량 합
			,sum(TMI.TOE_SUM) TOE_SUM -- 환상톤 합
			,sum(TMI.TON_KM_SUM) TON_KM_SUM -- 톤킬로 합
			-- ,TMI.DATE_MON -- 날짜,YYYYMM
		FROM T_LOGIS_TRANS_MON_INFO TMI  -- 월운송정보
		LEFT JOIN V_VEHICLE V       -- 차량 마스터
			ON TMI.ID_VEHICLE = V.ID_VEHICLE -- 차량ID
			-- LEFT JOIN T_SITE_INFO SI    -- 사업장
			--  ON SI.ID_SITE=V.ID_SITE   -- 사업장ID
		WHERE TMI.TRANS_TYPE_CD=1   -- 화물차
			AND TMI.DATE_MON = YYYYMM
		GROUP BY
			V.TRANS_TYPE -- 운송수단
			,V.S_CAR_TYPE -- 차종
			-- ,V.TRANS_TYPE_NM -- 운송수단 한글명
			,V.S_LINE_CD -- 노선구분
			-- ,V.LOGI_YCT_NM -- 노선구분 한글명
			,V.S_LOGI_WEI_CD -- 톤수
			,V.LOGI_WEI_NM -- 톤수 한글명
			,V.ID_SITE -- 사업장ID
			,V.S_SITE_NM -- 사업장명
			-- ,TMI.DATE_MON -- 날짜,YYYYMM
			,substring(TMI.DATE_MON,1,4) -- 연도
    ) T
    ON DUPLICATE KEY UPDATE
		LOADAGE_SUM = T.LOADAGE_SUM
		, FARE_SUM = T.FARE_SUM
		, VACANT_DISTANCE_SUM = T.VACANT_DISTANCE_SUM
		, DISTANCE_SUM = T.DISTANCE_SUM
		, MJ_SUM = T.MJ_SUM
		, GCO2_SUM = T.GCO2_SUM
		, ENG_VOL_SUM = T.ENG_VOL_SUM
		, TOE_SUM = T.TOE_SUM
		, TON_KM_SUM = T.TON_KM_SUM
    ;
    
    -- 철송 온실가스 에너지 현황
    -- T_LOGIS_TRANS_YEAR_TRAIN
    INSERT INTO T_LOGIS_TRANS_YEAR_TRAIN (
		C_YEAR, TRANS_TYPE_CD, TRANS_TYPE_NM, ID_SITE, S_SITE_NM, S_LINE_CD, S_SELFVRN_CD, LOGI_YCT_NM, S_LOGI_WEI_CD, LOGI_WEI_NM, S_CAR_TYPE, CAR_TYPE_NM
        , LOADAGE_SUM, FARE_SUM, VACANT_DISTANCE_SUM, DISTANCE_SUM, MJ_SUM, GCO2_SUM, ENG_VOL_SUM, TOE_SUM, TON_KM_SUM
    )
    SELECT *
    FROM (
		select
			substring(TMI.DATE_MON,1,4) as C_YEAR -- 연
			,TMI.TRANS_TYPE_CD -- 운송수단
			,(SELECT S_DESC FROM T_CODE WHERE S_CAT='TRANS_TYPE' and S_CD=TMI.TRANS_TYPE_CD ) TRANS_TYPE_NM -- 운송수단 한글명
			,SI.ID_SITE -- 사업장ID
			,SI.S_SITE_NM -- 사업장명
			,1 S_LINE_CD -- 노선구분
			,3 S_SELFVRN_CD -- 자차여부
			,'정기' LOGI_YCT_NM -- 노선구분 한글명
			,null S_LOGI_WEI_CD -- 톤수
			,null LOGI_WEI_NM -- 톤수 한글명
			,null S_CAR_TYPE -- 차종
			,null CAR_TYPE_NM -- 차종 한글명
			,sum(TMI.LOADAGE_SUM) LOADAGE_SUM -- 적재량 합
			,sum(TMI.FARE_SUM) FARE_SUM -- 운임료 합
			,sum(TMI.VACANT_DISTANCE_SUM) VACANT_DISTANCE_SUM -- 공차운행거리 합
			,sum(TMI.DISTANCE_SUM) DISTANCE_SUM -- 영차운행거리
			,sum(TMI.MJ_SUM) MJ_SUM -- 에너지 사용량(MJ) 합
			,sum(TMI.GCO2_SUM) GCO2_SUM -- 온실가스배출량(gCO2) 합
			,sum(TMI.ENG_VOL_SUM) ENG_VOL_SUM -- 연료사용량 합
			,sum(TMI.TOE_SUM) TOE_SUM -- 환상톤 합
			,sum(TMI.TON_KM_SUM) TON_KM_SUM -- 톤킬로 합
			-- ,TMI.DATE_MON -- 날짜,YYYYMM
		FROM T_LOGIS_TRANS_MON_INFO TMI
		left join T_ENG_POINT EP
			ON TMI.ID_ENG_POINT=EP.ID_ENG_POINT
		left join V_SITE_DEPT SI
			ON EP.ID_SITE=SI.ID_SITE
		where TRANS_TYPE_CD=3
			AND TMI.DATE_MON = YYYYMM
		GROUP BY
			TMI.TRANS_TYPE_CD -- 운송수단
			,SI.ID_SITE -- 사업장ID
			,substring(TMI.DATE_MON,1,4) -- 연도
    ) T
    ON DUPLICATE KEY UPDATE
		LOADAGE_SUM = T.LOADAGE_SUM
		, FARE_SUM = T.FARE_SUM
		, VACANT_DISTANCE_SUM = T.VACANT_DISTANCE_SUM
		, DISTANCE_SUM = T.DISTANCE_SUM
		, MJ_SUM = T.MJ_SUM
		, GCO2_SUM = T.GCO2_SUM
		, ENG_VOL_SUM = T.ENG_VOL_SUM
		, TOE_SUM = T.TOE_SUM
		, TON_KM_SUM = T.TON_KM_SUM
	;
    
    
    -- 해송 온실가스 에너지 현황
    -- T_LOGIS_TRANS_YEAR_SHIP
    INSERT INTO T_LOGIS_TRANS_YEAR_SHIP (
		C_YEAR, TRANS_TYPE_CD, TRANS_TYPE_NM, ID_SITE, S_SITE_NM, S_LINE_CD, S_SELFVRN_CD, LOGI_YCT_NM, S_LOGI_WEI_CD, LOGI_WEI_NM, S_CAR_TYPE, CAR_TYPE_NM
        , LOADAGE_SUM, FARE_SUM, VACANT_DISTANCE_SUM, DISTANCE_SUM, MJ_SUM, GCO2_SUM, ENG_VOL_SUM, TOE_SUM, TON_KM_SUM
    )
    SELECT *
    FROM (
		select
			substring(TMI.DATE_MON,1,4) YEAR -- 연
			,TMI.TRANS_TYPE_CD -- 운송수단
			,(SELECT S_DESC FROM T_CODE WHERE S_CAT='TRANS_TYPE' and S_CD=TMI.TRANS_TYPE_CD ) TRANS_TYPE_NM -- 운송수단 한글명
			,SI.ID_SITE -- 사업장ID
			,SI.S_SITE_NM -- 사업장명
			,2 S_LINE_CD -- 노선구분
			,3 S_SELFVRN_CD -- 자차여부
			,'비정기' LOGI_YCT_NM -- 노선구분 한글명
			,null S_LOGI_WEI_CD -- 톤수
			,null LOGI_WEI_NM -- 톤수 한글명
			,null S_CAR_TYPE -- 차종
			,null CAR_TYPE_NM -- 차종 한글명
			,sum(TMI.LOADAGE_SUM) LOADAGE_SUM -- 적재량 합
			,sum(TMI.FARE_SUM) FARE_SUM -- 운임료 합
			,sum(TMI.VACANT_DISTANCE_SUM) VACANT_DISTANCE_SUM -- 공차운행거리 합
			,sum(TMI.DISTANCE_SUM) DISTANCE_SUM -- 영차운행거리
			,sum(TMI.MJ_SUM) MJ_SUM -- 에너지 사용량(MJ) 합
			,sum(TMI.GCO2_SUM) GCO2_SUM -- 온실가스배출량(gCO2) 합
			,sum(TMI.ENG_VOL_SUM) ENG_VOL_SUM -- 연료사용량 합
			,sum(TMI.TOE_SUM) TOE_SUM -- 환상톤 합
			,sum(TMI.TON_KM_SUM) TON_KM_SUM -- 톤킬로 합
			-- ,TMI.DATE_MON -- 날짜,YYYYMM
		FROM T_LOGIS_TRANS_MON_INFO TMI
		left join T_ENG_POINT EP
			ON TMI.ID_ENG_POINT=EP.ID_ENG_POINT
		left join V_SITE_DEPT SI
			ON EP.ID_SITE=SI.ID_SITE
		where TMI.TRANS_TYPE_CD=2   -- 해송
			AND TMI.DATE_MON = YYYYMM
		GROUP BY
			TMI.TRANS_TYPE_CD -- 운송수단
			, SI.ID_SITE -- 사업장ID
			, SI.S_SITE_NM -- 사업장명
			, substring(TMI.DATE_MON,1,4) -- 연도
    ) T
    ON DUPLICATE KEY UPDATE
		LOADAGE_SUM = T.LOADAGE_SUM
		, FARE_SUM = T.FARE_SUM
		, VACANT_DISTANCE_SUM = T.VACANT_DISTANCE_SUM
		, DISTANCE_SUM = T.DISTANCE_SUM
		, MJ_SUM = T.MJ_SUM
		, GCO2_SUM = T.GCO2_SUM
		, ENG_VOL_SUM = T.ENG_VOL_SUM
		, TOE_SUM = T.TOE_SUM
		, TON_KM_SUM = T.TON_KM_SUM
	;
    
    
    -- 고정체(시설) 에너지 현황
    -- T_LOGIS_TRANS_YEAR_FACIL
	INSERT INTO T_LOGIS_TRANS_YEAR_FACIL (
		C_YEAR, TRANS_TYPE_CD, TRANS_TYPE_NM, ID_SITE, S_SITE_NM, S_LINE_CD, S_SELFVRN_CD, LOGI_YCT_NM, S_LOGI_WEI_CD, LOGI_WEI_NM, S_CAR_TYPE, CAR_TYPE_NM
        , LOADAGE_SUM, FARE_SUM, VACANT_DISTANCE_SUM, DISTANCE_SUM, MJ_SUM, GCO2_SUM, ENG_VOL_SUM, TOE_SUM, TON_KM_SUM
    )
    SELECT *
    FROM (
		SELECT 
			EGA.C_YEAR -- 연
			,0 TRANS_TYPE_CD -- 운송수단
			,'기타' TRANS_TYPE_NM -- 운송수단 한글명
			,SI.ID_SITE -- 사업장ID
			,SI.S_SITE_NM -- 사업장명
			,null S_LINE_CD -- 노선구분
			,3 S_SELFVRN_CD -- 자차여부
			,null LOGI_YCT_NM -- 노선구분 한글명
			,null S_LOGI_WEI_CD -- 톤수
			,null LOGI_WEI_NM -- 톤수 한글명
			,null S_CAR_TYPE -- 차종
			,null CAR_TYPE_NM -- 차종 한글명
			,0 LOADAGE_SUM -- 적재량 합
			,0 FARE_SUM -- 운임료 합
			,0 VACANT_DISTANCE_SUM -- 공차운행거리 합
			,0 DISTANCE_SUM -- 영차운행거리
			,sum(EGA.N_GJ) MJ_SUM -- 에너지 사용량(MJ) 합
			,sum(EGA.N_CO2) GCO2_SUM -- 온실가스배출량(gCO2) 합
			,0 ENG_VOL_SUM -- 연료사용량 합
			,sum(EGA.N_TOE) TOE_SUM -- 환상톤 합
			,0 TON_KM_SUM -- 톤킬로 합

		FROM T_ENG_POINT EP
		LEFT JOIN V_SITE_DEPT SI
			ON EP.ID_SITE=SI.ID_SITE
			and EP.S_LOGI_TYPE_CD='1'
			and EP.C_DEL_YN='N'
			and EP.C_POINT_YN='Y'
			and S_INV_CD in (1,6)
		LEFT JOIN T_ENG_GHG_AMT EGA
			ON EP.ID_ENG_POINT = EGA.ID_ENG_POINT
			and EP.C_DEL_YN='N'
		WHERE SI.ID_SITE is not null
			AND EGA.C_DATE_MON = YYYYMM
		GROUP BY
			SI.ID_SITE -- 사업장ID
			, EGA.C_YEAR -- 연도
    ) T
    ON DUPLICATE KEY UPDATE
		LOADAGE_SUM = T.LOADAGE_SUM
		, FARE_SUM = T.FARE_SUM
		, VACANT_DISTANCE_SUM = T.VACANT_DISTANCE_SUM
		, DISTANCE_SUM = T.DISTANCE_SUM
		, MJ_SUM = T.MJ_SUM
		, GCO2_SUM = T.GCO2_SUM
		, ENG_VOL_SUM = T.ENG_VOL_SUM
		, TOE_SUM = T.TOE_SUM
		, TON_KM_SUM = T.TON_KM_SUM
	;

END $$
DELIMITER ;

