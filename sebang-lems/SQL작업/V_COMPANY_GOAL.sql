/*
	V_COMPANY_GOAL

*/
-- alter 
-- create
-- VIEW V_COMPANY_GOAL AS
SELECT
	C_YEAR -- 연도
	, ID_SITE -- ID사업장
	, S_SITE_ID -- 사업장ID
	, S_SITE_NM -- 사업장명
	, ID_ENG_POINT -- ID  배출시설
	, S_POINT_NM -- 배출시설이름
	, ID_ENG_CEF -- ID 배출원관리
	, S_CEF_CD -- 배출구분, T_CODE에 CEF 참조
	, (select S_DESC from T_CODE where S_CAT='CEF' and S_CD = T.S_CEF_CD) CEF_NM -- 배출구분 이름
	, S_ENG_CD -- 사용 에너지원, T_CODE에 ENG 참조
	, (select S_DESC from T_CODE where S_CAT='ENG' and S_CD = T.S_ENG_CD) ENG_NM -- 사용 에너지원 이름 
	, S_MRVENG_CD -- 정부보고 에너지원, T_CODE에 MRVENG 참조
	, (select S_DESC from T_CODE where S_CAT='MRVENG' and S_CD = T.S_MRVENG_CD) MRVENG_NM -- 정부보고 에너지원 이
	, S_EMT_CD -- 배출원 구분, T_CODE에 EMT 참조
	, (select S_DESC from T_CODE where S_CAT='EMT' and S_CD = T.S_EMT_CD) EMT_NM -- 배출원 구분
	, S_IN_UNT_CD -- 입력단위, T_CODE에 UNT 참조
	, (select S_DESC from T_CODE where S_CAT='UNT' and S_CD = T.S_IN_UNT_CD) IN_UNT_NM -- 입력단위
	, S_CALC_UNT_CD -- 계산단위, T_CODE에 UNT 참조
	, (select S_DESC from T_CODE where S_CAT='UNT' and S_CD = T.S_CALC_UNT_CD) CALC_UNT_NM -- 계산단위
    
	, IFNULL(N_YEAR_ENG_GOAL, 0) as N_YEAR_ENG_GOAL -- 목표에너지,J
	, IFNULL(N_YEAR_GAS_GOAL, 0) as N_YEAR_GAS_GOAL -- 목표가스,CO2
	, IFNULL(N_GAS_GOAL_SUM, 0) as N_GAS_GOAL_SUM  -- 월별목표 합 가스
	, IFNULL(N_ENG_GOAL_SUM, 0) as N_ENG_GOAL_SUM  -- 월별목표 합 에너지
	, T.N_FACTOR -- 단위환산계수
	, T.N_OXY_VAL -- 산화율
	, T.S_EMI_CD -- 배출활동구분, T_CODE에 EMI 참조.목표관리용 배출활동 구분
	, T.C_TIER -- 계산식Tier
	, T.C_ACTV_TIER -- 활동도Tier
	, T.C_HEAT_TIER -- 발열량Tier
	, T.C_CEF_TIER -- 배출계수Tier
	, T.C_OXY_TIER -- 산화율Tier
	, T.ID_SITE_MON_SUM_GOAL -- 월별 사업장 목표 합 테이블의 row id
	, (select N_MON_ENG_GOAL from T_SITE_MON_GOAL M where M.ID_SITE_MON_SUM_GOAL=T.ID_SITE_MON_SUM_GOAL and M.C_MON=1) as M1	-- select * from T_SITE_MON_GOAL
	, (select N_MON_ENG_GOAL from T_SITE_MON_GOAL M where M.ID_SITE_MON_SUM_GOAL=T.ID_SITE_MON_SUM_GOAL and M.C_MON=2) as M2
	, (select N_MON_ENG_GOAL from T_SITE_MON_GOAL M where M.ID_SITE_MON_SUM_GOAL=T.ID_SITE_MON_SUM_GOAL and M.C_MON=3) as M3
	, (select N_MON_ENG_GOAL from T_SITE_MON_GOAL M where M.ID_SITE_MON_SUM_GOAL=T.ID_SITE_MON_SUM_GOAL and M.C_MON=4) as M4
	, (select N_MON_ENG_GOAL from T_SITE_MON_GOAL M where M.ID_SITE_MON_SUM_GOAL=T.ID_SITE_MON_SUM_GOAL and M.C_MON=5) as M5
	, (select N_MON_ENG_GOAL from T_SITE_MON_GOAL M where M.ID_SITE_MON_SUM_GOAL=T.ID_SITE_MON_SUM_GOAL and M.C_MON=6) as M6
	, (select N_MON_ENG_GOAL from T_SITE_MON_GOAL M where M.ID_SITE_MON_SUM_GOAL=T.ID_SITE_MON_SUM_GOAL and M.C_MON=7) as M7
	, (select N_MON_ENG_GOAL from T_SITE_MON_GOAL M where M.ID_SITE_MON_SUM_GOAL=T.ID_SITE_MON_SUM_GOAL and M.C_MON=8) as M8
	, (select N_MON_ENG_GOAL from T_SITE_MON_GOAL M where M.ID_SITE_MON_SUM_GOAL=T.ID_SITE_MON_SUM_GOAL and M.C_MON=9) as M9
	, (select N_MON_ENG_GOAL from T_SITE_MON_GOAL M where M.ID_SITE_MON_SUM_GOAL=T.ID_SITE_MON_SUM_GOAL and M.C_MON=10) as M10
	, (select N_MON_ENG_GOAL from T_SITE_MON_GOAL M where M.ID_SITE_MON_SUM_GOAL=T.ID_SITE_MON_SUM_GOAL and M.C_MON=11) as M11
	, (select N_MON_ENG_GOAL from T_SITE_MON_GOAL M where M.ID_SITE_MON_SUM_GOAL=T.ID_SITE_MON_SUM_GOAL and M.C_MON=12) as M12
FROM (
	select YM.YYYY as C_YEAR -- 연도
		, SI.ID_SITE -- ID사업장
		, SI.S_SITE_ID -- 사업장ID
		, SI.S_SITE_NM -- 사업장명
		, YG.N_YEAR_ENG_GOAL -- 목표에너지,J
		, YG.N_YEAR_GAS_GOAL -- 목표가스,CO2
		, YG.N_GAS_GOAL_SUM -- 월별목표 합 가스
		, YG.N_ENG_GOAL_SUM  -- 월별목표 합 에너지
		, EP.ID_ENG_POINT -- ID  배출시설
		, EP.S_POINT_NM -- 배출시설 이름
		-- ,EP.ID_ENG_POINT EP_ID_ENG_POINT -- 배출시설ID,SEQ.AI
		, EP.ID_ENG_CEF -- ID 배출원관리
		-- ,EC.ID_ENG_CEF EC_ID_ENG_CEF -- ID 배출원관리

		, EC.S_CEF_CD -- 배출구분, T_CODE에 CEF 참조
		, EC.S_ENG_CD -- 사용 에너지원, T_CODE에 ENG 참조
		, EC.S_MRVENG_CD -- 정부보고 에너지원, T_CODE에 MRVENG 참조
		, EC.S_EMT_CD -- 배출원 구분, T_CODE에 EMT 참조
		, EC.S_IN_UNT_CD -- 입력단위, T_CODE에 UNT 참조
		, EC.S_CALC_UNT_CD -- 계산단위, T_CODE에 UNT 참조
		, EC.N_FACTOR -- 단위환산계수
		, EC.N_OXY_VAL -- 산화율
		, EC.S_EMI_CD -- 배출활동구분, T_CODE에 EMI 참조.목표관리용 배출활동 구분
		, EC.C_TIER -- 계산식Tier
		, EC.C_ACTV_TIER -- 활동도Tier
		, EC.C_HEAT_TIER -- 발열량Tier
		, EC.C_CEF_TIER -- 배출계수Tier
		, EC.C_OXY_TIER -- 산화율Tier

		, SMSG.ID_SITE_MON_SUM_GOAL -- 월별 사업장 목표 합 테이블의 row id

	from  T_SITE_INFO as SI -- 사업장 -- select * from T_SITE_INFO;
	CROSS JOIN (
		SELECT DISTINCT YYYY FROM V_YEAR_MONTH   -- 연도만 필요하기 때문에 DISTINCT 해줌
	) as YM
	LEFT JOIN T_ENG_POINT as EP -- 배출시설
		ON SI.ID_SITE = EP.ID_SITE
	LEFT JOIN T_SITE_YEAR_GOAL as YG -- 년간사업장 목표   -- select * from T_SITE_YEAR_GOAL
		ON YG.ID_SITE = EP.ID_SITE AND YM.YYYY = YG.C_YEAR
	LEFT JOIN T_SITE_MON_SUM_GOAL as SMSG  -- 월별 배출원 목표 합(구,월별 사업장 목표 합)
		ON YG.ID_SITE_YEAR_GOAL = SMSG.ID_SITE_YEAR_GOAL and  SMSG.ID_ENG_POINT=EP.ID_ENG_POINT
	LEFT JOIN T_ENG_CEF as EC -- 배출원 관리
		ON EP.ID_ENG_CEF = EC.ID_ENG_CEF
	where SI.C_DEL_YN != 'Y' and EP.C_DEL_YN != 'Y'
) as T
# LEFT JOIN T_SITE_MON_GOAL as MG
# ON MG.ID_SITE_MON_SUM_GOAL = T.ID_SITE_MON_SUM_GOAL
# GROUP BY T.ID_SITE_MON_SUM_GOAL
ORDER BY T.C_YEAR, T.ID_SITE, T.ID_ENG_POINT
;

-- ex)
SELECT * FROM V_COMPANY_GOAL 
;
SELECT * FROM V_COMPANY_GOAL WHERE C_YEAR = '2019'
;

select * from T_ENG_POINT where ID_ENG_POINT in (114,115,116);
-- =======================================
