# SET SQL_SAFE_UPDATES = 0;		# Error Code: 1175 의 경우 설정
call SP_LEMS_DAILY_BATCH('201801');
call SP_LEMS_DAILY_BATCH(null);

select * from T_SCHEDULER_LOG order by SEQ DESC;


DELIMITER $$
drop procedure SP_LEMS_DAILY_BATCH;
create procedure SP_LEMS_DAILY_BATCH(
  IN IN_YYYYMM VARCHAR(255)
) 
BEGIN
	/*
	Created at 2019/11/12 By Richard

	@DESCRIPTION
	매일 일괄처리할 SP를 호출한다.
	@PARAM
	  IN_YYYYMM: 처리하고자 하는 YYYYMM 값. null 인 경우 default 자동으로 오늘을 기준으로 동작함
	@RETURN
	없음
	*/
  
	-- 어제 기준의 Year Month를 구한다.
	set @YYYYMMOfYesterday=IN_YYYYMM;
	if IN_YYYYMM is null
	then
	set @YYYYMMOfYesterday=DATE_FORMAT( DATE_ADD(now(), INTERVAL -1 DAY)/*yesterday*/,'%Y%m');
	end if;

	set @YYYYOfYesterday=substring(@YYYYMMOfYesterday,1,4);
	set @MMOfYesterday=substring(@YYYYMMOfYesterday,5,2);
  
  
	-- SP 시작시간 저장
	SET @SP_START_SEC = TIME_TO_SEC(now());

	/*
	월 운송정보 생성: 운송정보
	*/
	call SP_MAKE_LOGIS_TRANS_MON_INFO(@YYYYMMOfYesterday); # call SP_MAKE_LOGIS_TRANS_MON_INFO('201802');
  
  
	/*
	월 운송정보 - 효율화지표 생성: T_LOGIS_TRANS_MON_EFF_INFO
	*/
	call SP_MAKE_LOGIS_TRANS_MON_EFF_INFO(@YYYYMMOfYesterday); # call SP_MAKE_LOGIS_TRANS_MON_EFF_INFO('201802');

	/*
	월 실적정보생성 (화물차,철송,해송): 온실가스 에너지/ 에너지 실적등록 / 사용실적
	*/
	call SP_MAKE_ENG_POINT_AMT(@YYYYOfYesterday,@MMOfYesterday); # call SP_MAKE_ENG_POINT_AMT('2018', '02');
  
  
	/*
	배출시설별 원단위 생성
	*/
	call SP_MAKE_ENG_GHG_AMT(@YYYYOfYesterday,@MMOfYesterday); # call SP_MAKE_ENG_GHG_AMT('2018', '02');
  
  
	/*
	온실가스/에너지현황 data 생성
	*/
	call SP_MAKE_LOGIS_TRANS_YEAR(@YYYYMMOfYesterday); # call SP_MAKE_LOGIS_TRANS_YEAR('201802');
  

	-- SP 종료시간 저장
	SET @SP_END_SEC = TIME_TO_SEC(now());
	-- logging
	INSERT INTO T_SCHEDULER_LOG (SP_NM) VALUES ( concat('SP_LEMS_DAILY_BATCH:',@YYYYMMOfYesterday, ', Duration: ', @SP_END_SEC-@SP_START_SEC, ' s') );
  
END $$
DELIMITER ;





-- 이벤트 등록
DELIMITER $$
create 
-- ALTER 
event event_SP_LEMS_DAILY_BATCH
    ON SCHEDULE
      EVERY 1 DAY             -- 매일
	STARTS '2019-01-01 02:0:0' -- 02시에
      ENABLE  
    DO
    BEGIN
      call SP_LEMS_DAILY_BATCH(null);
	END$$
DELIMITER ;

-- 이벤트 목록 보기 
SELECT * FROM information_schema.EVENTS;