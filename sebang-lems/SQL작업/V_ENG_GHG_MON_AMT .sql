/*
    V_ENG_GHG_MON_AMT   
        월별분석: 사업장 전체 선택 후 조회

*/
-- CREATE 
-- alter
-- VIEW V_ENG_GHG_MON_AMT AS
SELECT 
    M.YYYY AS C_YEAR,
    M.MM AS C_MON,
    M.S_DESC AS S_DESC,
    IFNULL(D.GJ, 0) AS GJ,
    IFNULL(D.TOE, 0) AS TOE,
    IFNULL(D.CO2, 0) AS CO2,
    IFNULL(D.CH4, 0) AS CH4,
    IFNULL(D.N2O, 0) AS N2O,
    IFNULL(D.TCO2EQ, 0) AS TCO2EQ
FROM (
    SELECT 
        YM.YYYY AS YYYY,
        YM.MM AS MM,
        CD.S_CD AS S_CD,
        CD.S_DESC AS S_DESC
    FROM V_YEAR_MONTH as YM
    CROSS JOIN T_CODE as CD
    WHERE CD.S_CAT = 'EMT'
        AND CD.S_CD IN (1 , 2, 3, 4, 5, 9)
) M
LEFT JOIN (
    SELECT EA.C_YEAR AS C_YEAR,
        EA.C_MON AS C_MON,
        C.S_EMT_CD AS S_EMT_CD,
        (SELECT T_CODE.S_DESC FROM T_CODE WHERE T_CODE.S_CAT = 'EMT' AND T_CODE.S_CD = C.S_EMT_CD) AS S_DESC,
        SUM(EA.N_GJ) AS GJ,
        SUM(EA.N_TOE) AS TOE,
        SUM(EA.N_CO2) AS CO2,
        SUM(EA.N_CH4) AS CH4,
        SUM(EA.N_N2O) AS N2O,
        SUM(EA.N_TCO2EQ) AS TCO2EQ
    FROM T_ENG_GHG_AMT EA
    INNER JOIN T_ENG_POINT P 
        ON (EA.ID_ENG_POINT = P.ID_ENG_POINT)
	INNER JOIN T_SITE_INFO SI
		ON P.ID_SITE = SI.ID_SITE
    INNER JOIN T_ENG_CEF C 
        ON C.ID_ENG_CEF = P.ID_ENG_CEF
    WHERE SI.C_DEL_YN != 'Y'
		AND P.C_DEL_YN != 'Y'
        AND C.C_DEL_YN != 'Y'
	    AND P.S_INV_CD != 11    -- 배출 시설 11=화물차(용차) 제외
    GROUP BY EA.C_YEAR ,EA.C_MON, C.S_EMT_CD
) D 
    ON (M.YYYY = D.C_YEAR
        AND M.MM = D.C_MON
        AND M.S_CD = D.S_EMT_CD)
;            