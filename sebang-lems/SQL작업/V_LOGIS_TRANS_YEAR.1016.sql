/*

V_LOGIS_TRANS_MON / 온실가스 에너지 현황

화면: 물류에너지 관리 / 온실가스 에너지 / 온실가스 에너지 현황

select * from T_CODE                 ;
select * from T_LOGIS_TRANS_MON_INFO ; -- 월운송정보
select * from T_SITE_INFO            ; -- 사업장
select * from T_VEHICLE              ; -- 차량 마스터
select * from V_VEHICLE              ; -- 차량 마스터
select * from T_ERP_DEPT             ; -- ERP 조직도

select * from T_LOGIS_TRANS_INFO     ; -- 운송정보


*/

-- 차량 온실가스 에너지 현황
-- create 
-- alter 
-- view V_LOGIS_TRANS_YEAR_VHCL as
select
  TMI.TRANS_TYPE_CD -- 운송수단
  ,(SELECT S_DESC FROM T_CODE WHERE S_CAT='TRANS_TYPE' and S_CD=TMI.TRANS_TYPE_CD ) TRANS_TYPE_NM -- 운송수단 한글명
  ,V.S_LINE_CD -- 노선구분
  ,V.S_SELFVRN_CD -- 자차여부
  ,V.LOGI_YCT_NM -- 노선구분 한글명
  ,V.S_LOGI_WEI_CD -- 톤수
  ,V.LOGI_WEI_NM -- 톤수 한글명
  ,V.S_CAR_TYPE -- 차종
  ,(SELECT S_DESC FROM T_CODE WHERE S_CAT='CAR_TYPE' and S_CD=V.S_CAR_TYPE ) CAR_TYPE_NM -- 차종 한글명
  ,V.ID_SITE -- 사업장ID
  ,V.S_SITE_NM -- 사업장명
  ,sum(TMI.LOADAGE_SUM) LOADAGE_SUM -- 적재량 합
  ,sum(TMI.FARE_SUM) FARE_SUM -- 운임료 합
  ,sum(TMI.VACANT_DISTANCE_SUM) VACANT_DISTANCE_SUM -- 공차운행거리 합
  ,sum(TMI.DISTANCE_SUM) DISTANCE_SUM -- 영차운행거리
  ,sum(TMI.MJ_SUM) MJ_SUM -- 에너지 사용량(MJ) 합
  ,sum(TMI.GCO2_SUM) GCO2_SUM -- 온실가스배출량(gCO2) 합
  ,sum(TMI.ENG_VOL_SUM) ENG_VOL_SUM -- 연료사용량 합
  ,sum(TMI.TOE_SUM) TOE_SUM -- 환상톤 합
  ,sum(TMI.TON_KM_SUM) TON_KM_SUM -- 톤킬로 합
  -- ,TMI.DATE_MON -- 날짜,YYYYMM
  ,substring(TMI.DATE_MON,1,4) YEAR -- 연
from 
  T_LOGIS_TRANS_MON_INFO TMI  -- 월운송정보
  LEFT JOIN V_VEHICLE V       -- 차량 마스터
    ON TMI.ID_VEHICLE = V.ID_VEHICLE -- 차량ID
  -- LEFT JOIN T_SITE_INFO SI    -- 사업장
  --  ON SI.ID_SITE=V.ID_SITE   -- 사업장ID
  where TMI.TRANS_TYPE_CD=1   -- 화물차
  GROUP BY
    V.TRANS_TYPE -- 운송수단
    ,V.S_CAR_TYPE -- 차종
    -- ,V.TRANS_TYPE_NM -- 운송수단 한글명
    ,V.S_LINE_CD -- 노선구분
    -- ,V.LOGI_YCT_NM -- 노선구분 한글명
    ,V.S_LOGI_WEI_CD -- 톤수
    ,V.LOGI_WEI_NM -- 톤수 한글명
    ,V.ID_SITE -- 사업장ID
    ,V.S_SITE_NM -- 사업장명
    -- ,TMI.DATE_MON -- 날짜,YYYYMM
    ,substring(TMI.DATE_MON,1,4) -- 연도
order by
	s_site_nm
  ;

-- 철송 온실가스 에너지 현황
-- create 
-- alter 
-- view V_LOGIS_TRANS_YEAR_TRAIN as
select
  TMI.TRANS_TYPE_CD -- 운송수단
  ,(SELECT S_DESC FROM T_CODE WHERE S_CAT='TRANS_TYPE' and S_CD=TMI.TRANS_TYPE_CD ) TRANS_TYPE_NM -- 운송수단 한글명
  ,1 S_LINE_CD -- 노선구분
  ,3 S_SELFVRN_CD -- 자차여부
  ,'정기' LOGI_YCT_NM -- 노선구분 한글명
  ,null S_LOGI_WEI_CD -- 톤수
  ,null LOGI_WEI_NM -- 톤수 한글명
  ,null S_CAR_TYPE -- 차종
  ,null CAR_TYPE_NM -- 차종 한글명
  ,SI.ID_SITE -- 사업장ID
  ,SI.S_SITE_NM -- 사업장명
  ,sum(TMI.LOADAGE_SUM) LOADAGE_SUM -- 적재량 합
  ,sum(TMI.FARE_SUM) FARE_SUM -- 운임료 합
  ,sum(TMI.VACANT_DISTANCE_SUM) VACANT_DISTANCE_SUM -- 공차운행거리 합
  ,sum(TMI.DISTANCE_SUM) DISTANCE_SUM -- 영차운행거리
  ,sum(TMI.MJ_SUM) MJ_SUM -- 에너지 사용량(MJ) 합
  ,sum(TMI.GCO2_SUM) GCO2_SUM -- 온실가스배출량(gCO2) 합
  ,sum(TMI.ENG_VOL_SUM) ENG_VOL_SUM -- 연료사용량 합
  ,sum(TMI.TOE_SUM) TOE_SUM -- 환상톤 합
  ,sum(TMI.TON_KM_SUM) TON_KM_SUM -- 톤킬로 합
  -- ,TMI.DATE_MON -- 날짜,YYYYMM
  ,substring(TMI.DATE_MON,1,4) YEAR -- 연
from 
	T_LOGIS_TRANS_MON_INFO TMI
    left join T_ENG_POINT EP
		ON TMI.ID_ENG_POINT=EP.ID_ENG_POINT
    left join V_SITE_DEPT SI
		ON EP.ID_SITE=SI.ID_SITE
where TRANS_TYPE_CD=3 -- and date_mon='201911'
  GROUP BY
    TMI.TRANS_TYPE_CD -- 운송수단
    ,SI.ID_SITE -- 사업장ID
    ,substring(TMI.DATE_MON,1,4) -- 연도
    ;

select * from T_LOGIS_TRANS_MON_INFO
where TRANS_TYPE_CD=3 and date_mon='201911';

select * from V_LOGIS_TRANS_YEAR_TRAIN
where YEAR='2019';

select 
  TMI.TRANS_TYPE_CD -- 운송수단
  ,(SELECT S_DESC FROM T_CODE WHERE S_CAT='TRANS_TYPE' and S_CD=TMI.TRANS_TYPE_CD ) TRANS_TYPE_NM -- 운송수단 한글명
  ,null S_LINE_CD -- 노선구분
  ,null LOGI_YCT_NM -- 노선구분 한글명
  ,null S_LOGI_WEI_CD -- 톤수
  ,null LOGI_WEI_NM -- 톤수 한글명
  ,null S_CAR_TYPE -- 차종
  ,null CAR_TYPE_NM -- 차종 한글명
  ,SI.ID_SITE -- 사업장ID
  ,SI.S_SITE_NM -- 사업장명
  ,sum(TMI.LOADAGE_SUM) LOADAGE_SUM -- 적재량 합
  ,sum(TMI.FARE_SUM) FARE_SUM -- 운임료 합
  ,sum(TMI.VACANT_DISTANCE_SUM) VACANT_DISTANCE_SUM -- 공차운행거리 합
  ,sum(TMI.DISTANCE_SUM) DISTANCE_SUM -- 영차운행거리
  ,sum(TMI.MJ_SUM) MJ_SUM -- 에너지 사용량(MJ) 합
  ,sum(TMI.GCO2_SUM) GCO2_SUM -- 온실가스배출량(gCO2) 합
  ,sum(TMI.ENG_VOL_SUM) ENG_VOL_SUM -- 연료사용량 합
  ,sum(TMI.TOE_SUM) TOE_SUM -- 환상톤 합
  ,sum(TMI.TON_KM_SUM) TON_KM_SUM -- 톤킬로 합
  -- ,TMI.DATE_MON -- 날짜,YYYYMM
  ,substring(TMI.DATE_MON,1,4) YEAR -- 연
 from 
	T_LOGIS_TRANS_MON_INFO TMI
    left join T_ENG_POINT EP
		ON TMI.ID_ENG_POINT=EP.ID_ENG_POINT
    left join V_SITE_DEPT SI
		ON EP.ID_SITE=SI.ID_SITE
where TRANS_TYPE_CD=3 and date_mon='201911'
  GROUP BY
    TMI.TRANS_TYPE_CD -- 운송수단
    ,SI.ID_SITE -- 사업장ID
    ,substring(TMI.DATE_MON,1,4) -- 연도
;

select * from T_ENG_POINT
;

-- 해송 온실가스 에너지 현황
-- create 
-- alter 
-- view V_LOGIS_TRANS_YEAR_SHIP as
select
  TMI.TRANS_TYPE_CD -- 운송수단
  ,(SELECT S_DESC FROM T_CODE WHERE S_CAT='TRANS_TYPE' and S_CD=TMI.TRANS_TYPE_CD ) TRANS_TYPE_NM -- 운송수단 한글명
  ,2 S_LINE_CD -- 노선구분
  ,3 S_SELFVRN_CD -- 자차여부
  ,'비정기' LOGI_YCT_NM -- 노선구분 한글명
  ,null S_LOGI_WEI_CD -- 톤수
  ,null LOGI_WEI_NM -- 톤수 한글명
  ,null S_CAR_TYPE -- 차종
  ,null CAR_TYPE_NM -- 차종 한글명
  ,SI.ID_SITE -- 사업장ID
  ,SI.S_SITE_NM -- 사업장명
  ,sum(TMI.LOADAGE_SUM) LOADAGE_SUM -- 적재량 합
  ,sum(TMI.FARE_SUM) FARE_SUM -- 운임료 합
  ,sum(TMI.VACANT_DISTANCE_SUM) VACANT_DISTANCE_SUM -- 공차운행거리 합
  ,sum(TMI.DISTANCE_SUM) DISTANCE_SUM -- 영차운행거리
  ,sum(TMI.MJ_SUM) MJ_SUM -- 에너지 사용량(MJ) 합
  ,sum(TMI.GCO2_SUM) GCO2_SUM -- 온실가스배출량(gCO2) 합
  ,sum(TMI.ENG_VOL_SUM) ENG_VOL_SUM -- 연료사용량 합
  ,sum(TMI.TOE_SUM) TOE_SUM -- 환상톤 합
  ,sum(TMI.TON_KM_SUM) TON_KM_SUM -- 톤킬로 합
  -- ,TMI.DATE_MON -- 날짜,YYYYMM
  ,substring(TMI.DATE_MON,1,4) YEAR -- 연
from 
	T_LOGIS_TRANS_MON_INFO TMI
    left join T_ENG_POINT EP
		ON TMI.ID_ENG_POINT=EP.ID_ENG_POINT
    left join V_SITE_DEPT SI
		ON EP.ID_SITE=SI.ID_SITE
  where TMI.TRANS_TYPE_CD=2   -- 해송
  GROUP BY
    TMI.TRANS_TYPE_CD -- 운송수단
    ,SI.ID_SITE -- 사업장ID
    ,SI.S_SITE_NM -- 사업장명
    -- ,TMI.DATE_MON -- 날짜,YYYYMM
    ,substring(TMI.DATE_MON,1,4) -- 연도
    ;


-- 고정체(시설) 에너지 현황
-- create 
-- alter 
-- view V_LOGIS_TRANS_YEAR_FACIL as
select 
	0 TRANS_TYPE_CD -- 운송수단
  ,'기타' TRANS_TYPE_NM -- 운송수단 한글명
  ,null S_LINE_CD -- 노선구분
  ,3 S_SELFVRN_CD -- 자차여부
  ,null LOGI_YCT_NM -- 노선구분 한글명
  ,null S_LOGI_WEI_CD -- 톤수
  ,null LOGI_WEI_NM -- 톤수 한글명
  ,null S_CAR_TYPE -- 차종
  ,null CAR_TYPE_NM -- 차종 한글명
  ,SI.ID_SITE -- 사업장ID
  ,SI.S_SITE_NM -- 사업장명
  ,0 LOADAGE_SUM -- 적재량 합
  ,0 FARE_SUM -- 운임료 합
  ,0 VACANT_DISTANCE_SUM -- 공차운행거리 합
  ,0 DISTANCE_SUM -- 영차운행거리
  ,sum(EGA.N_GJ) MJ_SUM -- 에너지 사용량(MJ) 합
  ,sum(EGA.N_CO2) GCO2_SUM -- 온실가스배출량(gCO2) 합
  ,0 ENG_VOL_SUM -- 연료사용량 합
  ,sum(EGA.N_TOE) TOE_SUM -- 환상톤 합
  ,0 TON_KM_SUM -- 톤킬로 합
  -- ,TMI.DATE_MON -- 날짜,YYYYMM
  ,EGA.C_YEAR YEAR -- 연
  
 from 
	T_ENG_POINT EP
    LEFT JOIN V_SITE_DEPT SI
		ON EP.ID_SITE=SI.ID_SITE
			and EP.S_LOGI_TYPE_CD='1'
			and EP.C_DEL_YN='N'
            and EP.C_POINT_YN='Y'
            and S_INV_CD in (1,6)
    LEFT JOIN T_ENG_GHG_AMT EGA
		ON EP.ID_ENG_POINT = EGA.ID_ENG_POINT
			and EP.C_DEL_YN='N'
where -- EGA.C_YEAR='2019'
	SI.ID_SITE is not null
GROUP BY
    SI.ID_SITE -- 사업장ID
    ,EGA.C_YEAR -- 연도    
;        

select * from T_ENG_POINT;
select * from T_ENG_GHG_AMT EGA;

    
-- 예제
select * from V_LOGIS_TRANS_MON_VHCL 
  where
    ID_SITE=10
    -- and DATE_MON BETWEEN '201901' and '201907'
    and YEAR='2019'
union
select * from V_LOGIS_TRANS_MON_TRAIN 
  where
    ID_SITE=10
    -- and DATE_MON BETWEEN '201901' and '201907'
    and YEAR='2019'
union 
select * from V_LOGIS_TRANS_MON_SHIP
  where
    ID_SITE=10
    -- and DATE_MON BETWEEN '201901' and '201907'
    and YEAR='2019'
;
select * from V_LOGIS_TRANS_YEAR_FACIL
  where YEAR='2019'
;

select * from V_LOGIS_TRANS_YEAR_VHCL 
  where
	1=1
	-- ID_SITE=10
    -- and DATE_MON BETWEEN '201901' and '201907'
    and YEAR='2019'
union
select * from V_LOGIS_TRANS_YEAR_TRAIN 
  where
	1=1
    -- ID_SITE=10
    -- and DATE_MON BETWEEN '201901' and '201907'
    and YEAR='2019'
union 
select * from V_LOGIS_TRANS_YEAR_SHIP
  where
	1=1
    -- ID_SITE=10
    -- and DATE_MON BETWEEN '201901' and '201907'
    and YEAR='2019'
union
select * from V_LOGIS_TRANS_YEAR_FACIL     
  where
	1=1
    -- ID_SITE=10
    and YEAR='2019'
;

