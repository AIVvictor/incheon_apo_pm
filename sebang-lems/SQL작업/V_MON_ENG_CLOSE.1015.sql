/*

V_MON_ENG_CLOSE

온실가스 에너지/ 에너지 실적등록 / 월마감

T_MON_ENG_CLOSE -- 월마감 
T_SITE_INFO     -- 사업장
T_ENG_POINT     -- 배출시설
T_ENG_CEF       -- 배출원관리
T_ENG_GHG_AMT   -- 배출원온실가스 계산(집계)
T_CODE


T_SITE_INFO SI  -- 사업장
T_ENG_POINT EP    -- 배출시설
T_ENG_GHG_AMT EGA -- 배출원온실가스 계산(집계)
T_MON_ENG_CLOSE MC  -- 월마감 

select * from T_MON_ENG_CLOSE; -- 월마감 
select * from T_SITE_INFO    ; -- 사업장
select * from T_ENG_POINT    ; -- 배출시설
select * from T_ENG_CEF      ; -- 배출원관리
select * from T_ENG_GHG_AMT  ; -- 배출원온실가스 계산(집계)

*/
select * from V_MON_ENG_CLOSE_SUB;

-- alter VIEW V_MON_ENG_CLOSE_SUB  AS
SELECT SI.ID_SITE AS ID_SITE,      -- 사업장ID
       SI.S_SITE_ID AS S_SITE_ID,  -- 사업장ID
       SI.S_SITE_NM AS S_SITE_NM,  -- 사업장 이름
       SI.N_ORDER AS N_ORDER,      -- 정렬 순서
       SI.C_DEL_YN AS SI_DEL_YN,   -- 사업장 삭제 여부
       
       YYYY.S_DESC as C_YEAR,      -- 연도
       MM.S_CD as C_MON,         -- 월
       
       ifnull(sum(EGA.N_GJ), 0) AS GJ_SUM,        -- 에너지사용량_TJ
       ifnull(sum(EGA.N_TOE), 0) AS TOE_SUM,      -- 에너지사용량_TOE
       ifnull(sum(EGA.N_CO2), 0) AS CO2_SUM,      -- 온실가스배출량_CO2
       ifnull(sum(EGA.N_CH4), 0) AS CH4_SUM,      -- 온실가스배출량_CH4
       ifnull(sum(EGA.N_N2O), 0) AS N2O_SUM,      -- 온실가스배출량_N2O
       ifnull(sum(EGA.N_TCO2EQ), 0) AS TCO2EQ_SUM, -- 온실가스배출량_tCO2eq
       ifnull(MC.C_CLOSE_YN, 'N') AS C_CLOSE_YN,  -- 마감여부:'Y'=마감'N'=마감전
       MC.S_MOD_DATE -- 실적수정 종료일
FROM 
    T_SITE_INFO SI  -- 사업장
    CROSS JOIN T_CODE as YYYY
	  ON (YYYY.s_cat = 'YYYY' and YYYY.c_del_yn = 'N')
    CROSS JOIN T_CODE as MM
      ON (MM.s_cat = 'MM' and MM.c_del_yn = 'N')
    LEFT JOIN T_ENG_POINT EP    -- 배출시설
      ON (EP.ID_SITE = SI.ID_SITE)
    LEFT JOIN T_ENG_GHG_AMT EGA -- 배출원온실가스 계산(집계)
      ON (EP.ID_ENG_POINT = EGA.ID_ENG_POINT AND YYYY.S_CD = EGA.C_YEAR AND MM.S_CD = EGA.C_MON)
    LEFT JOIN T_MON_ENG_CLOSE MC  -- 월마감 
      ON (SI.ID_SITE = MC.ID_SITE AND YYYY.S_CD = MC.C_YEAR AND MM.S_CD = MC.C_MON)
where SI.C_DEL_YN != 'Y'
GROUP BY SI.ID_SITE, SI.S_SITE_ID,SI.N_ORDER, SI.C_DEL_YN, YYYY.S_DESC, MM.S_DESC
ORDER BY SI.ID_SITE, SI.N_ORDER, YYYY.S_DESC, MM.S_CD;

-- alter VIEW V_MON_ENG_CLOSE  AS
select
  T1.ID_SITE    -- 사업장ID
  ,T1.S_SITE_NM -- 사업장 이름
  ,T1.N_ORDER   -- 정렬 순서
  ,T1.SI_DEL_YN -- 사업장 삭제 여부
  ,T1.C_YEAR    -- 연도
  ,T1.C_MON     -- 월  
  ,T1.C_CLOSE_YN -- 마감여부:'Y'=마감'N'=마감전
  ,T1.S_MOD_DATE -- 실적수정 종료일
  
  ,ifnull(T1.GJ_SUM    ,0)   T1_GJ_SUM      -- 조회 기준 연도 에너지사용량_TJ
  ,ifnull(T1.TOE_SUM   ,0)   T1_TOE_SUM     -- 조회 기준 연도 에너지사용량_TOE
  ,ifnull(T1.CO2_SUM   ,0)   T1_CO2_SUM     -- 조회 기준 연도 온실가스배출량_CO2
  ,ifnull(T1.CH4_SUM   ,0)   T1_CH4_SUM     -- 조회 기준 연도 온실가스배출량_CH4
  ,ifnull(T1.N2O_SUM   ,0)   T1_N2O_SUM     -- 조회 기준 연도 온실가스배출량_N2O
  ,ifnull(T1.TCO2EQ_SUM,0)   T1_TCO2EQ_SUM  -- 조회 기준 연도 온실가스배출량_tCO2eq
  
  ,ifnull(T2.GJ_SUM    ,0)   T2_GJ_SUM      -- 전년도 에너지사용량_TJ
  ,ifnull(T2.TOE_SUM   ,0)   T2_TOE_SUM     -- 전년도 에너지사용량_TOE
  ,ifnull(T2.CO2_SUM   ,0)   T2_CO2_SUM     -- 전년도 온실가스배출량_CO2
  ,ifnull(T2.CH4_SUM   ,0)   T2_CH4_SUM     -- 전년도 온실가스배출량_CH4
  ,ifnull(T2.N2O_SUM   ,0)   T2_N2O_SUM     -- 전년도 온실가스배출량_N2O
  ,ifnull(T2.TCO2EQ_SUM,0)   T2_TCO2EQ_SUM  -- 전년도 온실가스배출량_tCO2eq
  
  ,ifnull(T1.GJ_SUM    ,0) - ifnull(T2.GJ_SUM    ,0) GJ_SUM_DELTA      -- 전년도 대비 에너지사용량_TJ 증감
  ,ifnull(T1.TOE_SUM   ,0) - ifnull(T2.TOE_SUM   ,0) TOE_SUM_DELTA     -- 전년도 대비 에너지사용량_TOE 증감
  ,ifnull(T1.CO2_SUM   ,0) - ifnull(T2.CO2_SUM   ,0) CO2_SUM_DELTA     -- 전년도 대비 온실가스배출량_CO2 증감
  ,ifnull(T1.CH4_SUM   ,0) - ifnull(T2.CH4_SUM   ,0) CH4_SUM_DELTA     -- 전년도 대비 온실가스배출량_CH4 증감
  ,ifnull(T1.N2O_SUM   ,0) - ifnull(T2.N2O_SUM   ,0) N2O_SUM_DELTA     -- 전년도 대비 온실가스배출량_N2O 증감
  ,ifnull(T1.TCO2EQ_SUM,0) - ifnull(T2.TCO2EQ_SUM,0) TCO2EQ_SUM_DELTA  -- 전년도 대비 온실가스배출량_tCO2eq 증감
  
  ,calcDeltaRate(ifnull(T2.GJ_SUM    ,0) ,ifnull(T1.GJ_SUM    ,0)) GJ_SUM_DELTA_RATE      -- 전년도 대비 에너지사용량_TJ 증감율 %
  ,calcDeltaRate(ifnull(T2.TOE_SUM   ,0) ,ifnull(T1.TOE_SUM   ,0)) TOE_SUM_DELTA_RATE     -- 전년도 대비 에너지사용량_TOE 증감율 %
  ,calcDeltaRate(ifnull(T2.CO2_SUM   ,0) ,ifnull(T1.CO2_SUM   ,0)) CO2_SUM_DELTA_RATE     -- 전년도 대비 온실가스배출량_CO2 증감율 %
  ,calcDeltaRate(ifnull(T2.CH4_SUM   ,0) ,ifnull(T1.CH4_SUM   ,0)) CH4_SUM_DELTA_RATE     -- 전년도 대비 온실가스배출량_CH4 증감율 %
  ,calcDeltaRate(ifnull(T2.N2O_SUM   ,0) ,ifnull(T1.N2O_SUM   ,0)) N2O_SUM_DELTA_RATE     -- 전년도 대비 온실가스배출량_N2O 증감율 %
  ,calcDeltaRate(ifnull(T2.TCO2EQ_SUM,0) ,ifnull(T1.TCO2EQ_SUM,0)) TCO2EQ_SUM_DELTA_RATE  -- 전년도 대비 온실가스배출량_tCO2eq 증감율 %

from
  V_MON_ENG_CLOSE_SUB T1 -- 조회 기준 연도
  LEFT JOIN V_MON_ENG_CLOSE_SUB T2 -- 전년도
    ON T1.C_YEAR-1=T2.C_YEAR
      and T1.ID_SITE = T2.ID_SITE
      and T1.C_MON = T2.C_MON
ORDER BY T1.N_ORDER, T1.C_YEAR, T1.C_MON;
;
      
      
      
-- sample
select calcDeltaRate(16.612, 16.904);
select (16.904-16.612)/16.612*100;
-- sample
select * from V_MON_ENG_CLOSE where C_YEAR=2019 and C_MON=7;


-- function
-- ineger version
delimiter $$
DROP FUNCTION IF EXISTS calcDeltaRate;
CREATE FUNCTION `calcDeltaRate`(a INT, b INT) RETURNS int(11)
BEGIN
  DECLARE result INT DEFAULT -1;
  if b=0
  then
    SET result = 0;
  else
    SET result = (b-a)/a*100;
  end if;
  RETURN result;
END $$
delimiter ;


-- DECIMAL version
delimiter $$
DROP FUNCTION IF EXISTS calcDeltaRate;
CREATE FUNCTION `calcDeltaRate`(a DECIMAL(20,5), b DECIMAL(20,5)) RETURNS DECIMAL(30,5)
BEGIN
  DECLARE result DECIMAL(20,3) DEFAULT -1;
  if a=0
  then
    SET result = 0;
  else
    SET result = (b-a)/a*100;
  end if;
  RETURN result;
END $$
delimiter ;


