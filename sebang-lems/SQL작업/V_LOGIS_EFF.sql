/*

  V_LOGIS_EFF
  
  화면 : 물류에너지 관리 / 온실가스 에너지 / 효율화 지표

  2019/11/8 by richard

*/

select * from V_LOGIS_EFF;
-- ==================================================================
-- 11/8 작성
-- create 
alter
view V_LOGIS_EFF as
select 
  2019 YEAR
  ,1 ID_SITE -- 사업장ID
  ,'1' SITE_NM -- 사업장명
  ,1 CAR_TYPE -- 차종
  ,'1' CAR_TYPE_NM -- 차종명
  ,1 LOGI_WEI_CD -- 톤수
  ,'1' LOGI_WEI_NM -- 톤수한글이름
  ,1 LINE_CD -- 노선구분
  ,'1' LINE_NM -- 노선구분한글이름
  ,1 TRANS_TYPE_CD -- 운송수단,T_CODE TRANS_TYPE 참조
  ,'2' TRANS_TYPE_NM -- 운송수단명
  ,1 M1
  ,1 M2
  ,1 M3
  ,1 M4
  ,1 M5
  ,1 M6
  ,1 M7
  ,1 M8
  ,1 M9
  ,1 M10
  ,1 M11
  ,1 M12
;
-- ==================================================================
-- 11/8 작성
-- create 
alter
view V_LOGIS_EFF_SUB as
select 
  M.ID_LOGIS_TRANS_MON_INFO
  ,V.ID_VEHICLE -- 차량ID
  ,V.S_VRN VRN -- 차량번호
  ,M.ID_ERP_DEPT -- ERP부서ID
  ,M.DATE_MON -- 날짜: YYYYMM
  ,substring(M.DATE_MON,1,4) YEAR -- 날짜: YYYY
  ,substring(M.DATE_MON,5,2) MON -- 날짜: MM
  ,M.TON_KM_SUM -- 톤킬로, 온실가스원단위
  ,M.GCO2_SUM -- 온실가스배출량(gCO2)
  ,M.MJ_SUM -- 에너지사용량(MJ)
  ,M.ENG_VOL_SUM -- 연료사용량
  -- ,V.S_SELFVRN_CD SELFVRN_CD -- 자차여부
  -- ,V.S_LINE_CD LINE_CD -- 노선구분(분정기/비정기)
  -- ,V.S_LOGI_WEI_CD LOGI_WEI_CD -- 톤수
  
from 
  T_LOGIS_TRANS_MON_INFO M -- 월운송정보
  LEFT JOIN T_VEHICLE V -- 차량 master
    ON V.ID_VEHICLE = M.ID_VEHICLE
  -- LEFT JOIN V_SITE_DEPT D -- 사업장+부서정보
  --   ON V.ID_ERP_DEPT = D.ID_ERP_DEPT;
;

select * from T_LOGIS_TRANS_MON_INFO;

select * from V_SITE_DEPT;

-- =====================================================================

select * from V_LOGIS_EFF_SUB;
select * from V_LOGIS_EFF;
select * from V_VEHICLE;
select * from T_CODE where S_CAT='CAR_TYPE';

-- INSERT INTO T_CODE(  S_CAT  ,S_CD  ,S_DESC  ,C_DEL_YN) VALUES (  'LOGI_WEI'  ,'1' ,'1톤' ,'N');

-- =====================================================================
-- 11/8 작성: 월별
create 
-- alter
view V_LOGIS_EFF_M as
select 
  V.ID_VEHICLE,V.S_VRN VRN,T.YEAR
  ,D.S_SITE_ID SITE_ID
  ,D.S_SITE_NM SITE_NM
  ,V.S_CAR_TYPE CAR_TYPE
  ,(select S_DESC from T_CODE where S_CAT='CAR_TYPE' and S_CD=V.S_CAR_TYPE) CAR_TYPE_NM
  ,V.S_SELFVRN_CD SELFVRN_CD -- 자차여부
  ,(select S_DESC from T_CODE where S_CAT='SELFVRN' and S_CD=V.S_SELFVRN_CD) SELFVRN_NM
  ,V.S_LINE_CD LINE_CD -- 노선구분(분정기/비정기)
  ,(select S_DESC from T_CODE where S_CAT='LOGI_YCT' and S_CD=V.S_LINE_CD) LINE_NM
  ,V.S_LOGI_WEI_CD LOGI_WEI_CD -- 톤수
  ,(select S_DESC from T_CODE where S_CAT='LOGI_WEI' and S_CD=V.S_LOGI_WEI_CD) LOGI_WEI_NM
  ,T.M1_TON_KM,  T.M1_GCO2,  T.M1_MJ,  T.M1_ENG_VOL 
  ,T.M2_TON_KM,  T.M2_GCO2,  T.M2_MJ,  T.M2_ENG_VOL 
  ,T.M3_TON_KM,  T.M3_GCO2,  T.M3_MJ,  T.M3_ENG_VOL
  ,T.M4_TON_KM,  T.M4_GCO2,  T.M4_MJ,  T.M4_ENG_VOL
  ,T.M5_TON_KM,  T.M5_GCO2,  T.M5_MJ,  T.M5_ENG_VOL
  ,T.M6_TON_KM,  T.M6_GCO2,  T.M6_MJ,  T.M6_ENG_VOL
  ,T.M7_TON_KM,  T.M7_GCO2,  T.M7_MJ,  T.M7_ENG_VOL
  ,T.M8_TON_KM,  T.M8_GCO2,  T.M8_MJ,  T.M8_ENG_VOL
  ,T.M9_TON_KM,  T.M9_GCO2,  T.M9_MJ,  T.M9_ENG_VOL
  ,T.M10_TON_KM, T.M10_GCO2, T.M10_MJ, T.M10_ENG_VOL
  ,T.M11_TON_KM, T.M11_GCO2, T.M11_MJ, T.M11_ENG_VOL
  ,T.M12_TON_KM, T.M12_GCO2, T.M12_MJ, T.M12_ENG_VOL
from 
  (SELECT 
    ID_VEHICLE, VRN, YEAR
    ,max(M1_TON_KM_SUM)  M1_TON_KM,    max(M1_GCO2_SUM)   M1_GCO2,  max(M1_MJ_SUM)  M1_MJ,  max(M1_ENG_VOL_SUM)  M1_ENG_VOL
    ,max(M2_TON_KM_SUM)  M2_TON_KM,    max(M2_GCO2_SUM)   M2_GCO2,  max(M2_MJ_SUM)  M2_MJ,  max(M2_ENG_VOL_SUM)  M2_ENG_VOL
    ,max(M3_TON_KM_SUM)  M3_TON_KM,    max(M3_GCO2_SUM)   M3_GCO2,  max(M3_MJ_SUM)  M3_MJ,  max(M3_ENG_VOL_SUM)  M3_ENG_VOL
    ,max(M4_TON_KM_SUM)  M4_TON_KM,    max(M4_GCO2_SUM)   M4_GCO2,  max(M4_MJ_SUM)  M4_MJ,  max(M4_ENG_VOL_SUM)  M4_ENG_VOL
    ,max(M5_TON_KM_SUM)  M5_TON_KM,    max(M5_GCO2_SUM)   M5_GCO2,  max(M5_MJ_SUM)  M5_MJ,  max(M5_ENG_VOL_SUM)  M5_ENG_VOL
    ,max(M6_TON_KM_SUM)  M6_TON_KM,    max(M6_GCO2_SUM)   M6_GCO2,  max(M6_MJ_SUM)  M6_MJ,  max(M6_ENG_VOL_SUM)  M6_ENG_VOL
    ,max(M7_TON_KM_SUM)  M7_TON_KM,    max(M7_GCO2_SUM)   M7_GCO2,  max(M7_MJ_SUM)  M7_MJ,  max(M7_ENG_VOL_SUM)  M7_ENG_VOL
    ,max(M8_TON_KM_SUM)  M8_TON_KM,    max(M8_GCO2_SUM)   M8_GCO2,  max(M8_MJ_SUM)  M8_MJ,  max(M8_ENG_VOL_SUM)  M8_ENG_VOL
    ,max(M9_TON_KM_SUM)  M9_TON_KM,    max(M9_GCO2_SUM)   M9_GCO2,  max(M9_MJ_SUM)  M9_MJ,  max(M9_ENG_VOL_SUM)  M9_ENG_VOL
    ,max(M10_TON_KM_SUM) M10_TON_KM,   max(M10_GCO2_SUM)  M10_GCO2, max(M10_MJ_SUM) M10_MJ, max(M10_ENG_VOL_SUM) M10_ENG_VOL
    ,max(M11_TON_KM_SUM) M11_TON_KM,   max(M11_GCO2_SUM)  M11_GCO2, max(M11_MJ_SUM) M11_MJ, max(M11_ENG_VOL_SUM) M11_ENG_VOL
    ,max(M12_TON_KM_SUM) M12_TON_KM,   max(M12_GCO2_SUM)  M12_GCO2, max(M12_MJ_SUM) M12_MJ, max(M12_ENG_VOL_SUM) M12_ENG_VOL
  FROM
  (
    select 
      -- ID_LOGIS_TRANS_MON_INFO
      -- ,DATE_MON -- 날짜: YYYYMM
      ID_VEHICLE -- 차량ID
      ,VRN -- 차량번호
      ,YEAR -- 날짜: YYYY
      -- ,MON -- 날짜: MM
      -- ,SELFVRN_CD -- 자차여부
      -- ,LOGI_YCT_CD -- 노선구분(분정기/비정기)
      -- ,LOGI_WEI_CD -- 톤수
      ,case when (MON=1) then TON_KM_SUM else null end  as M1_TON_KM_SUM  -- 톤킬로, 온실가스원단위
      ,case when (MON=1) then GCO2_SUM else null end    as M1_GCO2_SUM  -- 온실가스배출량(gCO2)
      ,case when (MON=1) then MJ_SUM else null end      as M1_MJ_SUM  -- 에너지사용량(MJ)
      ,case when (MON=1) then ENG_VOL_SUM else null end as M1_ENG_VOL_SUM  -- 연료사용량

      ,case when (MON=2) then TON_KM_SUM else null end  as M2_TON_KM_SUM  -- 톤킬로, 온실가스원단위
      ,case when (MON=2) then GCO2_SUM else null end    as M2_GCO2_SUM  -- 온실가스배출량(gCO2)
      ,case when (MON=2) then MJ_SUM else null end      as M2_MJ_SUM  -- 에너지사용량(MJ)
      ,case when (MON=2) then ENG_VOL_SUM else null end as M2_ENG_VOL_SUM  -- 연료사용량
      
      ,case when (MON=3) then TON_KM_SUM else null end  as M3_TON_KM_SUM  -- 톤킬로, 온실가스원단위
      ,case when (MON=3) then GCO2_SUM else null end    as M3_GCO2_SUM  -- 온실가스배출량(gCO2)
      ,case when (MON=3) then MJ_SUM else null end      as M3_MJ_SUM  -- 에너지사용량(MJ)
      ,case when (MON=3) then ENG_VOL_SUM else null end as M3_ENG_VOL_SUM  -- 연료사용량
      
      ,case when (MON=4) then TON_KM_SUM else null end  as M4_TON_KM_SUM  -- 톤킬로, 온실가스원단위
      ,case when (MON=4) then GCO2_SUM else null end    as M4_GCO2_SUM  -- 온실가스배출량(gCO2)
      ,case when (MON=4) then MJ_SUM else null end      as M4_MJ_SUM  -- 에너지사용량(MJ)
      ,case when (MON=4) then ENG_VOL_SUM else null end as M4_ENG_VOL_SUM  -- 연료사용량
      
      ,case when (MON=5) then TON_KM_SUM else null end  as M5_TON_KM_SUM  -- 톤킬로, 온실가스원단위
      ,case when (MON=5) then GCO2_SUM else null end    as M5_GCO2_SUM  -- 온실가스배출량(gCO2)
      ,case when (MON=5) then MJ_SUM else null end      as M5_MJ_SUM  -- 에너지사용량(MJ)
      ,case when (MON=5) then ENG_VOL_SUM else null end as M5_ENG_VOL_SUM  -- 연료사용량
      
      ,case when (MON=6) then TON_KM_SUM else null end  as M6_TON_KM_SUM  -- 톤킬로, 온실가스원단위
      ,case when (MON=6) then GCO2_SUM else null end    as M6_GCO2_SUM  -- 온실가스배출량(gCO2)
      ,case when (MON=6) then MJ_SUM else null end      as M6_MJ_SUM  -- 에너지사용량(MJ)
      ,case when (MON=6) then ENG_VOL_SUM else null end as M6_ENG_VOL_SUM  -- 연료사용량
      
      ,case when (MON=7) then TON_KM_SUM else null end  as M7_TON_KM_SUM  -- 톤킬로, 온실가스원단위
      ,case when (MON=7) then GCO2_SUM else null end    as M7_GCO2_SUM  -- 온실가스배출량(gCO2)
      ,case when (MON=7) then MJ_SUM else null end      as M7_MJ_SUM  -- 에너지사용량(MJ)
      ,case when (MON=7) then ENG_VOL_SUM else null end as M7_ENG_VOL_SUM  -- 연료사용량
      
      ,case when (MON=8) then TON_KM_SUM else null end  as M8_TON_KM_SUM  -- 톤킬로, 온실가스원단위
      ,case when (MON=8) then GCO2_SUM else null end    as M8_GCO2_SUM  -- 온실가스배출량(gCO2)
      ,case when (MON=8) then MJ_SUM else null end      as M8_MJ_SUM  -- 에너지사용량(MJ)
      ,case when (MON=8) then ENG_VOL_SUM else null end as M8_ENG_VOL_SUM  -- 연료사용량
      
      ,case when (MON=9) then TON_KM_SUM else null end  as M9_TON_KM_SUM  -- 톤킬로, 온실가스원단위
      ,case when (MON=9) then GCO2_SUM else null end    as M9_GCO2_SUM  -- 온실가스배출량(gCO2)
      ,case when (MON=9) then MJ_SUM else null end      as M9_MJ_SUM  -- 에너지사용량(MJ)
      ,case when (MON=9) then ENG_VOL_SUM else null end as M9_ENG_VOL_SUM  -- 연료사용량
      
      ,case when (MON=10) then TON_KM_SUM else null end  as M10_TON_KM_SUM  -- 톤킬로, 온실가스원단위
      ,case when (MON=10) then GCO2_SUM else null end    as M10_GCO2_SUM  -- 온실가스배출량(gCO2)
      ,case when (MON=10) then MJ_SUM else null end      as M10_MJ_SUM  -- 에너지사용량(MJ)
      ,case when (MON=10) then ENG_VOL_SUM else null end as M10_ENG_VOL_SUM  -- 연료사용량
      
      ,case when (MON=11) then TON_KM_SUM else null end  as M11_TON_KM_SUM  -- 톤킬로, 온실가스원단위
      ,case when (MON=11) then GCO2_SUM else null end    as M11_GCO2_SUM  -- 온실가스배출량(gCO2)
      ,case when (MON=11) then MJ_SUM else null end      as M11_MJ_SUM  -- 에너지사용량(MJ)
      ,case when (MON=11) then ENG_VOL_SUM else null end as M11_ENG_VOL_SUM  -- 연료사용량
      
      ,case when (MON=12) then TON_KM_SUM else null end  as M12_TON_KM_SUM  -- 톤킬로, 온실가스원단위
      ,case when (MON=12) then GCO2_SUM else null end    as M12_GCO2_SUM  -- 온실가스배출량(gCO2)
      ,case when (MON=12) then MJ_SUM else null end      as M12_MJ_SUM  -- 에너지사용량(MJ)
      ,case when (MON=12) then ENG_VOL_SUM else null end as M12_ENG_VOL_SUM  -- 연료사용량
         
    from 
      V_LOGIS_EFF_SUB E) T
    group by ID_VEHICLE, VRN, YEAR  ) T 
  LEFT JOIN T_VEHICLE V
    ON T.ID_VEHICLE=V.ID_VEHICLE
  LEFT JOIN V_SITE_DEPT D
    ON V.ID_ERP_DEPT=D.ID_ERP_DEPT
;

select * from V_SITE_DEPT; 

-- create 
-- alter
-- view V_LOGIS_EFF as
select * from V_LOGIS_EFF_M;


-- =====================================================================
-- 11/8 작성: 분기별
-- create 
alter
view V_LOGIS_EFF_Q as
select 
  V.ID_VEHICLE,V.S_VRN VRN,T.YEAR
  ,D.S_SITE_ID SITE_ID
  ,D.S_SITE_NM SITE_NM
  ,V.S_CAR_TYPE CAR_TYPE
  ,(select S_DESC from T_CODE where S_CAT='CAR_TYPE' and S_CD=V.S_CAR_TYPE) CAR_TYPE_NM
  ,V.S_SELFVRN_CD SELFVRN_CD -- 자차여부
  ,(select S_DESC from T_CODE where S_CAT='SELFVRN' and S_CD=V.S_SELFVRN_CD) SELFVRN_NM
  ,V.S_LINE_CD LINE_CD -- 노선구분(분정기/비정기)
  ,(select S_DESC from T_CODE where S_CAT='LOGI_YCT' and S_CD=V.S_LINE_CD) LINE_NM
  ,V.S_LOGI_WEI_CD LOGI_WEI_CD -- 톤수
  ,(select S_DESC from T_CODE where S_CAT='LOGI_WEI' and S_CD=V.S_LOGI_WEI_CD) LOGI_WEI_NM
  ,ifnull(T.M1_TON_KM  ,0) + ifnull(T.M2_TON_KM  ,0) + ifnull(T.M3_TON_KM  ,0)  Q1_TON_KM
  ,ifnull(T.M4_TON_KM  ,0) + ifnull(T.M5_TON_KM  ,0) + ifnull(T.M6_TON_KM  ,0)  Q2_TON_KM
  ,ifnull(T.M7_TON_KM  ,0) + ifnull(T.M8_TON_KM  ,0) + ifnull(T.M9_TON_KM  ,0)  Q3_TON_KM
  ,ifnull(T.M10_TON_KM ,0) + ifnull(T.M11_TON_KM ,0) + ifnull(T.M12_TON_KM ,0)  Q4_TON_KM
  ,ifnull(T.M1_GCO2    ,0) + ifnull(T.M2_GCO2    ,0) + ifnull(T.M3_GCO2    ,0)  Q1_GCO2
  ,ifnull(T.M4_GCO2    ,0) + ifnull(T.M5_GCO2    ,0) + ifnull(T.M6_GCO2    ,0)  Q2_GCO2
  ,ifnull(T.M7_GCO2    ,0) + ifnull(T.M8_GCO2    ,0) + ifnull(T.M9_GCO2    ,0)  Q3_GCO2
  ,ifnull(T.M10_GCO2   ,0) + ifnull(T.M11_GCO2   ,0) + ifnull(T.M12_GCO2   ,0)  Q4_GCO2
  ,ifnull(T.M1_MJ      ,0) + ifnull(T.M2_MJ      ,0) + ifnull(T.M3_MJ      ,0)  Q1_MJ 
  ,ifnull(T.M4_MJ      ,0) + ifnull(T.M5_MJ      ,0) + ifnull(T.M6_MJ      ,0)  Q2_MJ 
  ,ifnull(T.M7_MJ      ,0) + ifnull(T.M8_MJ      ,0) + ifnull(T.M9_MJ      ,0)  Q3_MJ 
  ,ifnull(T.M10_MJ     ,0) + ifnull(T.M11_MJ     ,0) + ifnull(T.M12_MJ     ,0)  Q4_MJ 
  ,ifnull(T.M1_ENG_VOL ,0) + ifnull(T.M2_ENG_VOL ,0) + ifnull(T.M3_ENG_VOL ,0)   Q1_ENG_VOL
  ,ifnull(T.M4_ENG_VOL ,0) + ifnull(T.M5_ENG_VOL ,0) + ifnull(T.M6_ENG_VOL ,0)    Q2_ENG_VOL
  ,ifnull(T.M7_ENG_VOL ,0) + ifnull(T.M8_ENG_VOL ,0) + ifnull(T.M9_ENG_VOL ,0)    Q3_ENG_VOL
  ,ifnull(T.M10_ENG_VOL,0) + ifnull(T.M11_ENG_VOL,0) + ifnull(T.M12_ENG_VOL,0) Q4_ENG_VOL
from 
  (SELECT 
    ID_VEHICLE, VRN, YEAR
    ,max(M1_TON_KM_SUM)  M1_TON_KM,    max(M1_GCO2_SUM)   M1_GCO2,  max(M1_MJ_SUM)  M1_MJ,  max(M1_ENG_VOL_SUM)  M1_ENG_VOL
    ,max(M2_TON_KM_SUM)  M2_TON_KM,    max(M2_GCO2_SUM)   M2_GCO2,  max(M2_MJ_SUM)  M2_MJ,  max(M2_ENG_VOL_SUM)  M2_ENG_VOL
    ,max(M3_TON_KM_SUM)  M3_TON_KM,    max(M3_GCO2_SUM)   M3_GCO2,  max(M3_MJ_SUM)  M3_MJ,  max(M3_ENG_VOL_SUM)  M3_ENG_VOL
    ,max(M4_TON_KM_SUM)  M4_TON_KM,    max(M4_GCO2_SUM)   M4_GCO2,  max(M4_MJ_SUM)  M4_MJ,  max(M4_ENG_VOL_SUM)  M4_ENG_VOL
    ,max(M5_TON_KM_SUM)  M5_TON_KM,    max(M5_GCO2_SUM)   M5_GCO2,  max(M5_MJ_SUM)  M5_MJ,  max(M5_ENG_VOL_SUM)  M5_ENG_VOL
    ,max(M6_TON_KM_SUM)  M6_TON_KM,    max(M6_GCO2_SUM)   M6_GCO2,  max(M6_MJ_SUM)  M6_MJ,  max(M6_ENG_VOL_SUM)  M6_ENG_VOL
    ,max(M7_TON_KM_SUM)  M7_TON_KM,    max(M7_GCO2_SUM)   M7_GCO2,  max(M7_MJ_SUM)  M7_MJ,  max(M7_ENG_VOL_SUM)  M7_ENG_VOL
    ,max(M8_TON_KM_SUM)  M8_TON_KM,    max(M8_GCO2_SUM)   M8_GCO2,  max(M8_MJ_SUM)  M8_MJ,  max(M8_ENG_VOL_SUM)  M8_ENG_VOL
    ,max(M9_TON_KM_SUM)  M9_TON_KM,    max(M9_GCO2_SUM)   M9_GCO2,  max(M9_MJ_SUM)  M9_MJ,  max(M9_ENG_VOL_SUM)  M9_ENG_VOL
    ,max(M10_TON_KM_SUM) M10_TON_KM,   max(M10_GCO2_SUM)  M10_GCO2, max(M10_MJ_SUM) M10_MJ, max(M10_ENG_VOL_SUM) M10_ENG_VOL
    ,max(M11_TON_KM_SUM) M11_TON_KM,   max(M11_GCO2_SUM)  M11_GCO2, max(M11_MJ_SUM) M11_MJ, max(M11_ENG_VOL_SUM) M11_ENG_VOL
    ,max(M12_TON_KM_SUM) M12_TON_KM,   max(M12_GCO2_SUM)  M12_GCO2, max(M12_MJ_SUM) M12_MJ, max(M12_ENG_VOL_SUM) M12_ENG_VOL
  FROM
  (
    select 
      -- ID_LOGIS_TRANS_MON_INFO
      -- ,DATE_MON -- 날짜: YYYYMM
      ID_VEHICLE -- 차량ID
      ,VRN -- 차량번호
      ,YEAR -- 날짜: YYYY
      -- ,MON -- 날짜: MM
      -- ,SELFVRN_CD -- 자차여부
      -- ,LOGI_YCT_CD -- 노선구분(분정기/비정기)
      -- ,LOGI_WEI_CD -- 톤수
      ,case when (MON=1) then TON_KM_SUM else null end  as M1_TON_KM_SUM  -- 톤킬로, 온실가스원단위
      ,case when (MON=1) then GCO2_SUM else null end    as M1_GCO2_SUM  -- 온실가스배출량(gCO2)
      ,case when (MON=1) then MJ_SUM else null end      as M1_MJ_SUM  -- 에너지사용량(MJ)
      ,case when (MON=1) then ENG_VOL_SUM else null end as M1_ENG_VOL_SUM  -- 연료사용량

      ,case when (MON=2) then TON_KM_SUM else null end  as M2_TON_KM_SUM  -- 톤킬로, 온실가스원단위
      ,case when (MON=2) then GCO2_SUM else null end    as M2_GCO2_SUM  -- 온실가스배출량(gCO2)
      ,case when (MON=2) then MJ_SUM else null end      as M2_MJ_SUM  -- 에너지사용량(MJ)
      ,case when (MON=2) then ENG_VOL_SUM else null end as M2_ENG_VOL_SUM  -- 연료사용량
      
      ,case when (MON=3) then TON_KM_SUM else null end  as M3_TON_KM_SUM  -- 톤킬로, 온실가스원단위
      ,case when (MON=3) then GCO2_SUM else null end    as M3_GCO2_SUM  -- 온실가스배출량(gCO2)
      ,case when (MON=3) then MJ_SUM else null end      as M3_MJ_SUM  -- 에너지사용량(MJ)
      ,case when (MON=3) then ENG_VOL_SUM else null end as M3_ENG_VOL_SUM  -- 연료사용량
      
      ,case when (MON=4) then TON_KM_SUM else null end  as M4_TON_KM_SUM  -- 톤킬로, 온실가스원단위
      ,case when (MON=4) then GCO2_SUM else null end    as M4_GCO2_SUM  -- 온실가스배출량(gCO2)
      ,case when (MON=4) then MJ_SUM else null end      as M4_MJ_SUM  -- 에너지사용량(MJ)
      ,case when (MON=4) then ENG_VOL_SUM else null end as M4_ENG_VOL_SUM  -- 연료사용량
      
      ,case when (MON=5) then TON_KM_SUM else null end  as M5_TON_KM_SUM  -- 톤킬로, 온실가스원단위
      ,case when (MON=5) then GCO2_SUM else null end    as M5_GCO2_SUM  -- 온실가스배출량(gCO2)
      ,case when (MON=5) then MJ_SUM else null end      as M5_MJ_SUM  -- 에너지사용량(MJ)
      ,case when (MON=5) then ENG_VOL_SUM else null end as M5_ENG_VOL_SUM  -- 연료사용량
      
      ,case when (MON=6) then TON_KM_SUM else null end  as M6_TON_KM_SUM  -- 톤킬로, 온실가스원단위
      ,case when (MON=6) then GCO2_SUM else null end    as M6_GCO2_SUM  -- 온실가스배출량(gCO2)
      ,case when (MON=6) then MJ_SUM else null end      as M6_MJ_SUM  -- 에너지사용량(MJ)
      ,case when (MON=6) then ENG_VOL_SUM else null end as M6_ENG_VOL_SUM  -- 연료사용량
      
      ,case when (MON=7) then TON_KM_SUM else null end  as M7_TON_KM_SUM  -- 톤킬로, 온실가스원단위
      ,case when (MON=7) then GCO2_SUM else null end    as M7_GCO2_SUM  -- 온실가스배출량(gCO2)
      ,case when (MON=7) then MJ_SUM else null end      as M7_MJ_SUM  -- 에너지사용량(MJ)
      ,case when (MON=7) then ENG_VOL_SUM else null end as M7_ENG_VOL_SUM  -- 연료사용량
      
      ,case when (MON=8) then TON_KM_SUM else null end  as M8_TON_KM_SUM  -- 톤킬로, 온실가스원단위
      ,case when (MON=8) then GCO2_SUM else null end    as M8_GCO2_SUM  -- 온실가스배출량(gCO2)
      ,case when (MON=8) then MJ_SUM else null end      as M8_MJ_SUM  -- 에너지사용량(MJ)
      ,case when (MON=8) then ENG_VOL_SUM else null end as M8_ENG_VOL_SUM  -- 연료사용량
      
      ,case when (MON=9) then TON_KM_SUM else null end  as M9_TON_KM_SUM  -- 톤킬로, 온실가스원단위
      ,case when (MON=9) then GCO2_SUM else null end    as M9_GCO2_SUM  -- 온실가스배출량(gCO2)
      ,case when (MON=9) then MJ_SUM else null end      as M9_MJ_SUM  -- 에너지사용량(MJ)
      ,case when (MON=9) then ENG_VOL_SUM else null end as M9_ENG_VOL_SUM  -- 연료사용량
      
      ,case when (MON=10) then TON_KM_SUM else null end  as M10_TON_KM_SUM  -- 톤킬로, 온실가스원단위
      ,case when (MON=10) then GCO2_SUM else null end    as M10_GCO2_SUM  -- 온실가스배출량(gCO2)
      ,case when (MON=10) then MJ_SUM else null end      as M10_MJ_SUM  -- 에너지사용량(MJ)
      ,case when (MON=10) then ENG_VOL_SUM else null end as M10_ENG_VOL_SUM  -- 연료사용량
      
      ,case when (MON=11) then TON_KM_SUM else null end  as M11_TON_KM_SUM  -- 톤킬로, 온실가스원단위
      ,case when (MON=11) then GCO2_SUM else null end    as M11_GCO2_SUM  -- 온실가스배출량(gCO2)
      ,case when (MON=11) then MJ_SUM else null end      as M11_MJ_SUM  -- 에너지사용량(MJ)
      ,case when (MON=11) then ENG_VOL_SUM else null end as M11_ENG_VOL_SUM  -- 연료사용량
      
      ,case when (MON=12) then TON_KM_SUM else null end  as M12_TON_KM_SUM  -- 톤킬로, 온실가스원단위
      ,case when (MON=12) then GCO2_SUM else null end    as M12_GCO2_SUM  -- 온실가스배출량(gCO2)
      ,case when (MON=12) then MJ_SUM else null end      as M12_MJ_SUM  -- 에너지사용량(MJ)
      ,case when (MON=12) then ENG_VOL_SUM else null end as M12_ENG_VOL_SUM  -- 연료사용량
         
    from 
      V_LOGIS_EFF_SUB E) T
    group by ID_VEHICLE, VRN, YEAR  ) T 
  LEFT JOIN T_VEHICLE V
    ON T.ID_VEHICLE=V.ID_VEHICLE
  LEFT JOIN V_SITE_DEPT D
    ON V.ID_ERP_DEPT=D.ID_ERP_DEPT
;


-- =====================================================================
-- 11/8 작성: 분기별
-- 11/11 수정: ,D.ID_SITE AS ID_SITE 추가
-- create 
-- alter
-- view V_LOGIS_EFF as
select 
  V.ID_VEHICLE,V.S_VRN VRN,T.YEAR
  ,D.ID_SITE AS ID_SITE
  ,D.S_SITE_ID SITE_ID
  ,D.S_SITE_NM SITE_NM
  ,V.S_CAR_TYPE CAR_TYPE
  ,(select S_DESC from T_CODE where S_CAT='CAR_TYPE' and S_CD=V.S_CAR_TYPE) CAR_TYPE_NM
  ,V.S_SELFVRN_CD SELFVRN_CD -- 자차여부
  ,(select S_DESC from T_CODE where S_CAT='SELFVRN' and S_CD=V.S_SELFVRN_CD) SELFVRN_NM
  ,V.S_LINE_CD LINE_CD -- 노선구분(분정기/비정기)
  ,(select S_DESC from T_CODE where S_CAT='LOGI_YCT' and S_CD=V.S_LINE_CD) LINE_NM
  ,V.S_LOGI_WEI_CD LOGI_WEI_CD -- 톤수
  ,(select S_DESC from T_CODE where S_CAT='LOGI_WEI' and S_CD=V.S_LOGI_WEI_CD) LOGI_WEI_NM
  ,T.M1_TON_KM,  T.M1_GCO2,  T.M1_MJ,  T.M1_ENG_VOL 
  ,T.M2_TON_KM,  T.M2_GCO2,  T.M2_MJ,  T.M2_ENG_VOL 
  ,T.M3_TON_KM,  T.M3_GCO2,  T.M3_MJ,  T.M3_ENG_VOL
  ,T.M4_TON_KM,  T.M4_GCO2,  T.M4_MJ,  T.M4_ENG_VOL
  ,T.M5_TON_KM,  T.M5_GCO2,  T.M5_MJ,  T.M5_ENG_VOL
  ,T.M6_TON_KM,  T.M6_GCO2,  T.M6_MJ,  T.M6_ENG_VOL
  ,T.M7_TON_KM,  T.M7_GCO2,  T.M7_MJ,  T.M7_ENG_VOL
  ,T.M8_TON_KM,  T.M8_GCO2,  T.M8_MJ,  T.M8_ENG_VOL
  ,T.M9_TON_KM,  T.M9_GCO2,  T.M9_MJ,  T.M9_ENG_VOL
  ,T.M10_TON_KM, T.M10_GCO2, T.M10_MJ, T.M10_ENG_VOL
  ,T.M11_TON_KM, T.M11_GCO2, T.M11_MJ, T.M11_ENG_VOL
  ,T.M12_TON_KM, T.M12_GCO2, T.M12_MJ, T.M12_ENG_VOL
  ,ifnull(T.M1_TON_KM  ,0) + ifnull(T.M2_TON_KM  ,0) + ifnull(T.M3_TON_KM  ,0)  Q1_TON_KM
  ,ifnull(T.M4_TON_KM  ,0) + ifnull(T.M5_TON_KM  ,0) + ifnull(T.M6_TON_KM  ,0)  Q2_TON_KM
  ,ifnull(T.M7_TON_KM  ,0) + ifnull(T.M8_TON_KM  ,0) + ifnull(T.M9_TON_KM  ,0)  Q3_TON_KM
  ,ifnull(T.M10_TON_KM ,0) + ifnull(T.M11_TON_KM ,0) + ifnull(T.M12_TON_KM ,0)  Q4_TON_KM
  ,ifnull(T.M1_GCO2    ,0) + ifnull(T.M2_GCO2    ,0) + ifnull(T.M3_GCO2    ,0)  Q1_GCO2
  ,ifnull(T.M4_GCO2    ,0) + ifnull(T.M5_GCO2    ,0) + ifnull(T.M6_GCO2    ,0)  Q2_GCO2
  ,ifnull(T.M7_GCO2    ,0) + ifnull(T.M8_GCO2    ,0) + ifnull(T.M9_GCO2    ,0)  Q3_GCO2
  ,ifnull(T.M10_GCO2   ,0) + ifnull(T.M11_GCO2   ,0) + ifnull(T.M12_GCO2   ,0)  Q4_GCO2
  ,ifnull(T.M1_MJ      ,0) + ifnull(T.M2_MJ      ,0) + ifnull(T.M3_MJ      ,0)  Q1_MJ 
  ,ifnull(T.M4_MJ      ,0) + ifnull(T.M5_MJ      ,0) + ifnull(T.M6_MJ      ,0)  Q2_MJ 
  ,ifnull(T.M7_MJ      ,0) + ifnull(T.M8_MJ      ,0) + ifnull(T.M9_MJ      ,0)  Q3_MJ 
  ,ifnull(T.M10_MJ     ,0) + ifnull(T.M11_MJ     ,0) + ifnull(T.M12_MJ     ,0)  Q4_MJ 
  ,ifnull(T.M1_ENG_VOL ,0) + ifnull(T.M2_ENG_VOL ,0) + ifnull(T.M3_ENG_VOL ,0)  Q1_ENG_VOL
  ,ifnull(T.M4_ENG_VOL ,0) + ifnull(T.M5_ENG_VOL ,0) + ifnull(T.M6_ENG_VOL ,0)  Q2_ENG_VOL
  ,ifnull(T.M7_ENG_VOL ,0) + ifnull(T.M8_ENG_VOL ,0) + ifnull(T.M9_ENG_VOL ,0)  Q3_ENG_VOL
  ,ifnull(T.M10_ENG_VOL,0) + ifnull(T.M11_ENG_VOL,0) + ifnull(T.M12_ENG_VOL,0)  Q4_ENG_VOL
from 
  (SELECT 
    ID_VEHICLE, VRN, YEAR
    ,max(M1_TON_KM_SUM)  M1_TON_KM,    max(M1_GCO2_SUM)   M1_GCO2,  max(M1_MJ_SUM)  M1_MJ,  max(M1_ENG_VOL_SUM)  M1_ENG_VOL
    ,max(M2_TON_KM_SUM)  M2_TON_KM,    max(M2_GCO2_SUM)   M2_GCO2,  max(M2_MJ_SUM)  M2_MJ,  max(M2_ENG_VOL_SUM)  M2_ENG_VOL
    ,max(M3_TON_KM_SUM)  M3_TON_KM,    max(M3_GCO2_SUM)   M3_GCO2,  max(M3_MJ_SUM)  M3_MJ,  max(M3_ENG_VOL_SUM)  M3_ENG_VOL
    ,max(M4_TON_KM_SUM)  M4_TON_KM,    max(M4_GCO2_SUM)   M4_GCO2,  max(M4_MJ_SUM)  M4_MJ,  max(M4_ENG_VOL_SUM)  M4_ENG_VOL
    ,max(M5_TON_KM_SUM)  M5_TON_KM,    max(M5_GCO2_SUM)   M5_GCO2,  max(M5_MJ_SUM)  M5_MJ,  max(M5_ENG_VOL_SUM)  M5_ENG_VOL
    ,max(M6_TON_KM_SUM)  M6_TON_KM,    max(M6_GCO2_SUM)   M6_GCO2,  max(M6_MJ_SUM)  M6_MJ,  max(M6_ENG_VOL_SUM)  M6_ENG_VOL
    ,max(M7_TON_KM_SUM)  M7_TON_KM,    max(M7_GCO2_SUM)   M7_GCO2,  max(M7_MJ_SUM)  M7_MJ,  max(M7_ENG_VOL_SUM)  M7_ENG_VOL
    ,max(M8_TON_KM_SUM)  M8_TON_KM,    max(M8_GCO2_SUM)   M8_GCO2,  max(M8_MJ_SUM)  M8_MJ,  max(M8_ENG_VOL_SUM)  M8_ENG_VOL
    ,max(M9_TON_KM_SUM)  M9_TON_KM,    max(M9_GCO2_SUM)   M9_GCO2,  max(M9_MJ_SUM)  M9_MJ,  max(M9_ENG_VOL_SUM)  M9_ENG_VOL
    ,max(M10_TON_KM_SUM) M10_TON_KM,   max(M10_GCO2_SUM)  M10_GCO2, max(M10_MJ_SUM) M10_MJ, max(M10_ENG_VOL_SUM) M10_ENG_VOL
    ,max(M11_TON_KM_SUM) M11_TON_KM,   max(M11_GCO2_SUM)  M11_GCO2, max(M11_MJ_SUM) M11_MJ, max(M11_ENG_VOL_SUM) M11_ENG_VOL
    ,max(M12_TON_KM_SUM) M12_TON_KM,   max(M12_GCO2_SUM)  M12_GCO2, max(M12_MJ_SUM) M12_MJ, max(M12_ENG_VOL_SUM) M12_ENG_VOL
  FROM
  (
    select 
      -- ID_LOGIS_TRANS_MON_INFO
      -- ,DATE_MON -- 날짜: YYYYMM
      ID_VEHICLE -- 차량ID
      ,VRN -- 차량번호
      ,YEAR -- 날짜: YYYY
      -- ,MON -- 날짜: MM
      -- ,SELFVRN_CD -- 자차여부
      -- ,LOGI_YCT_CD -- 노선구분(분정기/비정기)
      -- ,LOGI_WEI_CD -- 톤수
      ,case when (MON=1) then TON_KM_SUM else null end  as M1_TON_KM_SUM  -- 톤킬로, 온실가스원단위
      ,case when (MON=1) then GCO2_SUM else null end    as M1_GCO2_SUM  -- 온실가스배출량(gCO2)
      ,case when (MON=1) then MJ_SUM else null end      as M1_MJ_SUM  -- 에너지사용량(MJ)
      ,case when (MON=1) then ENG_VOL_SUM else null end as M1_ENG_VOL_SUM  -- 연료사용량

      ,case when (MON=2) then TON_KM_SUM else null end  as M2_TON_KM_SUM  -- 톤킬로, 온실가스원단위
      ,case when (MON=2) then GCO2_SUM else null end    as M2_GCO2_SUM  -- 온실가스배출량(gCO2)
      ,case when (MON=2) then MJ_SUM else null end      as M2_MJ_SUM  -- 에너지사용량(MJ)
      ,case when (MON=2) then ENG_VOL_SUM else null end as M2_ENG_VOL_SUM  -- 연료사용량
      
      ,case when (MON=3) then TON_KM_SUM else null end  as M3_TON_KM_SUM  -- 톤킬로, 온실가스원단위
      ,case when (MON=3) then GCO2_SUM else null end    as M3_GCO2_SUM  -- 온실가스배출량(gCO2)
      ,case when (MON=3) then MJ_SUM else null end      as M3_MJ_SUM  -- 에너지사용량(MJ)
      ,case when (MON=3) then ENG_VOL_SUM else null end as M3_ENG_VOL_SUM  -- 연료사용량
      
      ,case when (MON=4) then TON_KM_SUM else null end  as M4_TON_KM_SUM  -- 톤킬로, 온실가스원단위
      ,case when (MON=4) then GCO2_SUM else null end    as M4_GCO2_SUM  -- 온실가스배출량(gCO2)
      ,case when (MON=4) then MJ_SUM else null end      as M4_MJ_SUM  -- 에너지사용량(MJ)
      ,case when (MON=4) then ENG_VOL_SUM else null end as M4_ENG_VOL_SUM  -- 연료사용량
      
      ,case when (MON=5) then TON_KM_SUM else null end  as M5_TON_KM_SUM  -- 톤킬로, 온실가스원단위
      ,case when (MON=5) then GCO2_SUM else null end    as M5_GCO2_SUM  -- 온실가스배출량(gCO2)
      ,case when (MON=5) then MJ_SUM else null end      as M5_MJ_SUM  -- 에너지사용량(MJ)
      ,case when (MON=5) then ENG_VOL_SUM else null end as M5_ENG_VOL_SUM  -- 연료사용량
      
      ,case when (MON=6) then TON_KM_SUM else null end  as M6_TON_KM_SUM  -- 톤킬로, 온실가스원단위
      ,case when (MON=6) then GCO2_SUM else null end    as M6_GCO2_SUM  -- 온실가스배출량(gCO2)
      ,case when (MON=6) then MJ_SUM else null end      as M6_MJ_SUM  -- 에너지사용량(MJ)
      ,case when (MON=6) then ENG_VOL_SUM else null end as M6_ENG_VOL_SUM  -- 연료사용량
      
      ,case when (MON=7) then TON_KM_SUM else null end  as M7_TON_KM_SUM  -- 톤킬로, 온실가스원단위
      ,case when (MON=7) then GCO2_SUM else null end    as M7_GCO2_SUM  -- 온실가스배출량(gCO2)
      ,case when (MON=7) then MJ_SUM else null end      as M7_MJ_SUM  -- 에너지사용량(MJ)
      ,case when (MON=7) then ENG_VOL_SUM else null end as M7_ENG_VOL_SUM  -- 연료사용량
      
      ,case when (MON=8) then TON_KM_SUM else null end  as M8_TON_KM_SUM  -- 톤킬로, 온실가스원단위
      ,case when (MON=8) then GCO2_SUM else null end    as M8_GCO2_SUM  -- 온실가스배출량(gCO2)
      ,case when (MON=8) then MJ_SUM else null end      as M8_MJ_SUM  -- 에너지사용량(MJ)
      ,case when (MON=8) then ENG_VOL_SUM else null end as M8_ENG_VOL_SUM  -- 연료사용량
      
      ,case when (MON=9) then TON_KM_SUM else null end  as M9_TON_KM_SUM  -- 톤킬로, 온실가스원단위
      ,case when (MON=9) then GCO2_SUM else null end    as M9_GCO2_SUM  -- 온실가스배출량(gCO2)
      ,case when (MON=9) then MJ_SUM else null end      as M9_MJ_SUM  -- 에너지사용량(MJ)
      ,case when (MON=9) then ENG_VOL_SUM else null end as M9_ENG_VOL_SUM  -- 연료사용량
      
      ,case when (MON=10) then TON_KM_SUM else null end  as M10_TON_KM_SUM  -- 톤킬로, 온실가스원단위
      ,case when (MON=10) then GCO2_SUM else null end    as M10_GCO2_SUM  -- 온실가스배출량(gCO2)
      ,case when (MON=10) then MJ_SUM else null end      as M10_MJ_SUM  -- 에너지사용량(MJ)
      ,case when (MON=10) then ENG_VOL_SUM else null end as M10_ENG_VOL_SUM  -- 연료사용량
      
      ,case when (MON=11) then TON_KM_SUM else null end  as M11_TON_KM_SUM  -- 톤킬로, 온실가스원단위
      ,case when (MON=11) then GCO2_SUM else null end    as M11_GCO2_SUM  -- 온실가스배출량(gCO2)
      ,case when (MON=11) then MJ_SUM else null end      as M11_MJ_SUM  -- 에너지사용량(MJ)
      ,case when (MON=11) then ENG_VOL_SUM else null end as M11_ENG_VOL_SUM  -- 연료사용량
      
      ,case when (MON=12) then TON_KM_SUM else null end  as M12_TON_KM_SUM  -- 톤킬로, 온실가스원단위
      ,case when (MON=12) then GCO2_SUM else null end    as M12_GCO2_SUM  -- 온실가스배출량(gCO2)
      ,case when (MON=12) then MJ_SUM else null end      as M12_MJ_SUM  -- 에너지사용량(MJ)
      ,case when (MON=12) then ENG_VOL_SUM else null end as M12_ENG_VOL_SUM  -- 연료사용량
         
    from 
      V_LOGIS_EFF_SUB E) T
    group by ID_VEHICLE, VRN, YEAR  ) T 
  LEFT JOIN T_VEHICLE V
    ON T.ID_VEHICLE=V.ID_VEHICLE
  LEFT JOIN V_SITE_DEPT D
    ON V.ID_ERP_DEPT=D.ID_ERP_DEPT
;


-- ==================================================================
-- 11/8 작성
-- 11/29 수정: inner join으로 변경
-- create 
alter
view V_LOGIS_EFF_SUB as
select 
	M.ID_LOGIS_TRANS_MON_INFO
	, V.ID_VEHICLE -- 차량ID
	, V.S_VRN as VRN -- 차량번호
	, M.ID_ERP_DEPT -- ERP부서ID
	, M.DATE_MON -- 날짜: YYYYMM
	, substring(M.DATE_MON,1,4) as YEAR -- 날짜: YYYY
	, substring(M.DATE_MON,5,2) as MON -- 날짜: MM
	, M.TON_KM_SUM -- 톤킬로, 온실가스원단위
	, M.GCO2_SUM -- 온실가스배출량(gCO2)
	, M.MJ_SUM -- 에너지사용량(MJ)
	, M.ENG_VOL_SUM -- 연료사용량
	, V.S_SELFVRN_CD as SELFVRN_CD -- 자차여부
	, V.S_LINE_CD as LINE_CD -- 노선구분(분정기/비정기)
	, V.S_LOGI_WEI_CD as LOGI_WEI_CD -- 톤수
    , V.S_CAR_TYPE as CAR_TYPE
  
from T_LOGIS_TRANS_MON_INFO as M -- 월운송정보	-- select * from T_LOGIS_TRANS_MON_INFO
INNER JOIN T_VEHICLE as V -- 차량 master	-- select * from T_VEHICLE
	ON V.ID_VEHICLE = M.ID_VEHICLE
	-- LEFT JOIN V_SITE_DEPT D -- 사업장+부서정보
	--   ON V.ID_ERP_DEPT = D.ID_ERP_DEPT;
;
-- =====================================================================

select * from V_LOGIS_EFF;


-- =====================================================================
-- 11/8 작성: 분기별
-- 11/11 수정: ,D.ID_SITE AS ID_SITE 추가
-- 11/29 수정: 구조 변경
-- create 
alter
view V_LOGIS_EFF as
select 
	T.ID_VEHICLE
	,T.VRN
	,T.YEAR
	,D.ID_SITE AS ID_SITE
	,D.S_SITE_ID SITE_ID
	,D.S_SITE_NM SITE_NM
	,T.CAR_TYPE
	,(select S_DESC from T_CODE where S_CAT='CAR_TYPE' and S_CD=T.CAR_TYPE) CAR_TYPE_NM
	,T.SELFVRN_CD as SELFVRN_CD -- 자차여부
	,(select S_DESC from T_CODE where S_CAT='SELFVRN' and S_CD=T.SELFVRN_CD) SELFVRN_NM
	,T.LINE_CD as LINE_CD -- 노선구분(분정기/비정기)
	,(select S_DESC from T_CODE where S_CAT='LOGI_YCT' and S_CD=T.LINE_CD) LINE_NM
	,T.LOGI_WEI_CD as LOGI_WEI_CD -- 톤수
	,(select S_DESC from T_CODE where S_CAT='LOGI_WEI' and S_CD=T.LOGI_WEI_CD) LOGI_WEI_NM
	,T.M1_TON_KM,  T.M1_GCO2,  T.M1_MJ,  T.M1_ENG_VOL 
	,T.M2_TON_KM,  T.M2_GCO2,  T.M2_MJ,  T.M2_ENG_VOL 
	,T.M3_TON_KM,  T.M3_GCO2,  T.M3_MJ,  T.M3_ENG_VOL
	,T.M4_TON_KM,  T.M4_GCO2,  T.M4_MJ,  T.M4_ENG_VOL
	,T.M5_TON_KM,  T.M5_GCO2,  T.M5_MJ,  T.M5_ENG_VOL
	,T.M6_TON_KM,  T.M6_GCO2,  T.M6_MJ,  T.M6_ENG_VOL
	,T.M7_TON_KM,  T.M7_GCO2,  T.M7_MJ,  T.M7_ENG_VOL
	,T.M8_TON_KM,  T.M8_GCO2,  T.M8_MJ,  T.M8_ENG_VOL
	,T.M9_TON_KM,  T.M9_GCO2,  T.M9_MJ,  T.M9_ENG_VOL
	,T.M10_TON_KM, T.M10_GCO2, T.M10_MJ, T.M10_ENG_VOL
	,T.M11_TON_KM, T.M11_GCO2, T.M11_MJ, T.M11_ENG_VOL
	,T.M12_TON_KM, T.M12_GCO2, T.M12_MJ, T.M12_ENG_VOL
	,ifnull(T.M1_TON_KM  ,0) + ifnull(T.M2_TON_KM  ,0) + ifnull(T.M3_TON_KM  ,0)  Q1_TON_KM
	,ifnull(T.M4_TON_KM  ,0) + ifnull(T.M5_TON_KM  ,0) + ifnull(T.M6_TON_KM  ,0)  Q2_TON_KM
	,ifnull(T.M7_TON_KM  ,0) + ifnull(T.M8_TON_KM  ,0) + ifnull(T.M9_TON_KM  ,0)  Q3_TON_KM
	,ifnull(T.M10_TON_KM ,0) + ifnull(T.M11_TON_KM ,0) + ifnull(T.M12_TON_KM ,0)  Q4_TON_KM
	,ifnull(T.M1_GCO2    ,0) + ifnull(T.M2_GCO2    ,0) + ifnull(T.M3_GCO2    ,0)  Q1_GCO2
	,ifnull(T.M4_GCO2    ,0) + ifnull(T.M5_GCO2    ,0) + ifnull(T.M6_GCO2    ,0)  Q2_GCO2
	,ifnull(T.M7_GCO2    ,0) + ifnull(T.M8_GCO2    ,0) + ifnull(T.M9_GCO2    ,0)  Q3_GCO2
	,ifnull(T.M10_GCO2   ,0) + ifnull(T.M11_GCO2   ,0) + ifnull(T.M12_GCO2   ,0)  Q4_GCO2
	,ifnull(T.M1_MJ      ,0) + ifnull(T.M2_MJ      ,0) + ifnull(T.M3_MJ      ,0)  Q1_MJ 
	,ifnull(T.M4_MJ      ,0) + ifnull(T.M5_MJ      ,0) + ifnull(T.M6_MJ      ,0)  Q2_MJ 
	,ifnull(T.M7_MJ      ,0) + ifnull(T.M8_MJ      ,0) + ifnull(T.M9_MJ      ,0)  Q3_MJ 
	,ifnull(T.M10_MJ     ,0) + ifnull(T.M11_MJ     ,0) + ifnull(T.M12_MJ     ,0)  Q4_MJ 
	,ifnull(T.M1_ENG_VOL ,0) + ifnull(T.M2_ENG_VOL ,0) + ifnull(T.M3_ENG_VOL ,0)  Q1_ENG_VOL
	,ifnull(T.M4_ENG_VOL ,0) + ifnull(T.M5_ENG_VOL ,0) + ifnull(T.M6_ENG_VOL ,0)  Q2_ENG_VOL
	,ifnull(T.M7_ENG_VOL ,0) + ifnull(T.M8_ENG_VOL ,0) + ifnull(T.M9_ENG_VOL ,0)  Q3_ENG_VOL
	,ifnull(T.M10_ENG_VOL,0) + ifnull(T.M11_ENG_VOL,0) + ifnull(T.M12_ENG_VOL,0)  Q4_ENG_VOL
from (
    SELECT 
        ID_VEHICLE -- 차량ID
		, VRN -- 차량번호
		, YEAR -- 날짜: YYYY
		, SELFVRN_CD -- 자차여부
		, LINE_CD -- 노선구분(분정기/비정기)
		, LOGI_WEI_CD -- 톤수
        , CAR_TYPE
        , ID_ERP_DEPT
		,max(M1_TON_KM_SUM)  M1_TON_KM,    max(M1_GCO2_SUM)   M1_GCO2,  max(M1_MJ_SUM)  M1_MJ,  max(M1_ENG_VOL_SUM)  M1_ENG_VOL
		,max(M2_TON_KM_SUM)  M2_TON_KM,    max(M2_GCO2_SUM)   M2_GCO2,  max(M2_MJ_SUM)  M2_MJ,  max(M2_ENG_VOL_SUM)  M2_ENG_VOL
		,max(M3_TON_KM_SUM)  M3_TON_KM,    max(M3_GCO2_SUM)   M3_GCO2,  max(M3_MJ_SUM)  M3_MJ,  max(M3_ENG_VOL_SUM)  M3_ENG_VOL
		,max(M4_TON_KM_SUM)  M4_TON_KM,    max(M4_GCO2_SUM)   M4_GCO2,  max(M4_MJ_SUM)  M4_MJ,  max(M4_ENG_VOL_SUM)  M4_ENG_VOL
		,max(M5_TON_KM_SUM)  M5_TON_KM,    max(M5_GCO2_SUM)   M5_GCO2,  max(M5_MJ_SUM)  M5_MJ,  max(M5_ENG_VOL_SUM)  M5_ENG_VOL
		,max(M6_TON_KM_SUM)  M6_TON_KM,    max(M6_GCO2_SUM)   M6_GCO2,  max(M6_MJ_SUM)  M6_MJ,  max(M6_ENG_VOL_SUM)  M6_ENG_VOL
		,max(M7_TON_KM_SUM)  M7_TON_KM,    max(M7_GCO2_SUM)   M7_GCO2,  max(M7_MJ_SUM)  M7_MJ,  max(M7_ENG_VOL_SUM)  M7_ENG_VOL
		,max(M8_TON_KM_SUM)  M8_TON_KM,    max(M8_GCO2_SUM)   M8_GCO2,  max(M8_MJ_SUM)  M8_MJ,  max(M8_ENG_VOL_SUM)  M8_ENG_VOL
		,max(M9_TON_KM_SUM)  M9_TON_KM,    max(M9_GCO2_SUM)   M9_GCO2,  max(M9_MJ_SUM)  M9_MJ,  max(M9_ENG_VOL_SUM)  M9_ENG_VOL
		,max(M10_TON_KM_SUM) M10_TON_KM,   max(M10_GCO2_SUM)  M10_GCO2, max(M10_MJ_SUM) M10_MJ, max(M10_ENG_VOL_SUM) M10_ENG_VOL
		,max(M11_TON_KM_SUM) M11_TON_KM,   max(M11_GCO2_SUM)  M11_GCO2, max(M11_MJ_SUM) M11_MJ, max(M11_ENG_VOL_SUM) M11_ENG_VOL
		,max(M12_TON_KM_SUM) M12_TON_KM,   max(M12_GCO2_SUM)  M12_GCO2, max(M12_MJ_SUM) M12_MJ, max(M12_ENG_VOL_SUM) M12_ENG_VOL
	FROM (
		select 
			-- ID_LOGIS_TRANS_MON_INFO
			-- ,DATE_MON -- 날짜: YYYYMM
			ID_VEHICLE -- 차량ID
			, VRN -- 차량번호
			, YEAR -- 날짜: YYYY
			-- ,MON -- 날짜: MM
			, SELFVRN_CD -- 자차여부
			, LINE_CD -- 노선구분(분정기/비정기)
			, LOGI_WEI_CD -- 톤수
            , CAR_TYPE
            , ID_ERP_DEPT
			, case when (MON=1) then TON_KM_SUM else null end  as M1_TON_KM_SUM  -- 톤킬로, 온실가스원단위
			, case when (MON=1) then GCO2_SUM else null end    as M1_GCO2_SUM  -- 온실가스배출량(gCO2)
			, case when (MON=1) then MJ_SUM else null end      as M1_MJ_SUM  -- 에너지사용량(MJ)
			, case when (MON=1) then ENG_VOL_SUM else null end as M1_ENG_VOL_SUM  -- 연료사용량

			, case when (MON=2) then TON_KM_SUM else null end  as M2_TON_KM_SUM  -- 톤킬로, 온실가스원단위
			, case when (MON=2) then GCO2_SUM else null end    as M2_GCO2_SUM  -- 온실가스배출량(gCO2)
			, case when (MON=2) then MJ_SUM else null end      as M2_MJ_SUM  -- 에너지사용량(MJ)
			, case when (MON=2) then ENG_VOL_SUM else null end as M2_ENG_VOL_SUM  -- 연료사용량

			, case when (MON=3) then TON_KM_SUM else null end  as M3_TON_KM_SUM  -- 톤킬로, 온실가스원단위
			, case when (MON=3) then GCO2_SUM else null end    as M3_GCO2_SUM  -- 온실가스배출량(gCO2)
			, case when (MON=3) then MJ_SUM else null end      as M3_MJ_SUM  -- 에너지사용량(MJ)
			, case when (MON=3) then ENG_VOL_SUM else null end as M3_ENG_VOL_SUM  -- 연료사용량

			, case when (MON=4) then TON_KM_SUM else null end  as M4_TON_KM_SUM  -- 톤킬로, 온실가스원단위
			, case when (MON=4) then GCO2_SUM else null end    as M4_GCO2_SUM  -- 온실가스배출량(gCO2)
			, case when (MON=4) then MJ_SUM else null end      as M4_MJ_SUM  -- 에너지사용량(MJ)
			, case when (MON=4) then ENG_VOL_SUM else null end as M4_ENG_VOL_SUM  -- 연료사용량

			, case when (MON=5) then TON_KM_SUM else null end  as M5_TON_KM_SUM  -- 톤킬로, 온실가스원단위
			, case when (MON=5) then GCO2_SUM else null end    as M5_GCO2_SUM  -- 온실가스배출량(gCO2)
			, case when (MON=5) then MJ_SUM else null end      as M5_MJ_SUM  -- 에너지사용량(MJ)
			, case when (MON=5) then ENG_VOL_SUM else null end as M5_ENG_VOL_SUM  -- 연료사용량

			, case when (MON=6) then TON_KM_SUM else null end  as M6_TON_KM_SUM  -- 톤킬로, 온실가스원단위
			, case when (MON=6) then GCO2_SUM else null end    as M6_GCO2_SUM  -- 온실가스배출량(gCO2)
			, case when (MON=6) then MJ_SUM else null end      as M6_MJ_SUM  -- 에너지사용량(MJ)
			, case when (MON=6) then ENG_VOL_SUM else null end as M6_ENG_VOL_SUM  -- 연료사용량

			, case when (MON=7) then TON_KM_SUM else null end  as M7_TON_KM_SUM  -- 톤킬로, 온실가스원단위
			, case when (MON=7) then GCO2_SUM else null end    as M7_GCO2_SUM  -- 온실가스배출량(gCO2)
			, case when (MON=7) then MJ_SUM else null end      as M7_MJ_SUM  -- 에너지사용량(MJ)
			, case when (MON=7) then ENG_VOL_SUM else null end as M7_ENG_VOL_SUM  -- 연료사용량

			, case when (MON=8) then TON_KM_SUM else null end  as M8_TON_KM_SUM  -- 톤킬로, 온실가스원단위
			, case when (MON=8) then GCO2_SUM else null end    as M8_GCO2_SUM  -- 온실가스배출량(gCO2)
			, case when (MON=8) then MJ_SUM else null end      as M8_MJ_SUM  -- 에너지사용량(MJ)
			, case when (MON=8) then ENG_VOL_SUM else null end as M8_ENG_VOL_SUM  -- 연료사용량

			, case when (MON=9) then TON_KM_SUM else null end  as M9_TON_KM_SUM  -- 톤킬로, 온실가스원단위
			, case when (MON=9) then GCO2_SUM else null end    as M9_GCO2_SUM  -- 온실가스배출량(gCO2)
			, case when (MON=9) then MJ_SUM else null end      as M9_MJ_SUM  -- 에너지사용량(MJ)
			, case when (MON=9) then ENG_VOL_SUM else null end as M9_ENG_VOL_SUM  -- 연료사용량

			, case when (MON=10) then TON_KM_SUM else null end  as M10_TON_KM_SUM  -- 톤킬로, 온실가스원단위
			, case when (MON=10) then GCO2_SUM else null end    as M10_GCO2_SUM  -- 온실가스배출량(gCO2)
			, case when (MON=10) then MJ_SUM else null end      as M10_MJ_SUM  -- 에너지사용량(MJ)
			, case when (MON=10) then ENG_VOL_SUM else null end as M10_ENG_VOL_SUM  -- 연료사용량

			, case when (MON=11) then TON_KM_SUM else null end  as M11_TON_KM_SUM  -- 톤킬로, 온실가스원단위
			, case when (MON=11) then GCO2_SUM else null end    as M11_GCO2_SUM  -- 온실가스배출량(gCO2)
			, case when (MON=11) then MJ_SUM else null end      as M11_MJ_SUM  -- 에너지사용량(MJ)
			, case when (MON=11) then ENG_VOL_SUM else null end as M11_ENG_VOL_SUM  -- 연료사용량

			, case when (MON=12) then TON_KM_SUM else null end  as M12_TON_KM_SUM  -- 톤킬로, 온실가스원단위
			, case when (MON=12) then GCO2_SUM else null end    as M12_GCO2_SUM  -- 온실가스배출량(gCO2)
			, case when (MON=12) then MJ_SUM else null end      as M12_MJ_SUM  -- 에너지사용량(MJ)
			, case when (MON=12) then ENG_VOL_SUM else null end as M12_ENG_VOL_SUM  -- 연료사용량

		from V_LOGIS_EFF_SUB as E
	) T
	group by ID_VEHICLE, VRN, YEAR, SELFVRN_CD, LINE_CD, LOGI_WEI_CD, CAR_TYPE, ID_ERP_DEPT
) T 
-- LEFT JOIN T_VEHICLE V
-- ON T.ID_VEHICLE=V.ID_VEHICLE
LEFT JOIN V_SITE_DEPT D
	ON T.ID_ERP_DEPT=D.ID_ERP_DEPT
;