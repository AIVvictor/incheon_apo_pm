/*

	V_LOGIS_DRV_LOG 
	월 운송정보 - 화물차

*/

-- CREATE 
-- alter 
-- VIEW V_LOGIS_DRV_LOG AS
SELECT LTM.ID_LOGIS_TRANS_MON_INFO AS ID_LOGIS_TRANS_MON_INFO,
          LTM.DATE_MON AS DATE_MON,
          V.ID_VEHICLE AS ID_VEHICLE,
          V.S_VRN AS S_VRN,
          V.S_LINE_CD AS S_LINE_CD,
          (SELECT TC.S_DESC FROM T_CODE TC WHERE TC.S_CAT = 'LOGI_YCT' AND TC.S_CD = V.S_LINE_CD) AS LINE_NM,
          V.ID_TRANS_COM AS ID_TRANS_COM,
          TC.S_TRANS_COM_NM AS S_TRANS_COM_NM,
          ifnull(CAST(LTM.FUEL_EFF as VARCHAR(100)),0) AS S_FUEL_EFF, -- 공인연비
          LTM.LOGI_WEI_CD AS S_LOGI_WEI_CD,
          (SELECT TC.S_DESC FROM T_CODE TC WHERE     TC.S_CAT = 'LOGI_WEI' AND TC.S_CD = LTM.LOGI_WEI_CD) AS TON_NM,
          LTM.SELFVRN_CD AS S_SELFVRN_CD,
          (SELECT TC.S_DESC FROM T_CODE TC WHERE     TC.S_CAT = 'SELFVRN' AND TC.S_CD = LTM.SELFVRN_CD) AS SELFVRN_NM,
          LTM.TRANS_TYPE_CD AS TRANS_TYPE_CD, 
          (SELECT TC.S_DESC FROM T_CODE TC WHERE     TC.S_CAT = 'TRANS_TYPE' AND TC.S_CD = LTM.TRANS_TYPE_CD) AS TRANS_TYPE_NM,
          LTM.FARE_SUM AS FARE_SUM,
          LTM.LOADAGE_SUM AS LOADAGE_SUM,
          ifnull(LTM.DISTANCE_SUM,0) AS EMPTY_DISTANCE_SUM,
          ifnull(LTM.VACANT_DISTANCE_SUM,0) AS VACANT_DISTANCE_SUM,
          ifnull(LTM.DISTANCE_SUM,0) + ifnull(LTM.VACANT_DISTANCE_SUM,0) AS DISTANCE_SUM,
          ifnull(LTM.TOE_SUM,0) AS TOE_SUM,
          ifnull(LTM.ENG_VOL_SUM,0) AS ENG_VOL_SUM,
          ifnull(LTM.MJ_SUM,0) AS MJ_SUM,
          ifnull(LTM.GCO2_SUM,0) AS GCO2_SUM,
          LTM.BAS_UNT AS BAS_UNT
          ,LTM.BASE_VAL AS BASE_VAL
          ,V.ID_ERP_DEPT	-- ERP부서ID
          ,SI.S_SITE_NM		-- 부서명
          ,SI.ID_SITE		-- 부서ID
          , EP.S_POINT_NM	-- 배출시설명(선박명,철도명)
	FROM T_SITE_INFO as SI
    INNER join T_ENG_POINT as EP
		on SI.ID_SITE = EP.ID_SITE
	INNER JOIN T_LOGIS_TRANS_MON_INFO as LTM	-- select * from T_LOGIS_TRANS_MON_INFO limit 0, 100
		on EP.ID_ENG_POINT = LTM.ID_ENG_POINT
	LEFT JOIN T_VEHICLE V	-- select * from T_VEHICLE
		ON LTM.ID_VEHICLE = V.ID_VEHICLE
        -- and V.C_DEL_YN != 'Y'
	LEFT JOIN T_TRANS_COM TC
		ON LTM.ID_TRANS_COM = TC.ID_TRANS_COM
        and TC.C_DEL_YN != 'Y'
	where SI.C_DEL_YN != 'Y'
		and EP.C_DEL_YN != 'Y'
            
# where trans_type_cd = 2
#		LEFT JOIN V_SITE_DEPT SD
#			ON V.ID_ERP_DEPT=SD.ID_ERP_DEPT	
# select * from T_LOGIS_TRANS_MON_INFO where trans_type_cd = 2
;

            
-- =======================            
select * from V_SITE_DEPT
;
select * from T_VEHICLE
;
select * from V_VEHICLE
where ID_SITE=2
-- where S_CAR_STATUS_CD=1
;
select * from     
    V_VEHICLE V -- 차량Master
where 
    -- ID_VEHICLE='226374'
    S_VRN='경남81사7216'
;

select * from V_LOGIS_DRV_LOG 
where DATE_MON='201911';