/*
	년월 계산 함수
    기준이 될 년월(a)과 더해질 월간격(b)을 입력받아 a에 b만큼 더해진 값을 리턴한다.
    (ex: a='201901', b='-3' => ret='201810')
    
    IN PARAM:
		IN_YYYYMM VARCHAR(6) -- 년월: '201901', '201902',...
		IN_INTERVAL DECIMAL(20,6) -- 월단위 조회값: -3, -1, 3, 4,...
	OUT
		IN_YYYYMM VARCHAR(6)	-- 년월: '201901', '201902'...
	
    EXAMPLE:
		FN_CALC_ADD_YM('201901', -3) => '201810'
        FN_CALC_ADD_YM('201901', 3)  => '201904'
        FN_CALC_ADD_YM('201810', 5)  => '201903'
*/

delimiter $$
drop function FN_CALC_ADD_YM;
CREATE FUNCTION `FN_CALC_ADD_YM`(
    IN_YYYYMM VARCHAR(6) -- 년월: '201901', '201902',...
    , IN_INTERVAL int -- 월단위 조회값: -3, -1, 3, 4,...
) RETURNS VARCHAR(6)
BEGIN
    DECLARE YYYY VARCHAR(4);
    DECLARE MM VARCHAR(2);
    DECLARE YYYYMMDD_DATE date;
    DECLARE ret VARCHAR(6);
    
    set YYYY = SUBSTRING(IN_YYYYMM, 1, 4);
    set MM   = SUBSTRING(IN_YYYYMM, 5, 2);
    set YYYYMMDD_DATE = STR_TO_DATE(concat(YYYY, '-', MM, '-01'), '%Y-%m-%d');	-- YYYYMM01 -> date 변환
#    call SP_LOG('FN_CALC_ADD_YM', concat(IN_YYYYMM, ' / ', YYYY, ' / ', MM, ' / ', YYYYMMDD_DATE));

    set ret = DATE_FORMAT(date_add(YYYYMMDD_DATE, interval IN_INTERVAL month), '%Y%m');	-- interval month 적용 후 varchar(yyyy-mm) 변환
    return ret;
END $$
delimiter ;


select FN_CALC_ADD_YM('201901', -3);

#select * from T_DEBUG_LOG order by seq desc