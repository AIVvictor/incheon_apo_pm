
set @YYYYMM='201911';    
set @YYYY=substring(@YYYYMM,1,4);
set @MM=substring(@YYYYMM,5,2);

select @YYYYMM,@YYYY,@MM;

  /*
    화물차 data 생성
  */

  insert into 
  T_LOGIS_TRANS_MON_INFO
      (
        -- ID_LOGIS_TRANS_MON_INFO
       ID_TRANS_COM  -- 운수사ID
       ,ID_ENG_POINT  -- 배출시설ID
       ,ID_ERP_DEPT  -- EPR부서ID
       ,ID_VEHICLE -- 차량ID
       ,DATE_MON -- 날짜
       ,SELFVRN_CD -- 자차여부
       ,VRN -- 차량VRN
       ,TRANS_TYPE_CD  -- 운송수단1=화물차
       ,LOGI_YCT_CD -- 노선구분,정기/비정기
       ,LOGI_WEI_CD -- 톤수
       ,FARE_SUM -- 운임료
       ,LOADAGE_SUM  -- 적재량
       -- ,WEIGHT_SUM -- 총중량
       ,VACANT_DISTANCE_SUM -- 공차운행거리
       ,DISTANCE_SUM -- 영차운행거리
       ,REG_MOD -- 등록/수정일시,YYYYMMDDHHmiSS
       ,FUEL_EFF -- 연비
       ,ENG_VOL_SUM -- 연료사용량
       ,MJ_SUM -- 에너지사용량(MJ)
       ,GCO2_SUM -- 온실가스배출량(gCO2)
       ,TON_KM_SUM -- 톤킬로, 온실가스원단위
       ,TOE_SUM -- 환산톤
       ,BASE_VAL -- 원단위 값
	   -- ,BAS_UNT -- 원단위 
       -- ,S_REG_ID -- 등록자ID
       -- ,S_MOD_ID -- 수정자ID
       -- ,C_DEL_YN
       )
select * from (
	select 
		ID_TRANS_COM -- 운수사ID
		,ID_ENG_POINT -- 배출시설ID
		,ID_ERP_DEPT -- EPR부서ID
		,ID_VEHICLE -- 차량ID
		-- ,substring(T.C_DATE,1,6) YM_DATE1 -- 날짜
		,YM_DATE -- 날짜
		,S_SELFVRN_CD -- 자차여부
		,S_VRN  -- 차량VRN
		-- ,T.S_TRANS_TYPE_CD -- 운송수단
		,S_TRANS_TYPE_CD -- 운송수단1=화물차
		,S_LINE_CD -- 노선구분,정기/비정기
		,S_LOGI_WEI_CD -- 톤수
		-- ,T.S_START_PNT -- 출발지
		-- ,T.S_DEST_PNT -- 도착지
		,SUM_FARE -- 운임료
		,SUM_LOADAGE -- 적재량
		,SUM_VACANT_DISTANCE -- 공차운행거리
		,SUM_DISTANCE -- 영차운행거리
		,REG_MOD -- 등록/수정일시,YYYYMMDDHHmiSS
		,S_FUEL_EFF -- 연비
		,SUM_ENG_VOL -- 연료사용량
        ,FN_ENG_CONVERSION('GJ',  '2', DATE_FORMAT(now(), '%Y%m%d'), SUM_ENG_VOL) as SUM_MJ	-- 에너지사용량(GJ)
		,FN_ENG_CONVERSION('CO2', '2', DATE_FORMAT(now(), '%Y%m%d'), SUM_ENG_VOL) as SUM_GCO2 -- 온실가스배출량(TCO2)
        ,SUM_TON_KM -- ton.km
		,FN_ENG_CONVERSION('TOE', '2', DATE_FORMAT(now(), '%Y%m%d'), SUM_ENG_VOL) as SUM_TOE -- 환산톤
		,case 	when SUM_TON_KM=0 then 0 
				else FN_ENG_CONVERSION('CO2', '2', DATE_FORMAT(now(), '%Y%m%d'), SUM_ENG_VOL) / SUM_TON_KM end as BASE_VAL -- 온실 가스원단위 값
	from (       
		select
			-- 1 -- @rownum := @rownum + 1 R -- SEQ
			V.ID_TRANS_COM -- 운수사ID
			,E.ID_ENG_POINT -- 배출시설ID
			,V.ID_ERP_DEPT -- EPR부서ID
			,V.ID_VEHICLE -- 차량ID
			-- ,substring(T.C_DATE,1,6) YM_DATE1 -- 날짜
			,concat(YM.YYYY,YM.MM) YM_DATE -- 날짜
			,V.S_SELFVRN_CD -- 자차여부
			,V.S_VRN  -- 차량VRN
			-- ,T.S_TRANS_TYPE_CD -- 운송수단
			,1 S_TRANS_TYPE_CD -- 운송수단1=화물차
			,V.S_LINE_CD -- 노선구분,정기/비정기
			,V.S_LOGI_WEI_CD -- 톤수
			-- ,T.S_START_PNT -- 출발지
			-- ,T.S_DEST_PNT -- 도착지
			,ifnull(sum(T.S_FARE),0) SUM_FARE -- 운임료
			,ifnull(sum(T.S_LOADAGE),0) SUM_LOADAGE -- 적재량
			,ifnull(sum(T.N_VACANT_DISTANCE),0) SUM_VACANT_DISTANCE -- 공차운행거리
			,ifnull(sum(T.S_DISTANCE),0) SUM_DISTANCE -- 영차운행거리
			,DATE_FORMAT(NOW(),'%Y%m%d%H%i%s') REG_MOD -- 등록/수정일시,YYYYMMDDHHmiSS
			,ifnull(V.S_FUEL_EFF,0) S_FUEL_EFF -- 연비
			-- ,sum(T.N_ENG_VOL) SUM_ENG_VOL -- 연료사용량
			,ifnull(V.S_FUEL_EFF,0) * (ifnull(sum(T.S_DISTANCE),0) + ifnull(sum(T.N_VACANT_DISTANCE),0)) SUM_ENG_VOL -- 연료사용량
			-- ,FN_ENG_CONVERSION('GJ',  '2', DATE_FORMAT(now(), '%Y%m%d'), V.S_FUEL_EFF * (ifnull(sum(T.S_DISTANCE),0) + ifnull(sum(T.N_VACANT_DISTANCE),0))) as SUM_MJ	-- 에너지사용량(GJ)
			-- ,FN_ENG_CONVERSION('CO2', '2', DATE_FORMAT(now(), '%Y%m%d'), V.S_FUEL_EFF * (ifnull(sum(T.S_DISTANCE),0) + ifnull(sum(T.N_VACANT_DISTANCE),0))) as SUM_GCO2 -- 온실가스배출량(TCO2)
			-- ,(ifnull(sum(T.S_LOADAGE),0) * ifnull(sum(T.S_DISTANCE),0))	SUM_TON_KM -- 톤킬로, 온실가스원단위
            ,ifnull(sum(T.S_LOADAGE),0) * (ifnull(sum(T.S_DISTANCE),0) + ifnull(sum(T.N_VACANT_DISTANCE),0)) SUM_TON_KM -- 톤키로
			-- ,FN_ENG_CONVERSION('TOE', '2', DATE_FORMAT(now(), '%Y%m%d'), V.S_FUEL_EFF * (ifnull(sum(T.S_DISTANCE),0) + ifnull(sum(T.N_VACANT_DISTANCE),0))) as SUM_TOE -- 환산톤
			-- ,FN_ENG_CONVERSION('CO2', '2', DATE_FORMAT(now(), '%Y%m%d'), V.S_FUEL_EFF * (ifnull(sum(T.S_DISTANCE),0) + ifnull(sum(T.N_VACANT_DISTANCE),0))) / (ifnull(sum(T.S_LOADAGE),0) * ifnull(sum(T.S_DISTANCE),0)) BASE_VAL -- 온실 가스원단위 값

			-- ,sum(T.N_MJ) SUM_MJ -- 에너지사용량(MJ)
			-- ,sum(T.N_GCO2) SUM_GCO2 -- 온실가스배출량(gCO2)
			-- ,sum(T.N_TON_KM) SUM_TON_KM -- 톤킬로, 온실가스원단위
			-- ,sum(T.N_TOE) SUM_TOE -- 환산톤
			-- ,0 -- 온실가스원단위(gCO2eq/ton-km)
			-- ,S.ID_SITE -- 사업자ID
			-- ,S.S_SITE_ID -- 사업장ID
			-- ,S.S_SITE_NM -- 사업장명
		from 
		  T_VEHICLE V -- 차량Master
		  LEFT OUTER JOIN V_SITE_DEPT S -- 사업장과부서
			ON S.ID_ERP_DEPT = V.ID_ERP_DEPT -- ERP부서ID
		  inner JOIN T_ENG_POINT E -- 배출원
			ON S.ID_SITE = E.ID_SITE -- 사업장ID
			   and E.S_INV_CD in (10,11) -- 배출시설구분.  T_CODE.INV_CD,1=창고 2=선박(비정기) 3=철도(정기) 4=화물차(정기) 5=화물차(비정기)   6=사무실
		  LEFT OUTER JOIN T_LOGIS_TRANS_INFO T  -- 운송정보
			ON V.ID_VEHICLE=T.ID_VEHICLE -- 차량ID
			  -- and T.S_TRANS_TYPE_CD = 1
			  and substring(T.C_DATE,1,6)=@YYYYMM -- 집계할 연월 지정
		  Cross join V_YEAR_MONTH YM
	where
		-- 1=1 
		-- and 
		V.C_DEL_YN = 'N' 
		and V.ID_ERP_DEPT<>0 and V.ID_ERP_DEPT is not null
		and E.ID_ENG_POINT is not null
		and YM.YYYY=@YYYY and YM.MM=@MM
        -- and T.S_TRANS_TYPE_CD = 1
		-- T.ID_ENG_POINT is not null and S.ID_ERP_DEPT is not null
    group by
      V.ID_VEHICLE -- 차량ID
      ,S.ID_SITE  -- 사업자ID
      ,V.S_LINE_CD -- 정기/비정기
      ,substring(T.C_DATE,1,6)  -- 월별로 grouping
    ) U1
    ) U
	ON DUPLICATE KEY UPDATE
		-- 이미 저장한 data는 update 하지 않는다.
		-- LOADAGE_SUM = U.SUM_LOADAGE   -- 적재량
		FARE_SUM = U.SUM_FARE -- 운임료
		-- ,VACANT_DISTANCE_SUM = U.SUM_VACANT_DISTANCE-- 공차운행거리
		-- ,DISTANCE_SUM = U.SUM_DISTANCE -- 영차운행거리
		-- ,REG_MOD = DATE_FORMAT(NOW(),'%Y%m%d%H%i%s')-- 등록/수정일시,YYYYMMDDHHmiSS
		-- ,FUEL_EFF = U.S_FUEL_EFF -- 연비
		-- ,ENG_VOL_SUM = U.SUM_ENG_VOL -- 연료사용량
		-- ,MJ_SUM = U.SUM_MJ -- 에너지사용량(MJ)
		-- ,GCO2_SUM = U.SUM_GCO2 -- 온실가스배출량(gCO2)
		-- ,TON_KM_SUM = U.SUM_TON_KM -- 톤킬로, 온실가스원단위
		-- ,TOE_SUM = U.SUM_TOE -- 환산톤
		-- ,BASE_VAL = U.BASE_VAL -- 온실 가스원단위 값
		--  ,BAS_UNT = 0-- 온실가스원단위(gCO2eq/ton-km)
  ;



  /*
    해송 data 생성
  */
  insert into 
    T_LOGIS_TRANS_MON_INFO
        (
          -- ID_LOGIS_TRANS_MON_INFO
         ID_TRANS_COM  -- 운수사ID
         ,ID_VEHICLE -- 차량ID
         ,ID_ENG_POINT  -- 배출시설ID
         ,ID_ERP_DEPT  -- EPR부서ID
         ,DATE_MON -- 날짜
         ,SELFVRN_CD -- 자차여부
         ,VRN -- 차량VRN
         ,TRANS_TYPE_CD  -- 운송수단1=화물차
         ,LOGI_YCT_CD -- 노선구분,정기/비정기
         ,LOGI_WEI_CD -- 톤수
         ,FARE_SUM -- 운임료
         ,LOADAGE_SUM  -- 적재량
         -- ,WEIGHT_SUM -- 총중량
         ,VACANT_DISTANCE_SUM -- 공차운행거리
         ,DISTANCE_SUM -- 영차운행거리
         ,REG_MOD -- 등록/수정일시,YYYYMMDDHHmiSS
         ,FUEL_EFF -- 연비
         ,ENG_VOL_SUM -- 연료사용량
         ,MJ_SUM -- 에너지사용량(MJ)
         ,GCO2_SUM -- 온실가스배출량(gCO2)
         ,TON_KM_SUM -- 톤킬로, 온실가스원단위
         ,TOE_SUM -- 환산톤
		 ,BASE_VAL -- 원단위 값
         -- ,BAS_UNT -- 온실가스원단위(gCO2eq/ton-km)
         -- ,S_REG_ID -- 등록자ID
         -- ,S_MOD_ID -- 수정자ID
         -- ,C_DEL_YN
         )
  select * from (    
    SELECT
      -- 1 SEQ -- @rownum := @rownum + 1 R -- SEQ
      -- ,
      null ID_TRANS_COM -- 운수사ID
      ,null ID_VEHICLE -- 차량ID
      ,E.ID_ENG_POINT -- 배출시설ID unique index
      ,S.ID_ERP_DEPT -- EPR부서ID
      ,@monthOfYesterday YM_DATE -- 날짜 unique index
      ,2 S_SELFVRN_CD -- 자차여부:용차
      ,null S_VRN  -- 차량VRN
      -- ,T.S_TRANS_TYPE_CD -- 운송수단
      ,2 S_TRANS_TYPE_CD  -- 운송수단 2=선박
      ,2 S_LINE_CD -- 노선구분,정기(1)/비정기(2)
      ,0 S_LOGI_WEI_CD -- 톤수
      -- ,T.S_START_PNT -- 출발지
      -- ,T.S_DEST_PNT -- 도착지
      ,0 SUM_LOADAGE -- 적재량
      ,0 SUM_FARE -- 운임료
      ,0 SUM_VACANT_DISTANCE -- 공차운행거리
      ,0 SUM_DISTANCE -- 영차운행거리
      ,DATE_FORMAT(NOW(),'%Y%m%d%H%i%s') REG_MOD -- 등록/수정일시,YYYYMMDDHHmiSS
      ,0 S_FUEL_EFF -- 연비
      ,0 SUM_ENG_VOL -- 연료사용량
      ,0 SUM_MJ -- 에너지사용량(MJ)
      ,0 SUM_GCO2 -- 온실가스배출량(gCO2)
      ,0 SUM_TON_KM -- 톤킬로, 온실가스원단위
      ,0 SUM_TOE -- 환산톤
      ,0 -- 온실가스원단위(gCO2eq/ton-km)
    FROM
      T_ENG_POINT E -- 배출시설
      LEFT OUTER JOIN V_SITE_DEPT S -- 사업장과부서
        ON E.ID_SITE = S.ID_SITE -- 사업장
    where
      E.S_INV_CD = 2 -- 배출시설 선박
  ) U  
  ON DUPLICATE KEY UPDATE
    ID_VEHICLE = null
  ;

  /*
    철송 data 생성
  */
  insert into 
  T_LOGIS_TRANS_MON_INFO
      (
        -- ID_LOGIS_TRANS_MON_INFO
       ID_TRANS_COM  -- 운수사ID
       ,ID_VEHICLE -- 차량ID
       ,ID_ENG_POINT  -- 배출시설ID
       ,ID_ERP_DEPT  -- EPR부서ID
       ,DATE_MON -- 날짜
       ,SELFVRN_CD -- 자차여부
       ,VRN -- 차량VRN
       ,TRANS_TYPE_CD  -- 운송수단1=화물차
       ,LOGI_YCT_CD -- 노선구분,정기/비정기
       ,LOGI_WEI_CD -- 톤수
       ,FARE_SUM -- 운임료
       ,LOADAGE_SUM  -- 적재량
       -- ,WEIGHT_SUM -- 총중량
       ,VACANT_DISTANCE_SUM -- 공차운행거리
       ,DISTANCE_SUM -- 영차운행거리
       ,REG_MOD -- 등록/수정일시,YYYYMMDDHHmiSS
       ,FUEL_EFF -- 연비
       ,ENG_VOL_SUM -- 연료사용량
       ,MJ_SUM -- 에너지사용량(MJ)
       ,GCO2_SUM -- 온실가스배출량(gCO2)
       ,TON_KM_SUM -- 톤킬로, 온실가스원단위
       ,TOE_SUM -- 환산톤
       ,BAS_UNT -- 온실가스원단위(gCO2eq/ton-km)
       -- ,S_REG_ID -- 등록자ID
       -- ,S_MOD_ID -- 수정자ID
       -- ,C_DEL_YN
       )
  select * from (  
    SELECT
      -- 1 SEQ -- @rownum := @rownum + 1 R -- SEQ
      -- ,
      null ID_TRANS_COM -- 운수사ID
      ,null ID_VEHICLE -- 차량ID
      ,T.ID_ENG_POINT -- 배출시설ID
      ,T.ID_ERP_DEPT -- EPR부서ID
      ,substring(T.C_DATE,1,6) YM_DATE -- 날짜
      ,2 S_SELFVRN_CD -- 자차여부:용차
      ,null S_VRN  -- 차량VRN
      -- ,T.S_TRANS_TYPE_CD -- 운송수단
      ,3 S_TRANS_TYPE_CD  -- 운송수단 3=철도
      ,1 S_LINE_CD -- 노선구분,정기(1)/비정기(2)
      ,0 S_LOGI_WEI_CD -- 톤수
      -- ,T.S_START_PNT -- 출발지
      -- ,T.S_DEST_PNT -- 도착지
      ,sum(T.S_LOADAGE) SUM_LOADAGE -- 적재량
      ,sum(T.S_FARE) SUM_FARE -- 운임료
      ,sum(T.N_VACANT_DISTANCE) SUM_VACANT_DISTANCE -- 공차운행거리
      ,sum(T.S_DISTANCE) SUM_DISTANCE -- 영차운행거리
      ,DATE_FORMAT(NOW(),'%Y%m%d%H%i%s') REG_MOD -- 등록/수정일시,YYYYMMDDHHmiSS
      ,0 S_FUEL_EFF -- 연비
      ,sum(T.N_ENG_VOL) SUM_ENG_VOL -- 연료사용량
      ,sum(T.N_MJ) SUM_MJ -- 에너지사용량(MJ)
      ,sum(T.N_GCO2) SUM_GCO2 -- 온실가스배출량(gCO2)
      ,sum(T.N_TON_KM) SUM_TON_KM -- 톤킬로, 온실가스원단위
      ,sum(T.N_TOE) SUM_TOE -- 환산톤
      ,0 -- 온실가스원단위(gCO2eq/ton-km)
    FROM
      T_LOGIS_TRANS_INFO T  -- 운송정보
      LEFT OUTER JOIN V_SITE_DEPT S -- 사업장과부서
        ON T.ID_ERP_DEPT = S.ID_ERP_DEPT -- ERP부서ID
      LEFT OUTER JOIN T_ENG_POINT E -- 배출시설
        ON E.ID_SITE = S.ID_SITE
          and E.S_INV_CD=3 -- 철도(3)
    WHERE
      T.S_TRANS_TYPE_CD = 3 -- 철도
      and substring(T.C_DATE,1,6)=@monthOfYesterday
    GROUP BY
      ID_ENG_POINT -- 배출시설ID
      ,substring(T.C_DATE,1,6)  -- 연월별로 grouping
  ) U      
  ON DUPLICATE KEY UPDATE
    LOADAGE_SUM = U.SUM_LOADAGE   -- 적재량
    ,FARE_SUM = U.SUM_FARE -- 운임료
    ,VACANT_DISTANCE_SUM = U.SUM_VACANT_DISTANCE-- 공차운행거리
    ,DISTANCE_SUM = U.SUM_DISTANCE -- 영차운행거리
    ,REG_MOD = DATE_FORMAT(NOW(),'%Y%m%d%H%i%s')-- 등록/수정일시,YYYYMMDDHHmiSS
    ,FUEL_EFF = U.S_FUEL_EFF -- 연비
    ,ENG_VOL_SUM = U.SUM_ENG_VOL-- 연료사용량
    ,MJ_SUM = U.SUM_MJ-- 에너지사용량(MJ)
    ,GCO2_SUM = U.SUM_GCO2 -- 온실가스배출량(gCO2)
    ,TON_KM_SUM = U.SUM_TON_KM-- 톤킬로, 온실가스원단위
    ,TOE_SUM = U.SUM_TOE-- 환산톤
    ,BAS_UNT = 0 -- 온실가스원단위(gCO2eq/ton-km)
  ;

-- 철송 data select
SELECT
      -- 1 SEQ -- @rownum := @rownum + 1 R -- SEQ
      -- ,
      null ID_TRANS_COM -- 운수사ID
      ,null ID_VEHICLE -- 차량ID
      ,T.ID_ENG_POINT -- 배출시설ID
      ,T.ID_ERP_DEPT -- EPR부서ID
      -- ,substring(T.C_DATE,1,6) YM_DATE -- 날짜
		,concat(YM.YYYY,YM.MM) YM_DATE -- 날짜
        , substring(T.C_DATE,1,6)  YM_DATE_GRP  -- 연월별로 grouping
		,2 S_SELFVRN_CD -- 자차여부:용차
		,null S_VRN  -- 차량VRN
		-- ,T.S_TRANS_TYPE_CD -- 운송수단
		,3 S_TRANS_TYPE_CD  -- 운송수단 3=철도
		,1 S_LINE_CD -- 노선구분,정기(1)/비정기(2)
		,0 S_LOGI_WEI_CD -- 톤수
      -- ,T.S_START_PNT -- 출발지
      -- ,T.S_DEST_PNT -- 도착지
      ,sum(T.S_LOADAGE) SUM_LOADAGE -- 적재량
      ,sum(T.S_FARE) SUM_FARE -- 운임료
      ,sum(T.N_VACANT_DISTANCE) SUM_VACANT_DISTANCE -- 공차운행거리
      ,sum(T.S_DISTANCE) SUM_DISTANCE -- 영차운행거리
      ,DATE_FORMAT(NOW(),'%Y%m%d%H%i%s') REG_MOD -- 등록/수정일시,YYYYMMDDHHmiSS
      ,0 S_FUEL_EFF -- 연비
      ,sum(T.N_ENG_VOL) SUM_ENG_VOL -- 연료사용량
      ,sum(T.N_MJ) SUM_MJ -- 에너지사용량(MJ)
      ,sum(T.N_GCO2) SUM_GCO2 -- 온실가스배출량(gCO2)
      ,sum(T.N_TON_KM) SUM_TON_KM -- 톤킬로, 온실가스원단위
      ,sum(T.N_TOE) SUM_TOE -- 환산톤
      ,0 -- 온실가스원단위(gCO2eq/ton-km)
    FROM
		V_SITE_DEPT S -- 사업장과부서
		LEFT OUTER JOIN T_ENG_POINT E -- 배출시설
			ON E.ID_SITE = S.ID_SITE
				and E.S_INV_CD=3 -- 철도(3)
		LEFT OUTER JOIN T_LOGIS_TRANS_INFO T  -- 운송정보
			ON T.ID_ERP_DEPT = S.ID_ERP_DEPT -- ERP부서ID
				and substring(T.C_DATE,1,6)=@YYYYMM -- 집계할 연월 지정
		Cross join V_YEAR_MONTH YM
   WHERE
      -- T.S_TRANS_TYPE_CD = 3 -- 철도
      -- and 
		YM.YYYY=@YYYY and YM.MM=@MM
    GROUP BY
      ID_ENG_POINT -- 배출시설ID
      ,substring(T.C_DATE,1,6)  -- 연월별로 grouping
;


select *     FROM
		V_SITE_DEPT S -- 사업장과부서
		LEFT OUTER JOIN T_ENG_POINT E -- 배출시설
			ON E.ID_SITE = S.ID_SITE
				and E.S_INV_CD=3 -- 철도(3)
;

call SP_MAKE_LOGIS_TRANS_MON_INFO('201904');
call SP_MAKE_LOGIS_TRANS_MON_INFO('201905');
call SP_MAKE_LOGIS_TRANS_MON_INFO('201906');
call SP_MAKE_LOGIS_TRANS_MON_INFO('201907');
call SP_MAKE_LOGIS_TRANS_MON_INFO('201908');
call SP_MAKE_LOGIS_TRANS_MON_INFO('201909');
call SP_MAKE_LOGIS_TRANS_MON_INFO('201910');
call SP_MAKE_LOGIS_TRANS_MON_INFO('201911');
call SP_MAKE_LOGIS_TRANS_MON_INFO(null);

-- ID_VEHICLE 로부터 배출시설ID 얻기
select V.ID_VEHICLE,V.S_VRN,V.ID_ERP_DEPT,S.S_SITE_NM
  ,S.ID_SITE,E.ID_ENG_POINT
  from 
  T_VEHICLE V
  LEFT OUTER JOIN V_SITE_DEPT S
    ON V.ID_ERP_DEPT = S.ID_ERP_DEPT
  LEFT OUTER JOIN T_ENG_POINT E
    ON S.ID_SITE = E.ID_SITE
  ;
    select
      -- 1 -- @rownum := @rownum + 1 R -- SEQ
      V.ID_TRANS_COM -- 운수사ID
      ,E.ID_ENG_POINT -- 배출시설ID
      ,V.ID_ERP_DEPT -- EPR부서ID
      ,V.ID_VEHICLE -- 차량ID
      ,substring(T.C_DATE,1,6) YM_DATE -- 날짜
      ,V.S_SELFVRN_CD -- 자차여부
      ,V.S_VRN  -- 차량VRN
      -- ,T.S_TRANS_TYPE_CD -- 운송수단
      , 1 S_TRANS_TYPE_CD -- 운송수단1=화물차
      ,V.S_LINE_CD -- 노선구분,정기/비정기
      ,V.S_LOGI_WEI_CD -- 톤수
      -- ,T.S_START_PNT -- 출발지
      -- ,T.S_DEST_PNT -- 도착지
      ,sum(T.S_FARE) SUM_FARE -- 운임료
      ,sum(T.S_LOADAGE) SUM_LOADAGE -- 적재량
      ,sum(T.N_VACANT_DISTANCE) SUM_VACANT_DISTANCE -- 공차운행거리
      ,sum(T.S_DISTANCE) SUM_DISTANCE -- 영차운행거리
      ,DATE_FORMAT(NOW(),'%Y%m%d%H%i%s') REG_MOD -- 등록/수정일시,YYYYMMDDHHmiSS
      ,1 S_FUEL_EFF -- V.S_FUEL_EFF -- 연비
      ,sum(T.N_ENG_VOL) SUM_ENG_VOL -- 연료사용량
      ,sum(T.N_MJ) SUM_MJ -- 에너지사용량(MJ)
      ,sum(T.N_GCO2) SUM_GCO2 -- 온실가스배출량(gCO2)
      ,sum(T.N_TON_KM) SUM_TON_KM -- 톤킬로, 온실가스원단위
      ,sum(T.N_TOE) SUM_TOE -- 환산톤
      ,0 -- 온실가스원단위(gCO2eq/ton-km)
      -- ,S.ID_SITE -- 사업자ID
      -- ,S.S_SITE_ID -- 사업장ID
      -- ,S.S_SITE_NM -- 사업장명
    from 
      T_VEHICLE V -- 차량Master
      LEFT OUTER JOIN V_SITE_DEPT S -- 사업장과부서
        ON S.ID_ERP_DEPT = V.ID_ERP_DEPT -- ERP부서ID
      inner JOIN T_ENG_POINT E -- 배출원
        ON S.ID_SITE = E.ID_SITE -- 사업장ID
           and E.S_INV_CD in (10,11) -- 배출시설구분.  T_CODE.INV_CD,1=창고 2=선박(비정기) 3=철도(정기) 4=화물차(정기) 5=화물차(비정기)   6=사무실
      LEFT OUTER JOIN T_LOGIS_TRANS_INFO T  -- 운송정보
        ON V.ID_VEHICLE=T.ID_VEHICLE -- 차량ID
          and substring(T.C_DATE,1,6)=@YYYYMM -- 집계할 연월 지정
    where 1=1 
      -- and T.ID_ENG_POINT is not null and S.ID_ERP_DEPT is not null
    group by
      V.ID_VEHICLE -- 차량ID
      ,S.ID_SITE -- 사업자ID
      ,V.S_LINE_CD -- 정기/비정기
      ,substring(T.C_DATE,1,6)  -- 월별로 grouping
;