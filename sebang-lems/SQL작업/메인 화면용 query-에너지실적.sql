/*
	메인 화면용 query 정리
    
    참고:
		LOGI_TYPE	1	물류
		LOGI_TYPE	2	물류외
    
*/
select    * from V_SUM_AMT;

select   S_LOGI_TYPE_CD,S.* from V_SUM_AMT S
where S_LOGI_TYPE_CD = '2'
;

-- 에너지 실적
select    C_YEAR,
        sum(M1_GJ +M2_GJ + M3_GJ +M4_GJ +M5_GJ +M6_GJ +M7_GJ +M8_GJ +M9_GJ +M10_GJ +M11_GJ +M12_GJ) as nGjAll, -- 전체
        sum( case when S_LOGI_TYPE_CD = '1' then M1_GJ +M2_GJ + M3_GJ +M4_GJ +M5_GJ +M6_GJ +M7_GJ +M8_GJ +M9_GJ +M10_GJ +M11_GJ +M12_GJ else 0 end ) as nGjLogis -- 물류
from V_SUM_AMT
where C_YEAR is not null
	and C_YEAR in ('2018','2019')
GROUP BY C_YEAR 
ORDER BY C_YEAR;

-- 온실가스 배출 실적
select    C_YEAR,
        sum(M1_CO2 +M2_CO2 + M3_CO2 +M4_CO2 +M5_CO2 +M6_CO2 +M7_CO2 +M8_CO2 +M9_CO2 +M10_CO2 +M11_CO2 +M12_CO2) as nTco2All,  -- 전체
        sum( case when S_LOGI_TYPE_CD = '1' then M1_CO2 +M2_CO2 + M3_CO2 +M4_CO2 +M5_CO2 +M6_CO2 +M7_CO2 +M8_CO2 +M9_CO2 +M10_CO2 +M11_CO2 +M12_CO2 else 0 end ) as nTco2Logis -- 물류
from V_SUM_AMT
where C_YEAR is not null
	and C_YEAR in ('2018','2019')
GROUP BY C_YEAR
ORDER BY C_YEAR
;

-- 온실가스 에너지 현황(실적)
select 
	gg.C_YEAR
    ,gg.N_ENG_GOAL MRV_ENG_GOAL -- 정부목표
	,gg.N_GAS_GOAL MRV_GAS_GOAL -- 정부목표
	,sg.N_YEAR_GAS_GOAL -- 사내목표
	,sg.N_YEAR_ENG_GOAL -- 사내목표
	-- sg.N_YEAR_GAS_GOAL,
	
    -- ,sg.N_ENG_GOAL_SUM_AMT -- 월별 에너지 목표합
	-- ,sg.N_GAS_GOAL_SUM_AMT  -- 월별 가스 목표합
    ,ega.N_GAS_SUM_AMT  -- 가스 실적
    ,ega.N_ENG_SUM_AMT -- 에너지 실적
    
	from T_MRV_POINT_GOAL gg 
    left outer join (
			select 	C_YEAR, 	
					SUM(N_YEAR_GAS_GOAL) N_YEAR_GAS_GOAL , 	-- 사내 가스배출량 목표
					SUM(N_YEAR_ENG_GOAL) N_YEAR_ENG_GOAL   -- 사내 에너지 목표
					,SUM(N_GAS_GOAL_SUM) N_GAS_GOAL_SUM_AMT, -- 월별 가스 목표합
					SUM(N_ENG_GOAL_SUM) N_ENG_GOAL_SUM_AMT  -- 월별 에너지 목표합
			from T_SITE_YEAR_GOAL -- 년간사업장 목표
			group by C_YEAR
		) sg 
		on gg.C_YEAR = sg.C_YEAR  
	left outer join (
			select C_YEAR,
            		SUM(N_CO2) N_GAS_SUM_AMT, -- 가스 실적
					SUM(N_GJ) N_ENG_SUM_AMT -- 에너지 실적
			-- from T_ENG_POINT_AMT EPA
            from T_ENG_GHG_AMT EGA
            group by C_YEAR
        ) ega
		on gg.C_YEAR = ega.C_YEAR
	ORDER BY C_YEAR
;

select * 
from T_SITE_YEAR_GOAL
;

select * 
from T_ENG_GHG_AMT
;