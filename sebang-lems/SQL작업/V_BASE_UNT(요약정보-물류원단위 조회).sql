/*
  VIEW: V_BASE_UNT
  
  화면: 요약정보/ 물류원단위 조회/

  2019/10/28
  
	T_CODE
	T_ENG_POINT     -- 배출시설
	T_MON_ENG_CLOSE -- 월마감
	T_ENG_POINT_AMT -- 에너지사용량 실적관리

select * from T_ENG_POINT       ; -- 배출시설
select * from T_ENG_POINT_AMT   ; -- 에너지사용량 실적관리
select * from T_MON_ENG_CLOSE   ; -- 월마
select * from T_CODE            ;


원단위
  이동체
    적재량 - ton
    거리 - km
    ton.km - ton.km
  고정체
    면적
    가동시간
 */
 
select * from T_ENG_POINT       ; -- 배출시설
select * from T_ENG_POINT_AMT   ; -- 에너지사용량 실적관리
select * from T_MON_ENG_CLOSE   ; -- 월마감
select * from T_CODE where S_CAT='UNT';
select * from T_CODE where S_CAT='LOGI_TYPE'; -- 물류 구분



select 
  EP.ID_ENG_POINT
  ,EP.ID_SITE
  ,EP.ID_ENG_CEF -- 배출원
  ,EPA.N_TCO2 -- 온실가스(tCO2)
  ,EP.S_BASE_VAL -- 원단위 기준 값
  ,EP.S_BASE_UNT -- 원단위 기준 단위
  ,EPA.C_YEAR -- 연
  ,EPA.C_MON -- 월
  ,EP.S_INV_CD -- 배출시설구분.  T_CODE 배출시설구분 INV_CD 코드 참조
from 
  T_ENG_POINT EP -- 배출시설
  LEFT JOIN T_ENG_POINT_AMT EPA -- 에너지사용량 실적관리
    ON EP.ID_ENG_POINT = EPA.ID_ENG_POINT 
      and EP.S_LOGI_TYPE_CD=1 -- 물류 만 해당
  LEFT JOIN T_ENG_CEF EC  -- 배출원관리 
    ON EP.ID_ENG_CEF=EC.ID_ENG_CEF
  -- LEFT  JOIN T_ENG_GHG_AMT EGA -- 배출원온실가스 계산(집계)    T_ENG_GHG_AMT 를 join 해야할것 같은데?????
  --   ON EP.ID_ENG_POINT=EGA.ID_ENG_POINT -- 배출시설별
where 
  EP.S_BASE_VAL=''
  and EPA.C_YEAR='2019';

select * from V_BASE_UNT;

-- create 
alter view V_BASE_UNT as 
SELECT 
  1 ID_SITE -- 사업장 ID
  ,'사업장' S_SITE_NM -- 사업장명
  ,1 YCT_CD -- 노선구분
  ,'YCT_NM' YCT_NM -- 노선구분 이름
  ,'TCO2' SCU_CD -- 분석조회단위코드
  ,'TCO2' SCU_NM -- 분석조회단위명
  ,'BASE' BASE_UNT_CD -- 원단위 기준 코드
  ,'BASE' BASE_UNT_NM -- 원단위 기준명
  ,C_YEAR -- 연도
  ,1 M1_GJ
  ,1 M1_GJ_BASE
  ,1 M1_BASE_UNIT 
  ,1 M2_GJ
  ,1 M2_GJ_BASE
  ,1 M2_BASE_UNIT 
  ,1 M3_GJ
  ,1 M3_GJ_BASE
  ,1 M3_BASE_UNIT 
  ,1 M4_GJ
  ,1 M4_GJ_BASE
  ,1 M4_BASE_UNIT 
  ,1 M5_GJ
  ,1 M5_GJ_BASE
  ,1 M5_BASE_UNIT 
  ,1 M6_GJ
  ,1 M6_GJ_BASE
  ,1 M6_BASE_UNIT 
  ,1 M7_GJ
  ,1 M7_GJ_BASE
  ,1 M7_BASE_UNIT 
  ,1 M8_GJ
  ,1 M8_GJ_BASE
  ,1 M8_BASE_UNIT 
  ,1 M9_GJ
  ,1 M9_GJ_BASE
  ,1 M9_BASE_UNIT 
  ,1 M10_GJ
  ,1 M10_GJ_BASE
  ,1 M10_BASE_UNIT 
  ,1 M11_GJ
  ,1 M11_GJ_BASE
  ,1 M11_BASE_UNIT 
  ,1 M12_GJ
  ,1 M12_GJ_BASE
  ,1 M12_BASE_UNIT 

FROM T_ENG_GHG_AMT;






