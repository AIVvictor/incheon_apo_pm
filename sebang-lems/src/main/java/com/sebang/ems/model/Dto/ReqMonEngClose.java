package com.sebang.ems.model.Dto;

import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.sebang.ems.model.Do.TMonEngClose;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@ToString
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@JsonInclude(value = Include.NON_EMPTY)
public class ReqMonEngClose
{

	private Long idMonEngClose;
    
    // @NotNull(message = "idSite 는 필수 입력 값입니다. ")
    private Long idSite;
    
    @NotNull(message = "cYear 는 필수 입력 값입니다. ")
    private String cYear;
    
    @NotNull(message = "cMon 는 필수 입력 값입니다. ")
    private String cMon;
    
    private String cCloseYn;

    private Integer result;

    public ReqMonEngClose ( TMonEngClose c)
    {
        this.idMonEngClose =c.getIdMonEngClose();
        this.idSite = c.getIdSite();
        this.cYear = c.getcYear();
        this.cMon = c.getcMon();
        this.cCloseYn =c.getcCloseYn();
        this.result =0;
    }

    public Long getIdMonEngClose() {
        return idMonEngClose;
    }

    public Long getIdSite() {
        return idSite;
    }

    public String getcYear() {
        return cYear;
    }

    public String getcMon() {
        return cMon;
    }

    public void setcMon(String cMon) {
        this.cMon = cMon;
    }

    public String getcCloseYn() {
        return cCloseYn;
    }

    public Integer getResult() {
        return result;
    }

    public TMonEngClose toEntity()
    {
        TMonEngClose c = new TMonEngClose();
        c.setIdMonEngClose(this.idMonEngClose);
        c.setIdSite(this.idSite);
        c.setcYear(this.cYear);
        c.setcMon(this.cMon);
        c.setcCloseYn(this.cCloseYn);
        return c;
    }



}