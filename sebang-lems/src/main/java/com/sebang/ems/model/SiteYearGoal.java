package com.sebang.ems.model;

import java.util.List;
import java.util.Map;

import com.sebang.ems.model.Do.TSiteYearGoal;

import lombok.ToString;

@ToString
public class SiteYearGoal extends TSiteYearGoal
{

    private List<SiteMonGoal> monGoals;

    private Map<Long,List<SiteMonGoal>> pointMonGoals;

    public Map<Long, List<SiteMonGoal>> getPointMonGoals() {
        return pointMonGoals;
    }

    public void setPointMonGoals(Map<Long, List<SiteMonGoal>> pointMonGoals) {
        this.pointMonGoals = pointMonGoals;
    }

}