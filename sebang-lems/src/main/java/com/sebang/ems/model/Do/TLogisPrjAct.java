package com.sebang.ems.model.Do;

public class TLogisPrjAct {
    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column T_LOGIS_PRJ_ACT.ID_LOGIS_PRJ_ACT
     *
     * @mbg.generated Wed Oct 02 20:32:48 KST 2019
     */
    private Long idLogisPrjAct;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column T_LOGIS_PRJ_ACT.ID_LOGIS_PRJ
     *
     * @mbg.generated Wed Oct 02 20:32:48 KST 2019
     */
    private Long idLogisPrj;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column T_LOGIS_PRJ_ACT.C_YEAR
     *
     * @mbg.generated Wed Oct 02 20:32:48 KST 2019
     */
    private String cYear;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column T_LOGIS_PRJ_ACT.S_LOGI_WEI_CD
     *
     * @mbg.generated Wed Oct 02 20:32:48 KST 2019
     */
    private String sLogiWeiCd;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column T_LOGIS_PRJ_ACT.N_PLAN_CNT
     *
     * @mbg.generated Wed Oct 02 20:32:48 KST 2019
     */
    private Integer nPlanCnt;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column T_LOGIS_PRJ_ACT.N_PLAN_COST
     *
     * @mbg.generated Wed Oct 02 20:32:48 KST 2019
     */
    private Float nPlanCost;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column T_LOGIS_PRJ_ACT.N_PLAN_LITER
     *
     * @mbg.generated Wed Oct 02 20:32:48 KST 2019
     */
    private Float nPlanLiter;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column T_LOGIS_PRJ_ACT.N_PLAN_TCO2
     *
     * @mbg.generated Wed Oct 02 20:32:48 KST 2019
     */
    private Float nPlanTco2;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column T_LOGIS_PRJ_ACT.N_RSLT_CNT
     *
     * @mbg.generated Wed Oct 02 20:32:48 KST 2019
     */
    private Integer nRsltCnt;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column T_LOGIS_PRJ_ACT.N_RSLT_COST
     *
     * @mbg.generated Wed Oct 02 20:32:48 KST 2019
     */
    private Float nRsltCost;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column T_LOGIS_PRJ_ACT.N_RSLT_LITER
     *
     * @mbg.generated Wed Oct 02 20:32:48 KST 2019
     */
    private Float nRsltLiter;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column T_LOGIS_PRJ_ACT.N_RSLT_TCO2
     *
     * @mbg.generated Wed Oct 02 20:32:48 KST 2019
     */
    private Float nRsltTco2;

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column T_LOGIS_PRJ_ACT.ID_LOGIS_PRJ_ACT
     *
     * @return the value of T_LOGIS_PRJ_ACT.ID_LOGIS_PRJ_ACT
     *
     * @mbg.generated Wed Oct 02 20:32:48 KST 2019
     */
    public Long getIdLogisPrjAct() {
        return idLogisPrjAct;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column T_LOGIS_PRJ_ACT.ID_LOGIS_PRJ_ACT
     *
     * @param idLogisPrjAct the value for T_LOGIS_PRJ_ACT.ID_LOGIS_PRJ_ACT
     *
     * @mbg.generated Wed Oct 02 20:32:48 KST 2019
     */
    public void setIdLogisPrjAct(Long idLogisPrjAct) {
        this.idLogisPrjAct = idLogisPrjAct;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column T_LOGIS_PRJ_ACT.ID_LOGIS_PRJ
     *
     * @return the value of T_LOGIS_PRJ_ACT.ID_LOGIS_PRJ
     *
     * @mbg.generated Wed Oct 02 20:32:48 KST 2019
     */
    public Long getIdLogisPrj() {
        return idLogisPrj;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column T_LOGIS_PRJ_ACT.ID_LOGIS_PRJ
     *
     * @param idLogisPrj the value for T_LOGIS_PRJ_ACT.ID_LOGIS_PRJ
     *
     * @mbg.generated Wed Oct 02 20:32:48 KST 2019
     */
    public void setIdLogisPrj(Long idLogisPrj) {
        this.idLogisPrj = idLogisPrj;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column T_LOGIS_PRJ_ACT.C_YEAR
     *
     * @return the value of T_LOGIS_PRJ_ACT.C_YEAR
     *
     * @mbg.generated Wed Oct 02 20:32:48 KST 2019
     */
    public String getcYear() {
        return cYear;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column T_LOGIS_PRJ_ACT.C_YEAR
     *
     * @param cYear the value for T_LOGIS_PRJ_ACT.C_YEAR
     *
     * @mbg.generated Wed Oct 02 20:32:48 KST 2019
     */
    public void setcYear(String cYear) {
        this.cYear = cYear;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column T_LOGIS_PRJ_ACT.S_LOGI_WEI_CD
     *
     * @return the value of T_LOGIS_PRJ_ACT.S_LOGI_WEI_CD
     *
     * @mbg.generated Wed Oct 02 20:32:48 KST 2019
     */
    public String getsLogiWeiCd() {
        return sLogiWeiCd;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column T_LOGIS_PRJ_ACT.S_LOGI_WEI_CD
     *
     * @param sLogiWeiCd the value for T_LOGIS_PRJ_ACT.S_LOGI_WEI_CD
     *
     * @mbg.generated Wed Oct 02 20:32:48 KST 2019
     */
    public void setsLogiWeiCd(String sLogiWeiCd) {
        this.sLogiWeiCd = sLogiWeiCd;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column T_LOGIS_PRJ_ACT.N_PLAN_CNT
     *
     * @return the value of T_LOGIS_PRJ_ACT.N_PLAN_CNT
     *
     * @mbg.generated Wed Oct 02 20:32:48 KST 2019
     */
    public Integer getnPlanCnt() {
        return nPlanCnt;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column T_LOGIS_PRJ_ACT.N_PLAN_CNT
     *
     * @param nPlanCnt the value for T_LOGIS_PRJ_ACT.N_PLAN_CNT
     *
     * @mbg.generated Wed Oct 02 20:32:48 KST 2019
     */
    public void setnPlanCnt(Integer nPlanCnt) {
        this.nPlanCnt = nPlanCnt;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column T_LOGIS_PRJ_ACT.N_PLAN_COST
     *
     * @return the value of T_LOGIS_PRJ_ACT.N_PLAN_COST
     *
     * @mbg.generated Wed Oct 02 20:32:48 KST 2019
     */
    public Float getnPlanCost() {
        return nPlanCost;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column T_LOGIS_PRJ_ACT.N_PLAN_COST
     *
     * @param nPlanCost the value for T_LOGIS_PRJ_ACT.N_PLAN_COST
     *
     * @mbg.generated Wed Oct 02 20:32:48 KST 2019
     */
    public void setnPlanCost(Float nPlanCost) {
        this.nPlanCost = nPlanCost;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column T_LOGIS_PRJ_ACT.N_PLAN_LITER
     *
     * @return the value of T_LOGIS_PRJ_ACT.N_PLAN_LITER
     *
     * @mbg.generated Wed Oct 02 20:32:48 KST 2019
     */
    public Float getnPlanLiter() {
        return nPlanLiter;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column T_LOGIS_PRJ_ACT.N_PLAN_LITER
     *
     * @param nPlanLiter the value for T_LOGIS_PRJ_ACT.N_PLAN_LITER
     *
     * @mbg.generated Wed Oct 02 20:32:48 KST 2019
     */
    public void setnPlanLiter(Float nPlanLiter) {
        this.nPlanLiter = nPlanLiter;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column T_LOGIS_PRJ_ACT.N_PLAN_TCO2
     *
     * @return the value of T_LOGIS_PRJ_ACT.N_PLAN_TCO2
     *
     * @mbg.generated Wed Oct 02 20:32:48 KST 2019
     */
    public Float getnPlanTco2() {
        return nPlanTco2;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column T_LOGIS_PRJ_ACT.N_PLAN_TCO2
     *
     * @param nPlanTco2 the value for T_LOGIS_PRJ_ACT.N_PLAN_TCO2
     *
     * @mbg.generated Wed Oct 02 20:32:48 KST 2019
     */
    public void setnPlanTco2(Float nPlanTco2) {
        this.nPlanTco2 = nPlanTco2;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column T_LOGIS_PRJ_ACT.N_RSLT_CNT
     *
     * @return the value of T_LOGIS_PRJ_ACT.N_RSLT_CNT
     *
     * @mbg.generated Wed Oct 02 20:32:48 KST 2019
     */
    public Integer getnRsltCnt() {
        return nRsltCnt;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column T_LOGIS_PRJ_ACT.N_RSLT_CNT
     *
     * @param nRsltCnt the value for T_LOGIS_PRJ_ACT.N_RSLT_CNT
     *
     * @mbg.generated Wed Oct 02 20:32:48 KST 2019
     */
    public void setnRsltCnt(Integer nRsltCnt) {
        this.nRsltCnt = nRsltCnt;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column T_LOGIS_PRJ_ACT.N_RSLT_COST
     *
     * @return the value of T_LOGIS_PRJ_ACT.N_RSLT_COST
     *
     * @mbg.generated Wed Oct 02 20:32:48 KST 2019
     */
    public Float getnRsltCost() {
        return nRsltCost;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column T_LOGIS_PRJ_ACT.N_RSLT_COST
     *
     * @param nRsltCost the value for T_LOGIS_PRJ_ACT.N_RSLT_COST
     *
     * @mbg.generated Wed Oct 02 20:32:48 KST 2019
     */
    public void setnRsltCost(Float nRsltCost) {
        this.nRsltCost = nRsltCost;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column T_LOGIS_PRJ_ACT.N_RSLT_LITER
     *
     * @return the value of T_LOGIS_PRJ_ACT.N_RSLT_LITER
     *
     * @mbg.generated Wed Oct 02 20:32:48 KST 2019
     */
    public Float getnRsltLiter() {
        return nRsltLiter;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column T_LOGIS_PRJ_ACT.N_RSLT_LITER
     *
     * @param nRsltLiter the value for T_LOGIS_PRJ_ACT.N_RSLT_LITER
     *
     * @mbg.generated Wed Oct 02 20:32:48 KST 2019
     */
    public void setnRsltLiter(Float nRsltLiter) {
        this.nRsltLiter = nRsltLiter;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column T_LOGIS_PRJ_ACT.N_RSLT_TCO2
     *
     * @return the value of T_LOGIS_PRJ_ACT.N_RSLT_TCO2
     *
     * @mbg.generated Wed Oct 02 20:32:48 KST 2019
     */
    public Float getnRsltTco2() {
        return nRsltTco2;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column T_LOGIS_PRJ_ACT.N_RSLT_TCO2
     *
     * @param nRsltTco2 the value for T_LOGIS_PRJ_ACT.N_RSLT_TCO2
     *
     * @mbg.generated Wed Oct 02 20:32:48 KST 2019
     */
    public void setnRsltTco2(Float nRsltTco2) {
        this.nRsltTco2 = nRsltTco2;
    }
}