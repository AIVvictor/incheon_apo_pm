package com.sebang.ems.model.Do;

import java.math.BigDecimal;

public class TSiteMonSumGoal {
    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column T_SITE_MON_SUM_GOAL.ID_SITE_MON_SUM_GOAL
     *
     * @mbg.generated Wed Dec 04 16:51:28 KST 2019
     */
    private Long idSiteMonSumGoal;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column T_SITE_MON_SUM_GOAL.ID_SITE_YEAR_GOAL
     *
     * @mbg.generated Wed Dec 04 16:51:28 KST 2019
     */
    private Long idSiteYearGoal;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column T_SITE_MON_SUM_GOAL.ID_ENG_POINT
     *
     * @mbg.generated Wed Dec 04 16:51:28 KST 2019
     */
    private Long idEngPoint;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column T_SITE_MON_SUM_GOAL.N_GOAL_SUM
     *
     * @mbg.generated Wed Dec 04 16:51:28 KST 2019
     */
    private BigDecimal nGoalSum;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column T_SITE_MON_SUM_GOAL.N_GAS_GOAL
     *
     * @mbg.generated Wed Dec 04 16:51:28 KST 2019
     */
    private BigDecimal nGasGoal;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column T_SITE_MON_SUM_GOAL.N_ENG_GOAL
     *
     * @mbg.generated Wed Dec 04 16:51:28 KST 2019
     */
    private BigDecimal nEngGoal;

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column T_SITE_MON_SUM_GOAL.ID_SITE_MON_SUM_GOAL
     *
     * @return the value of T_SITE_MON_SUM_GOAL.ID_SITE_MON_SUM_GOAL
     *
     * @mbg.generated Wed Dec 04 16:51:28 KST 2019
     */
    public Long getIdSiteMonSumGoal() {
        return idSiteMonSumGoal;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column T_SITE_MON_SUM_GOAL.ID_SITE_MON_SUM_GOAL
     *
     * @param idSiteMonSumGoal the value for T_SITE_MON_SUM_GOAL.ID_SITE_MON_SUM_GOAL
     *
     * @mbg.generated Wed Dec 04 16:51:28 KST 2019
     */
    public void setIdSiteMonSumGoal(Long idSiteMonSumGoal) {
        this.idSiteMonSumGoal = idSiteMonSumGoal;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column T_SITE_MON_SUM_GOAL.ID_SITE_YEAR_GOAL
     *
     * @return the value of T_SITE_MON_SUM_GOAL.ID_SITE_YEAR_GOAL
     *
     * @mbg.generated Wed Dec 04 16:51:28 KST 2019
     */
    public Long getIdSiteYearGoal() {
        return idSiteYearGoal;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column T_SITE_MON_SUM_GOAL.ID_SITE_YEAR_GOAL
     *
     * @param idSiteYearGoal the value for T_SITE_MON_SUM_GOAL.ID_SITE_YEAR_GOAL
     *
     * @mbg.generated Wed Dec 04 16:51:28 KST 2019
     */
    public void setIdSiteYearGoal(Long idSiteYearGoal) {
        this.idSiteYearGoal = idSiteYearGoal;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column T_SITE_MON_SUM_GOAL.ID_ENG_POINT
     *
     * @return the value of T_SITE_MON_SUM_GOAL.ID_ENG_POINT
     *
     * @mbg.generated Wed Dec 04 16:51:28 KST 2019
     */
    public Long getIdEngPoint() {
        return idEngPoint;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column T_SITE_MON_SUM_GOAL.ID_ENG_POINT
     *
     * @param idEngPoint the value for T_SITE_MON_SUM_GOAL.ID_ENG_POINT
     *
     * @mbg.generated Wed Dec 04 16:51:28 KST 2019
     */
    public void setIdEngPoint(Long idEngPoint) {
        this.idEngPoint = idEngPoint;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column T_SITE_MON_SUM_GOAL.N_GOAL_SUM
     *
     * @return the value of T_SITE_MON_SUM_GOAL.N_GOAL_SUM
     *
     * @mbg.generated Wed Dec 04 16:51:28 KST 2019
     */
    public BigDecimal getnGoalSum() {
        return nGoalSum;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column T_SITE_MON_SUM_GOAL.N_GOAL_SUM
     *
     * @param nGoalSum the value for T_SITE_MON_SUM_GOAL.N_GOAL_SUM
     *
     * @mbg.generated Wed Dec 04 16:51:28 KST 2019
     */
    public void setnGoalSum(BigDecimal nGoalSum) {
        this.nGoalSum = nGoalSum;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column T_SITE_MON_SUM_GOAL.N_GAS_GOAL
     *
     * @return the value of T_SITE_MON_SUM_GOAL.N_GAS_GOAL
     *
     * @mbg.generated Wed Dec 04 16:51:28 KST 2019
     */
    public BigDecimal getnGasGoal() {
        return nGasGoal;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column T_SITE_MON_SUM_GOAL.N_GAS_GOAL
     *
     * @param nGasGoal the value for T_SITE_MON_SUM_GOAL.N_GAS_GOAL
     *
     * @mbg.generated Wed Dec 04 16:51:28 KST 2019
     */
    public void setnGasGoal(BigDecimal nGasGoal) {
        this.nGasGoal = nGasGoal;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column T_SITE_MON_SUM_GOAL.N_ENG_GOAL
     *
     * @return the value of T_SITE_MON_SUM_GOAL.N_ENG_GOAL
     *
     * @mbg.generated Wed Dec 04 16:51:28 KST 2019
     */
    public BigDecimal getnEngGoal() {
        return nEngGoal;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column T_SITE_MON_SUM_GOAL.N_ENG_GOAL
     *
     * @param nEngGoal the value for T_SITE_MON_SUM_GOAL.N_ENG_GOAL
     *
     * @mbg.generated Wed Dec 04 16:51:28 KST 2019
     */
    public void setnEngGoal(BigDecimal nEngGoal) {
        this.nEngGoal = nEngGoal;
    }
}