package com.sebang.ems.model.Do;

import java.util.Date;

public class TShip {
    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column T_SHIP.ID_SHIP
     *
     * @mbg.generated Mon Oct 14 11:40:24 KST 2019
     */
    private Long idShip;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column T_SHIP.S_SHIP_NM
     *
     * @mbg.generated Mon Oct 14 11:40:24 KST 2019
     */
    private String sShipNm;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column T_SHIP.S_LINE_CD
     *
     * @mbg.generated Mon Oct 14 11:40:24 KST 2019
     */
    private String sLineCd;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column T_SHIP.S_LOADAGE
     *
     * @mbg.generated Mon Oct 14 11:40:24 KST 2019
     */
    private Float sLoadage;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column T_SHIP.S_DISTANCE
     *
     * @mbg.generated Mon Oct 14 11:40:24 KST 2019
     */
    private Float sDistance;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column T_SHIP.S_FUEL_EFF
     *
     * @mbg.generated Mon Oct 14 11:40:24 KST 2019
     */
    private Float sFuelEff;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column T_SHIP.S_SHIP_STATUS_CD
     *
     * @mbg.generated Mon Oct 14 11:40:24 KST 2019
     */
    private String sShipStatusCd;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column T_SHIP.S_REG_ID
     *
     * @mbg.generated Mon Oct 14 11:40:24 KST 2019
     */
    private String sRegId;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column T_SHIP.D_REG_DATE
     *
     * @mbg.generated Mon Oct 14 11:40:24 KST 2019
     */
    private Date dRegDate;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column T_SHIP.S_MOD_ID
     *
     * @mbg.generated Mon Oct 14 11:40:24 KST 2019
     */
    private String sModId;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column T_SHIP.D_MOD_DATE
     *
     * @mbg.generated Mon Oct 14 11:40:24 KST 2019
     */
    private Date dModDate;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column T_SHIP.C_DEL_YN
     *
     * @mbg.generated Mon Oct 14 11:40:24 KST 2019
     */
    private String cDelYn;

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column T_SHIP.ID_SHIP
     *
     * @return the value of T_SHIP.ID_SHIP
     *
     * @mbg.generated Mon Oct 14 11:40:24 KST 2019
     */
    public Long getIdShip() {
        return idShip;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column T_SHIP.ID_SHIP
     *
     * @param idShip the value for T_SHIP.ID_SHIP
     *
     * @mbg.generated Mon Oct 14 11:40:24 KST 2019
     */
    public void setIdShip(Long idShip) {
        this.idShip = idShip;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column T_SHIP.S_SHIP_NM
     *
     * @return the value of T_SHIP.S_SHIP_NM
     *
     * @mbg.generated Mon Oct 14 11:40:24 KST 2019
     */
    public String getsShipNm() {
        return sShipNm;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column T_SHIP.S_SHIP_NM
     *
     * @param sShipNm the value for T_SHIP.S_SHIP_NM
     *
     * @mbg.generated Mon Oct 14 11:40:24 KST 2019
     */
    public void setsShipNm(String sShipNm) {
        this.sShipNm = sShipNm;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column T_SHIP.S_LINE_CD
     *
     * @return the value of T_SHIP.S_LINE_CD
     *
     * @mbg.generated Mon Oct 14 11:40:24 KST 2019
     */
    public String getsLineCd() {
        return sLineCd;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column T_SHIP.S_LINE_CD
     *
     * @param sLineCd the value for T_SHIP.S_LINE_CD
     *
     * @mbg.generated Mon Oct 14 11:40:24 KST 2019
     */
    public void setsLineCd(String sLineCd) {
        this.sLineCd = sLineCd;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column T_SHIP.S_LOADAGE
     *
     * @return the value of T_SHIP.S_LOADAGE
     *
     * @mbg.generated Mon Oct 14 11:40:24 KST 2019
     */
    public Float getsLoadage() {
        return sLoadage;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column T_SHIP.S_LOADAGE
     *
     * @param sLoadage the value for T_SHIP.S_LOADAGE
     *
     * @mbg.generated Mon Oct 14 11:40:24 KST 2019
     */
    public void setsLoadage(Float sLoadage) {
        this.sLoadage = sLoadage;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column T_SHIP.S_DISTANCE
     *
     * @return the value of T_SHIP.S_DISTANCE
     *
     * @mbg.generated Mon Oct 14 11:40:24 KST 2019
     */
    public Float getsDistance() {
        return sDistance;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column T_SHIP.S_DISTANCE
     *
     * @param sDistance the value for T_SHIP.S_DISTANCE
     *
     * @mbg.generated Mon Oct 14 11:40:24 KST 2019
     */
    public void setsDistance(Float sDistance) {
        this.sDistance = sDistance;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column T_SHIP.S_FUEL_EFF
     *
     * @return the value of T_SHIP.S_FUEL_EFF
     *
     * @mbg.generated Mon Oct 14 11:40:24 KST 2019
     */
    public Float getsFuelEff() {
        return sFuelEff;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column T_SHIP.S_FUEL_EFF
     *
     * @param sFuelEff the value for T_SHIP.S_FUEL_EFF
     *
     * @mbg.generated Mon Oct 14 11:40:24 KST 2019
     */
    public void setsFuelEff(Float sFuelEff) {
        this.sFuelEff = sFuelEff;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column T_SHIP.S_SHIP_STATUS_CD
     *
     * @return the value of T_SHIP.S_SHIP_STATUS_CD
     *
     * @mbg.generated Mon Oct 14 11:40:24 KST 2019
     */
    public String getsShipStatusCd() {
        return sShipStatusCd;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column T_SHIP.S_SHIP_STATUS_CD
     *
     * @param sShipStatusCd the value for T_SHIP.S_SHIP_STATUS_CD
     *
     * @mbg.generated Mon Oct 14 11:40:24 KST 2019
     */
    public void setsShipStatusCd(String sShipStatusCd) {
        this.sShipStatusCd = sShipStatusCd;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column T_SHIP.S_REG_ID
     *
     * @return the value of T_SHIP.S_REG_ID
     *
     * @mbg.generated Mon Oct 14 11:40:24 KST 2019
     */
    public String getsRegId() {
        return sRegId;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column T_SHIP.S_REG_ID
     *
     * @param sRegId the value for T_SHIP.S_REG_ID
     *
     * @mbg.generated Mon Oct 14 11:40:24 KST 2019
     */
    public void setsRegId(String sRegId) {
        this.sRegId = sRegId;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column T_SHIP.D_REG_DATE
     *
     * @return the value of T_SHIP.D_REG_DATE
     *
     * @mbg.generated Mon Oct 14 11:40:24 KST 2019
     */
    public Date getdRegDate() {
        return dRegDate;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column T_SHIP.D_REG_DATE
     *
     * @param dRegDate the value for T_SHIP.D_REG_DATE
     *
     * @mbg.generated Mon Oct 14 11:40:24 KST 2019
     */
    public void setdRegDate(Date dRegDate) {
        this.dRegDate = dRegDate;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column T_SHIP.S_MOD_ID
     *
     * @return the value of T_SHIP.S_MOD_ID
     *
     * @mbg.generated Mon Oct 14 11:40:24 KST 2019
     */
    public String getsModId() {
        return sModId;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column T_SHIP.S_MOD_ID
     *
     * @param sModId the value for T_SHIP.S_MOD_ID
     *
     * @mbg.generated Mon Oct 14 11:40:24 KST 2019
     */
    public void setsModId(String sModId) {
        this.sModId = sModId;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column T_SHIP.D_MOD_DATE
     *
     * @return the value of T_SHIP.D_MOD_DATE
     *
     * @mbg.generated Mon Oct 14 11:40:24 KST 2019
     */
    public Date getdModDate() {
        return dModDate;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column T_SHIP.D_MOD_DATE
     *
     * @param dModDate the value for T_SHIP.D_MOD_DATE
     *
     * @mbg.generated Mon Oct 14 11:40:24 KST 2019
     */
    public void setdModDate(Date dModDate) {
        this.dModDate = dModDate;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column T_SHIP.C_DEL_YN
     *
     * @return the value of T_SHIP.C_DEL_YN
     *
     * @mbg.generated Mon Oct 14 11:40:24 KST 2019
     */
    public String getcDelYn() {
        return cDelYn;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column T_SHIP.C_DEL_YN
     *
     * @param cDelYn the value for T_SHIP.C_DEL_YN
     *
     * @mbg.generated Mon Oct 14 11:40:24 KST 2019
     */
    public void setcDelYn(String cDelYn) {
        this.cDelYn = cDelYn;
    }
}