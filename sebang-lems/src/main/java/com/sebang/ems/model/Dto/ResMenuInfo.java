package com.sebang.ems.model.Dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.sebang.ems.model.Menu;
import com.sebang.ems.model.Do.TMenuInfo;

@JsonInclude(value = Include.NON_EMPTY)
public class ResMenuInfo extends TMenuInfo {
    private String sFullMenuNm;

    private Long idAuthMenu;

    public ResMenuInfo(final Menu m) {
        this.setIdMenuInfo(m.getIdMenuInfo());
        this.setsMenuId(m.getsMenuId());
        this.setsUMenuId(m.getsUMenuId());
        this.setsMenuNm(m.getsMenuNm());
        this.setnLev(m.getnLev());
        this.setsPagePath(m.getsPagePath());
        this.setnOrder(m.getnOrder());
        this.setsMenuCat(m.getsMenuCat());
        this.setIdAuthMenu(m.getIdAuthMenu());
    }

    public ResMenuInfo(final TMenuInfo m) {
        this.setIdMenuInfo(m.getIdMenuInfo());
        this.setsMenuId(m.getsMenuId());
        this.setsUMenuId(m.getsUMenuId());
        this.setsMenuNm(m.getsMenuNm());
        this.setnLev(m.getnLev());
        this.setsPagePath(m.getsPagePath());
        this.setnOrder(m.getnOrder());
        this.setsMenuCat(m.getsMenuCat());
    }

    public String getsFullMenuNm() {
        return sFullMenuNm;
    }

    public void setsFullMenuNm(final String sFullMenuNm) {
        this.sFullMenuNm = sFullMenuNm;
    }

    public Long getIdAuthMenu() {
        return idAuthMenu;
    }

    public void setIdAuthMenu(Long idAuthMenu) {
        this.idAuthMenu = idAuthMenu;
    }
}