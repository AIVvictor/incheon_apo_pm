package com.sebang.ems.model.Dto;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import javax.validation.constraints.NotNull;

import com.sebang.ems.model.SiteMonGoal;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;
import lombok.ToString;

@ToString
@NoArgsConstructor
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@Builder
public class ReqSiteMonGoal {
    @NotNull(message = "idEngPoint 는 필수 입력 값입니다.")
    private Long idEngPoint;

    private BigDecimal m1;
    private BigDecimal m2;
    private BigDecimal m3;
    private BigDecimal m4;
    private BigDecimal m5;
    private BigDecimal m6;
    private BigDecimal m7;
    private BigDecimal m8;
    private BigDecimal m9;
    private BigDecimal m10;
    private BigDecimal m11;
    private BigDecimal m12;

    public Long getIdEngPoint() {
        return idEngPoint;
    }

    public void setIdEngPoint(Long idEngPoint) {
        this.idEngPoint = idEngPoint;
    }

    public BigDecimal getM1() {
        return m1;
    }

    public void setM1(BigDecimal m1) {
        this.m1 = m1;
    }

    public BigDecimal getM2() {
        return m2;
    }

    public void setM2(BigDecimal m2) {
        this.m2 = m2;
    }

    public BigDecimal getM3() {
        return m3;
    }

    public void setM3(BigDecimal m3) {
        this.m3 = m3;
    }

    public BigDecimal getM4() {
        return m4;
    }

    public void setM4(BigDecimal m4) {
        this.m4 = m4;
    }

    public BigDecimal getM5() {
        return m5;
    }

    public void setM5(BigDecimal m5) {
        this.m5 = m5;
    }

    public BigDecimal getM6() {
        return m6;
    }

    public void setM6(BigDecimal m6) {
        this.m6 = m6;
    }

    public BigDecimal getM7() {
        return m7;
    }

    public void setM7(BigDecimal m7) {
        this.m7 = m7;
    }

    public BigDecimal getM8() {
        return m8;
    }

    public void setM8(BigDecimal m8) {
        this.m8 = m8;
    }

    public BigDecimal getM9() {
        return m9;
    }

    public void setM9(BigDecimal m9) {
        this.m9 = m9;
    }

    public BigDecimal getM10() {
        return m10;
    }

    public void setM10(BigDecimal m10) {
        this.m10 = m10;
    }

    public BigDecimal getM11() {
        return m11;
    }

    public void setM11(BigDecimal m11) {
        this.m11 = m11;
    }

    public BigDecimal getM12() {
        return m12;
    }

    public void setM12(BigDecimal m12) {
        this.m12 = m12;
    }

    public List<SiteMonGoal> toEntity() {
        List<SiteMonGoal> ls = new ArrayList<SiteMonGoal>();
        if (m1 != null) {
            SiteMonGoal s = new SiteMonGoal();
            s.setIdEngPoint(this.idEngPoint);
            s.setcMon("1");
            s.setnMonEngGoal(this.m1);

            ls.add(s);
        }

        if (m2 != null) {
            SiteMonGoal s = new SiteMonGoal();
            s.setIdEngPoint(this.idEngPoint);
            s.setcMon("2");
            s.setnMonEngGoal(this.m2);

            ls.add(s);
        }

        if (m3 != null) {
            SiteMonGoal s = new SiteMonGoal();
            s.setIdEngPoint(this.idEngPoint);
            s.setcMon("3");
            s.setnMonEngGoal(this.m3);

            ls.add(s);
        }

        if (m4 != null) {
            SiteMonGoal s = new SiteMonGoal();
            s.setIdEngPoint(this.idEngPoint);
            s.setcMon("4");
            s.setnMonEngGoal(this.m4);

            ls.add(s);
        }

        if (m5 != null) {
            SiteMonGoal s = new SiteMonGoal();
            s.setIdEngPoint(this.idEngPoint);
            s.setcMon("5");
            s.setnMonEngGoal(this.m5);

            ls.add(s);
        }

        if (m6 != null) {
            SiteMonGoal s = new SiteMonGoal();
            s.setIdEngPoint(this.idEngPoint);
            s.setcMon("6");
            s.setnMonEngGoal(this.m6);

            ls.add(s);
        }

        if (m7 != null) {
            SiteMonGoal s = new SiteMonGoal();
            s.setIdEngPoint(this.idEngPoint);
            s.setcMon("7");
            s.setnMonEngGoal(this.m7);

            ls.add(s);
        }

        if (m8 != null) {
            SiteMonGoal s = new SiteMonGoal();
            s.setIdEngPoint(this.idEngPoint);
            s.setcMon("8");
            s.setnMonEngGoal(this.m8);

            ls.add(s);
        }

        if (m9 != null) {
            SiteMonGoal s = new SiteMonGoal();
            s.setIdEngPoint(this.idEngPoint);
            s.setcMon("9");
            s.setnMonEngGoal(this.m9);

            ls.add(s);
        }

        if (m10 != null) {
            SiteMonGoal s = new SiteMonGoal();
            s.setIdEngPoint(this.idEngPoint);
            s.setcMon("10");
            s.setnMonEngGoal(this.m10);

            ls.add(s);
        }

        if (m11 != null) {
            SiteMonGoal s = new SiteMonGoal();
            s.setIdEngPoint(this.idEngPoint);
            s.setcMon("11");
            s.setnMonEngGoal(this.m11);

            ls.add(s);
        }

        if (m12 != null) {
            SiteMonGoal s = new SiteMonGoal();
            s.setIdEngPoint(this.idEngPoint);
            s.setcMon("12");
            s.setnMonEngGoal(this.m12);

            ls.add(s);
        }
        return ls;
    }
}