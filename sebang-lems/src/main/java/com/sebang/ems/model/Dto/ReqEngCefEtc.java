package com.sebang.ems.model.Dto;

import javax.validation.constraints.NotNull;
import com.sebang.ems.model.Do.TEngCef;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@ToString
@Setter
@NoArgsConstructor
@AllArgsConstructor(access=AccessLevel.PRIVATE)
@Builder
public class ReqEngCefEtc
{
    private Long idEngCef;

    private String sCefCd;

    private String sEmiCd ;         // 배출원 구분


    private String sEmtCd ;         // 배출원 구분

    @NotNull(message = "입력단위(sInUntCd) 는 필수 입력값입니다.")
    private String sInUntCd;   // 입력 단위

    @NotNull(message = "기타온실가스명 (sOthCefNm) 는 필수 입력값입니다.")
    private String sOthCefNm;  // 기타온실가스

    @NotNull(message = "사용목적 (sOthPurpose) 는 필수 입력값입니다.")
    private String sOthPurpose;  // 사용목적

    private String sRegId;

    private String sModId;

    private String cDelYn;

    public Long getIdEngCef() {
        return idEngCef;
    }

    public String getsCefCd() {
        return sCefCd;
    }

    public void setsCefCd(String sCefCd) {
        this.sCefCd = sCefCd;
    }

    public String getsEmtCd() {
        return sEmtCd;
    }

    public void setsEmtCd(String sEmtCd) {
        this.sEmtCd = sEmtCd;
    }

    public String getsInUntCd() {
        return sInUntCd;
    }

    public void setsInUntCd(String sInUntCd) {
        this.sInUntCd = sInUntCd;
    }

    public String getsOthCefNm() {
        return sOthCefNm;
    }

    public void setsOthCefNm(String sOthCefNm) {
        this.sOthCefNm = sOthCefNm;
    }

    public String getsOthPurpose() {
        return sOthPurpose;
    }

    public void setsOthPurpose(String sOthPurpose) {
        this.sOthPurpose = sOthPurpose;
    }

    public String getsRegId() {
        return sRegId;
    }

    public void setsRegId(String sRegId) {
        this.sRegId = sRegId;
    }

    public String getsModId() {
        return sModId;
    }

    public void setsModId(String sModId) {
        this.sModId = sModId;
    }

    public String getcDelYn() {
        return cDelYn;
    }

    public void setcDelYn(String cDelYn) {
        this.cDelYn = cDelYn;
    }

    public String getsEmiCd() {
        return sEmiCd;
    }

    public void setsEmiCd(String sEmiCd) {
        this.sEmiCd = sEmiCd;
    }


    public ReqEngCef toEntity()
    {
        ReqEngCef c = new ReqEngCef();
        c.setIdEngCef(this.idEngCef);
        //c.setIdEngPoint(this.idEngPoint);
        c.setsCefCd(this.sCefCd);
        c.setsEmiCd(this.sEmiCd); // 기타
        c.setsEmtCd("9"); // 기타
        c.setsInUntCd(this.sInUntCd);

        c.setsRegId(this.sRegId);
        c.setsModId(this.sModId);
        c.setcDelYn(this.cDelYn);
        c.setsOthCefNm(this.sOthCefNm);
        c.setsOthPurpose(this.sOthPurpose);
        return c;
    }



}