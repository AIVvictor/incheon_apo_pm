package com.sebang.ems.model.Dto;

import javax.validation.constraints.NotNull;

import com.sebang.ems.model.Do.TEngPoint;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;
// import lombok.Getter;
// import lombok.Setter;
import lombok.ToString;

@ToString
@NoArgsConstructor
@AllArgsConstructor(access=AccessLevel.PRIVATE)
@Builder
public class ReqEngPoint
{

    private Long idEngPoint;

    @NotNull(message = "idEngCef 는 필수 입력 값입니다.")
    private Long idEngCef;

    @NotNull(message = "idSite 는 필수 입력 값입니다.")
    private Long idSite;

    private Integer subSeq;

  //  @NotNull(message = "sPointId 는 필수 입력 값입니다.")
    private String sPointId;

    @NotNull(message = "sPointNm 는 필수 입력 값입니다.")
    private String sPointNm;
    
    @NotNull(message = "cPointYn 는 필수 입력 값입니다.")
    private String cPointYn;

    private Short nOrder;

    private String sInvCd;

    private String sInvSeq;

    private String cSYear;

    private String cEYear;

    private String nCapa;

    private String sUntCapa;

    private String nCapa2;

    private String nUntCapa2;

    @NotNull(message = "sLogiTypeCd 는 필수 입력 값입니다.")
    private String sLogiTypeCd;

    private String sRegId;

    private String sModId;

    @NotNull(message = "cDelYn 는 필수 입력 값입니다.")
    private String cDelYn;

    private Float sBaseVal; // 원단위 기준 값

    private String sBaseUnt;  // 원단위 기준 단위

    private Float sFuelEff;   // 공인 연비

    

    public Long getIdEngPoint() {
        return idEngPoint;
    }

    public Long getIdEngCef() {
        return idEngCef;
    }

    public Long getIdSite() {
        return idSite;
    }

    public Integer getSubSeq() {
        return subSeq;
    }

    public String getsPointId() {
        return sPointId;
    }

    public void setsPointId(String sPointId) {
        this.sPointId = sPointId;
    }

    public String getsPointNm() {
        return sPointNm;
    }

    public void setsPointNm(String sPointNm) {
        this.sPointNm = sPointNm;
    }

    public String getcPointYn() {
        return cPointYn;
    }

    public void setcPointYn(String cPointYn) {
        this.cPointYn = cPointYn;
    }

    public Short getnOrder() {
        return nOrder;
    }

    public void setnOrder(Short nOrder) {
        this.nOrder = nOrder;
    }

    public String getsInvCd() {
        return sInvCd;
    }

    public void setsInvCd(String sInvCd) {
        this.sInvCd = sInvCd;
    }

    public String getsInvSeq() {
        return sInvSeq;
    }

    public void setsInvSeq(String sInvSeq) {
        this.sInvSeq = sInvSeq;
    }

    public String getcSYear() {
        return cSYear;
    }

    public void setcSYear(String cSYear) {
        this.cSYear = cSYear;
    }

    public String getcEYear() {
        return cEYear;
    }

    public void setcEYear(String cEYear) {
        this.cEYear = cEYear;
    }

    public String getnCapa() {
        return nCapa;
    }

    public void setnCapa(String nCapa) {
        this.nCapa = nCapa;
    }

    public String getsUntCapa() {
        return sUntCapa;
    }

    public void setsUntCapa(String sUntCapa) {
        this.sUntCapa = sUntCapa;
    }

    public String getnCapa2() {
        return nCapa2;
    }

    public void setnCapa2(String nCapa2) {
        this.nCapa2 = nCapa2;
    }

    public String getnUntCapa2() {
        return nUntCapa2;
    }

    public void setnUntCapa2(String nUntCapa2) {
        this.nUntCapa2 = nUntCapa2;
    }

    public String getsLogiTypeCd() {
        return sLogiTypeCd;
    }

    public void setsLogiTypeCd(String sLogicTypeCd) {
        this.sLogiTypeCd = sLogicTypeCd;
    }

    public String getsRegId() {
        return sRegId;
    }

    public void setsRegId(String sRegId) {
        this.sRegId = sRegId;
    }

    public String getsModId() {
        return sModId;
    }

    public void setsModId(String sModId) {
        this.sModId = sModId;
    }

    public String getcDelYn() {
        return cDelYn;
    }

    public void setcDelYn(String cDelYn) {
        this.cDelYn = cDelYn;
    }

    public void setIdEngPoint(Long idEngPoint) {
        this.idEngPoint = idEngPoint;
    }

    public void setIdEngCef(Long idEngCef) {
        this.idEngCef = idEngCef;
    }

    public void setIdSite(Long idSite) {
        this.idSite = idSite;
    }

    public void setSubSeq(Integer subSeq) {
        this.subSeq = subSeq;
    }

    public Float getsBaseVal() {
        return sBaseVal;
    }

    public void setsBaseVal(Float sBaseVal) {
        this.sBaseVal = sBaseVal;
    }

    public String getsBaseUnt() {
        return sBaseUnt;
    }

    public void setsBaseUnt(String sBaseUnt) {
        this.sBaseUnt = sBaseUnt;
    }

    public Float getsFuelEff() {
        return sFuelEff;
    }

    public void setsFuelEff(Float sFuelEff) {
        this.sFuelEff = sFuelEff;
    }

    public TEngPoint toEntity()
    {
        TEngPoint p = new TEngPoint();
        p.setIdEngPoint(this.idEngPoint);
        p.setIdEngCef(this.idEngCef);
        p.setIdSite(this.idSite);
        p.setSubSeq(this.subSeq);
        p.setsPointId(this.sPointId);
        p.setsPointNm(this.sPointNm);
        p.setcPointYn(this.cPointYn);
        p.setnOrder(this.nOrder);
        p.setsInvCd(this.sInvCd);
        p.setsInvSeq(this.sInvSeq);
        p.setcSYear(this.cSYear);
        p.setcEYear(this.cEYear);
        p.setnCapa(this.nCapa);
        p.setsUntCapa(this.sUntCapa);
        p.setnCapa2(this.nCapa2);
        p.setnUntCapa2(this.nUntCapa2);
        p.setsLogiTypeCd(this.sLogiTypeCd);
        p.setsRegId(this.sRegId);
        p.setsModId(this.sModId);
        p.setcDelYn(this.cDelYn);
        p.setsBaseVal(this.sBaseVal);
        p.setsBaseUnt(this.sBaseUnt);
        p.setsFuelEff(this.sFuelEff);
        
        return p;
    }
}