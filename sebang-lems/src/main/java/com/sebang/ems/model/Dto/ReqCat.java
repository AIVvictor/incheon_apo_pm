package com.sebang.ems.model.Dto;

import javax.validation.constraints.NotNull;

import com.sebang.ems.model.Do.TCat;

import lombok.Setter;
import lombok.ToString;

@ToString
@Setter
public class ReqCat
{
    private Long idCat;
    
    @NotNull(message = "sCat 은 필수 입력 값입니다. ")
    private String sCat;
    
    @NotNull(message = "sCatDesc 은 필수 입력 값입니다. ")
    private String sCatDesc;
    
    private String sCatExp;
    
    private String sUseYn;
    
    private String sEditYn;

    @NotNull(message = "cDelYn 은 필수 입력 값입니다. ")
    private String cDelYn;

    private String sRegId;

    private String sModId;

    public Long getIdCat() {
        return idCat;
    }

    public String getsCat() {
        return sCat;
    }

    public void setsCat(String sCat) {
        this.sCat = sCat;
    }

    public String getsCatDesc() {
        return sCatDesc;
    }

    public void setsCatDesc(String sCatDesc) {
        this.sCatDesc = sCatDesc;
    }

    public String getsCatExp() {
        return sCatExp;
    }

    public void setsCatExp(String sCatExp) {
        this.sCatExp = sCatExp;
    }

    public String getsUseYn() {
        return sUseYn;
    }

    public void setsUseYn(String sUseYn) {
        this.sUseYn = sUseYn;
    }

    public String getsEditYn() {
        return sEditYn;
    }

    public void setsEditYn(String sEditYn) {
        this.sEditYn = sEditYn;
    }

    public String getcDelYn() {
        return cDelYn;
    }

    public void setcDelYn(String cDelYn) {
        this.cDelYn = cDelYn;
    }

    public String getsRegId() {
        return sRegId;
    }

    public void setsRegId(String sRegId) {
        this.sRegId = sRegId;
    }

    public String getsModId() {
        return sModId;
    }

    public void setsModId(String sModId) {
        this.sModId = sModId;
    }

    public TCat toEntity()
    {
        TCat c = new TCat();
        c.setIdCat(this.idCat);
        c.setsCat(this.sCat);
        c.setsCatDesc(this.sCatDesc);
        c.setsCatExp(this.sCatExp);
        c.setsUseYn(this.sUseYn);
        c.setsEditYn(this.sEditYn);
        c.setcDelYn(this.cDelYn);
        c.setsModId(this.sModId);
        c.setsRegId(this.sRegId);
        return c;
    }
}