package com.sebang.ems.model;

import java.math.BigDecimal;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.Setter;
import lombok.ToString;

@ToString
@Setter
@JsonInclude(value = Include.NON_EMPTY)
public class SumAmt4Home
{
    private String cYear;

    private BigDecimal nTco2All;

    private BigDecimal nTco2Logis;

    private BigDecimal nTco2NonLogis;

    private BigDecimal nGjAll;

    private BigDecimal nGjLogis;

    private BigDecimal nGjNonLogis;

    public String getcYear() {
        return cYear;
    }

    public BigDecimal getnTco2All() {
        return nTco2All;
    }

    public BigDecimal getnTco2Logis() {
        return nTco2Logis;
    }

    public BigDecimal getnTco2NonLogis() {
        return nTco2NonLogis;
    }

    public BigDecimal getnGjAll() {
        return nGjAll;
    }

    public BigDecimal getnGjLogis() {
        return nGjLogis;
    }

    public BigDecimal getnGjNonLogis() {
        return nGjNonLogis;
    }
}