package com.sebang.ems.model.Dto;

import javax.validation.constraints.NotNull;

import com.sebang.ems.model.Do.TCode;

import lombok.Setter;
import lombok.ToString;

@ToString
@Setter
public class ReqCode{
    
    private Long idCode;

    @NotNull(message = "sCat 은 필수 입력 값입니다.")
    private String sCat;
    
    @NotNull(message = "sCd 은 필수 입력 값입니다.")
    private String sCd;
    
    @NotNull(message = "sDesc 은 필수 입력 값입니다.")
    private String sDesc;
    
    private String sExp;
    
    private String sVal;
    
    private String nOrder;
    
    private String sMrvCd;
    
    private String sMrvDesc;
    
    @NotNull(message = "cDelYn 은 필수 입력 값입니다.")
    private String cDelYn;

    private String sRegId;

    private String sModId;

    public Long getIdCode() {
        return idCode;
    }

    public String getsCat() {
        return sCat;
    }

    public void setsCat(String sCat) {
        this.sCat = sCat;
    }

    public String getsCd() {
        return sCd;
    }

    public void setsCd(String sCd) {
        this.sCd = sCd;
    }

    public String getsDesc() {
        return sDesc;
    }

    public void setsDesc(String sDesc) {
        this.sDesc = sDesc;
    }

    public String getsExp() {
        return sExp;
    }

    public void setsExp(String sExp) {
        this.sExp = sExp;
    }

    public String getsVal() {
        return sVal;
    }

    public void setsVal(String sVal) {
        this.sVal = sVal;
    }

    public String getnOrder() {
        return nOrder;
    }

    public void setnOrder(String nOrder) {
        this.nOrder = nOrder;
    }

    public String getsMrvCd() {
        return sMrvCd;
    }

    public void setsMrvCd(String sMrvCd) {
        this.sMrvCd = sMrvCd;
    }

    public String getsMrvDesc() {
        return sMrvDesc;
    }

    public void setsMrvDesc(String sMrvDesc) {
        this.sMrvDesc = sMrvDesc;
    }

    public String getcDelYn() {
        return cDelYn;
    }

    public void setcDelYn(String cDelYn) {
        this.cDelYn = cDelYn;
    }

    public String getsRegId() {
        return sRegId;
    }

    public void setsRegId(String sRegId) {
        this.sRegId = sRegId;
    }

    public String getsModId() {
        return sModId;
    }

    public void setsModId(String sModId) {
        this.sModId = sModId;
    }

    public TCode toEntity()
    {
        TCode c = new TCode();
        c.setIdCode(this.idCode);
        c.setsCat(this.sCat);
        c.setsCd(this.sCd);
        c.setsDesc(this.sDesc);
        c.setsExp(this.sExp);
        c.setsVal(this.sVal);
        c.setnOrder(this.nOrder);
        c.setsMrvCd(this.sMrvCd);
        c.setsMrvDesc(this.sMrvDesc);
        c.setcDelYn(this.cDelYn);
        c.setsRegId(this.sRegId);
        c.setsModId(this.sModId);
    
        return c;
    }

    


}