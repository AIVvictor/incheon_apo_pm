package com.sebang.ems.model.Dto;

import javax.validation.constraints.NotNull;

import com.sebang.ems.model.Do.TBoard;

import lombok.Setter;
import lombok.ToString;

@Setter
@ToString
public class ReqBoard
{
    private String idBoard;
    private String sBoardType;
    @NotNull(message = "제목은 필수 입력 값입니다.")
    private String sTitle;
    private String sContent;
    @NotNull(message = "작성자 ID는 필수 입력 값입니다.")
    private String sRegEmpId;
    private String gFileId;

    public String getIdBoard() {
        return idBoard;
    }

    public String getsBoardType() {
        return sBoardType;
    }

    public void setsBoardType(String sBoardType) {
        this.sBoardType = sBoardType;
    }

    public String getsTitle() {
        return sTitle;
    }

    public void setsTitle(String sTitle) {
        this.sTitle = sTitle;
    }

    public String getsContent() {
        return sContent;
    }

    public void setsContent(String sContent) {
        this.sContent = sContent;
    }

    public String getsRegEmpId() {
        return sRegEmpId;
    }

    public void setsRegEmpId(String sRegEmpId) {
        this.sRegEmpId = sRegEmpId;
    }

    public String getgFileId() {
        return gFileId;
    }

    public void setgFileId(String gFileId) {
        this.gFileId = gFileId;
    }


    public TBoard toEntity()
    {
        TBoard b = new TBoard();

        b.setIdBoard(this.idBoard);
        b.setsBoardType(this.sBoardType);
        b.setsTitle(this.sTitle);
        b.setsContent(this.sContent);
        b.setsRegEmpId(this.sRegEmpId);
        b.setgFileId(this.gFileId);
        
        return b;
    }
}