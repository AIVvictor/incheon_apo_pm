package com.sebang.ems.model.Dto;

import java.math.BigDecimal;

import javax.validation.constraints.NotNull;
import com.sebang.ems.model.Do.TEngCef;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@ToString
@Setter
@NoArgsConstructor
@AllArgsConstructor(access=AccessLevel.PRIVATE)
@Builder
public class ReqEngCef
{
    private Long idEngCef;

   // private Long idEngPoint;

    @NotNull(message = "sCefCd 는 필수 입력값입니다.")
    private String sCefCd;    // 배출 구분 

    @NotNull(message = "sEngCd 는 필수 입력값입니다.")
    private String sEngCd;    // 사용에너지원 코드

    @NotNull(message = "sMrvengCd 는 필수 입력값입니다.")
    private String sMrvengCd;  // 정부 보고 에너지원

    @NotNull(message = "sEmtCd 는 필수 입력값입니다.")
    private String sEmtCd;     // 배출원 구분

    private String sInUntCd;   // 입력 단위

    private String sCalcUntCd; // 계산 단위 

    private BigDecimal nFactor;     // 단위 환산계수

    private BigDecimal nOxyVal;     // 산화율

    @NotNull(message = "sEmiCd 는 필수 입력값입니다.")
    private String sEmiCd;     // 배출 활동 구분
 
    private String cTier;      // 계산식 Tier  

    private String cActvTier;  // 활동도 Tier

    private String cHeatTier;  // 발열량 Tier

    private String cCefTier;   // 배출계수 Tier

    private String cOxyTier;   // 산화률 Tier

    private String sRegId;

    private String sModId;

    private String cDelYn;

    private String sOthCefNm;  // 기타온실가스

    private String sOthPurpose;  // 사용목적

    public Long getIdEngCef() {
        return idEngCef;
    }

    // public Long getIdEngPoint() {
    //     return idEngPoint;
    // }

    public String getsCefCd() {
        return sCefCd;
    }

    public void setsCefCd(String sCefCd) {
        this.sCefCd = sCefCd;
    }

    public String getsEngCd() {
        return sEngCd;
    }

    public void setsEngCd(String sEngCd) {
        this.sEngCd = sEngCd;
    }

    public String getsMrvengCd() {
        return sMrvengCd;
    }

    public void setsMrvengCd(String sMrvengCd) {
        this.sMrvengCd = sMrvengCd;
    }

    public String getsEmtCd() {
        return sEmtCd;
    }

    public void setsEmtCd(String sEmtCd) {
        this.sEmtCd = sEmtCd;
    }

    public String getsInUntCd() {
        return sInUntCd;
    }

    public void setsInUntCd(String sInUntCd) {
        this.sInUntCd = sInUntCd;
    }

    public String getsCalcUntCd() {
        return sCalcUntCd;
    }

    public void setsCalcUntCd(String sCalcUntCd) {
        this.sCalcUntCd = sCalcUntCd;
    }

    public BigDecimal getnFactor() {
        return nFactor;
    }

    public void setnFactor(BigDecimal nFactor) {
        this.nFactor = nFactor;
    }

    public BigDecimal getnOxyVal() {
        return nOxyVal;
    }

    public void setnOxyVal(BigDecimal nOxyVal) {
        this.nOxyVal = nOxyVal;
    }

    public String getsEmiCd() {
        return sEmiCd;
    }

    public void setsEmiCd(String sEmiCd) {
        this.sEmiCd = sEmiCd;
    }

    public String getcTier() {
        return cTier;
    }

    public void setcTier(String cTier) {
        this.cTier = cTier;
    }

    public String getcActvTier() {
        return cActvTier;
    }

    public void setcActvTier(String cActvTier) {
        this.cActvTier = cActvTier;
    }

    public String getcHeatTier() {
        return cHeatTier;
    }

    public void setcHeatTier(String cHeatTier) {
        this.cHeatTier = cHeatTier;
    }

    public String getcCefTier() {
        return cCefTier;
    }

    public void setcCefTier(String cCefTier) {
        this.cCefTier = cCefTier;
    }

    public String getcOxyTier() {
        return cOxyTier;
    }

    public void setcOxyTier(String cOxyTier) {
        this.cOxyTier = cOxyTier;
    }

    public String getsRegId() {
        return sRegId;
    }

    public void setsRegId(String sRegId) {
        this.sRegId = sRegId;
    }

    public String getsModId() {
        return sModId;
    }

    public void setsModId(String sModId) {
        this.sModId = sModId;
    }

    public String getcDelYn() {
        return cDelYn;
    }

    public void setcDelYn(String cDelYn) {
        this.cDelYn = cDelYn;
    }

    public String getsOthCefNm() {
        return sOthCefNm;
    }

    public void setsOthCefNm(String sOthCefNm) {
        this.sOthCefNm = sOthCefNm;
    }

    public String getsOthPurpose() {
        return sOthPurpose;
    }

    public void setsOthPurpose(String sOthPurpose) {
        this.sOthPurpose = sOthPurpose;
    }

    public TEngCef toEntity()
    {
        TEngCef c = new TEngCef();
        c.setIdEngCef(this.idEngCef);
        //c.setIdEngPoint(this.idEngPoint);
        c.setsCefCd(this.sCefCd);
        c.setsEngCd(this.sEngCd);
        c.setsMrvengCd(this.sMrvengCd);
        c.setsEmtCd(this.sEmtCd);
        c.setsInUntCd(this.sInUntCd);
        c.setsCalcUntCd(this.sCalcUntCd);
        c.setnFactor(this.nFactor);
        c.setnOxyVal(this.nOxyVal);
        c.setsEmiCd(this.sEmiCd);
        c.setcTier(this.cTier);
        c.setcActvTier(this.cActvTier);
        c.setcHeatTier(this.cHeatTier);
        c.setcCefTier(this.cCefTier);
        c.setcOxyTier(this.cOxyTier);
        c.setsRegId(this.sRegId);
        c.setsModId(this.sModId);
        c.setcDelYn(this.cDelYn);
        c.setsOthCefNm(this.sOthCefNm);
        c.setsOthPurpose(this.sOthPurpose);
        
        return c;
    }
}