package com.sebang.ems.model.Dto;

import com.opencsv.bean.CsvBindByPosition;
import com.sebang.ems.model.Do.TErpUsers;
import com.sebang.ems.util.DateUtil;
import com.sebang.ems.util.StringUtil;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Setter
@Getter
@ToString
public class ReqErpUsers {
    private Long idErpUsers;

    @CsvBindByPosition(position = 0)
    private String sEmpId; // 사번

    @CsvBindByPosition(position = 1)
    private String sEmpNm; // 성명

    @CsvBindByPosition(position = 2)
    private String sDeptId; // 부서코드

    @CsvBindByPosition(position = 3)
    private String sEmpMail; // 이메일

    @CsvBindByPosition(position = 4)
    private String cRankCd; // 직급 코드

    @CsvBindByPosition(position = 5)
    private String cRankNm; // 직급명

    @CsvBindByPosition(position = 6)
    private String cPosCd; // 직책코드

    @CsvBindByPosition(position = 7)
    private String cPosNm; // 직책이름

    @CsvBindByPosition(position = 8)
    private String cEDate; // 퇴사일

    private String cSDate;

    private String cDelYn; // 삭제 여부

    public TErpUsers toEntity() {
        TErpUsers u = new TErpUsers();
        u.setIdErpUsers(this.idErpUsers);
        u.setsEmpId(this.sEmpId);
        u.setsEmpNm(this.sEmpNm);
        u.setsDeptId(this.sDeptId);
        
        if(this.cSDate!=null && this.cSDate.length()>0)
            u.setcSDate(DateUtil.convertYYYYMMDDToDate(this.cSDate));
        if(this.cEDate!=null && this.cEDate.length()>0)        
            u.setcEDate(DateUtil.convertYYYYMMDDToDate(this.cEDate));
        
        u.setsEmpMail(this.sEmpMail);
        u.setcRankCd(this.cRankCd);
        u.setcRankNm(this.cRankNm);
        u.setcPosCd(this.cPosCd);
        u.setcPosNm(this.cPosNm);
        if(this.cEDate!=null && this.cEDate.length()>0)
            u.setcDelYn("Y");
        else
            u.setcDelYn("N");
        return u;
    }

    public boolean isEqual(TErpUsers u) {
        boolean ret = true;

        if(!StringUtil.isEmpty(this.sEmpId))
        {
            if (!sEmpId.equals(u.getsEmpId()) )
            return false;
        }else
        {
            if(!StringUtil.isEmpty(u.getsEmpId()))
                return false;
        }


        if (!StringUtil.isEmpty(this.sEmpNm)) {
            if (!sEmpNm.equals(u.getsEmpNm()) )
                return false;
        } else {
            if (!StringUtil.isEmpty(u.getsEmpNm()))
                return false;
        }

        if (!StringUtil.isEmpty(this.sDeptId)) {
            if (!sDeptId.equals(u.getsDeptId()))
                return false;
        } else {
            if (!StringUtil.isEmpty(u.getsDeptId()))
                return false;
        }

        if (!StringUtil.isEmpty(this.cSDate)) {
            String s = DateUtil.convertToYYYYMMDD(u.getcSDate(),"-");
            if (!cSDate.equals(s))
                return false;
        } else {
            if (u.getcSDate() != null)
                return false;
        }

        if (!StringUtil.isEmpty(this.cEDate)) {
            String s = DateUtil.convertToYYYYMMDD(u.getcEDate(),"-");
            if (!cEDate.equals(s) )
                return false;
        } else {
            if (u.getcEDate() != null)
                return false;
        }

        if (!StringUtil.isEmpty(this.sEmpMail)) {
            if (!sEmpMail.equals(u.getsEmpMail()) )
                return false;
        } else {
            if (!StringUtil.isEmpty(u.getsEmpMail()))
                return false;
        }

        if (!StringUtil.isEmpty(this.cRankCd)) {
            if (!cRankCd.equals(u.getcRankCd()))
                return false;
        } else {
            if (!StringUtil.isEmpty(u.getcRankCd()))
                return false;
        }

        if (!StringUtil.isEmpty(this.cRankNm )) {
            if (!cRankNm.equals(u.getcRankNm()) )
                return false;
        } else {
            if (!StringUtil.isEmpty(u.getcRankNm()))
                return false;
        }

        if (!StringUtil.isEmpty(this.cPosCd)) {
            if (!cPosCd.equals(u.getcPosCd()))
                return false;
        } else {
            if (!StringUtil.isEmpty(u.getcPosCd()))
                return false;
        }

        if (!StringUtil.isEmpty(this.cPosNm)) {
            if (!this.cPosNm.equals(u.getcPosNm()) )
                return false;
        } else {
            if (!StringUtil.isEmpty(u.getcPosNm()))
                return false;
        }

        // if (!StringUtil.isEmpty(this.cDelYn)) {
        //     if (!this.cDelYn.equals(u.getcDelYn()) )
        //         return false;
        // } else {
        //     if (!StringUtil.isEmpty(u.getcDelYn()))
        //         return false;
        // }
 
        return ret;
    }

}