package com.sebang.ems.model.Dto;

import javax.validation.constraints.NotNull;

import com.sebang.ems.model.Do.TSiteInfo;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@Builder
public class ReqSite {

    private Long idSite;

    // 권선임 요청 사항 11/13
    //@NotNull(message = "사업장 ID 는 필수 입력 값입니다.") 
    private String sSiteId;

    private Short nOrder;

    @NotNull(message = "사업장 명은 필수 입력 값입니다.")
    private String sSiteNm;

    private String sOwner;

    private String sSiteRegNum;

    private String sSiteAddr;

    private String sSiteTel;

    private String sSiteCat;

    @NotNull(message = "사업장 구분은 필수 입력 값입니다.")
    private String sSitePartyCd;

    private String cCloseYear;

    private String sPicNm;

    private String sPicDept;

    private String sPicPos;

    private String sPicTel;

    private String sPicHp;

    private String sPicMail;

    private String sPicFax;

    private String sMainPrd;

    private String sSmlEmiYn;

    private String sCertPro;

    private String sCertEnv;

    private String sRegId;

    private String sModId;

    @NotNull(message = "삭제여부는 필수 입력 값입니다.")
    private String cDelYn;

    private String gMap;

    private String gPhoto;

    private String gArr;

    private String gPro;

    public Long getIdSite() {
        return idSite;
    }

    public String getsSiteId() {
        return sSiteId;
    }

    public void setsSiteId(String sSiteId) {
        this.sSiteId = sSiteId;
    }

    public Short getnOrder() {
        return nOrder;
    }

    public void setnOrder(Short nOrder) {
        this.nOrder = nOrder;
    }

    public String getsSiteNm() {
        return sSiteNm;
    }

    public void setsSiteNm(String sSiteNm) {
        this.sSiteNm = sSiteNm;
    }

    public String getsOwner() {
        return sOwner;
    }

    public void setsOwner(String sOwner) {
        this.sOwner = sOwner;
    }

    public String getsSiteRegNum() {
        return sSiteRegNum;
    }

    public void setsSiteRegNum(String sSiteRegNum) {
        this.sSiteRegNum = sSiteRegNum;
    }

    public String getsSiteAddr() {
        return sSiteAddr;
    }

    public void setsSiteAddr(String sSiteAddr) {
        this.sSiteAddr = sSiteAddr;
    }

    public String getsSiteTel() {
        return sSiteTel;
    }

    public void setsSiteTel(String sSiteTel) {
        this.sSiteTel = sSiteTel;
    }

    public String getsSiteCat() {
        return sSiteCat;
    }

    public void setsSiteCat(String sSiteCat) {
        this.sSiteCat = sSiteCat;
    }

    public String getsSitePartyCd() {
        return sSitePartyCd;
    }

    public void setsParty(String sParty) {
        this.sSitePartyCd = sParty;
    }

    public String getcCloseYear() {
        return cCloseYear;
    }

    public void setcCloseYear(String cCloseYear) {
        this.cCloseYear = cCloseYear;
    }

    public String getsPicNm() {
        return sPicNm;
    }

    public void setsPicNm(String sPicNm) {
        this.sPicNm = sPicNm;
    }

    public String getsPicDept() {
        return sPicDept;
    }

    public void setsPicDept(String sPicDept) {
        this.sPicDept = sPicDept;
    }

    public String getsPicPos() {
        return sPicPos;
    }

    public void setsPicPos(String sPicPos) {
        this.sPicPos = sPicPos;
    }

    public String getsPicTel() {
        return sPicTel;
    }

    public void setsPicTel(String sPicTel) {
        this.sPicTel = sPicTel;
    }

    public String getsPicHp() {
        return sPicHp;
    }

    public void setsPicHp(String sPicHp) {
        this.sPicHp = sPicHp;
    }

    public String getsPicMail() {
        return sPicMail;
    }

    public void setsPicMail(String sPicMail) {
        this.sPicMail = sPicMail;
    }

    public String getsPicFax() {
        return sPicFax;
    }

    public void setsPicFax(String sPicFax) {
        this.sPicFax = sPicFax;
    }

    public String getsMainPrd() {
        return sMainPrd;
    }

    public void setsMainPrd(String sMainPrd) {
        this.sMainPrd = sMainPrd;
    }

    public String getsSmlEmiYn() {
        return sSmlEmiYn;
    }

    public void setsSmlEmiYn(String sSmlEmiYn) {
        this.sSmlEmiYn = sSmlEmiYn;
    }

    public String getsCertPro() {
        return sCertPro;
    }

    public void setsCertPro(String sCertPro) {
        this.sCertPro = sCertPro;
    }

    public String getsCertEnv() {
        return sCertEnv;
    }

    public void setsCertEnv(String sCertEnv) {
        this.sCertEnv = sCertEnv;
    }

    public String getsRegId() {
        return sRegId;
    }

    public void setsRegId(String sRegId) {
        this.sRegId = sRegId;
    }


    public String getsModId() {
        return sModId;
    }

    public void setsModId(String sModId) {
        this.sModId = sModId;
    }

    public String getcDelYn() {
        return cDelYn;
    }

    public void setcDelYn(String cDelYn) {
        this.cDelYn = cDelYn;
    }

    public String getgMap() {
        return gMap;
    }

    public void setgMap(String gMap) {
        this.gMap = gMap;
    }

    public String getgPhoto() {
        return gPhoto;
    }

    public void setgPhoto(String gPhoto) {
        this.gPhoto = gPhoto;
    }

    public String getgArr() {
        return gArr;
    }

    public void setgArr(String gArr) {
        this.gArr = gArr;
    }

    public String getgPro() {
        return gPro;
    }

    public void setgPro(String gPro) {
        this.gPro = gPro;
    }

    public TSiteInfo toEntity() {
         TSiteInfo t = new TSiteInfo();
            t.setIdSite(this.idSite);
            t.setsSiteId(this.sSiteId);
            t.setnOrder(this.nOrder);
            t.setsSiteNm(this.sSiteNm) ;
            t.setsOwner(this.sOwner) ;
            t.setsSiteRegNum(this.sSiteRegNum);
            t.setsSiteAddr(this.sSiteAddr) ;
            t.setsSiteTel(this.sSiteTel) ;
            t.setsSiteCat(this.sSiteCat) ;
            t.setsSitePartyCd(this.sSitePartyCd) ;
            t.setcCloseYear(this.cCloseYear) ;
            t.setsPicNm(this.sPicNm) ;
            t.setsPicDept(this.sPicDept) ;
            t.setsPicPos(this.sPicPos) ;
            t.setsPicTel(this.sPicTel) ;
            t.setsPicHp(this.sPicHp) ;
            t.setsPicMail(this.sPicMail) ;
            t.setsPicFax(this.sPicFax) ;
            t.setsMainPrd(this.sMainPrd) ;
            t.setsSmlEmiYn(this.sSmlEmiYn) ;
            t.setsCertPro(this.sCertPro) ;
            t.setsCertEnv(this.sCertEnv) ;
            t.setsRegId(this.sRegId) ;               
            t.setsModId(this.sModId) ;
            t.setcDelYn(this.cDelYn) ;
            t.setgMap(this.gMap) ;
            t.setgPhoto(this.gPhoto) ;
            t.setgArr(this.gArr) ;
            t.setgPro(this.gPro) ;
            return t;
    }

}