package com.sebang.ems.model;

import com.sebang.ems.model.Do.TSiteInfo;

import lombok.Setter;
import lombok.ToString;

/**
 * SiteDept 사업장 정보
 * @author promaker
 *
 */
@Setter
//@Getter
@ToString
//@JsonInclude(value = Include.NON_EMPTY)
public class SiteDept extends TSiteInfo{

    Long idErpDept;

    String sDeptId;

    String sUDeptId;

    String sDeptNm;

    String sFullDeptId;

    String sFullDeptNm;

    String sMapFileNm;

    String sPhotoFileNm;

    String sArrFileNm;

    String sProFileNm;

    /**
     * ID파일정보
     */
    String sMapIdFile;     
    
    /**
     * ID파일정보
     */
    String sPhotoIdFile; 
    
    /**
     * ID파일정보
     */
    String sArrIdFile;  

    /**
     * ID파일정보
     */
    String sProIdFile;

    Long idGasDeptMapping;

    public Long getIdErpDept()
    {
        return idErpDept;
    }

    public String getsDeptId()
    {
        return sDeptId;
    }

    public String getsUDeptId()
    {
        return sUDeptId;
    }
    
    public String getsDeptNm()
    {
        return sDeptNm;
    }
    
    public String getsFullDeptId()
    {
        return sFullDeptId;
    }
    
    public String getsFullDeptNm()
    {
        return sFullDeptNm;
    }

    public String getsMapFileNm(){
        return sMapFileNm;
    }

    public String getsPhotoFileNm(){
        return sPhotoFileNm;
    }

    public String getsArrFileNm(){
        return sArrFileNm;
    };

    public String getsProFileNm(){
        return sProFileNm;
    };
    
    /**
     * ID파일정보
     */
	public String getsMapIdFile() {
		return sMapIdFile;
	}
    /**
     * ID파일정보
     */
	public String getsPhotoIdFile() {
		return sPhotoIdFile;
	}
    /**
     * ID파일정보
     */
	public String getsArrIdFile() {
		return sArrIdFile;
	}
    /**
     * ID파일정보
     */
	public String getsProIdFile() {
		return sProIdFile;
	}

    public Long getIdGasDeptMapping() {
        return idGasDeptMapping;
    }

}