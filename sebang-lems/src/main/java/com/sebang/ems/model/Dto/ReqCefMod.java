package com.sebang.ems.model.Dto;

import java.math.BigDecimal;

import javax.validation.constraints.NotNull;

import com.sebang.ems.model.Do.TCefMod;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@ToString
@Setter
@NoArgsConstructor
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@Builder
public class ReqCefMod {
    private Long idCefMod;

    @NotNull(message = "idEngCef 는 필수 입력 값입니다.")
    private Long idEngCef;
    private String cYear;
    private BigDecimal nHeat;
    private BigDecimal nTotHeat;
    private BigDecimal nCo2Cef;
    private BigDecimal nCh4Cef;
    private BigDecimal nN2oCef;
    private BigDecimal nModCal;
    private String sGhgCd;
    private BigDecimal cPer;

    private String sRegId;
    private String sModId;
    private BigDecimal nToeCef;

    public Long getIdCefMod() {
        return idCefMod;
    }

    public Long getIdEngCef() {
        return idEngCef;
    }

    public String getcYear() {
        return cYear;
    }

    public void setcYear(String cYear) {
        this.cYear = cYear;
    }

    public BigDecimal getnHeat() {
        return nHeat;
    }

    public void setnHeat(BigDecimal nHeat) {
        this.nHeat = nHeat;
    }

    public BigDecimal getnTotHeat() {
        return nTotHeat;
    }

    public void setnTotHeat(BigDecimal nTotHeat) {
        this.nTotHeat = nTotHeat;
    }

    public BigDecimal getnCo2Cef() {
        return nCo2Cef;
    }

    public void setnCo2Cef(BigDecimal nCo2Cef) {
        this.nCo2Cef = nCo2Cef;
    }

    public BigDecimal getnCh4Cef() {
        return nCh4Cef;
    }

    public void setnCh4Cef(BigDecimal nCh4Cef) {
        this.nCh4Cef = nCh4Cef;
    }

    public BigDecimal getnN2oCef() {
        return nN2oCef;
    }

    public void setnN2oCef(BigDecimal nN2oCef) {
        this.nN2oCef = nN2oCef;
    }

    public BigDecimal getnModCal() {
        return nModCal;
    }

    public void setnModCal(BigDecimal nModCal) {
        this.nModCal = nModCal;
    }

    public String getsGhgCd() {
        return sGhgCd;
    }

    public void setsGhgCd(String sGhgCd) {
        this.sGhgCd = sGhgCd;
    }

    public BigDecimal getcPer() {
        return cPer;
    }

    public void setcPer(BigDecimal cPer) {
        this.cPer = cPer;
    }

    public String getsRegId() {
        return sRegId;
    }

    public void setsRegId(String sRegId) {
        this.sRegId = sRegId;
    }

    public String getsModId() {
        return sModId;
    }

    public void setsModId(String sModId) {
        this.sModId = sModId;
    }

    public BigDecimal getnToeCef() {
        return nToeCef;
    }

    public void setnToeCef(BigDecimal nToeCef) {
        this.nToeCef = nToeCef;
    }
    
    public TCefMod toEntity()
    {
        TCefMod c = new TCefMod();

        c.setIdCefMod(this.idCefMod);
        c.setIdEngCef(this.idEngCef);
        c.setcYear(this.cYear);
        c.setnHeat(this.nHeat);
        c.setnTotHeat(this.nTotHeat);
        c.setnCo2Cef(this.nCo2Cef);
        c.setnCh4Cef(this.nCh4Cef);
        c.setnN2oCef(this.nN2oCef);
        c.setnModCal(this.nModCal);
        c.setsGhgCd(this.sGhgCd);
        c.setcPer(this.cPer);
        c.setsRegId(this.sRegId);
        c.setsModId(this.sModId);
        c.setnToeCef(this.nToeCef);
        return c;
    }

 
 



}