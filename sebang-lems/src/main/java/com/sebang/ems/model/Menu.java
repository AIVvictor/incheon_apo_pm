package com.sebang.ems.model;

import com.sebang.ems.model.Do.TMenuInfo;

import lombok.ToString;

@ToString(callSuper = true)
public class Menu extends TMenuInfo
{
    private Long idAuthMenu;
   
    private String sAuthCd;

    public String getsAuthCd() {
        return sAuthCd;
    }

    public void setsAuthCd(String sAuthCd) {
        this.sAuthCd = sAuthCd;
    }

    public Long getIdAuthMenu() {
        return idAuthMenu;
    }

    public void setIdAuthMenu(Long idAuthMenu) {
        this.idAuthMenu = idAuthMenu;
    }
}