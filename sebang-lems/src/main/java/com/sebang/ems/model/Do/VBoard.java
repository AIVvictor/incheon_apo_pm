package com.sebang.ems.model.Do;

import java.util.Date;

public class VBoard {
    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column V_BOARD.ID_BOARD
     *
     * @mbg.generated Thu Oct 24 19:58:41 KST 2019
     */
    private Long idBoard;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column V_BOARD.S_BOARD_TYPE
     *
     * @mbg.generated Thu Oct 24 19:58:41 KST 2019
     */
    private String sBoardType;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column V_BOARD.S_TITLE
     *
     * @mbg.generated Thu Oct 24 19:58:41 KST 2019
     */
    private String sTitle;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column V_BOARD.S_CONTENT
     *
     * @mbg.generated Thu Oct 24 19:58:41 KST 2019
     */
    private String sContent;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column V_BOARD.S_REG_EMP_ID
     *
     * @mbg.generated Thu Oct 24 19:58:41 KST 2019
     */
    private String sRegEmpId;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column V_BOARD.S_EMP_NM
     *
     * @mbg.generated Thu Oct 24 19:58:41 KST 2019
     */
    private String sEmpNm;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column V_BOARD.D_REG_DATE
     *
     * @mbg.generated Thu Oct 24 19:58:41 KST 2019
     */
    private Date dRegDate;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column V_BOARD.G_FILE_ID
     *
     * @mbg.generated Thu Oct 24 19:58:41 KST 2019
     */
    private String gFileId;

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column V_BOARD.ID_BOARD
     *
     * @return the value of V_BOARD.ID_BOARD
     *
     * @mbg.generated Thu Oct 24 19:58:41 KST 2019
     */
    public Long getIdBoard() {
        return idBoard;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column V_BOARD.ID_BOARD
     *
     * @param idBoard the value for V_BOARD.ID_BOARD
     *
     * @mbg.generated Thu Oct 24 19:58:41 KST 2019
     */
    public void setIdBoard(Long idBoard) {
        this.idBoard = idBoard;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column V_BOARD.S_BOARD_TYPE
     *
     * @return the value of V_BOARD.S_BOARD_TYPE
     *
     * @mbg.generated Thu Oct 24 19:58:41 KST 2019
     */
    public String getsBoardType() {
        return sBoardType;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column V_BOARD.S_BOARD_TYPE
     *
     * @param sBoardType the value for V_BOARD.S_BOARD_TYPE
     *
     * @mbg.generated Thu Oct 24 19:58:41 KST 2019
     */
    public void setsBoardType(String sBoardType) {
        this.sBoardType = sBoardType;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column V_BOARD.S_TITLE
     *
     * @return the value of V_BOARD.S_TITLE
     *
     * @mbg.generated Thu Oct 24 19:58:41 KST 2019
     */
    public String getsTitle() {
        return sTitle;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column V_BOARD.S_TITLE
     *
     * @param sTitle the value for V_BOARD.S_TITLE
     *
     * @mbg.generated Thu Oct 24 19:58:41 KST 2019
     */
    public void setsTitle(String sTitle) {
        this.sTitle = sTitle;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column V_BOARD.S_CONTENT
     *
     * @return the value of V_BOARD.S_CONTENT
     *
     * @mbg.generated Thu Oct 24 19:58:41 KST 2019
     */
    public String getsContent() {
        return sContent;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column V_BOARD.S_CONTENT
     *
     * @param sContent the value for V_BOARD.S_CONTENT
     *
     * @mbg.generated Thu Oct 24 19:58:41 KST 2019
     */
    public void setsContent(String sContent) {
        this.sContent = sContent;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column V_BOARD.S_REG_EMP_ID
     *
     * @return the value of V_BOARD.S_REG_EMP_ID
     *
     * @mbg.generated Thu Oct 24 19:58:41 KST 2019
     */
    public String getsRegEmpId() {
        return sRegEmpId;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column V_BOARD.S_REG_EMP_ID
     *
     * @param sRegEmpId the value for V_BOARD.S_REG_EMP_ID
     *
     * @mbg.generated Thu Oct 24 19:58:41 KST 2019
     */
    public void setsRegEmpId(String sRegEmpId) {
        this.sRegEmpId = sRegEmpId;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column V_BOARD.S_EMP_NM
     *
     * @return the value of V_BOARD.S_EMP_NM
     *
     * @mbg.generated Thu Oct 24 19:58:41 KST 2019
     */
    public String getsEmpNm() {
        return sEmpNm;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column V_BOARD.S_EMP_NM
     *
     * @param sEmpNm the value for V_BOARD.S_EMP_NM
     *
     * @mbg.generated Thu Oct 24 19:58:41 KST 2019
     */
    public void setsEmpNm(String sEmpNm) {
        this.sEmpNm = sEmpNm;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column V_BOARD.D_REG_DATE
     *
     * @return the value of V_BOARD.D_REG_DATE
     *
     * @mbg.generated Thu Oct 24 19:58:41 KST 2019
     */
    public Date getdRegDate() {
        return dRegDate;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column V_BOARD.D_REG_DATE
     *
     * @param dRegDate the value for V_BOARD.D_REG_DATE
     *
     * @mbg.generated Thu Oct 24 19:58:41 KST 2019
     */
    public void setdRegDate(Date dRegDate) {
        this.dRegDate = dRegDate;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column V_BOARD.G_FILE_ID
     *
     * @return the value of V_BOARD.G_FILE_ID
     *
     * @mbg.generated Thu Oct 24 19:58:41 KST 2019
     */
    public String getgFileId() {
        return gFileId;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column V_BOARD.G_FILE_ID
     *
     * @param gFileId the value for V_BOARD.G_FILE_ID
     *
     * @mbg.generated Thu Oct 24 19:58:41 KST 2019
     */
    public void setgFileId(String gFileId) {
        this.gFileId = gFileId;
    }
}