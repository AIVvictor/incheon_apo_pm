package com.sebang.ems.model.Dto;

import lombok.Setter;
import lombok.ToString;

@ToString
@Setter
public class ResSunPower {

    private String cYear;

    private Integer amt;

    public String getcYear() {
        return cYear;
    }

    public Integer getAmt() {
        return amt;
    }

}