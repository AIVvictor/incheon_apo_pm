package com.sebang.ems.model;

import java.math.BigDecimal;

public class EngPointAmt {

    private Long idEngPointAmt;

    private String cYear;

    private String cMon;

    private Long idSite;

    private String sSiteNm;

    private Long idErpDept;

    private String sDeptNm;

    private String sEmtCd;
    
    private String sEmtCdNm;
    
    private Long idEngPoint;

    
    private String sPointId;

    
    private String sPointNm;

   
    private String sEngCd;

   
    private String sEngCdNm;

  
    private String sInUntCdNm;

    
    private Float nEngEtc;

  
    private String nEngEtcUntCd;

    
    private String nEngEtcUntCdNm;

    private BigDecimal nUseAmt;
   
    private BigDecimal nCost;
  
    private String gBillId;
    
    private Long idFile;
   
    private String sFileNm;

    private BigDecimal nGj;

    private BigDecimal nTco2;
    
    private BigDecimal sBaseVal;
    
    private BigDecimal nToe;

    private String sInvCd;

    public Long getIdEngPointAmt() {
        return idEngPointAmt;
    }

    
    public void setIdEngPointAmt(Long idEngPointAmt) {
        this.idEngPointAmt = idEngPointAmt;
    }

    
    public String getcYear() {
        return cYear;
    }

   
    public void setcYear(String cYear) {
        this.cYear = cYear;
    }

   
    public String getcMon() {
        return cMon;
    }

   
    public void setcMon(String cMon) {
        this.cMon = cMon;
    }

 
    public Long getIdSite() {
        return idSite;
    }

  
    public void setIdSite(Long idSite) {
        this.idSite = idSite;
    }

    
    public String getsSiteNm() {
        return sSiteNm;
    }

  
    public void setsSiteNm(String sSiteNm) {
        this.sSiteNm = sSiteNm;
    }

   
    public Long getIdErpDept() {
        return idErpDept;
    }

  
    public void setIdErpDept(Long idErpDept) {
        this.idErpDept = idErpDept;
    }

    
    public String getsDeptNm() {
        return sDeptNm;
    }

    
    public void setsDeptNm(String sDeptNm) {
        this.sDeptNm = sDeptNm;
    }


    public String getsEmtCd() {
        return sEmtCd;
    }

    
    public void setsEmtCd(String sEmtCd) {
        this.sEmtCd = sEmtCd;
    }

  
    public String getsEmtCdNm() {
        return sEmtCdNm;
    }

    
    public void setsEmtCdNm(String sEmtCdNm) {
        this.sEmtCdNm = sEmtCdNm;
    }

    public Long getIdEngPoint() {
        return idEngPoint;
    }

    
    public void setIdEngPoint(Long idEngPoint) {
        this.idEngPoint = idEngPoint;
    }

    
    public String getsPointId() {
        return sPointId;
    }

  
    public void setsPointId(String sPointId) {
        this.sPointId = sPointId;
    }

   
    public String getsPointNm() {
        return sPointNm;
    }

  
    public void setsPointNm(String sPointNm) {
        this.sPointNm = sPointNm;
    }

    
    public String getsEngCd() {
        return sEngCd;
    }

    
    public void setsEngCd(String sEngCd) {
        this.sEngCd = sEngCd;
    }

   
    public String getsEngCdNm() {
        return sEngCdNm;
    }

    
    public void setsEngCdNm(String sEngCdNm) {
        this.sEngCdNm = sEngCdNm;
    }

  
    public String getsInUntCdNm() {
        return sInUntCdNm;
    }

    public void setsInUntCdNm(String sInUntCdNm) {
        this.sInUntCdNm = sInUntCdNm;
    }

    
    public Float getnEngEtc() {
        return nEngEtc;
    }

  
    public void setnEngEtc(Float nEngEtc) {
        this.nEngEtc = nEngEtc;
    }

    
    public String getnEngEtcUntCd() {
        return nEngEtcUntCd;
    }

    
    public void setnEngEtcUntCd(String nEngEtcUntCd) {
        this.nEngEtcUntCd = nEngEtcUntCd;
    }

  
    public String getnEngEtcUntCdNm() {
        return nEngEtcUntCdNm;
    }

    
    public void setnEngEtcUntCdNm(String nEngEtcUntCdNm) {
        this.nEngEtcUntCdNm = nEngEtcUntCdNm;
    }

  
    public BigDecimal getnUseAmt() {
        return nUseAmt;
    }

   
    public void setnUseAmt(BigDecimal nUseAmt) {
        this.nUseAmt = nUseAmt;
    }

  
    public BigDecimal getnCost() {
        return nCost;
    }

  
    public void setnCost(BigDecimal nCost) {
        this.nCost = nCost;
    }

    
    public String getgBillId() {
        return gBillId;
    }

   
    public void setgBillId(String gBillId) {
        this.gBillId = gBillId;
    }

  
    public Long getIdFile() {
        return idFile;
    }

  
    public void setIdFile(Long idFile) {
        this.idFile = idFile;
    }

    
    public String getsFileNm() {
        return sFileNm;
    }

   
    public void setsFileNm(String sFileNm) {
        this.sFileNm = sFileNm;
    }

    public BigDecimal getnGj() {
        return nGj;
    }

    public void setnGj(BigDecimal nGj) {
        this.nGj = nGj;
    }

    public BigDecimal getnTco2() {
        return nTco2;
    }

    public void setnTco2(BigDecimal nTco2) {
        this.nTco2 = nTco2;
    }

    public BigDecimal getsBaseVal() {
        return sBaseVal;
    }

    public void setsBaseVal(BigDecimal sBaseVal) {
        this.sBaseVal = sBaseVal;
    }

    public BigDecimal getnToe() {
        return nToe;
    }

    public void setnToe(BigDecimal nToe) {
        this.nToe = nToe;
    }

	public String getsInvCd() {
		return sInvCd;
	}

	public void setsInvCd(String sInvCd) {
		this.sInvCd = sInvCd;
	}
    
    
}