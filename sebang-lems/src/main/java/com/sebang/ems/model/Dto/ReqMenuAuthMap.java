package com.sebang.ems.model.Dto;

import javax.validation.constraints.NotNull;

import com.sebang.ems.model.Do.TAuthMenu;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@ToString
@Setter
@NoArgsConstructor
@AllArgsConstructor(access=AccessLevel.PRIVATE)
@Builder
public class ReqMenuAuthMap
{  
    private Long idAuthMenu;

    @NotNull(message = "sAuthCd 는 필수 입력 값입니다. ")
    private String sAuthCd;


    @NotNull(message = "idMenuInfo 는 필수 입력 값입니다. ")
    private Long idMenuInfo;

    
    public Long getIdAuthMenu() {
        return idAuthMenu;
    }

    public Long getIdMenuInfo() {
        return idMenuInfo;
    }

    public TAuthMenu toEntity()
    {
        TAuthMenu m = new TAuthMenu();
        m.setIdAuthMenu(this.idAuthMenu);
        m.setIdMenuInfo(this.idMenuInfo);
        m.setsAuthCd(this.sAuthCd);
        return m;
    }

    public String getsAuthCd() {
        return sAuthCd;
    }

    public void setsAuthCd(String sAuthCd) {
        this.sAuthCd = sAuthCd;
    }
}