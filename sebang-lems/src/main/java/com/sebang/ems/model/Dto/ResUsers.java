package com.sebang.ems.model.Dto;

import javax.validation.constraints.NotNull;

import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Setter
@ToString
@RequiredArgsConstructor
public class ResUsers
{
    @NonNull
    String sEmpId;

    @NotNull
    String sEmpNm;

    String sRankNm;

    @NonNull
    String sAuthType;

    @NonNull
    String jwtHeader;

    public String getsEmpId()
    {
        return sEmpId;
    }

    public String getsAuthType()
    {
        return sAuthType;
    }

    public String getJwtHeader()
    {
        return jwtHeader;
    }

    public String getsEmpNm() {
        return sEmpNm;
    }

    public void setsEmpNm(String sEmpNm) {
        this.sEmpNm = sEmpNm;
    }

    public String getsRankNm(){
        return this.sRankNm;
    }
}