package com.sebang.ems.model.Dto;

import javax.validation.constraints.NotNull;

import com.sebang.ems.model.Do.TSunPower;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Setter
@ToString
@Builder
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@NoArgsConstructor
public class ReqSunPower
{
    private Long idSunPower;
    @NotNull(message = "nPower 는 필수 입력 값입니다. ")
    private Integer nPower;
    
    @NotNull(message="cYear 는 필수 입력 값입니다.")
    private String cYear;

    @NotNull(message="cYear 는 필수 입력 값입니다.")
    private String cMon;

    private String nPoint;

    private String cDelYn;
    
    private String sRegId;

    private String sModId;

    public Long getIdSunPower() {
        return idSunPower;
    }

    public Integer getnPower() {
        return nPower;
    }

    public void setnPower(Integer nPower) {
        this.nPower = nPower;
    }

    public String getcYear() {
        return cYear;
    }

    public void setcYear(String cYear) {
        this.cYear = cYear;
    }

    public String getcMon() {
        return cMon;
    }

    public void setcMon(String cMon) {
        this.cMon = cMon;
    }

    public String getnPoint() {
        return nPoint;
    }

    public void setnPoint(String nPoint) {
        this.nPoint = nPoint;
    }

    public String getcDelYn() {
        return cDelYn;
    }

    public void setcDelYn(String cDelYn) {
        this.cDelYn = cDelYn;
    }

    public String getsRegId() {
        return sRegId;
    }

    public void setsRegId(String sRegId) {
        this.sRegId = sRegId;
    }

    public String getsModId() {
        return sModId;
    }

    public void setsModId(String sModId) {
        this.sModId = sModId;
    }

    public TSunPower toEntity()
    {
        TSunPower s = new TSunPower();

        s.setIdSunPower(this.idSunPower);
        s.setnPower(this.nPower);
        s.setcYear(this.cYear);
        s.setcMon(this.cMon);
        s.setnPoint(this.nPoint);
        s.setcDelYn(this.cDelYn);
        s.setsRegId(this.sRegId);
        s.setsModId(this.sModId);
        return s;
    }

    
}