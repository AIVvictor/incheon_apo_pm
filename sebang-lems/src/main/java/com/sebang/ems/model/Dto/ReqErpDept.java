package com.sebang.ems.model.Dto;

import com.opencsv.bean.CsvBindByPosition;
import com.sebang.ems.model.Do.TRawErpDept;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Setter
@Getter
@ToString
public class ReqErpDept
{
    @CsvBindByPosition(position = 0)
    String sDeptCd;

    @CsvBindByPosition(position = 1)
    String sDeptNm;

    @CsvBindByPosition(position = 2)
    String sUDeptCd;

    String cDelYn="N";

    public TRawErpDept toEntity()
    {
        TRawErpDept r = new TRawErpDept();
        r.setsDeptId(this.sDeptCd);
        r.setsDeptNm(this.sDeptNm);
        r.setsUDeptId(this.sUDeptCd);
        r.setcDelYn("N");
        return r;
    }

    public boolean isEqual(TRawErpDept r)
    {
        boolean ret= true;

        if( sDeptCd != null)
        {
            if(!sDeptCd.equals(r.getsDeptId()))
                return false;        
        }else
        {
            if(r.getsDeptId() != null)
                return false;
        }

        if( sDeptNm != null)
        {
            if(!sDeptNm.equals(r.getsDeptNm()))
                return false;        
        }else{
            if(r.getsDeptNm() != null)
            return false;
        }

        if( cDelYn != null)
        {
            if(!cDelYn.equals(r.getcDelYn()))
                return false;        
        }else{
            if(r.getcDelYn() != null)
            return false;
        }
            
        return ret;
    }
}