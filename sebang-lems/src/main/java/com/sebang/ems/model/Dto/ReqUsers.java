package com.sebang.ems.model.Dto;

import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Setter
@ToString
@RequiredArgsConstructor
public class ReqUsers
{
    @NonNull
    String sEmpId;

    @NonNull
    String sAuthType;

    public String getsEmpId()
    {
        return sEmpId;
    }

    public String getsAuthType()
    {
        return sAuthType;
    }
}