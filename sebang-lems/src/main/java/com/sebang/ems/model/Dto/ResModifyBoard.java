/**
 * 
 */
package com.sebang.ems.model.Dto;

import lombok.Setter;
import lombok.ToString;

/**
 * 게시판 등록/수정/삭제 응답 dto
 * @author promaker
 *
 */
//@Getter
@Setter
@ToString
//@NoArgsConstructor
//@AllArgsConstructor(access=AccessLevel.PRIVATE)
//@JsonInclude(value = Include.NON_EMPTY)
//@Builder
public class ResModifyBoard {
	/**
	 * 게시물 Id
	 */
	//@JsonProperty("sBoardId")
	public String sBoardId;
	/**
	 * 게시판 종류
	 */
	//@JsonProperty("sBoardType")
	public String sBoardType;
	
}
