package com.sebang.ems.model.Do;

import java.util.Date;

public class TLogisPrj {
    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column T_LOGIS_PRJ.ID_LOGIS_PRJ
     *
     * @mbg.generated Wed Oct 02 20:32:48 KST 2019
     */
    private Long idLogisPrj;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column T_LOGIS_PRJ.S_PRJ_NM
     *
     * @mbg.generated Wed Oct 02 20:32:48 KST 2019
     */
    private String sPrjNm;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column T_LOGIS_PRJ.S_LOGI_PTY_CD
     *
     * @mbg.generated Wed Oct 02 20:32:48 KST 2019
     */
    private String sLogiPtyCd;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column T_LOGIS_PRJ.S_PRJ_COMM
     *
     * @mbg.generated Wed Oct 02 20:32:48 KST 2019
     */
    private String sPrjComm;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column T_LOGIS_PRJ.S_LOGI_MEA_CD
     *
     * @mbg.generated Wed Oct 02 20:32:48 KST 2019
     */
    private String sLogiMeaCd;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column T_LOGIS_PRJ.S_LOGI_UNT_CD
     *
     * @mbg.generated Wed Oct 02 20:32:48 KST 2019
     */
    private String sLogiUntCd;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column T_LOGIS_PRJ.S_LOGI_TAR_CD
     *
     * @mbg.generated Wed Oct 02 20:32:48 KST 2019
     */
    private String sLogiTarCd;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column T_LOGIS_PRJ.S_DEPT
     *
     * @mbg.generated Wed Oct 02 20:32:48 KST 2019
     */
    private String sDept;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column T_LOGIS_PRJ.S_PIC
     *
     * @mbg.generated Wed Oct 02 20:32:48 KST 2019
     */
    private String sPic;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column T_LOGIS_PRJ.C_YEAR
     *
     * @mbg.generated Wed Oct 02 20:32:48 KST 2019
     */
    private String cYear;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column T_LOGIS_PRJ.C_S_MON
     *
     * @mbg.generated Wed Oct 02 20:32:48 KST 2019
     */
    private String cSMon;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column T_LOGIS_PRJ.C_E_MON
     *
     * @mbg.generated Wed Oct 02 20:32:48 KST 2019
     */
    private String cEMon;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column T_LOGIS_PRJ.S_LOGI_MONEY
     *
     * @mbg.generated Wed Oct 02 20:32:48 KST 2019
     */
    private Float sLogiMoney;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column T_LOGIS_PRJ.S_PRJ_EFF
     *
     * @mbg.generated Wed Oct 02 20:32:48 KST 2019
     */
    private String sPrjEff;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column T_LOGIS_PRJ.G_LOGIS_PRJ_FILE
     *
     * @mbg.generated Wed Oct 02 20:32:48 KST 2019
     */
    private String gLogisPrjFile;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column T_LOGIS_PRJ.S_FUEL_EFF
     *
     * @mbg.generated Wed Oct 02 20:32:48 KST 2019
     */
    private Float sFuelEff;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column T_LOGIS_PRJ.S_DISTANCE
     *
     * @mbg.generated Wed Oct 02 20:32:48 KST 2019
     */
    private Float sDistance;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column T_LOGIS_PRJ.S_ENG_VOL
     *
     * @mbg.generated Wed Oct 02 20:32:48 KST 2019
     */
    private Float sEngVol;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column T_LOGIS_PRJ.S_MOVE_CON
     *
     * @mbg.generated Wed Oct 02 20:32:48 KST 2019
     */
    private Float sMoveCon;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column T_LOGIS_PRJ.S_ENG_IMP
     *
     * @mbg.generated Wed Oct 02 20:32:48 KST 2019
     */
    private Float sEngImp;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column T_LOGIS_PRJ.S_DAY_RED
     *
     * @mbg.generated Wed Oct 02 20:32:48 KST 2019
     */
    private Float sDayRed;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column T_LOGIS_PRJ.S_YEAR_RED
     *
     * @mbg.generated Wed Oct 02 20:32:48 KST 2019
     */
    private Float sYearRed;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column T_LOGIS_PRJ.S_ENG_AMT
     *
     * @mbg.generated Wed Oct 02 20:32:48 KST 2019
     */
    private Float sEngAmt;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column T_LOGIS_PRJ.S_RED_AMT
     *
     * @mbg.generated Wed Oct 02 20:32:48 KST 2019
     */
    private Float sRedAmt;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column T_LOGIS_PRJ.S_GAS_RED
     *
     * @mbg.generated Wed Oct 02 20:32:48 KST 2019
     */
    private Float sGasRed;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column T_LOGIS_PRJ.S_REG_ID
     *
     * @mbg.generated Wed Oct 02 20:32:48 KST 2019
     */
    private String sRegId;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column T_LOGIS_PRJ.D_REG_DATE
     *
     * @mbg.generated Wed Oct 02 20:32:48 KST 2019
     */
    private Date dRegDate;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column T_LOGIS_PRJ.S_MOD_ID
     *
     * @mbg.generated Wed Oct 02 20:32:48 KST 2019
     */
    private String sModId;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column T_LOGIS_PRJ.D_MOD_DATE
     *
     * @mbg.generated Wed Oct 02 20:32:48 KST 2019
     */
    private Date dModDate;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column T_LOGIS_PRJ.C_DEL_YN
     *
     * @mbg.generated Wed Oct 02 20:32:48 KST 2019
     */
    private String cDelYn;

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column T_LOGIS_PRJ.ID_LOGIS_PRJ
     *
     * @return the value of T_LOGIS_PRJ.ID_LOGIS_PRJ
     *
     * @mbg.generated Wed Oct 02 20:32:48 KST 2019
     */
    public Long getIdLogisPrj() {
        return idLogisPrj;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column T_LOGIS_PRJ.ID_LOGIS_PRJ
     *
     * @param idLogisPrj the value for T_LOGIS_PRJ.ID_LOGIS_PRJ
     *
     * @mbg.generated Wed Oct 02 20:32:48 KST 2019
     */
    public void setIdLogisPrj(Long idLogisPrj) {
        this.idLogisPrj = idLogisPrj;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column T_LOGIS_PRJ.S_PRJ_NM
     *
     * @return the value of T_LOGIS_PRJ.S_PRJ_NM
     *
     * @mbg.generated Wed Oct 02 20:32:48 KST 2019
     */
    public String getsPrjNm() {
        return sPrjNm;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column T_LOGIS_PRJ.S_PRJ_NM
     *
     * @param sPrjNm the value for T_LOGIS_PRJ.S_PRJ_NM
     *
     * @mbg.generated Wed Oct 02 20:32:48 KST 2019
     */
    public void setsPrjNm(String sPrjNm) {
        this.sPrjNm = sPrjNm;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column T_LOGIS_PRJ.S_LOGI_PTY_CD
     *
     * @return the value of T_LOGIS_PRJ.S_LOGI_PTY_CD
     *
     * @mbg.generated Wed Oct 02 20:32:48 KST 2019
     */
    public String getsLogiPtyCd() {
        return sLogiPtyCd;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column T_LOGIS_PRJ.S_LOGI_PTY_CD
     *
     * @param sLogiPtyCd the value for T_LOGIS_PRJ.S_LOGI_PTY_CD
     *
     * @mbg.generated Wed Oct 02 20:32:48 KST 2019
     */
    public void setsLogiPtyCd(String sLogiPtyCd) {
        this.sLogiPtyCd = sLogiPtyCd;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column T_LOGIS_PRJ.S_PRJ_COMM
     *
     * @return the value of T_LOGIS_PRJ.S_PRJ_COMM
     *
     * @mbg.generated Wed Oct 02 20:32:48 KST 2019
     */
    public String getsPrjComm() {
        return sPrjComm;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column T_LOGIS_PRJ.S_PRJ_COMM
     *
     * @param sPrjComm the value for T_LOGIS_PRJ.S_PRJ_COMM
     *
     * @mbg.generated Wed Oct 02 20:32:48 KST 2019
     */
    public void setsPrjComm(String sPrjComm) {
        this.sPrjComm = sPrjComm;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column T_LOGIS_PRJ.S_LOGI_MEA_CD
     *
     * @return the value of T_LOGIS_PRJ.S_LOGI_MEA_CD
     *
     * @mbg.generated Wed Oct 02 20:32:48 KST 2019
     */
    public String getsLogiMeaCd() {
        return sLogiMeaCd;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column T_LOGIS_PRJ.S_LOGI_MEA_CD
     *
     * @param sLogiMeaCd the value for T_LOGIS_PRJ.S_LOGI_MEA_CD
     *
     * @mbg.generated Wed Oct 02 20:32:48 KST 2019
     */
    public void setsLogiMeaCd(String sLogiMeaCd) {
        this.sLogiMeaCd = sLogiMeaCd;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column T_LOGIS_PRJ.S_LOGI_UNT_CD
     *
     * @return the value of T_LOGIS_PRJ.S_LOGI_UNT_CD
     *
     * @mbg.generated Wed Oct 02 20:32:48 KST 2019
     */
    public String getsLogiUntCd() {
        return sLogiUntCd;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column T_LOGIS_PRJ.S_LOGI_UNT_CD
     *
     * @param sLogiUntCd the value for T_LOGIS_PRJ.S_LOGI_UNT_CD
     *
     * @mbg.generated Wed Oct 02 20:32:48 KST 2019
     */
    public void setsLogiUntCd(String sLogiUntCd) {
        this.sLogiUntCd = sLogiUntCd;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column T_LOGIS_PRJ.S_LOGI_TAR_CD
     *
     * @return the value of T_LOGIS_PRJ.S_LOGI_TAR_CD
     *
     * @mbg.generated Wed Oct 02 20:32:48 KST 2019
     */
    public String getsLogiTarCd() {
        return sLogiTarCd;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column T_LOGIS_PRJ.S_LOGI_TAR_CD
     *
     * @param sLogiTarCd the value for T_LOGIS_PRJ.S_LOGI_TAR_CD
     *
     * @mbg.generated Wed Oct 02 20:32:48 KST 2019
     */
    public void setsLogiTarCd(String sLogiTarCd) {
        this.sLogiTarCd = sLogiTarCd;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column T_LOGIS_PRJ.S_DEPT
     *
     * @return the value of T_LOGIS_PRJ.S_DEPT
     *
     * @mbg.generated Wed Oct 02 20:32:48 KST 2019
     */
    public String getsDept() {
        return sDept;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column T_LOGIS_PRJ.S_DEPT
     *
     * @param sDept the value for T_LOGIS_PRJ.S_DEPT
     *
     * @mbg.generated Wed Oct 02 20:32:48 KST 2019
     */
    public void setsDept(String sDept) {
        this.sDept = sDept;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column T_LOGIS_PRJ.S_PIC
     *
     * @return the value of T_LOGIS_PRJ.S_PIC
     *
     * @mbg.generated Wed Oct 02 20:32:48 KST 2019
     */
    public String getsPic() {
        return sPic;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column T_LOGIS_PRJ.S_PIC
     *
     * @param sPic the value for T_LOGIS_PRJ.S_PIC
     *
     * @mbg.generated Wed Oct 02 20:32:48 KST 2019
     */
    public void setsPic(String sPic) {
        this.sPic = sPic;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column T_LOGIS_PRJ.C_YEAR
     *
     * @return the value of T_LOGIS_PRJ.C_YEAR
     *
     * @mbg.generated Wed Oct 02 20:32:48 KST 2019
     */
    public String getcYear() {
        return cYear;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column T_LOGIS_PRJ.C_YEAR
     *
     * @param cYear the value for T_LOGIS_PRJ.C_YEAR
     *
     * @mbg.generated Wed Oct 02 20:32:48 KST 2019
     */
    public void setcYear(String cYear) {
        this.cYear = cYear;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column T_LOGIS_PRJ.C_S_MON
     *
     * @return the value of T_LOGIS_PRJ.C_S_MON
     *
     * @mbg.generated Wed Oct 02 20:32:48 KST 2019
     */
    public String getcSMon() {
        return cSMon;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column T_LOGIS_PRJ.C_S_MON
     *
     * @param cSMon the value for T_LOGIS_PRJ.C_S_MON
     *
     * @mbg.generated Wed Oct 02 20:32:48 KST 2019
     */
    public void setcSMon(String cSMon) {
        this.cSMon = cSMon;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column T_LOGIS_PRJ.C_E_MON
     *
     * @return the value of T_LOGIS_PRJ.C_E_MON
     *
     * @mbg.generated Wed Oct 02 20:32:48 KST 2019
     */
    public String getcEMon() {
        return cEMon;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column T_LOGIS_PRJ.C_E_MON
     *
     * @param cEMon the value for T_LOGIS_PRJ.C_E_MON
     *
     * @mbg.generated Wed Oct 02 20:32:48 KST 2019
     */
    public void setcEMon(String cEMon) {
        this.cEMon = cEMon;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column T_LOGIS_PRJ.S_LOGI_MONEY
     *
     * @return the value of T_LOGIS_PRJ.S_LOGI_MONEY
     *
     * @mbg.generated Wed Oct 02 20:32:48 KST 2019
     */
    public Float getsLogiMoney() {
        return sLogiMoney;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column T_LOGIS_PRJ.S_LOGI_MONEY
     *
     * @param sLogiMoney the value for T_LOGIS_PRJ.S_LOGI_MONEY
     *
     * @mbg.generated Wed Oct 02 20:32:48 KST 2019
     */
    public void setsLogiMoney(Float sLogiMoney) {
        this.sLogiMoney = sLogiMoney;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column T_LOGIS_PRJ.S_PRJ_EFF
     *
     * @return the value of T_LOGIS_PRJ.S_PRJ_EFF
     *
     * @mbg.generated Wed Oct 02 20:32:48 KST 2019
     */
    public String getsPrjEff() {
        return sPrjEff;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column T_LOGIS_PRJ.S_PRJ_EFF
     *
     * @param sPrjEff the value for T_LOGIS_PRJ.S_PRJ_EFF
     *
     * @mbg.generated Wed Oct 02 20:32:48 KST 2019
     */
    public void setsPrjEff(String sPrjEff) {
        this.sPrjEff = sPrjEff;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column T_LOGIS_PRJ.G_LOGIS_PRJ_FILE
     *
     * @return the value of T_LOGIS_PRJ.G_LOGIS_PRJ_FILE
     *
     * @mbg.generated Wed Oct 02 20:32:48 KST 2019
     */
    public String getgLogisPrjFile() {
        return gLogisPrjFile;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column T_LOGIS_PRJ.G_LOGIS_PRJ_FILE
     *
     * @param gLogisPrjFile the value for T_LOGIS_PRJ.G_LOGIS_PRJ_FILE
     *
     * @mbg.generated Wed Oct 02 20:32:48 KST 2019
     */
    public void setgLogisPrjFile(String gLogisPrjFile) {
        this.gLogisPrjFile = gLogisPrjFile;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column T_LOGIS_PRJ.S_FUEL_EFF
     *
     * @return the value of T_LOGIS_PRJ.S_FUEL_EFF
     *
     * @mbg.generated Wed Oct 02 20:32:48 KST 2019
     */
    public Float getsFuelEff() {
        return sFuelEff;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column T_LOGIS_PRJ.S_FUEL_EFF
     *
     * @param sFuelEff the value for T_LOGIS_PRJ.S_FUEL_EFF
     *
     * @mbg.generated Wed Oct 02 20:32:48 KST 2019
     */
    public void setsFuelEff(Float sFuelEff) {
        this.sFuelEff = sFuelEff;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column T_LOGIS_PRJ.S_DISTANCE
     *
     * @return the value of T_LOGIS_PRJ.S_DISTANCE
     *
     * @mbg.generated Wed Oct 02 20:32:48 KST 2019
     */
    public Float getsDistance() {
        return sDistance;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column T_LOGIS_PRJ.S_DISTANCE
     *
     * @param sDistance the value for T_LOGIS_PRJ.S_DISTANCE
     *
     * @mbg.generated Wed Oct 02 20:32:48 KST 2019
     */
    public void setsDistance(Float sDistance) {
        this.sDistance = sDistance;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column T_LOGIS_PRJ.S_ENG_VOL
     *
     * @return the value of T_LOGIS_PRJ.S_ENG_VOL
     *
     * @mbg.generated Wed Oct 02 20:32:48 KST 2019
     */
    public Float getsEngVol() {
        return sEngVol;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column T_LOGIS_PRJ.S_ENG_VOL
     *
     * @param sEngVol the value for T_LOGIS_PRJ.S_ENG_VOL
     *
     * @mbg.generated Wed Oct 02 20:32:48 KST 2019
     */
    public void setsEngVol(Float sEngVol) {
        this.sEngVol = sEngVol;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column T_LOGIS_PRJ.S_MOVE_CON
     *
     * @return the value of T_LOGIS_PRJ.S_MOVE_CON
     *
     * @mbg.generated Wed Oct 02 20:32:48 KST 2019
     */
    public Float getsMoveCon() {
        return sMoveCon;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column T_LOGIS_PRJ.S_MOVE_CON
     *
     * @param sMoveCon the value for T_LOGIS_PRJ.S_MOVE_CON
     *
     * @mbg.generated Wed Oct 02 20:32:48 KST 2019
     */
    public void setsMoveCon(Float sMoveCon) {
        this.sMoveCon = sMoveCon;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column T_LOGIS_PRJ.S_ENG_IMP
     *
     * @return the value of T_LOGIS_PRJ.S_ENG_IMP
     *
     * @mbg.generated Wed Oct 02 20:32:48 KST 2019
     */
    public Float getsEngImp() {
        return sEngImp;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column T_LOGIS_PRJ.S_ENG_IMP
     *
     * @param sEngImp the value for T_LOGIS_PRJ.S_ENG_IMP
     *
     * @mbg.generated Wed Oct 02 20:32:48 KST 2019
     */
    public void setsEngImp(Float sEngImp) {
        this.sEngImp = sEngImp;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column T_LOGIS_PRJ.S_DAY_RED
     *
     * @return the value of T_LOGIS_PRJ.S_DAY_RED
     *
     * @mbg.generated Wed Oct 02 20:32:48 KST 2019
     */
    public Float getsDayRed() {
        return sDayRed;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column T_LOGIS_PRJ.S_DAY_RED
     *
     * @param sDayRed the value for T_LOGIS_PRJ.S_DAY_RED
     *
     * @mbg.generated Wed Oct 02 20:32:48 KST 2019
     */
    public void setsDayRed(Float sDayRed) {
        this.sDayRed = sDayRed;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column T_LOGIS_PRJ.S_YEAR_RED
     *
     * @return the value of T_LOGIS_PRJ.S_YEAR_RED
     *
     * @mbg.generated Wed Oct 02 20:32:48 KST 2019
     */
    public Float getsYearRed() {
        return sYearRed;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column T_LOGIS_PRJ.S_YEAR_RED
     *
     * @param sYearRed the value for T_LOGIS_PRJ.S_YEAR_RED
     *
     * @mbg.generated Wed Oct 02 20:32:48 KST 2019
     */
    public void setsYearRed(Float sYearRed) {
        this.sYearRed = sYearRed;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column T_LOGIS_PRJ.S_ENG_AMT
     *
     * @return the value of T_LOGIS_PRJ.S_ENG_AMT
     *
     * @mbg.generated Wed Oct 02 20:32:48 KST 2019
     */
    public Float getsEngAmt() {
        return sEngAmt;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column T_LOGIS_PRJ.S_ENG_AMT
     *
     * @param sEngAmt the value for T_LOGIS_PRJ.S_ENG_AMT
     *
     * @mbg.generated Wed Oct 02 20:32:48 KST 2019
     */
    public void setsEngAmt(Float sEngAmt) {
        this.sEngAmt = sEngAmt;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column T_LOGIS_PRJ.S_RED_AMT
     *
     * @return the value of T_LOGIS_PRJ.S_RED_AMT
     *
     * @mbg.generated Wed Oct 02 20:32:48 KST 2019
     */
    public Float getsRedAmt() {
        return sRedAmt;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column T_LOGIS_PRJ.S_RED_AMT
     *
     * @param sRedAmt the value for T_LOGIS_PRJ.S_RED_AMT
     *
     * @mbg.generated Wed Oct 02 20:32:48 KST 2019
     */
    public void setsRedAmt(Float sRedAmt) {
        this.sRedAmt = sRedAmt;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column T_LOGIS_PRJ.S_GAS_RED
     *
     * @return the value of T_LOGIS_PRJ.S_GAS_RED
     *
     * @mbg.generated Wed Oct 02 20:32:48 KST 2019
     */
    public Float getsGasRed() {
        return sGasRed;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column T_LOGIS_PRJ.S_GAS_RED
     *
     * @param sGasRed the value for T_LOGIS_PRJ.S_GAS_RED
     *
     * @mbg.generated Wed Oct 02 20:32:48 KST 2019
     */
    public void setsGasRed(Float sGasRed) {
        this.sGasRed = sGasRed;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column T_LOGIS_PRJ.S_REG_ID
     *
     * @return the value of T_LOGIS_PRJ.S_REG_ID
     *
     * @mbg.generated Wed Oct 02 20:32:48 KST 2019
     */
    public String getsRegId() {
        return sRegId;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column T_LOGIS_PRJ.S_REG_ID
     *
     * @param sRegId the value for T_LOGIS_PRJ.S_REG_ID
     *
     * @mbg.generated Wed Oct 02 20:32:48 KST 2019
     */
    public void setsRegId(String sRegId) {
        this.sRegId = sRegId;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column T_LOGIS_PRJ.D_REG_DATE
     *
     * @return the value of T_LOGIS_PRJ.D_REG_DATE
     *
     * @mbg.generated Wed Oct 02 20:32:48 KST 2019
     */
    public Date getdRegDate() {
        return dRegDate;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column T_LOGIS_PRJ.D_REG_DATE
     *
     * @param dRegDate the value for T_LOGIS_PRJ.D_REG_DATE
     *
     * @mbg.generated Wed Oct 02 20:32:48 KST 2019
     */
    public void setdRegDate(Date dRegDate) {
        this.dRegDate = dRegDate;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column T_LOGIS_PRJ.S_MOD_ID
     *
     * @return the value of T_LOGIS_PRJ.S_MOD_ID
     *
     * @mbg.generated Wed Oct 02 20:32:48 KST 2019
     */
    public String getsModId() {
        return sModId;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column T_LOGIS_PRJ.S_MOD_ID
     *
     * @param sModId the value for T_LOGIS_PRJ.S_MOD_ID
     *
     * @mbg.generated Wed Oct 02 20:32:48 KST 2019
     */
    public void setsModId(String sModId) {
        this.sModId = sModId;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column T_LOGIS_PRJ.D_MOD_DATE
     *
     * @return the value of T_LOGIS_PRJ.D_MOD_DATE
     *
     * @mbg.generated Wed Oct 02 20:32:48 KST 2019
     */
    public Date getdModDate() {
        return dModDate;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column T_LOGIS_PRJ.D_MOD_DATE
     *
     * @param dModDate the value for T_LOGIS_PRJ.D_MOD_DATE
     *
     * @mbg.generated Wed Oct 02 20:32:48 KST 2019
     */
    public void setdModDate(Date dModDate) {
        this.dModDate = dModDate;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column T_LOGIS_PRJ.C_DEL_YN
     *
     * @return the value of T_LOGIS_PRJ.C_DEL_YN
     *
     * @mbg.generated Wed Oct 02 20:32:48 KST 2019
     */
    public String getcDelYn() {
        return cDelYn;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column T_LOGIS_PRJ.C_DEL_YN
     *
     * @param cDelYn the value for T_LOGIS_PRJ.C_DEL_YN
     *
     * @mbg.generated Wed Oct 02 20:32:48 KST 2019
     */
    public void setcDelYn(String cDelYn) {
        this.cDelYn = cDelYn;
    }
}