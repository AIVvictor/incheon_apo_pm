package com.sebang.ems.model;

import java.math.BigDecimal;

import javax.validation.constraints.NotNull;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@ToString
@Setter
@Getter
public class SumAmt
{
    @NotNull
    private String cYear;

    @NotNull
    private String cMon;

    private String sDesc;

    private BigDecimal gj;

    private BigDecimal co2;

    private BigDecimal ch4;

    private BigDecimal n2o;

    private BigDecimal tco2eq;

    private BigDecimal toe;
}