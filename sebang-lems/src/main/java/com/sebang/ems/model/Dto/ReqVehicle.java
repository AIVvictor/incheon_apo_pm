package com.sebang.ems.model.Dto;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import com.sebang.ems.model.Do.TVehicle;
import com.sebang.ems.util.DateUtil;
import com.sebang.ems.util.StringUtil;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;
import lombok.ToString;

@ToString
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@NoArgsConstructor
@Builder
public class ReqVehicle{
    
    private Long idVehicle;

    private Long idTransCom;

    @Size(min=6,max=10, message ="부서 ID (sErpId) 의 Length 는6 이상 10 이하입니다." )
    @NotNull(message ="부서 ID (sErpId) 은 필수 입력 값입니다. ")
    private String sErpId;
    private Long idErpDept;

    @Size(min=8,max=15, message ="차량 번호 (sVrn) 의 Length 는 10 이상 15 이하입니다." )
    @NotNull(message ="차량 번호 (sVrn) 은 필수 입력 값입니다. ")
    private String sVrn;
    
    private String sLineCd;

    @NotNull(message ="운수사 ID (sTransComId) 은 필수 입력 값입니다. ")
    private String sTransComId;
    
    @NotNull(message ="톤수 코드 (sLogiWeiCd) 은 필수 입력 값입니다. ")
    private String sLogiWeiCd;
    
    @NotNull(message ="차종 (sCarType) 은 필수 입력 값입니다. ")
    private String sCarType;
    
    private String sVin;
    
    private String sFuelTypeCd;

    private Float  nFuelEff;
    
    @Pattern(regexp = "(19|20)\\d{2}(0[1-9]|1[012])",message = "차량연식의 형식은 YYYYMM 입니다.")
    @NotNull(message = "차량 연식 (sCarMade) 는 필수 입력 값입니다. ")
    private String sCarMade;
    
    @Pattern(regexp = "[12]",message = "1: 업무용, 2: 운송용")
    @NotNull(message = "용도(sUseTypeCd) 는 필수 입력 값입니다. ")
    private String sUseTypeCd;
    
    @Pattern(regexp = "[12]",message = "1: 정상, 2: 삭제")
    @NotNull(message = "차량 상태(sCarStatusCd) 는 필수 입력 값입니다. ")
    private String sCarStatusCd;
    
    @NotNull(message = "제조사(sCarCompany) 는 필수 입력 값입니다. ")
    private String sCarCompany;
    
    @NotNull(message = "차량 모델(sCarModel) 는 필수 입력 값입니다. ")
    private String sCarModel;
    
    @Pattern(regexp = "(19|20)\\d{2}(0[1-9]|1[012])(0[1-9]|[12][0-9]|3[01])(0[0-9]|1[0-9]|2[0-3])([0-5][0-9])([0-5][0-9])",message = "날짜형식은 YYYYMMDDHHmiSS 입니다.")
    @NotNull(message = "등록일시(sRegDate) 는 필수 입력 값입니다. ")
    private String sRegDate;

    @Pattern(regexp = "(19|20)\\d{2}(0[1-9]|1[012])(0[1-9]|[12][0-9]|3[01])(0[0-9]|1[0-9]|2[0-3])([0-5][0-9])([0-5][0-9])",message = "날짜형식은 YYYYMMDDHHmiSS 입니다.")
    @NotNull(message = "수정일시(sModDate) 는 필수 입력 값입니다. ")
    private String sModDate;
    
    @Pattern(regexp = "(19|20)\\d{2}(0[1-9]|1[012])(0[1-9]|[12][0-9]|3[01])(0[0-9]|1[0-9]|2[0-3])([0-5][0-9])([0-5][0-9])",message = "날짜형식은 YYYYMMDDHHmiSS 입니다.")
    @NotNull(message = "대차일시 (sCarLoanDate )는 필수 입력 값입니다. ")
    private String  sCarLoanDate;
    
    private String sRegId;
    
    private String sModId;
    
    private String cDelYn;

    @Pattern(regexp = "[12]",message = "1: 정기, 2: 비정기")
    @NotNull(message = "노선 구분(sLogiYctCd) 는 필수 입력 값입니다. ")
    private String sLogiYctCd;

    private int index;

    @Pattern(regexp = "[12]",message = "1: 자차, 2: 용차")
    @NotNull(message = "자차 여부(sSelfvrnCd) 는 필수 입력 값입니다. ")
    private String sSelfvrnCd;

    public Long getIdVehicle() {
        return idVehicle;
    }

    public String getsVrn() {
        return sVrn;
    }

    public void setsVrn(String sVrn) {
        this.sVrn = sVrn;
    }

    // public String getsTransTypeCd() {
    //     return sTransTypeCd;
    // }

    // public void setsTransTypeCd(String sTransTypeCd) {
    //     this.sTransTypeCd = sTransTypeCd;
    // }

    public String getsLineCd() {
        return sLineCd;
    }

    public void setsLineCd(String sLineCd) {
        this.sLineCd = sLineCd;
    }

    public String getsLogiWeiCd() {
        return sLogiWeiCd;
    }

    public void setsLogiWeiCd(String sLogiWeiCd) {
        this.sLogiWeiCd = sLogiWeiCd;
    }

    public String getsCarType() {
        return sCarType;
    }

    public void setsCarType(String sCarType) {
        this.sCarType = sCarType;
    }

    public String getsVin() {
        return sVin;
    }

    public void setsVin(String sVin) {
        this.sVin = sVin;
    }

    public String getsFuelTypeCd() {
        return sFuelTypeCd;
    }

    public void setsFuelTypeCd(String sFuelTypeCd) {
        this.sFuelTypeCd = sFuelTypeCd;
    }

    public String getsCarMade() {
        return sCarMade;
    }

    public void setsCarMade(String sCarMade) {
        this.sCarMade = sCarMade;
    }

    public String getsUseTypeCd() {
        return sUseTypeCd;
    }

    public void setsUseTypeCd(String sUseTypeCd) {
        this.sUseTypeCd = sUseTypeCd;
    }

    public String getsCarStatusCd() {
        return sCarStatusCd;
    }

    public void setsCarStatusCd(String sCarStatusCd) {
        this.sCarStatusCd = sCarStatusCd;
    }

    public String getsCarCompany() {
        return sCarCompany;
    }

    public void setsCarCompany(String sCarCompany) {
        this.sCarCompany = sCarCompany;
    }

    public String getsCarModel() {
        return sCarModel;
    }

    public void setsCarModel(String sCarModel) {
        this.sCarModel = sCarModel;
    }

    public String getsRegDate() {
        return sRegDate;
    }

    public void setsRegDate(String sRegDate) {
        this.sRegDate = sRegDate;
    }

    public String getsModDate() {
        return sModDate;
    }

    public void setsModDate(String sModDate) {
        this.sModDate = sModDate;
    }

    public String getsRegId() {
        return sRegId;
    }

    public void setsRegId(String sRegId) {
        this.sRegId = sRegId;
    }

    public String getsModId() {
        return sModId;
    }

    public void setsModId(String sModId) {
        this.sModId = sModId;
    }

    public String getcDelYn() {
        return cDelYn;
    }

    public void setcDelYn(String cDelYn) {
        this.cDelYn = cDelYn;
    }

    public void setIdVehicle(Long idVehicle) {
        this.idVehicle = idVehicle;
    }

    public String getsErpId() {
        return sErpId;
    }

    public void setsErpId(String sErpId) {
        this.sErpId = sErpId;
    }

    public String getsTransComId() {
        return sTransComId;
    }

    public void setsTransComId(String sTransComId) {
        this.sTransComId = sTransComId;
    }

    
    public String getsLogiYctCd() {
        return sLogiYctCd;
    }

    public void setsLogiYctCd(String sLogiYctCd) {
        this.sLogiYctCd = sLogiYctCd;
    }

   

    public Float getnFuelEff() {
        return nFuelEff;
    }

    public void setnFuelEff(Float nFuelEff) {
        this.nFuelEff = nFuelEff;
    }
    
    public Long getIdTransCom() {
        return idTransCom;
    }

    public void setIdTransCom(Long idTransCom) {
        this.idTransCom = idTransCom;
    }

    public String getsCarLoanDate() {
        return sCarLoanDate;
    }

    public void setsCarLoanDate(String sCarLoanDate) {
        this.sCarLoanDate = sCarLoanDate;
    }

    public void setIndex(int i) {
        this.index = i;
    }
    
    public int getIndex() {
        return this.index;
    }

    public String getsSelfvrnCd() {
        return sSelfvrnCd;
    }

    public void setsSelfvrnCd(String sSelfvrnCd) {
        this.sSelfvrnCd = sSelfvrnCd;
    }

    public void setIdErpDept(Long idErpDept) {
        this.idErpDept = idErpDept;
    }

    public TVehicle toEntity()
    {
        TVehicle v = new TVehicle();
        v.setIdVehicle(this.idVehicle);
        v.setIdTransCom(this.idTransCom);
        v.setsVrn(this.sVrn);
        v.setsLineCd(this.sLogiYctCd);  // 노선 구분 (정기/ 비정기)
        if(StringUtil.isEmpty(this.sLogiWeiCd)){
            v.setsLogiWeiCd("99.9"); // 톤수
        }else{
            v.setsLogiWeiCd(this.sLogiWeiCd); // 톤수
        }

        v.setsCarType(this.sCarType);
        v.setsVin(this.sVin);
      
        if(StringUtil.isEmpty(this.getsFuelTypeCd())) // 연료 타입
            v.setsFuelTypeCd("1");
        else
            v.setsFuelTypeCd(this.sFuelTypeCd);

        v.setsFuelEff(this.nFuelEff);  // 공인연비
        v.setsCarMade(this.sCarMade);
        v.setsUseTypeCd(this.sUseTypeCd);
        v.setsCarStatusCd(this.sCarStatusCd);
        v.setsCarCompany(this.sCarCompany);
        v.setsCarModel(this.sCarModel);

        v.setsRegDate(DateUtil.convertYYYYMMDDToDate(this.sRegDate));
        v.setsModDate(DateUtil.convertYYYYMMDDToDate(this.sModDate));
        
        v.setsCarLoanDate(DateUtil.convertYYYYMMDDToDate(this.sCarLoanDate));
        v.setsRegId(this.sRegId);
        v.setsModId(this.sModId);
        if("1".equals(this.sCarStatusCd))
            v.setcDelYn("N");
        else
            v.setcDelYn("Y");
        v.setsTransComCd(this.sTransComId);
        v.setsErpDeptCd(this.sErpId);        
        v.setsSelfvrnCd(this.sSelfvrnCd);
        v.setIdErpDept(this.idErpDept);
        return v;
    }








}