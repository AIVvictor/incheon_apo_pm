package com.sebang.ems.model.Dto;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import com.sebang.ems.model.Do.TCorpInfo;

import lombok.Builder;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Setter
@ToString
@NoArgsConstructor
public class ReqCorpInfo {

    private Long idCorp;

    @NotNull(message = "법인ID 는 필수 입력 값입니다.")
    String sCorpId;

    @NotNull(message = "법인명은 필수 입력 값입니다.")
    private String sCorpNm;
    
    @NotNull(message = "법인번호 필수 입력 값입니다.")
    private String sCorpRegNum;
    
    @NotNull(message = "대표자명은 필수 입력 값입니다.")
    private String sOwner;
    
    @NotNull(message = "법인 대표 전화번호는 필수 입력 값입니다.")
    private String sCorpTel;
    
    @NotNull(message = "대표업종 항목은 필수 입력 값입니다.")
    private String sCorpCat;
    
    @NotNull(message = "법인소재지 항목은 필수 입력 값입니다.")
    private String sCorpAddr;
    
    private String sPicNm;
    
    private String sPicDept;
    
    private String sPicPos;
    
    private String sPicTel;
    
    private String sPicHp;
    
    private String sPicMail;
    
    private String sMainPrd;

    private String sSmlYn;

    private String sCertPro;

    private String sCertEnv;

    private String sNgmsCorp;

    private String sRegId;

    private String sModId;

    private String cDelYn;

    public Long getIdCorp() {
        return idCorp;
    }

    public String getsCorpId() {
        return sCorpId;
    }

    public String getsCorpNm() {
        return sCorpNm;
    }

    public String getsCorpRegNum() {
        return sCorpRegNum;
    }

    public String getsOwner() {
        return sOwner;
    }

    public String getsCorpTel() {
        return sCorpTel;
    }

    public String getsCorpCat() {
        return sCorpCat;
    }

    public String getsCorpAddr() {
        return sCorpAddr;
    }

    public String getsPicNm() {
        return sPicNm;
    }

    public String getsPicDept() {
        return sPicDept;
    }

    public String getsPicPos() {
        return sPicPos;
    }

    public String getsPicTel() {
        return sPicTel;
    }

    public String getsPicHp() {
        return sPicHp;
    }

    public String getsPicMail() {
        return sPicMail;
    }

    public String getsMainPrd() {
        return sMainPrd;
    }

    public String getsSmlYn() {
        return sSmlYn;
    }

    public String getsCertPro() {
        return sCertPro;
    }

    public String getsCertEnv() {
        return sCertEnv;
    }

    public String getsNgmsCorp() {
        return sNgmsCorp;
    }

    public String getsRegId() {
        return sRegId;
    }

    public String getsModId() {
        return sModId;
    }

    public String getcDelYn() {
        return cDelYn;
    }

    public TCorpInfo toEntity()
    {
       TCorpInfo c = new TCorpInfo();
        c.setIdCorp(this.idCorp);
        c.setsCorpId(this.sCorpId);
        c.setsCorpNm(this.sCorpNm);
        c.setsCorpRegNum(this.sCorpRegNum);
        c.setsOwner(this.sOwner);
        c.setsCorpTel(this.sCorpTel);
        c.setsCorpCat(this. sCorpCat);
        c.setsCorpAddr(this.sCorpAddr);
        c.setsPicNm(this.sPicNm);
        c.setsPicDept(this.sPicDept);
        c.setsPicPos(this.sPicPos);
        c.setsPicTel(this.sPicTel);
        c.setsPicHp(this.sPicHp);
        c.setsPicMail(this.sPicMail);
        c.setsMainPrd(this.sMainPrd);
        c.setsSmlYn(this.sSmlYn) ;
        c.setsCertPro(this.sCertPro);
        c.setsCertEnv(this.sCertEnv);
        c.setsNgmsCorp(this.sNgmsCorp);
        c.setsRegId(this.sRegId);
        c.setsModId(this.sModId) ;
        c.setcDelYn(this.cDelYn) ;
       return c;
    }

}