package com.sebang.ems.model.Dto;

import java.math.BigDecimal;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import com.sebang.ems.model.Do.TLogisTransInfo;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@Builder
public class ReqLogisTransInfo
{
    private Long idLogisTransInfo;

    @NotNull(message = "배차ID(sVhclDispld) 는 필수 입력 값입니다. ")
    private String sVhclDispld;
    
    private Long idVehicle;

    @Pattern(regexp = "(19|20)\\d{2}(0[1-9]|1[012])(0[1-9]|[12][0-9]|3[01])",message = "날짜의 형식은 YYYYMMDD 입니다.")
    @NotNull(message = "날짜(cDate) 는 필수 입력 값입니다. ")
    private String cDate;
    
    private String sCount;
    
    private String sSelfvrnCd;
    
    private String sVrn;
    
    @Pattern(regexp = "[123]",message = "운송수단 1: 화물차, 2: 선박, 3: 철도" )
    @NotNull(message = "운송수단(sTransTypeCd) 는 필수 입력 값입니다. ")
    private String sTransTypeCd;
    
    @Size(min=6,max=10, message ="부서 ID (sErpId) 의 Length 는6 이상 10 이하입니다." )
    @NotNull(message ="부서 ID (sErpId) 은 필수 입력 값입니다. ")
    private String sErpId;

    private Long idErpDept;  // 부서ID

    private Long idTransCom;
    
    private String sTransCompId;
    
    private String sLogiWeiCd;
    
    private String sStartPnt;
        
    private String sDestPnt;

    @NotNull(message = "적재량(sLoadage)는 필수 입력 값입니다.")
    private String sLoadage;
    
    private BigDecimal sFare;

    private BigDecimal nFuelEff; //공인연비
    
    private String sPrdCd;
    
    private String sPrdNm;
        
    private BigDecimal nVacantDistance;
    
    private BigDecimal sDistance;
    
    @Pattern(regexp = "(19|20)\\d{2}(0[1-9]|1[012])(0[1-9]|[12][0-9]|3[01])(0[0-9]|1[0-9]|2[0-3])([0-5][0-9])([0-5][0-9])",message = "날짜형식은 YYYYMMDDHHmiSS 입니다.")
    @NotNull(message = "등록일시(sRegMod) 필수 입력 값입니다.")
    private String sRegMod;    
    
    private BigDecimal nEng;
    
    private BigDecimal nMj;
    
    private BigDecimal nGco2;
    
    private BigDecimal nTonKm;   // 톤킬로 온실가스 원단위 
    
    // @Pattern(regexp = "[12]",message = "(비)정기 노선 구분 1: 정기, 2: 비정기" )
    // @NotNull(message = "(비)노선구분 (sLogiYctCd) 는 필수 입력 값입니다. ")
    private String sLogiYctCd; 

    private BigDecimal nToe;     // 환산톤

    private BigDecimal nVol;     // 부피

    private String nVolUnt;  // 부피단위: 'm3', 'L'


    @Pattern(regexp = "[12]",message = "1: 정상, 2: 삭제")
    @NotNull(message = "운영상태(sComStatusCd)는 필수 입력 값입니다.")
    private String sComStatusCd;

    private String sRegId;

    private String sModId;

    private int index;

    private Long idEngPoint;

    public Long getIdVehicle() {
        return idVehicle;
    }

    public String getcDate() {
        return cDate;
    }

    public void setcDate(String cDate) {
        this.cDate = cDate;
    }

    public String getsCount() {
        return sCount;
    }

    public void setsCount(String sCount) {
        this.sCount = sCount;
    }

    public String getsSelfvrnCd() {
        return sSelfvrnCd;
    }

    public void setsSelfvrnCd(String sSelfvrnCd) {
        this.sSelfvrnCd = sSelfvrnCd;
    }

    public String getsVrn() {
        return sVrn;
    }

    public void setsVrn(String sVrn) {
        this.sVrn = sVrn;
    }

    public String getsTransTypeCd() {
        return sTransTypeCd;
    }

    public void setsTransTypeCd(String sTransTypeCd) {
        this.sTransTypeCd = sTransTypeCd;
    }

    public String getsTransCompId() {
        return sTransCompId;
    }

    public void setsTransCompId(String sTransComp) {
        this.sTransCompId = sTransComp;
    }

    public String getsLogiWeiCd() {
        return sLogiWeiCd;
    }

    public void setsLogiWeiCd(String sLogiWeiCd) {
        this.sLogiWeiCd = sLogiWeiCd;
    }

    public String getsStartPnt() {
        return sStartPnt;
    }

    public void setsStartPnt(String sStartPnt) {
        this.sStartPnt = sStartPnt;
    }

    public String getsDestPnt() {
        return sDestPnt;
    }

    public void setsDestPnt(String sDestPnt) {
        this.sDestPnt = sDestPnt;
    }

    public String getsLoadage() {
        return sLoadage;
    }

    public void setsLoadage(String sLoadage) {
        this.sLoadage = sLoadage;
    }

    public BigDecimal getsFare() {
        return sFare;
    }

    public void setsFare(BigDecimal sFare) {
        this.sFare = sFare;
    }

    public String getsPrdCd() {
        return sPrdCd;
    }

    public void setsPrdCd(String sPrdCd) {
        this.sPrdCd = sPrdCd;
    }

    public String getsPrdNm() {
        return sPrdNm;
    }

    public void setsPrdNm(String sPrdNm) {
        this.sPrdNm = sPrdNm;
    }

    public BigDecimal getnVacantDistance() {
        return nVacantDistance;
    }

    public void setnVacantDistance(BigDecimal nVacantDistance) {
        this.nVacantDistance = nVacantDistance;
    }

    public BigDecimal getsDistance() {
        return sDistance;
    }

    public void setsDistance(BigDecimal sDistance) {
        this.sDistance = sDistance;
    }

    public String getsRegMod() {
        return sRegMod;
    }

    public void setsRegMod(String sRegMod) {
        this.sRegMod = sRegMod;
    }

    public BigDecimal getnEng() {
        return nEng;
    }

    public void setnEng(BigDecimal nEng) {
        this.nEng = nEng;
    }

    public BigDecimal getnMj() {
        return nMj;
    }

    public void setnMj(BigDecimal nMj) {
        this.nMj = nMj;
    }

    public BigDecimal getnGco2() {
        return nGco2;
    }

    public void setnGco2(BigDecimal nGco2) {
        this.nGco2 = nGco2;
    }

    public BigDecimal getnTonKm() {
        return nTonKm;
    }

    public void setnTonKm(BigDecimal nTonKm) {
        this.nTonKm = nTonKm;
    }

    public Long getIdLogisTransInfo() {
        return idLogisTransInfo;
    }

    public String getsVhclDispld() {
        return sVhclDispld;
    }

    public void setsVhclDispld(String sVhclDispld) {
        this.sVhclDispld = sVhclDispld;
    }

    public String getsErpId() {
        return sErpId;
    }

    public void setsErpId(String sErpId) {
        this.sErpId = sErpId;
    }

    public String getsLogiYctCd() {
        return sLogiYctCd;
    }

    public void setsLogiYctCd(String sLogiYctCd) {
        this.sLogiYctCd = sLogiYctCd;
    }

    public String getsComStatusCd() {
        return sComStatusCd;
    }

    public void setsComStatusCd(String sComStatusCd) {
        this.sComStatusCd = sComStatusCd;
    }

    public BigDecimal getnVol() {
        return nVol;
    }

    public void setnVol(BigDecimal nVol) {
        this.nVol = nVol;
    }

    public String getnVolUnt() {
        return nVolUnt;
    }

    public void setnVolUnt(String nVolUnt) {
        this.nVolUnt = nVolUnt;
    }

    public String getsRegId() {
        return sRegId;
    }

    public void setsRegId(String sRegId) {
        this.sRegId = sRegId;
    }

    public String getsModId() {
        return sModId;
    }

    public void setsModId(String sModId) {
        this.sModId = sModId;
    }

    public int getIndex() {
        return index;
    }    

    public Long getIdTransCom() {
        return idTransCom;
    }

    public Long getIdErpDept() {
        return idErpDept;
    }

    public BigDecimal getnToe() {
        return nToe;
    }

    public void setnToe(BigDecimal nToe) {
        this.nToe = nToe;
    }

    public Long getIdEngPoint() {
        return idEngPoint;
    }

    public BigDecimal getnFuelEff() {
        return nFuelEff;
    }

    public void setnFuelEff(BigDecimal nFuelEff) {
        this.nFuelEff = nFuelEff;
    }

    public TLogisTransInfo toEntity()
    {
        TLogisTransInfo d = new TLogisTransInfo();
        
        d.setTransId(this.getsVhclDispld());
        d.setIdLogisTransInfo(this.idLogisTransInfo);
        d.setIdVehicle(this.idVehicle);
        d.setIdTransCom(this.idTransCom);
        d.setcDate(this.cDate);
        d.setsCount(this.sCount);
        d.setsSelfvrnCd(this.sSelfvrnCd);
        d.setsVrn(this.sVrn);
        d.setsTransTypeCd(this.sTransTypeCd);        
        d.setsLogiWeiCd(this.sLogiWeiCd);
        d.setsStartPnt(this.sStartPnt);
        d.setsDestPnt(this.sDestPnt);
        if(this.sLoadage!=null){
            try{
                BigDecimal f=  new BigDecimal(this.sLoadage);   
                d.setsLoadage(f);
            }catch(Exception e){
                d.setsLoadage(BigDecimal.ZERO);
            }   
        
        }

        if("1".equals(sComStatusCd)){
            d.setcDelYn("N");
        }
        else
            d.setcDelYn("Y");        

        if(this.sFare!=null)
            d.setsFare(sFare.longValue());

        d.setsPrdCd(this.sPrdCd);
        d.setsPrdNm(this.sPrdNm);
       
        d.setnVacantDistance(this.nVacantDistance);
        d.setsDistance(this.sDistance);  
    
        d.setsRegMod(this.sRegMod);
        d.setnMj(this.nMj);
        d.setnGco2(this.nGco2);
        d.setnTonKm(this.nTonKm);
        d.setnToe(this.nToe);
        d.setnVolume(this.nVol);
        d.setsVolumeUnt(this.nVolUnt);

        d.setsRegId(this.sRegId);
        d.setsModId(this.sModId);
        d.setnFuelEff(this.nFuelEff);
        d.setIdErpDept(this.idErpDept);  
        d.setIdEngPoint(this.idEngPoint);      
        return d;

    }








}

