package com.sebang.ems.model.Dto;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.validation.constraints.NotNull;

import com.sebang.ems.domain.Exception.BaseException;
import com.sebang.ems.model.Do.TGasDeptMapping;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@ToString
@Setter
@Builder
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@NoArgsConstructor
public class ReqDeptMap
{

    Long idGasDeptMapping;

    @NotNull(message = "사업장 No 는 필수 입력 값입니다. ")
    Long idSite;

    @NotNull(message = "부서 No 는 필수 입력 값입니다. ")
    Long idErpDept;

    public Long getIdErpDept() {
        return idErpDept;
    }

    public Long getIdSite() {
        return idSite;
    }

    public Long getIdGasDeptMapping() {
        return idGasDeptMapping;
    }

    public TGasDeptMapping toEntity()
    {
        TGasDeptMapping m = new TGasDeptMapping();
        m.setIdGasDeptMapping(this.idGasDeptMapping);
        m.setIdSite(this.idSite);
        m.setIdErpDept(this.idErpDept);
        return m;
    }


    public static List<ReqDeptMap> toList(Map<String,String> paramMap)
    {
        List<ReqDeptMap> result = new ArrayList<ReqDeptMap>();
        for(String key :paramMap.keySet())
        {
            String value = paramMap.get(key);
            if( value ==  null || value.length()==0)
                continue;

            String depts[] =  value.split(",");
            for(String dept : depts)
            {
                ReqDeptMap m = new ReqDeptMap();
                try{                    
                    m.setIdSite(Long.parseLong(key));
                    m.setIdErpDept(Long.parseLong(dept));
                }catch(NumberFormatException ex){
                    throw new BaseException("cann't convert key " + key+" ,value "+ dept  );
                }
                result.add(m);
            }
        }

        return result;
    }

}