package com.sebang.ems.model.Dto;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.sebang.ems.model.Do.VBoardWithBLOBs;

@JsonInclude(value = Include.NON_EMPTY)
public class ResQueryBoard {
	/**
	 *
	 */
	private Long idBoard;

	/**
	 *
	 */
	private String sBoardType;

	/**
	 *
	 */
	private String sTitle;

	/**
	 *
	 */
	private String sContent;

	/**
	 *
	 */
	private String sRegEmpId;

	/**
	 *
	 */
	private String sEmpNm;

	/**
	 *
	 */
	private Date dRegDate;

	/**
	 *
	 */
	private String gFileId;

	public class FileInfo {
		private String idFile;
		private String sFileNm;
//		private String sFilePath;

		public String getIdFile() {
			return idFile;
		}

		public void setIdFile(String idFile) {
			this.idFile = idFile;
		}

		public String getsFileNm() {
			return sFileNm;
		}

		public void setsFileNm(String sFileNm) {
			this.sFileNm = sFileNm;
		}

//		public String getsFilePath() {
//			return sFilePath;
//		}
//
//		public void setsFilePath(String sFilePath) {
//			this.sFilePath = sFilePath;
//		}

	}

	List<FileInfo> files = new ArrayList<FileInfo>();

	public Long getIdBoard() {
		return idBoard;
	}

	public void setIdBoard(Long idBoard) {
		this.idBoard = idBoard;
	}

	public String getsBoardType() {
		return sBoardType;
	}

	public void setsBoardType(String sBoardType) {
		this.sBoardType = sBoardType;
	}

	public String getsTitle() {
		return sTitle;
	}

	public void setsTitle(String sTitle) {
		this.sTitle = sTitle;
	}

	public String getsContent() {
		return sContent;
	}

	public void setsContent(String sContent) {
		this.sContent = sContent;
	}

	public String getsRegEmpId() {
		return sRegEmpId;
	}

	public void setsRegEmpId(String sRegEmpId) {
		this.sRegEmpId = sRegEmpId;
	}

	public String getsEmpNm() {
		return sEmpNm;
	}

	public void setsEmpNm(String sEmpNm) {
		this.sEmpNm = sEmpNm;
	}

	public Date getdRegDate() {
		return dRegDate;
	}

	public void setdRegDate(Date dRegDate) {
		this.dRegDate = dRegDate;
	}

	public String getgFileId() {
		return gFileId;
	}

	public void setgFileId(String gFileId) {
		this.gFileId = gFileId;
	}

	public List<FileInfo> getFiles() {
		return files;
	}

	public void setFiles(List<FileInfo> files) {
		this.files = files;
	}

	public void put(VBoardWithBLOBs b,boolean flagBody) {
		setIdBoard(b.getIdBoard());
		setsBoardType(b.getsBoardType());
		setsTitle(b.getsTitle());
		setsContent(flagBody?b.getsContent():null);
		setdRegDate(b.getdRegDate());
		setsRegEmpId(b.getsRegEmpId());
		setsEmpNm(b.getsEmpNm());
		setgFileId(b.getgFileId());

		String[] idFiles = b.getIdFile() != null ? b.getIdFile().split(",") : null;
		String[] fileNms = b.getsFileNm() != null ? b.getsFileNm().split(",") : null;

		if (idFiles != null) {
			for (int i = 0; i < idFiles.length; i++) {
				FileInfo fi = new FileInfo();
				if (idFiles != null && idFiles[i] != null) {
					fi.setIdFile(idFiles[i]);
				}
				if (fileNms != null && fileNms[i] != null) {
					fi.setsFileNm(fileNms[i]);
				}
				files.add(fi);
			}
		}
	}

	public ResQueryBoard() {
	}

	/**
	 * VBoardWithBLOBs 을 ResQueryBoard 으로 변환
	 * @param in List of VBoardWithBLOBs
	 * @param flagBody content body의 포함 여부
	 */
	public ResQueryBoard(VBoardWithBLOBs in,boolean flagBody) {
		put(in,flagBody);
	}

	/**
	 * VBoardWithBLOBs 목록을 ResQueryBoard 목록으로 변환
	 * @param in List of VBoardWithBLOBs
	 * @param flagBody content body의 포함 여부
	 * @return
	 */
	public static List<ResQueryBoard> makeResponse(List<VBoardWithBLOBs> in,boolean flagBody) {
		List<ResQueryBoard> ret = new ArrayList<ResQueryBoard>();

		for (VBoardWithBLOBs bb : in) {
			ret.add(new ResQueryBoard(bb,flagBody));
		}

		return ret;
	}

}