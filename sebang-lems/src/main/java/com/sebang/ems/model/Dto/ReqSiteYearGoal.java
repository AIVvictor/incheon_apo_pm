package com.sebang.ems.model.Dto;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.constraints.NotNull;

import com.sebang.ems.model.SiteMonGoal;
import com.sebang.ems.model.SiteYearGoal;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;
import lombok.ToString;


@ToString
@NoArgsConstructor
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@Builder
public class ReqSiteYearGoal
{
    @NotNull (message = "idSite 는 필수 입력 값입니다. ")
    private Long idSite;

    @NotNull (message = "cYear 는 필수 입력 값입니다. ")
    private String cYear;

    private BigDecimal nYearGasGoal;

    private BigDecimal nYearEngGoal;

    private List<ReqSiteMonGoal> goals;

    public Long getIdSite() {
        return idSite;
    }

    public void setIdSite(Long idSite) {
        this.idSite = idSite;
    }

    public String getcYear() {
        return cYear;
    }

    public void setcYear(String cYear) {
        this.cYear = cYear;
    }

    public BigDecimal getnYearGasGoal() {
        return nYearGasGoal;
    }

    public void setnYearGasGoal(BigDecimal nYearGasGoal) {
        this.nYearGasGoal = nYearGasGoal;
    }

    public BigDecimal getnYearEngGoal() {
        return nYearEngGoal;
    }

    public void setnYearEngGoal(BigDecimal nYearEngGoal) {
        this.nYearEngGoal = nYearEngGoal;
    }

    public List<ReqSiteMonGoal> getGoals() {
        return goals;
    }

    public void setGoals(List<ReqSiteMonGoal> goals) {
        this.goals = goals;
    }

    public SiteYearGoal toEntity()
    {
        SiteYearGoal sm = new SiteYearGoal();
        sm.setIdSite(this.idSite);
        sm.setcYear(this.cYear);
        sm.setnYearEngGoal(this.nYearEngGoal);
        sm.setnYearGasGoal(this.nYearGasGoal);
        
        Map<Long, List<SiteMonGoal>> map = new HashMap<Long,List<SiteMonGoal>>();

        for(ReqSiteMonGoal mg :goals){
            map.put(mg.getIdEngPoint(),mg.toEntity());
            //sm.setMonGoals(mg.toEntity());
            sm.setPointMonGoals(map);
        }
        return sm;
    }
}