package com.sebang.ems.model.Do;

import java.util.Date;

public class TRawErpDept {
    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column T_RAW_ERP_DEPT.S_DEPT_ID
     *
     * @mbg.generated Thu Oct 31 11:56:28 KST 2019
     */
    private String sDeptId;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column T_RAW_ERP_DEPT.S_U_DEPT_ID
     *
     * @mbg.generated Thu Oct 31 11:56:28 KST 2019
     */
    private String sUDeptId;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column T_RAW_ERP_DEPT.S_DEPT_NM
     *
     * @mbg.generated Thu Oct 31 11:56:28 KST 2019
     */
    private String sDeptNm;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column T_RAW_ERP_DEPT.S_REG_ID
     *
     * @mbg.generated Thu Oct 31 11:56:28 KST 2019
     */
    private String sRegId;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column T_RAW_ERP_DEPT.D_REG_DATE
     *
     * @mbg.generated Thu Oct 31 11:56:28 KST 2019
     */
    private Date dRegDate;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column T_RAW_ERP_DEPT.S_MOD_ID
     *
     * @mbg.generated Thu Oct 31 11:56:28 KST 2019
     */
    private String sModId;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column T_RAW_ERP_DEPT.D_MOD_DATE
     *
     * @mbg.generated Thu Oct 31 11:56:28 KST 2019
     */
    private Date dModDate;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column T_RAW_ERP_DEPT.C_DEL_YN
     *
     * @mbg.generated Thu Oct 31 11:56:28 KST 2019
     */
    private String cDelYn;

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column T_RAW_ERP_DEPT.S_DEPT_ID
     *
     * @return the value of T_RAW_ERP_DEPT.S_DEPT_ID
     *
     * @mbg.generated Thu Oct 31 11:56:28 KST 2019
     */
    public String getsDeptId() {
        return sDeptId;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column T_RAW_ERP_DEPT.S_DEPT_ID
     *
     * @param sDeptId the value for T_RAW_ERP_DEPT.S_DEPT_ID
     *
     * @mbg.generated Thu Oct 31 11:56:28 KST 2019
     */
    public void setsDeptId(String sDeptId) {
        this.sDeptId = sDeptId;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column T_RAW_ERP_DEPT.S_U_DEPT_ID
     *
     * @return the value of T_RAW_ERP_DEPT.S_U_DEPT_ID
     *
     * @mbg.generated Thu Oct 31 11:56:28 KST 2019
     */
    public String getsUDeptId() {
        return sUDeptId;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column T_RAW_ERP_DEPT.S_U_DEPT_ID
     *
     * @param sUDeptId the value for T_RAW_ERP_DEPT.S_U_DEPT_ID
     *
     * @mbg.generated Thu Oct 31 11:56:28 KST 2019
     */
    public void setsUDeptId(String sUDeptId) {
        this.sUDeptId = sUDeptId;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column T_RAW_ERP_DEPT.S_DEPT_NM
     *
     * @return the value of T_RAW_ERP_DEPT.S_DEPT_NM
     *
     * @mbg.generated Thu Oct 31 11:56:28 KST 2019
     */
    public String getsDeptNm() {
        return sDeptNm;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column T_RAW_ERP_DEPT.S_DEPT_NM
     *
     * @param sDeptNm the value for T_RAW_ERP_DEPT.S_DEPT_NM
     *
     * @mbg.generated Thu Oct 31 11:56:28 KST 2019
     */
    public void setsDeptNm(String sDeptNm) {
        this.sDeptNm = sDeptNm;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column T_RAW_ERP_DEPT.S_REG_ID
     *
     * @return the value of T_RAW_ERP_DEPT.S_REG_ID
     *
     * @mbg.generated Thu Oct 31 11:56:28 KST 2019
     */
    public String getsRegId() {
        return sRegId;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column T_RAW_ERP_DEPT.S_REG_ID
     *
     * @param sRegId the value for T_RAW_ERP_DEPT.S_REG_ID
     *
     * @mbg.generated Thu Oct 31 11:56:28 KST 2019
     */
    public void setsRegId(String sRegId) {
        this.sRegId = sRegId;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column T_RAW_ERP_DEPT.D_REG_DATE
     *
     * @return the value of T_RAW_ERP_DEPT.D_REG_DATE
     *
     * @mbg.generated Thu Oct 31 11:56:28 KST 2019
     */
    public Date getdRegDate() {
        return dRegDate;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column T_RAW_ERP_DEPT.D_REG_DATE
     *
     * @param dRegDate the value for T_RAW_ERP_DEPT.D_REG_DATE
     *
     * @mbg.generated Thu Oct 31 11:56:28 KST 2019
     */
    public void setdRegDate(Date dRegDate) {
        this.dRegDate = dRegDate;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column T_RAW_ERP_DEPT.S_MOD_ID
     *
     * @return the value of T_RAW_ERP_DEPT.S_MOD_ID
     *
     * @mbg.generated Thu Oct 31 11:56:28 KST 2019
     */
    public String getsModId() {
        return sModId;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column T_RAW_ERP_DEPT.S_MOD_ID
     *
     * @param sModId the value for T_RAW_ERP_DEPT.S_MOD_ID
     *
     * @mbg.generated Thu Oct 31 11:56:28 KST 2019
     */
    public void setsModId(String sModId) {
        this.sModId = sModId;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column T_RAW_ERP_DEPT.D_MOD_DATE
     *
     * @return the value of T_RAW_ERP_DEPT.D_MOD_DATE
     *
     * @mbg.generated Thu Oct 31 11:56:28 KST 2019
     */
    public Date getdModDate() {
        return dModDate;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column T_RAW_ERP_DEPT.D_MOD_DATE
     *
     * @param dModDate the value for T_RAW_ERP_DEPT.D_MOD_DATE
     *
     * @mbg.generated Thu Oct 31 11:56:28 KST 2019
     */
    public void setdModDate(Date dModDate) {
        this.dModDate = dModDate;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column T_RAW_ERP_DEPT.C_DEL_YN
     *
     * @return the value of T_RAW_ERP_DEPT.C_DEL_YN
     *
     * @mbg.generated Thu Oct 31 11:56:28 KST 2019
     */
    public String getcDelYn() {
        return cDelYn;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column T_RAW_ERP_DEPT.C_DEL_YN
     *
     * @param cDelYn the value for T_RAW_ERP_DEPT.C_DEL_YN
     *
     * @mbg.generated Thu Oct 31 11:56:28 KST 2019
     */
    public void setcDelYn(String cDelYn) {
        this.cDelYn = cDelYn;
    }
}