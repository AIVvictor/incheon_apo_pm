package com.sebang.ems.model;

import org.apache.poi.ss.usermodel.Row;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@ToString
public class Product {

	private String uniqueId;

	private String name;

	private String comment;
	
	private String pass;

	public static Product from(Row row) {
		String v=null;
		if(row.getCell(3)!=null)
			v= row.getCell(3).getStringCellValue();
		
		return new Product(row.getCell(0).getStringCellValue(), row.getCell(1).getStringCellValue(), row.getCell(2).getStringCellValue(),v);
	}
}
