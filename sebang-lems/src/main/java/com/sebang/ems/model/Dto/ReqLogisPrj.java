package com.sebang.ems.model.Dto;

import javax.validation.constraints.NotNull;

import com.sebang.ems.model.Do.TLogisPrj;
import com.sebang.ems.model.Do.TLogisPrjAct;
import com.sebang.ems.model.Do.TLogisPrjAct2;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@ToString
@Setter
@AllArgsConstructor(access=AccessLevel.PRIVATE)
@NoArgsConstructor
@Builder
public class ReqLogisPrj
{
    private Long idLogisPrj;

    @NotNull(message = "sPrjNm 은 필수 입력 값입니다. ")
    private String sPrjNm;

    @NotNull(message = "sPrjNm 은 필수 입력 값입니다. ")
    private String sLogiPtyCd;
    
    @NotNull(message = "sPrjComm 은 필수 입력 값입니다. ")
    private String sPrjComm;

    @NotNull(message = "sLogiMeaCd 은 필수 입력 값입니다. ")
    private String sLogiMeaCd;

    private String sLogiUntCd;

    @NotNull(message = "sLogiTarCd 은 필수 입력 값입니다. ")
    private String sLogiTarCd;

    private String sDept;

    private String sPic;

    @NotNull(message = "cYear 은 필수 입력 값입니다. ")
    private String cYear;

    @NotNull(message = "cSMon 은 필수 입력 값입니다. ")
    private String cSMon;

    @NotNull(message = "cEMon 은 필수 입력 값입니다. ")
    private String cEMon;

    @NotNull(message = "sLogiMoney 은 필수 입력 값입니다. ")
    private Float sLogiMoney;

    private String sPrjEff;

    private String gLogisPrjFile;

    private String sRegId;

    private String sModId;

    private String cDelYn;

    //개량형
    private Long idLogisPrjAct;

    private Integer nPlanCnt;

    private Float nPlanCost;

    private Float nPlanLiter;

    private Float nPlanTco2;

    private Integer nRsltCnt;

    private Float nRsltCost;

    private Float nRsltLiter;

    private Float nRsltTco2;

    //비계량형
    //private Long idLogisPrjAct2;

    private String cMon;

    private Float nPlanRate;

    private Float nRsltRate;

    public Long getIdLogisPrj() {
        return idLogisPrj;
    }

    public String getsPrjNm() {
        return sPrjNm;
    }

    public void setsPrjNm(String sPrjNm) {
        this.sPrjNm = sPrjNm;
    }

    public String getsLogiPtyCd() {
        return sLogiPtyCd;
    }

    public void setsLogiPtyCd(String sLogiPtyCd) {
        this.sLogiPtyCd = sLogiPtyCd;
    }

    public String getsPrjComm() {
        return sPrjComm;
    }

    public void setsPrjComm(String sPrjComm) {
        this.sPrjComm = sPrjComm;
    }

    public String getsLogiMeaCd() {
        return sLogiMeaCd;
    }

    public void setsLogiMeaCd(String sLogiMeaCd) {
        this.sLogiMeaCd = sLogiMeaCd;
    }

    public String getsLogiUntCd() {
        return sLogiUntCd;
    }

    public void setsLogiUntCd(String sLogiUntCd) {
        this.sLogiUntCd = sLogiUntCd;
    }

    public String getsLogiTarCd() {
        return sLogiTarCd;
    }

    public void setsLogiTarCd(String sLogiTarCd) {
        this.sLogiTarCd = sLogiTarCd;
    }

    public String getsDept() {
        return sDept;
    }

    public void setsDept(String sDept) {
        this.sDept = sDept;
    }

    public String getsPic() {
        return sPic;
    }

    public void setsPic(String sPic) {
        this.sPic = sPic;
    }

    public String getcYear() {
        return cYear;
    }

    public void setcYear(String cYear) {
        this.cYear = cYear;
    }

    public String getcSMon() {
        return cSMon;
    }

    public void setcSMon(String cSMon) {
        this.cSMon = cSMon;
    }

    public String getcEMon() {
        return cEMon;
    }

    public void setcEMon(String cEMon) {
        this.cEMon = cEMon;
    }

    public Float getsLogiMoney() {
        return sLogiMoney;
    }

    public void setsLogiMoney(Float sLogiMoney) {
        this.sLogiMoney = sLogiMoney;
    }

    public String getsPrjEff() {
        return sPrjEff;
    }

    public void setsPrjEff(String sPrjEff) {
        this.sPrjEff = sPrjEff;
    }

    public String getgLogisPrjFile() {
        return gLogisPrjFile;
    }

    public void setgLogisPrjFile(String gLogisPrjFile) {
        this.gLogisPrjFile = gLogisPrjFile;
    }

    public String getsRegId() {
        return sRegId;
    }

    public void setsRegId(String sRegId) {
        this.sRegId = sRegId;
    }

    public String getsModId() {
        return sModId;
    }

    public void setsModId(String sModId) {
        this.sModId = sModId;
    }

    public String getcDelYn() {
        return cDelYn;
    }

    public void setcDelYn(String cDelYn) {
        this.cDelYn = cDelYn;
    }

    public Long getIdLogisPrjAct() {
        return idLogisPrjAct;
    }

    public Integer getnPlanCnt() {
        return nPlanCnt;
    }

    public void setnPlanCnt(Integer nPlanCnt) {
        this.nPlanCnt = nPlanCnt;
    }

    public Float getnPlanCost() {
        return nPlanCost;
    }

    public void setnPlanCost(Float nPlanCost) {
        this.nPlanCost = nPlanCost;
    }

    public Float getnPlanLiter() {
        return nPlanLiter;
    }

    public void setnPlanLiter(Float nPlanLiter) {
        this.nPlanLiter = nPlanLiter;
    }

    public Float getnPlanTco2() {
        return nPlanTco2;
    }

    public void setnPlanTco2(Float nPlanTco2) {
        this.nPlanTco2 = nPlanTco2;
    }

    public Integer getnRsltCnt() {
        return nRsltCnt;
    }

    public void setnRsltCnt(Integer nRsltCnt) {
        this.nRsltCnt = nRsltCnt;
    }

    public Float getnRsltCost() {
        return nRsltCost;
    }

    public void setnRsltCost(Float nRsltCost) {
        this.nRsltCost = nRsltCost;
    }

    public Float getnRsltLiter() {
        return nRsltLiter;
    }

    public void setnRsltLiter(Float nRsltLiter) {
        this.nRsltLiter = nRsltLiter;
    }

    public Float getnRsltTco2() {
        return nRsltTco2;
    }

    public void setnRsltTco2(Float nRsltTco2) {
        this.nRsltTco2 = nRsltTco2;
    }

 

    public String getcMon() {
        return cMon;
    }

    public void setcMon(String cMon) {
        this.cMon = cMon;
    }

    public Float getnPlanRate() {
        return nPlanRate;
    }

    public void setnPlanRate(Float nPlanRate) {
        this.nPlanRate = nPlanRate;
    }

    public Float getnRsltRate() {
        return nRsltRate;
    }

    public void setnRsltRate(Float nRsltRate) {
        this.nRsltRate = nRsltRate;
    }

    public TLogisPrj toEntity()
    {
        TLogisPrj l = new TLogisPrj();
            l.setIdLogisPrj(this.idLogisPrj);
            l.setsPrjNm(this.sPrjNm);
            l.setsLogiPtyCd(this.sLogiPtyCd);
            l.setsPrjComm(this.sPrjComm);
            l.setsLogiMeaCd(this.sLogiMeaCd);
            l.setsLogiUntCd(this.sLogiUntCd);
            l.setsLogiTarCd(this.sLogiTarCd);
            l.setsDept(this.sDept);
            l.setsPic(this.sPic);
            l.setcYear(this.cYear);
            l.setcSMon(this.cSMon);
            l.setcEMon(this.cEMon);
            l.setsLogiMoney(this.sLogiMoney);
            l.setsPrjEff(this.sPrjEff);
            l.setgLogisPrjFile(this.gLogisPrjFile);
            l.setcDelYn(this.cDelYn);
        return l;
    }

    public TLogisPrjAct toEntityAct()
    {
        TLogisPrjAct l = new TLogisPrjAct();

        l.setIdLogisPrjAct(this.idLogisPrjAct);
        l.setIdLogisPrj(this.idLogisPrj);
        l.setcYear(this.cYear);
        l.setnPlanCnt(this.nPlanCnt);
        l.setnPlanCost(this.nPlanCost);
        l.setnPlanLiter(this.nPlanLiter);
        l.setnPlanTco2(this.nPlanTco2);
        l.setnRsltCnt(this.nRsltCnt);
        l.setnRsltCost(this.nRsltCost);
        l.setnRsltLiter(this.nRsltLiter);
        l.setnRsltTco2(this.nRsltTco2);
        return l;
    }
    
    public TLogisPrjAct2 toEntityAct2()
    {
        TLogisPrjAct2 l = new TLogisPrjAct2();
        l.setIdLogisPrjAct2(this.idLogisPrjAct);
        l.setIdLogisPrj(this.idLogisPrj);
        l.setcYear(this.cYear);
        l.setcMon(this.cMon);
        l.setnPlanRate(this.nPlanRate);
        l.setnRsltRate(this.nRsltRate);
        return l;
    }
}