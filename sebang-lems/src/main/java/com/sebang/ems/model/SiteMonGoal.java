package com.sebang.ems.model;

import java.math.BigDecimal;

import lombok.ToString;

@ToString
public class SiteMonGoal
{
    private Long idSiteMonSumGoal;

    private Long idEngPoint;

    private String cMon;

    private BigDecimal nMonEngGoal;

    public Long getIdSiteMonSumGoal() {
        return idSiteMonSumGoal;
    }

    public void setIdSiteMonSumGoal(Long idSiteMonSumGoal) {
        this.idSiteMonSumGoal = idSiteMonSumGoal;
    }

    public Long getIdEngPoint() {
        return idEngPoint;
    }

    public void setIdEngPoint(Long idEngPoint) {
        this.idEngPoint = idEngPoint;
    }

    public String getcMon() {
        return cMon;
    }

    public void setcMon(String cMon) {
        this.cMon = cMon;
    }

    public BigDecimal getnMonEngGoal() {
        return nMonEngGoal;
    }

    public void setnMonEngGoal(BigDecimal nMonEngGoal) {
        this.nMonEngGoal = nMonEngGoal;
    }
}