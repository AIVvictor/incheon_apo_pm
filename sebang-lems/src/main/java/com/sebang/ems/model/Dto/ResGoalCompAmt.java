package com.sebang.ems.model.Dto;

import java.math.BigDecimal;

import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.sebang.ems.model.Do.VGoalResultAnalyAcc;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@ToString
@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@JsonInclude(value = Include.NON_EMPTY)
@Builder
public class ResGoalCompAmt{
    
    @NotNull
    protected String cYear;

    @NotNull
    protected String cMon;

    private Long idSite;
    
    private Long idEngPoint;

    private BigDecimal nEngGoal;

    private BigDecimal nGasGoal;

    protected BigDecimal gj;

    protected BigDecimal co2;

    protected BigDecimal ch4;

    protected BigDecimal n2o;

    protected BigDecimal tco2eq;

    protected BigDecimal toe;


    public static ResGoalCompAmt toEntity(VGoalResultAnalyAcc v)
    {
        ResGoalCompAmt r = new ResGoalCompAmtBuilder()
                        .cYear(v.getcYear())
                        .cMon(v.getcMon())
                        .idSite(v.getIdSite())
                        .idEngPoint(v.getIdEngCef())
                        .nEngGoal(v.getnEngGoal())
                        .nGasGoal(v.getnGasGoal())
                        .gj(v.getnGj())
                        .co2(v.getnCo2())
                        .ch4(v.getnCh4())
                        .n2o(v.getnN2o())
                        .toe(v.getnToe())
                        .tco2eq(v.getnTco2eq())
                        .build();
        return r;
    } 

}