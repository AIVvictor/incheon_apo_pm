package com.sebang.ems.model;

import java.math.BigDecimal;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;


import lombok.Setter;
import lombok.ToString;

@ToString
@Setter
@JsonInclude(value = Include.NON_EMPTY)
public class SiteYearGoal4Home
{

    private String cYear;

    private BigDecimal nYearGasGoal;  // 정부 온실가스 배출 목표

    private BigDecimal nYearEngGoal;  // 정부 에너지 목표

    private BigDecimal nGasGoal;      //  사내온실가스 목표

    private BigDecimal nEngGoal;      // 사내 에너지 목표

    private BigDecimal nTco2;         // 온실가스 실적

    private BigDecimal nGj;           // 에너지 실적

    private BigDecimal nTco2PerYearGasGoal;       // 정부목표 대비 온실가스실적 달성률

    private BigDecimal nGjPerYearEngGoal;         // 정부목표 대비 에너지실적 달성률

    private BigDecimal nTco2PerGasGoal;           // 사내목표 대비 온실가스실적 달성률

    private BigDecimal nGjPerEngGoal;             // 사내목표 대비 에너지실적 달성률

    public String getcYear() {
        return cYear;
    }

    public BigDecimal getnYearGasGoal() {
        return nYearGasGoal;
    }

    public BigDecimal getnYearEngGoal() {
        return nYearEngGoal;
    }

    public BigDecimal getnGasGoal() {
        return nGasGoal;
    }

    public BigDecimal getnEngGoal() {
        return nEngGoal;
    }


    public BigDecimal getnTco2() {
        return nTco2;
    }

    public BigDecimal getnGj() {
        return nGj;
    }

    public BigDecimal getnTco2PerYearGasGoal() {
        return nTco2PerYearGasGoal;
    }

    public BigDecimal getnGjPerYearEngGoal() {
        return nGjPerYearEngGoal;
    }

    public BigDecimal getnTco2PerGasGoal() {
        return nTco2PerGasGoal;
    }


    public BigDecimal getnGjPerEngGoal() {
        return nGjPerEngGoal;
    }
}