package com.sebang.ems.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.sebang.ems.model.Do.TFiles;


import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Setter
@Getter
@ToString
@JsonInclude(value = Include.NON_EMPTY)
public class Gfiles extends TFiles{

    private String gId;
}