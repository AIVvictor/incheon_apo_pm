package com.sebang.ems.model.Dto;

import javax.validation.constraints.NotNull;

import com.sebang.ems.model.Do.TMrvPointGoal;
import com.sebang.ems.model.Do.TSiteYearGoal;

import lombok.Setter;
import lombok.ToString;

@Setter
@ToString
public class ReqYearGoal
{
    private Long idMrvPointGoal;

    @NotNull(message = "cYear 는 필수 입력 값입니다. ")
    private String cYear;

    private String sGasengCd;

    @NotNull(message = "nGoal 는 필수 입력 값입니다. ")
    private Float nGoal;

    @NotNull(message = "gNot 는 필수 입력 값입니다. ")
    private String gNot;

    private Long idSiteYearGoal;
    
    private Long idSite;
    
    private Long idEngPoint;
        
    private Float sMonGoalSum;
    
    private Float cYearGoalSum;

    public TMrvPointGoal toEntityMrvPointGoal(){
        TMrvPointGoal g = new TMrvPointGoal();
        g.setIdMrvPointGoal(this.idMrvPointGoal);
        g.setcYear(this.cYear);
        // g.setsGasengCd(this.sGasengCd);
        // g.setnGoal(this.nGoal);
        // g.setgNot(this.gNot);
        return g;
    }

    public TSiteYearGoal toEntitySiteYearGoal()
    {
        TSiteYearGoal s = new TSiteYearGoal();
        s.setIdSiteYearGoal(this.idSiteYearGoal);
        s.setIdSite(this.idSite);
        // s.setIdEngPoint(this.idEngPoint);
        // s.setsGasengCd(this.sGasengCd);
        s.setcYear(this.cYear);
        // s.setsMonGoalSum(this.sMonGoalSum);
        // s.setcYearGoalSum(this.cYearGoalSum);
        return s;
    }


}