package com.sebang.ems.model.Dto;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

import com.sebang.ems.model.Do.TTransCom;
import com.sebang.ems.util.StringUtil;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;
import lombok.ToString;


@ToString
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@NoArgsConstructor
@Builder
public class ReqTransCom {

    @NotNull(message="sTransComId 는 필수 입력값입니다. ")
    private String sTransComId;

    @NotNull(message="sTransComNm 는 필수 입력값입니다. ")
    private String sTransComNm;

    @Pattern(regexp = "[12]",message = "1: 정상, 2: 삭제")
    @NotNull(message="sTransComNm 는 필수 입력값입니다. ")
    private String sComStatusCd;

    @NotNull(message = "sRegDate 는 필수 입력 값입니다. ")
    private String sRegDate;
    
    @NotNull(message = "sModDate 는 필수 입력 값입니다. ")
    private String sModDate;    

    private String cDelYn;

    private String sRegId;

    private String sModId;;

    private int index;

    public String getsTransComNm() {
        return sTransComNm;
    }

    public void setsTransComNm(String sTransComNm) {
        this.sTransComNm = sTransComNm;
    }

    public String getsComStatusCd() {
        return sComStatusCd;
    }

    public void setsComStatusCd(String sComStatusCd) {
        this.sComStatusCd = sComStatusCd;
    }



    public String getcDelYn() {
        return cDelYn;
    }

    public void setcDelYn(String cDelYn) {
        this.cDelYn = cDelYn;
    }

    public String getsTransComId() {
        return sTransComId;
    }

    public void setsTransComId(String sTransComId) {
        this.sTransComId = sTransComId;
    }

    public String getsRegId() {
        return sRegId;
    }

    public void setsRegId(String sRegId) {
        this.sRegId = sRegId;
    }

    public String getsModId() {
        return sModId;
    }

    public void setsModId(String sModId) {
        this.sModId = sModId;
    }

    public String getsRegDate() {
        return sRegDate;
    }

    public void setsRegDate(String sRegDate) {
        this.sRegDate = sRegDate;
    }

    public String getsModDate() {
        return sModDate;
    }

    public void setsModDate(String sModDate) {
        this.sModDate = sModDate;
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }


    public TTransCom toEntity()
    {
        TTransCom t = new TTransCom();
        
        t.setsTransComId(this.sTransComId);
        
        if(this.getsTransComNm()!=null){
            t.setsTransComNm(this.sTransComNm.trim());
        }else
            t.setsTransComNm(this.sTransComNm);
        t.setsComStatusCd(this.sComStatusCd);

        if(!StringUtil.isEmpty(this.sComStatusCd)){
            if("1".equals(this.sComStatusCd))
                t.setcDelYn("N");
            else
                t.setcDelYn("Y");            
        };
        t.setsRegId(this.sRegId);
        t.setsModId(this.sModId); 
        return t;
    }

    


}