package com.sebang.ems.model.Dto;

import java.math.BigDecimal;

import javax.validation.constraints.NotNull;

import com.sebang.ems.model.Do.TMrvPointGoal;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;
import lombok.ToString;

@ToString
@NoArgsConstructor
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@Builder
public class ReqMrvPointGoal
{
    private Long idMrvPointGoal;
    
    @NotNull(message = "cYear 은 필수 입력 값입니다. ") 
    private String cYear;
    
    @NotNull(message = "nGasGoal 은 필수 입력 값입니다. ") 
    private BigDecimal nGasGoal;
    
    @NotNull(message = "nEngGoal 은 필수 입력 값입니다. ") 
    private BigDecimal nEngGoal;
    
    private String gMrvFile;

    public Long getIdMrvPointGoal() {
        return idMrvPointGoal;
    }

    public void setIdMrvPointGoal(Long idMrvPointGoal) {
        this.idMrvPointGoal = idMrvPointGoal;
    }

    public String getcYear() {
        return cYear;
    }

    public void setcYear(String cYear) {
        this.cYear = cYear;
    }

    public BigDecimal getnGasGoal() {
        return nGasGoal;
    }

    public void setnGasGoal(BigDecimal nGasGoal) {
        this.nGasGoal = nGasGoal;
    }

    public BigDecimal getnEngGoal() {
        return nEngGoal;
    }

    public void setnEngGoal(BigDecimal nEngGoal) {
        this.nEngGoal = nEngGoal;
    }

    public String getgMrvFile() {
        return gMrvFile;
    }

    public void setgMrvFile(String gMrvFile) {
        this.gMrvFile = gMrvFile;
    }

    public TMrvPointGoal toEntity()
    {
        TMrvPointGoal g = new TMrvPointGoal();
           g.setIdMrvPointGoal(this.idMrvPointGoal);
            g.setcYear(this.cYear);
            g.setnGasGoal(this.nGasGoal);
            g.setnEngGoal(this.nEngGoal);
            g.setgMrvFile(this.gMrvFile);
        return g;
    }
}