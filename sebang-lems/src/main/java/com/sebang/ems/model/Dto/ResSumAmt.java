package com.sebang.ems.model.Dto;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;

import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.sebang.ems.model.SumAmt;
import com.sebang.ems.model.Do.VSumAmt;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@ToString
@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@Builder
@JsonInclude(value = Include.NON_EMPTY)
public class ResSumAmt {
    @NotNull
    protected String cYear;

    @NotNull
    protected String cMon;

    protected String sDesc;

    protected BigDecimal gj;

    protected BigDecimal co2;

    protected BigDecimal ch4;

    protected BigDecimal n2o;

    protected BigDecimal tco2eq;

    protected BigDecimal toe;

    public String getcYear() {
        return cYear;
    }

    public void setcYear(String cYear) {
        this.cYear = cYear;
    }

    public String getcMon() {
        return cMon;
    }

    public void setcMon(String cMon) {
        this.cMon = cMon;
    }

    public String getsDesc()    
    {
        return sDesc;
    }

    public static List<ResSumAmt> toList(VSumAmt v) {
        List<ResSumAmt> s = Arrays.asList(ResSumAmt.toEntity1(v), ResSumAmt.toEntity2(v), ResSumAmt.toEntity3(v),
                ResSumAmt.toEntity4(v), ResSumAmt.toEntity5(v), ResSumAmt.toEntity6(v), ResSumAmt.toEntity7(v),
                ResSumAmt.toEntity8(v), ResSumAmt.toEntity9(v), ResSumAmt.toEntity10(v), ResSumAmt.toEntity11(v),
                ResSumAmt.toEntity12(v));
        return s;
    }

    public static ResSumAmt toEntity1(VSumAmt v) {
        ResSumAmt r = new ResSumAmtBuilder().cYear(v.getcYear()).cMon("1").gj(v.getM1Gj()).co2(v.getM1Co2())
                .ch4(v.getM1Ch4()).n2o(v.getM1N2o()).tco2eq(v.getM1Tco2eq()).toe(v.getM1Toe())
                .build();

        return r;
    }

    public static ResSumAmt toEntity2(VSumAmt v) {
        ResSumAmt r = new ResSumAmtBuilder().cYear(v.getcYear()).cMon("2").gj(v.getM2Gj()).co2(v.getM2Co2())
                .ch4(v.getM2Ch4()).n2o(v.getM2N2o()).tco2eq(v.getM2Tco2eq()).toe(v.getM2Toe())
                .build();

        return r;
    }

    public static ResSumAmt toEntity3(VSumAmt v) {
        ResSumAmt r = new ResSumAmtBuilder().cYear(v.getcYear()).cMon("3").gj(v.getM3Gj()).co2(v.getM3Co2())
                .ch4(v.getM3Ch4()).n2o(v.getM3N2o()).tco2eq(v.getM3Tco2eq()).toe(v.getM3Toe())
                .build();

        return r;
    }

    public static ResSumAmt toEntity4(VSumAmt v) {
        ResSumAmt r = new ResSumAmtBuilder().cYear(v.getcYear()).cMon("4").gj(v.getM4Gj()).co2(v.getM4Co2())
                .ch4(v.getM4Ch4()).n2o(v.getM4N2o()).tco2eq(v.getM4Tco2eq()).toe(v.getM4Toe())
                .build();

        return r;
    }

    public static ResSumAmt toEntity5(VSumAmt v) {
        ResSumAmt r = new ResSumAmtBuilder().cYear(v.getcYear()).cMon("5").gj(v.getM5Gj()).co2(v.getM5Co2())
                .ch4(v.getM5Ch4()).n2o(v.getM5N2o()).tco2eq(v.getM5Tco2eq()).toe(v.getM5Toe())
                .build();

        return r;
    }

    public static ResSumAmt toEntity6(VSumAmt v) {
        ResSumAmt r = new ResSumAmtBuilder().cYear(v.getcYear()).cMon("6").gj(v.getM6Gj()).co2(v.getM6Co2())
                .ch4(v.getM6Ch4()).n2o(v.getM6N2o()).tco2eq(v.getM6Tco2eq()).toe(v.getM6Toe())
                .build();

        return r;
    }

    public static ResSumAmt toEntity7(VSumAmt v) {
        ResSumAmt r = new ResSumAmtBuilder().cYear(v.getcYear()).cMon("7").gj(v.getM7Gj()).co2(v.getM7Co2())
                .ch4(v.getM7Ch4()).n2o(v.getM7N2o()).tco2eq(v.getM7Tco2eq()).toe(v.getM7Toe())
                .build();

        return r;
    }

    public static ResSumAmt toEntity8(VSumAmt v) {
        ResSumAmt r = new ResSumAmtBuilder().cYear(v.getcYear()).cMon("8").gj(v.getM8Gj()).co2(v.getM8Co2())
                .ch4(v.getM8Ch4()).n2o(v.getM8N2o()).tco2eq(v.getM8Tco2eq()).toe(v.getM8Toe())
                .build();

        return r;
    }

    public static ResSumAmt toEntity9(VSumAmt v) {
        ResSumAmt r = new ResSumAmtBuilder().cYear(v.getcYear()).cMon("9").gj(v.getM9Gj()).co2(v.getM9Co2())
                .ch4(v.getM9Ch4()).n2o(v.getM9N2o()).tco2eq(v.getM9Tco2eq()).toe(v.getM9Toe())
                .build();

        return r;
    }

    public static ResSumAmt toEntity10(VSumAmt v) {
        ResSumAmt r = new ResSumAmtBuilder().cYear(v.getcYear()).cMon("10").gj(v.getM10Gj()).co2(v.getM10Co2())
                .ch4(v.getM10Ch4()).n2o(v.getM10N2o()).tco2eq(v.getM10Tco2eq()).toe(v.getM10Toe())
                .build();

        return r;
    }

    public static ResSumAmt toEntity11(VSumAmt v) {
        ResSumAmt r = new ResSumAmtBuilder().cYear(v.getcYear()).cMon("11").gj(v.getM11Gj()).co2(v.getM11Co2())
                .ch4(v.getM11Ch4()).n2o(v.getM11N2o()).tco2eq(v.getM11Tco2eq()).toe(v.getM11Toe())
                .build();

        return r;
    }

    public static ResSumAmt toEntity12(VSumAmt v) {
        ResSumAmt r = new ResSumAmtBuilder().cYear(v.getcYear()).cMon("12").gj(v.getM12Gj()).co2(v.getM12Co2())
                .ch4(v.getM12Ch4()).n2o(v.getM12N2o()).tco2eq(v.getM12Tco2eq()).toe(v.getM12Toe())
                .build();

        return r;
    }

    public static ResSumAmt toEntity(SumAmt v) {
        ResSumAmt r = new ResSumAmtBuilder()
                .cYear(v.getCYear())
                .cMon(v.getCMon())
                .gj(v.getGj())
                .co2(v.getCo2())
                .ch4(v.getCh4())
                .n2o(v.getN2o())
                .tco2eq(v.getTco2eq())
                .toe(v.getToe()) 
                .sDesc(v.getSDesc())
                .build();
        return r;
    }

}