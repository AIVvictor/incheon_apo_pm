package com.sebang.ems.model;

import java.util.Date;

import javax.validation.constraints.NotBlank;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.sebang.ems.model.Do.TErpDept;
import com.sebang.ems.model.Do.TSiteInfo;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;
import lombok.ToString;

@ToString
@NoArgsConstructor
@AllArgsConstructor(access=AccessLevel.PRIVATE)
@JsonInclude(value = Include.NON_EMPTY)
@Builder
public class Employee
{
    private Long idEmp;

    private Long idErpUsers;

    @NotBlank(message = "사번는 필수 입력값입니다.")
    private String sEmpId;

    private String sEmpPwd;

   // @NotBlank(message = "사원명은 필수 입력값입니다.")
    private String sEmpNm;

    private Long idErpDept;

    private Long idSite;

    private Date cSDate;

    private Date cEDate;

    @NotBlank(message = "이메일은 필수 입력값입니다.")
    private String sEmpMail;

    private String cEmpHp;

    /**
     * 권한코드
     */
    private String cAuthCode;

    /**
     * 권한명
     */
    private String sAuthType;
    
    private String cRankCd;

    private String cRankNm;

    /**
     * 상태코드: 1=재직,2=퇴사
     */
    private String sStatusCd;
    
    /** 
     * 2019/11/20 added by richard
     * 
     * 상태명: 1=재직,2=퇴사
     */
    private String sStatusNm;

    private String sRegId;

    private Date dRegDate;

    private String sModId;

    private Date dModDate;

    private String cDelYn;

    private TSiteInfo site;

    private TErpDept dept;
    
     /** 
     * 2019/11/28 added by cm.lee
     * 변경 할 평문 암호
     */
    private String sNewPwd;
    
    public Long getIdEmp() {
        return idEmp;
    }

    public void setIdEmp(Long idEmp) {
        this.idEmp = idEmp;
    }

    public Long getIdErpUsers() {
        return idErpUsers;
    }

    public void setIdErpUsers(Long idErpUsers) {
        this.idErpUsers = idErpUsers;
    }

    public String getsEmpId() {
        return sEmpId;
    }

    public void setsEmpId(String sEmpId) {
        this.sEmpId = sEmpId;
    }

    public String getsEmpPwd() {
        return sEmpPwd;
    }

    public void setsEmpPwd(String sEmpPwd) {
        this.sEmpPwd = sEmpPwd;
    }

    public String getsEmpNm() {
        return sEmpNm;
    }

    public void setsEmpNm(String sEmpNm) {
        this.sEmpNm = sEmpNm;
    }

    public Long getIdErpDept() {
        return idErpDept;
    }

    public void setIdErpDept(Long idErpDept) {
        this.idErpDept = idErpDept;
    }

    public Long getIdSite() {
        return idSite;
    }

    public void setIdSite(Long idSite) {
        this.idSite = idSite;
    }

    public Date getcSDate() {
        return cSDate;
    }

    public void setcSDate(Date cSDate) {
        this.cSDate = cSDate;
    }

    public Date getcEDate() {
        return cEDate;
    }

    public void setcEDate(Date cEDate) {
        this.cEDate = cEDate;
    }

    public String getsEmpMail() {
        return sEmpMail;
    }

    public void setsEmpMail(String sEmpMail) {
        this.sEmpMail = sEmpMail;
    }

    public String getcEmpHp() {
        return cEmpHp;
    }

    public void setcEmpHp(String cEmpHp) {
        this.cEmpHp = cEmpHp;
    }

	public String getcAuthCode() {
        return cAuthCode;
    }

    public void setcAuthCode(String cAuthCode) {
        this.cAuthCode = cAuthCode;
    }

    public String getcRankCd() {
        return cRankCd;
    }

    public void setcRankCd(String cRankCd) {
        this.cRankCd = cRankCd;
    }

    public String getcRankNm() {
        return cRankNm;
    }

    public void setcRankNm(String cRankNm) {
        this.cRankNm = cRankNm;
    }

    public String getsRegId() {
        return sRegId;
    }

    public void setsRegId(String sRegId) {
        this.sRegId = sRegId;
    }

    public Date getdRegDate() {
        return dRegDate;
    }

    public void setdRegDate(Date dRegDate) {
        this.dRegDate = dRegDate;
    }

    public String getsModId() {
        return sModId;
    }

    public void setsModId(String sModId) {
        this.sModId = sModId;
    }

    public Date getdModDate() {
        return dModDate;
    }

    public void setdModDate(Date dModDate) {
        this.dModDate = dModDate;
    }

    public String getcDelYn() {
        return cDelYn;
    }

    public void setcDelYn(String cDelYn) {
        this.cDelYn = cDelYn;
    }

    public TSiteInfo getSite() {
        return site;
    }

    public TErpDept getDept() {
        return dept;
    }

    public void setSite(TSiteInfo site) {
        this.site = site;
    }

    public void setDept(TErpDept dept) {
        this.dept = dept;
    }

    public String getsStatusCd() {
        return sStatusCd;
    }

    public void setsStatusCd(String sStatusCd) {
        this.sStatusCd = sStatusCd;
    }

	public String getsStatusNm() {
		return sStatusNm;
	}

	public void setsStatusNm(String sStatusNm) {
		this.sStatusNm = sStatusNm;
	}

	public String getsAuthType() {
		return sAuthType;
	}

	public void setsAuthType(String sAuthType) {
		this.sAuthType = sAuthType;
	}

    public String getsNewPwd() {
        return sNewPwd;
    }

    public void setsNewPwd(String sNewPwd) {
        this.sNewPwd = sNewPwd;
    }   
}