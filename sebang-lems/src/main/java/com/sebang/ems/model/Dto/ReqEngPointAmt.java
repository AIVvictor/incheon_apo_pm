package com.sebang.ems.model.Dto;

import java.math.BigDecimal;

import javax.validation.constraints.NotNull;

import com.sebang.ems.model.Do.TEngPointAmt;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;
import lombok.ToString;

@ToString()
@NoArgsConstructor
@AllArgsConstructor(access=AccessLevel.PRIVATE)
@Builder
public class ReqEngPointAmt
{
    private Long idEngPointAmt;

    @NotNull(message = "idEngPoint 는 필수 입력값입니다.")
    private Long idEngPoint;

    // @NotNull(message = "idErpDept는 필수 입력값입니다.")
    private Long idErpDept;

    @NotNull(message = "cYear 는 필수 입력값입니다.")
    private String cYear;

    @NotNull(message = "cMon 는 필수 입력값입니다.")
    private String cMon;

    @NotNull(message = "nUseAmt 는 필수 입력값입니다.")
    private BigDecimal nUseAmt;

    private BigDecimal nCost;

    private BigDecimal nEngEtc;

    private String nEngEtcUntCd;

    private String gBillId;

    private String sRegId;

    private String sModId;

    private String cDelYn;

    private BigDecimal nGj;

    private BigDecimal nTco2;

    private BigDecimal sBaseVal;

    public Long getIdEngPointAmt() {
        return idEngPointAmt;
    }

    public Long getIdEngPoint() {
        return idEngPoint;
    }

    public String getcYear() {
        return cYear;
    }

    public void setcYear(String cYear) {
        this.cYear = cYear;
    }

    public String getcMon() {
        return cMon;
    }

    public void setcMon(String cMon) {
        this.cMon = cMon;
    }

    public BigDecimal getnUseAmt() {
        return nUseAmt;
    }

    public void setnUseAmt(BigDecimal nUseAmt) {
        this.nUseAmt = nUseAmt;
    }

    public BigDecimal getnCost() {
        return nCost;
    }

    public void setnCost(BigDecimal nCost) {
        this.nCost = nCost;
    }

    public String getgBillId() {
        return gBillId;
    }

    public void setgBillId(String gBillId) {
        this.gBillId = gBillId;
    }

    public String getsRegId() {
        return sRegId;
    }

    public void setsRegId(String sRegId) {
        this.sRegId = sRegId;
    }

    public String getsModId() {
        return sModId;
    }

    public void setsModId(String sModId) {
        this.sModId = sModId;
    }

    public String getcDelYn() {
        return cDelYn;
    }

    public void setcDelYn(String cDelYn) {
        this.cDelYn = cDelYn;
    }

    public BigDecimal getnGj() {
        return nGj;
    }

    public void setnGj(BigDecimal sGj) {
        this.nGj = nGj;
    }

    public BigDecimal getnTco2() {
        return nTco2;
    }

    public void setnTco2(BigDecimal sTco2) {
        this.nTco2 = nTco2;
    }

    public void setIdEngPointAmt(Long idEngPointAmt) {
        this.idEngPointAmt = idEngPointAmt;
    }

    public void setIdEngPoint(Long idEngPoint) {
        this.idEngPoint = idEngPoint;
    }

    public Long getIdErpDept() {
        return idErpDept;
    }

    public void setIdErpDept(Long idErpDept) {
        this.idErpDept = idErpDept;
    }

    public BigDecimal getsBaseVal() {
        return sBaseVal;
    }

    public void setsBaseVal(BigDecimal sBaseVal) {
        this.sBaseVal = sBaseVal;
    }
    
 

    public BigDecimal getnEngEtc() {
        return nEngEtc;
    }

    public void setnEngEtc(BigDecimal nEngEtc) {
        this.nEngEtc = nEngEtc;
    }

    public String getnEngEtcUntCd() {
        return nEngEtcUntCd;
    }

    public void setnEngEtcUntCd(String nEngEtcUntCd) {
        this.nEngEtcUntCd = nEngEtcUntCd;
    }

    public TEngPointAmt toEntity()
    {
        TEngPointAmt a = new TEngPointAmt();

        a.setIdEngPointAmt(this.idEngPointAmt);
        a.setIdEngPoint(this.idEngPoint);
        a.setIdErpDept(this.idErpDept);
        a.setcYear(this.cYear);
        a.setcMon(this.cMon);
        a.setnUseAmt(this.nUseAmt);
        a.setnCost(this.nCost);
        a.setnEngEtc(this.nEngEtc);
        a.setnEngEtcUntCd(this.nEngEtcUntCd);
        a.setgBillId(this.gBillId);
        a.setsRegId(this.sRegId);
        a.setsModId(this.sModId);
        a.setcDelYn(this.cDelYn);
        a.setnGj(this.nGj);
        a.setnTco2(this.nTco2);
        a.setsBaseVal(this.getsBaseVal());

        return a;
    }

}