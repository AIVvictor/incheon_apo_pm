package com.sebang.ems.util;

//import com.sebang.ems.config.AppConfig;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Environment {
    public static final Logger logger = LoggerFactory.getLogger(Environment.class);

	static Environment self = new Environment();

	public static Environment instance() {
		return self;
	}
	

    public void load() {
		//setV2xServerConnect(AppConfig.instance().getPropertyBool("debug.useV2Xconnect", true));
		//logger.info("DEBUG OPTIONS: debug.useV2Xconnect:             :{}", isV2xServerConnect());
	}
}