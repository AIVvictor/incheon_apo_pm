package com.sebang.ems.util;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.Reader;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;

import com.opencsv.bean.ColumnPositionMappingStrategy;
import com.opencsv.bean.CsvToBean;
import com.opencsv.bean.CsvToBeanBuilder;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class CsvToObject<T> {

    public static final Logger logger = LoggerFactory.getLogger(CsvToObject.class);

    public class CsvTransfer<T> {

        private List<String[]> csvStringList;

        private List<T> csvList;

        public CsvTransfer() {
        }

        public List<String[]> getCsvStringList() {
            if (csvStringList != null)
                return csvStringList;
            return new ArrayList<String[]>();
        }

        public void addLine(String[] line) {
            if (this.csvList == null)
                this.csvStringList = new ArrayList<>();
            this.csvStringList.add(line);
        }

        public void setCsvStringList(List<String[]> csvStringList) {
            this.csvStringList = csvStringList;
        }

        public void setCsvList(List<T> csvList) {
            this.csvList = csvList;
        }

        public List<T> getCsvList() {
            if (csvList != null)
                return csvList;
            return new ArrayList<T>();
        }

    }

    public void convertToUtf8(Path source,Path target) throws IOException {
      String s = source.toAbsolutePath().toString();
      String o = target.toAbsolutePath().toString();

        BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(s), "MS949"));
        BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(o), "UTF-8"));

        String line;
        try {

            while ((line = br.readLine()) != null) {
                String utf8 = new String(line.getBytes(), "UTF-8");
                logger.debug(utf8);
                bw.write(utf8);
                bw.newLine();
            }
            bw.flush();

        } catch (Exception e) {
            logger.info("Done!" + "charset:" + Charset.forName("utf-8"));
        } finally {
            br.close();
            bw.close();
        }
    }


    public List<T> build(Path path, Class clazz,Charset cs) throws Exception {
        CsvTransfer<T> csvTransfer = new CsvTransfer<T>();
        ColumnPositionMappingStrategy ms = new ColumnPositionMappingStrategy();
        ms.setType(clazz);    

        Reader reader = Files.newBufferedReader(path, cs);
        CsvToBean cb = new CsvToBeanBuilder(reader).withType(clazz).withMappingStrategy(ms).build();

        csvTransfer.setCsvList(cb.parse());
        reader.close();
        return csvTransfer.getCsvList();
    }

}