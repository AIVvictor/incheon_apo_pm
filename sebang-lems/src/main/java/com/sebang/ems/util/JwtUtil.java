package com.sebang.ems.util;

import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.exceptions.JWTCreationException;
import com.auth0.jwt.exceptions.JWTDecodeException;
import com.auth0.jwt.exceptions.JWTVerificationException;
import com.auth0.jwt.interfaces.DecodedJWT;
import com.sebang.ems.auth.jwt.JwtInfo;

import org.springframework.security.core.userdetails.UserDetails;

import java.util.Date;

public abstract class JwtUtil {

	
	/** 토큰을 생성한다. (만료일 무제한)
	 * @param userDetails 사용자 인증 정보
	 * @return String 토큰
	 */
	public static String createToken(UserDetails userDetails) {
		return createToken(userDetails, DateUtil.nowAfterDaysToDate(JwtInfo.EXPIRES_LIMIT));
	}
	
	
	/** 토큰을 생성한다. 
	 * @param userDetails 사용자 인증정보
	 * @param date 만료일
	 * @return String 토큰
	 */
	private static String createToken(UserDetails userDetails, Date date) {
		try {
			return JWT.create()
					.withIssuer(JwtInfo.ISSUER)
					.withClaim("id", userDetails.getUsername())
					.withClaim("role", userDetails.getAuthorities().toArray()[0].toString())
					.withExpiresAt(date)
					.sign(JwtInfo.getAlgorithm());
		} catch (JWTCreationException createEx) {
			return null;
		}
	}
	
	
	/** 토큰 검증. 
	 * @param token JWT 문자열
	 * @return Boolean 인증 여부
	 */
	public static Boolean verify(String token) {
		try {
			JWTVerifier verifier = JWT.require(JwtInfo.getAlgorithm()).build();
			verifier.verify(token);
			
			return Boolean.TRUE;
		} catch (JWTVerificationException verifyEx) {
			return Boolean.FALSE;
		}
	}

	
	/**  갱신토큰을 생성
	 * @param userDetails 사용자 인증 정보
	 * @return String 토큰
	 */
	public static String refreshToken(UserDetails userDetails) {
		return createToken(userDetails, DateUtil.nowAfterDaysToDate(JwtInfo.EXPIRES_LIMIT));
	}
	
	
	/** 토큰을 디코딩
	 * @param token 토큰 문자열
	 * @return DecodedJWT 토큰 정보.
	 */
	public static DecodedJWT tokenToJwt(String token) {
		try {
			return JWT.decode(token);
		} catch (JWTDecodeException decodeEx) {
			return null;
		}
	}
}