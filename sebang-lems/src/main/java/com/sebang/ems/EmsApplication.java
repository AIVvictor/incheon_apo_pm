package com.sebang.ems;

import com.sebang.ems.storage.StorageProperties;
import com.sebang.ems.storage.StorageService;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Profile;

@SpringBootApplication
@EnableConfigurationProperties(StorageProperties.class)
public class EmsApplication {

	public static void main(String[] args) {
		SpringApplication.run(EmsApplication.class, args);
	}

    //참고 https://cnpnote.tistory.com/entry/SPRING-JUnit-%ED%85%8C%EC%8A%A4%ED%8A%B8-%EC%A4%91-Application-CommandLineRunner-%ED%81%B4%EB%9E%98%EC%8A%A4-%EC%8B%A4%ED%96%89-%EB%B0%A9%EC%A7%80
    @Bean
    @Profile("!test")
    CommandLineRunner init(StorageService storageService) {
        return (args) -> {
         //   storageService.deleteAll();
            storageService.init();
        };
    }

}
