package com.sebang.ems.component;

import java.util.List;

import com.sebang.ems.dao.UsersMapper;
import com.sebang.ems.model.Do.TUsers;
//import com.sebang.ems.repository.MemberRepository;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

@Component
public class InitComponent implements ApplicationRunner {

	public static final Logger logger = LoggerFactory.getLogger(InitComponent.class);

	@Autowired
	private UsersMapper usersMapper;


	@Autowired
	private PasswordEncoder passwordEncoder;

	@Transactional
	@Override
	public void run(ApplicationArguments args) {

		logger.info("app run ");

		List<TUsers> u =usersMapper.select();
		if(u.size() == 0){
			TUsers user = new TUsers();
			user.setsEmpId("tiger");
			user.setsEmpNm("tiger");
			user.setsEmpPwd(passwordEncoder.encode("1234"));
			user.setcAuthCode("IUSR");
			usersMapper.insert(user);

			user.setsEmpId("naruto");
			user.setsEmpNm("naruto");
			user.setcAuthCode("SYS");
			usersMapper.insert(user);

		}
	}
}