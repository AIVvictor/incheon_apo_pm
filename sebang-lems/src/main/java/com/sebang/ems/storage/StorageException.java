package com.sebang.ems.storage;

public class StorageException extends RuntimeException {


    private static final long serialVersionUID = 5395127024424481374L;

    public StorageException(String message) {
        super(message);
    }

    public StorageException(String message, Throwable cause) {
        super(message, cause);
    }
}
