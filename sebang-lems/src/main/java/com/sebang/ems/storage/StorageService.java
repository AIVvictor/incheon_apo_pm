package com.sebang.ems.storage;

import org.springframework.core.io.Resource;
import org.springframework.web.multipart.MultipartFile;

import java.nio.file.Path;
import java.util.stream.Stream;

public interface StorageService {

    void init();

    void store(MultipartFile file, String subPath);

    Stream<Path> loadAll();

    Path load(String filename);

    Resource loadAsResource(String filename, String subPath);

    void deleteAll();

    void delete(String path ,String filename );

}