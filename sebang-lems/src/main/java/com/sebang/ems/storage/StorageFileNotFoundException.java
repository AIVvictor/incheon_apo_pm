package com.sebang.ems.storage;

public class StorageFileNotFoundException extends StorageException {

    private static final long serialVersionUID = -2469942467597341795L;

    public StorageFileNotFoundException(String message) {
        super(message);
    }

    public StorageFileNotFoundException(String message, Throwable cause) {
        super(message, cause);
    }
}