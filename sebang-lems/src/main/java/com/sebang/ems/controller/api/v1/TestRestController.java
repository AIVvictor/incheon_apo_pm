package com.sebang.ems.controller.api.v1;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import com.sebang.ems.component.ExcelReader;
import com.sebang.ems.controller.BaseRestController;
import com.sebang.ems.domain.common.FieldErrorDetail;
import com.sebang.ems.domain.common.ResultCode;
import com.sebang.ems.domain.common.ResultPage;
import com.sebang.ems.domain.common.ResultValue;
import com.sebang.ems.domain.common.SearchParam;
import com.sebang.ems.model.Product;
import com.sebang.ems.view.ExcelView;

import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.ui.Model;
import org.springframework.validation.BindException;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.View;


@CrossOrigin(origins = "*")
@RestController
@RequestMapping("/api/v1")
public class TestRestController extends BaseRestController{

    public static final Logger logger = LoggerFactory.getLogger(TestRestController.class);

    @Autowired
    private ExcelReader excelReader;
	
    @CrossOrigin(origins = "*")
    @RequestMapping(value="/test",method = RequestMethod.GET,consumes = MediaType.APPLICATION_JSON_VALUE)    
	public ResponseEntity<ResultPage> test( HttpServletRequest request,
											@Valid  @ModelAttribute SearchParam reqParam,
											BindingResult bindingResult ) throws org.springframework.validation.BindException 
    { 
		logger.info("test=>"+this.convertString(reqParam));

		String [] codes = new String[1];
		Object [] args = new Object[1];
		args[0] = reqParam.getCMonEnd();
		codes[0]="NotNull";
		FieldError e = new FieldError("searchParam","cMonEng","cMonEng는 필수 입력 값입니다. ");
		
		bindingResult.addError(e);
	
		if(bindingResult.hasErrors())
		{
			bindingResult.getFieldErrorCount();
			logger.info("test=1"+bindingResult.getFieldErrorCount());
			List<FieldError> re=bindingResult.getFieldErrors();
			for(FieldError ex :re){
				
				logger.info("test=1"+ex.getDefaultMessage());
				logger.info("test=2"+ex.getField());	
			}

			throw new BindException(bindingResult);		
		//	logger.info("test=1"+bindingResult.getAllErrors());

			//return new ResponseEntity<ResultPage>(new ResultPage(ResultCode.FAILED,null,0,0,10), HttpStatus.OK);
    	
		}	
		return new ResponseEntity<ResultPage>(new ResultPage(ResultCode.SUCCESS,null,0,0,10), HttpStatus.OK);
	}
	
	@CrossOrigin(origins = "*")
    @RequestMapping(value="/test2",method = RequestMethod.GET,consumes = MediaType.APPLICATION_JSON_VALUE)    
	public ResponseEntity<ResultPage> test2( HttpServletRequest request,
											@Valid  @ModelAttribute SearchParam reqParam) 
    { 
		logger.info("test=>"+reqParam);
	
		return new ResponseEntity<ResultPage>(new ResultPage(ResultCode.SUCCESS,null,0,0,10), HttpStatus.OK);
	}
	

    @RequestMapping(value = "/excel", method = RequestMethod.POST) 
	public View viewExcel(HttpServletRequest request, HttpServletResponse response,Model model) { 
		
		logger.debug("viewExcel");
		// 임의의 데이터를 만듬. 
		List<String> listData = new ArrayList<String>(); 
		listData.add("홍길동"); 
		listData.add("나그네"); 
		listData.add("홍길동"); 
		listData.add("홍길동"); 
		listData.add("홍길동"); 
		listData.add("홍길동"); 
		listData.add("홍길동"); 
		
		// 차트를 만들기 위한 통계자료도 구한다. 
		List<Map<String, Object>> listStat = new ArrayList<Map<String, Object>>(); 
		Map<String, Object> mapStat = new HashMap<String, Object>(); 
		mapStat.put("name", "홍길동"); 
		mapStat.put("count", 6); 
		listStat.add(mapStat); 
		mapStat = new HashMap<String, Object>(); 
		mapStat.put("name", "나그네"); 
		mapStat.put("count", 1); 
		listStat.add(mapStat); 
		
		// 데이터를 담는 다. 
		model.addAttribute("data", listData); 
		model.addAttribute("stat", listStat); 
        model.addAttribute("viewname","templates/excel/excel.xls");
        model.addAttribute("output","result.xls");
		// 엑셀을 출력한다. 
		return new ExcelView(); 
		
	}

	@PostMapping("/excel/all")
	public List<Product> readExcel(@RequestParam("file") MultipartFile multipartFile) throws IOException, InvalidFormatException {
		logger.info("readExcel");
        List<Product> ret = excelReader.readFileToList(multipartFile, Product::from);
        
		return ret;
	}

	// @ExceptionHandler(BindException.class)
	// @ResponseStatus(HttpStatus.BAD_REQUEST)
	// public ValidationResult handleBindException(BindException bindException, Locale locale) {
	//    return ValidationResult.create(bindException, messageSource, locale);
	// }

	@ExceptionHandler(BindException.class)
	public ResponseEntity<?> handleValidationExceptions(BindException ex) {
	
		BindingResult bindingResult = (BindingResult)ex;

		bindingResult.getFieldErrorCount();
		logger.info("test=1"+bindingResult.getFieldErrorCount());
		List<FieldError> re=bindingResult.getFieldErrors();
		for(FieldError f :re){
			
			logger.info("test=1"+f.getDefaultMessage());
			logger.info("test=2"+f.getField());	
			
		}

		List<FieldErrorDetail> details =
		bindingResult.getFieldErrors().stream()
					.map(error->FieldErrorDetail.create(error,error.getDefaultMessage()))
					.collect(Collectors.toList());

		return new ResponseEntity<ResultValue>(new ResultValue(ResultCode.INVALID_INPUT_FIELD,details), HttpStatus.OK);
	}


}