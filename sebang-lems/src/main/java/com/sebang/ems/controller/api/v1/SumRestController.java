package com.sebang.ems.controller.api.v1;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import com.sebang.ems.domain.common.ResultCode;
import com.sebang.ems.domain.common.ResultValue;
import com.sebang.ems.domain.common.SearchParam;
import com.sebang.ems.model.Do.VBaseUntAmt;
import com.sebang.ems.model.Dto.ResSumAmt;
import com.sebang.ems.service.AmtServiceImpl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindException;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@CrossOrigin(origins = "*")
@RestController
@RequestMapping("/api/v1")
public class SumRestController {

    public static final Logger logger = LoggerFactory.getLogger(SumRestController.class);

    @Autowired
    AmtServiceImpl amtService;

    // 전체 실적 조회
    @RequestMapping(value = "/sum/111/ls", method = RequestMethod.GET, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> getSum111Ls(HttpServletRequest request, @ModelAttribute SearchParam reqParam) {
        logger.info("getSum111Ls() RequestParam:" + reqParam);
        ResultCode resCode = ResultCode.SUCCESS;

        Map<String, Map<String, List<ResSumAmt>>> resMap = amtService.searchTotalSumAmt(reqParam);
        if (resMap.size() == 0) {
            resCode = ResultCode.NO_DATA;
        }

        return new ResponseEntity<ResultValue>(new ResultValue(resCode, resMap), HttpStatus.OK);
    }

    // 연도별 조회
    @RequestMapping(value = "/sum/121/ls", method = RequestMethod.GET, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> getSum121Ls(HttpServletRequest request, @ModelAttribute SearchParam reqParam) {
        logger.info("getSum121Ls() RequestParam:" + reqParam);
        ResultCode resCode = ResultCode.SUCCESS;

        if (reqParam.getCYear() != null) {
            String s = reqParam.getCYear();
            reqParam.setArrYear(s.split(","));
        }

        Map<String, List<ResSumAmt>> resMap = amtService.searchYearSumAmt(reqParam);
        if (resMap.size() == 0)
            resCode = ResultCode.NO_DATA;

        return new ResponseEntity<ResultValue>(new ResultValue(resCode, resMap), HttpStatus.OK);
    }

    // 물류원 단위 조회
    @RequestMapping(value = "/sum/131/ls", method = RequestMethod.GET, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> getSum131Ls(HttpServletRequest request, @ModelAttribute SearchParam reqParam,
            BindingResult bindingResult) throws BindException {
        logger.info("getSum131Ls() RequestParam:" + reqParam);
        ResultCode resCode = ResultCode.SUCCESS;

        if (reqParam.getCYear() == null) {
            FieldError e = new FieldError("SearchParam", "cYear", "cYear 은 필수 입력값입니다. ");
            bindingResult.addError(e);
        }

        if (reqParam.getSBaseUnt() == null) {
            FieldError e = new FieldError("SearchParam", "sBaseUnt", "sBaseUnt 은 필수 입력값입니다. ");
            bindingResult.addError(e);
        }

//        if (reqParam.getSLineCd() == null) {
//            FieldError e = new FieldError("SearchParam", "sLineCd", "sLineCd 은 필수 입력값입니다. ");
//            bindingResult.addError(e);
//        }
        
        if (reqParam.getSSelfvrnCd() == null) {
            FieldError e = new FieldError("SearchParam", "sSelfvrnCd", "sSelfvrnCd 은 필수 입력값입니다. ");
            bindingResult.addError(e);
        }
        

        if (bindingResult.hasErrors()) {
            throw new BindException(bindingResult);
        }
      
        String s = reqParam.getCYear();
        reqParam.setArrYear(s.split(","));

        reqParam.setSLogiTypeCd("1");
        List<VBaseUntAmt> resList = amtService.searchLogisBaseUntAnalysis(reqParam);
        if (resList.size() == 0)
            resCode = ResultCode.NO_DATA;

        return new ResponseEntity<ResultValue>(new ResultValue(resCode, resList), HttpStatus.OK);
    }
}