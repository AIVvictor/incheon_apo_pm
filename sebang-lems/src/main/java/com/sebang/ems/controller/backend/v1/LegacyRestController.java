package com.sebang.ems.controller.backend.v1;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import com.sebang.ems.component.validator.CustomCollectionValidator;
import com.sebang.ems.config.AppConfig;
import com.sebang.ems.controller.BaseRestController;
import com.sebang.ems.domain.Exception.InvalidParamException;
import com.sebang.ems.domain.Exception.NotAllowedAddrException;
import com.sebang.ems.domain.common.FieldErrorDetail;
import com.sebang.ems.domain.common.FieldErrorEx;
import com.sebang.ems.domain.common.ResultCode;
import com.sebang.ems.domain.common.ResultValue;
import com.sebang.ems.model.Do.TTransCom;
import com.sebang.ems.model.Do.TVehicle;
import com.sebang.ems.model.Dto.ReqLogisTransInfo;
import com.sebang.ems.model.Dto.ReqTransCom;
import com.sebang.ems.model.Dto.ReqVehicle;
import com.sebang.ems.service.LogisServiceImpl;
import com.sebang.ems.util.StringUtil;

import org.hibernate.validator.internal.engine.ConstraintViolationImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindException;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@CrossOrigin(origins = "*")
@RestController
@RequestMapping("/backend/v1")
public class LegacyRestController extends BaseRestController {

    public static final Logger logger = LoggerFactory.getLogger(LegacyRestController.class);

    @Autowired
    LogisServiceImpl logisService;

    @Autowired
    CustomCollectionValidator customCollectionValidator;

    // 용차 등록
    @RequestMapping(value = "/vehicle/ls", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ResultValue> postVehicleLs(HttpServletRequest request,
            @Valid @RequestBody List<ReqVehicle> reqParam, BindingResult bindingResult) throws BindException {
        String clientIp = getRemoteAddr(request);
        logger.info("clientIp:" + clientIp + "," + " postVehicleLs=> " + convertString(reqParam));

        String allows = AppConfig.instance().getProperty("legacy.ip.allows");
        if (allows != null) {
            String[] whites = allows.split(",");

            if (!StringUtil.contains(whites, clientIp))
                throw new NotAllowedAddrException("Not Allowed IP:" + clientIp);
        }

        int i = 0;
        for (ReqVehicle rv : reqParam)
        {
            rv.setsModId("SYS");
            rv.setIndex(i++);
        }
        
        customCollectionValidator.validate(reqParam, bindingResult);
        if (bindingResult.hasErrors())
            throw new BindException(bindingResult);

        logisService.validateForVehicle(reqParam, bindingResult);            
        if (bindingResult.hasErrors())
            throw new InvalidParamException(bindingResult);

        logisService.upsertVehicles(reqParam);

        return new ResponseEntity<ResultValue>(new ResultValue(ResultCode.SUCCESS, null), HttpStatus.OK);
    }

    // 운수사 등록
    @RequestMapping(value = "/transcom/ls", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ResultValue> postTransComLs(HttpServletRequest request,
            @Valid @RequestBody List<ReqTransCom> reqParam, BindingResult bindingResult) throws BindException {
        String clientIp = getRemoteAddr(request);
        logger.info("clientIp:" + clientIp + "," + "postTransComLs=>" + convertString(reqParam));

        String allows = AppConfig.instance().getProperty("legacy.ip.allows");
        if (allows != null) {
            String[] whites = allows.split(",");

            if (!StringUtil.contains(whites, clientIp))
                throw new NotAllowedAddrException("Not Allowed IP:" + clientIp);
        }

        int i = 0;
        for (ReqTransCom rv : reqParam)
            rv.setIndex(i++);

        customCollectionValidator.validate(reqParam, bindingResult);

        if (bindingResult.hasErrors())
            throw new BindException(bindingResult);

        for (ReqTransCom v : reqParam)
            v.setsModId("SYS");

        logisService.upsertTransComs(reqParam);

        return new ResponseEntity<ResultValue>(new ResultValue(ResultCode.SUCCESS, null), HttpStatus.OK);
    }

    // 운행정보 등록
    @RequestMapping(value = "/drivinginfo/ls", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ResultValue> postDrivinginfoLs(HttpServletRequest request,
            @Valid @RequestBody List<ReqLogisTransInfo> reqParam, BindingResult bindingResult) throws BindException {
        String clientIp = getRemoteAddr(request);
        logger.info("clientIp:" + clientIp + "," + "postDrivinginfoLs=>" + convertString(reqParam));

        String allows = AppConfig.instance().getProperty("legacy.ip.allows");
        if (allows != null) {
            String[] whites = allows.split(",");

            if (!StringUtil.contains(whites, clientIp))
                throw new NotAllowedAddrException("Not Allowed IP:" + clientIp);
        }

        int i = 0;
        for (ReqLogisTransInfo rv : reqParam){
            rv.setsModId("SYS");
            rv.setIndex(i++);
        }

        customCollectionValidator.validate(reqParam, bindingResult);

        if (bindingResult.hasErrors())
            throw new BindException(bindingResult);

        logisService.validateForTransInfo(reqParam, bindingResult);
        if (bindingResult.hasErrors())
            throw new InvalidParamException(bindingResult);

        logisService.upsertLogisTransInfos(reqParam);

        return new ResponseEntity<ResultValue>(new ResultValue(ResultCode.SUCCESS, null), HttpStatus.OK);
    }

    @ExceptionHandler(BindException.class)
    public ResponseEntity<?> handleBindException(BindException ex) {
        BindingResult bindingResult = (BindingResult) ex;

        bindingResult.getFieldErrorCount();
        logger.debug("Field Error Count : " + bindingResult.getFieldErrorCount());
        List<FieldError> re = bindingResult.getFieldErrors();
        List<FieldErrorDetail> details = new ArrayList<FieldErrorDetail>();
        for (FieldError f : re) {

            logger.debug("Default Message : " + f.getDefaultMessage());
            logger.debug("Field : " + f.getField());
            Integer index = null;
            String id = null;

            ConstraintViolationImpl c = f.unwrap(ConstraintViolationImpl.class);
            Object o = c.getRootBean();
            if (ReqVehicle.class == o.getClass()) {
                ReqVehicle v = (ReqVehicle) o;
                index = v.getIndex();
                id = v.getsVrn();
            }
            if (ReqTransCom.class == o.getClass()) {
                ReqTransCom v = (ReqTransCom) o;
                index = v.getIndex();
                id = v.getsTransComNm();
            }
            if (ReqLogisTransInfo.class == o.getClass()) {
                ReqLogisTransInfo v = (ReqLogisTransInfo) o;
                index = v.getIndex();
                id = v.getsVhclDispld();
            }

            // logger.debug("source : " + o);
            FieldErrorDetail fd = FieldErrorDetail.create(index, id, f, f.getDefaultMessage());
            details.add(fd);
        }
        return new ResponseEntity<ResultValue>(new ResultValue(ResultCode.INVALID_INPUT_FIELD, details), HttpStatus.OK);
    }

    @ExceptionHandler(NotAllowedAddrException.class)
    public ResponseEntity<?> NotAllowedAddrExceptionException() {
        return ResponseEntity.notFound().build();
    }

    @ExceptionHandler(InvalidParamException.class)
    public ResponseEntity<?> handleInvalidParamException(BindException ex) {
        BindingResult bindingResult = (BindingResult) ex;

        bindingResult.getFieldErrorCount();
        logger.debug("Field Error Count : " + bindingResult.getFieldErrorCount());
        List<FieldError> re = bindingResult.getFieldErrors();
        List<FieldErrorDetail> details = new ArrayList<FieldErrorDetail>();
        for (FieldError ff : re) {
            FieldErrorEx f = (FieldErrorEx) ff;
            logger.debug("Default Message : " + f.getDefaultMessage());
            logger.debug("Field : " + f.getField());
            // Integer index=null;
            // String id = null;

            // logger.debug("source : " + o);
            FieldErrorDetail fd = FieldErrorDetail.create(f.getIndex(), f.getId(), f, f.getDefaultMessage());
            details.add(fd);
        }
        return new ResponseEntity<ResultValue>(new ResultValue(ResultCode.INVALID_INPUT_FIELD, details), HttpStatus.OK);
    }
}