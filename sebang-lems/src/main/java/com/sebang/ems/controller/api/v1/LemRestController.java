package com.sebang.ems.controller.api.v1;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import com.sebang.ems.component.validator.CustomCollectionValidator;
import com.sebang.ems.controller.BaseRestController;
import com.sebang.ems.domain.Exception.BaseException;
import com.sebang.ems.domain.common.ResultCode;
import com.sebang.ems.domain.common.ResultValue;
import com.sebang.ems.domain.common.SearchParam;
import com.sebang.ems.model.Do.TLogisTransMonInfo;
import com.sebang.ems.model.Do.TVehicle;
import com.sebang.ems.model.Do.VLogisDrvLog;
import com.sebang.ems.model.Do.VLogisEff;
import com.sebang.ems.model.Do.VLogisTransYearVhcl;
import com.sebang.ems.model.Do.VVehicle;
import com.sebang.ems.model.Dto.ReqMonEngClose;
import com.sebang.ems.model.Dto.ReqVehicle;
import com.sebang.ems.service.AmtServiceImpl;
import com.sebang.ems.service.LogisServiceImpl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindException;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@CrossOrigin(origins = "*")
@RestController
@RequestMapping("/api/v1/lem")
public class LemRestController extends BaseRestController{

    public static final Logger logger = LoggerFactory.getLogger(LemRestController.class);

    @Autowired
    CustomCollectionValidator customCollectionValidator;

    @Autowired
    LogisServiceImpl logisService;
    
    @Autowired
    AmtServiceImpl amtService;

    // 용차 운행정보 등록 목록 조회
    @RequestMapping(value = "/411/ls", method = RequestMethod.GET, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> getLem411Ls(HttpServletRequest request, @ModelAttribute SearchParam reqParam,
            BindingResult bindingResult) throws BindException {
        logger.info("getLem411Ls() RequestParam:" + reqParam);

        ResultCode resCode = ResultCode.SUCCESS;

        if (reqParam.getCYear() == null) {
            FieldError e = new FieldError("SearchParam", "cYear", "cYear 는 필수 입력 값입니다. ");
            bindingResult.addError(e);
        }

        if (reqParam.getCMon() == null) {
            FieldError e = new FieldError("SearchParam", "cMon", "cMon 는 필수 입력 값입니다. ");
            bindingResult.addError(e);
        }

        if (bindingResult.hasErrors())
            throw new BindException(bindingResult);

        // if (reqParam.getCDate() == null || reqParam.getSDrvType() == null)
        // return new ResponseEntity<ResultValue>(new
        // ResultValue(ResultCode.INVALID_INPUT_FIELD, null),
        // HttpStatus.OK);

        List<VLogisDrvLog> result = logisService.searchVLogisDrvLog(reqParam);
        if (result.size() == 0)
            resCode = ResultCode.NO_DATA;

        return new ResponseEntity<ResultValue>(new ResultValue(resCode, result), HttpStatus.OK);
    }

    // 용차 운행정보 목록 저장
    @RequestMapping(value = "/411/ls", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> postLem411Ls(HttpServletRequest request, @RequestBody List<TLogisTransMonInfo> reqParam,
            BindingResult bindingResult) throws BindException {
        logger.info("postLem411Ls() RequestParam:" + convertString(reqParam));

        String sModId = getUserId(request);
        if(sModId == null)
            throw new BaseException("Invalid token");
        

        customCollectionValidator.validate(reqParam, bindingResult);
        for (TLogisTransMonInfo mon : reqParam) {
            if (mon.getIdLogisTransMonInfo() == null) {
                FieldError e = new FieldError("TLogisTransMonInfo", "idTransLogsMonInfo",
                        "idLogisTransMonInfo 는 필수입력 값입니다. ");
                bindingResult.addError(e);
            }

            if(mon.getDistanceSum() == null){
                FieldError e = new FieldError("TLogisTransMonInfo", "distanceSum",
                "운송 거리(distanceSum) 는 필수입력 값입니다.");
                bindingResult.addError(e);
            }else if(BigDecimal.ZERO.compareTo(mon.getDistanceSum() ) >=0)
            {
                FieldError e = new FieldError("TLogisTransMonInfo", "distanceSum",
                "운송 거리(distanceSum) 는 0 커야 합니다. ");
                bindingResult.addError(e);
            }

            
            if(mon.getLoadageSum() == null){
                FieldError e = new FieldError("TLogisTransMonInfo", "loadageSum",
                "적재량 (loadageSum) 는 필수입력 값입니다.");
                bindingResult.addError(e);
            }else if(BigDecimal.ZERO.compareTo(mon.getLoadageSum()) >=0){
                FieldError e = new FieldError("TLogisTransMonInfo", "loadageSum",
                "적재량 (loadageSum) 0 보다 커여야 합니다.");
                bindingResult.addError(e);               
            }

            mon.setsModId(sModId);
        }

        if (bindingResult.hasErrors()) {
            throw new BindException(bindingResult);
        }
        
        if ( reqParam != null && reqParam.size() > 0 ) {
        	// 마감처리된 년월인지 확인
        	String dataMon = reqParam.get(0).getDateMon();
        	String cYear = dataMon.substring(0, 4);
        	String cMon = dataMon.substring(4, 6);
        	Integer close = amtService.findMonthEndClosing(cYear,cMon);
        	if(close != null && close > 0) {
        		// 마감된 년월일 경우 303코드 발생
        		return new ResponseEntity<ResultValue>(new ResultValue(ResultCode.ALREADY_MONTHEND_CLOSING, "마감되어 수정이 불가능한 일자입니다."), HttpStatus.OK);
        	}
        	
        	// 현재년월보다 같거나 큰 년월인지 확인
        	Calendar cal = Calendar.getInstance();
        	int iYear = Integer.parseInt(cYear);
        	int iMon = Integer.parseInt(cMon);
        	int calYear = cal.get(Calendar.YEAR);
        	int calMon = (cal.get(Calendar.MONTH)+1);
        	if ( iYear > calYear || (iYear == calYear && iMon >= calMon) ) {
        		// 마감된 년월일 경우 303코드 발생
        		return new ResponseEntity<ResultValue>(new ResultValue(ResultCode.FAILED, "수정이 불가능한 일자입니다. (" + cYear + "-" + cMon + ")"), HttpStatus.OK);
        	}
        }

        // 운송정보 저장
        logisService.upsertLogisTransMonInfo(reqParam);
        return new ResponseEntity<ResultValue>(new ResultValue(ResultCode.SUCCESS, null), HttpStatus.OK);
    }

  

    // 차량정보 목록 조회
    @RequestMapping(value = "/461/ls", method = RequestMethod.GET, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> getLem461Ls(HttpServletRequest request, @ModelAttribute SearchParam reqParam) {
        logger.info("getLem461Ls() RequestParam:" + reqParam);

        List<VVehicle> list = logisService.searchVehicle(reqParam);

        return new ResponseEntity<ResultValue>(new ResultValue(ResultCode.SUCCESS, list), HttpStatus.OK);
    }

    // 차량정보 목록 저장
    @RequestMapping(value = "/461/ls", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> postLem461Ls(HttpServletRequest request, @RequestBody List<ReqVehicle> reqParam,
            BindingResult bindingResult) throws BindException {
        logger.info("postLem461Ls() RequestParam:" + convertString(reqParam));

        String sModId = getUserId(request);
        if(sModId == null)
            throw new BaseException("Invalid token");

     //   customCollectionValidator.validate(reqParam, bindingResult);
        for (ReqVehicle v : reqParam) {

            if (v.getIdVehicle() == null) {
                FieldError e = new FieldError("ReqVehicle", "idVehicle", "idVehicle 는 필수 입력 값입니다.");
                bindingResult.addError(e);
            }

            // if (v.getIdTransCom() == null) {
            //     FieldError e = new FieldError("ReqVehicle", "idTransCom", "idTransCom 은 필수 입력 값입니다.");
            //     bindingResult.addError(e);
            // }
            v.setsModId(sModId);
        }

        // if (bindingResult.hasErrors())
        //     throw new BindException(bindingResult);

        logisService.updateVehicle(reqParam);
        return new ResponseEntity<ResultValue>(new ResultValue(ResultCode.SUCCESS, null), HttpStatus.OK);
    }

    // 차량정보 목록 삭제
    @RequestMapping(value = "/461/ls", method = RequestMethod.DELETE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> deleteLem461Ls(HttpServletRequest request, @ModelAttribute SearchParam reqParam,
            BindingResult bindingResult) throws BindException {

        logger.info("deleteLem461Ls() RequestParam:" + reqParam);

        if (reqParam.getIdVehicles() == null) {
            FieldError e = new FieldError("ReqVehicle", "idVehicles", "idVehicles 는 필수 입력 값입니다.");
            bindingResult.addError(e);
        }

        if (bindingResult.hasErrors())
            throw new BindException(bindingResult);

        String sModId = getUserId(request);
        if(sModId == null)
            throw new BaseException("Invalid token");

        String[] idVehicles = reqParam.getIdVehicles().split(",");
        List<TVehicle> list = new ArrayList<TVehicle>();

        for (String id : idVehicles) {
            TVehicle v = new TVehicle();
            try {
                v.setIdVehicle(Long.parseLong(id));
            } catch (NumberFormatException ex) {
                throw new BaseException("Invalid id " + id);
            }
            v.setsModId(sModId);
            list.add(v);
        }

        logisService.deleteVehicle(list);

        return new ResponseEntity<ResultValue>(new ResultValue(ResultCode.SUCCESS, null), HttpStatus.OK);
    }


    // 온실가스/에너지 현황 조회
    @RequestMapping(value = "/451/ls", method = RequestMethod.GET, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> getLem451Ls(HttpServletRequest request, @ModelAttribute SearchParam reqParam) {
        logger.info("getLem451Ls() RequestParam:" + reqParam);
        ResultCode resCode = ResultCode.SUCCESS;

        Map<String, List<VLogisTransYearVhcl>> resMap = new HashMap<String, List<VLogisTransYearVhcl>>();

        List<VLogisTransYearVhcl> list = logisService.searchVLogisTransYear(reqParam);
        if (list.size() != 0) {

            for (VLogisTransYearVhcl item : list) {
                String key = item.getYear();
                if (resMap.containsKey(key)) {
                    List<VLogisTransYearVhcl> sites = resMap.get(key);
                    sites.add(item);
                } else {
                    List<VLogisTransYearVhcl> sites = new ArrayList<VLogisTransYearVhcl>();
                    sites.add(item);
                    resMap.put(key, sites);
                }
            }
        } else
            resCode = ResultCode.NO_DATA;

        return new ResponseEntity<ResultValue>(new ResultValue(resCode, resMap), HttpStatus.OK);
    }

    // 효율화 지표 조회
    @RequestMapping(value = "/452/ls", method = RequestMethod.GET, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> getLem452Ls(HttpServletRequest request, @ModelAttribute SearchParam reqParam,
            BindingResult bindingResult) throws BindException {
        logger.info("getLem452Ls() RequestParam:" + reqParam);
        ResultCode resCode = ResultCode.SUCCESS;

        if (reqParam.getCYear() == null) {
            FieldError e = new FieldError("SearchParam", "cYear", "cYear는 필수 입력 값입니다. ");
            bindingResult.addError(e);
        }

		/*
		 * if (reqParam.getSLineCd() == null) { FieldError e = new
		 * FieldError("SearchParam", "sLineCd", "sLineCd는 필수 입력 값입니다. ");
		 * bindingResult.addError(e); }
		 */

        if (bindingResult.hasErrors())
            throw new BindException(bindingResult);

        String s = reqParam.getCYear();
        reqParam.setArrYear(s.split(","));

        List<VLogisEff> list = logisService.searchVLogisEffs(reqParam);
        if(list.size() == 0)
            resCode = ResultCode.NO_DATA;

        return new ResponseEntity<ResultValue>(new ResultValue(resCode, list), HttpStatus.OK);
    }

    // 사내 목표대비 실적 조회
    @RequestMapping(value = "/4531/ls", method = RequestMethod.GET, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> getLem4531Ls(HttpServletRequest request, @ModelAttribute SearchParam reqParam) {
        logger.info("getLem4531Ls() RequestParam:" + reqParam);

        return new ResponseEntity<ResultValue>(new ResultValue(ResultCode.FAILED, null), HttpStatus.OK);
    }

}