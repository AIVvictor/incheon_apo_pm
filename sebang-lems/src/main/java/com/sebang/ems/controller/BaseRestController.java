package com.sebang.ems.controller;

import java.util.Collection;

import javax.servlet.http.HttpServletRequest;

import com.auth0.jwt.interfaces.DecodedJWT;
import com.sebang.ems.auth.jwt.JwtInfo;
import com.sebang.ems.util.JwtUtil;

public class BaseRestController {

    /**
     * Collection 을 String 으로 변환
     * 
     * @param target list object
     * @return String 변환된 문자열
     */
    public String convertString(Object target) {
        StringBuilder sb = new StringBuilder();

        if (target.getClass() == Collection.class) {

            Collection collection = (Collection) target;

            for (Object object : collection) {
                sb.append(object.toString());
            }
            return sb.toString();
        } else
            return target.toString();
    }

    /**
     * Http header 의 토큰 정보를 디코딩 해서 사용자 ID 를 얻는다.
     * 
     * @param request
     * @return String
     */
    public String getUserId(HttpServletRequest request) {

        String token = request.getHeader(JwtInfo.HEADER_NAME);

        DecodedJWT jwt = JwtUtil.tokenToJwt(token);
        String userId = jwt.getClaim("id").asString();
        return userId;
    }

    /**
     * remote ip address ref
     * https://www.lesstif.com/pages/viewpage.action?pageId=18220218
     * 
     * @param request
     * @return String remote ip address
     */
    public String getRemoteAddr(HttpServletRequest request) {
        String ip = request.getHeader("X-FORWARDED-FOR");
        if (ip == null)
            ip = request.getRemoteAddr();

        return ip;
    }
}
