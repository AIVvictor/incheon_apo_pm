package com.sebang.ems.controller.web;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;


@Controller
public class TestController {
	

    @GetMapping("/")
	String index(Model model)
	{    	
		return "test/index";
    }
    
	@GetMapping("/test")
	String test(Model model)
	{    	
		return "test/index";
	}
	
	
}
