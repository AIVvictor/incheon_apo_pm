package com.sebang.ems.controller.api.v1;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import com.sebang.ems.component.validator.CustomCollectionValidator;
import com.sebang.ems.controller.BaseRestController;
import com.sebang.ems.domain.Exception.BaseException;
import com.sebang.ems.domain.common.ResultCode;
import com.sebang.ems.domain.common.ResultValue;
import com.sebang.ems.domain.common.SearchParam;
import com.sebang.ems.model.EngPointAmt;
import com.sebang.ems.model.Do.TEngPointAmt;
import com.sebang.ems.model.Do.VBaseUntAmt;
import com.sebang.ems.model.Do.VGoalResultAnalyAcc;
import com.sebang.ems.model.Do.VMonEngClose;
import com.sebang.ems.model.Do.VMonSiteEngCefAmt;
import com.sebang.ems.model.Dto.ReqEngPointAmt;
import com.sebang.ems.model.Dto.ReqMonEngClose;
import com.sebang.ems.model.Dto.ResSumAmt;
import com.sebang.ems.service.AmtServiceImpl;
import com.sebang.ems.service.GoalServiceImpl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindException;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@CrossOrigin(origins = "*")
@RestController
@RequestMapping("/api/v1")
public class GhgRestController extends BaseRestController {

    public static final Logger logger = LoggerFactory.getLogger(GhgRestController.class);

    @Autowired
    CustomCollectionValidator customCollectionValidator;

    @Autowired
    AmtServiceImpl amtService;

    @Autowired
    GoalServiceImpl goalService;

    /**
     * 월마감 목록 조회
     * @param request
     * @param reqParam
     * @return
     */
    @RequestMapping(value = "/ghg/315/ls", method = RequestMethod.GET, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> getGhg315Ls(HttpServletRequest request, @ModelAttribute SearchParam reqParam) {
        logger.info("getGhg315Ls() RequestParam:" + reqParam);
        ResultCode resCode = ResultCode.SUCCESS;

//        if (reqParam.getIdSite() != null) {
//            List<VMonSiteEngCefAmt> list = amtService.searchMonSiteEngCefAmt(reqParam);
//            if (list.size() == 0)
//                resCode = ResultCode.NO_DATA;
//            return new ResponseEntity<ResultValue>(new ResultValue(resCode, list), HttpStatus.OK);
//
//        } else {

            List<VMonEngClose> list = amtService.searchMonEngClose(reqParam);
            if (list.size() == 0)
                resCode = ResultCode.NO_DATA;

            return new ResponseEntity<ResultValue>(new ResultValue(resCode, list), HttpStatus.OK);
//        }
    }

    /**
     * 사업장 배출 실적 현황
     * @param request
     * @param reqParam
     * @return
     */
    @RequestMapping(value = "/ghg/315/em/res/ls", method = RequestMethod.GET, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> getGhg315EmResLs(HttpServletRequest request, @ModelAttribute SearchParam reqParam) {
        logger.info("getGhg315EmResLs() RequestParam:" + reqParam);
        ResultCode resCode = ResultCode.SUCCESS;

        if( reqParam.getIdSite() == null || reqParam.getCYear() == null || reqParam.getCMon() == null )
        {
        	resCode = ResultCode.NOT_NULL_FIELD;
            return new ResponseEntity<ResultValue>(new ResultValue(resCode, null), HttpStatus.OK);
        }
        
        List<VMonSiteEngCefAmt> list = amtService.searchMonSiteEngCefAmt(reqParam);
        if (list.size() == 0)
            resCode = ResultCode.NO_DATA;
        return new ResponseEntity<ResultValue>(new ResultValue(resCode, list), HttpStatus.OK);
        
    }

    // 월마감 마감 여부 확인
    @RequestMapping(value = "/ghg/315/close", method = RequestMethod.GET, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> getGhg315Close(HttpServletRequest request, @RequestParam("cYear") String cYear, @RequestParam("cMon")String cMon)
    {
        logger.info("getGhg315Close() RequestParam:" + cYear+" cMon:" +cMon);
        String cCloseYn = "N";
        Integer close = amtService.findMonthEndClosing(cYear,cMon);
        if(close != null && close > 0)
            cCloseYn = "Y";

        ReqMonEngClose c =  new ReqMonEngClose();
        c.setCYear(cYear);
        c.setcMon(cMon);
        c.setCCloseYn(cCloseYn);

        return new ResponseEntity<ResultValue>(new ResultValue(ResultCode.SUCCESS, c), HttpStatus.OK);
    }


    // 월마감 마감
    @RequestMapping(value = "/ghg/315/close", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> postGhg315Close(HttpServletRequest request, @RequestBody @Valid ReqMonEngClose reqParam,
            BindingResult bindingResult) throws BindException {
        logger.info("postGhg315Close() RequestParam:" + reqParam);

        if (bindingResult.hasErrors())
            throw new BindException(bindingResult);
        reqParam.setCCloseYn("G");
        reqParam.setIdSite(null);// 전체 사업장

        amtService.closeMonEngClose(reqParam.toEntity());
        return new ResponseEntity<ResultValue>(new ResultValue(ResultCode.SUCCESS, null), HttpStatus.OK);
    }

    // 월마감 마감 취소
    @RequestMapping(value = "/ghg/315/close/cancel", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> postGhg315CloseCancel(HttpServletRequest request,
            @RequestBody @Valid ReqMonEngClose reqParam, BindingResult bindingResult) throws BindException {
        logger.info("postGhg315CloseCancel() RequestParam:" + reqParam);

        if (bindingResult.hasErrors())
            throw new BindException(bindingResult);
        reqParam.setCCloseYn("N");

        amtService.cancelMonEngClose(reqParam.toEntity());
        return new ResponseEntity<ResultValue>(new ResultValue(ResultCode.SUCCESS, null), HttpStatus.OK);
    }

    // 사용실적 목록 조회
    @RequestMapping(value = "/ghg/316/ls", method = RequestMethod.GET, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> getGhg316Ls(HttpServletRequest request, @ModelAttribute SearchParam reqParam,
            BindingResult bindingResult) throws BindException {
        logger.info("getGhg316Ls() RequestParam:" + reqParam);
        ResultCode resCode = ResultCode.SUCCESS;

        if (reqParam.getCYear() == null) {
            FieldError e = new FieldError("SearchParam", "cYear", "cYear 는 필수 입력값입니다.");
            bindingResult.addError(e);
        }

        if (reqParam.getCMon() == null) {
            FieldError e = new FieldError("SearchParam", "cMon", "cMon 는 필수 입력값입니다.");
            bindingResult.addError(e);
        }

        if (bindingResult.hasErrors())
            throw new BindException(bindingResult);

        List<EngPointAmt> list = amtService.searchEngPointAmt(reqParam);
        if (list.size() == 0)
            resCode = ResultCode.NO_DATA;

        return new ResponseEntity<ResultValue>(new ResultValue(resCode, list), HttpStatus.OK);
    }

    // 사용실적 목록 저장
    @RequestMapping(value = "/ghg/316/ls", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> postGhg316ls(HttpServletRequest request, @RequestBody @Valid List<ReqEngPointAmt> reqParam,
            BindingResult bindingResult) throws BindException {
        logger.info("postGhg316ls() RequestParam:" + convertString(reqParam));

        customCollectionValidator.validate(reqParam, bindingResult);

        if (bindingResult.hasErrors())
            throw new BindException(bindingResult);

        String userId = getUserId(request);
        if (userId == null)
            throw new BaseException("Invalid Token");

        List<TEngPointAmt> engPointAmt = new ArrayList<TEngPointAmt>();
        for (ReqEngPointAmt a : reqParam) {
            a.setsModId(userId);
            engPointAmt.add(a.toEntity());
        }

        amtService.upsertEngPointAmts(engPointAmt);
        return new ResponseEntity<ResultValue>(new ResultValue(ResultCode.SUCCESS, null), HttpStatus.OK);
    }

    // 연도별분석 조회
    @RequestMapping(value = "/ghg/321/ls", method = RequestMethod.GET, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> getGhg321Ls(HttpServletRequest request, @ModelAttribute SearchParam reqParam,
            BindingResult bindingResult) throws BindException {
        logger.info("getGhg321Ls() RequestParam:" + reqParam);

        ResultCode resCode = ResultCode.SUCCESS;

        if (reqParam.getCYear() == null) {
            FieldError e = new FieldError("SearchParam", "cYear", "cYear 는 필수 입력 값입니다. ");
            bindingResult.addError(e);
        }

        if (reqParam.getCYearEnd() == null) {
            FieldError e = new FieldError("SearchParam", "cYearEnd", "cYearEnd 는 필수 입력 값입니다. ");
            bindingResult.addError(e);
        }
        if (bindingResult.hasErrors()) {
            throw new BindException(bindingResult);
        }

        Map<String, List<ResSumAmt>> resMap = amtService.searchYearAnalysisAmt(reqParam);
        if (resMap.size() == 0)
            resCode = ResultCode.NO_DATA;

        return new ResponseEntity<ResultValue>(new ResultValue(resCode, resMap), HttpStatus.OK);
    }

    // 월별분석 조회
    @RequestMapping(value = "/ghg/322/ls", method = RequestMethod.GET, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> getGhg322Ls(HttpServletRequest request, @ModelAttribute SearchParam reqParam,
            BindingResult bindingResult) throws BindException {
        logger.info("getGhg322Ls() RequestParam:" + reqParam);
        ResultCode resCode = ResultCode.SUCCESS;

        if (reqParam.getCYear() == null) {
            FieldError e = new FieldError("SearchParam", "cYear", "cYear 는 필수 입력 값입니다. ");
            bindingResult.addError(e);
        }

        if (reqParam.getCYearEnd() == null) {
            FieldError e = new FieldError("SearchParam", "cYearEnd", "cYearEnd 는 필수 입력 값입니다. ");
            bindingResult.addError(e);
        }

        if (reqParam.getCMon() == null) {
            FieldError e = new FieldError("SearchParam", "cMon", "cMon 는 필수 입력 값입니다. ");
            bindingResult.addError(e);
        }

        if (reqParam.getCMonEnd() == null) {
            FieldError e = new FieldError("SearchParam", "cMonEnd", "cMonEnd 는 필수 입력 값입니다. ");
            bindingResult.addError(e);
        }

        if (bindingResult.hasErrors()) {
            throw new BindException(bindingResult);
        }

        Map<String, List<ResSumAmt>> resMap = amtService.searchMonAnalysisAmt(reqParam);
        if (resMap.size() == 0)
            resCode = ResultCode.NO_DATA;

        return new ResponseEntity<ResultValue>(new ResultValue(resCode, resMap), HttpStatus.OK);
    }

    // 목표대비 실적분석 조회
    @RequestMapping(value = "/ghg/324/ls", method = RequestMethod.GET, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> getGhg324Ls(HttpServletRequest request, @ModelAttribute SearchParam reqParam,
            BindingResult bindingResult) throws BindException {
        logger.info("getGhg324Ls() RequestParam:" + reqParam);

        ResultCode resCode = ResultCode.SUCCESS;
        if (reqParam.getCYear() == null) {
            FieldError e = new FieldError("SearchParam", "cYear", "cYear 은 필수 입력 값입니다. ");
            bindingResult.addError(e);
        }

        if (bindingResult.hasErrors())
            throw new BindException(bindingResult);

        List<VGoalResultAnalyAcc> list = goalService.searchSiteMonGoalArchRate(reqParam);
        if (list.size() == 0)
            resCode = ResultCode.NO_DATA;

        return new ResponseEntity<ResultValue>(new ResultValue(resCode, list), HttpStatus.OK);
    }

    // 원단위 분석 조회
    @RequestMapping(value = "/ghg/325/ls", method = RequestMethod.GET, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> getGhg325Ls(HttpServletRequest request, @ModelAttribute SearchParam reqParam,
            BindingResult bindingResult) throws BindException {
        logger.info("getGhg325Ls() RequestParam:" + reqParam);

        ResultCode resCode = ResultCode.SUCCESS;

        if (reqParam.getCYear() == null) {
            FieldError e = new FieldError("SearchParam", "cYear", "cYear 은 필수 입력값입니다. ");
            bindingResult.addError(e);
        }

        if (bindingResult.hasErrors()) {
            throw new BindException(bindingResult);
        }

        String s = reqParam.getCYear();
        reqParam.setArrYear(s.split(","));

        //reqParam.setSLogiTypeCd("1");
        List<VBaseUntAmt> resList = amtService.searchBaseUntAmtAnalysis(reqParam);
        if (resList.size() == 0)
            resCode = ResultCode.NO_DATA;

        return new ResponseEntity<ResultValue>(new ResultValue(resCode, resList), HttpStatus.OK);
    }
}