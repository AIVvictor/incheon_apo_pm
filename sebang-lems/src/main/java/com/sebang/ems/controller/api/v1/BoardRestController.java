package com.sebang.ems.controller.api.v1;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindException;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.auth0.jwt.interfaces.DecodedJWT;
import com.sebang.ems.auth.jwt.JwtInfo;
import com.sebang.ems.controller.BaseRestController;
import com.sebang.ems.domain.common.ResultCode;
import com.sebang.ems.domain.common.ResultValue;
import com.sebang.ems.domain.common.SearchParam;
import com.sebang.ems.model.Do.TBoard;
import com.sebang.ems.model.Do.VBoardWithBLOBs;
import com.sebang.ems.model.Dto.ReqBoard;
import com.sebang.ems.model.Dto.ResModifyBoard;
import com.sebang.ems.model.Dto.ResQueryBoard;
import com.sebang.ems.service.BoardServiceImpl;
import com.sebang.ems.util.JwtUtil;

@CrossOrigin(origins = "*")
@RestController
@RequestMapping("/api/v1/board")
public class BoardRestController extends BaseRestController{

	public static final Logger logger = LoggerFactory.getLogger(BoardRestController.class);

	@Autowired
	BoardServiceImpl boardService;

	/**
	 * 게시물 목록 요청
	 * @param request
	 * @param screenId
	 * @param reqParam
	 * @return
	 */
	@CrossOrigin(origins = "*")
	@RequestMapping(value = "/{screenId}/ls", method = RequestMethod.GET, consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> getBoardLs(HttpServletRequest request, @PathVariable("screenId") String screenId,
			@ModelAttribute SearchParam reqParam) {
		
		String param = reqParam.getSTitle();
		logger.info("getBoardLs() GET " + screenId +"="+getBoardName(screenId)+ ", sTitle=" + param);

		ResultCode rc = ResultCode.FAILED;

		
		List<ResQueryBoard> resQB = null;
		// 검색 keyword를 trim 한다.
		if (param != null) {
			param = param.trim();
		}

		// 검색 keyword 가 zero length string 이면, null 로 강제로 set 한다. 전체 조회로 간주한다.
		if (param != null && param.length() == 0) {
			param = null;
		}
		
		List<VBoardWithBLOBs> bnList=null;
		switch (screenId) {
		case "621": // 공지사항
			bnList = boardService.findNotice(param);
			// -----------------
			break;
		case "631": // Q&A
			bnList = boardService.findQA(param);
			break;
		case "641": // 자료실
			bnList = boardService.findMaterial(param);
			break;
		case "651": // 용어사전
			bnList = boardService.findTerms(param);
			break;
		}
		// 조회 결과가 없는 경우 --
		if (bnList == null) {
			rc = ResultCode.NO_DATA;
			resQB  = null;
		} else if (bnList.size() == 0) {
			rc = ResultCode.NO_DATA;
			resQB  = null;
		}else {
			rc = ResultCode.SUCCESS;
			resQB = ResQueryBoard.makeResponse(bnList,false);
		}

		return new ResponseEntity<ResultValue>(new ResultValue(rc, resQB), HttpStatus.OK);

	}

	/**
	 * 게시물 상세 정보 요청
	 * @param request
	 * @param screenId
	 * @param reqParam
	 * @return
	 */
	@CrossOrigin(origins = "*")
	@RequestMapping(value = "/{screenId}/dtl", method = RequestMethod.GET, consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> getBoardDtl(HttpServletRequest request, @PathVariable("screenId") String screenId,
			@ModelAttribute SearchParam reqParam) {

		// 게시판 Id
		String param = reqParam.getNBoardId();
		logger.info("getBoardDtl() GET " + screenId +"="+getBoardName(screenId)+ ", nBoardId=" + param);

		VBoardWithBLOBs bdDtl = null;
		ResultCode rc = ResultCode.SUCCESS;

		bdDtl = boardService.findOneByBoardId(param);

		ResQueryBoard resQB = null;
		// 조회 결과가 없는 경우 --
		if (bdDtl == null) {
			rc = ResultCode.NO_DATA;
			bdDtl = null;
		}
		else {
			resQB = new ResQueryBoard(bdDtl,true);
		}

		return new ResponseEntity<ResultValue>(new ResultValue(rc, resQB), HttpStatus.OK);
	}

	/**
	 * 게시물 등록/수정 요청. board id 가 null 인 경우 등록으로 동작한다.
	 * @param request
	 * @param screenId
	 * @param reqParam
	 * @return
	 */
	@CrossOrigin(origins = "*")
	@RequestMapping(value = "/{screenId}/dtl", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> postBoardDtl(HttpServletRequest request, @PathVariable("screenId") String screenId,
			@RequestBody @Valid ReqBoard reqBody, BindingResult bindingResult) throws BindException {

		logger.info("postBoardDtl() POST " + screenId +"="+getBoardName(screenId)+ ", Request Body=" + reqBody);

		if (bindingResult.hasErrors())
			throw new BindException(bindingResult);

		// 사용자Id(사번)얻기
		String token = request.getHeader(JwtInfo.HEADER_NAME);
		DecodedJWT jwt = JwtUtil.tokenToJwt(token);
		String userId = jwt.getClaim("id").asString();
		logger.debug("User Id: " + userId);

		TBoard tbdDtl = null;
		VBoardWithBLOBs vbdDtl = null;
		ResultCode rc = ResultCode.SUCCESS;
		ResModifyBoard res = new ResModifyBoard();

		String param = reqBody.getIdBoard();

		// 검색 keyword 가 zero length string 이면, null 로 강제로 set 한다. 전체 조회로 간주한다.
		if (param != null && param.length() == 0) {
			param = null;
		}

		if (param != null) {
			// board id 로 있는지 검색.
			vbdDtl = boardService.findOneByBoardId(param);
		} else {
			vbdDtl = null;
		}

		tbdDtl = new TBoard();
		tbdDtl.setIdBoard(param);
		tbdDtl.setsTitle(reqBody.getsTitle());
		tbdDtl.setsContent(reqBody.getsContent());
		tbdDtl.setsBoardType(getBoardType(screenId));
		tbdDtl.setsRegEmpId(userId);// reqBody.getsRegEmpId());
		tbdDtl.setgFileId(reqBody.getgFileId());

		long ret=0;
		if (vbdDtl == null) {
			// 없으면 insert 한다.
			ret = boardService.insert(tbdDtl);
			res.setSBoardId(tbdDtl.getIdBoard().toString());
		} else {
			res.setSBoardId(tbdDtl.getIdBoard().toString());
			// update 한다.
			ret = boardService.update(tbdDtl);
		}
		res.setSBoardType(tbdDtl.getsBoardType());
		if(ret == 0 )
		{
 			rc = ResultCode.NO_DATA;
 			res = null;
		}
		
		return new ResponseEntity<ResultValue>(new ResultValue(rc, res), HttpStatus.OK);
	}

	/**
	 * 게시물 삭제.
	 * @param request
	 * @param screenId
	 * @param reqParam
	 * @return
	 */
	@CrossOrigin(origins = "*")
	@RequestMapping(value="/{screenId}/dtl",method = RequestMethod.DELETE,consumes = MediaType.APPLICATION_JSON_VALUE)    
    public ResponseEntity<?> deleteBoardDtl(HttpServletRequest request,
    		@PathVariable("screenId") String screenId,
    		@ModelAttribute SearchParam reqParam) 
    {
		// 게시판 Id
		String param = reqParam.getNBoardId();
		logger.info("deleteBoardDtl() DELETE " + screenId +"="+getBoardName(screenId)+ ", nBoardId=" + param);
		
        TBoard tbdDtl = null;
    	ResultCode 	 rc = ResultCode.SUCCESS;
    	
    	ResModifyBoard res = new ResModifyBoard();
    	
 		
 		Integer ret = boardService.deleteOneByBoardId(param);
 		
 		// 조회 결과가 없는 경우 --
 		if(ret==0) {
 			rc = ResultCode.NO_DATA;
 			res = null;
 		}else if(ret>0) {
 			res.sBoardId = param;
 			res.sBoardType = getBoardType(screenId);
 		}
 		         
 		return new ResponseEntity<ResultValue>(new ResultValue(rc,res), HttpStatus.OK);
         
    }
	
	/**
	 * screen Id로부터 board Type 정보를 반환한다.
	 * @param screenId
	 * @return
	 */
	static String getBoardType(String screenId) {
		String sBoardType="";
		switch (screenId) {
		case "621":
			sBoardType = BoardServiceImpl.constNotice; // 공지사항
			break;
		case "651":
			sBoardType = BoardServiceImpl.constScrap; // 스크랩
			break;
		case "641":
			sBoardType = BoardServiceImpl.constMaterial; // 자료실
			break;
		case "631":
			sBoardType = BoardServiceImpl.constQA; // Q&A
			break;
		}
		return sBoardType;
	}
	
	/**
	 * screen Id로부터 board 이름을 반환한다.
	 * @param screenId
	 * @return
	 */
	static String getBoardName(String screenId) {
		String sBoardName="Unknown";
		switch (screenId) {
		case "621":
			sBoardName = "Notice"; // 공지사항
			break;
		case "651":
			sBoardName = "Scrap"; // 스크랩
			break;
		case "641":
			sBoardName = "Material"; // 자료실
			break;
		case "631":
			sBoardName = "QA"; // Q&A
			break;
		}
		return sBoardName;
	}
}

