package com.sebang.ems.controller.api.v1;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import com.sebang.ems.controller.BaseRestController;
import com.sebang.ems.domain.Exception.BaseException;
import com.sebang.ems.domain.common.ResultCode;
import com.sebang.ems.domain.common.ResultValue;
import com.sebang.ems.domain.common.SearchParam;
import com.sebang.ems.model.SiteYearGoal4Home;
import com.sebang.ems.model.SumAmt4Home;
import com.sebang.ems.model.Dto.ResSunPower;
import com.sebang.ems.service.AmtServiceImpl;
import com.sebang.ems.service.GoalServiceImpl;
import com.sebang.ems.service.SunPowerServiceImpl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@CrossOrigin(origins = "*")
@RestController
@RequestMapping("/api/v1/home")
public class HomeRestController extends BaseRestController {

    public static final Logger logger = LoggerFactory.getLogger(HomeRestController.class);

    SunPowerServiceImpl sunPowerService;

    AmtServiceImpl amtService;

    GoalServiceImpl goalService;

    @Autowired
    public HomeRestController(SunPowerServiceImpl sunPowerService, AmtServiceImpl amtService,
            GoalServiceImpl goalService) {
        this.sunPowerService = sunPowerService;
        this.amtService = amtService;
        this.goalService = goalService;
    }

    /**
     * 온실가스 배출실적 목록
     * 
     * @param request
     * @param reqParam
     * @return
     */
    @RequestMapping(value = "/010/ghg/ls", method = RequestMethod.GET, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> getHome010GhgLs(HttpServletRequest request, @ModelAttribute SearchParam reqParam) {
        logger.info("getHome010GhgLs() RequestParam:" + reqParam);
        ResultCode resultCode = ResultCode.SUCCESS;

        if (reqParam.getCYear() == null)
            throw new BaseException("cYear 는 필수 입력 값입니다. ");

        String s = reqParam.getCYear();
        reqParam.setArrYear(s.split(","));

        List<SumAmt4Home> list = amtService.searchGasSumAmt4Home(reqParam);
        if (list.size() == 0)
            resultCode = ResultCode.NO_DATA;

        return new ResponseEntity<ResultValue>(new ResultValue(resultCode, list), HttpStatus.OK);
    }

    /**
     * 에너지 사용실적 배출실적 목록
     * 
     * @param request
     * @param reqParam
     * @return
     */
    @RequestMapping(value = "/010/eng/ls", method = RequestMethod.GET, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> getHome010EngLs(HttpServletRequest request, @ModelAttribute SearchParam reqParam) {
        logger.info("getHome010EngLs() RequestParam:");
        ResultCode resultCode = ResultCode.SUCCESS;

        if (reqParam.getCYear() == null)
            throw new BaseException("cYear 는 필수 입력 값입니다. ");

        String s = reqParam.getCYear();
        reqParam.setArrYear(s.split(","));

        List<SumAmt4Home> list = amtService.searchEngSumAmt4Home(reqParam);
        if (list.size() == 0)
            resultCode = ResultCode.NO_DATA;

        return new ResponseEntity<ResultValue>(new ResultValue(resultCode, list), HttpStatus.OK);
    }

    /**
     * 온실가스 에너지 현황 목록
     * 
     * @param request
     * @param reqParam
     * @return
     */
    @RequestMapping(value = "/010/ge/dtl", method = RequestMethod.GET, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> getHome010GeDtl(HttpServletRequest request,@ModelAttribute SearchParam reqParam) {
        logger.info("getHome010GeDtl() RequestParam:");
        ResultCode resultCode = ResultCode.SUCCESS;

        if (reqParam.getCYear() == null)
            throw new BaseException("cYear 는 필수 입력 값입니다. ");

        List<SiteYearGoal4Home> list =  goalService.searchSiteYearGoal4Home(reqParam);
        if (list.size() == 0)
            resultCode = ResultCode.NO_DATA;

        return new ResponseEntity<ResultValue>(new ResultValue(resultCode, list), HttpStatus.OK);
    }

    /**
     * 태양광 발전 목록
     * 
     * @param request
     * @param reqParam
     * @return
     */
    @RequestMapping(value = "/010/soleng/ls", method = RequestMethod.GET, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> getHome010SolEngLs(HttpServletRequest request, @ModelAttribute SearchParam reqParam) {
        logger.info("getHome010SolEngLs() RequestParam:" + reqParam);
        ResultCode resultCode = ResultCode.SUCCESS;

        if (reqParam.getCYear() == null)
            throw new BaseException("cYear 는 필수 입력 값입니다. ");

        String s = reqParam.getCYear();
        reqParam.setArrYear(s.split(","));

        List<ResSunPower> list = sunPowerService.searchYearSum(reqParam);
        if (list.size() == 0)
            resultCode = ResultCode.NO_DATA;

        return new ResponseEntity<ResultValue>(new ResultValue(resultCode, list), HttpStatus.OK);
    }
}