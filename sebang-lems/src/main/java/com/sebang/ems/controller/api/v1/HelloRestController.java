package com.sebang.ems.controller.api.v1;

import javax.servlet.http.HttpServletRequest;

import com.sebang.ems.domain.common.ResultCode;
import com.sebang.ems.domain.common.ResultPage;

import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;


@CrossOrigin(origins = "*")
@RestController
@RequestMapping("/api/v1")
public class HelloRestController {
	
	 @CrossOrigin(origins = "*")
	 @RequestMapping(value="/hello",method = RequestMethod.GET,consumes = MediaType.APPLICATION_JSON_VALUE)    
    public ResponseEntity<ResultPage> test( HttpServletRequest request) 
    { 
			
		 return new ResponseEntity<ResultPage>(new ResultPage(ResultCode.SUCCESS,null,0,0,10), HttpStatus.OK);
    }
}