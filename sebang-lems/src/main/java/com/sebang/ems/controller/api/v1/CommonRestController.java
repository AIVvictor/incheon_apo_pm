package com.sebang.ems.controller.api.v1;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import com.auth0.jwt.interfaces.DecodedJWT;
import com.sebang.ems.auth.jwt.JwtInfo;
import com.sebang.ems.component.validator.CustomCollectionValidator;
import com.sebang.ems.controller.BaseRestController;
import com.sebang.ems.domain.Exception.BaseException;
import com.sebang.ems.domain.common.ResultCode;
import com.sebang.ems.domain.common.ResultValue;
import com.sebang.ems.domain.common.SearchParam;
import com.sebang.ems.model.Gfiles;
import com.sebang.ems.model.KeyValue;
import com.sebang.ems.model.Menu;
import com.sebang.ems.model.Do.TCode;
import com.sebang.ems.model.Do.TErpDept;
import com.sebang.ems.model.Do.TFiles;
import com.sebang.ems.model.Do.TMenuInfo;
import com.sebang.ems.model.Dto.ReqMenuAuthMap;
import com.sebang.ems.model.Dto.ResMenuInfo;
import com.sebang.ems.service.CodeServiceImpl;
import com.sebang.ems.service.FileServiceImpl;
import com.sebang.ems.service.SiteServiceImpl;
import com.sebang.ems.storage.StorageFileNotFoundException;
import com.sebang.ems.storage.StorageService;
import com.sebang.ems.util.DateUtil;
import com.sebang.ems.util.Guid;
import com.sebang.ems.util.JwtUtil;
import com.sebang.ems.util.StringUtil;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindException;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

@CrossOrigin(origins = "*")
@RestController
@RequestMapping("/api/v1")
public class CommonRestController extends BaseRestController {

    public static final Logger logger = LoggerFactory.getLogger(CommonRestController.class);

    private final StorageService storageService;

    @Autowired
    CustomCollectionValidator customCollectionValidator;

    @Autowired
    FileServiceImpl fileService;

    @Autowired
    CodeServiceImpl codeService;

    @Autowired
    SiteServiceImpl siteService;

    @Autowired
    public CommonRestController(StorageService storageService) {
        this.storageService = storageService;
    }

    // 파일 다운로드
    @ResponseBody
    @GetMapping("/common/file/{gid}")
    public ResponseEntity<Resource> handleFileDownload(@PathVariable("gid") String gid,
            @RequestParam("sMenuId") String sMenuId, @RequestParam(value = "idFile", required = false) Long idFile) {
        logger.info("handleFileDownload filename:" + gid + ", sMenuId:" + sMenuId + ", idFile:" + idFile);

        String path = null;
        String filename = null;

        if (idFile != null) {
            TFiles f = fileService.findOneByIdFile(idFile);
            path = f.getsFilePath();
            filename = f.getsFileNm();
        } else {
            Gfiles f = fileService.findOneByGid(gid);
            if (f == null)
                throw new StorageFileNotFoundException("Invalid gid:" + gid);

            path = f.getsFilePath();
            filename = f.getsFileNm();
        }
        Resource file = storageService.loadAsResource(filename, path);
        return ResponseEntity.ok().header(HttpHeaders.CONTENT_TYPE, "application/octet-stream")
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + file.getFilename() + "\"")
                .body(file);
    }

    // 파일 업로드
    @PostMapping("/common/file")
    public ResponseEntity<?> handleFileUpload(@RequestParam(value = "file", required = false) MultipartFile file,
            @RequestParam(value = "cmdTag", required = false) String sCmdTag, @RequestParam("sMenuId") String sMenuId,
            @RequestParam(value = "gid", required = false) String gid,
            @RequestParam(value = "idFile", required = false) Long idFile) throws BaseException {
        logger.info("handleFileUpload" + " sMenuId:" + sMenuId + ", gid:" + gid + ", cmdTag:" + sCmdTag + " ,idfile:"
                + idFile);

        if (gid != null) {
            Gfiles g = fileService.findOneByGid(gid);
            if (g == null)
                throw new BaseException("gid is not exists!");
        }

        if ("D".equals(sCmdTag)) {

            if (gid == null) {
                // FieldError e = new FieldError("gid", "gid", "삭제시 gid는 필수 입력 값입니다. ");
                throw new BaseException("삭제시 gid는 필수 입력 값입니다.");
            }

            if (idFile == null) {
                throw new BaseException("삭제시 idFile는 필수 입력 값입니다. ");
            }

            fileService.deleteFile(idFile);
            return new ResponseEntity<ResultValue>(new ResultValue(ResultCode.SUCCESS, null), HttpStatus.OK);
        }

        if (file == null) {
            throw new BaseException("file 을 선택하지 않았습니다.");
        }

        logger.info("handleFileUpload filename:" + file.getOriginalFilename());

        String fid = Guid.genUUID();
        String subdir = DateUtil.getDateFormat(new Date());
        subdir += "/" + sMenuId + "/" + fid;

        storageService.store(file, subdir);

        // storageService.store(file,uiId+"/"+Guid.genUUID());
        // redirectAttributes.addFlashAttribute("message",
        // "You successfully uploaded " + file.getOriginalFilename() + "!");

        Gfiles f = new Gfiles();
        f.setsFileId(fid);
        f.setsFileNm(file.getOriginalFilename());
        f.setsScreenId(sMenuId);
        f.setsFilePath(subdir);
        if (gid != null)
            f.setGId(gid);
        if (idFile != null)
            f.setIdFile(idFile);

        fileService.upsertFiles(f);

        Gfiles ret = new Gfiles();
        ret.setIdFile(f.getIdFile());
        ret.setsFileId(f.getsFileId());
        ret.setGId(f.getGId());

        return new ResponseEntity<ResultValue>(new ResultValue(ResultCode.SUCCESS, ret), HttpStatus.OK);
    }

    // 권한별 메뉴 목록
    @RequestMapping(value = "/common/menu/auth", method = RequestMethod.GET, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> getCommonMenuAuth(HttpServletRequest request, @ModelAttribute SearchParam reqParam) {
        logger.info("getCommonMenuAuth() RequestParam:" + reqParam);

        String token = request.getHeader(JwtInfo.HEADER_NAME);

        DecodedJWT jwt = JwtUtil.tokenToJwt(token);
        logger.info("jwt claims.id     : " + jwt.getClaim("role").asString());

        // String role = jwt.getClaim("role").asString();

        String role = jwt.getClaim("role").asString();
        List<Menu> list = codeService.findMenu(role);

        List<TMenuInfo> allMenu = codeService.findMenuMst(null);
        List<ResMenuInfo> result = new ArrayList<ResMenuInfo>();
        for (Menu menu : list) {
            ResMenuInfo res = new ResMenuInfo(menu);
            String s = codeService.buildFullMenuName(allMenu, menu.getsMenuId());
            res.setsFullMenuNm(s);
            result.add(res);
        }
        return new ResponseEntity<ResultValue>(new ResultValue(ResultCode.SUCCESS, result), HttpStatus.OK);
    }

    // 권한별 메뉴 목록
    @RequestMapping(value = "/common/menu/auth/{auth}", method = RequestMethod.GET, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> getCommonMenuAuth(HttpServletRequest request, @PathVariable("auth") String auth) {
        logger.info("getCommonMenuAuth() RequestParam:" + auth);

        String token = request.getHeader(JwtInfo.HEADER_NAME);

        DecodedJWT jwt = JwtUtil.tokenToJwt(token);
        logger.info("jwt claims.id     : " + jwt.getClaim("role").asString());

        String role = jwt.getClaim("role").asString();
        if (!"SYS".equals(role.toUpperCase()))
            throw new BaseException("Invalid role");

        List<Menu> list = codeService.findMenu(auth);

        List<TMenuInfo> allMenu = codeService.findMenuMst(null);
        List<ResMenuInfo> result = new ArrayList<ResMenuInfo>();
        for (Menu menu : list) {
            ResMenuInfo res = new ResMenuInfo(menu);
            String s = codeService.buildFullMenuName(allMenu, menu.getsMenuId());
            res.setsFullMenuNm(s);
            result.add(res);
        }
        return new ResponseEntity<ResultValue>(new ResultValue(ResultCode.SUCCESS, result), HttpStatus.OK);
    }

    // 권한별 메뉴 목록 등록 [리스트]
    @RequestMapping(value = "/common/menu/auth", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> postCommonMenuAuth(HttpServletRequest request, @RequestBody @Valid List<ReqMenuAuthMap> reqParam,
            BindingResult bindingResult) throws BindException {
        logger.info("postCommonMenuAuth() RequestParam:" + reqParam);

        customCollectionValidator.validate(reqParam, bindingResult);

        if (bindingResult.hasErrors())
            throw new BindException(bindingResult);

        String token = request.getHeader(JwtInfo.HEADER_NAME);
        DecodedJWT jwt = JwtUtil.tokenToJwt(token);
        logger.info("jwt claims.id     : " + jwt.getClaim("role").asString());
        // String role = jwt.getClaim("role").asString();

        codeService.upsertAuthMenu(reqParam);
        return new ResponseEntity<ResultValue>(new ResultValue(ResultCode.SUCCESS, null), HttpStatus.OK);
    }

    // 권한별 메뉴 목록 등록 [리스트]
    // @RequestMapping(value = "/common/menu/auth/{authMenuCd}/{idMenuInfos}", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    // public ResponseEntity<?> postCommonMenuAuthLs(HttpServletRequest request,
    //         @PathVariable("authMenuCd") String authMenuCd, @PathVariable List<Long> idMenuInfos) {
    //     logger.info("postCommonMenuAuth() RequestParam:" + authMenuCd + "idMenuInfos: " + idMenuInfos.toString());
    //     String token = request.getHeader(JwtInfo.HEADER_NAME);
    //     DecodedJWT jwt = JwtUtil.tokenToJwt(token);
    //     logger.info("jwt claims.id     : " + jwt.getClaim("role").asString());

    //     // String role = jwt.getClaim("role").asString();

    //     List<TCode> codes = codeService.find("AUTH");
    //     boolean isContained = false;
    //     for(TCode code : codes)
    //     {
    //         if(code.getsCd().equals(authMenuCd))
    //             isContained =  true;
    //     }

    //     if(isContained == false)
    //         throw new BaseException("등록되지 않은 권한입니다. ");

    //     List<ReqMenuAuthMap> list = new ArrayList<ReqMenuAuthMap>();
    //     for (Long idMenuInfo : idMenuInfos) {
    //         ReqMenuAuthMap r = new ReqMenuAuthMap();
    //         r.setsAuthCd(authMenuCd);
    //         r.setIdMenuInfo((Long) idMenuInfo);
    //         list.add(r);
    //     }

    //     codeService.upsertAuthMenu(list);
    //     return new ResponseEntity<ResultValue>(new ResultValue(ResultCode.SUCCESS, null), HttpStatus.OK);
    // }

    // 권한별 메뉴 목록 삭제
    @RequestMapping(value = "/common/menu/auth/{authMenuCd}/{idMenuInfo}", method = RequestMethod.DELETE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> deleteCommonMenuAuth(HttpServletRequest request, @PathVariable("authMenuCd") String authMenuCd, @PathVariable("idMenuInfo") List<Long> idMenuInfos )
    {
        logger.info("deleteCommonMenuAuth() authMenuCd:" + authMenuCd + "idMenuInfo"+ idMenuInfos.toString());

        String token = request.getHeader(JwtInfo.HEADER_NAME);

        DecodedJWT jwt = JwtUtil.tokenToJwt(token);
        logger.info("jwt claims.id     : " + jwt.getClaim("role").asString());

        List<TCode> codes = codeService.find("AUTH");
        boolean isContained = false;
        for(TCode code : codes)
        {
            if(code.getsCd().equals(authMenuCd))
                isContained =  true;
        }
        if(isContained == false)
         throw new BaseException("등록되지 않은 권한입니다. ");

        //List<ReqMenuAuthMap> list = new ArrayList<ReqMenuAuthMap>();
        for (Long idMenuInfo : idMenuInfos) {
            ReqMenuAuthMap r = new ReqMenuAuthMap();
            r.setsAuthCd(authMenuCd);
            r.setIdMenuInfo((Long) idMenuInfo);
          //  list.add(r);
          codeService.deleteMenuAuthMap(r.toEntity());
        }

        // String role = jwt.getClaim("role").asString();
        //
        return new ResponseEntity<ResultValue>(new ResultValue(ResultCode.SUCCESS, null), HttpStatus.OK);
    }

    // 전체 메뉴 목록
    @RequestMapping(value = "/common/menu/all", method = RequestMethod.GET, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> getCommonMenuAll(HttpServletRequest request, @ModelAttribute SearchParam reqParam) {
        logger.info("getCommonMenuAuth() RequestParam:" + reqParam);

        String token = request.getHeader(JwtInfo.HEADER_NAME);

        DecodedJWT jwt = JwtUtil.tokenToJwt(token);
        logger.info("jwt claims.id     : " + jwt.getClaim("role").asString());

        // String role = jwt.getClaim("role").asString();
        List<TMenuInfo> list = codeService.findMenuMst(null);

        List<ResMenuInfo> result = new ArrayList<ResMenuInfo>();
        for (TMenuInfo menu : list) {
            ResMenuInfo res = new ResMenuInfo(menu);
            String s = codeService.buildFullMenuName(list, menu.getsMenuId());
            res.setsFullMenuNm(s);
            result.add(res);
        }

        return new ResponseEntity<ResultValue>(new ResultValue(ResultCode.SUCCESS, result), HttpStatus.OK);
    }

    // ERP 조직(부서목록)
    @RequestMapping(value = "/common/erp/dept/ls", method = RequestMethod.GET, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> getErpDeptLs(HttpServletRequest request, @ModelAttribute SearchParam reqParam,
            BindingResult bindingResult) throws ServletException, BindException {
        logger.info("getErpDeptLs() RequestParam:" + reqParam);

        String sDeptId = reqParam.getSDeptId();

        if (StringUtil.isEmpty(sDeptId)) {
            sDeptId = "310000";
        }
        if (bindingResult.hasErrors())
            throw new BindException(bindingResult);

        List<TErpDept> result = siteService.findChildDeptsByDeptId(sDeptId);

        return new ResponseEntity<ResultValue>(new ResultValue(ResultCode.SUCCESS, result), HttpStatus.OK);
    }

    /**
     * ERP 조직(부서목록)을 반환. internal node를 모두 leaf node 로도 표현하도록 추가
     * @param request
     * @param reqParam
     * @param bindingResult
     * @return
     * @throws ServletException
     * @throws BindException
     */
    @RequestMapping(value = "/common/erp/dept/ls/full", method = RequestMethod.GET, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> getErpDeptLsFull(HttpServletRequest request, @ModelAttribute SearchParam reqParam,
            BindingResult bindingResult) throws ServletException, BindException {
        logger.info("getErpDeptLsFull() RequestParam:" + reqParam);
        List<TErpDept> result=null;
		try {
			String sDeptId = reqParam.getSDeptId();

			if (sDeptId == null) {
				FieldError e = new FieldError("SearchParam", "sDeptId", "sDeptId 는 필수 입력 값입니다.");
				bindingResult.addError(e);
			}
			if (bindingResult.hasErrors())
				throw new BindException(bindingResult);

			result = siteService.findAllChildDeptsByDeptId(sDeptId);

			return new ResponseEntity<ResultValue>(new ResultValue(ResultCode.SUCCESS, result), HttpStatus.OK);
		} finally {
	        logger.info("getErpDeptLsFull() result:" + result);

		}
    }

    // 조회 기준 요청
    @RequestMapping(value = "/common/keylist", method = RequestMethod.GET, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> getKeylist(HttpServletRequest request, @ModelAttribute SearchParam reqParam) {
        logger.info("getKeylist() RequestParam:" + reqParam);

        if (reqParam.getSUiId() == null) {
            return new ResponseEntity<ResultValue>(new ResultValue(ResultCode.INVALID_INPUT_FIELD, null),
                    HttpStatus.OK);
        }

        HashMap<String, List<KeyValue>> result = codeService.buildKeylist(reqParam.getSUiId());

        return new ResponseEntity<ResultValue>(new ResultValue(ResultCode.SUCCESS, result), HttpStatus.OK);
    }

}