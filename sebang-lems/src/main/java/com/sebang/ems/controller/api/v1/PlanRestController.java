package com.sebang.ems.controller.api.v1;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import com.sebang.ems.component.validator.CustomCollectionValidator;
import com.sebang.ems.controller.BaseRestController;
import com.sebang.ems.domain.Exception.BaseException;
import com.sebang.ems.domain.common.ResultCode;
import com.sebang.ems.domain.common.ResultValue;
import com.sebang.ems.domain.common.SearchParam;
import com.sebang.ems.model.SiteYearGoal;
import com.sebang.ems.model.Do.TMrvPointGoal;
import com.sebang.ems.model.Do.VBau;
import com.sebang.ems.model.Do.VCompanyGoal;
import com.sebang.ems.model.Do.VMrvGoal;
import com.sebang.ems.model.Do.VSiteGoal;
import com.sebang.ems.model.Dto.ReqMrvPointGoal;
import com.sebang.ems.model.Dto.ReqSiteYearGoal;
import com.sebang.ems.service.GoalServiceImpl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindException;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@CrossOrigin(origins = "*")
@RestController
@RequestMapping("/api/v1")
public class PlanRestController extends BaseRestController {

    public static final Logger logger = LoggerFactory.getLogger(PlanRestController.class);

    @Autowired
    CustomCollectionValidator customCollectionValidator;

    @Autowired
    GoalServiceImpl goalService;

    // BAU분석 조회
    @RequestMapping(value = "/plan/211/ls", method = RequestMethod.GET, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> getPlan211Ls(HttpServletRequest request, @ModelAttribute SearchParam reqParam,
            BindingResult bindingResult) throws BindException {
        logger.info("getPlan211Ls() RequestParam:" + reqParam);

        ResultCode resCode = ResultCode.SUCCESS;
        if (reqParam.getCYear() == null) {
            FieldError e = new FieldError("SearchParam", "cYear", "cYear 은 필수입렵값입니다. ");
            bindingResult.addError(e);
        }

        if (reqParam.getCYearEnd() == null) {
            FieldError e = new FieldError("SearchParam", "cYearEnd", "cYearEnd 은 필수입렵값입니다. ");
            bindingResult.addError(e);
        }

        if (bindingResult.hasErrors()) {
            throw new BindException(bindingResult);
        }

        List<VBau> list = goalService.searchBau(reqParam);
        if (list.size() == 0)
            resCode = ResultCode.NO_DATA;

        return new ResponseEntity<ResultValue>(new ResultValue(resCode, list), HttpStatus.OK);
    }

    // 정부목표 전사목표 목록 조회
    @RequestMapping(value = "/plan/212/gov/ls", method = RequestMethod.GET, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> getPlan212GovLs(HttpServletRequest request, @ModelAttribute SearchParam reqParam) {
        logger.info("getPlan212GovLs() RequestParam:" + reqParam);

        List<VMrvGoal> result = goalService.searchMrvGoal(reqParam);

        return new ResponseEntity<ResultValue>(new ResultValue(ResultCode.SUCCESS, result), HttpStatus.OK);
    }

    // 정부목표 전사목표 상세 조회
    @RequestMapping(value = "/plan/212/gov/dtl", method = RequestMethod.GET, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> getPlan212GovDtl(HttpServletRequest request, @ModelAttribute SearchParam reqParam) {
        logger.info("getPlan212GovDtl() RequestParam:" + reqParam);
        List<VSiteGoal> result = goalService.searchSiteGoal(reqParam);
        return new ResponseEntity<ResultValue>(new ResultValue(ResultCode.SUCCESS, result), HttpStatus.OK);
    }

    // 정부목표 전사목표 목록 저장
    @RequestMapping(value = "/plan/212/gov/dtl", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> postPlan212GovDtl(HttpServletRequest request, @RequestBody @Valid ReqMrvPointGoal reqParam,
            BindingResult bindingResult) throws BindException {
        logger.info("postPlan212GovDtl() RequestParam:" + reqParam);

        // customCollectionValidator.validate(reqParam, bindingResult);

        String sModId = getUserId(request);
        if (sModId == null)
            throw new BaseException("Invalid token");

        if (bindingResult.hasErrors())
            throw new BindException(bindingResult);

        goalService.upsertMrvPointGoal(reqParam.toEntity());

        return new ResponseEntity<ResultValue>(new ResultValue(ResultCode.SUCCESS, null), HttpStatus.OK);
    }

    // 정부목표 전사목표 목록 삭제
    @RequestMapping(value = "/plan/212/gov/dtl", method = RequestMethod.DELETE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> deletePlan212GovDtl(HttpServletRequest request, @ModelAttribute SearchParam reqParam,
            BindingResult bindingResult) throws BindException {
        logger.info("deletePlan212GovDtl() RequestParam:" + reqParam);

        if (reqParam.getCYear() == null) {
            FieldError error = new FieldError("SearchParam", "cYear", "cYear 값은 필수 입력 값입니다.");
            bindingResult.addError(error);
            throw new BindException(bindingResult);
        }

        TMrvPointGoal mg = new TMrvPointGoal();
        mg.setcYear(reqParam.getCYear());

        goalService.clearMrvPointGoal(mg);

        return new ResponseEntity<ResultValue>(new ResultValue(ResultCode.SUCCESS, null), HttpStatus.OK);
    }

    // 정부목표 사업장별 목표 목록 조회
    @RequestMapping(value = "/plan/212/fac/ls", method = RequestMethod.GET, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> getPlan212FacLs(HttpServletRequest request, @ModelAttribute SearchParam reqParam) {
        logger.info("getPlan212FacLs() RequestParam:" + reqParam);

        List<VSiteGoal> result = goalService.searchSiteGoal(reqParam);
        return new ResponseEntity<ResultValue>(new ResultValue(ResultCode.SUCCESS, result), HttpStatus.OK);
    }

    // 사내목표 온실가스 목표 목록 조회
    @CrossOrigin(origins = "*")
    @RequestMapping(value = "/plan/213/ghg/ls", method = RequestMethod.GET, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> getPLan213GhgLs(HttpServletRequest request, @ModelAttribute SearchParam reqParam) {
        logger.info("getPLan213GhgLs() RequestParam:" + reqParam);

        List<VCompanyGoal> list = goalService.searchSiteMonGoal(reqParam);

        return new ResponseEntity<ResultValue>(new ResultValue(ResultCode.SUCCESS, list), HttpStatus.OK);
    }

    // 사내목표 온실가스 목표 목록 저장
    @RequestMapping(value = "/plan/213/ghg/ls", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> postPlan213GhgLs(HttpServletRequest request,
            @Valid @RequestBody List<ReqSiteYearGoal> reqParam, BindingResult bindingResult) throws BindException {
        logger.info("postPlan213GhgLs() RequestParam:" + convertString(reqParam));

        customCollectionValidator.validate(reqParam, bindingResult);

        if (bindingResult.hasErrors())
            throw new BindException(bindingResult);

        List<SiteYearGoal> s = new ArrayList<SiteYearGoal>();
        for (ReqSiteYearGoal y : reqParam)
            s.add(y.toEntity());

        goalService.upsertSiteYearsGoal(s);

        return new ResponseEntity<ResultValue>(new ResultValue(ResultCode.SUCCESS, null), HttpStatus.OK);
    }

    // 사내목표 온실가스 목표 저장
    @RequestMapping(value = "/plan/213/ghg/dtl", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> postPlan213GhgDtl(HttpServletRequest request, @Valid @RequestBody ReqSiteYearGoal reqParam,
            BindingResult bindingResult) throws BindException {
        logger.info("postPlan213GhgDtl() RequestParam:" + reqParam);

        if (bindingResult.hasErrors())
            throw new BindException(bindingResult);

        List<SiteYearGoal> s = new ArrayList<SiteYearGoal>();
        s.add(reqParam.toEntity());

        goalService.upsertSiteYearsGoal(s);
        return new ResponseEntity<ResultValue>(new ResultValue(ResultCode.FAILED, null), HttpStatus.OK);
    }
}