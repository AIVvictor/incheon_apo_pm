package com.sebang.ems.controller.api.v1;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import com.sebang.ems.component.validator.CustomCollectionValidator;
import com.sebang.ems.controller.BaseRestController;
import com.sebang.ems.domain.Exception.BaseException;
import com.sebang.ems.domain.common.ResultCode;
import com.sebang.ems.domain.common.ResultValue;
import com.sebang.ems.domain.common.SearchParam;
import com.sebang.ems.model.Employee;
import com.sebang.ems.model.SiteDept;
import com.sebang.ems.model.Do.TCat;
import com.sebang.ems.model.Do.TCefMod;
import com.sebang.ems.model.Do.TCode;
import com.sebang.ems.model.Do.TCorpInfo;
import com.sebang.ems.model.Do.TSiteInfo;
import com.sebang.ems.model.Do.TSunPower;
import com.sebang.ems.model.Do.TUsers;
import com.sebang.ems.model.Do.VEngCef;
import com.sebang.ems.model.Do.VEngPoint;
import com.sebang.ems.model.Dto.ReqCat;
import com.sebang.ems.model.Dto.ReqCefMod;
import com.sebang.ems.model.Dto.ReqCode;
import com.sebang.ems.model.Dto.ReqCorpInfo;
import com.sebang.ems.model.Dto.ReqDeptMap;
import com.sebang.ems.model.Dto.ReqEngCef;
import com.sebang.ems.model.Dto.ReqEngCefEtc;
import com.sebang.ems.model.Dto.ReqEngPoint;
import com.sebang.ems.model.Dto.ReqSite;
import com.sebang.ems.model.Dto.ReqSunPower;
import com.sebang.ems.service.CefServiceImpl;
import com.sebang.ems.service.CodeServiceImpl;
import com.sebang.ems.service.EmployeeServiceImpl;
import com.sebang.ems.service.InfoServiceImpl;
import com.sebang.ems.service.SiteServiceImpl;
import com.sebang.ems.service.SunPowerServiceImpl;
import com.sebang.ems.util.StringUtil;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.validation.BindException;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@CrossOrigin(origins = "*")
@RestController
@RequestMapping("/api/v1/info")
public class InfoRestController extends BaseRestController {

    public static final Logger logger = LoggerFactory.getLogger(InfoRestController.class);

    @Autowired
    CustomCollectionValidator customCollectionValidator;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    InfoServiceImpl infoService;

    @Autowired
    EmployeeServiceImpl employeeService;

    @Autowired
    SiteServiceImpl siteService;

    @Autowired
    CefServiceImpl cefService;

    @Autowired
    SunPowerServiceImpl sunPowerService;

    /**
     * 법인 상세 조회
     * @param request
     * @param reqParam
     * @return
     */
    @RequestMapping(value = "/511/dtl", method = RequestMethod.GET, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> getInfo511Dtl(HttpServletRequest request, @ModelAttribute SearchParam reqParam) {
        logger.info("getInfo511Dtl() RequestParam:" + reqParam);

        TCorpInfo result = siteService.findCorp("");
        if (result == null)
            throw new BaseException("can't find Corporation Infomation");

        return new ResponseEntity<ResultValue>(new ResultValue(ResultCode.SUCCESS, result), HttpStatus.OK);
    }

    /**
     * 법인 관리 저장.
     * @param request
     * @param reqParam
     * @param bindingResult
     * @return
     * @throws BindException
     */
    @RequestMapping(value = "/511/dtl", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> postInfoDtl(HttpServletRequest request, @RequestBody @Valid ReqCorpInfo reqParam,
            BindingResult bindingResult) throws BindException {
        logger.info("postInfoDtl() RequestParam:" + reqParam);

        if (bindingResult.hasErrors())
            throw new BindException(bindingResult);

        String sModId = getUserId(request);
        if (sModId == null)
            throw new BaseException("Invalid Token");

        reqParam.setSModId(sModId);
        siteService.upsertCorp(reqParam.toEntity());
        return new ResponseEntity<ResultValue>(new ResultValue(ResultCode.SUCCESS, null), HttpStatus.OK);
    }

    /**
     * 사업장 관리
     * @param request
     * @param reqParam
     * @return
     */
    @RequestMapping(value = "/521/ls", method = RequestMethod.GET, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> getInfo521Ls(HttpServletRequest request, @ModelAttribute SearchParam reqParam) {
        logger.info("getInfo521Ls() RequestParam:" + reqParam);
        List<TSiteInfo> result = new ArrayList<TSiteInfo>();
        if (reqParam.getIdSite() != null) {
            result = infoService.findSiteByKey(reqParam.getIdSite());
        } else
            result = infoService.findSiteByKey(null);
        return new ResponseEntity<ResultValue>(new ResultValue(ResultCode.SUCCESS, result), HttpStatus.OK);
    }


    /**
     * deprecated 사업장관리 상세 조회
     * @param request
     * @param reqParam
     * @return
     */
    @RequestMapping(value = "/521/dtl", method = RequestMethod.GET, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> getInfo521Dtl(HttpServletRequest request, @ModelAttribute SearchParam reqParam) {
        TSiteInfo result = null;
        logger.info("getInfo521Dtl() RequestParam:" + reqParam);
        if (reqParam.getIdSite() != null) {
            List<TSiteInfo> res = null;
            res = infoService.findSiteByKey(reqParam.getIdSite());
            if (res.size() > 0)
                result = res.get(0);
        }

        return new ResponseEntity<ResultValue>(new ResultValue(ResultCode.SUCCESS, result), HttpStatus.OK);
    }

    /**
     * 사업장관리 목록 삭제
     * @param request
     * @param reqParam
     * @param bindingResult
     * @return
     * @throws BindException
     */
    @RequestMapping(value = "/521/ls", method = RequestMethod.DELETE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> deleteInfo521(HttpServletRequest request, @ModelAttribute SearchParam reqParam,
            BindingResult bindingResult) throws BindException {
        logger.info("deleteInfo521() RequestParam:" + reqParam);

        if (reqParam.getIdSites() == null) {
            FieldError e = new FieldError("SearchParam", "idSites", "idSites 는 필수 입력 값입니다.");
            bindingResult.addError(e);
        }

        if (bindingResult.hasErrors())
            throw new BindException(bindingResult);

        String[] ss = reqParam.getIdSites().split(",");

        ArrayList<Long> idSiteList = new ArrayList<Long>();
        for (String s : ss) {
            try {
                Long l = Long.parseLong(s);
                idSiteList.add(l);
            } catch (NumberFormatException e) {
                throw new BaseException(e.getMessage());
            }
        }

        String sModId = getUserId(request);
        if (sModId == null)
            throw new BaseException("Invalid token");

        infoService.deleteSites(idSiteList, sModId);

        return new ResponseEntity<ResultValue>(new ResultValue(ResultCode.SUCCESS, null), HttpStatus.OK);
    }

    /**
     * 사업장관리 등록/수정
     * @param request
     * @param reqParam
     * @param bindingResult
     * @return
     * @throws BindException
     */
    @RequestMapping(value = "/521/dtl", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> postInfo521Dtl(HttpServletRequest request, @RequestBody @Valid ReqSite reqParam,
            BindingResult bindingResult) throws BindException {
        logger.info("postInfo521Dtl() RequestParam:" + reqParam);

        if (bindingResult.hasErrors())
            throw new BindException(bindingResult);

        String sModId = getUserId(request);
        if (sModId == null)
            throw new BaseException("Invalid token");

        reqParam.setsModId(sModId);
        infoService.upsertSite(reqParam.toEntity());

        return new ResponseEntity<ResultValue>(new ResultValue(ResultCode.SUCCESS, null), HttpStatus.OK);
    }

    /**
     * 조직관리 목록 조회
     * @param request
     * @param reqParam
     * @return
     */
    @RequestMapping(value = "/531/ls", method = RequestMethod.GET, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> getInfo531Ls(HttpServletRequest request, @ModelAttribute SearchParam reqParam) {
        logger.info("getInfo531Ls() RequestParam:" + reqParam);
        Long idSite = reqParam.getIdSite();

        List<SiteDept> result = infoService.findSiteDept(idSite);
        return new ResponseEntity<ResultValue>(new ResultValue(ResultCode.SUCCESS, result), HttpStatus.OK);
    }


    /**
     * 조직관리 목록 등록/수정 [다수]
     * @param request
     * @param reqParam
     * @param bindingResult
     * @return
     * @throws BindException
     */
    @RequestMapping(value = "/531/ls", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> postInfo531ls(HttpServletRequest request, @RequestBody @Valid List<ReqDeptMap> reqParam,
            BindingResult bindingResult) throws BindException {
        logger.info("postInfo531ls() RequestParam:" + convertString(reqParam));
        customCollectionValidator.validate(reqParam, bindingResult);

        if (bindingResult.hasErrors())
            throw new BindException(bindingResult);

        siteService.addDeptToSite(reqParam);

        return new ResponseEntity<ResultValue>(new ResultValue(ResultCode.SUCCESS, null), HttpStatus.OK);
    }

    /**
     * 조직관리 목록 등록/수정
     * @param request
     * @param reqParam
     * @param bindingResult
     * @return
     * @throws BindException
     */
    @RequestMapping(value = "/531/dtl", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> postInfo531Dtl(HttpServletRequest request, @RequestBody @Valid ReqDeptMap reqParam,
            BindingResult bindingResult) throws BindException {
        logger.info("postInfo531Dtl() RequestParam:" + reqParam);

        if (bindingResult.hasErrors())
            throw new BindException(bindingResult);

        siteService.addDeptToSite(reqParam);
        return new ResponseEntity<ResultValue>(new ResultValue(ResultCode.SUCCESS, null), HttpStatus.OK);
    }


    /**
     * 조직관리 하위 조직 삭제
     * @param request
     * @param reqParam
     * @return
     */
    @RequestMapping(value = "/531/ls/sub", method = RequestMethod.DELETE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> deleteInfo531SubLs(HttpServletRequest request, @RequestParam Map<String,String> reqParam
            ) {
        logger.info("deleteInfo531LsSub() RequestParam:" +convertString(reqParam));
        List<ReqDeptMap> param =   ReqDeptMap.toList(reqParam); 
          
        siteService.removeDeptFromSite(param);
        return new ResponseEntity<ResultValue>(new ResultValue(ResultCode.SUCCESS, null), HttpStatus.OK);
    }

    /**
     * 사용자관리 목록 조회
     * @param request
     * @param reqParam
     * @return
     */
    @RequestMapping(value = "/541/ls", method = RequestMethod.GET, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> getInfo541Ls(HttpServletRequest request, @ModelAttribute SearchParam reqParam) {
        logger.info("getInfo541ls() RequestParam: sEmpId=" + reqParam.sEmpId);

        // 사용자 ID가 zero length string인 경우 null 로 치환한다.
		if (reqParam.sEmpId != null && reqParam.sEmpId.trim().length() == 0) {
			reqParam.sEmpId = null;
		}
        
        List<Employee> result = employeeService.searchEmployee(reqParam);
        return new ResponseEntity<ResultValue>(new ResultValue(ResultCode.SUCCESS, result), HttpStatus.OK);
    }

    /**
     * 권한 목록 관리
     * @param request
     * @param reqParam
     * @return
     */
    @CrossOrigin(origins = "*")
    @RequestMapping(value = "/541/ath/ls", method = RequestMethod.GET, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> getInfo541AthLs(HttpServletRequest request, @ModelAttribute SearchParam reqParam) {
        logger.info("getInfo541AthLs() RequestParam:" + reqParam);

        List<TCode> result = infoService.findAuth();
        return new ResponseEntity<ResultValue>(new ResultValue(ResultCode.SUCCESS, result), HttpStatus.OK);
    }

    /**
     * 사용자 권한 관리 등록 수정
     * @param request
     * @param reqParam
     * @param bindingResult
     * @return
     * @throws BindException
     * @throws ServletException
     */
    @RequestMapping(value = "/541/ls", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> postInfo541Ls(HttpServletRequest request, @RequestBody @Valid List<Employee> reqParam,
            BindingResult bindingResult) throws BindException, ServletException {
        logger.info("postInfo541Ls() RequestParam:" + convertString(reqParam));

        customCollectionValidator.validate(reqParam, bindingResult);

        if (bindingResult.hasErrors())
            throw new BindException(bindingResult);

        String sModId = getUserId(request);
        if (sModId == null)
            throw new BaseException("Invalid token");

        for (Employee e : reqParam)
            e.setsModId(sModId);

        employeeService.upsertEmployees(reqParam);
        return new ResponseEntity<ResultValue>(new ResultValue(ResultCode.SUCCESS, null), HttpStatus.OK);
    }

    /**
     * 사용자 암호 초기화 및 변경
     * @param request
     * @param reqParam
     * @param bindingResult
     * @return
     * @throws BindException
     * @throws ServletException
     */
    @RequestMapping(value = "/591/ls/pw/init", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> postInfo591LsPwd(HttpServletRequest request, @RequestBody List<Employee> reqParam,
            BindingResult bindingResult) throws BindException, ServletException {
        logger.info("postInfo591LsPwInit() RequestParam:" + convertString(reqParam));

        customCollectionValidator.validate(reqParam, bindingResult);
        for (Employee em : reqParam) {
            if (em.getsEmpId() == null) {
                FieldError e = new FieldError("Employee", "sEmpId", "사번(sEmpId)은 필수 입력 값입니다.");
                bindingResult.addError(e);
            }

            if (em.getsEmpMail() == null) {
                FieldError e = new FieldError("Employee", "EmpMail", "이메일(sEmpMail)은 필수 입력 값입니다.");
                bindingResult.addError(e);
            }
        }

        if (bindingResult.hasErrors())
            throw new BindException(bindingResult);

        String sModId = getUserId(request);
        if (sModId == null)
            throw new BaseException("Invalid token");

        for (Employee e : reqParam)
            e.setsModId(sModId);

        employeeService.upsertEmployee(reqParam);

        return new ResponseEntity<ResultValue>(new ResultValue(ResultCode.SUCCESS, null), HttpStatus.OK);
    }

    /**
     * 사용자별 권한 메뉴 목록 등록
     * @param request
     * @param reqParam
     * @param bindingResult
     * @return
     * @throws BindException
     * @throws ServletException
     */
    @RequestMapping(value = "/591/menu/auth", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> postInfo591MenuAuth(HttpServletRequest request, @RequestBody Employee reqParam,
            BindingResult bindingResult) throws BindException, ServletException {
        logger.info("postInfo591MenuAuth() RequestParam:" + reqParam);

        // TODO: 사용자별 권한 메뉴 목록 등록

        return new ResponseEntity<ResultValue>(new ResultValue(ResultCode.SUCCESS, null), HttpStatus.OK);
    }

    /**
     * 사용자 상세 정보 편집 저장
     * @param request
     * @param reqParam
     * @param bindingResult
     * @return
     * @throws BindException
     */
    @RequestMapping(value = "/5411/dtl", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> postInfo541Dtl(HttpServletRequest request, @RequestBody Employee reqParam,
            BindingResult bindingResult) throws BindException {
        logger.info("postInfo541Dtl() RequestParam:" + reqParam);

        if (bindingResult.hasErrors())
            throw new BindException(bindingResult);

        String sModId = getUserId(request);
        if (sModId == null)
            throw new BaseException("Invalid token");

        reqParam.setsModId(sModId);

        if(StringUtil.isEmpty(reqParam.getsEmpId()))
            throw new BaseException("사번(sEmpId)는 필수 입력 값입니다. ");


        // 사원번호로 중복 체크
        TUsers re = employeeService.findUserByEmpId(reqParam.getsEmpId());
        if (re != null) {
            reqParam.setIdEmp(re.getIdEmp());

            // 암호 변경 인경우 
            if(!StringUtil.isEmpty(reqParam.getsNewPwd())){
                if(reqParam.getsEmpPwd() == null)
                    throw new BaseException("평문암호(sEmpPwd)는 필수 입력 값입니다. ");

                if (!passwordEncoder.matches(reqParam.getsEmpPwd(),re.getsEmpPwd())) {
                    return new ResponseEntity<ResultValue>(new ResultValue(ResultCode.NOT_MATCH_PWD, "암호가 일치하지 않습니다."), HttpStatus.OK);
                }
                reqParam.setsEmpPwd(reqParam.getsNewPwd()); 
            }else
                reqParam.setsEmpPwd(re.getsEmpPwd());

            if (reqParam.getcDelYn() == null)
                reqParam.setcDelYn(re.getcDelYn());
            
            if (reqParam.getsEmpNm() == null)
                reqParam.setsEmpNm(re.getsEmpNm());
            
            
        } else {
            // 최초 등록일 경우 암호
            if(!StringUtil.isEmpty(reqParam.getsEmpMail())){
                if(reqParam.getsEmpPwd() == null)
                    throw new BaseException("이메일(sEmpMail)는 필수 입력 값입니다. ");
            }

            String email = reqParam.getsEmpMail();
            String[] array = email.split("@");
            if (reqParam.getsEmpPwd() == null) {
                logger.info("기본 암호로 등록");
                reqParam.setsEmpPwd(array[0]);
            } else {
                logger.info("사용자 입력 암호로 등록");
            }

            String sEmpPwd = reqParam.getsEmpPwd();
            if (sEmpPwd != null) {
                String encPassword = passwordEncoder.encode(sEmpPwd);
                reqParam.setsEmpPwd(encPassword);
            }
        }


        employeeService.upsertEmployee(reqParam);
        return new ResponseEntity<ResultValue>(new ResultValue(ResultCode.SUCCESS, null), HttpStatus.OK);
    }

    /**
     * 배출계수관리 직접배출 에너지 목록 조회
     * @param request
     * @param reqParam
     * @return
     */
    @RequestMapping(value = "/5511/de/ls", method = RequestMethod.GET, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> getInfo5511Dels(HttpServletRequest request, @ModelAttribute SearchParam reqParam) {
        logger.info("getInfo5511Dels() RequestParam:" + reqParam);

        // String sEmtCd = null;
        // List<VEngCef> result = cefService.findByEmt(sEmtCd);

        if (reqParam.getSCefCd() == null)
            throw new BaseException("S_CEF_CD is Empty");

        if (reqParam.getSEmtCd() == null)
            throw new BaseException("S_EMT_CD is Empty");

        List<VEngCef> result = cefService.search(reqParam);
        return new ResponseEntity<ResultValue>(new ResultValue(ResultCode.SUCCESS, result), HttpStatus.OK);
    }

    /**
     * 배출계수관리 직접배출 에너지 목록 저장     * 
     * @param request
     * @param reqParam
     * @param bindingResult
     * @return
     * @throws BindException
     */
    @RequestMapping(value = "/5511/de/ls", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> postInfo5511DeLs(HttpServletRequest request, @RequestBody @Valid List<ReqEngCef> reqParam,
            BindingResult bindingResult) throws BindException {
        logger.info("postInfo5511DeLs() RequestParam:" + convertString(reqParam));

        customCollectionValidator.validate(reqParam, bindingResult);

        if (bindingResult.hasErrors())
            throw new BindException(bindingResult);

        String sModId = getUserId(request);
        if (sModId == null)
            throw new BaseException("Invalid token");

        for (ReqEngCef e : reqParam)
            e.setsModId(sModId);

        cefService.upsert(reqParam);

        return new ResponseEntity<ResultValue>(new ResultValue(ResultCode.SUCCESS, null), HttpStatus.OK);
    }

    /**
     * 배출계수관리 직접배출 에너지 목록 삭제
     * @param request
     * @param reqParam
     * @param bindingResult
     * @return
     * @throws BindException
     */
    @RequestMapping(value = "/5511/de/ls", method = RequestMethod.DELETE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> deleteInfo5511DeLs(HttpServletRequest request, @ModelAttribute SearchParam reqParam,
            BindingResult bindingResult) throws BindException {
        logger.info("deleteInfo5511DeLs() RequestParam:" + reqParam);

        if (reqParam.getIdList() == null) {
            FieldError e = new FieldError("SearchParam", "idList", "idList 는 필수 입력 값입니다.");
            bindingResult.addError(e);
        }

        if (bindingResult.hasErrors())
            throw new BindException(bindingResult);

        List<ReqEngCef> list = new ArrayList<ReqEngCef>();
        String[] ids = reqParam.getIdList().split(",");
        try {
            for (String s : ids) {
                ReqEngCef c = new ReqEngCef();
                c.setIdEngCef(Long.parseLong(s));
                list.add(c);
            }
        } catch (Exception ex) {
            throw new BaseException(ex.getMessage());
        }

        String sModId = getUserId(request);
        if (sModId == null)
            throw new BaseException("Invalid token");

        cefService.delete(list, sModId);
        return new ResponseEntity<ResultValue>(new ResultValue(ResultCode.SUCCESS, null), HttpStatus.OK);
    }

    /**
     * 배출계수관리 직접배출 배출계수 목록 조회
     * @param request
     * @param reqParam
     * @param bindingResult
     * @return
     * @throws BindException
     */
    @RequestMapping(value = "/5511/def/ls", method = RequestMethod.GET, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> getInfo5511DefLs(HttpServletRequest request, @ModelAttribute SearchParam reqParam,
            BindingResult bindingResult) throws BindException {
        logger.info("getInfo5511DefLs() RequestParam:" + reqParam);

        if (reqParam.getIdEngCef() == null) {
            FieldError e = new FieldError("SearchParam", "idEngCef", "idEngCef 는 필수 입력 값입니다. ");
            bindingResult.addError(e);
        }

        if (bindingResult.hasErrors())
            throw new BindException(bindingResult);

        Long idEngCef = reqParam.getIdEngCef();
        List<TCefMod> result = cefService.findMod(idEngCef);
        return new ResponseEntity<ResultValue>(new ResultValue(ResultCode.SUCCESS, result), HttpStatus.OK);
    }

    /**
     * 배출계수관리 직접배출 배출계수 목록 저장
     * @param request
     * @param reqParam
     * @param bindingResult
     * @return
     * @throws BindException
     */
    @RequestMapping(value = "/5511/def/ls", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> postInfo5511DefLs(HttpServletRequest request, @RequestBody @Valid List<ReqCefMod> reqParam,
            BindingResult bindingResult) throws BindException {
        logger.info("postInfo5511DefLs() RequestParam:" + convertString(reqParam));
        customCollectionValidator.validate(reqParam, bindingResult);

        for (ReqCefMod param : reqParam) {
        	if (StringUtils.isEmpty(param.getcYear())) {
        		FieldError e = new FieldError("RequestParam", "cYear", "cYear 는 필수 입력 값입니다. ");
        		bindingResult.addError(e);
        	}
        	if (param.getnHeat() == null) {
        		FieldError e = new FieldError("RequestParam", "nHeat", "nHeat 는 필수 입력 값입니다. ");
        		bindingResult.addError(e);
        	}
        	if (param.getnTotHeat() == null) {
        		FieldError e = new FieldError("RequestParam", "nTotHeat", "nTotHeat 는 필수 입력 값입니다. ");
        		bindingResult.addError(e);
        	}
        	if (param.getnCo2Cef() == null) {
        		FieldError e = new FieldError("RequestParam", "nCo2Cef", "nCo2Cef 는 필수 입력 값입니다. ");
        		bindingResult.addError(e);
        	}
        	if (param.getnCh4Cef() == null) {
        		FieldError e = new FieldError("RequestParam", "nCh4Cef", "nCh4Cef 는 필수 입력 값입니다. ");
        		bindingResult.addError(e);
        	}
        	if (param.getnN2oCef() == null) {
        		FieldError e = new FieldError("RequestParam", "nN2oCef", "nN2oCef 는 필수 입력 값입니다. ");
        		bindingResult.addError(e);
        	}
        	if (param.getnModCal() == null) {
        		FieldError e = new FieldError("RequestParam", "nModCal", "nModCal 는 필수 입력 값입니다. ");
        		bindingResult.addError(e);
        	}
        	if (bindingResult.hasErrors())
        		throw new BindException(bindingResult);
        }
        
        

        String sModId = getUserId(request);
        if (sModId == null)
            throw new BaseException("Invalid token");

        for (ReqCefMod e : reqParam)
            e.setsModId(sModId);

        
        

        cefService.upsertCefMod(reqParam);
        return new ResponseEntity<ResultValue>(new ResultValue(ResultCode.SUCCESS, null), HttpStatus.OK);
    }

    /**
     * 배출계수관리 직접배출 배출계수 목록 삭제
     * @param request
     * @param reqParam
     * @param bindingResult
     * @return
     * @throws BindException
     */
    @RequestMapping(value = "/5511/def/ls", method = RequestMethod.DELETE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> deleteInfo5511DefLs(HttpServletRequest request, @ModelAttribute SearchParam reqParam,
            BindingResult bindingResult) throws BindException {
        logger.info("deleteInfo5511DefLs() RequestParam:" + reqParam);
        if (reqParam.getIdList() == null) {
            FieldError e = new FieldError("SearchParam", "idList", "idList 는 필수 입력 값입니다.");
            bindingResult.addError(e);
        }

        if (bindingResult.hasErrors())
            throw new BindException(bindingResult);

        List<ReqCefMod> list = new ArrayList<ReqCefMod>();
        String[] ids = reqParam.getIdList().split(",");
        try {
            for (String s : ids) {
                ReqCefMod c = new ReqCefMod();
                c.setIdCefMod(Long.parseLong(s));
                list.add(c);
            }
        } catch (Exception ex) {
            throw new BaseException(ex.getMessage());
        }

        String sModId = getUserId(request);
        if (sModId == null)
            throw new BaseException("Invalid token");

        cefService.deleteCefMod(list, sModId);

        return new ResponseEntity<ResultValue>(new ResultValue(ResultCode.SUCCESS, null), HttpStatus.OK);
    }

    /**
     * 배출계수관리 간접배출 에너지 목록 조회
     * @param request
     * @param reqParam
     * @param bindingResult
     * @return
     * @throws BindException
     */
    @RequestMapping(value = "/5521/ie/ls", method = RequestMethod.GET, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> getInfo5521IeLs(HttpServletRequest request, @ModelAttribute SearchParam reqParam,
            BindingResult bindingResult) throws BindException {
        logger.info("getInfo5521IeLs() RequestParam:" + reqParam);

        // String sEmtCd = null;
        // List<VEngCef> result = cefService.findByEmt(sEmtCd);
        if (reqParam.getSCefCd() == null) {
            FieldError e = new FieldError("SearchParam", "sCefCd", "sCefCd 는 필수 입력 값입니다.");
            bindingResult.addError(e);
        }

        if (reqParam.getSEmtCd() == null) {
            FieldError e = new FieldError("SearchParam", "sEmtCd", "sEmtCd 는 필수 입력 값입니다.");
            bindingResult.addError(e);
        }

        if (bindingResult.hasErrors())
            throw new BindException(bindingResult);

        List<VEngCef> result = cefService.search(reqParam);

        return new ResponseEntity<ResultValue>(new ResultValue(ResultCode.SUCCESS, result), HttpStatus.OK);
    }

    /**
     * 배출계수관리 간접배출 에너지 목록 저장
     * @param request
     * @param reqParam
     * @param bindingResult
     * @return
     * @throws BindException
     */
    @RequestMapping(value = "/5521/ie/ls", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> postInfo5521IeLs(HttpServletRequest request, @RequestBody List<ReqEngCef> reqParam,
            BindingResult bindingResult) throws BindException {
        logger.info("postInfo5521IeLs() RequestParam:" + convertString(reqParam));

        customCollectionValidator.validate(reqParam, bindingResult);

        if (bindingResult.hasErrors())
            throw new BindException(bindingResult);

        String sModId = getUserId(request);
        if (sModId == null)
            throw new BaseException("Invalid token");

        for (ReqEngCef c : reqParam)
            c.setsModId(sModId);

        cefService.upsert(reqParam);
        return new ResponseEntity<ResultValue>(new ResultValue(ResultCode.SUCCESS, null), HttpStatus.OK);
    }

    /**
     * 배출계수관리 간접배출 에너지 목록 삭제
     * @param request
     * @param reqParam
     * @param bindingResult
     * @return
     * @throws BindException
     */
    @RequestMapping(value = "/5521/ie/ls", method = RequestMethod.DELETE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> deleteInfo5521IeLs(HttpServletRequest request, @ModelAttribute SearchParam reqParam,
            BindingResult bindingResult) throws BindException {
        logger.info("deleteInfo5521IeLs() RequestParam:" + reqParam);

        if (reqParam.getIdList() == null) {
            FieldError e = new FieldError("SearchParam", "idList", "idList 는 필수 입력 값입니다.");
            bindingResult.addError(e);
        }

        if (bindingResult.hasErrors())
            throw new BindException(bindingResult);

        List<ReqEngCef> list = new ArrayList<ReqEngCef>();
        String[] ids = reqParam.getIdList().split(",");
        try {
            for (String s : ids) {
                ReqEngCef c = new ReqEngCef();
                c.setIdEngCef(Long.parseLong(s));
                list.add(c);
            }
        } catch (Exception ex) {
            throw new BaseException(ex.getMessage());
        }

        String sModId = getUserId(request);
        if (sModId == null)
            throw new BaseException("Invalid token");
        cefService.delete(list, sModId);

        return new ResponseEntity<ResultValue>(new ResultValue(ResultCode.SUCCESS, null), HttpStatus.OK);

    }

    /**
     * 배출계수관리 간접배출 배출계수 목록 조회
     * @param request
     * @param reqParam
     * @param bindingResult
     * @return
     * @throws BindException
     */
    @RequestMapping(value = "/5521/ief/ls", method = RequestMethod.GET, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> getInfo5521IefLs(HttpServletRequest request, @ModelAttribute SearchParam reqParam,
            BindingResult bindingResult) throws BindException {
        logger.info("getInfo5521IefLs() RequestParam:" + reqParam);
        if (reqParam.getIdEngCef() == null) {
            FieldError e = new FieldError("SearchParam", "idEngCef", "idEngCef 는 필수 입력 값입니다. ");
            bindingResult.addError(e);
        }

        if (bindingResult.hasErrors())
            throw new BindException(bindingResult);

        Long idEngCef = reqParam.getIdEngCef();
        List<TCefMod> result = cefService.findMod(idEngCef);
        return new ResponseEntity<ResultValue>(new ResultValue(ResultCode.SUCCESS, result), HttpStatus.OK);
    }

    /**
     * 배출계수관리 간접배출 배출계수 목록 저장
     * @param request
     * @param reqParam
     * @param bindingResult
     * @return
     * @throws BindException
     */
    @RequestMapping(value = "/5521/ief/ls", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> postInfo5521IefLs(HttpServletRequest request, @RequestBody @Valid List<ReqCefMod> reqParam,
            BindingResult bindingResult) throws BindException {
        logger.info("postInfo5521IefLs() RequestParam:" + convertString(reqParam));
        customCollectionValidator.validate(reqParam, bindingResult);
        
        for (ReqCefMod param : reqParam) {
        	if (StringUtils.isEmpty(param.getcYear())) {
        		FieldError e = new FieldError("RequestParam", "cYear", "cYear 는 필수 입력 값입니다. ");
        		bindingResult.addError(e);
        	}
        	if (param.getnHeat() == null) {
        		FieldError e = new FieldError("RequestParam", "nHeat", "nHeat 는 필수 입력 값입니다. ");
        		bindingResult.addError(e);
        	}
        	if (param.getnTotHeat() == null) {
        		FieldError e = new FieldError("RequestParam", "nTotHeat", "nTotHeat 는 필수 입력 값입니다. ");
        		bindingResult.addError(e);
        	}
        	if (param.getnCo2Cef() == null) {
        		FieldError e = new FieldError("RequestParam", "nCo2Cef", "nCo2Cef 는 필수 입력 값입니다. ");
        		bindingResult.addError(e);
        	}
        	if (param.getnCh4Cef() == null) {
        		FieldError e = new FieldError("RequestParam", "nCh4Cef", "nCh4Cef 는 필수 입력 값입니다. ");
        		bindingResult.addError(e);
        	}
        	if (param.getnN2oCef() == null) {
        		FieldError e = new FieldError("RequestParam", "nN2oCef", "nN2oCef 는 필수 입력 값입니다. ");
        		bindingResult.addError(e);
        	}
        	if (param.getnModCal() == null) {
        		FieldError e = new FieldError("RequestParam", "nModCal", "nModCal 는 필수 입력 값입니다. ");
        		bindingResult.addError(e);
        	}
        	if (bindingResult.hasErrors())
        		throw new BindException(bindingResult);
        }


        String sModId = getUserId(request);
        if (sModId == null)
            throw new BaseException("Invalid token");

        for (ReqCefMod c : reqParam)
            c.setsModId(sModId);

        cefService.upsertCefMod(reqParam);
        return new ResponseEntity<ResultValue>(new ResultValue(ResultCode.SUCCESS, null), HttpStatus.OK);
    }

    /**
     * 배출계수관리 간접배출 배출계수 목록 삭제
     * @param request
     * @param reqParam
     * @param bindingResult
     * @return
     * @throws BindException
     */
    @RequestMapping(value = "/5521/ief/ls", method = RequestMethod.DELETE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> deleteInfo5511IefLs(HttpServletRequest request, @ModelAttribute SearchParam reqParam,
            BindingResult bindingResult) throws BindException {
        logger.info("deleteInfo5511IefLs() RequestParam:" + reqParam);
        if (reqParam.getIdList() == null) {
            FieldError e = new FieldError("SearchParam", "idList", "idList 는 필수 입력 값입니다.");
            bindingResult.addError(e);
        }

        if (bindingResult.hasErrors())
            throw new BindException(bindingResult);

        List<ReqCefMod> list = new ArrayList<ReqCefMod>();
        String[] ids = reqParam.getIdList().split(",");
        try {
            for (String s : ids) {
                ReqCefMod c = new ReqCefMod();
                c.setIdCefMod(Long.parseLong(s));
                list.add(c);
            }
        } catch (Exception ex) {
            throw new BaseException(ex.getMessage());
        }

        String sModId = getUserId(request);
        if (sModId == null)
            throw new BaseException("Invalid token");
        cefService.deleteCefMod(list, sModId);

        return new ResponseEntity<ResultValue>(new ResultValue(ResultCode.SUCCESS, null), HttpStatus.OK);
    }

    /**
     * 배출계수관리 기타배출 에너지 목록 조회
     * @param request
     * @return
     */
    @RequestMapping(value = "/553/etc/emi/ls", method = RequestMethod.GET, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> getInfo553EtcEmiLs(HttpServletRequest request) {

        logger.info("getInfo553EtcEmiLs RequestParam:");

        SearchParam reqParam = new SearchParam();
        ResultCode resCode = ResultCode.SUCCESS;

        reqParam.setSCefCd("3"); // 기타

        List<VEngCef> result = cefService.search(reqParam);
        if (result.size() == 0)
            resCode = ResultCode.NO_DATA;

        return new ResponseEntity<ResultValue>(new ResultValue(resCode, result), HttpStatus.OK);
    }

    /**
     * 배출계수관리 기타배출 에너지 목록 저장
     * @param request
     * @param reqParam
     * @param bindingResult
     * @return
     * @throws BindException
     */
    @RequestMapping(value = "/553/etc/emi/ls", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> postInfo553EtcEmiLs(HttpServletRequest request, @RequestBody List<ReqEngCefEtc> reqParam,
            BindingResult bindingResult) throws BindException {
        logger.info("postInfo553EtcEmiLs() RequestParam:" + convertString(reqParam));

        customCollectionValidator.validate(reqParam, bindingResult);

        if (bindingResult.hasErrors())
            throw new BindException(bindingResult);

        String sModId = getUserId(request);
        if (sModId == null)
            throw new BaseException("Invalid token");

        List<ReqEngCef> params = new ArrayList<ReqEngCef>();
        for (ReqEngCefEtc e : reqParam) {
            ReqEngCef c = e.toEntity();
            c.setsModId(sModId);
            params.add(c);
        }
        cefService.upsert(params);
        return new ResponseEntity<ResultValue>(new ResultValue(ResultCode.SUCCESS, null), HttpStatus.OK);
    }

    /**
     * 배출계수관리 기타배출 에너지 목록 삭제
     * @param request
     * @param reqParam
     * @param bindingResult
     * @return
     * @throws BindException
     */
    @RequestMapping(value = "/553/etc/emi/ls", method = RequestMethod.DELETE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> deleteInfo553EtcEmiLs(HttpServletRequest request, @ModelAttribute SearchParam reqParam,
            BindingResult bindingResult) throws BindException {
        logger.info("deleteInfo553EtcEmi() RequestParam:" + reqParam);

        if (reqParam.getIdList() == null) {
            FieldError e = new FieldError("SearchParam", "idList", "idList 는 필수 입력 값입니다.");
            bindingResult.addError(e);
        }

        if (bindingResult.hasErrors())
            throw new BindException(bindingResult);

        List<ReqEngCef> list = new ArrayList<ReqEngCef>();
        String[] ids = reqParam.getIdList().split(",");
        try {
            for (String s : ids) {
                ReqEngCef c = new ReqEngCef();
                c.setIdEngCef(Long.parseLong(s));
                list.add(c);
            }
        } catch (Exception ex) {
            throw new BaseException(ex.getMessage());
        }

        String sModId = getUserId(request);
        if (sModId == null)
            throw new BaseException("Invalid token");
        cefService.delete(list, sModId);

        return new ResponseEntity<ResultValue>(new ResultValue(ResultCode.SUCCESS, null), HttpStatus.OK);

    }

    /**
     * 배출계수관리 온실가스/구성비율 목록 조회
     * @param request
     * @param reqParam
     * @param bindingResult
     * @return
     * @throws BindException
     */
    @RequestMapping(value = "/553/etc/gr/ls", method = RequestMethod.GET, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> getInfo553EtcGrLs(HttpServletRequest request, @ModelAttribute SearchParam reqParam,
            BindingResult bindingResult) throws BindException {
        logger.info("getInfo553EtcGrLs() RequestParam:" + reqParam);

        if (reqParam.getIdEngCef() == null) {
            FieldError e = new FieldError("SearchParam", "idEngCef", "idEngCef 는 필수 입력 값입니다. ");
            bindingResult.addError(e);
        }

        if (bindingResult.hasErrors())
            throw new BindException(bindingResult);

        Long idEngCef = reqParam.getIdEngCef();
        List<TCefMod> result = cefService.findMod(idEngCef);
        return new ResponseEntity<ResultValue>(new ResultValue(ResultCode.SUCCESS, result), HttpStatus.OK);
    }

    /**
     * 배출계수관리 온실가스/구성비율 목록 저장
     * @param request
     * @param reqParam
     * @param bindingResult
     * @return
     * @throws BindException
     */
    @RequestMapping(value = "/553/etc/gr/ls", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> postInfo553EtcGrLs(HttpServletRequest request,
            @RequestBody @Valid List<ReqCefMod> reqParam, BindingResult bindingResult) throws BindException {
        logger.info("postInfo5521IefLs() RequestParam:" + convertString(reqParam));

        customCollectionValidator.validate(reqParam, bindingResult);

        if (bindingResult.hasErrors())
            throw new BindException(bindingResult);

        String sModId = getUserId(request);
        if (sModId == null)
            throw new BaseException("Invalid token");

        for (ReqCefMod c : reqParam)
            c.setsModId(sModId);

        cefService.upsertCefMod(reqParam);
        return new ResponseEntity<ResultValue>(new ResultValue(ResultCode.SUCCESS, null), HttpStatus.OK);
    }

    /**
     * 배출계수관리 온실가스/구성비율 목록 삭제
     * @param request
     * @param reqParam
     * @param bindingResult
     * @return
     * @throws BindException
     */
    @RequestMapping(value = "/553/etc/gr/ls", method = RequestMethod.DELETE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> deleteInfo553EtcGrLs(HttpServletRequest request, @ModelAttribute SearchParam reqParam,
            BindingResult bindingResult) throws BindException {
        logger.info("deleteInfo5511IefLs() RequestParam:" + reqParam);
        if (reqParam.getIdList() == null) {
            FieldError e = new FieldError("SearchParam", "idList", "idList 는 필수 입력 값입니다.");
            bindingResult.addError(e);
        }

        if (bindingResult.hasErrors())
            throw new BindException(bindingResult);

        List<ReqCefMod> list = new ArrayList<ReqCefMod>();
        String[] ids = reqParam.getIdList().split(",");
        try {
            for (String s : ids) {
                ReqCefMod c = new ReqCefMod();
                c.setIdCefMod(Long.parseLong(s));
                list.add(c);
            }
        } catch (Exception ex) {
            throw new BaseException(ex.getMessage());
        }

        String sModId = getUserId(request);
        if (sModId == null)
            throw new BaseException("Invalid token");
        cefService.deleteCefMod(list, sModId);

        return new ResponseEntity<ResultValue>(new ResultValue(ResultCode.SUCCESS, null), HttpStatus.OK);
    }

    /**
     * 배출시설 목록 조회
     * @param request
     * @param reqParam
     * @return
     */
    @RequestMapping(value = "/571/ls", method = RequestMethod.GET, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> getInfo571Ls(HttpServletRequest request, @ModelAttribute SearchParam reqParam) {
        logger.info("getInfo571Ls() RequestParam:" + reqParam);

        Long idSite = reqParam.getIdSite();

        List<VEngPoint> result = cefService.findPointInSite(idSite);

        return new ResponseEntity<ResultValue>(new ResultValue(ResultCode.SUCCESS, result), HttpStatus.OK);
    }

     /**
     * 배출시설 상세 목록 조회
     * @param request
     * @param reqParam
     * @param bindingResult
     * @return
     * @throws BindException
     */
    @RequestMapping(value = "/571/dtl", method = RequestMethod.GET, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> getInfo571Dtl(HttpServletRequest request, @ModelAttribute SearchParam reqParam,
            BindingResult bindingResult) throws BindException {
        logger.info("getInfo571Dtl() RequestParam:" + reqParam);

        Long idSite = reqParam.getIdSite();
        Long idEngPoint = reqParam.getIdEngPoint();

        if (idSite == null) {
            FieldError e = new FieldError("SearchParam", "idSite", "idSite 는 필수 입력 값입니다.");
            bindingResult.addError(e);
        }

        if (idEngPoint == null) {
            FieldError e = new FieldError("SearchParam", "idEngPoint", "idEngPoint 는 필수 입력 값입니다.");
            bindingResult.addError(e);
        }

        if (bindingResult.hasErrors())
            throw new BindException(bindingResult);

        List<VEngPoint> result = cefService.findPoint(idSite, idEngPoint);

        return new ResponseEntity<ResultValue>(new ResultValue(ResultCode.SUCCESS, result), HttpStatus.OK);
    }

    /**
     * 배출시설 상제정보 수정
     * @param request
     * @param reqParam
     * @param bindingResult
     * @return
     * @throws BindException
     */
    @RequestMapping(value = "/571/dtl", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> postInfo571Dtl(HttpServletRequest request, @RequestBody @Valid ReqEngPoint reqParam,
            BindingResult bindingResult) throws BindException {
        logger.info("postInfo571Dtl() RequestParam:" + reqParam);

        if (bindingResult.hasErrors())
            throw new BindException(bindingResult);

        String sModId = getUserId(request);
        if (sModId == null)
            throw new BaseException("Invalid token");

        reqParam.setsModId(sModId);

        cefService.upsertEngPoint(reqParam.toEntity());
        return new ResponseEntity<ResultValue>(new ResultValue(ResultCode.SUCCESS, null), HttpStatus.OK);
    }

    /**
     * 배출시설 목록 삭제
     * @param request
     * @param reqParam
     * @return
     */
    @RequestMapping(value = "/571/dtl", method = RequestMethod.DELETE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> deleteInfo571Dtl(HttpServletRequest request, @ModelAttribute SearchParam reqParam) {
        logger.info("deleteInfo571Dtl() RequestParam:" + reqParam);

        Long idEngPoint = reqParam.getIdEngPoint();
        if (idEngPoint == null)
            throw new BaseException("idEngPoint is null.");

        String sModId = getUserId(request);
        if (sModId == null)
            throw new BaseException("Invalid token");

        cefService.deleteEngPoint(idEngPoint, sModId);
        return new ResponseEntity<ResultValue>(new ResultValue(ResultCode.SUCCESS, null), HttpStatus.OK);
    }

    @Autowired
    CodeServiceImpl codeService;

    /**
     * 코드 관리 그룹 조회
     * @param request
     * @param reqParam
     * @return
     */
    @RequestMapping(value = "/561/g/ls", method = RequestMethod.GET, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> getInfo561Gls(HttpServletRequest request, @ModelAttribute SearchParam reqParam) {
        logger.info("getInfo561Gls() RequestParam:" + reqParam);

        String sCat = reqParam.getSCat();
        List<TCat> result = codeService.findCat(sCat);
        return new ResponseEntity<ResultValue>(new ResultValue(ResultCode.SUCCESS, result), HttpStatus.OK);
    }


    /**
     * 코드 관리 그룹 저장
     * @param request
     * @param reqParam
     * @param bindingResult
     * @return
     * @throws BindException
     */
    @RequestMapping(value = "/561/g/dtl", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> postInfo561G(HttpServletRequest request, @RequestBody @Valid ReqCat reqParam,
            BindingResult bindingResult) throws BindException {
        logger.info("postInfo561G() RequestParam:" + reqParam);

        if (bindingResult.hasErrors())
            throw new BindException(bindingResult);

        String sModId = getUserId(request);
        if (sModId == null)
            throw new BaseException("Invalid token");

        reqParam.setsModId(sModId);

        codeService.upsertCat(reqParam.toEntity());
        return new ResponseEntity<ResultValue>(new ResultValue(ResultCode.SUCCESS, null), HttpStatus.OK);
    }

    /**
     * 코드관리 그룹 삭제
     * @param request
     * @param reqParam
     * @return
     */
    @Deprecated
    @RequestMapping(value = "/561/g/dtl", method = RequestMethod.DELETE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> deleteInfo561G(HttpServletRequest request, @ModelAttribute SearchParam reqParam) {
        logger.info("deleteInfo561G() RequestParam:" + reqParam);

        Long sCat = reqParam.getIdCat();
        if (sCat == null)
            throw new BaseException("sCat is null");

        String sModId = getUserId(request);
        if (sModId == null)
            throw new BaseException("Invalid token");

        codeService.deleteCat(sCat, sModId);
        return new ResponseEntity<ResultValue>(new ResultValue(ResultCode.FAILED, null), HttpStatus.OK);
    }

    /**
     * 코드관리 상세 조회
     * @param request
     * @param reqParam
     * @return
     */
    @RequestMapping(value = "/561/d/ls", method = RequestMethod.GET, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> getInfo561Dls(HttpServletRequest request, @ModelAttribute SearchParam reqParam) {
        logger.info("getInfo561Dls() RequestParam:" + reqParam);

        String sCat = reqParam.getSCat();
        if (sCat == null)
            throw new BaseException("sCat is null");

        List<TCode> result = codeService.findIncludingDelY(sCat);

        return new ResponseEntity<ResultValue>(new ResultValue(ResultCode.SUCCESS, result), HttpStatus.OK);
    }

    /**
     * 코드 관리 상세 저장
     * @param request
     * @param reqParam
     * @param bindingResult
     * @return
     * @throws BindException
     */
    @RequestMapping(value = "/561/d/dtl", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> postInfo561D(HttpServletRequest request, @RequestBody @Valid ReqCode reqParam,
            BindingResult bindingResult) throws BindException {
        logger.info("postInfo561D() RequestParam:" + reqParam);

        if (bindingResult.hasErrors())
            throw new BindException(bindingResult);

        String sModId = getUserId(request);
        if (sModId == null)
            throw new BaseException("Invalid token");
        
        String sCat = reqParam.getsCat();
        if (StringUtils.isEmpty(sCat))
            throw new BaseException("sCat is empty");
        
        String sCd = reqParam.getsCd();
        if (StringUtils.isEmpty(sCd))
            throw new BaseException("sCd is empty");

        reqParam.setsModId(sModId);
        codeService.upsertCode(reqParam.toEntity());

        return new ResponseEntity<ResultValue>(new ResultValue(ResultCode.SUCCESS, null), HttpStatus.OK);
    }

    /**
     * 코드관리 상제 삭제
     * @param request
     * @param reqParam
     * @return
     */
    @Deprecated
    @RequestMapping(value = "/561/d", method = RequestMethod.DELETE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> deleteInfo561D(HttpServletRequest request, @ModelAttribute SearchParam reqParam) {
        logger.info("deleteInfo561D() RequestParam:" + reqParam);

        Long idCode = reqParam.getIdCode();
        if (idCode == null)
            throw new BaseException("idCode is null");

        String sModId = getUserId(request);
        if (sModId == null)
            throw new BaseException("Invalid token");

        codeService.deleteCode(idCode, sModId);
        return new ResponseEntity<ResultValue>(new ResultValue(ResultCode.FAILED, null), HttpStatus.OK);
    }

    /**
     * 태양광 발전량 조회
     * @param request
     * @param reqParam
     * @return
     */
    @RequestMapping(value = "/581/ls", method = RequestMethod.GET, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> getInfo581Ls(HttpServletRequest request, @ModelAttribute SearchParam reqParam) {
        logger.info("getInfo581Ls() RequestParam:" + reqParam);
        ResultCode resultCode = ResultCode.SUCCESS;

        List<TSunPower> list = sunPowerService.search(reqParam);
        if (list.size() == 0)
            resultCode = ResultCode.NO_DATA;

        return new ResponseEntity<ResultValue>(new ResultValue(resultCode, list), HttpStatus.OK);
    }

    /**
     * 태양광 발전량 저장
     * @param request
     * @param reqParam
     * @param bindingResult
     * @return
     * @throws BindException
     */
    @RequestMapping(value = "/581/ls", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> postInfo581Ls(HttpServletRequest request, @RequestBody @Valid List<ReqSunPower> reqParam,
            BindingResult bindingResult) throws BindException {
        logger.info("postInfo581Ls() RequestParam:" + convertString(reqParam));

        customCollectionValidator.validate(reqParam, bindingResult);

        if (bindingResult.hasErrors())
            throw new BindException(bindingResult);

        String sModId = getUserId(request);
        if (sModId == null)
            throw new BaseException("Invalid token");

        for (ReqSunPower sp : reqParam)
            sp.setsModId(sModId);

        sunPowerService.upsert(reqParam);

        return new ResponseEntity<ResultValue>(new ResultValue(ResultCode.SUCCESS, null), HttpStatus.OK);
    }

    /**
     * 태양광 발전량 삭제
     * @param request
     * @param reqParam
     * @param bindingResult
     * @return
     * @throws BindException
     */
    @RequestMapping(value = "/581/ls", method = RequestMethod.DELETE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> deleteInfo581Ls(HttpServletRequest request, @ModelAttribute SearchParam reqParam,
            BindingResult bindingResult) throws BindException {
        logger.info("deleteInfo581Ls() RequestParam:" + reqParam);

        if (reqParam.getIdList() == null) {
            FieldError err = new FieldError("SearchParam", "idList", "idList는 필수 입력 값입니다. ");
            bindingResult.addError(err);
        }

        if (bindingResult.hasErrors())
            throw new BindException(bindingResult);

        String sModId = getUserId(request);
        if (sModId == null)
            throw new BaseException("Invalid token");

        ArrayList<ReqSunPower> list = new ArrayList<ReqSunPower>();
        String[] ids = reqParam.getIdList().split(",");
        for (String s : ids) {
            try {
                ReqSunPower sp = new ReqSunPower();
                sp.setIdSunPower(Long.parseLong(s));
                sp.setcDelYn("Y");
                sp.setsModId(sModId);
                list.add(sp);
            } catch (NumberFormatException ex) {
                throw new BaseException(s + "can not convert long");
            }
        }

        sunPowerService.upsert(list);
        return new ResponseEntity<ResultValue>(new ResultValue(ResultCode.SUCCESS, null), HttpStatus.OK);
    }

}