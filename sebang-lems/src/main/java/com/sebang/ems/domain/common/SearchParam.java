package com.sebang.ems.domain.common;

import java.util.Arrays;
import java.util.Date;

import javax.validation.constraints.NotBlank;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Setter
@Getter
// @ToString
@JsonInclude(value = Include.NON_EMPTY)
public class SearchParam {
	@NotBlank(message = "cYear is mandatory")
	String cYear; // 조회 연 [YYYY]

	String cYearEnd; // 조회 연끝 [YYYY]

	String cMon; // 조회 월 [MM]

	String cMonEnd; // 조회 월끝 [MM]

	String arrYear[]; // 연도 다중 선택

	String cDate; // YYYYMM

	Date startDt; // 검색 시작

	Date endDt; // 검색 종료

	String sScuCd; // 분석 조회 단위 [1: 고유 단위 , 2 :TJ, 3:TOE, 4: tCO2]

	String sEngType; // 조회 구분

	// String sMonAccYn; // 월 누적

	// String sumGrpYn; // 합계 그래프

	String sVrn; // 차량번호

	Long idTransCom; // 운수회사id

	String sLogiWeiCd;// 톤수

	String sLineCd; // 노선 구분 [1: 정기, 2:비정기]

	String transTypeCd; // [1 화물, 2 선박, 3 철도]

	String carType; // 차종 [11: 윙바디, 12: 탑차 ,13: 냉동 ,14: 풀카 ,15: 트랙터 ,16: 로우배드 ,17: 샤시 ,9: 카고트럭 ,96:
					// 철도 ,97: 탱크로리 ,98: 덤프 ,99: 기타]
	String sCarType;	// carType과 동일

	String sLogiTypeCd; // 물류/ 물류외

	String sBaseUnt;// 원단위 기준 [1: ton.km,2:대수,3: 적재량(kg) 4:면적(m2) ]

	String idVehicles; // idVehicle 다중 선택 필드

	Long idSite; // 사업장 seqNo

	String idSites; // 사업장 seqNo 다중 선택 필드

	String arrIdSite[]; // 사업부 다중 선택

	Long idErpDept; // 부서 seqNo

	String sDeptId; // 부서 seqNo

	Long idEngCef; // 배출원 seqNo;

	String idList; // 멀티 선택

	Long idCefMod; // 배출계수 seqNo

	public String sEmpId; // 관리자 ID

	Long idEngPoint; // 배출 시설 seqNo

	String idEngPoints; // 배출시설 seqNo 스트링 리스트

	String arrIdEngPoint[]; // 배출 시설 다중 선택

	String sCat; // 코드 카테로리

	Long idCat; // 코드 카테로리 seqNo

	Long idCode; // code seqNo

	String sBoardType; // 게시판 타입

	String nBoardId; // 게시판 ID

	/**
	 * 게시판
	 */
	String sTitle; // 게시판

	String legend; // 범례

	String sDrvType; // 운행 구분 정기, 비정기

	// String sPrjNm; // 물류전환사업명

	String sUiId; // 화면ID

	String sCefCd; // 1:직업, 2:간접: 3: 기타

	String sEmtCd; // 1:고정,2:이동 ,3 :간접-전기,4:간접-스팀,5:간접-온수, 6: 공정 9 기타

	String sGasEngCd;// GAS, ENG
	
	String qryObjType; // 1:화물, 2:선박, 3:철도, 4:기타(고정체)
	
	String sSelfvrnCd;	// 1:자차, 2:용차

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("SearchParam [");
		if (cYear != null) {
			builder.append(" cYear=");
			builder.append(cYear);
		}
		if (cYearEnd != null) {
			builder.append(" cYearEnd=");
			builder.append(cYearEnd);
		}
		if (cMon != null) {
			builder.append(" cMon=");
			builder.append(cMon);
		}
		if (cMonEnd != null) {
			builder.append(" cMonEnd=");
			builder.append(cMonEnd);
		}
		if (arrYear != null) {
			builder.append(" arrYear=");
			builder.append(Arrays.toString(arrYear));
		}
		if (cDate != null) {
			builder.append(" cDate=");
			builder.append(cDate);
		}
		if (startDt != null) {
			builder.append(" startDt=");
			builder.append(startDt);
		}
		if (endDt != null) {
			builder.append(" endDt=");
			builder.append(endDt);
		}
		if (sScuCd != null) {
			builder.append(" sScuCd=");
			builder.append(sScuCd);
		}
		if (sEngType != null) {
			builder.append(" sEngType=");
			builder.append(sEngType);
		}
		if (sVrn != null) {
			builder.append(" sVrn=");
			builder.append(sVrn);
		}
		if (idTransCom != null) {
			builder.append(" idTransCom=");
			builder.append(idTransCom);
		}
		if (sLogiWeiCd != null) {
			builder.append(" sLogiWeiCd=");
			builder.append(sLogiWeiCd);
		}
		if (sLineCd != null) {
			builder.append(" sLineCd=");
			builder.append(sLineCd);
		}
		if (transTypeCd != null) {
			builder.append(" transTypeCd=");
			builder.append(transTypeCd);
		}
		if (sCarType != null) {
			builder.append(" sCarType=");
			builder.append(sCarType);
		}
		if (carType != null) {
			builder.append(" carType=");
			builder.append(carType);
		}
		if (sLogiTypeCd != null) {
			builder.append(" sLogiTypeCd=");
			builder.append(sLogiTypeCd);
		}
		if (sBaseUnt != null) {
			builder.append(" sBaseUnt=");
			builder.append(sBaseUnt);
		}
		if (idVehicles != null) {
			builder.append(" idVehicles=");
			builder.append(idVehicles);
		}
		if (idSite != null) {
			builder.append(" idSite=");
			builder.append(idSite);
		}
		if (idSites != null) {
			builder.append(" idSites=");
			builder.append(idSites);
		}
		if (arrIdSite != null) {
			builder.append(" arrIdSite=");
			builder.append(Arrays.toString(arrIdSite));
		}
		if (idErpDept != null) {
			builder.append(" idErpDept=");
			builder.append(idErpDept);
		}
		if (sDeptId != null) {
			builder.append(" sDeptId=");
			builder.append(sDeptId);
		}
		if (idEngCef != null) {
			builder.append(" idEngCef=");
			builder.append(idEngCef);
		}
		if (idList != null) {
			builder.append(" idList=");
			builder.append(idList);
		}
		if (idCefMod != null) {
			builder.append(" idCefMod=");
			builder.append(idCefMod);
		}
		if (sEmpId != null) {
			builder.append(" sEmpId=");
			builder.append(sEmpId);
		}
		if (idEngPoint != null) {
			builder.append(" idEngPoint=");
			builder.append(idEngPoint);
		}
		if (idEngPoints != null) {
			builder.append(" idEngPoints=");
			builder.append(idEngPoints);
		}
		if (arrIdEngPoint != null) {
			builder.append(" arrIdEngPoint=");
			builder.append(Arrays.toString(arrIdEngPoint));
		}
		if (sCat != null) {
			builder.append(" sCat=");
			builder.append(sCat);
		}
		if (idCat != null) {
			builder.append(" idCat=");
			builder.append(idCat);
		}
		if (idCode != null) {
			builder.append(" idCode=");
			builder.append(idCode);
		}
		if (sBoardType != null) {
			builder.append(" sBoardType=");
			builder.append(sBoardType);
		}
		if (nBoardId != null) {
			builder.append(" nBoardId=");
			builder.append(nBoardId);
		}
		if (sTitle != null) {
			builder.append(" sTitle=");
			builder.append(sTitle);
		}
		if (legend != null) {
			builder.append(" legend=");
			builder.append(legend);
		}
		if (sDrvType != null) {
			builder.append(" sDrvType=");
			builder.append(sDrvType);
		}
		if (sUiId != null) {
			builder.append(" sUiId=");
			builder.append(sUiId);
		}
		if (sCefCd != null) {
			builder.append(" sCefCd=");
			builder.append(sCefCd);
		}
		if (sEmtCd != null) {
			builder.append(" sEmtCd=");
			builder.append(sEmtCd);
		}
		if (sGasEngCd != null) {
			builder.append(" sGasEngCd=");
			builder.append(sGasEngCd);
		}
		if (qryObjType != null) {
			builder.append(" qryObjType=");
			builder.append(qryObjType);
		}
		if (sSelfvrnCd != null) {
			builder.append(" sSelfvrnCd=");
			builder.append(sSelfvrnCd);
		}
		
		builder.append(" ]");
		return builder.toString();
	}

};
