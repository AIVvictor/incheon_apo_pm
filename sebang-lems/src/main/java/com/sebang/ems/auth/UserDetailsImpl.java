package com.sebang.ems.auth;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.User;

import lombok.ToString;

import java.util.List;

import com.sebang.ems.model.Do.TUsers;

@ToString
public class UserDetailsImpl extends User {

	private String detail; 

	private String rankNm;

	public UserDetailsImpl(String id, List<GrantedAuthority> authorities) {
		super(id, "", authorities);
	}

	public UserDetailsImpl(TUsers member, List<GrantedAuthority> authorities) {
		super(member.getsEmpId(), member.getsEmpPwd(), authorities);
		this.detail = member.getsEmpNm();
		this.rankNm = member.getcRankNm();
	}

	public String getDetail() {
		return detail;
	}

	public void setDetail(String detail) {
		this.detail = detail;
	}

	public String getRankNm() {
		return rankNm;
	}

	public void setRankNm(String rankNm) {
		this.rankNm = rankNm;
	}
}