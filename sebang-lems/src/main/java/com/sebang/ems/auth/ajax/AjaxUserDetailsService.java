package com.sebang.ems.auth.ajax;

import com.sebang.ems.auth.UserDetailsImpl;
import com.sebang.ems.dao.UsersMapper;

import com.sebang.ems.model.Do.TUsers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;

@Component
public class AjaxUserDetailsService implements UserDetailsService {

	@Autowired
	UsersMapper usersMapper;
	//private MemberRepository repository;

	@Override
	public UserDetails loadUserByUsername(String username) {
	//	Member user = repository.findById(username).orElse(null);
		TUsers user = usersMapper.findById(username);

		if (user == null) {
			throw new UsernameNotFoundException(username + "라는 사용자가 없습니다.");
		}
		return new UserDetailsImpl(user, AuthorityUtils.createAuthorityList(user.getcAuthCode()));
    
	//	return new UserDetailsImpl(user, AuthorityUtils.createAuthorityList(user.getRole()));
    }
}
