package com.sebang.ems.auth;

import java.io.PrintWriter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.sebang.ems.auth.jwt.JwtInfo;
import com.sebang.ems.domain.common.ResultCode;
import com.sebang.ems.domain.common.ResultValue;
import com.sebang.ems.model.Dto.ResUsers;
import com.sebang.ems.util.JwtUtil;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.MediaType;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.web.authentication.AuthenticationFailureHandler;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.stereotype.Component;

@Component
public class BaseSecurityHandler implements AuthenticationSuccessHandler, AuthenticationFailureHandler {

	public static final Logger logger = LoggerFactory.getLogger(BaseSecurityHandler.class);

	@Override
	public void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response,
			Authentication authentication) {

		UserDetailsImpl userDetails = (UserDetailsImpl) authentication.getPrincipal();		
		logger.debug("onAuthenticationSuccess  " + userDetails);
		// UserDetails userDetails = new
		// UserDetailsImpl(authentication.getPrincipal().toString(),
		// new ArrayList<>(authentication.getAuthorities()));
		response.setContentType(MediaType.APPLICATION_JSON_UTF8_VALUE);
		String token = JwtUtil.createToken(userDetails); 
		response.setHeader(JwtInfo.HEADER_NAME, token);
		try {
			GrantedAuthority author = userDetails.getAuthorities().iterator().next();
			//com.sebang.ems.model.Dto.ReqUsers u = new ReqUsers(userDetails.getUsername(), author.getAuthority());
			com.sebang.ems.model.Dto.ResUsers u = new ResUsers(userDetails.getUsername(), author.getAuthority(),token);
			u.setsEmpNm(userDetails.getDetail());
			u.setSRankNm(userDetails.getRankNm());
			
			ResultValue rv = new ResultValue(ResultCode.SUCCESS, u);

			ObjectMapper m = new ObjectMapper();
			String jsonBody = m.writeValueAsString(rv);
			response.setContentType("application/json; charset=UTF-8");
			response.setCharacterEncoding("UTF-8");
			PrintWriter out = response.getWriter();
			out.println(jsonBody);
		} catch (Exception e) {
			logger.error(e.toString());
		}
	}

	@Override
	public void onAuthenticationFailure(HttpServletRequest request, HttpServletResponse response,
			AuthenticationException exception) {

		// throw new ResponseStatusException(HttpStatus.FORBIDDEN,
		// exception.getMessage());
		response.setContentType(MediaType.APPLICATION_JSON_UTF8_VALUE);
		response.setCharacterEncoding("UTF-8");
		response.setHeader(JwtInfo.HEADER_NAME, null);
		try {
			PrintWriter out = response.getWriter();
			if("Bad credentials".equals(exception.getMessage())){
				out.println( "{\"resultCode\":\"501\"}");
			}else
			{
				out.println( "{\"resultCode\":\"500\"}");
			}
			
		} catch (Exception e) {
			logger.error(e.toString());
		}
	}

}
