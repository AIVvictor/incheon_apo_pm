package com.sebang.ems.dao;

import java.util.List;

import com.sebang.ems.model.Do.TTransCom;

public interface TransComMapper {

    List<TTransCom> findAll();

}