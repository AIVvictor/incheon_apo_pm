package com.sebang.ems.dao;

import java.util.List;

import com.sebang.ems.model.Menu;
import com.sebang.ems.model.Do.TAuthMenu;
import com.sebang.ems.model.Do.TMenuInfo;

import org.apache.ibatis.annotations.Param;


public interface MenuMapper {

    List<TMenuInfo> findMst(String sAuthCd);

    List<Menu> find(String sAuthCd);

    TAuthMenu findMenuAuth(@Param("idAuthMenu") Long idAuthMenu);

	void upsertAuthMenu(TAuthMenu authMenuKey);

	void deleteAuthMenu(TAuthMenu authMenuKey);

  
}