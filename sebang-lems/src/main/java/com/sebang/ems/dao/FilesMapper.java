package com.sebang.ems.dao;

import java.util.List;

import com.sebang.ems.model.Gfiles;
import com.sebang.ems.model.Do.TFiles;

import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.ResultMap;
import org.apache.ibatis.annotations.Select;

public interface FilesMapper {

    List<Gfiles> findByGid(String gid);

    void upsertFiles(Gfiles files);

    // 스토리지에서 삭제처리.
    void cleanFile (Gfiles files);

    void upsertGfiles(Gfiles gfile);        

    @ResultMap("com.sebang.ems.dao.gen.TFilesMapper.BaseResultMap")
    @Select("select * from T_FILES where ID_FILE = #{idFile}")
    TFiles fineOne(@Param("idFile") Long idFile);

    //삭제된 파일목록 조회.
    List<Gfiles> findDeletedFileMarks(Integer maxcount);
}