package com.sebang.ems.dao.gen;

import com.sebang.ems.model.Do.TCorpInfo;

public interface TCorpInfoMapper {
    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table T_CORP_INFO
     *
     * @mbg.generated Wed Oct 02 20:32:48 KST 2019
     */
    int deleteByPrimaryKey(Long idCorp);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table T_CORP_INFO
     *
     * @mbg.generated Wed Oct 02 20:32:48 KST 2019
     */
    int insert(TCorpInfo record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table T_CORP_INFO
     *
     * @mbg.generated Wed Oct 02 20:32:48 KST 2019
     */
    int insertSelective(TCorpInfo record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table T_CORP_INFO
     *
     * @mbg.generated Wed Oct 02 20:32:48 KST 2019
     */
    TCorpInfo selectByPrimaryKey(Long idCorp);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table T_CORP_INFO
     *
     * @mbg.generated Wed Oct 02 20:32:48 KST 2019
     */
    int updateByPrimaryKeySelective(TCorpInfo record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table T_CORP_INFO
     *
     * @mbg.generated Wed Oct 02 20:32:48 KST 2019
     */
    int updateByPrimaryKey(TCorpInfo record);
}