package com.sebang.ems.dao.gen;

import com.sebang.ems.model.Do.TSiteMonGoal;

public interface TSiteMonGoalMapper {
    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table T_SITE_MON_GOAL
     *
     * @mbg.generated Thu Dec 05 15:50:06 KST 2019
     */
    int deleteByPrimaryKey(Long idSiteMonGoal);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table T_SITE_MON_GOAL
     *
     * @mbg.generated Thu Dec 05 15:50:06 KST 2019
     */
    int insert(TSiteMonGoal record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table T_SITE_MON_GOAL
     *
     * @mbg.generated Thu Dec 05 15:50:06 KST 2019
     */
    int insertSelective(TSiteMonGoal record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table T_SITE_MON_GOAL
     *
     * @mbg.generated Thu Dec 05 15:50:06 KST 2019
     */
    TSiteMonGoal selectByPrimaryKey(Long idSiteMonGoal);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table T_SITE_MON_GOAL
     *
     * @mbg.generated Thu Dec 05 15:50:06 KST 2019
     */
    int updateByPrimaryKeySelective(TSiteMonGoal record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table T_SITE_MON_GOAL
     *
     * @mbg.generated Thu Dec 05 15:50:06 KST 2019
     */
    int updateByPrimaryKey(TSiteMonGoal record);
}