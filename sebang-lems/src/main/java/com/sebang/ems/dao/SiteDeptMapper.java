package com.sebang.ems.dao;

import java.util.Date;
import java.util.List;

import com.sebang.ems.domain.common.SearchParam;
import com.sebang.ems.model.SiteDept;
import com.sebang.ems.model.Do.TErpDept;
import com.sebang.ems.model.Do.TGasDeptMapping;
import com.sebang.ems.model.Do.TSiteInfo;
import com.sebang.ems.model.Dto.ReqErpDept;

import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.ResultMap;
import org.apache.ibatis.annotations.Select;

public interface SiteDeptMapper {

    List<TSiteInfo> findSiteById(Long idSite);

    List<SiteDept> findSite(Long idSite);

    void upsertSite(TSiteInfo siteInfo);

    List<TErpDept> findAllDept();

    List<SiteDept> searchSiteDept(SearchParam search);
    
    @ResultMap("com.sebang.ems.dao.gen.TGasDeptMappingMapper.BaseResultMap")
    @Select("SELECT ID_GAS_DEPT_MAPPING, ID_SITE, ID_ERP_DEPT FROM T_GAS_DEPT_MAPPING where ID_SITE=#{idSite} AND ID_ERP_DEPT=#{idErpDept}")
    TGasDeptMapping findErpDeptInSite(@Param("idSite") Long idSite, @Param("idErpDept") Long idErpDept);

    @Select("Select ID_SITE FROM T_GAS_DEPT_MAPPING WHERE ID_ERP_DEPT = #{idErpDept} limit 1")
    Long findIdSiteByIdErpDept(@Param("idErpDept") Long idErpDept);

    @Select("SELECT ID_ERP_DEPT FROM T_ERP_DEPT WHERE S_DEPT_ID=#{sDeptId}")
    Long findErpDeptKeyById(@Param("sDeptId") String sDeptId);

    SiteDept findSiteDeptMapping(@Param("idGasDeptMapping") Long idGasDeptMapping);

    void addDept(TGasDeptMapping param);

    void removeDept(TGasDeptMapping param);

    // 사업장 부서 맵핑 등록 수정
    void upsertSiteDept(TGasDeptMapping param);

    // T_RAW_ERP_DEPT.C_DEY_YN 를 'Y' 로 셋팅. 
    void deleteRawErpDept(List<ReqErpDept> item);

    List<SiteDept> findDeleteListErpDept(List<ReqErpDept> item);

    @Select("select MAX(D_MOD_DATE) FROM T_RAW_ERP_DEPT ORDER BY D_MOD_DATE DESC LIMIT 1")
    Date lastModifiedAtTRawErpDept();

    // T_RAW_ERP_DEPT -> T_ERP_DEPT  
    void importErpDeptFormRawErpDept(Date updateAt);

    List<SiteDept> findUpsertListErpDept(Date updateAt);


}