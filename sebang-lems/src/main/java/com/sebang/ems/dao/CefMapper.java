package com.sebang.ems.dao;

import java.util.List;
import java.util.Map;

import com.sebang.ems.domain.common.SearchParam;
import com.sebang.ems.model.Do.TCefMod;
import com.sebang.ems.model.Do.VEngCef;
import com.sebang.ems.model.Do.VEngPoint;

import org.apache.ibatis.annotations.Param;

public interface CefMapper {

    List<VEngCef> findByEmt(String sEmtCd);

    List<TCefMod> findMod(Long idEngCef);

    List<VEngPoint> searchPoint(SearchParam searchParam);

    List<VEngPoint> findPointInSite(Long idSite);

	List<VEngCef> searchCef(SearchParam searchParam);

    // 배출시설 ID 를 생성 한다. 
    String generateEngPointId(@Param("idSite") Long idSite);
    
    List<VEngCef> searchEngCefDropdownList();

}