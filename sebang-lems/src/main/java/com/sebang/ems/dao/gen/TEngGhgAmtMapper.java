package com.sebang.ems.dao.gen;

import com.sebang.ems.model.Do.TEngGhgAmt;

public interface TEngGhgAmtMapper {
    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table T_ENG_GHG_AMT
     *
     * @mbg.generated Tue Dec 03 17:50:12 KST 2019
     */
    int deleteByPrimaryKey(Long idEngGhgAmt);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table T_ENG_GHG_AMT
     *
     * @mbg.generated Tue Dec 03 17:50:12 KST 2019
     */
    int insert(TEngGhgAmt record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table T_ENG_GHG_AMT
     *
     * @mbg.generated Tue Dec 03 17:50:12 KST 2019
     */
    int insertSelective(TEngGhgAmt record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table T_ENG_GHG_AMT
     *
     * @mbg.generated Tue Dec 03 17:50:12 KST 2019
     */
    TEngGhgAmt selectByPrimaryKey(Long idEngGhgAmt);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table T_ENG_GHG_AMT
     *
     * @mbg.generated Tue Dec 03 17:50:12 KST 2019
     */
    int updateByPrimaryKeySelective(TEngGhgAmt record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table T_ENG_GHG_AMT
     *
     * @mbg.generated Tue Dec 03 17:50:12 KST 2019
     */
    int updateByPrimaryKey(TEngGhgAmt record);
}