package com.sebang.ems.dao;

import java.util.List;

import com.sebang.ems.domain.common.SearchParam;
import com.sebang.ems.model.Do.TEngPoint;
import com.sebang.ems.model.Do.TLogisTransInfo;
import com.sebang.ems.model.Do.TLogisTransMonInfo;
import com.sebang.ems.model.Do.TTransCom;
import com.sebang.ems.model.Do.TVehicle;
import com.sebang.ems.model.Do.VLogisDrvLog;
import com.sebang.ems.model.Do.VLogisEff;
import com.sebang.ems.model.Do.VLogisTransYearVhcl;
import com.sebang.ems.model.Do.VVehicle;

import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.ResultMap;
import org.apache.ibatis.annotations.Select;

public interface LogisMapper {

    List<TLogisTransInfo> searchLogisTransInfo(SearchParam param);

    List<VLogisDrvLog> searchVLogisDrvLog(SearchParam param);

    TTransCom findTransComByName(String sTransComNm);
 
    TTransCom findTransComById(String sTransComId);
     
    void upsertTransCom(TTransCom transCom);

    @ResultMap("com.sebang.ems.dao.gen.TLogisTransInfoMapper.BaseResultMap")
    @Select("SELECT * FROM T_LOGIS_TRANS_INFO WHERE ID_LOGIS_TRANS_INFO = #{idTransLogisInfo}")
    TLogisTransInfo findLogisTransInfo(Long idLogisTransInfo);

    @ResultMap("com.sebang.ems.dao.gen.TLogisTransInfoMapper.BaseResultMap")
    @Select("SELECT * FROM T_LOGIS_TRANS_INFO WHERE TRANS_ID = #{transId} limit 1")
    TLogisTransInfo findLogisTransInfoByTransId(@Param("transId") String sTransId);

    void upsertLogisTransInfo(TLogisTransInfo transInfo);


    @ResultMap("com.sebang.ems.dao.gen.TLogisTransMonInfoMapper.BaseResultMap")
    @Select("SELECT * FROM T_LOGIS_TRANS_MON_INFO WHERE ID_LOGIS_TRANS_MON_INFO = #{idTransLogisMonInfo}")
    TLogisTransMonInfo findLogisTransMonInfo(@Param("idTransLogisMonInfo") Long idTransLogisMonInfo);
    
    void upsertLogisTransMonInfo(TLogisTransMonInfo transMonInfo);

    @ResultMap("com.sebang.ems.dao.gen.TVehicleMapper.BaseResultMap")
    @Select("SELECT ID_VEHICLE, ID_TRANS_COM,S_FUEL_EFF ,S_SELFVRN_CD FROM T_VEHICLE WHERE S_VRN = #{sVrn}")
    TVehicle findVehicleByVrn(@Param("sVrn") String sVrn);

    @ResultMap("com.sebang.ems.dao.gen.TVehicleMapper.BaseResultMap")
    @Select("SELECT ID_VEHICLE,ID_ERP_DEPT,ID_TRANS_COM,S_VRN,S_FUEL_EFF FROM T_VEHICLE WHERE ID_VEHICLE = #{idVehicle}")
    TVehicle findVehicleByKey(@Param("idVehicle") Long idVehicle);

    void upsertVehicle(TVehicle vehicle);

    @ResultMap("com.sebang.ems.dao.gen.TEngPointMapper.BaseResultMap")
    @Select("SELECT ID_ENG_POINT ,S_FUEL_EFF FROM T_ENG_POINT WHERE S_INV_CD=#{sInvCd} AND C_DEL_YN='N' LIMIT 1")
    TEngPoint  findEngPointByInvCd(@Param("sInvCd") String sInvCs);

    // 배출 시설 ID 화물 차량
    TEngPoint findEngPointForVehicle(@Param("idErpDept") Long idErpDept, @Param("sVrn") String sVrn );
    
     // 배출 시설 ID 철도
     TEngPoint findEngPointForTrain(@Param("idErpDept") Long idErpDept);   

    List<VVehicle> searchVehicle(SearchParam param);

	List<VLogisTransYearVhcl> searchVLogisTransYear(SearchParam p);

    List<VLogisEff> searchVLogisEff(SearchParam p);

}