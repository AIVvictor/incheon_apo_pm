package com.sebang.ems.dao;

import com.sebang.ems.model.Do.TBoard;

public interface BoardMapper
{
    Long insertSelective(TBoard board);

    int updateByPrimaryKeySelective(TBoard board);

	int deleteDetailOneByBoardId(String boardId);
	
	
}