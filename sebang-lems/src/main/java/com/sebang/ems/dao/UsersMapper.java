package com.sebang.ems.dao;

import com.sebang.ems.domain.common.SearchParam;
import com.sebang.ems.model.Employee;
import com.sebang.ems.model.Do.TErpUsers;
import com.sebang.ems.model.Do.TUsers;
import com.sebang.ems.model.Dto.ReqErpUsers;

import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.ResultMap;
import org.apache.ibatis.annotations.Select;

import java.util.Date;
import java.util.List;

public interface UsersMapper {

    List<TUsers> select();

    TUsers findById(String sEmpId);

    void insert(TUsers u);

    void update(TUsers u);

    void upsert(TUsers u);

    List<Employee> searchEmployee(SearchParam search); 

    void upsertEmployee(Employee emp);

    @Select("select D_MOD_DATE FROM T_ERP_USERS ORDER BY D_MOD_DATE DESC LIMIT 1")
    Date lastModifiedAtTErpUsers();

    @ResultMap("com.sebang.ems.dao.gen.TErpUsersMapper.BaseResultMap")
    @Select("select * from T_ERP_USERS where S_EMP_ID = #{sEmpId}")
    TErpUsers findErpUserByEmpId(@Param("sEmpId")String sEmpId);

    void upsertErpUsers(TErpUsers u);

    void deleteRawErpUsers(List<ReqErpUsers> users);
    
    List<TErpUsers> findDeleteRawErpUsers(List<ReqErpUsers> users); 

    List<TErpUsers> pullErpUsers(Date updatedAt);

}