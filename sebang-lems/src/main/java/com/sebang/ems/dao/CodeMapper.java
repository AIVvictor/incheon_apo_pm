package com.sebang.ems.dao;

import java.util.List;

import com.sebang.ems.model.Do.TCat;
import com.sebang.ems.model.Do.TCode;

public interface CodeMapper {

    List<TCode> find(String sCat);

    List<TCode> findIncludingDelY(String sCat);

    List<TCat> findCat(String sCat);
    
    void upsertCode(TCode code);

    void upsertCat(TCat cat);
  
}