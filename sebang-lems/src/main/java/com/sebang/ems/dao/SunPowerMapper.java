package com.sebang.ems.dao;

import java.util.List;

import com.sebang.ems.domain.common.SearchParam;
import com.sebang.ems.model.Do.TSunPower;

import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.ResultMap;
import org.apache.ibatis.annotations.Select;

public interface SunPowerMapper
{
    public List<TSunPower> searchSunPower(SearchParam search);
    public List<TSunPower> searchSunPowerYearSum(SearchParam search);

    @ResultMap("com.sebang.ems.dao.gen.TSunPowerMapper.BaseResultMap")
    @Select("SELECT * FROM T_SUN_POWER WHERE ID_SUN_POWER = #{idSunPower}")
    public TSunPower findSunPower(@Param("idSunPower") Long idSunPower);

    void upsert(TSunPower sunpower);
}