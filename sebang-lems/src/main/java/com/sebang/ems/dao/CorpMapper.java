package com.sebang.ems.dao;

import com.sebang.ems.model.Do.TCorpInfo;

public interface CorpMapper {

    TCorpInfo findOne(String sCorpId);

    void upsert(TCorpInfo corpInfo);
}