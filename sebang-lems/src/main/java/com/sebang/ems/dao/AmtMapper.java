package com.sebang.ems.dao;

import java.math.BigDecimal;
import java.util.List;

import com.sebang.ems.domain.common.SearchParam;
import com.sebang.ems.model.EngPointAmt;
import com.sebang.ems.model.SumAmt;
import com.sebang.ems.model.SumAmt4Home;
import com.sebang.ems.model.Do.TEngPointAmt;
import com.sebang.ems.model.Do.TMonEngClose;
import com.sebang.ems.model.Do.VBaseUntAmt;
import com.sebang.ems.model.Do.VMonEngClose;
import com.sebang.ems.model.Do.VMonSiteEngCefAmt;
import com.sebang.ems.model.Do.VSumAmt;
import com.sebang.ems.model.Dto.ReqMonEngClose;

import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.ResultMap;
import org.apache.ibatis.annotations.Select;

public interface AmtMapper {

    List<EngPointAmt> searchEngPointAmt(SearchParam search);

    @ResultMap("com.sebang.ems.dao.gen.TEngPointAmtMapper.BaseResultMap")
    @Select("select * from T_ENG_POINT_AMT where ID_ENG_POINT=#{idEngPoint} and C_YEAR=#{cYear} and C_MON=#{cMon}")
    TEngPointAmt findEngPointAmt(@Param("idEngPoint") long idEngPoint, @Param("cYear") String cYear,
            @Param("cMon") String cMon);

    EngPointAmt findEngConversion(@Param("idEngPoint") long idEngPoint, @Param("nUseAmt") BigDecimal nUseAmt);
    
    EngPointAmt findEngConversionBaseVal(@Param("idEngPoint") long idEngPoint, @Param("nTco2") BigDecimal nTco2);
    
    

    void upsertEngPointAmt(TEngPointAmt amt);

    //월마감 조회
    TMonEngClose findMonthEndClosing(TMonEngClose close);
 
    // 월마감 조회 idEngPoint
    TMonEngClose findMonthEndClosingByIdEngPoint(@Param("idEngPoint") Long idEngPoint, @Param("cYear") String cYear,@Param("cMon") String cMon);
    
    // 월마감 확인
    @Select("select count(*)  from T_MON_ENG_CLOSE where C_YEAR=#{cYear} and C_MON = LPAD(#{cMon},2,0) and (C_CLOSE_YN='Y' or C_CLOSE_YN='G')")
    Integer enableMonthEndClosing( @Param("cYear") String cYear,@Param("cMon") String cMon);

    // 월마감
    void upsertMonEngClose(TMonEngClose close);

    // 월마감 취소
    void cancelMonEngClose(TMonEngClose close);

    // 월마감 후처리 Procedure 
    void callSPDoCloseMonth(ReqMonEngClose c);

    @Deprecated
    void TEST2(ReqMonEngClose c);

    List<VMonEngClose> searchMonEngClose(SearchParam param);

    List<VMonSiteEngCefAmt> searchMonSiteEngCefAmt(SearchParam param);

    List<VSumAmt> searchTotalSumAmt(SearchParam p);

    List<VSumAmt> searchYearSumAmt(SearchParam p);

    // 연도별 분석
    List<SumAmt> searchYearAnalysisAmt(SearchParam p);

    // 사업부별 연도 분석
    List<SumAmt> searchYearAnalysisSiteAmt(SearchParam p);

    // 배출시설별 연도 분석
    List<SumAmt> searchYearAnalysisPointAmt(SearchParam p);

    // 월별 분석
    List<SumAmt> searchMonAnalysisAmt(SearchParam p);

    // 사업부별 월별 분석
    List<SumAmt> searchMonAnalysisSiteAmt(SearchParam p);

    // 배출 시설 월별 분석
    List<SumAmt> searchMonAnalysisPointAmt(SearchParam p);

    // 물류 원단위 분석
    List<VBaseUntAmt> searchLogisBaseUntAnalysis(SearchParam p);

    // 원단위 분석
    List<VBaseUntAmt> searchBaseUntAmtAnalysis(SearchParam p);

    // 온실가스 배출 시설 for Home
    List<SumAmt4Home> searchGasSumAmt4Home(SearchParam p);

    // 에너지 배출 실적 for Home
    List<SumAmt4Home> searchEngSumAmt4Home(SearchParam p);
}