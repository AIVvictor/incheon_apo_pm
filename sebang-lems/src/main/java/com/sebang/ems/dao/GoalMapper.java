package com.sebang.ems.dao;

import java.util.List;

import com.sebang.ems.domain.common.SearchParam;
import com.sebang.ems.model.SiteYearGoal4Home;
import com.sebang.ems.model.Do.TMrvPointGoal;
import com.sebang.ems.model.Do.TSiteMonGoal;
import com.sebang.ems.model.Do.TSiteMonSumGoal;
import com.sebang.ems.model.Do.TSiteYearGoal;
import com.sebang.ems.model.Do.VBau;
import com.sebang.ems.model.Do.VCompanyGoal;
import com.sebang.ems.model.Do.VGoalResultAnalyAcc;
import com.sebang.ems.model.Do.VMrvGoal;
import com.sebang.ems.model.Do.VSiteGoal;

import org.apache.ibatis.annotations.ResultMap;
import org.apache.ibatis.annotations.Select;
import org.springframework.data.repository.query.Param;

public interface GoalMapper {

    // List<TSiteYearGoal> searchSiteYearGoal(SearchParam search);

    @ResultMap("com.sebang.ems.dao.gen.TSiteYearGoalMapper.BaseResultMap")
    @Select("select * from T_SITE_YEAR_GOAL WHERE C_YEAR=#{cYear} AND ID_SITE=#{idSite}")
    TSiteYearGoal findSiteYearGoal(@Param("cYear") String cYear, @Param("idSite") Long idSite);

    // 사업장별 년간 목표 등록
    void upsertSiteYearGoal(TSiteYearGoal goal);

    @ResultMap("com.sebang.ems.dao.gen.TSiteMonSumGoalMapper.BaseResultMap")
    @Select("SELECT * FROM T_SITE_MON_SUM_GOAL WHERE ID_SITE_YEAR_GOAL=#{idSiteYearGoal} AND ID_ENG_POINT=#{idEngPoint} ")
    TSiteMonSumGoal findSiteMonSumGoal(@Param("idSiteYearGoal") Long idSiteYearGoal,
            @Param("idEngPoint") Long idEngPoint);

    void upsertSiteMonSumGoal(TSiteMonSumGoal monSumGoal);

    @ResultMap("com.sebang.ems.dao.gen.TSiteMonGoalMapper.BaseResultMap")
    @Select("SELECT * FROM T_SITE_MON_GOAL WHERE ID_SITE_MON_SUM_GOAL = #{idSiteMonSumGoal} AND C_MON=#{cMon}")
    TSiteMonGoal findSiteMonGoal(@Param("idSiteMonSumGoal") Long idSiteMonSumGoal, @Param("cMon") String cMon);

    // 사업장 에너지원별 목표 등록
    void upsertSiteMonGoal(TSiteMonGoal goal);

    @ResultMap("com.sebang.ems.dao.gen.TMrvPointGoalMapper.BaseResultMap")
    @Select("select * from T_MRV_POINT_GOAL where C_YEAR=#{cYear} limit 1")
    TMrvPointGoal findMrvGoal(@Param("cYear") String cYear);

    // 정부 목표 등록
    void upsertMrvPointGoal(TMrvPointGoal goal);

    // 정부 목표 삭제
    void clearMvrPointGoal(TMrvPointGoal g);

    void updateSiteMonSumGoal(Long idSiteMonSumGoal);

    void updateSiteYearGoal(Long idSiteYearGoal);

    List<VMrvGoal> searchVMrvGoal(SearchParam search);

    List<VSiteGoal> searchSiteGoal(SearchParam search);

    List<VCompanyGoal> searchSiteMonGoal(SearchParam search);

    // 목표 대비 실적 분석
    List<VGoalResultAnalyAcc> searchSiteMonGoalArchRate(SearchParam search);

    // BAU 분석
    List<VBau> searchBau(SearchParam p);

    // 온실 가스 에너지 현황(실적)
	List<SiteYearGoal4Home> searchSiteYearGoal4Home(SearchParam search);
}