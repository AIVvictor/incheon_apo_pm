package com.sebang.ems.dao.gen;

import com.sebang.ems.model.Do.TMonEngClose;

public interface TMonEngCloseMapper {
    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table T_MON_ENG_CLOSE
     *
     * @mbg.generated Wed Oct 02 20:32:48 KST 2019
     */
    int deleteByPrimaryKey(Long idMonEngClose);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table T_MON_ENG_CLOSE
     *
     * @mbg.generated Wed Oct 02 20:32:48 KST 2019
     */
    int insert(TMonEngClose record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table T_MON_ENG_CLOSE
     *
     * @mbg.generated Wed Oct 02 20:32:48 KST 2019
     */
    int insertSelective(TMonEngClose record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table T_MON_ENG_CLOSE
     *
     * @mbg.generated Wed Oct 02 20:32:48 KST 2019
     */
    TMonEngClose selectByPrimaryKey(Long idMonEngClose);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table T_MON_ENG_CLOSE
     *
     * @mbg.generated Wed Oct 02 20:32:48 KST 2019
     */
    int updateByPrimaryKeySelective(TMonEngClose record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table T_MON_ENG_CLOSE
     *
     * @mbg.generated Wed Oct 02 20:32:48 KST 2019
     */
    int updateByPrimaryKey(TMonEngClose record);
}