package com.sebang.ems.dao.gen;

import com.sebang.ems.model.Do.TErpUsers;

public interface TErpUsersMapper {
    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table T_ERP_USERS
     *
     * @mbg.generated Wed Oct 02 20:32:48 KST 2019
     */
    int deleteByPrimaryKey(Long idErpUsers);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table T_ERP_USERS
     *
     * @mbg.generated Wed Oct 02 20:32:48 KST 2019
     */
    int insert(TErpUsers record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table T_ERP_USERS
     *
     * @mbg.generated Wed Oct 02 20:32:48 KST 2019
     */
    int insertSelective(TErpUsers record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table T_ERP_USERS
     *
     * @mbg.generated Wed Oct 02 20:32:48 KST 2019
     */
    TErpUsers selectByPrimaryKey(Long idErpUsers);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table T_ERP_USERS
     *
     * @mbg.generated Wed Oct 02 20:32:48 KST 2019
     */
    int updateByPrimaryKeySelective(TErpUsers record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table T_ERP_USERS
     *
     * @mbg.generated Wed Oct 02 20:32:48 KST 2019
     */
    int updateByPrimaryKey(TErpUsers record);
}