package com.sebang.ems.config;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Properties;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

/**
 * 
 * 외부 설정 파일 "lems.cnf" 으로부터 각종 실행 환경을 load한다. 실행파일과 같은 directory에 있어야 loading
 * 가능함.
 * 
 * @author promaker
 *
 */
@Component
public class AppConfig {

	public static final Logger logger = LoggerFactory.getLogger(AppConfig.class);

	static AppConfig self = new AppConfig();

	public static AppConfig instance() {
		return self;
	}

	Object lock = new Object();
	Properties properties = null;
	InputStream isProperties;
	long lastModifiedTime = 0;

	// 프로퍼티 파일 위치
	private String propFile = "/lems.cnf";

	public AppConfig() {
		load();
	}

	@PostConstruct
	public void init() {
		self = this;
	}

	public AppConfig load() {
		try {

			Path currentRelativePath = Paths.get("");
			String s = currentRelativePath.toAbsolutePath().toString();
			s += propFile;

			File file = new File(s);
			long lastModifiedTime = file.lastModified();
			if (this.lastModifiedTime == lastModifiedTime) {
				return null;
			}

			logger.info("Load config file: {}", s);
			properties = new Properties();
			synchronized (lock) {
				properties.load(new FileInputStream(s));
			}
			this.lastModifiedTime = lastModifiedTime;
			// properties.load(isProperties);
		} catch (Exception ex) {
			logger.error("[Exception] load() {}", ex.toString());
		}
		return this;
	}

	public String getProperty(String key) {
		synchronized (lock) {
			return properties.getProperty(key);
		}
	}

	public String getProperty(String key, String defaultVal) {
		String val;
		synchronized (lock) {
			val = properties.getProperty(key);
		}
		if (val == null) {
			return defaultVal.trim();
		}
		val = val.trim();
		return val;
	}

	public Integer getPropertyInt(String key) {
		String val;
		synchronized (lock) {
			val = properties.getProperty(key);
		}
		if (val == null)
			return 0;
		val = val.trim();
		return Integer.parseInt(val);
	}

	public Integer getPropertyInt(String key, int defaultVal) {
		String val;
		synchronized (lock) {
			val = properties.getProperty(key);
		}
		if (val == null)
			return defaultVal;
		val = val.trim();
		return Integer.parseInt(val);
	}

	public boolean getPropertyBool(String key, boolean defaultVal) {
		String val;
		synchronized (lock) {
			val = properties.getProperty(key);
		}
		if (val == null)
			return defaultVal;
		val = val.trim();

		switch (val.toLowerCase()) {
		case "true":
			return true;
		case "false":
			return false;
		}
		return false;
	}

	public double getPropertyDouble(String key, double defaultVal) {
		String val;
		synchronized (lock) {
			val = properties.getProperty(key);
		}
		double dVal = 0;
		if (val == null)
			return defaultVal;
		val = val.trim();
		dVal = Double.parseDouble(val);
		return dVal;
	}

	public Boolean getPropertyBool(String key) {
		String val;
		synchronized (lock) {
			val = properties.getProperty(key);
		}
		if (val == null)
			return false;
		val = val.trim();

		switch (val.toLowerCase()) {
		case "true":
			return true;
		case "false":
			return false;
		}
		return false;
	}
}
