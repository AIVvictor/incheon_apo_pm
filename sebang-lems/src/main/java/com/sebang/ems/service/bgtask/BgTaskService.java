package com.sebang.ems.service.bgtask;

import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import javax.annotation.PostConstruct;

import com.sebang.ems.config.AppConfig;
import com.sebang.ems.service.AmtServiceImpl;
import com.sebang.ems.service.FileServiceImpl;
import com.sebang.ems.service.SyncServiceImpl;
import com.sebang.ems.util.Environment;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

@Service
public class BgTaskService {

	public static final Logger logger = LoggerFactory.getLogger(BgTaskService.class);

	private final SyncServiceImpl syncService;

	private final AmtServiceImpl amtService;

	private final FileServiceImpl fileService;

	@Autowired
	public BgTaskService(SyncServiceImpl syncService, FileServiceImpl fileService, AmtServiceImpl amtService) {
		this.syncService = syncService;
		this.fileService = fileService;
		this.amtService = amtService;
	}

	/**
	 * Single-tone
	 */
	static BgTaskService self;

	private ScheduledThreadPoolExecutor exec = new ScheduledThreadPoolExecutor(3);
	ScheduledFuture<?>[] schedule = new ScheduledFuture<?>[3];

	int interval = 1; // 1sec
	long BgTaskMemDBLoader_tick = 0;
	long BgTaskCleanStorage_tick = 0;

	public static BgTaskService instance() {
		return self;
	}

	@PostConstruct
	public void init() {
		self = this;
		logger.info("BgTaskService.init()");

		startBgTaskSyncErpLoader();
		// startBgTaskPostMonthEndClosing();
	}

	@Scheduled(fixedDelay = 1000, initialDelay = 1000)
	private void startBgTaskPostMonthEndClosing() {
		// schedule[1] = exec.scheduleAtFixedRate(new Runnable() {
		// public void run() {
		logger.trace(" startBgTaskPostMonthEndClosing()-start");

		try {

			if (AppConfig.instance().getPropertyBool("bgtask.use.PostMonthEndClosing", true)) {
				// 1. waiting comman queue
				Event eh = QueueEvent.instance().pop();
				if (eh != null) {
					logger.info("startBgTaskPostMonthEndClosing()-run() ID {}", eh.getKey());
					// 1. 월마가 후처리
					amtService.callSpDoCloseMonth(eh.getItem());
				}
			}
		} catch (Exception e) {
			logger.error("[EXCEPTION] startBgTaskPostMonthEndClosing()-run() {}", e.toString());
			return;
		}
		logger.trace(" startBgTaskPostMonthEndClosing()-finish");
		// }
		// }, 0, interval, TimeUnit.SECONDS);
	}

	private void startBgTaskSyncErpLoader() {
		schedule[0] = exec.scheduleAtFixedRate(new Runnable() {

			public void run() {
				logger.trace(" startBgTaskSyncErpLoader()-start");

				try {
					if (BgTaskMemDBLoader_tick == 0)
						return;

					Thread.currentThread().setName("BgTaskLoader");

					loadEnvironment(BgTaskMemDBLoader_tick);

					if (AppConfig.instance().getPropertyBool("bgtask.use.loader", true) && BgTaskMemDBLoader_tick > 0) {
						syncErpDept(BgTaskMemDBLoader_tick);
						syncErpUsers(BgTaskMemDBLoader_tick);
						syncErpRetire(BgTaskMemDBLoader_tick);
					}

				} catch (Exception e) {
					logger.error("[EXCEPTION] startBgTaskSyncErpLoader()-run() {}", e.toString());
					return;
				} finally {
					BgTaskMemDBLoader_tick++;
				}
				logger.trace(" startBgTaskSyncErpLoader()-finish");
			}

		}, 0, interval, TimeUnit.SECONDS);

	}

	boolean loadEnvironment(long tick) {

		int interval = AppConfig.instance().getPropertyInt("bgtask.interval.loadEnvironment", 20);
		// logger.debug("loadEnvironment: tick=" + tick + ", interval=" + interval + ",
		// tick % interval=" + (tick % interval));

		if (tick % interval != 0) {
			return false;
		}
		logger.trace("start");
		if (AppConfig.instance().load() != null) {
			Environment.instance().load();
		}
		logger.trace("end");
		return true;
	}

	boolean syncErpDept(long tick) {
		int interval = AppConfig.instance().getPropertyInt("bgtask.interval.sync.dept", 600);
		if (interval == 0 || tick % interval != 0) {
			return false;
		}

		boolean enabledTrace = AppConfig.instance().getPropertyBool("sync.trace");
		boolean backupCsv = AppConfig.instance().getPropertyBool("sync.backup");

		logger.trace("start");
		try {
			syncService.syncErpDept(backupCsv, enabledTrace);
		} catch (Exception ex) {
			logger.error("syncErpDept=>" + ex.getMessage());
		}
		logger.trace("end");
		return true;
	}

	boolean syncErpUsers(long tick) {
		int interval = AppConfig.instance().getPropertyInt("bgtask.interval.sync.person", 600);

		if (interval == 0 || tick % interval != 0) {
			return false;
		}

		boolean enabledTrace = AppConfig.instance().getPropertyBool("sync.trace");
		boolean backupCsv = AppConfig.instance().getPropertyBool("sync.backup");

		logger.trace("start");
		try {
			syncService.syncErpUsers(backupCsv, enabledTrace);
		} catch (Exception ex) {
			logger.error("syncErpUsers=>" + ex.getMessage());
		}
		logger.trace("end");
		return true;
	}

	boolean syncErpRetire(long tick) {
		int interval = AppConfig.instance().getPropertyInt("bgtask.interval.sync.retire", 600);

		if (interval == 0 || tick % interval != 0) {
			return false;
		}

		boolean enabledTrace = AppConfig.instance().getPropertyBool("sync.trace");
		boolean backupCsv = AppConfig.instance().getPropertyBool("sync.backup");

		logger.trace("start");
		try {
			syncService.syncErpRetire(backupCsv, enabledTrace);
		} catch (Exception ex) {
			logger.error("syncErpRetire=>" + ex.getMessage());
		}
		logger.trace("end");
		return true;
	}

	@Scheduled(fixedDelay = 1000, initialDelay = 10000)
	public void scheduleFixedRateTask() {
		logger.trace("start scheduleFixedRateTask");
		try {
			if (BgTaskCleanStorage_tick == 0)
				return;

			int cleanInterval = AppConfig.instance().getPropertyInt("clean.interval.storaget", 3600);

			if (BgTaskCleanStorage_tick % cleanInterval != 0) {
				return;
			}

			fileService.cleanFiles();
		} catch (Exception e) {
			logger.error("[EXCEPTION] scheduleFixedRateTask()-run() {}", e.toString());
			return;
		} finally {
			BgTaskCleanStorage_tick++;
		}
		logger.trace(" scheduleFixedRateTask()-finish");
	}

}
