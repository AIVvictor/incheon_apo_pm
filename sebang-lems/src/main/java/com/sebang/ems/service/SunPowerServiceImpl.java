package com.sebang.ems.service;

import java.util.ArrayList;
import java.util.List;

import com.sebang.ems.dao.SunPowerMapper;
import com.sebang.ems.domain.Exception.BaseException;
import com.sebang.ems.domain.common.SearchParam;
import com.sebang.ems.model.Do.TSunPower;
import com.sebang.ems.model.Dto.ReqSunPower;
import com.sebang.ems.model.Dto.ResSunPower;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.TransactionDefinition;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.DefaultTransactionDefinition;

@Service
public class SunPowerServiceImpl implements SunPowerService {

    public static final Logger logger = LoggerFactory.getLogger(SunPowerServiceImpl.class);

    @Autowired
    SunPowerMapper sunPowerMapper;

    @Autowired
    private PlatformTransactionManager transactionManager;

    DefaultTransactionDefinition def = null;
    TransactionStatus status = null;

    public List<TSunPower> search(SearchParam search) {
        return sunPowerMapper.searchSunPower(search);
    };

    public List<ResSunPower> searchYearSum(SearchParam search) {
        List<TSunPower> list = sunPowerMapper.searchSunPowerYearSum(search);
        List<ResSunPower> res = new ArrayList<ResSunPower>();
        for(TSunPower sun : list)
        {
            ResSunPower sum = new ResSunPower();
            sum.setCYear(sun.getcYear());
            sum.setAmt(sun.getnPower());
            res.add(sum);
        }
        return res;

    };


    public void upsert(TSunPower s) {
        TSunPower rs = sunPowerMapper.findSunPower(s.getIdSunPower());
        if (rs != null) {
            if (s.getcYear() == null)
                s.setcYear(rs.getcYear());

            if (s.getcMon() == null)
                s.setcMon(rs.getcMon());

            if (s.getnPower() == null)
                s.setnPower(rs.getnPower());
        }

        sunPowerMapper.upsert(s);
    }

    public void upsert(List<ReqSunPower> reqParam) {

            def = new DefaultTransactionDefinition();
            def.setPropagationBehavior(TransactionDefinition.PROPAGATION_REQUIRED);
            status = transactionManager.getTransaction(def);
            try {
            for (ReqSunPower sp : reqParam) {
                TSunPower rs = sunPowerMapper.findSunPower(sp.getIdSunPower());
                if (rs != null) {
                    if (sp.getcYear() == null)
                        sp.setcYear(rs.getcYear());

                    if (sp.getcMon() == null)
                        sp.setcMon(rs.getcMon());

                    if (sp.getnPower() == null)
                        sp.setnPower(rs.getnPower());
                }
                sunPowerMapper.upsert(sp.toEntity());
            }
            transactionManager.commit(status);
        } catch (Exception e) {
            transactionManager.rollback(status);
            logger.error("upsert ERROR "+ e.getMessage());
            throw new BaseException(e.getMessage());
        }
        
    }
}
