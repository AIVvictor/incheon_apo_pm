package com.sebang.ems.service;

import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.Date;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.annotation.PostConstruct;

import com.sebang.ems.config.AppConfig;
import com.sebang.ems.dao.SiteDeptMapper;
import com.sebang.ems.dao.UsersMapper;
import com.sebang.ems.dao.gen.TErpUsersMapper;
import com.sebang.ems.dao.gen.TRawErpDeptMapper;
import com.sebang.ems.model.SiteDept;
import com.sebang.ems.model.Do.TErpUsers;
import com.sebang.ems.model.Do.TRawErpDept;
import com.sebang.ems.model.Do.TUsers;
import com.sebang.ems.model.Dto.ReqErpDept;
import com.sebang.ems.model.Dto.ReqErpUsers;
import com.sebang.ems.util.CsvToObject;
import com.sebang.ems.util.DateUtil;
import com.sebang.ems.util.StringUtil;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.factory.PasswordEncoderFactories;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

@Service
public class SyncServiceImpl implements SyncService {
    public static final Logger logger = LoggerFactory.getLogger(SyncServiceImpl.class);

    final String deptCsvName = "dept.csv";
    final String personCsvName = "person.csv";
    final String retireCsvName = "retire.csv";

    String importPath;

    long lastModifiedAtforDept = 0;
    long lastModifiedAtforPerson = 0;
    long lastModifiedAtforRetire = 0;

    private PasswordEncoder passwordEncoder;

    boolean enabledTrace = false;

    String loadCsvAtStar = "false";

    @Autowired
    SiteDeptMapper siteDeptMapper;

    @Autowired
    TRawErpDeptMapper rawErpDeptMapper;

    @Autowired
    UsersMapper usersMapper;

    @Autowired
    TErpUsersMapper erpUsersMapper;


    @PostConstruct
    public void init() {
        importPath = AppConfig.instance().getProperty("sync.path");
        logger.info("import path " + importPath);

        Path currentRelativePath = Paths.get(importPath + "/" + deptCsvName);
        String s = currentRelativePath.toAbsolutePath().toString();
        logger.info("import csv path " + s);
        passwordEncoder = PasswordEncoderFactories.createDelegatingPasswordEncoder();

        loadCsvAtStar = AppConfig.instance().getProperty("bgtask.loadcsv.atStart");
        if ("false".equals(loadCsvAtStar)) {
            Path depcsvPath = Paths.get(importPath + "/" + deptCsvName);

            if (Files.exists(depcsvPath)) {
                File f1 = new File(depcsvPath.toAbsolutePath().toString());
                this.lastModifiedAtforDept = f1.lastModified();

                logger.debug("<<<< Set  dept.csv last modified at " + lastModifiedAtforDept + " >>>>");
            }

            Path personcsvPath = Paths.get(importPath + "/" + personCsvName);
            if (Files.exists(depcsvPath)) {
                File f2 = new File(personcsvPath.toAbsolutePath().toString());
                this.lastModifiedAtforPerson = f2.lastModified();

                logger.debug("<<<< Set  person.csv last modified at " + lastModifiedAtforPerson + " >>>>");
            }

            Path retirecsvPath = Paths.get(importPath + "/" + retireCsvName);
            if (Files.exists(retirecsvPath)) {
                File file = new File(retirecsvPath.toAbsolutePath().toString());

                this.lastModifiedAtforRetire = file.lastModified();
                logger.debug("<<<< Set  retire.csv last modified at " + lastModifiedAtforRetire + " >>>>");
            }

            return;
        }
    }

    public boolean isErpUser(String sErpUserId) {
        boolean isErpUser = false;

        Pattern infoPattern = Pattern.compile("7[0789][0-9]{6}");
        Matcher infoMatcher = infoPattern.matcher(sErpUserId);
        if (infoMatcher.find()) { // find가 group보다 선행되어야 합니다.
            isErpUser = true;
            /// System.out.println(infoMatcher.group()); // 77000162
        }
        return isErpUser;
    }

    public boolean isEmail(String mail)
    {
        boolean result = false;
        Pattern p = Pattern.compile("^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,6}$", Pattern.CASE_INSENSITIVE);
        Matcher m = p.matcher(mail);
        if(m.find())
            result = true;

        return result;
    }

    public boolean isErpDept(String sErpDept) {
        boolean isErpUser = false;

        Pattern infoPattern = Pattern.compile("7[0-9]{7}");
        Matcher infoMatcher = infoPattern.matcher(sErpDept);
        if (infoMatcher.find()) { 
            isErpUser = true;
        }
        return isErpUser;
    }

    public void syncErpDept(boolean backupCsv, boolean btrace) {
        if (importPath == null)
            return;

        Path currentRelativePath = Paths.get(importPath + "/" + deptCsvName);
        Path deptUtf8RelativePath = Paths.get(importPath + "/" + "dept_utf8.csv");
        String s = currentRelativePath.toAbsolutePath().toString();
        if (!Files.exists(currentRelativePath)) {
            logger.debug("import csv not found " + s);
            return;
        }
        logger.debug("<<<< syncErpDept >>>>");

        // 1 파일 변경 여부 확인
        File file = new File(s);
        long lastModifiedAt = file.lastModified();
        if (this.lastModifiedAtforDept == lastModifiedAt) {
            return;
        }

        logger.info("<<<< dept.csv last modified at " + lastModifiedAt + " >>>>");

        // 2.T_RAW_ERP_DEPT 의 마지막 업데이트 시간 읽기.
        Date lastRawErpDeptUpdatedAt = siteDeptMapper.lastModifiedAtTRawErpDept();
        if (lastRawErpDeptUpdatedAt == null)
            lastRawErpDeptUpdatedAt = DateUtil.nowAfterDaysToDate(-1l);
        logger.info("<<<< syncErpDept T_RAW_ERP_DEPT last modified at " + lastRawErpDeptUpdatedAt +" >>>>");

        // 3. dept.csv 읽기.
        CsvToObject<ReqErpDept> csv = new CsvToObject<ReqErpDept>();
        try {
            csv.convertToUtf8(currentRelativePath, deptUtf8RelativePath);
            List<ReqErpDept> dept = csv.build(deptUtf8RelativePath, ReqErpDept.class, Charset.forName("UTF-8"));
            if (dept.size() == 0)
                throw new Exception("Empty File " + currentRelativePath);

            logger.info("<<<< syncErpDept dept.csv READ ROW COUNT =" + dept.size() + " >>>>");

            int updatedCount=0;
            // 4. T_RAW_ERP_DEPT 와 변경사항을 반영.
            for (ReqErpDept csvDept : dept) {
                boolean isUpdate = false;
                if ("부서코드".equals(csvDept.getSDeptCd()))
                    continue;

                if (csvDept.getSDeptCd() == null)
                    continue;

                if (csvDept.getSDeptCd().length() == 0)
                    continue;

            //    if(!isErpDept(csvDept.getSDeptCd()))
                //    continue;
                
                String sDeptCd = csvDept.getSDeptCd().trim();

                TRawErpDept rawDept = rawErpDeptMapper.selectByPrimaryKey(sDeptCd);
                if (rawDept != null) {
                    if (!csvDept.isEqual(rawDept)) {
                        rawErpDeptMapper.updateByPrimaryKeySelective(csvDept.toEntity());
                        isUpdate = true;
                    }
                } else {
                    rawDept = new TRawErpDept();
                    rawDept.setsDeptId(sDeptCd);
                    rawDept.setsDeptNm(csvDept.getSDeptNm());
                    if(csvDept.getSUDeptCd()!=null){
                        csvDept.setSUDeptCd(csvDept.getSUDeptCd().trim());
                    }
                    rawDept.setsUDeptId(csvDept.getSUDeptCd());
                    rawDept.setcDelYn("N");
                    rawErpDeptMapper.insertSelective(rawDept);
                    isUpdate = true;
                }

                if (isUpdate){
                    logger.info("syncErpDept updated " + csvDept);
                    updatedCount++;
                }
            }            
            logger.info("<<<< syncErpDept T_RAW_ERP_DEPT UPDATED COUNT " + updatedCount + ">>>>");;
            // 5. dept 이외의 항목 삭제=C_DEL_YN = 'Y' 모 마크
            if (btrace) {
                List<SiteDept> delList = siteDeptMapper.findDeleteListErpDept(dept);
                delList.stream().forEach(delDept -> {
                    logger.info("delete list=>" + delDept);
                });
            }
            siteDeptMapper.deleteRawErpDept(dept);

            // 6. 마지막 업데이트 이후의 변경 건만 T_ERP_DEPT에 업데이트
            if (btrace) {
                List<SiteDept> upsertList = siteDeptMapper.findUpsertListErpDept(lastRawErpDeptUpdatedAt);
                upsertList.stream().forEach(upsertDept -> {
                    logger.info("PULL DEPT =>" + upsertDept);
                });
                logger.info("<<<< syncErpDept PULL UPDATE LIST COUNT " + upsertList.size() + ">>>>");
            }

            siteDeptMapper.importErpDeptFormRawErpDept(lastRawErpDeptUpdatedAt);
            Files.delete(deptUtf8RelativePath);

            this.lastModifiedAtforDept = lastModifiedAt;
            logger.info("<<<< syncErpDept PULL UPDATE LIST " + " DONE " + " >>>>");
        } catch (Exception e) {
            logger.error("syncErpDept ERROR " + e.getMessage());
            return;
        }

        if (backupCsv) {
            logger.info("<<<< BACKUP dept.csv file  " + ">>>>");
            // 7. dept.csv 를 이동.
            Path target = Paths.get(importPath + "/" + deptCsvName + "-" + DateUtil.getTimeNowFormat());
            try {
                Files.move(currentRelativePath, target, StandardCopyOption.REPLACE_EXISTING);
                
            } catch (IOException e) {
                logger.error("syncErpDept Don't Move ERROR " + e.getMessage());
            }
        }

    }

    public void syncErpUsers(boolean bakupCsv, boolean btrace) {

        if (importPath == null)
            return;

        Path currentRelativePath = Paths.get(importPath + "/" + "person.csv");
        Path personUtf8RelativePath = Paths.get(importPath + "/" + "person_utf8.csv");
        String s = currentRelativePath.toAbsolutePath().toString();
        if (!Files.exists(currentRelativePath)) {
            logger.debug("import csv not found " + s);
            return;
        }

        // 1 파일 변경 여부 확인
        File file = new File(s);
        long lastModifiedAt = file.lastModified();
        if (this.lastModifiedAtforPerson == lastModifiedAt) {
            return;
        }
        logger.info("<<<< syncErpUsers person.csv file last modified at " + lastModifiedAt);

        // 2.T_ERP_USERS 의 마지막 업데이트 시간 읽기.
        Date lastModifiedAtTErpUsers = usersMapper.lastModifiedAtTErpUsers();
        if (lastModifiedAtTErpUsers == null)
            lastModifiedAtTErpUsers = new Date();

        logger.info("<<<< syncErpUsers T_ERP_USERS last modified at " + lastModifiedAtTErpUsers + ">>>>");

        // 3. person.csv 읽기.
        CsvToObject<ReqErpUsers> csv = new CsvToObject<ReqErpUsers>();
        try {

            csv.convertToUtf8(currentRelativePath, personUtf8RelativePath);
            List<ReqErpUsers> users = csv.build(personUtf8RelativePath, ReqErpUsers.class, Charset.forName("UTF-8"));
            if (users.size() == 0)
                throw new Exception("Empty File " + currentRelativePath);

            logger.info("<<<< syncErpUsers person.csv READ ROW COUNT =" + users.size() + " >>>>");

            // 4. T_ERP_USERS 와 변경사항을 반영.
            for (ReqErpUsers erpUser : users) {
                boolean isUpdate = false;
                if ("사번".equals(erpUser.getSEmpId()))
                    continue;

                if (erpUser.getSEmpId() == null)
                    continue;

                if (erpUser.getSEmpId().length() == 0)
                    continue;

                if (!isErpUser(erpUser.getSEmpId())) {
                    logger.debug("NOT ERP USER " + erpUser);
                    continue;
                }

                if (StringUtil.isEmpty(erpUser.getSEmpMail())) // email 이 비어 있는 경우
                    continue;

                String mail[] = erpUser.getSEmpMail().split("@");
                if (mail.length == 1)
                    continue;

                if (StringUtil.isEmpty(mail[1])) // 도메인 영역이 비어 있으면
                    continue;

                TErpUsers u = usersMapper.findErpUserByEmpId(erpUser.getSEmpId());
                try {
                    if (u != null) {
                        if (!erpUser.isEqual(u)) {
                            erpUser.setIdErpUsers(u.getIdErpUsers());
                            usersMapper.upsertErpUsers(erpUser.toEntity());
                            isUpdate = true;
                        }
                    } else {
                        usersMapper.upsertErpUsers(erpUser.toEntity());
                        isUpdate = true;
                    }

                    if (isUpdate)
                        logger.debug("syncErpUser updated " + erpUser);

                } catch (Exception ex) {
                    logger.error("NOT IMPORT " + u.getsEmpId() + "error : " + ex.getMessage());
                }

            }

            // 5. user 이외의 항목 삭제=C_DEL_YN = 'Y' 로 마크
            if (btrace) {
                List<TErpUsers> res = usersMapper.findDeleteRawErpUsers(users);
                res.stream().forEach(erpUsers -> {
                    logger.info("delete EmpId:" + erpUsers.getsEmpId() + ",EmpNm:" + erpUsers.getsEmpNm());
                });
            }
            usersMapper.deleteRawErpUsers(users);

            // 6. 마지막 업데이트 이후의 변경 건만 T_ERP_USER 에서 가져온다.
            List<TErpUsers> list = usersMapper.pullErpUsers(lastModifiedAtTErpUsers);
            logger.info("<<<< syncErpUsers PULL UPDATE LIST COUNT " + list.size() + ">>>>");

            for (TErpUsers erpUser : list) {
                if (erpUser.getsEmpMail() == null || erpUser.getsEmpMail().length() == 0) // email 이 비어 있는 경우
                    continue;

                String mail[] = erpUser.getsEmpMail().split("@");
                if (StringUtil.isEmpty(mail[1])) // 도메인 영역이 비어 있으면
                    continue;

                TUsers user = usersMapper.findById(erpUser.getsEmpId());
                if (user == null) {
                    user = new TUsers();

                    String encPassword = passwordEncoder.encode(mail[0]);
                    user.setsEmpPwd(encPassword);
                    user.setsEmpId(erpUser.getsEmpId());
                    user.setsStatusCd("1"); /* 입사 */
                    user.setcDelYn("N");
                    user.setcAuthCode("IUSR");
                }

                if (erpUser.getsEmpNm() == null)
                    user.setsEmpNm("");
                else
                    user.setsEmpNm(erpUser.getsEmpNm());

                Long idErpDept = siteDeptMapper.findErpDeptKeyById(erpUser.getsDeptId());
                user.setIdErpDept(idErpDept);

                if (idErpDept != null) {
                    Long idSite = siteDeptMapper.findIdSiteByIdErpDept(idErpDept);
                    user.setIdSite(idSite);
                }

                user.setcRankCd(erpUser.getcRankCd());
                user.setcRankNm(erpUser.getcRankNm());
                user.setsEmpMail(erpUser.getsEmpMail());
                user.setcDelYn(erpUser.getcDelYn());

                if (erpUser.getcEDate() != null) {
                    user.setcEDate(DateUtil.convertToYYYYMMDD(erpUser.getcEDate(), null));
                    user.setcDelYn("Y");
                }

                usersMapper.upsert(user);

                if (btrace) {
                    StringBuilder sb = new StringBuilder();
                    sb.append("idEmp:" + user.getIdEmp());
                    sb.append(",sEmpId:" + user.getsEmpId());
                    sb.append(",sEmpNm:" + user.getsEmpNm());
                    sb.append(",idErpDept:" + user.getIdErpDept());
                    sb.append(",idSite:" + user.getIdSite());
                    sb.append(",cSDate:" + user.getcSDate());
                    sb.append(",cEDate:" + user.getcEDate());
                    sb.append(",sEmpMail:" + user.getsEmpMail());
                    sb.append(",cRankCd:" + user.getcRankCd());
                    sb.append(",cRankNm:" + user.getcRankNm());
                    // sb.append(",cPosCd:"+tu.getcPosCd());
                    // sb.append(",cPosNm:"+ tu.getcPosNm());
                    sb.append(",cDelYn:" + user.getcDelYn());

                    logger.info("import User:" + sb.toString());
                }
            }
            Files.delete(personUtf8RelativePath);

            logger.info("<<<< syncErpUsers PULL UPDATE LIST COUNT " + list.size() + " DONE " + " >>>>");
            this.lastModifiedAtforPerson = lastModifiedAt;
        } catch (Exception e) {
            logger.error("syncErpUsers ERROR " + e.getMessage());
            e.printStackTrace();
            return;
        }

        if (bakupCsv) {
            logger.info("<<<< syncErpUsers BACKUP person.csv file  " + ">>>>");

            // 7. dept.csv 를 이동.
            Path target = Paths.get(importPath + "/" + personCsvName + "-" + DateUtil.getTimeNowFormat());
            try {
                Files.move(currentRelativePath, target, StandardCopyOption.REPLACE_EXISTING);
            } catch (IOException e) {
                logger.error("syncErpUsers Don't Move ERROR " + e.getMessage());
            }

        }

    }

    public void syncErpRetire(boolean backupCsv, boolean btrace) {

        if (importPath == null)
            return;

        Path currentRelativePath = Paths.get(importPath + "/" + "retire.csv");
        Path retireUtf8RelativePath = Paths.get(importPath + "/" + "retire_utf8.csv");
        String s = currentRelativePath.toAbsolutePath().toString();
        if (!Files.exists(currentRelativePath)) {
            logger.debug("import csv not found " + s);
            return;
        }
        logger.debug("<<<< syncRetire >>>>");

        // 1 파일 변경 여부 확인
        File file = new File(s);
        long lastModifiedAt = file.lastModified();
        if (this.lastModifiedAtforRetire == lastModifiedAt)
            return;

        logger.info("<<<< syncRetire retire.csv last modified at " + lastModifiedAt + " >>>>");

        // 2.T_ERP_USERS 의 마지막 업데이트 시간 읽기.
        Date lastModifiedAtTErpUsers = usersMapper.lastModifiedAtTErpUsers();
        logger.info("<<<< syncRetire T_ERP_USERS last modified at " + lastModifiedAtTErpUsers + " >>>>");

        // 3. retire.csv 읽기.
        CsvToObject<ReqErpUsers> csv = new CsvToObject<ReqErpUsers>();
        try {
            csv.convertToUtf8(currentRelativePath, retireUtf8RelativePath);
            List<ReqErpUsers> users = csv.build(retireUtf8RelativePath, ReqErpUsers.class, Charset.forName("UTF-8"));
            if (users.size() == 0)
                throw new Exception("Empty File " + currentRelativePath);

            logger.info("<<<< syncRetire retire.csv READ ROW COUNT =" + users.size() + " >>>>");

            // 4. T_ERP_USERS 와 변경사항을 반영.
            for (ReqErpUsers retiredUser : users) {
                // boolean isUpdate = false;
                if ("사번".equals(retiredUser.getSEmpId()))
                    continue;

                if (retiredUser.getSEmpId() == null)
                    continue;

                if (retiredUser.getSEmpId().length() == 0)
                    continue;

                TErpUsers u = usersMapper.findErpUserByEmpId(retiredUser.getSEmpId());
                if (u == null) {
                    logger.info("syncRetire Not found Emp record " + retiredUser);
                    continue;
                }

                if ("Y".equals(u.getcDelYn()))// 이미 삭제 처리된 사원
                    continue;

                retiredUser.setIdErpUsers(u.getIdErpUsers());
                retiredUser.setCDelYn("Y");
                usersMapper.upsertErpUsers(retiredUser.toEntity());

                logger.info("syncRetire updated " + retiredUser);
            }

            // 6. 마지막 업데이트 이후의 변경 건만 T_USERS에 업데이트
            List<TErpUsers> list = usersMapper.pullErpUsers(lastModifiedAtTErpUsers);
            logger.info("<<<< syncRetire PULL UPDATE LIST COUNT " + list.size() + ">>>>");
            for (TErpUsers erpUser : list) {
                TUsers tu = usersMapper.findById(erpUser.getsEmpId());
                if (tu == null) {
                    logger.info("syncRetire not found Empid in T_USERS " + erpUser.getsEmpId());
                    continue;
                }

                if ("2".equals(tu.getsStatusCd()) || "Y".equals(tu.getcDelYn()))
                    continue; // 이미 삭제 처리가 된 경우

                tu.setsEmpNm("");
                tu.setcDelYn("Y");
                tu.setsEmpMail("");
                tu.setcEDate(DateUtil.convertToYYYYMMDD(erpUser.getcEDate(), null));
                tu.setsStatusCd("2"); /* 퇴사 */

                usersMapper.update(tu);

                if (btrace) {
                    StringBuilder sb = new StringBuilder();
                    sb.append("idErpUsers:" + erpUser.getIdErpUsers());
                    sb.append("sEmpId:" + erpUser.getsEmpId());
                    sb.append("sEmpNm:" + erpUser.getsEmpNm());
                    sb.append("sDeptId:" + erpUser.getsDeptId());
                    sb.append("cSDate:" + erpUser.getcSDate());
                    sb.append("cEDate:" + erpUser.getcEDate());
                    sb.append("sEmpMail:" + erpUser.getsEmpMail());
                    sb.append("cRankCd:" + erpUser.getcRankCd());
                    sb.append("cRankNm:" + erpUser.getcRankNm());
                    sb.append("cPosCd:" + erpUser.getcPosCd());
                    sb.append("cPosNm:" + erpUser.getcPosNm());
                    sb.append("cDelYn:" + erpUser.getcDelYn());

                    logger.info("retired User:" + sb.toString());
                }
            }
            Files.delete(retireUtf8RelativePath);
            logger.info("<<<< syncRetire PULL UPDATE LIST COUNT " + list.size() + " DONE " + " >>>>");
            this.lastModifiedAtforRetire = lastModifiedAt;
        } catch (Exception e) {
            logger.error("syncRetire ERROR " + e.getMessage());
            return;
        }

        if (backupCsv) {
            // 7. retire.csv 를 이동.
            Path target = Paths.get(importPath + "/" + retireCsvName + "-" + DateUtil.getTimeNowFormat());
            try {
                Files.move(currentRelativePath, target, StandardCopyOption.REPLACE_EXISTING);
            } catch (IOException e) {
                logger.error("syncErpRetire Don't Move ERROR " + e.getMessage());
            }
        }

    }
}