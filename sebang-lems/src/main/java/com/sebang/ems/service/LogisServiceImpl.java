package com.sebang.ems.service;

import java.math.BigDecimal;
import java.util.List;

import javax.validation.Valid;

import com.sebang.ems.dao.AmtMapper;
import com.sebang.ems.dao.LogisMapper;
import com.sebang.ems.dao.SiteDeptMapper;
import com.sebang.ems.domain.Exception.AlreadyMonthEndClosingException;
import com.sebang.ems.domain.Exception.BaseException;
import com.sebang.ems.domain.common.FieldErrorEx;
import com.sebang.ems.domain.common.SearchParam;
import com.sebang.ems.model.EngPointAmt;
import com.sebang.ems.model.Do.TEngPoint;
import com.sebang.ems.model.Do.TLogisTransInfo;
import com.sebang.ems.model.Do.TLogisTransMonInfo;
import com.sebang.ems.model.Do.TMonEngClose;
import com.sebang.ems.model.Do.TTransCom;
import com.sebang.ems.model.Do.TVehicle;
import com.sebang.ems.model.Do.VLogisDrvLog;
import com.sebang.ems.model.Do.VLogisEff;
import com.sebang.ems.model.Do.VLogisTransYearVhcl;
import com.sebang.ems.model.Do.VVehicle;
import com.sebang.ems.model.Dto.ReqLogisTransInfo;
import com.sebang.ems.model.Dto.ReqTransCom;
import com.sebang.ems.model.Dto.ReqVehicle;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.TransactionDefinition;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.support.DefaultTransactionDefinition;
import org.springframework.validation.BindingResult;

@Service
public class LogisServiceImpl implements LogisService {

    static final String S_INV_TRANS_CD_VEHICLE = "1";
    static final String S_INV_TRANS_CD_SHIP = "2";
    static final String S_INV_TRANS_CD_TRAIN = "3";

    public static final Logger logger = LoggerFactory.getLogger(LogisServiceImpl.class);

    @Autowired
    LogisMapper logisMapper;

    @Autowired
    AmtMapper amtMapper;

    @Autowired
    SiteDeptMapper siteDeptMapper;

    @Autowired
    private PlatformTransactionManager transactionManager;

    DefaultTransactionDefinition def = null;
    TransactionStatus status = null;

    /**
     * 운송정보 조회
     * 
     * @param param
     * @return List<VLogisDrvLog>
     */
    public List<VLogisDrvLog> searchVLogisDrvLog(SearchParam param) {

        if (param.getCMon() != null && param.getCMon().length() == 1) {
            param.setCMon("0" + param.getCMon());
        }

        return logisMapper.searchVLogisDrvLog(param);
    }

    /**
     * 운송정보 등록 수정
     * 
     * @param drv
     */
    public void upsertLogisTransInfo(TLogisTransInfo drv) {
        TLogisTransInfo info = logisMapper.findLogisTransInfo(drv.getIdLogisTransInfo());
        if (info != null) {
            if (drv.getIdEngPoint() == null)
                drv.setIdEngPoint(info.getIdEngPoint());

            if (drv.getcDate() == null)
                drv.setcDate(info.getcDate());

            if (drv.getnFuelEff() == null)
                drv.setnFuelEff(info.getnFuelEff());
        }

        logisMapper.upsertLogisTransInfo(drv);
    }

    /**
     * 운송 정보 삭제
     * 
     * @param idLogisTransInfo
     */
    public void deleteLogisTransInfo(Long idLogisTransInfo) {
        TLogisTransInfo drv = new TLogisTransInfo();
        drv.setIdLogisTransInfo(idLogisTransInfo);
        drv.setcDelYn("Y");
        logisMapper.upsertLogisTransInfo(drv);
    }

    /**
     * 운송 정보 수정
     * 
     * @param drv
     */
    public void upsertLogisTransMonInfo(TLogisTransMonInfo drv) {

        if (drv.getLoadageSum() == null || BigDecimal.ZERO.compareTo(drv.getLoadageSum()) >= 0) {
            logger.error("loadageSum <= 0");
            throw new BaseException("loadageSum <= 0");
        }

        if (drv.getDistanceSum() == null || BigDecimal.ZERO.compareTo(drv.getDistanceSum()) >= 0) {
            logger.error("distanceSum <= 0");
            throw new BaseException("distanceSum <= 0");
        }

        TLogisTransMonInfo info = logisMapper.findLogisTransMonInfo(drv.getIdLogisTransMonInfo());
        if (info != null) {
            if (drv.getIdEngPoint() == null)
                drv.setIdEngPoint(info.getIdEngPoint());

            if (drv.getDateMon() == null)
                drv.setDateMon(info.getDateMon()); // DB NOT NULL CONSTRAINS

            if (drv.getFuelEff() == null)
                drv.setFuelEff(info.getFuelEff()); // DB NOT NULL CONSTRAINS
        }

        if (drv.getDistanceSum() != null && drv.getFuelEff() != null) {
            if (BigDecimal.ZERO.compareTo(drv.getFuelEff()) >= 0) {
                logger.error("fuelErr <= 0");
                throw new BaseException("fuelErr <= 0");
            }

            BigDecimal fuelEff = drv.getFuelEff();

            // 연료 사용량 = 운송거리 / 공인연비
            BigDecimal volSum = drv.getDistanceSum().divide(fuelEff, 4, BigDecimal.ROUND_CEILING);
            drv.setEngVolSum(volSum);
        }

        if (drv.getEngVolSum() != null) {
            EngPointAmt amt = amtMapper.findEngConversion(drv.getIdEngPoint(), drv.getEngVolSum());
            if (amt != null) {
                drv.setGco2Sum(amt.getnTco2()); // 온실가스 배출량
                drv.setMjSum(amt.getnGj()); // 에너지 사용량
                drv.setToeSum(amt.getnToe());// 환산톤합
                if (drv.getGco2Sum() != null && drv.getLoadageSum() != null && drv.getDistanceSum() != null) {
                    if (BigDecimal.ZERO.compareTo(drv.getLoadageSum()) != 0
                            && BigDecimal.ZERO.compareTo(drv.getDistanceSum()) != 0) {

                        // 적재량을 KG 에서 Ton 으로 환산
                        BigDecimal loadageSumTon = drv.getLoadageSum().divide(new BigDecimal(1000), 4,
                                BigDecimal.ROUND_CEILING);

                        // BigDecimal tonKmSum = drv.getGco2Sum().divide( loadageSumTon.multiply(
                        // drv.getDistanceSum()),4,BigDecimal.ROUND_CEILING);
                        BigDecimal tonKmSum = loadageSumTon.multiply(drv.getDistanceSum());
                        drv.setTonKmSum(tonKmSum);
                    }
                }
            }
        }

        logisMapper.upsertLogisTransMonInfo(drv);
    }

    /**
     * 운송 정보 등록 수정 [월 집계 다중 등록]
     * 
     * @param reqParam
     */
    public void upsertLogisTransMonInfo(List<TLogisTransMonInfo> reqParam) {

        if (reqParam.size() > 0) {
            TLogisTransMonInfo a = reqParam.get(0);
            if (a.getDateMon() != null && a.getDateMon().length() == 6) {
                String cYear = a.getDateMon().substring(0, 4);
                // 201911
                String cMon = a.getDateMon().substring(4, 6);

                // TMonEngClose close =
                // amtMapper.findMonthEndClosingByIdErpDept(a.getIdErpDept(), cYear, cMon);
                TMonEngClose close = amtMapper.findMonthEndClosingByIdEngPoint(a.getIdEngPoint(), cYear, cMon);

                if (close != null && "Y".equals(close.getcCloseYn()))
                    throw new AlreadyMonthEndClosingException("Closed Eng Mon");

            }

        }

        def = new DefaultTransactionDefinition();
        def.setPropagationBehavior(TransactionDefinition.PROPAGATION_REQUIRED);
        status = transactionManager.getTransaction(def);
        try {
            for (TLogisTransMonInfo item : reqParam) {
                upsertLogisTransMonInfo(item);
            }
            transactionManager.commit(status);
        } catch (Exception ex) {
            transactionManager.rollback(status);
            logger.error("upsertLogisTransMonInfo ERROR " + ex.getMessage());
            throw new BaseException(ex.getMessage());
        }
        
    }

    /**
     * 운송정보 삭제 월 집계
     * 
     * @param idLogisTransMonInfo
     */
    public void deleteLogisTransMonInfo(Long idLogisTransMonInfo) {
        TLogisTransMonInfo drv = new TLogisTransMonInfo();
        drv.setIdLogisTransMonInfo(idLogisTransMonInfo);
        drv.setcDelYn("Y");
        logisMapper.upsertLogisTransMonInfo(drv);
    }

    /**
     * 차량 정보 수정 단일
     * 
     * @param vehicle
     */
    public void updateVehicle(TVehicle vehicle) {
        TVehicle v = logisMapper.findVehicleByKey(vehicle.getIdVehicle());
        if (v == null)
            throw new BaseException("Invalid vehicle id=>" + vehicle.getIdVehicle());

        vehicle.setIdErpDept(v.getIdErpDept());
        vehicle.setIdTransCom(v.getIdTransCom());
        vehicle.setsVrn(v.getsVrn());

        logisMapper.upsertVehicle(vehicle);
    }

    /**
     * 차량을 수정 다중 수정 물류에너지관리/차량정보 화면에서 노선 구분과 차량 연비 수정을 위한 용도.
     * 
     * @param list
     */
    public void updateVehicle(List<ReqVehicle> list) {

        try {
            def = new DefaultTransactionDefinition();
            def.setPropagationBehavior(TransactionDefinition.PROPAGATION_REQUIRED);
            status = transactionManager.getTransaction(def);
            for (ReqVehicle v : list) {
                TVehicle t = new TVehicle();
                t.setIdVehicle(v.getIdVehicle());
                t.setsLineCd(v.getsLineCd());
                t.setsFuelEff(v.getnFuelEff());
                t.setsModId(v.getsModId());
                updateVehicle(t);
            }
            transactionManager.commit(status);
        } catch (Exception ex) {
            transactionManager.rollback(status);
            logger.error("updateVehicle ERROR " + ex.getMessage());
            throw new BaseException(ex.getMessage());
        }
        
    }

    /**
     * 차량정보 등록 수정. 다중 등록
     * 
     * @param list
     */
    @Transactional(rollbackFor = Exception.class)
    public void upsertVehicles(List<ReqVehicle> list) {

        // def = new DefaultTransactionDefinition();
        // def.setPropagationBehavior(TransactionDefinition.PROPAGATION_REQUIRED);
        // status = transactionManager.getTransaction(def);
        // try {
        for (ReqVehicle v : list) {
            TVehicle e = v.toEntity();
            logisMapper.upsertVehicle(e);
        }

        // } catch (Exception e) {
        // transactionManager.rollback(status);
        // logger.error("upsertVehicles ERROR " + e.getMessage());
        // throw new BaseException(e.getMessage());
        // } finally{
        // transactionManager.commit(status);
        // }
    }

    public TVehicle findVehicleByVrn(String sVrn) {
        return logisMapper.findVehicleByVrn(sVrn);
    }

    /**
     * 운수사 정보 조회
     * 
     * @param sTransComNm
     */
    public TTransCom findTransComById(String sTransComId) {
        return logisMapper.findTransComById(sTransComId);
    }

    /**
     * 부서 정보 조회
     * 
     * @param sErpId
     */
    public Long findErpDeptIdByErpId(String sErpId) {
        return siteDeptMapper.findErpDeptKeyById(sErpId);
    }

    /**
     * 운수사 정보 등록 수정 - 다중
     * 
     * @param list
     */
    @Transactional(rollbackFor = Exception.class)
    public void upsertTransComs(List<ReqTransCom> list) {
        // def = new DefaultTransactionDefinition();
        // def.setPropagationBehavior(TransactionDefinition.PROPAGATION_REQUIRED);
        // status = transactionManager.getTransaction(def);
        // try {

        for (ReqTransCom t : list) {
            logisMapper.upsertTransCom(t.toEntity());
        }
        // } catch (Exception e) {
        // transactionManager.rollback(status);
        // logger.error("upsertTransCom =>" + e.getMessage());
        // throw new BaseException(e.getMessage());
        // } finally
        // {
        // transactionManager.commit(status);
        // }
    }

    /**
     * 운송 정보 등록 수정
     * 
     * @param list
     */
    @Transactional(rollbackFor = Exception.class)
    public void upsertLogisTransInfos(List<ReqLogisTransInfo> list) {

        // def = new DefaultTransactionDefinition();
        // def.setPropagationBehavior(TransactionDefinition.PROPAGATION_REQUIRED);
        // status = transactionManager.getTransaction(def);
        // try {

        for (ReqLogisTransInfo t : list) {
            TLogisTransInfo e = t.toEntity();

            if (e.getnFuelEff() == null)
                e.setnFuelEff(BigDecimal.ZERO);

            // if (e.getIdLogisTransInfo() == null && e.getTransId() != null) {
            //     TLogisTransInfo transInfo = logisMapper.findLogisTransInfoByTransId(e.getTransId());
            //     if (transInfo != null)
            //         e.setIdLogisTransInfo(transInfo.getIdLogisTransInfo());
            // }
            logisMapper.upsertLogisTransInfo(e);
        }
        // transactionManager.commit(status);
        // } catch (Exception e) {
        // transactionManager.rollback(status);
        // logger.error("upsertTransCom ERROR " + e.getMessage());
        // throw new BaseException(e.getMessage());
        // }finally
        // {
        // transactionManager.commit(status);
        // }
    }

    /**
     * 차량 정보 검색
     * 
     * @param p
     * @return List<VVehicle>
     */
    public List<VVehicle> searchVehicle(SearchParam p) {
        return logisMapper.searchVehicle(p);
    }

    /**
     * 차량 정보 삭제
     * 
     * @param vehicle
     */
    public void deleteVehicle(TVehicle vehicle) {

        TVehicle v = logisMapper.findVehicleByKey(vehicle.getIdVehicle());
        if (v == null)
            throw new BaseException("Invalid vehicle id=>" + vehicle.getIdVehicle());

        vehicle.setIdErpDept(v.getIdErpDept());
        vehicle.setIdTransCom(v.getIdTransCom());
        vehicle.setcDelYn("Y");

        logisMapper.upsertVehicle(vehicle);
    }

    /**
     * 차량 정보 삭제 다중
     * 
     * @param list
     */
    public void deleteVehicle(List<TVehicle> list) {
        try {
            def = new DefaultTransactionDefinition();
            def.setPropagationBehavior(TransactionDefinition.PROPAGATION_REQUIRED);
            status = transactionManager.getTransaction(def);
            for (TVehicle v : list) {
                deleteVehicle(v);
            }
            transactionManager.commit(status);
        } catch (Exception ex) {
            transactionManager.rollback(status);
            logger.error("deleteVehicle ERROR " + ex.getMessage());
            throw new BaseException(ex.getMessage());
        }
        
    }

    /**
     * @param p 온실가스/에너지 현황 조회
     * @return List<VLogisTransYearVhcl>
     */
    public List<VLogisTransYearVhcl> searchVLogisTransYear(SearchParam p) {
        return logisMapper.searchVLogisTransYear(p);
    }

    /**
     * @param p 효율화 조회
     * @return List<VLogisEff>
     */
    public List<VLogisEff> searchVLogisEffs(SearchParam p) {
        return logisMapper.searchVLogisEff(p);
    }

    /**
     * 차량 Master Parameter 검증
     * 
     * @param reqParam
     * @param bindingResult
     */
    public void validateForVehicle(@Valid List<ReqVehicle> reqParam, BindingResult bindingResult) {

        int index = 0;
        for (ReqVehicle v : reqParam) {
            if (v.getsErpId() != null && v.getsErpId().length() > 0 && !"999999".equals(v.getsErpId())) {

                Long idErpDept = siteDeptMapper.findErpDeptKeyById(v.getsErpId());
                if (idErpDept == null) {
                    FieldErrorEx e = new FieldErrorEx(v.getsVrn(), index, "ReqLogisTransInfoList", "sErpId",
                            v.getsErpId() + "는 미등록 부서입니다.");
                    bindingResult.addError(e);
                } else
                    v.setIdErpDept(idErpDept);
            } else
                v.setIdErpDept(0l);

            if (v.getsTransComId() != null) {
                TTransCom tc = logisMapper.findTransComById(v.getsTransComId());
                if (tc == null) {
                    FieldErrorEx e = new FieldErrorEx(v.getsVrn(), index, "ReqLogisTransInfoList", "sTransComId",
                            v.getsTransComId() + " 운수사정보가 없습니다.");
                    bindingResult.addError(e);
                } else if ("Y".equals(tc.getcDelYn())) {
                    FieldErrorEx e = new FieldErrorEx(v.getsVrn(), index, "ReqLogisTransInfoList", "sTransComId",
                            v.getsTransComId() + " 삭제된 운수사 입니다.");
                    bindingResult.addError(e);
                } else
                    v.setIdTransCom(tc.getIdTransCom());
            }
        }
    }

    /**
     * 운송 정보의 Parameter 검증
     * 
     * @param reqParam
     * @param bindingResult
     */
    public void validateForTransInfo(@Valid List<ReqLogisTransInfo> reqParam, BindingResult bindingResult) {
        int index = 0;
        for (ReqLogisTransInfo l : reqParam) {

            String id = l.getsVhclDispld();

            // 미등록 부서 체크
            if (l.getsErpId() != null && l.getsErpId().length() > 0 && !"999999".equals(l.getsErpId())) {

                Long idErpDept = findErpDeptIdByErpId(l.getsErpId());
                if (idErpDept == null) {
                    FieldErrorEx e = new FieldErrorEx(id, index, "ReqLogisTransInfoList", "sErpId",
                            l.getsErpId() + "는 미등록 부서입니다.");
                    bindingResult.addError(e);
                } else
                    l.setIdErpDept(idErpDept);
            } else
                l.setIdErpDept(null);

            if (l.getIdTransCom() == null && l.getsTransCompId() != null) {
                TTransCom tc = logisMapper.findTransComById(l.getsTransCompId());
                if (tc != null)
                    l.setIdTransCom(tc.getIdTransCom());
            }

            if ("1".equals(l.getsTransTypeCd())) // 화물차 인경우
            {

                if (l.getsVrn() == null) {
                    FieldErrorEx e = new FieldErrorEx(id, index, "ReqLogisTransInfoList", "sVrn",
                            "차량번호(sVrn) 는 필수 입력 값입니다.");
                    bindingResult.addError(e);
                } else {

                    TVehicle v = findVehicleByVrn(l.getsVrn());
                    if (v == null) {
                        String errorMessge = l.getsVrn() + " 는 등록되지 차량입니다. ";
                        FieldErrorEx e = new FieldErrorEx(id, index, "ReqLogisTransInfoList", "sVrn", errorMessge);
                        bindingResult.addError(e);
                    } else {
                        l.setIdVehicle(v.getIdVehicle());
                        l.setIdTransCom(v.getIdTransCom());

                        if (v.getsFuelEff() != null)
                            l.setnFuelEff(new BigDecimal(v.getsFuelEff()));

                        if (l.getsSelfvrnCd() == null) // T_VEHICLE 테이블의 값을 참조한다.
                            l.setsSelfvrnCd(v.getsSelfvrnCd());
                    }
                }

                if (l.getsSelfvrnCd() == null) {
                    FieldErrorEx e = new FieldErrorEx(id, index, "ReqLogisTransInfoList", "sSelfvrnCd",
                            "자차여부(sSelfvrnCd) 는 필수 입력 값입니다.");

                    bindingResult.addError(e);
                }

                if (l.getsTransCompId() == null) {
                    FieldErrorEx e = new FieldErrorEx(id, index, "ReqLogisTransInfoList", "sTransCompId",
                            "운수회사(sTransCompId) 는 필수 입력 값입니다.");
                    bindingResult.addError(e);
                } else {
                    TTransCom tt = findTransComById(l.getsTransCompId());
                    if (tt == null) {
                        FieldErrorEx e = new FieldErrorEx(id, index, "ReqLogisTransInfoList", "sTransCompId",
                                l.getsTransCompId() + " 는 미 등록 운수사입니다.");
                        bindingResult.addError(e);
                    } else {
                        if ("Y".equals(tt.getcDelYn())) {
                            FieldErrorEx e = new FieldErrorEx(id, index, "ReqLogisTransInfoList", "sTransCompId",
                                    l.getsTransCompId() + " 는 삭제된 운수사입니다.");
                            bindingResult.addError(e);

                        } else
                            l.setIdTransCom(tt.getIdTransCom());
                    }
                }

                if (l.getsLogiWeiCd() == null) {
                    FieldErrorEx e = new FieldErrorEx(id, index, "ReqLogisTransInfoList", "sLogiWeiCd",
                            "톤수(sLogiWeiCd) 는 필수 입력 값입니다.");
                    bindingResult.addError(e);
                }

                // 부서가 배정되어 있는 경우 배출시설 등록
                if (!("999999").equals(l.getsErpId()) && l.getIdErpDept() != null) {
                    TEngPoint p = logisMapper.findEngPointForVehicle(l.getIdErpDept(), l.getsVrn());
                    if (p != null) {
                        l.setIdEngPoint(p.getIdEngPoint());
                    } else {
                        logger.error("배차id:" + l.getsVhclDispld() + " : 배출시설이 등록 되어 있지 않습니다 ");
                        // throw new BaseException("배차id:" + t.getsVhclDispld()+ " : 배출시설이 등록 되어 있지 않습니다
                        // ");
                    }
                }

            }

            // 선박
            if ("2".equals(l.getsTransTypeCd())) {

                TEngPoint p = logisMapper.findEngPointByInvCd(LogisServiceImpl.S_INV_TRANS_CD_SHIP); // 선박
                if (p == null) {
                    // throw new BaseException("배차id:" + t.getsVhclDispld()+ " : 배출시설이 등록 되어 있지 않습니다
                    // ");
                    logger.error("배차id:" + l.getsVhclDispld() + " : 배출시설이 등록 되어 있지 않습니다 ");
                } else
                    l.setIdEngPoint(p.getIdEngPoint());

                if (l.getnFuelEff() == null)
                    l.setnFuelEff(new BigDecimal(p.getsFuelEff()));

                if (l.getsFare() == null) {
                    FieldErrorEx e = new FieldErrorEx(id, index, "ReqLogisTransInfoList", "sFare",
                            "운임료(sFare) 는 필수 입력 값입니다.");
                    bindingResult.addError(e);
                }

            }

            // 철도
            if ("3".equals(l.getsTransTypeCd())) {
                if (l.getsFare() == null) {
                    FieldErrorEx e = new FieldErrorEx(id, index, "ReqLogisTransInfoList", "sFare",
                            "운임료(sFare) 는 필수 입력 값입니다.");
                    bindingResult.addError(e);
                }

                if (l.getsDistance() == null) {
                    FieldErrorEx e = new FieldErrorEx(id, index, "ReqLogisTransInfoList", "sDistance",
                            "영차운행거리(sDistance) 는 필수 입력 값입니다.");
                    bindingResult.addError(e);
                }

                TEngPoint p = logisMapper.findEngPointForTrain(l.getIdErpDept()); // 철도
                if (p == null) {
                    // throw new BaseException("배차id:" + t.getsVhclDispld()+ " : 배출시설이 등록 되어 있지 않습니다
                    // ");
                    logger.error("배차id:" + l.getsVhclDispld() + " : 배출시설이 등록 되어 있지 않습니다 ");
                } else
                    l.setIdEngPoint(p.getIdEngPoint());

                if (l.getnFuelEff() == null)
                    l.setnFuelEff(new BigDecimal(p.getsFuelEff()));
            }
            index++;
        }

    }

}