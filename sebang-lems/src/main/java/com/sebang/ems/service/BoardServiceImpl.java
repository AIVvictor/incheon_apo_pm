package com.sebang.ems.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sebang.ems.dao.BoardMapper;
import com.sebang.ems.dao.gen.VBoardMapper;
import com.sebang.ems.model.Do.TBoard;
import com.sebang.ems.model.Do.VBoard;
import com.sebang.ems.model.Do.VBoardWithBLOBs;

@Service
public class BoardServiceImpl implements BoardService {
	/**
	 * 공지사항
	 */
	static public String constNotice = "1"; // 공지사항 
	/**
	 * Q&A 
	 */
	static public String constQA = "2"; // Q&A 
	/**
	 * 자료실
	 */
	static public String constMaterial = "3"; // 자료실 
	/**
	 * 용어사전
	 */
	static public String constScrap= "4"; // 스크랩 
	
	@Autowired
	VBoardMapper vBoardMapper;

	/**
	 * T_BOARD mapper
	 */
	@Autowired
	BoardMapper boardMapper;

	
	List<VBoardWithBLOBs> find(String subject,String boardType) {
		VBoard key = new VBoard();
		List<VBoardWithBLOBs> ret;
		if(subject==null) {
			key.setsTitle(null);
		}else {
			key.setsTitle(subject);
		}
		key.setsBoardType(boardType);
		ret = vBoardMapper.select(key);
		return ret;
    }

	
	/**
	 * 공지사항 조회
	 * @param subject 검색keyword. 제목. null 이면 전체 조회. not null 이면 like 검색
	 * @return
	 */
	public List<VBoardWithBLOBs> findNotice(String subject) {
		return find(subject, constNotice);
    }

	
	public VBoardWithBLOBs findOneByBoardId(String boardId) {
		VBoardWithBLOBs one = null;
		VBoard key = new VBoard();
		if(boardId==null) {
			return null;
		}else {
			one = vBoardMapper.selectDetailOneByBoardId(boardId);
		}
		return one;
    }
	
	public int deleteOneByBoardId(String boardId) {
		if(boardId==null||boardId.length()==0) {
			return 0;
		}
		return boardMapper.deleteDetailOneByBoardId(boardId);
    }
	/**
	 * 고지사항 등록
	 * @param notice
	 * @return
	 */
	public Long insert(TBoard notice) {

		return boardMapper.insertSelective(notice);
		
	}
	
	/**
	 * 공지사항 수정. idBoard 필드 입력 필수.
	 * @param notice
	 * @return
	 */
	public int update(TBoard obj) {
		return boardMapper.updateByPrimaryKeySelective(obj);
	}

	/**
	 * Q&A 검색
	 * @param subject
	 * @return
	 */
	public List<VBoardWithBLOBs> findQA(String subject) {
		return find(subject,this.constQA);
    }
	
	/**
	 * 자료실 검색
	 * @param subject
	 * @return
	 */
	public List<VBoardWithBLOBs> findMaterial(String subject) {
		return find(subject,this.constMaterial);
    }
	
	/**
	 * 용어사전 검색
	 * @param subject
	 * @return
	 */
	public List<VBoardWithBLOBs> findTerms(String subject) {
		return find(subject,this.constScrap);
    }
	
}
