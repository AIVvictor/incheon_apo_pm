package com.sebang.ems.service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

import com.sebang.ems.dao.CefMapper;
import com.sebang.ems.dao.CodeMapper;
import com.sebang.ems.dao.LogisMapper;
import com.sebang.ems.dao.MenuMapper;
import com.sebang.ems.dao.SiteDeptMapper;
import com.sebang.ems.dao.TransComMapper;
import com.sebang.ems.model.KeyValue;
import com.sebang.ems.model.Menu;
import com.sebang.ems.model.SiteDept;
import com.sebang.ems.model.Do.TAuthMenu;
import com.sebang.ems.model.Do.TCat;
import com.sebang.ems.model.Do.TCode;
import com.sebang.ems.model.Do.TMenuInfo;
import com.sebang.ems.model.Do.TTransCom;
import com.sebang.ems.model.Do.VEngCef;
import com.sebang.ems.model.Dto.ReqMenuAuthMap;
import com.sebang.ems.util.StringUtil;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CodeServiceImpl implements CodeService {

    @Autowired
    CodeMapper codeMapper;

    @Autowired
    SiteDeptMapper siteDeptMapper;

    @Autowired
    LogisMapper logisMapper;

    @Autowired
    MenuMapper menuMapper;
    
    @Autowired
    CefMapper cefMapper;
    
    @Autowired
    TransComMapper transComMapper;

    
    /** 카테고리 조회
     * @param sCat : String 카테고리
     * @return List<TCat>
     */
    public List<TCat> findCat(String sCat) {
        return codeMapper.findCat(sCat);
    }

    
    /** 카테고리 내에 코드 조회
     * @param sCat : String 카테고리
     * @return List<KeyValue>
     */
    public List<KeyValue> toList(String sCat) {

        List<KeyValue> item = new ArrayList<KeyValue>();
        List<TCode> codes = codeMapper.find(sCat);
        codes.stream().forEach(i -> {
            // item.put(i.getsCd(), i.getsDesc());
            KeyValue co = new KeyValue(i.getsCd(), i.getsDesc());
            item.add(co);
        });

        return item;

    }

    
    /** 모든 사업장 조회
     * @return List<KeyValue>
     */
    public List<KeyValue> findSiteAll() {

        List<KeyValue> item = new ArrayList<KeyValue>();
        List<SiteDept> sites = siteDeptMapper.findSite(null);
        sites.stream().forEach(i -> {
            // item.put(i.getsCd(), i.getsDesc());
            KeyValue co = new KeyValue(i.getIdSite().toString(), i.getsSiteNm());
            item.add(co);
        });
        return item;
    }
   
    /** 
     * @return List<KeyValue>
     */
    public List<KeyValue> findYongchaType() {

        List<KeyValue> item = new ArrayList<KeyValue>();

        item.add(new KeyValue("1", "정기"));
        item.add(new KeyValue("2", "부정기"));

        return item;
    }

    /**
     * 에너지원 리스트: 배출계수에 등록한 에너지원을 가져와서 선택함
     * @return
     */
    public List<KeyValue> findEngAll() {

        List<KeyValue> item = new ArrayList<KeyValue>();
        
        List<VEngCef> codes = cefMapper.searchEngCefDropdownList();
        codes.stream().forEach(i -> {
        	String idEngCef = Long.toString(i.getIdEngCef());
            KeyValue co = new KeyValue(idEngCef, i.getsEngCdNm());
            item.add(co);
        });
        return item;
    }

    /**
     * 운수회사 리스트: T_TRANS_COM 내 운수회사 정보를 가져와서 선택함
     * @return
     */
    public List<KeyValue> findTransComAll() {

        List<KeyValue> item = new ArrayList<KeyValue>();
        
        List<TTransCom> codes = transComMapper.findAll();
        codes.stream().forEach(i -> {
//        	String sTransComId = i.getsTransComId();
        	String sTransComId = i.getIdTransCom().toString();
            KeyValue co = new KeyValue(sTransComId, i.getsTransComNm());
            item.add(co);
        });
        return item;
    }
    

    
    /** 카테고리 등록 수정
     * @param cat
     */
    public void upsertCat(TCat cat) {
        codeMapper.upsertCat(cat);
    }

    
    /** 카테고리 삭제
     * @param idCat
     * @param sModId
     */
    public void deleteCat(Long idCat, String sModId) {
        TCat cat = new TCat();
        cat.setIdCat(idCat);
        cat.setcDelYn("Y");
        cat.setsModId(sModId);
        codeMapper.upsertCat(cat);
    }

    
    /** 카테고리 조회
     * @param sCat
     * @return List<TCode>
     */
    public List<TCode> find(String sCat) {
        return codeMapper.find(sCat);
    }
    
    /** 삭제여부 Y를 포함한 코드 조회
     * @param sCat
     * @return List<TCode>
     */
    public List<TCode> findIncludingDelY(String sCat) {
        return codeMapper.findIncludingDelY(sCat);
    }
    
    /** 코드 등록 수정
     * @param code
     */
    public void upsertCode(TCode code) {
        codeMapper.upsertCode(code);
    }
    
    /** 코드 삭제
     * @param idCode
     * @param sModId
     */
    public void deleteCode(Long idCode, String sModId) {
        TCode code = new TCode();
        code.setIdCode(idCode);
        code.setcDelYn("Y");
        code.setsModId(sModId);
        codeMapper.upsertCode(code);
    }

    
    /** Key list 조회
     * @param sUiId : String 화면 ID
     * @return HashMap<String, List<KeyValue>>
     */
    public HashMap<String, List<KeyValue>> buildKeylist(String sUiId) {
        HashMap<String, List<KeyValue>> keyList = new HashMap<String, List<KeyValue>>();
        
        // 전체 실적 조회
        if ("111".equals(sUiId)) {
            List<KeyValue> scu = toList("SCU");
            keyList.put("SCU", scu);
            
            List<KeyValue> yyyy = toList("YYYY");
            keyList.put("YYYY", yyyy);
        }
        
        // 연도별 조회
        if ("121".equals(sUiId)) {
        	// 조회년도
            List<KeyValue> yyyy = toList("YYYY");
            keyList.put("YYYY", yyyy);
        	
            // 조회구분
            List<KeyValue> logiType = toList("LOGI_TYPE");
            keyList.put("LOGI_TYPE", logiType);
            
            // 조회단위
            List<KeyValue> scu = toList("SCU");
            keyList.put("SCU", scu);
        }
        
        // 물류원단위 조회
        if ("131".equals(sUiId)) {
            //조회년도
        	List<KeyValue> yyyy = toList("YYYY");
            keyList.put("YYYY", yyyy);

            // 노선구분
//            List<KeyValue> logiYct = toList("LOGI_YCT");
//            keyList.put("LOGI_YCT", logiYct);

            // 자차여부
            List<KeyValue> selfVrn = toList("SELFVRN");
            keyList.put("SELFVRN", selfVrn);
            
            // 조회단위
            List<KeyValue> scu = toList("SCU");
            keyList.put("SCU", scu);
            
            // 원단위 기준
            List<KeyValue> baseUnt = toList("BASE_UNT_131");
            keyList.put("BASE_UNT_131", baseUnt);
        }
        
        // BAU분석
        if ("211".equals(sUiId)) {
            List<KeyValue> scu = toList("SCU");
            keyList.put("SCU", scu);
            
            List<KeyValue> yyyy = toList("YYYY");
            keyList.put("YYYY", yyyy);
            
            List<KeyValue> baseUnt = toList("BASE_UNT");
            keyList.put("BASE_UNT", baseUnt);
        }
        
        // 정부목표
        if ("212".equals(sUiId)) {
            List<KeyValue> yyyy = toList("YYYY");
            keyList.put("YYYY", yyyy);
        }

        if ("213".equals(sUiId)) {
            List<KeyValue> yyyy = toList("YYYY");
            keyList.put("YYYY", yyyy);

            List<KeyValue> sites = findSiteAll();
            keyList.put("SITE", sites);
        }

        // 월마감
        if ("315".equals(sUiId)) {
            List<KeyValue> yyyy = toList("YYYY");
            keyList.put("YYYY", yyyy);

            List<KeyValue> mm = toList("MM");
            keyList.put("MM", mm);

            List<KeyValue> sites = findSiteAll();
            keyList.put("SITE", sites);
        }

        // 사용실적
        if ("316".equals(sUiId)) {
            List<KeyValue> sites = findSiteAll();
            keyList.put("SITE", sites);
        	
            List<KeyValue> yyyy = toList("YYYY");
            keyList.put("YYYY", yyyy);

            List<KeyValue> mm = toList("MM");
            keyList.put("MM", mm);

            List<KeyValue> emt = toList("EMT");
            keyList.put("EMT", emt);
            
            List<KeyValue> baseUnt = toList("BASE_UNT");
            keyList.put("BASE_UNT", baseUnt);
        }

        // 연도별 분석
        if ("321".equals(sUiId)) {
            List<KeyValue> yyyy = toList("YYYY");
            keyList.put("YYYY", yyyy);

            List<KeyValue> scu = toList("SCU");
            keyList.put("SCU", scu);

            List<KeyValue> sites = findSiteAll();
            keyList.put("SITE", sites);
        }

        // 월별 분석
        if ("322".equals(sUiId)) {
            List<KeyValue> yyyy = toList("YYYY");
            keyList.put("YYYY", yyyy);

            List<KeyValue> mm = toList("MM");
            keyList.put("MM", mm);

            List<KeyValue> scu = toList("SCU");
            keyList.put("SCU", scu);

            List<KeyValue> sites = findSiteAll();
            keyList.put("SITE", sites);
        }

        // 목표대비 실적분석
        if ("324".equals(sUiId)) {
            List<KeyValue> yyyy = toList("YYYY");
            keyList.put("YYYY", yyyy);

            List<KeyValue> scu = toList("SCU");
            keyList.put("SCU", scu);

            List<KeyValue> eng = toList("ENG");
            keyList.put("ENG", eng);

            List<KeyValue> sites = findSiteAll();
            keyList.put("SITE", sites);
        }

        // 원단윈분석
        if ("325".equals(sUiId)) {
            List<KeyValue> yyyy = toList("YYYY");
            keyList.put("YYYY", yyyy);

            List<KeyValue> scu = toList("SCU");
            keyList.put("SCU", scu);

            List<KeyValue> sites = findSiteAll();
            keyList.put("SITE", sites);
            
            List<KeyValue> baseUnt = toList("BASE_UNT"); // 기준 단위
            keyList.put("BASE_UNT", baseUnt);
        }

        // 운송정보
        if ("411".equals(sUiId)) {
            List<KeyValue> yyyy = toList("YYYY");
            keyList.put("YYYY", yyyy);

            List<KeyValue> mm = toList("MM");
            keyList.put("MM", mm);
            
            // 노선구분
//            List<KeyValue> logiYct = toList("LOGI_YCT");
//            keyList.put("LOGI_YCT", logiYct);
            
            // 자차여부
            List<KeyValue> selfVrn = toList("SELFVRN");
            keyList.put("SELFVRN", selfVrn);
            
            // 사업장
            List<KeyValue> sites = findSiteAll();
            keyList.put("SITE", sites);
        }

        // 용차 운행정보 등록
        if ("431".equals(sUiId) || "4311".equals(sUiId)) {
            List<KeyValue> yyyy = toList("YYYY");
            keyList.put("YYYY", yyyy);

            List<KeyValue> mm = toList("MM");
            keyList.put("MM", mm);

            List<KeyValue> drvType = findYongchaType();
            keyList.put("DRV_TYPE", drvType);

            // // 전환사업명
            // List<KeyValue> LogiPrj = findLogisPrjAll();
            // keyList.put("LOGI_PRJ", LogiPrj);

            // // 전환사업유형
            // List<KeyValue> logiPty = toList("LOGI_PTY");
            // keyList.put("LOGI_PTY", logiPty);

            // // 전환사업구분
            // List<KeyValue> logiMea = toList("LOGI_MEA");
            // keyList.put("LOGI_MEA", logiMea);

            // // 전환사업 대상
            // List<KeyValue> logTar = toList("LOGI_TAR");
            // keyList.put("LOGI_TAR", logTar);

            // // 전환사업 대상
            // List<KeyValue> logUnt = toList("LOGI_UNT");
            // keyList.put("LOGI_UNT", logUnt);

        }

        // // 물류전환사업 결과조회
        // if ("432".equals(sUiId)) {
        //     // 전환사업명
        //     List<KeyValue> LogiPrj = findLogisPrjAll();
        //     keyList.put("LOGI_PRJ", LogiPrj);

        //     // 조회구분
        //     List<KeyValue> acu = toList("ACU");
        //     keyList.put("ACU", acu);

        //     // 조회단위
        //     List<KeyValue> gaseng = toList("GASENG");
        //     keyList.put("GASENG", gaseng);
        // }
        
        // 온실가스 에너지 현황
        if ("451".equals(sUiId)) {
            List<KeyValue> sites = findSiteAll();
            keyList.put("SITE", sites);
            
            List<KeyValue> carType = toList("CAR_TYPE");
            keyList.put("CAR_TYPE", carType);
            
            List<KeyValue> qryObjType = toList("QRY_OBJ_TYPE");
            keyList.put("QRY_OBJ_TYPE", qryObjType);
            
//            List<KeyValue> logiYct = toList("LOGI_YCT");
//            keyList.put("LOGI_YCT", logiYct);
            
            // 자차여부
            List<KeyValue> selfVrn = toList("SELFVRN");
            keyList.put("SELFVRN", selfVrn);
            
            List<KeyValue> logiWei = toList("LOGI_WEI");
            keyList.put("LOGI_WEI", logiWei);
            
            List<KeyValue> scu = toList("SCU");
            keyList.put("SCU", scu);
            
            List<KeyValue> yyyy = toList("YYYY");
            keyList.put("YYYY", yyyy);
        }
        
        // 효율화 지표
        if ("452".equals(sUiId)) {
        	// 차량구분
            List<KeyValue> carType = toList("CAR_TYPE");
            keyList.put("CAR_TYPE", carType);
        	
            // 사업장
            List<KeyValue> sites = findSiteAll();
            keyList.put("SITE", sites);
            
            // 차종(톤급)
            List<KeyValue> logiWei = toList("LOGI_WEI");
            keyList.put("LOGI_WEI", logiWei);
            
            // 노선구분
//            List<KeyValue> logiYct = toList("LOGI_YCT");
//            keyList.put("LOGI_YCT", logiYct);       
            
            // 자차여부
            List<KeyValue> selfVrn = toList("SELFVRN");
            keyList.put("SELFVRN", selfVrn);
            
            
            // 조회단위
            List<KeyValue> qryMq = toList("QRY_MQ");
            keyList.put("QRY_MQ", qryMq); 
            
            // 조회년도
            List<KeyValue> yyyy = toList("YYYY");
            keyList.put("YYYY", yyyy);
            
            // 조회구분
            List<KeyValue> qryObjType = toList("QRY_OBJ_TYPE");
            keyList.put("QRY_OBJ_TYPE", qryObjType);
        }


        // 사내목표 대비실적
        if ("4531".equals(sUiId) || "4532".equals(sUiId)) {
            // 전환사업명
            List<KeyValue> yyyy = toList("YYYY");
            keyList.put("YYYY", yyyy);

            List<KeyValue> acu = toList("ACU");
            keyList.put("ACU", acu);

        }
        
        // 차량정보
        if ("461".equals(sUiId)) {
            List<KeyValue> site = findSiteAll();
            keyList.put("SITE", site);
            
            List<KeyValue> logiWei = toList("LOGI_WEI");
            keyList.put("LOGI_WEI", logiWei);
            
//            List<KeyValue> logiYct = toList("LOGI_YCT");
//            keyList.put("LOGI_YCT", logiYct);
            
            // 자차여부
            List<KeyValue> selfVrn = toList("SELFVRN");
            keyList.put("SELFVRN", selfVrn);
            
            List<KeyValue> transCom = findTransComAll();
            keyList.put("TRANS_COM", transCom);
        }

        // 사업장관리
        if ("521".equals(sUiId)) {
            List<KeyValue> siteParty = toList("SITE_PARTY");
            keyList.put("SITE_PARTY", siteParty);
            
            List<KeyValue> yyyy = toList("YYYY");
            keyList.put("YYYY", yyyy);
        }
        
        // 조직관리
        if ("531".equals(sUiId)) {
            List<KeyValue> site = findSiteAll();
            keyList.put("SITE", site);
        }
        
        // 사용자권한관리
        if ("591".equals(sUiId)) {
            List<KeyValue> site = findSiteAll();
            keyList.put("SITE", site);
            
            List<KeyValue> empStat = toList("EMP_STAT");
            keyList.put("EMP_STAT", empStat);
        }
        
        // 직접배출
        if ("551".equals(sUiId)) {
            List<KeyValue> eng = toList("ENG");
            keyList.put("ENG", eng);
            
            List<KeyValue> mrveng = toList("MRVENG");
            keyList.put("MRVENG", mrveng);
            
            List<KeyValue> unt = toList("UNT");
            keyList.put("UNT", unt);
            
            List<KeyValue> emi = toList("EMI");
            keyList.put("EMI", emi);
            
            List<KeyValue> yyyy = toList("YYYY");
            keyList.put("YYYY", yyyy);
        }
        
        // 간접배출
        if ("552".equals(sUiId)) {
            List<KeyValue> eng = toList("ENG");
            keyList.put("ENG", eng);
            
            List<KeyValue> mrveng = toList("MRVENG");
            keyList.put("MRVENG", mrveng);
            
            List<KeyValue> unt = toList("UNT");
            keyList.put("UNT", unt);
            
            List<KeyValue> emi = toList("EMI");
            keyList.put("EMI", emi);
            
            List<KeyValue> yyyy = toList("YYYY");
            keyList.put("YYYY", yyyy);
        }
        
        // 기타배출
        if ("553".equals(sUiId)) {
            List<KeyValue> unt = toList("UNT");
            keyList.put("UNT", unt);
        	
            List<KeyValue> ghg = toList("GHG");
            keyList.put("GHG", ghg);
        }
        
        // 배출원관리
        if ("571".equals(sUiId)) {
            List<KeyValue> site = findSiteAll(); // 사업장
            keyList.put("SITE", site);
            
            List<KeyValue> inv = toList("INV"); // 배출시설구분
            keyList.put("INV", inv);
            
            List<KeyValue> unt = toList("UNT"); // 시설용량단위, 세부시설용량단위
            keyList.put("UNT", unt);
            
            List<KeyValue> yyyy = toList("YYYY"); // 시작년도, 종료년도
            keyList.put("YYYY", yyyy);
            
            List<KeyValue> baseUnt = toList("BASE_UNT"); // 기준 단위
            keyList.put("BASE_UNT", baseUnt);
            
            List<KeyValue> eng = findEngAll(); // 에너지원
            keyList.put("ENG", eng);
            
            List<KeyValue> logiType = toList("LOGI_TYPE"); // 물류구분
            keyList.put("LOGI_TYPE", logiType);
        }

        // 태양광관리
        if ("581".equals(sUiId)) {
            List<KeyValue> yyyy = toList("YYYY");
            keyList.put("YYYY", yyyy);

            List<KeyValue> mm = toList("MM");
            keyList.put("MM", mm);
        }
        
        // 태양광관리
        if ("591".equals(sUiId)) {
            List<KeyValue> auth = toList("AUTH");
            keyList.put("AUTH", auth);
        }

        return keyList;
    }

    
    
    /** 상위 메뉴 조회
     * @param allMenu
     * @param menu
     * @return TMenuInfo
     */
    public TMenuInfo upperMenu(List<TMenuInfo> allMenu, TMenuInfo menu)
    {
        if(menu == null)
            return null;
            
        if(StringUtil.isEmpty(menu.getsUMenuId()))
            return null;

        for( TMenuInfo m : allMenu)
        {
            if(menu.getsUMenuId().equals(m.getsMenuId()))
                return m;
        }
        return null;
    }

    
    /** 메뉴 조회
     * @param allMenu : List 모든 메뉴
     * @param sMenuId : 메뉴 Id
     * @return TMenuInfo
     */
    public TMenuInfo findMenu(List<TMenuInfo> allMenu, String sMenuId)
    {
        if(StringUtil.isEmpty(sMenuId))
            return null;

        for( TMenuInfo m : allMenu)
        {
            if(sMenuId.equals(m.getsMenuId()))
                return m;
        }
        return null;
    }

    /** 메뉴의 Full Name 생성
     * @param allMenu : List  전체 메뉴 리스트
     * @param sMenuId : String 메뉴 ID
     * @return String
     */
    public String buildFullMenuName(List<TMenuInfo> allMenu, String sMenuId)
    {   
       // String fullMenu = "";
   //     String menuid = sMenuId;

        List<TMenuInfo> menu = new ArrayList<TMenuInfo>();

        if(StringUtil.isEmpty(sMenuId))
            return sMenuId;

        TMenuInfo curPos = findMenu(allMenu, sMenuId);
        if(curPos == null)
            return sMenuId;

        menu.add(curPos);

        TMenuInfo upper=null;
        do{
            upper = upperMenu(allMenu, curPos);
            if(upper != null){
                menu.add(upper);
                curPos = upper;
            }else
                break;            
        }while (upper != null);
        
        Collections.reverse(menu);

        StringBuilder sb =  new StringBuilder();
        for(TMenuInfo m : menu)
        {
             sb.append(m.getsMenuNm());
             if(!sMenuId.equals(m.getsMenuId()))
                sb.append('>');
        }

        return sb.toString();
    }
    
    
    
    /** 권한 별 메뉴 MST 목록 조회
     * @param sAuthCd : String 권한 코드
     * @return List<TMenuInfo>
     */
    public List<TMenuInfo> findMenuMst(String sAuthCd)
    {
        return menuMapper.findMst(sAuthCd);
    }
    
    /** 권한 별 메뉴 목록 조회
     * @param sAuthCd : String 권한 코드
     * @return List<TMenuInfo>
     */
    public List<Menu> findMenu(String sAuthCd)
    {
        return menuMapper.find(sAuthCd);
    }

    // /** 권한 등록 수정
    //  * @param authMenuKey
    //  */
    // public void upsertAuthMenu(TAuthMenu authMenuKey)
    // {
    //     TAuthMenu re= menuMapper.findMenuAuth(authMenuKey.getsAuthCd(),authMenuKey.getIdMenuInfo());
    //     if(re != null)
    //     {
    //         authMenuKey.setIdAuthMenu(re.getIdAuthMenu());
    //     }
    //     menuMapper.upsertAuthMenu(authMenuKey);
    // }

    
    /** 권한 등록 수정 리스트
     * @param list
     */
    public void upsertAuthMenu(List<ReqMenuAuthMap> list )
    {
        for(ReqMenuAuthMap authMenu : list)
        {
            if(authMenu.getsAuthCd() == null || authMenu.getIdMenuInfo() == null )
                continue; 

            if(authMenu.getIdAuthMenu()!=null)
            {
                TAuthMenu ta =  menuMapper.findMenuAuth(authMenu.getIdAuthMenu());
                if(ta!=null)
                {
                    authMenu.setIdAuthMenu(ta.getIdAuthMenu());
                }   
            }
            menuMapper.upsertAuthMenu(authMenu.toEntity());
        }
    }

    
    /** 권한 별 메뉴 목록 삭제
     * @param authMenuKey
     */
    public void deleteMenuAuthMap(TAuthMenu authMenuKey)
    {
        menuMapper.deleteAuthMenu(authMenuKey);
    }
}