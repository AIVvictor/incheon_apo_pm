package com.sebang.ems.service;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

import com.sebang.ems.dao.AmtMapper;
import com.sebang.ems.dao.GoalMapper;
import com.sebang.ems.domain.Exception.BaseException;
import com.sebang.ems.domain.common.SearchParam;
import com.sebang.ems.model.EngPointAmt;
import com.sebang.ems.model.SiteMonGoal;
import com.sebang.ems.model.SiteYearGoal;
import com.sebang.ems.model.SiteYearGoal4Home;
import com.sebang.ems.model.Do.TMrvPointGoal;
import com.sebang.ems.model.Do.TSiteMonGoal;
import com.sebang.ems.model.Do.TSiteMonSumGoal;
import com.sebang.ems.model.Do.TSiteYearGoal;
import com.sebang.ems.model.Do.VBau;
import com.sebang.ems.model.Do.VCompanyGoal;
import com.sebang.ems.model.Do.VGoalResultAnalyAcc;
import com.sebang.ems.model.Do.VMrvGoal;
import com.sebang.ems.model.Do.VSiteGoal;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.TransactionDefinition;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.DefaultTransactionDefinition;

@Service
public class GoalServiceImpl implements GoalService {

    public static final Logger logger = LoggerFactory.getLogger(GoalServiceImpl.class);

    @Autowired
    GoalMapper goalMapper;

    @Autowired
    AmtMapper amtMapper;

    @Autowired
    private PlatformTransactionManager transactionManager;

    DefaultTransactionDefinition def = null;
    TransactionStatus status = null;

    /**
     * 전사 목표 목록 저장
     * 
     * @param g
     */
    public void upsertMrvPointGoal(TMrvPointGoal g) {
        if (g.getIdMrvPointGoal() == null) {
            TMrvPointGoal mg = goalMapper.findMrvGoal(g.getcYear());
            if (mg != null) {
                g.setIdMrvPointGoal(mg.getIdMrvPointGoal());
            }
        }
        goalMapper.upsertMrvPointGoal(g);
    }

    /**
     * 전사 목표 목록 삭제
     * 
     * @param g
     */
    public void clearMrvPointGoal(TMrvPointGoal g) {
        if (g.getIdMrvPointGoal() == null) {
            TMrvPointGoal mg = goalMapper.findMrvGoal(g.getcYear());
            if (mg != null) {
                g.setIdMrvPointGoal(mg.getIdMrvPointGoal());
            }
        }
        g.setgMrvFile(null);
        g.setnEngGoal(BigDecimal.ZERO);
        g.setnGasGoal(BigDecimal.ZERO);
        goalMapper.clearMvrPointGoal(g);
    }

    /**
     * 온실가스 목록 목표 저장
     * 
     * @param siteYearGoals
     */
    public void upsertSiteYearsGoal(List<SiteYearGoal> siteYearGoals) {

        def = new DefaultTransactionDefinition();
        def.setPropagationBehavior(TransactionDefinition.PROPAGATION_REQUIRED);
        status = transactionManager.getTransaction(def);

        try {

            for (SiteYearGoal g : siteYearGoals) {
                TSiteYearGoal tg = goalMapper.findSiteYearGoal(g.getcYear(), g.getIdSite());
                if (tg != null)
                    g.setIdSiteYearGoal(tg.getIdSiteYearGoal());

                goalMapper.upsertSiteYearGoal(g);

                Map<Long, List<SiteMonGoal>> pointMonGoalMap = g.getPointMonGoals();
                if (pointMonGoalMap == null || pointMonGoalMap.size() == 0)
                    continue;

                for (Long idEngPoint : pointMonGoalMap.keySet()) {

                    TSiteMonSumGoal monSumGoal = goalMapper.findSiteMonSumGoal(g.getIdSiteYearGoal(), idEngPoint);
                    if (monSumGoal == null) {
                        monSumGoal = new TSiteMonSumGoal();
                        monSumGoal.setIdEngPoint(idEngPoint);
                        monSumGoal.setIdSiteYearGoal(g.getIdSiteYearGoal());
                        monSumGoal.setnGoalSum(BigDecimal.ZERO);
                        goalMapper.upsertSiteMonSumGoal(monSumGoal);
                    }

                    Long idSiteMonSumGoal = monSumGoal.getIdSiteMonSumGoal();
                    if (idSiteMonSumGoal == null)
                        throw new BaseException("idSiteMonSumGoal is null");

                    List<SiteMonGoal> pointMonGoals = pointMonGoalMap.get(idEngPoint);

                    for (SiteMonGoal mg : pointMonGoals) {
                        TSiteMonGoal monGoal = goalMapper.findSiteMonGoal(idSiteMonSumGoal, mg.getcMon());
                        if (monGoal == null) {
                            monGoal = new TSiteMonGoal();
                            monGoal.setIdSiteMonSumGoal(idSiteMonSumGoal);
                            monGoal.setcMon(mg.getcMon());
                        }

                        monGoal.setnMonEngGoal(mg.getnMonEngGoal());
                        EngPointAmt amt= amtMapper.findEngConversion(idEngPoint, mg.getnMonEngGoal());
                        if(amt!=null){
                            monGoal.setnEngGoal(amt.getnGj());
                            monGoal.setnGasGoal(amt.getnTco2());           
                        }                 

                        goalMapper.upsertSiteMonGoal(monGoal);
                    }
                    goalMapper.updateSiteMonSumGoal(idSiteMonSumGoal);
                }
                goalMapper.updateSiteYearGoal(g.getIdSiteYearGoal());
            }  
            transactionManager.commit(status);           
        } catch (Exception ex) {
            transactionManager.rollback(status);
            logger.error("upsertSiteYearsGoal error =>" + ex.getMessage());
            throw new BaseException("upsertSiteYearsGoal error =>" + ex.getMessage());
        }
             
    }

    /**
     * 전사 목표 조회
     * 
     * @param search
     * @return List<VMrvGoal>
     */
    public List<VMrvGoal> searchMrvGoal(SearchParam search) {
        return goalMapper.searchVMrvGoal(search);
    }

    /**
     * 전사목표 상세 조회
     * 
     * @param search
     * @return List<VSiteGoal>
     */
    public List<VSiteGoal> searchSiteGoal(SearchParam search) {
        return goalMapper.searchSiteGoal(search);
    }

    /**
     * 온실 가스 목표 목록조회
     * 
     * @param search
     * @return List<VCompanyGoal>
     */
    public List<VCompanyGoal> searchSiteMonGoal(SearchParam search) {
        return goalMapper.searchSiteMonGoal(search);
    }

    /**
     * 목표 대비 실적 조회
     * 
     * @param search
     * @return List<VGoalResultAnalyAcc>
     */
    public List<VGoalResultAnalyAcc> searchSiteMonGoalArchRate(SearchParam search) {
        return goalMapper.searchSiteMonGoalArchRate(search);
    }

    /**
     * BAU 분석
     * @param search
     * @return List<VBau>
     */
    public List<VBau> searchBau(SearchParam search) {
        return goalMapper.searchBau(search);
    }

    public List<SiteYearGoal4Home> searchSiteYearGoal4Home(SearchParam search)
    {
        List<SiteYearGoal4Home> list = goalMapper.searchSiteYearGoal4Home(search);
        for(SiteYearGoal4Home g :list)
        {
            // 정부목표 대비 에너지 실적 달성률
            if(g.getnYearEngGoal()!=null && 
               BigDecimal.ZERO.compareTo(g.getnYearEngGoal())!=0 && 
               g.getnGj()!=null)
            {
             
                BigDecimal nGjPerYearEngGoal=  g.getnGj().divide(g.getnYearEngGoal(),4,BigDecimal.ROUND_CEILING);
                g.setNGjPerYearEngGoal(nGjPerYearEngGoal.multiply(new BigDecimal(100)));
            }

            
            // 정부목표 대비 온실가스 실적 달성률
            if(g.getnYearGasGoal()!=null && 
               BigDecimal.ZERO.compareTo(g.getnYearGasGoal())!=0 && 
               g.getnTco2()!=null)
            {
                BigDecimal nTco2PerYearGasGoal=  g.getnTco2().divide(g.getnYearGasGoal(),4,BigDecimal.ROUND_CEILING);
                g.setNTco2PerYearGasGoal(nTco2PerYearGasGoal.multiply(new BigDecimal(100)));
            }

            // 사내목표 대비 에너지 실적 달성률
            if(g.getnEngGoal()!=null && 
               BigDecimal.ZERO.compareTo(g.getnEngGoal())!=0 && 
               g.getnGj()!=null)
            {
                BigDecimal nEngGoal=  g.getnGj().divide(g.getnEngGoal(),4,BigDecimal.ROUND_CEILING);
                g.setNGjPerEngGoal(nEngGoal.multiply(new BigDecimal(100)));
            }

            
            // 사내목표 대비 온실가스 실적 달성률
            if(g.getnGasGoal()!=null && 
               BigDecimal.ZERO.compareTo(g.getnGasGoal())!=0 && 
               g.getnTco2()!=null)
            {
                BigDecimal nGjPerGasGoal=  g.getnTco2().divide(g.getnGasGoal(),4,BigDecimal.ROUND_CEILING);
                g.setNTco2PerGasGoal(nGjPerGasGoal.multiply(new BigDecimal(100)));
            }
        }

        return list;
    }

}