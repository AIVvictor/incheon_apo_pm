package com.sebang.ems.service;

import java.util.List;

import com.sebang.ems.dao.UsersMapper;
import com.sebang.ems.domain.Exception.BaseException;
import com.sebang.ems.domain.common.SearchParam;
import com.sebang.ems.model.Employee;
import com.sebang.ems.model.Do.TUsers;
import com.sebang.ems.util.StringUtil;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.TransactionDefinition;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.DefaultTransactionDefinition;

@Service
public class EmployeeServiceImpl implements EmployeeService {

    public static final Logger logger = LoggerFactory.getLogger(EmployeeServiceImpl.class);

    @Autowired
    UsersMapper userMapper;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private PlatformTransactionManager transactionManager;

    DefaultTransactionDefinition def = null;
    TransactionStatus status = null;

    public Employee findEmployeeByEmpId(String sEmpId) {
        SearchParam p = new SearchParam();
        p.setSEmpId(sEmpId);
        List<Employee> r = userMapper.searchEmployee(p);
        if (r.size() == 0)
            return null;

        return r.get(0);
    }

    public List<Employee> searchEmployee(SearchParam p) {
        return userMapper.searchEmployee(p);
    }

    /**
     * 사번으로 사원정보 검색
     * 
     * @param sEmpId 사번
     * @return TUsers
     */
    public TUsers findUserByEmpId(String sEmpId) {
        return userMapper.findById(sEmpId);
    }

    public void upsertEmployee(Employee emp) {

        if (StringUtil.isEmpty(emp.getsEmpPwd())) {

            if (emp.getsEmpMail() == null)
                throw new BaseException("email is null");

            logger.debug("기본 암호로 등록");
            String email = emp.getsEmpMail();
            String[] array = email.split("@");

            emp.setsEmpPwd(array[0]);
        } else {
            logger.debug("사용자 입력 암호로 등록");
        }

        String sEmpPwd = emp.getsEmpPwd();

        if (!sEmpPwd.contains("{sha256}") && !sEmpPwd.contains("{bcrypt}") && !sEmpPwd.contains("{pbkdf2}")
                && !sEmpPwd.contains("{noop}")) {
            String encPassword = passwordEncoder.encode(sEmpPwd);
            emp.setsEmpPwd(encPassword);
        }

        if (emp.getsEmpNm() == null)
            emp.setsEmpNm(""); // NOT NULL contrains
        userMapper.upsertEmployee(emp);
        // upsertEmployee(emp);
    }

    public void upsertEmployee(List<Employee> reqParam) {

        def = new DefaultTransactionDefinition();
        def.setPropagationBehavior(TransactionDefinition.PROPAGATION_REQUIRED);
        status = transactionManager.getTransaction(def);
        try {
            for (Employee emp : reqParam) {
                // 사원번호로 중복 체크
                TUsers re = findUserByEmpId(emp.getsEmpId());
                if (re == null)
                    throw new BaseException(emp.getsEmpId() + "는 존재하지 않는 사원입니다. ");

                emp.setIdEmp(re.getIdEmp());

                if (emp.getcDelYn() == null)
                    emp.setcDelYn(re.getcDelYn());

                if (emp.getsEmpNm() == null)
                    emp.setsEmpNm(re.getsEmpNm());

                upsertEmployee(emp);
            }
            transactionManager.commit(status);
        } catch (Exception e) {
            transactionManager.rollback(status);
            logger.error("upsertEmployee ERROR " + e.getMessage());
            throw new BaseException(e.getMessage());
        }

    }

    public void deleteEmployee(String sEmpId) {
        Employee emp = new Employee();
        emp.setsEmpId(sEmpId);
        emp.setcDelYn("Y");
        userMapper.upsertEmployee(emp);
    }

    public void upsertEmployees(List<Employee> employees) {

        def = new DefaultTransactionDefinition();
        def.setPropagationBehavior(TransactionDefinition.PROPAGATION_REQUIRED);
        status = transactionManager.getTransaction(def);
        try {
            for (Employee item : employees) {
                // 사원번호로 중복 체크
                TUsers re = findUserByEmpId(item.getsEmpId());
                if (re != null) {
                    item.setIdEmp(re.getIdEmp());
                    item.setsEmpPwd(re.getsEmpPwd());

                    if (item.getcDelYn() == null)
                        item.setcDelYn(re.getcDelYn());
                } else {
                    // 최초 등록일 경우 암호
                    String email = item.getsEmpMail();
                    String[] array = email.split("@");
                    if (item.getsEmpPwd() == null) {
                        logger.info("기본 암호로 등록");
                        item.setsEmpPwd(array[0]);
                    } else {
                        logger.info("사용자 입력 암호로 등록");
                    }

                    String sEmpPwd = item.getsEmpPwd();
                    if (sEmpPwd != null) {
                        String encPassword = passwordEncoder.encode(sEmpPwd);
                        item.setsEmpPwd(encPassword);
                    }
                }
                userMapper.upsertEmployee(item);

            }
            transactionManager.commit(status);
        } catch (Exception e) {

            transactionManager.rollback(status);
            logger.error("upsertEmployees ERROR " + e.getMessage());
            throw new BaseException(e.getMessage());
        }

    }

}
