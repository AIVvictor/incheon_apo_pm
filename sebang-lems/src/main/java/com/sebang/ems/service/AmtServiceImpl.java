package com.sebang.ems.service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.sebang.ems.dao.AmtMapper;
import com.sebang.ems.dao.SiteDeptMapper;
import com.sebang.ems.domain.Exception.AlreadyMonthEndClosingException;
import com.sebang.ems.domain.Exception.BaseException;
import com.sebang.ems.domain.common.SearchParam;
import com.sebang.ems.model.EngPointAmt;
import com.sebang.ems.model.SumAmt;
import com.sebang.ems.model.SumAmt4Home;
import com.sebang.ems.model.Do.TEngPointAmt;
import com.sebang.ems.model.Do.TMonEngClose;
import com.sebang.ems.model.Do.VBaseUntAmt;
import com.sebang.ems.model.Do.VMonEngClose;
import com.sebang.ems.model.Do.VMonSiteEngCefAmt;
import com.sebang.ems.model.Do.VSumAmt;
import com.sebang.ems.model.Dto.ReqMonEngClose;
import com.sebang.ems.model.Dto.ResSumAmt;
import com.sebang.ems.service.bgtask.Event;
import com.sebang.ems.service.bgtask.QueueEvent;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.TransactionDefinition;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.support.DefaultTransactionDefinition;

@Service
public class AmtServiceImpl implements AmtService {

    public static final Logger logger = LoggerFactory.getLogger(AmtServiceImpl.class);

    @Autowired
    AmtMapper amtMapper;

    @Autowired
    SiteDeptMapper siteDeptMapper;

    @Autowired
    private PlatformTransactionManager transactionManager;

    DefaultTransactionDefinition def = null;
    TransactionStatus status = null;

    
    /** 배출 시설별 실적 조회
     * @param search
     * @return List<EngPointAmt>
     */
    public List<EngPointAmt> searchEngPointAmt(SearchParam search) {
        return amtMapper.searchEngPointAmt(search);
    }

    
    /** 
     * 배출 시설별 실적 등록 수정
     * @param arr
     */
    public void upsertEngPointAmts(List<TEngPointAmt> arr) {
        
        if(arr.size()>0)
        {
            TEngPointAmt a = arr.get(0);
    		TMonEngClose close =  amtMapper.findMonthEndClosingByIdEngPoint(a.getIdEngPoint()
                ,a.getcYear()
                ,a.getcMon());

            if(close!=null && "Y".equals(close.getcCloseYn()))
                throw new AlreadyMonthEndClosingException("Closed Eng Mon");    
        }
       
        def = new DefaultTransactionDefinition();
        def.setPropagationBehavior(TransactionDefinition.PROPAGATION_REQUIRED);
        status = transactionManager.getTransaction(def);
        
        try {

            for (TEngPointAmt amt : arr) {

                
                if (amt.getIdEngPointAmt() == null) {
                    TEngPointAmt ramt = amtMapper.findEngPointAmt(amt.getIdEngPoint(), amt.getcYear(), amt.getcMon());
                    if (ramt != null)
                        amt.setIdEngPointAmt(ramt.getIdEngPointAmt());
                }
                
                if(amt.getIdEngPoint()!= null && amt.getnUseAmt()!=null)
                {
                    EngPointAmt a =  amtMapper.findEngConversion(amt.getIdEngPoint(), amt.getnUseAmt());
                    if(a!=null){
                        if(a.getnGj()!=null)
                            amt.setnGj(a.getnGj());
                        
                        if(a.getnTco2()!=null)
                            amt.setnTco2(a.getnTco2());
                        
                        if(a.getnToe()!=null)
                            amt.setnToe(a.getnToe());   
                    }
                    
                }
                // S_BASE_VAL 계산 로직 추가
                if(amt.getIdEngPoint()!= null && amt.getnTco2()!=null) {
	                EngPointAmt b = amtMapper.findEngConversionBaseVal(amt.getIdEngPoint(), amt.getnTco2());
	                if(b!=null){
	                	if(b.getsBaseVal()!=null)	// 배출시설이 차량일때는 BaseVal을 구하지 않으며(null이며) 이 경우는 BaseVal의 입력을 스킵한다.
	                		amt.setsBaseVal(b.getsBaseVal());
	                }
                }
                amtMapper.upsertEngPointAmt(amt);
            }
            transactionManager.commit(status);
        } catch (Exception ex) {
            transactionManager.rollback(status);
            logger.error("upsertEngPointAmts ERROR " + ex.getMessage());
            throw new BaseException(ex.getMessage());
        }         
    }

    /** 
     * 월 마감 여부 확인
     * @param close
     */
    public Integer findMonthEndClosing(String cYear,String cMon){
        return amtMapper.enableMonthEndClosing(cYear,cMon);
    }
    
    // /** 
    //  * 월 마감 , 월마감 취소
    //  * @param close
    //  */
    // public void upsertMonEngClose(TMonEngClose close) {
    //     TMonEngClose ret = amtMapper.findMonthEndClosing(close);
    //     if (ret != null) {
    //         close.setIdMonEngClose(ret.getIdMonEngClose());
    //     }
    //     amtMapper.upsertMonEngClose(close);
    // }

   
    /** 월마감  
     * @param close
     */
    @Transactional(rollbackFor = Exception.class)
    public void closeMonEngClose(TMonEngClose close) {

        close.setcCloseYn("G");

        if (close.getcMon() != null && close.getcMon().length() == 1) {
            close.setcMon("0" + close.getcMon());
        }

        amtMapper.upsertMonEngClose(close);

        String key = close.getcYear()+close.getcMon();
        Event qe = new Event(key,new ReqMonEngClose(close));        
        QueueEvent.instance().push(qe);
    }    
    
    /** 월마감 취소
     * @param close
     */
    @Transactional(rollbackFor = Exception.class)
    public void cancelMonEngClose(TMonEngClose close) {
        close.setcCloseYn("N");
        if(close.getcMon() != null && close.getcMon().length()==1)
        {
            close.setcMon("0"+close.getcMon());
        }
        amtMapper.cancelMonEngClose(close);
    }

    /**
     * 월 마감 조회 
     * @param param : SearchParam
     * @return List<VMonEngClose>
     */
    public List<VMonEngClose> searchMonEngClose(SearchParam param) {
        return amtMapper.searchMonEngClose(param);
    }

    /**
     * 월 마감 후처리 작업
     * @param param : ReqMonEngClose
     */
    public void callSpDoCloseMonth(Object c)
    {
        if(c.getClass() !=  ReqMonEngClose.class)
            throw new BaseException("callSpDoCloseMonth invalid class" );

        ReqMonEngClose close = (ReqMonEngClose)c;

        String cYYYYMM = close.getcYear()+close.getcMon();
        close.setCYear(cYYYYMM);
        amtMapper.callSPDoCloseMonth(close);
        if(close.getResult() == null || (close.getResult()!=null && close.getResult() != 1))
        {
            logger.error("<<<< callSpDoCloseMonth error >>>>" );
        }else
             logger.info("<<<< callSpDoCloseMonth success >>>>" );
        
    }

    /** 사업부별 월 에너지 배출 실적
     * @param param
     * @return List<VMonSiteEngCefAmt>
     */
    public List<VMonSiteEngCefAmt> searchMonSiteEngCefAmt(SearchParam param) {
        return amtMapper.searchMonSiteEngCefAmt(param);
    }

    /**
     * 전체 실적 조회
     * @param p
     * @return Map<String,Map<String,List<ResSumAmt>>>
     */
    public Map<String, Map<String, List<ResSumAmt>>> searchTotalSumAmt(SearchParam p) {

        
        Map<String, Map<String, List<ResSumAmt>>> resMap = new HashMap<String, Map<String, List<ResSumAmt>>>();
        
        Integer cYear = Integer.parseInt(p.getCYear());
        Integer bYear = cYear - 1;
        // 1. 반드시 내려가야 할 기본 구조 생성
        for (int i=cYear; i>=bYear; i--) {
        	Map<String, List<ResSumAmt>> sumAmt = new HashMap<String, List<ResSumAmt>>();
            
            VSumAmt emptyVSumAmt = new VSumAmt();
            emptyVSumAmt.setcYear(String.valueOf(i));
            emptyVSumAmt.setM1Gj(BigDecimal.ZERO); emptyVSumAmt.setM1Co2(BigDecimal.ZERO); emptyVSumAmt.setM1Toe(BigDecimal.ZERO);
            emptyVSumAmt.setM2Gj(BigDecimal.ZERO); emptyVSumAmt.setM2Co2(BigDecimal.ZERO); emptyVSumAmt.setM2Toe(BigDecimal.ZERO);
            emptyVSumAmt.setM3Gj(BigDecimal.ZERO); emptyVSumAmt.setM3Co2(BigDecimal.ZERO); emptyVSumAmt.setM3Toe(BigDecimal.ZERO);
            emptyVSumAmt.setM4Gj(BigDecimal.ZERO); emptyVSumAmt.setM4Co2(BigDecimal.ZERO); emptyVSumAmt.setM4Toe(BigDecimal.ZERO);
            emptyVSumAmt.setM5Gj(BigDecimal.ZERO); emptyVSumAmt.setM5Co2(BigDecimal.ZERO); emptyVSumAmt.setM5Toe(BigDecimal.ZERO);
            emptyVSumAmt.setM6Gj(BigDecimal.ZERO); emptyVSumAmt.setM6Co2(BigDecimal.ZERO); emptyVSumAmt.setM6Toe(BigDecimal.ZERO);
            emptyVSumAmt.setM7Gj(BigDecimal.ZERO); emptyVSumAmt.setM7Co2(BigDecimal.ZERO); emptyVSumAmt.setM7Toe(BigDecimal.ZERO);
            emptyVSumAmt.setM8Gj(BigDecimal.ZERO); emptyVSumAmt.setM8Co2(BigDecimal.ZERO); emptyVSumAmt.setM8Toe(BigDecimal.ZERO);
            emptyVSumAmt.setM9Gj(BigDecimal.ZERO); emptyVSumAmt.setM9Co2(BigDecimal.ZERO); emptyVSumAmt.setM9Toe(BigDecimal.ZERO);
            emptyVSumAmt.setM10Gj(BigDecimal.ZERO); emptyVSumAmt.setM10Co2(BigDecimal.ZERO); emptyVSumAmt.setM10Toe(BigDecimal.ZERO);
            emptyVSumAmt.setM11Gj(BigDecimal.ZERO); emptyVSumAmt.setM11Co2(BigDecimal.ZERO); emptyVSumAmt.setM11Toe(BigDecimal.ZERO);
            emptyVSumAmt.setM12Gj(BigDecimal.ZERO); emptyVSumAmt.setM12Co2(BigDecimal.ZERO); emptyVSumAmt.setM12Toe(BigDecimal.ZERO);

            emptyVSumAmt.setsLogiTypeCd("1");
            sumAmt.put("1", ResSumAmt.toList(emptyVSumAmt));

            emptyVSumAmt.setsLogiTypeCd("2");
            sumAmt.put("2", ResSumAmt.toList(emptyVSumAmt));
            
            resMap.put(String.valueOf(i), sumAmt);
        }
        
        // 2. 올해 년도 set
        List<VSumAmt> list = amtMapper.searchTotalSumAmt(p);
        for (VSumAmt item : list) {
            String key = item.getcYear();
            if (resMap.containsKey(key)) {	// 이미 입력한 년도 데이터가 있을 경우, 
                Map<String, List<ResSumAmt>> sumAmt = resMap.get(key);
                sumAmt.put(item.getsLogiTypeCd(), ResSumAmt.toList(item));
            } else {
                Map<String, List<ResSumAmt>> sumAmt = new HashMap<String, List<ResSumAmt>>();
                sumAmt.put(item.getsLogiTypeCd(), ResSumAmt.toList(item));
                resMap.put(key, sumAmt);
            }
        }
        
        // 2. 작년 년도 set
        p.setCYear(String.valueOf(bYear));
        List<VSumAmt> list2 = amtMapper.searchTotalSumAmt(p);

        for (VSumAmt item : list2) {
            String key = item.getcYear();
            if (resMap.containsKey(key)) {	// 이미 입력한 년도 데이터가 있을 경우, 
                Map<String, List<ResSumAmt>> sumAmt = resMap.get(key);
                sumAmt.put(item.getsLogiTypeCd(), ResSumAmt.toList(item));
            } else {
                Map<String, List<ResSumAmt>> sumAmt = new HashMap<String, List<ResSumAmt>>();
                sumAmt.put(item.getsLogiTypeCd(), ResSumAmt.toList(item));
                resMap.put(key, sumAmt);
            }
        }
        
        
        return resMap;
    }

    /**
     * 연도 별 실적 조회
     * @param p
     * @return Map<String,List<ResSumAmt>>
     */
    public Map<String, List<ResSumAmt>> searchYearSumAmt(SearchParam p) {

        List<VSumAmt> list = amtMapper.searchYearSumAmt(p);
        Map<String, List<ResSumAmt>> resMap = new HashMap<String, List<ResSumAmt>>();

        for (VSumAmt item : list) {
            String key = "1";
            if (resMap.containsKey(key)) {
                List<ResSumAmt> sumAmt = resMap.get(key);
                sumAmt.add(ResSumAmt.toEntity1(item));
            } else {
                List<ResSumAmt> sumAmt = new ArrayList<ResSumAmt>();
                sumAmt.add(ResSumAmt.toEntity1(item));
                resMap.put(key, sumAmt);
            }

            key = "2";
            if (resMap.containsKey(key)) {
                List<ResSumAmt> sumAmt = resMap.get(key);
                sumAmt.add(ResSumAmt.toEntity2(item));
            } else {
                List<ResSumAmt> sumAmt = new ArrayList<ResSumAmt>();
                sumAmt.add(ResSumAmt.toEntity2(item));
                resMap.put(key, sumAmt);
            }

            key = "3";
            if (resMap.containsKey(key)) {
                List<ResSumAmt> sumAmt = resMap.get(key);
                sumAmt.add(ResSumAmt.toEntity3(item));
            } else {
                List<ResSumAmt> sumAmt = new ArrayList<ResSumAmt>();
                sumAmt.add(ResSumAmt.toEntity3(item));
                resMap.put(key, sumAmt);
            }

            key = "4";
            if (resMap.containsKey(key)) {
                List<ResSumAmt> sumAmt = resMap.get(key);
                sumAmt.add(ResSumAmt.toEntity4(item));
            } else {
                List<ResSumAmt> sumAmt = new ArrayList<ResSumAmt>();
                sumAmt.add(ResSumAmt.toEntity4(item));
                resMap.put(key, sumAmt);
            }

            key = "5";
            if (resMap.containsKey(key)) {
                List<ResSumAmt> sumAmt = resMap.get(key);
                sumAmt.add(ResSumAmt.toEntity5(item));
            } else {
                List<ResSumAmt> sumAmt = new ArrayList<ResSumAmt>();
                sumAmt.add(ResSumAmt.toEntity5(item));
                resMap.put(key, sumAmt);
            }

            key = "6";
            if (resMap.containsKey(key)) {
                List<ResSumAmt> sumAmt = resMap.get(key);
                sumAmt.add(ResSumAmt.toEntity6(item));
            } else {
                List<ResSumAmt> sumAmt = new ArrayList<ResSumAmt>();
                sumAmt.add(ResSumAmt.toEntity6(item));
                resMap.put(key, sumAmt);
            }

            key = "7";
            if (resMap.containsKey(key)) {
                List<ResSumAmt> sumAmt = resMap.get(key);
                sumAmt.add(ResSumAmt.toEntity7(item));
            } else {
                List<ResSumAmt> sumAmt = new ArrayList<ResSumAmt>();
                sumAmt.add(ResSumAmt.toEntity7(item));
                resMap.put(key, sumAmt);
            }

            key = "8";
            if (resMap.containsKey(key)) {
                List<ResSumAmt> sumAmt = resMap.get(key);
                sumAmt.add(ResSumAmt.toEntity8(item));
            } else {
                List<ResSumAmt> sumAmt = new ArrayList<ResSumAmt>();
                sumAmt.add(ResSumAmt.toEntity8(item));
                resMap.put(key, sumAmt);
            }

            key = "9";
            if (resMap.containsKey(key)) {
                List<ResSumAmt> sumAmt = resMap.get(key);
                sumAmt.add(ResSumAmt.toEntity9(item));
            } else {
                List<ResSumAmt> sumAmt = new ArrayList<ResSumAmt>();
                sumAmt.add(ResSumAmt.toEntity9(item));
                resMap.put(key, sumAmt);
            }

            key = "10";
            if (resMap.containsKey(key)) {
                List<ResSumAmt> sumAmt = resMap.get(key);
                sumAmt.add(ResSumAmt.toEntity10(item));
            } else {
                List<ResSumAmt> sumAmt = new ArrayList<ResSumAmt>();
                sumAmt.add(ResSumAmt.toEntity10(item));
                resMap.put(key, sumAmt);
            }

            key = "11";
            if (resMap.containsKey(key)) {
                List<ResSumAmt> sumAmt = resMap.get(key);
                sumAmt.add(ResSumAmt.toEntity11(item));
            } else {
                List<ResSumAmt> sumAmt = new ArrayList<ResSumAmt>();
                sumAmt.add(ResSumAmt.toEntity11(item));
                resMap.put(key, sumAmt);
            }

            key = "12";
            if (resMap.containsKey(key)) {
                List<ResSumAmt> sumAmt = resMap.get(key);
                sumAmt.add(ResSumAmt.toEntity12(item));
            } else {
                List<ResSumAmt> sumAmt = new ArrayList<ResSumAmt>();
                sumAmt.add(ResSumAmt.toEntity12(item));
                resMap.put(key, sumAmt);
            }
        }
        return resMap;
    }

    
    /** 
     * 년 실적 분석
     * @param p
     * @return Map<String, List<ResSumAmt>>
     */
    public Map<String, List<ResSumAmt>> searchYearAnalysisAmt(SearchParam p) {

        List<SumAmt> list = null;
        
        if( p.getIdSites()!=null && p.getIdEngPoints()!=null){
            p.setArrIdEngPoint(p.getIdEngPoints().split(","));
            list= amtMapper.searchYearAnalysisPointAmt(p);	// 배출시설별 연도 분석
        }else if (p.getIdSites()!=null)
        {
            p.setArrIdSite(p.getIdSites().split(","));
            list= amtMapper.searchYearAnalysisSiteAmt(p);	// 사업부별 연도 분석
        }else{
            list= amtMapper.searchYearAnalysisAmt(p);	// 연도별 분석
        }

        Map<String, List<ResSumAmt>> resMap = new HashMap<String, List<ResSumAmt>>();
        for (SumAmt item : list) {
            String key = item.getCYear();
            if (resMap.containsKey(key)) {
                List<ResSumAmt> sumAmt = resMap.get(key);
                sumAmt.add(ResSumAmt.toEntity(item));

            } else {
                List<ResSumAmt> sumAmt = new ArrayList<ResSumAmt>();
                sumAmt.add(ResSumAmt.toEntity(item));
                resMap.put(key, sumAmt);
            }
        }
        return resMap;
    }

    
    /** 
     * 월별 실적 분석
     * @param p
     * @return Map<String, List<ResSumAmt>>
     */
    public Map<String, List<ResSumAmt>> searchMonAnalysisAmt(SearchParam p) {

        List<SumAmt> list = null;
        
        if( p.getIdSites()!=null && p.getIdEngPoints()!=null){
            p.setArrIdEngPoint(p.getIdEngPoints().split(","));
            list= amtMapper.searchMonAnalysisPointAmt(p);
        }else if (p.getIdSites()!=null)
        {
            p.setArrIdSite(p.getIdSites().split(","));
            list= amtMapper.searchMonAnalysisSiteAmt(p);
        }else{
            list= amtMapper.searchMonAnalysisAmt(p);
        }

        Map<String, List<ResSumAmt>> resMap = new HashMap<String, List<ResSumAmt>>();
        for (SumAmt item : list) {
            String key = item.getCYear()+ (item.getCMon().length()==1?"0"+item.getCMon():item.getCMon());
            if (resMap.containsKey(key)) {
                List<ResSumAmt> sumAmt = resMap.get(key);
                sumAmt.add(ResSumAmt.toEntity(item));

            } else {
                List<ResSumAmt> sumAmt = new ArrayList<ResSumAmt>();
                sumAmt.add(ResSumAmt.toEntity(item));
                resMap.put(key, sumAmt);
            }
        }
        return resMap;
    }

    /** 물류 원단위 분석
     * @param p
     * @return List<VBaseUntAmt>
     */
    public List<VBaseUntAmt> searchLogisBaseUntAnalysis(SearchParam p)
    {
        p.setSLogiTypeCd("1"); // 물류
        return amtMapper.searchLogisBaseUntAnalysis(p);
    }

    /** 원단위 분석
     * @param p
     * @return List<VBaseUntAmt>
     */
    public List<VBaseUntAmt> searchBaseUntAmtAnalysis(SearchParam p)
    {
        return amtMapper.searchBaseUntAmtAnalysis(p);
    }


    public List<SumAmt4Home> searchGasSumAmt4Home(SearchParam p) {
    
        return amtMapper.searchGasSumAmt4Home(p);
    }

    public List<SumAmt4Home> searchEngSumAmt4Home(SearchParam p) {
    
        return amtMapper.searchEngSumAmt4Home(p);
    }

}
