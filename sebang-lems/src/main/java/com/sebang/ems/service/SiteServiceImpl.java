package com.sebang.ems.service;

import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;

import com.sebang.ems.dao.CodeMapper;
import com.sebang.ems.dao.CorpMapper;
import com.sebang.ems.dao.SiteDeptMapper;
import com.sebang.ems.dao.UsersMapper;
import com.sebang.ems.dao.gen.TErpDeptMapper;
import com.sebang.ems.domain.Exception.BaseException;
import com.sebang.ems.domain.common.SearchParam;
import com.sebang.ems.model.Employee;
import com.sebang.ems.model.SiteDept;
import com.sebang.ems.model.Do.TCode;
import com.sebang.ems.model.Do.TCorpInfo;
import com.sebang.ems.model.Do.TErpDept;
import com.sebang.ems.model.Do.TGasDeptMapping;
import com.sebang.ems.model.Do.TSiteInfo;
import com.sebang.ems.model.Dto.ReqDeptMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.TransactionDefinition;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.DefaultTransactionDefinition;

@Service
public class SiteServiceImpl implements SiteService {

    public static final Logger logger = LoggerFactory.getLogger(SiteServiceImpl.class);

    @Autowired
    CorpMapper corpMapper;

    @Autowired
    SiteDeptMapper siteMapper;

    @Autowired
    UsersMapper userMapper;

    @Autowired
    CodeMapper codeMapper;

    @Autowired
    TErpDeptMapper erpDeptMapper;

    @Autowired
    private PlatformTransactionManager transactionManager;

    DefaultTransactionDefinition def = null;
    TransactionStatus status = null;

    
    /** 법인 정보 조회
     * @param sCorpId 법인 ID
     * @return TCorpInfo
     */
    public TCorpInfo findCorp(String sCorpId) {

        TCorpInfo result = corpMapper.findOne(sCorpId);

        return result;
    }

    
    /** 법인 정보 등록 수정
     * @param corpInfo
     */
    public void upsertCorp(TCorpInfo corpInfo) {
        Long idCorp = null;
        if (null != corpInfo.getsCorpId()) {
            TCorpInfo c = corpMapper.findOne(corpInfo.getsCorpId());
            if (c != null)
                idCorp = c.getIdCorp();
        }
        corpInfo.setIdCorp(idCorp);

        corpMapper.upsert(corpInfo);
    }

    
    /** 사업장 정보 찾기
     * @param idSite site seq no 
     * @return List<TSiteInfo>
     */
    public List<TSiteInfo> findSiteById(Long idSite) {
        return siteMapper.findSiteById(idSite);
    }

    
    /** 사업장 정보 등록 수정
     * @param site
     */
    public void upsertSite(TSiteInfo site) {
        siteMapper.upsertSite(site);
    }

    
    /** 사업장 정보 삭제
     * @param idSite
     */
    public void deleteSite(Long idSite) {
        TSiteInfo site = new TSiteInfo();

        site.setIdSite(idSite);
        site.setcDelYn("Y");
        siteMapper.upsertSite(site);
    }

    
    /** 부서 리스트 찾기
     * @return List<TErpDept>
     */
    public List<TErpDept> findAllErpDept() {
        return siteMapper.findAllDept();
    }

    
    /** 사업부 내 부서 찾기
     * @param idSite site seq null 이면 모든 부서 
     * @return List<SiteDept>
     */
    public List<SiteDept> findSiteDept(Long idSite) {
        SearchParam p = new SearchParam();
        p.setIdSite(idSite);
        return siteMapper.searchSiteDept(p);
    }

    
    // /** 사업부에 부서 등록 - 단건
    //  * @param idSite 사업부 seq no
    //  * @param idDept 부서 seq no
    //  */
    // public void addDeptToSite(Long idSite, Long idDept) {
    //     TGasDeptMapping sp = siteMapper.findErpDeptInSite(idSite,idDept);
    //     if (sp != null) {
    //         siteMapper.removeDept(sp);
    //     }
    //     TGasDeptMapping p = new TGasDeptMapping();
    //     p.setIdSite(idSite);
    //     p.setIdErpDept(idDept);
    //     siteMapper.addDept(p);
    // }

    public void addDeptToSite(ReqDeptMap reqParam) {
        
        if(reqParam.getIdGasDeptMapping()!=null)
        {
            SiteDept m = siteMapper.findSiteDeptMapping(reqParam.getIdGasDeptMapping());
            if(m!= null)
                reqParam.setIdGasDeptMapping(m.getIdGasDeptMapping());
        }

        siteMapper.upsertSiteDept(reqParam.toEntity());   
    }

    

    
    /**
     * 사업부에 부서 등록 - 다중
     * 
     * @param reqParam
     */
    public void addDeptToSite(List<ReqDeptMap> reqParam) {

        def = new DefaultTransactionDefinition();
        def.setPropagationBehavior(TransactionDefinition.PROPAGATION_REQUIRED);
        status = transactionManager.getTransaction(def);
        try {
            for (ReqDeptMap dep : reqParam) {
                addDeptToSite(dep);
            }
            transactionManager.commit(status);
        } catch (Exception e) {
            transactionManager.rollback(status);
            logger.error("addDeptToSite Error " + e.getMessage());
            throw new BaseException(e.getMessage());
        }
        
    }

    
    /** 사업부에서 부서 삭제 - 단건
     * @param sSiteId
     * @param sDeptId
     */
    public void removeDeptFromSite(Long sSiteId, Long sDeptId) {
        TGasDeptMapping p = new TGasDeptMapping();

        p.setIdSite(sSiteId);
        p.setIdErpDept(sDeptId);
        siteMapper.removeDept(p);
    }

    public void removeDeptFromSite(List<ReqDeptMap> param) {

        def = new DefaultTransactionDefinition();
        def.setPropagationBehavior(TransactionDefinition.PROPAGATION_REQUIRED);
        status = transactionManager.getTransaction(def);
        try {
            for (ReqDeptMap dep : param) {
                siteMapper.removeDept(dep.toEntity());
            }
            transactionManager.commit(status);
        } catch (Exception e) {
            transactionManager.rollback(status);
            logger.error("removeDeptFromSite Error " + e.getMessage());
            throw new BaseException(e.getMessage());
        } 
        
    }

   
    /** 사원 정보 등록 수정
     * @param emp
     */
    public void upsertEmployee(Employee emp) {
        userMapper.upsertEmployee(emp);
    }

    
    /** 사원 정보 삭제
     * @param sEmpId
     */
    public void deleteEmployee(String sEmpId) {
        Employee emp = new Employee();
        emp.setsEmpId(sEmpId);
        emp.setcDelYn("Y");
        userMapper.upsertEmployee(emp);
    }

    
    /** 권한 코드 가져오기
     * @return List<TCode>
     */
    public List<TCode> findAuth() {
        return codeMapper.find("AUTH");
    }

    
    /** 부서 정보 가져오기
     * @param dept
     * @param deptid
     * @return TErpDept
     */
    private TErpDept findDept(List<TErpDept> dept, String deptid) {
        TErpDept ret = null;

        for (TErpDept i : dept) {
            if (deptid.equals(i.getsDeptId())) {
                logger.debug("id:" + i.getsDeptId() + ",Nm:" + i.getsDeptNm());
                return i;
            }
        }
        return ret;
    }

    
    /** 전체 부서명 가져오기 
     * @param dept : List<TErpDept> 전체 부서리스트.
     * @param inDeptId : String  부서Id
     * @return String : Full name 부서명
     */
    String buildFullDeptNm(List<TErpDept> dept, String inDeptId) {
        String deptNm = "";
        StringBuilder sb = new StringBuilder();
        String deptId = inDeptId;

        TErpDept ch = null;

        do {

            ch = findDept(dept, deptId);
            if (ch != null) {
                deptId = ch.getsUDeptId();
                deptNm = ch.getsDeptNm();
                if (sb.length() == 0)
                    sb.append(deptNm);
                else
                    sb.insert(0, deptNm + " > ");
            }
        } while (ch != null);

        return sb.toString();
    }

    public void fillFullDeptNm() {
        List<TErpDept> depts = siteMapper.findAllDept();

        for (TErpDept dept : depts) {
            String fullDeptNm = buildFullDeptNm(depts, dept.getsDeptId());
            dept.setsFullDeptNm(fullDeptNm);
            erpDeptMapper.updateByPrimaryKeySelective(dept);
        }
    }
    
    /** 하위 부서 리스트 가져오기
     * @param allDepts
     * @param deptid 부서 Id
     * @return List<TErpDept>
     */
    List<TErpDept> findChildDepts(List<TErpDept> allDepts, String deptid) {
        List<TErpDept> ret = new ArrayList<TErpDept>();
        allDepts.stream().forEach(i -> {
            if (deptid.equals(i.getsUDeptId())) {
                // System.out.println("id:" + i.getsDeptId() + ",Nm:" + i.getsDeptNm());
                ret.add(i);
            }
        });
        return ret;
    }

    /**
     *  하위 부서 목록 가져오기
     * @param allDepts: List 전체 부서 목록
     * @param depts : List  부서 ID 목록
     * @param leafDepts : List  Leaf 부서 목록
     * @param includeInternalNode : internalNode를 Leaf Node로 포함할지 말지 결정.
     * @return List 하위 부서 ID 목록
     */
    List<TErpDept> findChildDepts(List<TErpDept> allDepts, List<TErpDept> depts, List<TErpDept> leafDepts,boolean includeInternalNode) {
        List<TErpDept> next = new ArrayList<TErpDept>();

        for (TErpDept dept : depts) {
            List<TErpDept> r = findChildDepts(allDepts, dept.getsDeptId());
            if(includeInternalNode==true)
                leafDepts.add(dept);
            else if (includeInternalNode==false && r.size() == 0)
                leafDepts.add(dept);
            next.addAll(r);
        };
        return next;
    }

    
    /** 하위 부서 Full Name 리스트를 가져온다.
     * @param sDeptId : String 부서 ID
     * @return List<String> 부서 풀네임 리스트
     */
    public List<TErpDept> findChildDeptsByDeptId(String sDeptId) {

        List<TErpDept> leaf = new ArrayList<TErpDept>();
        List<TErpDept> depts = new ArrayList<TErpDept>();

        List<TErpDept> allDepts = siteMapper.findAllDept();
        
        
        
        
        TErpDept target = findDept(allDepts,sDeptId);  
        if(target== null)
            return new ArrayList<TErpDept>();

        depts.add(target);

        List<TErpDept> childDepts = null;
        int level = 0;
        do {
            logger.debug("level " + level++ + ", childs size:" + depts.size() + ", results size:" + leaf.size());
            childDepts = findChildDepts(allDepts, depts, leaf,false);
            depts.clear();
            depts.addAll(childDepts);
        } while (childDepts.size() > 0);

        List<String> resultDepts = new ArrayList<String>();

        for (TErpDept leafDept : leaf) {
            leafDept.setsFullDeptNm(buildFullDeptNm(allDepts, leafDept.getsDeptId()));
        }
        return leaf;
    }

    /**
     * 하위 부서 Full Name 리스트를 가져온다.
     * 모든 internal node를 leaf 하는 full name을 추가한다.
     * @param sDeptId
     * @return
     */
    public List<TErpDept> findAllChildDeptsByDeptId(String sDeptId) {

        List<TErpDept> leaf = new ArrayList<TErpDept>();
        List<TErpDept> depts = new ArrayList<TErpDept>();
        List<TErpDept> allDepts = siteMapper.findAllDept();
        
        TErpDept target = findDept(allDepts,sDeptId);  
        if(target== null)
            return new ArrayList<TErpDept>();

        depts.add(target);

        List<TErpDept> childDepts = null;
        int level = 0;

        do {
            logger.debug("level " + level++ + ", childs size:" + depts.size() + ", results size:" + leaf.size());
            childDepts = findChildDepts(allDepts, depts, leaf,true);
            
            depts.clear();
            depts.addAll(childDepts);
            
        } while (childDepts.size() > 0);

        List<String> resultDepts = new ArrayList<String>();

        for (TErpDept leafDept : leaf) {
            leafDept.setsFullDeptNm(buildFullDeptNm(allDepts, leafDept.getsDeptId()));
        }
        return leaf;
    }
}
