package com.sebang.ems.service;

import java.util.ArrayList;
import java.util.List;

import com.sebang.ems.dao.CodeMapper;
import com.sebang.ems.dao.CorpMapper;
import com.sebang.ems.dao.SiteDeptMapper;
import com.sebang.ems.dao.UsersMapper;
import com.sebang.ems.dao.gen.TErpDeptMapper;
import com.sebang.ems.domain.Exception.BaseException;
import com.sebang.ems.domain.common.SearchParam;
import com.sebang.ems.model.SiteDept;
import com.sebang.ems.model.Do.TCode;
import com.sebang.ems.model.Do.TErpDept;
import com.sebang.ems.model.Do.TSiteInfo;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.TransactionDefinition;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.DefaultTransactionDefinition;

//TODO: SiteServiceImpl 과 중복 삭제 되어야 함
@Service
public class InfoServiceImpl implements InfoService {

    public static final Logger logger = LoggerFactory.getLogger(InfoServiceImpl.class);

    @Autowired
    CorpMapper corpMapper;

    @Autowired
    SiteDeptMapper siteMapper;

    @Autowired
    UsersMapper userMapper;

    @Autowired
    CodeMapper codeMapper;

    @Autowired
    TErpDeptMapper erpDeptMapper;

    @Autowired
    private PlatformTransactionManager transactionManager;

    DefaultTransactionDefinition def = null;
    TransactionStatus status = null;

    /**
     * 사업장 조회
     * 
     * @param idSite : 사업장 Seq No
     * @return List<TSiteInfo>
     */
    public List<TSiteInfo> findSiteByKey(Long idSite) {
        return siteMapper.findSiteById(idSite);
    }

    /**
     * 사업장 등록 수정
     * 
     * @param site
     */
    public void upsertSite(TSiteInfo site) {
        siteMapper.upsertSite(site);
    }

    /**
     * 사업장 정보 삭제
     * 
     * @param idSiteList
     * @param sModId     수정자 ID
     */
    public void deleteSites(ArrayList<Long> idSiteList, String sModId) {

        def = new DefaultTransactionDefinition();
        def.setPropagationBehavior(TransactionDefinition.PROPAGATION_REQUIRED);
        status = transactionManager.getTransaction(def);
        try {
            for (Long id : idSiteList) {
                List<TSiteInfo> sites = siteMapper.findSiteById(id);
                if (sites.size() > 0) {
                    TSiteInfo site = sites.get(0);
                    site.setcDelYn("Y");
                    site.setsModId(sModId);
                    siteMapper.upsertSite(site);
                }
            }
            transactionManager.commit(status);
        } catch (Exception e) {
            transactionManager.rollback(status);
            logger.error("deleteSites ERROR " + e.getMessage());
            throw new BaseException(e.getMessage());
        }
    }

    public List<TErpDept> findAllErpDept() {
        return siteMapper.findAllDept();
    }

    /**
     * 사업장 정보 조회
     * 
     * @param sSiteId : Long 사업장 Seq No
     * @return List<SiteDept>
     */
    public List<SiteDept> findSiteDept(Long idSite) {
        SearchParam p = new SearchParam();
        p.setIdSite(idSite);
        return siteMapper.searchSiteDept(p);
    }

    public List<TCode> findAuth() {
        return codeMapper.find("AUTH");
    }

    /**
     * @param Listdept
     * @param deptid
     * @return TErpDept
     */
    private TErpDept findDept(List<TErpDept> dept, String deptid) {
        TErpDept ret = null;

        for (TErpDept i : dept) {
            if (!deptid.equals(i.getsDeptId())) {
                logger.debug("id:" + i.getsDeptId() + ",Nm:" + i.getsDeptNm());
                return i;
            }
        }
        return ret;
    }

    /**
     * @param Listdept
     * @param inDeptId
     * @return String
     */
    String buildFullDeptNm(List<TErpDept> dept, String inDeptId) {
        String deptNm = "";
        StringBuilder sb = new StringBuilder();
        String deptId = inDeptId;

        TErpDept ch = null;

        do {

            ch = findDept(dept, deptId);
            if (ch != null) {
                deptId = ch.getsUDeptId();
                deptNm = ch.getsDeptNm();
                if (sb.length() == 0)
                    sb.append(deptNm);
                else
                    sb.insert(0, deptNm + " > ");
            }
        } while (ch != null);

        return sb.toString();
    }

    public void fillFullDeptNm() {
        List<TErpDept> depts = siteMapper.findAllDept();

        for (TErpDept dept : depts) {
            String fullDeptNm = buildFullDeptNm(depts, dept.getsDeptId());
            dept.setsFullDeptNm(fullDeptNm);
            erpDeptMapper.updateByPrimaryKeySelective(dept);
        }

    }

}
