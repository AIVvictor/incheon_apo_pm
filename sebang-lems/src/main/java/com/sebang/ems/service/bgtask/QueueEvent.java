package com.sebang.ems.service.bgtask;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class QueueEvent
{
    public static final Logger logger = LoggerFactory.getLogger(QueueEvent.class);

    
    
    public BlockingQueue<Event> reportEventQ = new LinkedBlockingQueue<Event>();

	/**
	 * Single-tone
	 */
	static QueueEvent self = new QueueEvent();

	public static QueueEvent instance() {
		return self;
	}
	// =====================================================================

	/**
	 * ER event를 Smart City 관제 시스템으로 전송하기 위해 queue에 넣는다.
	 * @param er
	 * @return
	 */
	public boolean push(Event er) {

		if(er.getKey() ==  null)
			return false;

		for(Event e :reportEventQ)
		{
			// 이미 등록 되어 있는 경우 
			if(er.getKey().equals(e.getKey()))
				return true;
		};
		
		try {
			
			reportEventQ.put(er);
			
		} catch (InterruptedException e) {
			logger.error("put: " + e.toString());
			return false;
		}
		
		return true;

	}

	public Event pop() {
		
		Event e = null;
		
		try {
			if( reportEventQ.size() <= 0 )
				return e;
			e = reportEventQ.take();
			
		} catch (InterruptedException ex) {
			
			logger.error("pop: " + ex.toString());
			
		}
		
		return e;
	}
}