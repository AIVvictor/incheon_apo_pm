package com.sebang.ems.service;

import java.util.List;
import java.util.Map;

import com.sebang.ems.dao.CefMapper;
import com.sebang.ems.dao.gen.TCefModMapper;
import com.sebang.ems.dao.gen.TEngCefMapper;
import com.sebang.ems.dao.gen.TEngPointMapper;
import com.sebang.ems.domain.Exception.BaseException;
import com.sebang.ems.domain.common.SearchParam;
import com.sebang.ems.model.Do.TCefMod;
import com.sebang.ems.model.Do.TEngCef;
import com.sebang.ems.model.Do.TEngPoint;
import com.sebang.ems.model.Do.VEngCef;
import com.sebang.ems.model.Do.VEngPoint;
import com.sebang.ems.model.Dto.ReqCefMod;
import com.sebang.ems.model.Dto.ReqEngCef;
import com.sebang.ems.util.StringUtil;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.TransactionDefinition;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.DefaultTransactionDefinition;

@Service
public class CefServiceImpl implements CefService {

    public static final Logger logger = LoggerFactory.getLogger(EmployeeServiceImpl.class);

    @Autowired
    CefMapper cefMapper;

    @Autowired
    TEngCefMapper engCefMapper;

    @Autowired
    TCefModMapper cefModMapper;

    @Autowired
    TEngPointMapper engPointMapper;

    @Autowired
    private PlatformTransactionManager transactionManager;

    DefaultTransactionDefinition def = null;
    TransactionStatus status = null;

    /**
     * 배출원 찾기
     * 
     * @param sEmtCd
     * @return List<VEngCef>
     */
    public List<VEngCef> findByEmt(String sEmtCd) {
        return cefMapper.findByEmt(sEmtCd);
    }

    /**
     * 배출원 조회
     * 
     * @param search
     * @return List<VEngCef>
     */
    public List<VEngCef> search(SearchParam search) {
        return cefMapper.searchCef(search);
    }

    /**
     * 배출원 등록 수정
     * 
     * @param engCef
     */
    public void upsert(TEngCef engCef) {
        if (engCef.getIdEngCef() == null)
            engCefMapper.insertSelective(engCef);
        else
            engCefMapper.updateByPrimaryKeySelective(engCef);
    }

    /**
     * 배출원 등록 수정
     * 
     * @param reqParam
     */
    public void upsert(List<ReqEngCef> reqParam) {

      
        def = new DefaultTransactionDefinition();
        def.setPropagationBehavior(TransactionDefinition.PROPAGATION_REQUIRED);
        status = transactionManager.getTransaction(def);
        try {
            for (ReqEngCef cef : reqParam) {
                if (cef.getIdEngCef() == null) {
                    cef.setsRegId(cef.getsModId());
                    engCefMapper.insertSelective(cef.toEntity());
                } else
                    engCefMapper.updateByPrimaryKeySelective(cef.toEntity());
            }
            transactionManager.commit(status);
        } catch (Exception ex) {
            transactionManager.rollback(status);
            logger.error("upsert ERROR " + ex.getMessage());
            throw new BaseException(ex.getMessage());
        }
    }

    /**
     * 배출원 삭제
     * 
     * @param idEngCef
     */
    public void delete(Long idEngCef) {

        TEngCef engCef = new TEngCef();
        engCef.setIdEngCef(idEngCef);
        engCef.setcDelYn("Y");
        engCefMapper.updateByPrimaryKeySelective(engCef);
    }

    /**
     * 배출원 삭제
     * 
     * @param list
     * @param sModId
     */
    public void delete(List<ReqEngCef> list, String sModId) {

        def = new DefaultTransactionDefinition();
        def.setPropagationBehavior(TransactionDefinition.PROPAGATION_REQUIRED);
        status = transactionManager.getTransaction(def);
        try {
            for (ReqEngCef c : list) {
                TEngCef engCef = c.toEntity();
                engCef.setcDelYn("Y");
                engCef.setsModId(sModId);
                engCefMapper.updateByPrimaryKeySelective(engCef);
            }       
            transactionManager.commit(status);    
        } catch (Exception ex) {
            transactionManager.rollback(status);
            logger.error("delete ERROR " + ex.getMessage());
            throw new BaseException(ex.getMessage());
        } 
      
    }

    /**
     * 배출 계수 찾기
     * 
     * @param idEngCef
     * @return List<TCefMod>
     */
    public List<TCefMod> findMod(Long idEngCef) {
        return cefMapper.findMod(idEngCef);
    }

    /**
     * 배출 계수 등록 수정
     * 
     * @param cefMod
     */
    public void upsertCefMod(TCefMod cefMod) {
        if (cefMod.getIdCefMod() != null)
            cefModMapper.updateByPrimaryKeySelective(cefMod);
        else
            cefModMapper.insertSelective(cefMod);
    }

    /**
     * 배출 계수 등록 수정
     * 
     * @param reqParam
     */
    public void upsertCefMod(List<ReqCefMod> reqParam) {

        def = new DefaultTransactionDefinition();
        def.setPropagationBehavior(TransactionDefinition.PROPAGATION_REQUIRED);
        status = transactionManager.getTransaction(def);
        try {
            for (ReqCefMod mod : reqParam) {
                if (mod.getIdCefMod() == null) {
                    mod.setsRegId(mod.getsModId());
                    ;
                    cefModMapper.insertSelective(mod.toEntity());
                } else
                    cefModMapper.updateByPrimaryKeySelective(mod.toEntity());
            }
            transactionManager.commit(status);
        } catch (Exception ex) {
            transactionManager.rollback(status);
            logger.error("upsertCefMod ERROR " + ex.getMessage());
            throw new BaseException(ex.getMessage());
        } 
        
    }

    /**
     * 배출 계수 삭제
     * 
     * @param idCefMod
     */
    public void deleteCefMod(Long idCefMod) {

        TCefMod cefMod = new TCefMod();
        cefMod.setIdCefMod(idCefMod);
        // TODO : cDelYn 항목 추가
        cefModMapper.updateByPrimaryKeySelective(cefMod);
    }

    /**
     * 배출 계수 삭제
     * 
     * @param list
     * @param sModId
     */
    public void deleteCefMod(List<ReqCefMod> list, String sModId) {
        def = new DefaultTransactionDefinition();
        def.setPropagationBehavior(TransactionDefinition.PROPAGATION_REQUIRED);
        status = transactionManager.getTransaction(def);
        try {

            for (ReqCefMod c : list) {
                TCefMod engCef = c.toEntity();
                engCef.setcDelYn("Y");
                engCef.setsModId(sModId);
                cefModMapper.updateByPrimaryKeySelective(engCef);
            }
            transactionManager.commit(status);
        } catch (Exception ex) {
            transactionManager.rollback(status);
            logger.error("deleteCefMod ERROR " + ex.getMessage());
            throw new BaseException(ex.getMessage());
        } 
        
    }

    /**
     * 배출시설 찾기
     * 
     * @param idSite     사업장 SEQ NO
     * @param idEngPoint 배출시설 SEQ NO
     * @return List<VEngPoint>
     */
    public List<VEngPoint> findPoint(Long idSite, Long idEngPoint) {
        SearchParam p = new SearchParam();
        p.setIdEngPoint(idEngPoint);
        p.setIdSite(idSite);
        return cefMapper.searchPoint(p);
    }

    /**
     * 사업장내 배출시설 목록 조회
     * 
     * @param idSite : Long 사업장 SEQ NO
     * @return List<VEngPoint> 사업장 목록
     */
    public List<VEngPoint> findPointInSite(Long idSite) {
        return cefMapper.findPointInSite(idSite);
    }

    /**
     * 배출 시설 등록 수정
     * 
     * @param engPoint
     */
    public void upsertEngPoint(TEngPoint engPoint) {

        if (engPoint.getIdEngPoint() != null) {
            engPoint.setsPointId(null); // 배출 시설 ID 는 수정 할 수 없음.
            engPointMapper.updateByPrimaryKeySelective(engPoint);
        } else {
        
            // 배출시설 ID 를 생성한다.
            String pointId = generatePointId(engPoint.getIdSite());
            if(pointId == null)
                throw new BaseException("Invalid idSite");
            
            if(pointId.length()!= 8)
                throw new BaseException("Invalid ENG POINT ID");
            
            engPoint.setsPointId(pointId);                
            String  seq = pointId.substring(2,8);

            Integer i = Integer.parseInt(seq);
            Short s = Short.parseShort(seq);
            engPoint.setSubSeq(i);
            engPoint.setnOrder(s);

            engPoint.setsRegId(engPoint.getsModId());
            engPointMapper.insertSelective(engPoint);
        }
    }

    /**
     * 배출시설 ID 발급
     * 
     * @param idSite
     * @return String 
     */
    public String generatePointId(Long idSite) {

        if(idSite == null)
            throw new BaseException("idSite is mandantory");

        return cefMapper.generateEngPointId(idSite);
    }

    /**
     * 배출 시설 삭제
     * 
     * @param idEngPoint
     * @param sModId
     */
    public void deleteEngPoint(Long idEngPoint, String sModId) {
        TEngPoint po = new TEngPoint();
        po.setIdEngPoint(idEngPoint);
        po.setcDelYn("Y");
        po.setsModId(sModId);
        engPointMapper.updateByPrimaryKeySelective(po);
    }
}
