package com.sebang.ems.service.bgtask;

public class Event
    {
        private Object item;

        private String key;

        public Event(String key, Object item)
        {
            this.key = key;
            this.item = item;
        }

        public Object getItem() {
            return item;
        }

		public String getKey() {
            return key;
        }
    }