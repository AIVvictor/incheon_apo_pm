package com.sebang.ems.service;

import java.util.List;

import com.sebang.ems.dao.FilesMapper;
import com.sebang.ems.domain.Exception.BaseException;
import com.sebang.ems.model.Gfiles;
import com.sebang.ems.model.Do.TFiles;
import com.sebang.ems.storage.StorageService;
import com.sebang.ems.util.Guid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.TransactionDefinition;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.DefaultTransactionDefinition;

@Service
public class FileServiceImpl implements FileService {

    public static final Logger logger = LoggerFactory.getLogger(FileServiceImpl.class);

    private final FilesMapper filesMapper;

    private final StorageService storageService;

    @Autowired
    private PlatformTransactionManager transactionManager;

    DefaultTransactionDefinition def = null;
    TransactionStatus status = null;

    @Autowired
    public FileServiceImpl(FilesMapper fileMapper,StorageService storageService)
    {
        this.filesMapper = fileMapper;
        this.storageService = storageService;
    }


    /**
     * 파일 정보 검색
     * 
     * @param gid : String Global File ID
     * @return Gfiles
     */
    public Gfiles findOneByGid(String gid) {
        List<Gfiles> r = filesMapper.findByGid(gid);
        if (r.size() > 0)
            return r.get(0);

        return null;
    }

    /**
     * 파일 정보를 등록 수정
     * 
     * @param gfile
     */
    public void upsertFiles(Gfiles gfile) {
        def = new DefaultTransactionDefinition();
        def.setPropagationBehavior(TransactionDefinition.PROPAGATION_REQUIRED);
        status = transactionManager.getTransaction(def);
        try {

            filesMapper.upsertFiles(gfile);

            logger.info("fileid =>" + gfile.getIdFile().toString());
            if (gfile.getGId() == null)
                gfile.setGId(Guid.genUUID());

            filesMapper.upsertGfiles(gfile);
            transactionManager.commit(status);
        } catch (Exception ex) {
            transactionManager.rollback(status);
            logger.error("upsertFiles ERROR " + ex.getMessage());
            throw new BaseException(ex.getMessage());
        }
    }

    /**
     * 파일 삭제
     * 
     * @param gid    gid
     * @param idFile file seq no
     */
    public void deleteFile(Long idFile) {
        TFiles f = filesMapper.fineOne(idFile);
        if (f == null)
            throw new BaseException("Invalid idFile");
        Gfiles g = new Gfiles();

        g.setcDelYn("Y");
        g.setIdFile(idFile);
        g.setsFileNm(f.getsFileNm());
        g.setsFilePath(f.getsFilePath());
        g.setsScreenId(f.getsScreenId());
        g.setsFileId(f.getsFileId());
        filesMapper.upsertFiles(g);
    }

    /**
     * 파일 찾기
     * 
     * @param idFile file seq no
     * @return TFiles
     */
    public TFiles findOneByIdFile(Long idFile) {
        return filesMapper.fineOne(idFile);
    }

    /**
     * 삭제 표시된 파일 조회 영구 삭제 조건 - T_FILES.C_DEL_YN = 'Y' and T_FILES.S_TEMP_COL IS NULL
     * 
     * @param maxcount : Integer 조회건 수
     * @return List<Gfiles>
     */
    public List<Gfiles> findDeletedFileMarks(int maxcount) {
        return filesMapper.findDeletedFileMarks((Integer) maxcount);
    }

    /**
     * 영구 삭제 표시
     * T_FILES.S_TEMP_COL = 'clean'
     * @param idFile : Long 파일 SEQ NO
     */
    public void markCleanFile(Long idFile) {

        Gfiles g = new Gfiles();

        g.setIdFile(idFile);
        g.setsTempCol("clean");
        filesMapper.cleanFile(g);
    }

    public void cleanFiles() {

        
        List<Gfiles> mark = findDeletedFileMarks(50);
        for (Gfiles g : mark) {
            
            if(g.getsFilePath() ==  null || g.getsFileNm() ==null)
            {
                markCleanFile(g.getIdFile());
                continue;
            }
            
            storageService.delete(g.getsFilePath(),g.getsFileNm());
            markCleanFile(g.getIdFile());
            logger.debug("cleanFile : idFile:" + g.getGId()+" filename " + g.getsFilePath()+"/"+g.getsFileNm() );
        }
    }
}