package com.sebang.ems.controller.backend.v1;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.sebang.ems.EmsApplication;
import com.sebang.ems.domain.common.ResultCode;
import com.sebang.ems.domain.common.ResultValue;
import com.sebang.ems.model.Dto.ReqLogisTransInfo;
import com.sebang.ems.model.Dto.ReqTransCom;
import com.sebang.ems.model.Dto.ReqVehicle;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.MOCK, classes = { EmsApplication.class,
		ObjectMapper.class })
@AutoConfigureMockMvc(secure = false)
@ContextConfiguration(classes = EmsApplication.class)
@ActiveProfiles("test")
@EnableWebMvc
public class LegacyRestControllerTest{
	@Autowired
	private MockMvc mvc;

	@Autowired
	private ObjectMapper objectMapper;

	@Test
	public void TEST_LEGACY_차량Master_전송() throws Exception {

		String testURL = "/backend/v1/vehicle/ls";
		
		ReqVehicle c= new ReqVehicle().builder()
					   .sVrn("대구06도2227")
					   .sCarStatusCd("1")
					   .sTransComId("S000001")
					   .sErpId("999999")
					   .sUseTypeCd("2")
					   .sCarMade("190001")
					   .sLogiWeiCd("")
					   .sCarType("1")
					   .sCarLoanDate("20190501000000")
					   .sModDate("20190501121212")
					   .sCarModel("1")
					   .sRegDate("20190601010010")
					   .sCarCompany("세방자동차")
					   .sFuelTypeCd("")
					   .sLogiYctCd("2")
					   .nFuelEff(125.5f)	
					   .sSelfvrnCd("2")				   
					   .build();

		ReqVehicle c1= new ReqVehicle().builder()
		 	    .sVrn("경북07가1234")
				.sCarStatusCd("1")
				.sTransComId("S000001")
				.sErpId("999999")
				.sUseTypeCd("1")
				.sCarMade("197510")
				.sLogiWeiCd("1")
				.sCarType("1")
				.sCarLoanDate("20190501000000")
				.sModDate("20190501121212")
				.sCarModel("1")
				.sRegDate("20190601010010")
				.sCarCompany("1")
				.sLogiYctCd("1")
				.sSelfvrnCd("1")			
				.build();			   
			
		List<ReqVehicle> list = new ArrayList<ReqVehicle>();
		list.add(c);
		list.add(c1);
		
					   
		
		String memberToJson = objectMapper.writeValueAsString(list);

		ResultValue r = new ResultValue(ResultCode.SUCCESS, null);
		// String resul
		mvc.perform(post(testURL).content(memberToJson)
				.contentType(MediaType.APPLICATION_JSON_UTF8)
				.accept(MediaType.APPLICATION_JSON_UTF8))
				.andDo(print())
				.andExpect(status().isOk())
				.andExpect(MockMvcResultMatchers.jsonPath("$.resultCode").value("000"));

	}

	@Test
	public void TEST_LEGACY_운수사Master_전송() throws Exception {

		String testURL = "/backend/v1/transcom/ls";
		
		ReqTransCom c= new ReqTransCom().builder()
					   .sTransComId("S000001")
					   .sTransComNm("TEST운수사")					     
					   .sComStatusCd("1")
					   .sModDate("20190501121212")
					   .sRegDate("20190601010010")
					   .build();
						 
		List<ReqTransCom> list = new ArrayList<ReqTransCom>();
		list.add(c);
					   
		String memberToJson = objectMapper.writeValueAsString(list);

		ResultValue r = new ResultValue(ResultCode.SUCCESS, null);
		// String resul
		mvc.perform(post(testURL).content(memberToJson)
				.contentType(MediaType.APPLICATION_JSON_UTF8)
				.accept(MediaType.APPLICATION_JSON_UTF8))
				.andDo(print())
				.andExpect(status().isOk())
				.andExpect(MockMvcResultMatchers.jsonPath("$.resultCode").value("000"));

	}

	@Test
	public void TEST_LEGACY_운송정보_화물차_전송() throws Exception {

		String testURL = "/backend/v1/drivinginfo/ls";
		
		ReqLogisTransInfo c= new ReqLogisTransInfo().builder()
		.sVhclDispld("1")
		.sRegMod("20190501121212")		
		.sLoadage("1234567890.2")
		.cDate("20191201")
		.sTransTypeCd("1")			   
		.sComStatusCd("1")
		.sSelfvrnCd("2")
		.sVrn("부산99사7929")
		.sTransCompId("S000001")
		.sLogiWeiCd("1")
		.sErpId("326200")
		.sLogiYctCd("1")
		.nTonKm(new BigDecimal(1234.2))
		.nVol(new BigDecimal(1234.2))
		.nToe(new BigDecimal(4321.2))
		.nVolUnt("1")
		.build();
						 
		List<ReqLogisTransInfo> list = new ArrayList<ReqLogisTransInfo>();
		list.add(c);
					   
		String memberToJson = objectMapper.writeValueAsString(list);

		ResultValue r = new ResultValue(ResultCode.SUCCESS, null);
		// String resul
		mvc.perform(post(testURL).content(memberToJson)
				.contentType(MediaType.APPLICATION_JSON_UTF8)
				.accept(MediaType.APPLICATION_JSON_UTF8))
				.andDo(print())
				.andExpect(status().isOk())
				.andExpect(MockMvcResultMatchers.jsonPath("$.resultCode").value("000"));

	}


	@Test
	public void TEST_LEGACY_운송정보_철도_전송() throws Exception {

		String testURL = "/backend/v1/drivinginfo/ls";
		
		ReqLogisTransInfo c= new ReqLogisTransInfo().builder()
		.sVhclDispld("2")
		.sRegMod("20190501121212")		
		.sLoadage("112.2")
		.cDate("20191201")
		.sTransTypeCd("3")			   
		.sComStatusCd("1")
	//	.sSelfvrnCd("1")
		// .sVrn("경기1나22227")
		.sTransCompId("S000001")
		.sLogiWeiCd("1")
		.sErpId("320000")
		.sFare(new BigDecimal(10))
		.sDistance(new BigDecimal(10000))
		.sLogiYctCd("1")
		.build();
						 
		List<ReqLogisTransInfo> list = new ArrayList<ReqLogisTransInfo>();
		list.add(c);
					   
		String memberToJson = objectMapper.writeValueAsString(list);

		ResultValue r = new ResultValue(ResultCode.SUCCESS, null);
		// String resul
		mvc.perform(post(testURL).content(memberToJson)
				.contentType(MediaType.APPLICATION_JSON_UTF8)
				.accept(MediaType.APPLICATION_JSON_UTF8))
				.andDo(print())
				.andExpect(status().isOk())
				.andExpect(MockMvcResultMatchers.jsonPath("$.resultCode").value("000"));

	}

	@Test
	public void TEST_LEGACY_운송정보_선박_전송() throws Exception {

		String testURL = "/backend/v1/drivinginfo/ls";
		
		ReqLogisTransInfo c= new ReqLogisTransInfo().builder()
		.sTransTypeCd("2")			   
		.sVhclDispld("3")
		.sRegMod("20190501121212")		
		.sLoadage("112.2")
		.cDate("20191201")
		.sComStatusCd("1")
		.sSelfvrnCd("1")
		.sVrn("경기1나22227")
		.sTransCompId("S000001")
		.sLogiWeiCd("1")
		.sErpId("320000")
		.sFare(new BigDecimal(10))
		.sDistance(new BigDecimal(10000f))	
		.sLogiYctCd("1")
		.build();
						 
		List<ReqLogisTransInfo> list = new ArrayList<ReqLogisTransInfo>();
		list.add(c);
					   
		String memberToJson = objectMapper.writeValueAsString(list);

		ResultValue r = new ResultValue(ResultCode.SUCCESS, null);
		// String resul
		mvc.perform(post(testURL).content(memberToJson)
				.contentType(MediaType.APPLICATION_JSON_UTF8)
				.accept(MediaType.APPLICATION_JSON_UTF8))
				.andDo(print())
				.andExpect(status().isOk())
				.andExpect(MockMvcResultMatchers.jsonPath("$.resultCode").value("000"));

	}
	

}