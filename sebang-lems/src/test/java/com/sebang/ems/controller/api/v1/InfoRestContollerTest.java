package com.sebang.ems.controller.api.v1;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.sebang.ems.EmsApplication;
import com.sebang.ems.config.MockJwt;
import com.sebang.ems.model.Employee;
import com.sebang.ems.model.Dto.ReqCefMod;
import com.sebang.ems.model.Dto.ReqCorpInfo;
import com.sebang.ems.model.Dto.ReqDeptMap;
import com.sebang.ems.model.Dto.ReqEngCef;
import com.sebang.ems.model.Dto.ReqEngCefEtc;
import com.sebang.ems.model.Dto.ReqEngPoint;
import com.sebang.ems.model.Dto.ReqSite;
import com.sebang.ems.model.Dto.ReqSunPower;

import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.MOCK, classes = { EmsApplication.class,
		ObjectMapper.class })
@AutoConfigureMockMvc(secure = false)
@ContextConfiguration(classes = EmsApplication.class)
@ActiveProfiles("test")
@EnableWebMvc
public class InfoRestContollerTest extends MockJwt {
	@Autowired
	private MockMvc mvc;

	@Autowired
	private ObjectMapper objectMapper;


	@BeforeClass
    public static void beforeClass_createJwtToken() throws Exception {
		createToken();
    }

	@Test
	public void TEST_법인관리_저장() throws Exception {

		String testURL = "/api/v1/info/511/dtl";
		ReqCorpInfo c = new ReqCorpInfo();
		c.setSCorpNm("세방");
		c.setSCorpId("7000000");
		c.setSCorpRegNum("111-11-0002");
		c.setSOwner("홍길동");
		c.setSCorpAddr("서울");
		c.setSCorpTel("111-111-111");
		c.setSCorpCat("물류");

		String memberToJson = objectMapper.writeValueAsString(c);

		mvc.perform(post(testURL).header("jwt-header",JWT_TOKEN)
				.content(memberToJson)
				.contentType(MediaType.APPLICATION_JSON_UTF8)
				.accept(MediaType.APPLICATION_JSON_UTF8)).andExpect(status().isOk());

	}

	@Test
	public void TEST_사업장관리_등록_수정() throws Exception {
		String testURL = "/api/v1/info/521/dtl";
		ReqSite c = ReqSite.builder().idSite(17l).sSiteId("00003").sSiteNm("강남사업부").sSiteTel("123-3456").cDelYn("Y")
				.sSitePartyCd("1").cCloseYear("2025").build();

		String memberToJson = objectMapper.writeValueAsString(c);

		mvc.perform(post(testURL).header("jwt-header",JWT_TOKEN)
				.content(memberToJson).contentType(MediaType.APPLICATION_JSON_UTF8)
				.accept(MediaType.APPLICATION_JSON_UTF8)).andExpect(status().isOk())
				.andExpect(MockMvcResultMatchers.jsonPath("$.resultCode").value("000"));
	}

	@Test
	public void TEST_사업장관리_목록_삭제() throws Exception {
		String testURL = "/api/v1/info/521/ls?idSites=1,2";

		mvc.perform(delete(testURL).header("jwt-header", JWT_TOKEN).contentType(MediaType.APPLICATION_JSON_UTF8)
				.accept(MediaType.APPLICATION_JSON_UTF8)).andExpect(status().isOk())
				.andExpect(MockMvcResultMatchers.jsonPath("$.resultCode").value("000"));
	}

	@Test
	public void TEST_조직목록_조회() throws Exception {
		String testURL = "/api/v1/info/531/ls?idSite=0";
	
		mvc.perform(get(testURL).header("jwt-header",JWT_TOKEN)
				.contentType(MediaType.APPLICATION_JSON_UTF8)
				.accept(MediaType.APPLICATION_JSON_UTF8)).andExpect(status().isOk())
				.andDo(print())
				.andExpect(MockMvcResultMatchers.jsonPath("$.resultCode").value("000"));
	}

	@Test
	public void TEST_조직목록_등록_수정() throws Exception {
		String testURL = "/api/v1/info/531/ls";
		
		ReqDeptMap c = ReqDeptMap.builder()
								.idGasDeptMapping(242l)
								.idSite(0l)
								.idErpDept(1l).build();
	//	ReqDeptMap c1 = ReqDeptMap.builder().idSite(2l).idErpDept(100l).build();


		List list = new ArrayList<ReqDeptMap>();
		list.add(c);
	//	list.add(c1);
		

		String memberToJson = objectMapper.writeValueAsString(list);

	//	ResultValue r = new ResultValue(ResultCode.SUCCESS, null);
		mvc.perform(post(testURL).header("jwt-header",JWT_TOKEN)
				.content(memberToJson).contentType(MediaType.APPLICATION_JSON_UTF8)
				.accept(MediaType.APPLICATION_JSON_UTF8)).andExpect(status().isOk())
				.andExpect(MockMvcResultMatchers.jsonPath("$.resultCode").value("000"));
	}

	@Test
	public void TEST_조직관리_하위_조직_삭제() throws Exception {
		String testURL = "/api/v1/info/531/ls/sub?1=1,2&2=2";

		mvc.perform(delete(testURL)
				.header("jwt-header", JWT_TOKEN)
				.contentType(MediaType.APPLICATION_JSON_UTF8)
				.accept(MediaType.APPLICATION_JSON_UTF8))
				.andExpect(status().isOk())
				.andExpect(MockMvcResultMatchers.jsonPath("$.resultCode").value("000"));
	}

	@Test
	public void TEST_조직관리_하위_조직_삭제_Invalid() throws Exception {

		String invlidURL = "/api/v1/info/531/ls/sub?1=1,2&2=2&a=b";

		mvc.perform(delete(invlidURL).header("jwt-header", JWT_TOKEN).contentType(MediaType.APPLICATION_JSON_UTF8)
				.accept(MediaType.APPLICATION_JSON_UTF8))
				.andDo(print())
				.andExpect(status().isOk())
				.andExpect(MockMvcResultMatchers.jsonPath("$.resultCode").value("100"));
	}

	@Test
	public void TEST_사용자관리_목록_등록_수정() throws Exception {
		String testURL = "/api/v1/info/541/ls";
		ArrayList<Employee> list = new ArrayList<Employee>();
		Employee c = Employee.builder()
					.sEmpId("99999998")
					.sEmpNm("flashman")
					.sEmpPwd("1234")
					.sEmpMail("flashman@gmail.com")
					.cAuthCode("IUSR")
					.build();
		list.add(c);
		String memberToJson = objectMapper.writeValueAsString(list);

	//	ResultValue r = new ResultValue(ResultCode.SUCCESS, null);
		// String resul
		mvc.perform(post(testURL).header("jwt-header",JWT_TOKEN)
				.content(memberToJson).contentType(MediaType.APPLICATION_JSON_UTF8)
				.accept(MediaType.APPLICATION_JSON_UTF8))
				.andDo(print())
				.andExpect(status().isOk())
				.andExpect(MockMvcResultMatchers.jsonPath("$.resultCode").value("000"));
	}

	@Test
	public void TEST_사용자관리_암호수정_수정() throws Exception {
		String testURL = "/api/v1/info/5411/dtl";
		//ArrayList<Employee> list = new ArrayList<Employee>();
		Employee c = Employee.builder()
					.sEmpId("99999997")
					.sEmpNm("flashman")
					.sEmpPwd("5678")
					.sEmpMail("flashman@gmail.com")
					.sNewPwd("5678")
					.build();
	//	list.add(c);
		String memberToJson = objectMapper.writeValueAsString(c);

	//	ResultValue r = new ResultValue(ResultCode.SUCCESS, null);
		// String resul
		mvc.perform(post(testURL).header("jwt-header",JWT_TOKEN)
				.content(memberToJson).contentType(MediaType.APPLICATION_JSON_UTF8)
				.accept(MediaType.APPLICATION_JSON_UTF8))
				.andDo(print())
				.andExpect(status().isOk())
				.andExpect(MockMvcResultMatchers.jsonPath("$.resultCode").value("000"));
	}

	@Test
	public void TEST_사용자별_권한_메뉴_목록_등록() throws Exception {
		// String testURL= "/api/v1/info/591/menu/auth";
		// Employee c = Employee.builder()
		// .sEmpId("99999999")
		// .sEmpNm("flashman")
		// .sEmpPwd("1234")
		// .sEmpMail("changmanyi@gmail2.com")
		// .cAuthCode("IUSR")
		// .build();

		// String memberToJson = objectMapper.writeValueAsString(c);

		// ResultValue r= new ResultValue(ResultCode.SUCCESS, null);
		// //String resul
		// mvc.perform(post(testURL)
		// .header("jwt-header",
		// "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJyb2xlIjoiU1lTIiwiaXNzIjoic2ViYW5nIiwiaWQiOiJuYXJ1dG8iLCJleHAiOjE1NzAyODUyMTd9.CjIeoJ4S_7gQ__TFsJmxmrpUCY7wy2a1FSz7VrzA22Y")
		// .content(memberToJson)
		// .contentType(MediaType.APPLICATION_JSON_UTF8)
		// .accept(MediaType.APPLICATION_JSON_UTF8))
		// .andExpect(status().isOk())
		// .andExpect(MockMvcResultMatchers.jsonPath("$.resultCode").value("000"));

	}

	@Test
	public void TEST_사용자_암호_초기화_및_변경() throws Exception {
		String testURL = "/api/v1/info/591/ls/pw/init";
		Employee c = Employee.builder().sEmpId("99999997")
							.sEmpNm("flashman")
				//			.sEmpPwd("1234")
							.sEmpMail("flashman@gmail2.com")
							.cAuthCode("IUSR").build();
		ArrayList<Employee> list = new ArrayList<Employee>();
		list.add(c);
		String memberToJson = objectMapper.writeValueAsString(list);

	//	ResultValue r = new ResultValue(ResultCode.SUCCESS, null);
		// String resul
		mvc.perform(post(testURL).header("jwt-header",JWT_TOKEN)
				.content(memberToJson).contentType(MediaType.APPLICATION_JSON_UTF8)
				.accept(MediaType.APPLICATION_JSON_UTF8)).andExpect(status().isOk())
				.andExpect(MockMvcResultMatchers.jsonPath("$.resultCode").value("000"));
	}

	@Test
	public void TEST_배출계수관리_직접배출_에너지_목록_저장() throws Exception {
		String testURL = "/api/v1/info/5511/de/ls";
		ReqEngCef c = ReqEngCef.builder()
						.idEngCef(1L)
						.cTier("A")					
						.sCefCd("1") // 직접배출
						.sEngCd("1")
						.sMrvengCd("33")
						.sEmtCd("1")
						.sInUntCd("3")
						.sCalcUntCd("3")
						.sEmiCd("1002")
						.cDelYn("N")
						.build();
		List<ReqEngCef> list = new ArrayList<ReqEngCef>();
		list.add(c);
				
		String memberToJson = objectMapper.writeValueAsString(list);
		mvc.perform(post(testURL).header("jwt-header",JWT_TOKEN)
				.content(memberToJson).contentType(MediaType.APPLICATION_JSON_UTF8)
				.accept(MediaType.APPLICATION_JSON_UTF8)).andExpect(status().isOk())
				.andDo(print())
				.andExpect(MockMvcResultMatchers.jsonPath("$.resultCode").value("000")				
				);
	}

	@Test
	public void TEST_배출계수관리_직접배출_배출계수_목록_저장() throws Exception {
		String testURL = "/api/v1/info/5511/def/ls";
		ReqCefMod c = ReqCefMod.builder()
            .idEngCef(1l)
            .nCo2Cef(new BigDecimal(5.0))
            .nCh4Cef(new BigDecimal(5.0))
            .nN2oCef(new BigDecimal(5.0))
            .nModCal(new BigDecimal(5.0))
            .cPer(new BigDecimal(2.0))
			.build();

		List<ReqCefMod> list = new ArrayList<ReqCefMod>();
		list.add(c);
		
		String memberToJson = objectMapper.writeValueAsString(list);
		mvc.perform(post(testURL).header("jwt-header",JWT_TOKEN)
				.content(memberToJson).contentType(MediaType.APPLICATION_JSON_UTF8)
				.accept(MediaType.APPLICATION_JSON_UTF8)).andExpect(status().isOk())
				.andDo(print())
				.andExpect(MockMvcResultMatchers.jsonPath("$.resultCode").value("000"));
	}

	@Test
	public void TEST_배출계수관리_간접배출_에너지_목록_저장() throws Exception {
		String testURL = "/api/v1/info/5521/ie/ls";
		ReqEngCef c = ReqEngCef.builder()
						.idEngCef(2L)
						.cTier("B")					
						.sCefCd("2")   // 간접배출
						.sEngCd("6")
						.sMrvengCd("33")
						.sEmtCd("3")
						.sInUntCd("3")
						.sCalcUntCd("3")
						.sEmiCd("6001")
						.cDelYn("N")
						.build();
		List<ReqEngCef> list = new ArrayList<ReqEngCef>();
		list.add(c);
				
		String memberToJson = objectMapper.writeValueAsString(list);
		mvc.perform(post(testURL).header("jwt-header",JWT_TOKEN)
				.content(memberToJson).contentType(MediaType.APPLICATION_JSON_UTF8)
				.accept(MediaType.APPLICATION_JSON_UTF8)).andExpect(status().isOk())
				.andDo(print())
				.andExpect(MockMvcResultMatchers.jsonPath("$.resultCode").value("000")				
				);
	}

	@Test
	public void TEST_배출계수관리_간접배출_배출계수_목록_저장() throws Exception {
		String testURL = "/api/v1/info/5521/ief/ls";
		ReqCefMod c = ReqCefMod.builder()
            .idEngCef(2l)
            .nCo2Cef(new BigDecimal(5.0))
            .nCh4Cef(new BigDecimal(5.0))
            .nN2oCef(new BigDecimal(5.0))
            .nModCal(new BigDecimal(5.0))
            .cPer(new BigDecimal(2.0))	
			.build();

		List<ReqCefMod> list = new ArrayList<ReqCefMod>();
		list.add(c);
		
		String memberToJson = objectMapper.writeValueAsString(list);
		mvc.perform(post(testURL).header("jwt-header",JWT_TOKEN)
				.content(memberToJson).contentType(MediaType.APPLICATION_JSON_UTF8)
				.accept(MediaType.APPLICATION_JSON_UTF8)).andExpect(status().isOk())
				.andDo(print())
				.andExpect(MockMvcResultMatchers.jsonPath("$.resultCode").value("000"));
	}

	@Test
	public void TEST_배출계수관리_기타온실가스_배출_목록_조회() throws Exception {
		String testURL = "/api/v1/info/553/etc/emi/ls";
				
		mvc.perform(get(testURL)
				.header("jwt-header",JWT_TOKEN)
				//.content(memberToJson)
				.contentType(MediaType.APPLICATION_JSON_UTF8)
				.accept(MediaType.APPLICATION_JSON_UTF8)).andExpect(status().isOk())
				.andDo(print())
				.andExpect(MockMvcResultMatchers.jsonPath("$.resultCode").value("000")				
				);
	}


	@Test
	public void TEST_배출계수관리_기타온실가스_배출_목록_저장() throws Exception {
		String testURL = "/api/v1/info/553/etc/emi/ls";
		ReqEngCefEtc c = ReqEngCefEtc.builder()
						.idEngCef(3L)						
						.sCefCd("3")   // 간접배출
						.sInUntCd("4")
						.sEmiCd("7001")
						.sOthCefNm("CO2가스치환용")
						.sOthPurpose("제품내 산소치환용")
						.cDelYn("N")						
						.build();
		List<ReqEngCefEtc> list = new ArrayList<ReqEngCefEtc>();
		list.add(c);
				
		String memberToJson = objectMapper.writeValueAsString(list);
		mvc.perform(post(testURL).header("jwt-header",JWT_TOKEN)
				.content(memberToJson).contentType(MediaType.APPLICATION_JSON_UTF8)
				.accept(MediaType.APPLICATION_JSON_UTF8)).andExpect(status().isOk())
				.andDo(print())
				.andExpect(MockMvcResultMatchers.jsonPath("$.resultCode").value("000")				
				);
	}

	@Test
	public void TEST_배출계수관리_기타온실가스_배출_목록_삭제() throws Exception {
		String testURL = "/api/v1/info/553/etc/emi/ls?idList=3";
		mvc.perform(delete(testURL).header("jwt-header",JWT_TOKEN)
				//.content(memberToJson)
				.contentType(MediaType.APPLICATION_JSON_UTF8)
				.accept(MediaType.APPLICATION_JSON_UTF8)).andExpect(status().isOk())
				.andDo(print())
				.andExpect(MockMvcResultMatchers.jsonPath("$.resultCode").value("000"));
	}

	@Test
	public void TEST_배출계수관리_온실가스_구성비율_목록_조회() throws Exception {
		String testURL = "/api/v1/info/553/etc/gr/ls?idEngCef=3";
		
		mvc.perform(get(testURL).header("jwt-header",JWT_TOKEN)
				.contentType(MediaType.APPLICATION_JSON_UTF8)
				.accept(MediaType.APPLICATION_JSON_UTF8)).andExpect(status().isOk())
				.andDo(print())
				.andExpect(MockMvcResultMatchers.jsonPath("$.resultCode").value("000"));
	}

	@Test
	public void TEST_배출계수관리_온실가스_구성비율_목록_저장() throws Exception {
		String testURL = "/api/v1/info/553/etc/gr/ls";
		ReqCefMod c = ReqCefMod.builder()
            .idEngCef(3l)
			.cPer(new BigDecimal(13))
			.sGhgCd("4010")
			.build();

		List<ReqCefMod> list = new ArrayList<ReqCefMod>();
		list.add(c);
		
		String memberToJson = objectMapper.writeValueAsString(list);
		mvc.perform(post(testURL).header("jwt-header",JWT_TOKEN)
				.content(memberToJson).contentType(MediaType.APPLICATION_JSON_UTF8)
				.accept(MediaType.APPLICATION_JSON_UTF8)).andExpect(status().isOk())
				.andDo(print())
				.andExpect(MockMvcResultMatchers.jsonPath("$.resultCode").value("000"));
	}

	@Test
	public void TEST_TEST_배출계수관리_온실가스_구성비율_목록_삭제() throws Exception {
		String testURL = "/api/v1/info/553/etc/gr/ls?idList=2,3,27";
		mvc.perform(delete(testURL).header("jwt-header",JWT_TOKEN)
				//.content(memberToJson)
				.contentType(MediaType.APPLICATION_JSON_UTF8)
				.accept(MediaType.APPLICATION_JSON_UTF8)).andExpect(status().isOk())
				.andDo(print())
				.andExpect(MockMvcResultMatchers.jsonPath("$.resultCode").value("000"));
	}

	@Test
	public void TEST_배출시설_목록_조회() throws Exception {
		String testURL = "/api/v1/info/571/ls";
		
		
		mvc.perform(get(testURL).header("jwt-header",JWT_TOKEN)
				.contentType(MediaType.APPLICATION_JSON_UTF8)
				.accept(MediaType.APPLICATION_JSON_UTF8))
				.andExpect(status().isOk())
				.andDo(print())
				.andExpect(MockMvcResultMatchers.jsonPath("$.resultCode").value("000"));
	}

	@Test
	public void TEST_배출시설_상세조회() throws Exception {
		String testURL = "/api/v1/info/571/dtl?idSite=3&idEngPoint=2";
		
		
		mvc.perform(get(testURL).header("jwt-header",JWT_TOKEN)
				.contentType(MediaType.APPLICATION_JSON_UTF8)
				.accept(MediaType.APPLICATION_JSON_UTF8))
				.andExpect(status().isOk())
				.andDo(print())
				.andExpect(MockMvcResultMatchers.jsonPath("$.resultCode").value("000"));
	}



	@Test
	public void TEST_배출시설_상제정보_수정() throws Exception {
		String testURL = "/api/v1/info/571/dtl";
		ReqEngPoint c = ReqEngPoint.builder()
			.idEngPoint(151l)
			.sPointNm("세방배출시설_TEST")
			.cPointYn("Y")
			.cDelYn("N")
		//	.sPointId("00001P0005")
			.idSite(16l)
			.idEngCef(3l)
			.sLogiTypeCd("1")
			.sBaseUnt("1")
			.sBaseVal(10f)
			.sFuelEff(123.2f)
			.build();

		String memberToJson = objectMapper.writeValueAsString(c);

		// ResultValue r = new ResultValue(ResultCode.SUCCESS, null);
		// String resul
		mvc.perform(post(testURL).header("jwt-header",JWT_TOKEN)
				.content(memberToJson)
				.contentType(MediaType.APPLICATION_JSON_UTF8)
				.accept(MediaType.APPLICATION_JSON_UTF8))
				.andExpect(status().isOk())
				.andDo(print())
				.andExpect(MockMvcResultMatchers.jsonPath("$.resultCode").value("000"));
	}


	@Test
	public void TEST_태양광_발전량_조회() throws Exception {
		String testURL = "/api/v1/info/581/ls";
		mvc.perform(get(testURL).header("jwt-header",JWT_TOKEN)
				.contentType(MediaType.APPLICATION_JSON_UTF8)
				.accept(MediaType.APPLICATION_JSON_UTF8))
				.andExpect(status().isOk())
				.andDo(print())
				.andExpect(MockMvcResultMatchers.jsonPath("$.resultCode").value("000"));
	}

	@Test
	public void TEST_태양광_발전량_등록() throws Exception {
		String testURL = "/api/v1/info/581/ls";

		ReqSunPower c = ReqSunPower.builder()
		.cYear("2019")
		.cMon("11")
		.nPower(12312)
		.nPoint("서울 강남")
		.build();

		List<ReqSunPower> list = new ArrayList<ReqSunPower>();
		list.add(c);
		
			
		String memberToJson = objectMapper.writeValueAsString(list);


		mvc.perform(post(testURL).header("jwt-header",JWT_TOKEN)
				.contentType(MediaType.APPLICATION_JSON_UTF8)
				.content(memberToJson)
				.accept(MediaType.APPLICATION_JSON_UTF8))
				.andExpect(status().isOk())
				.andDo(print())
				.andExpect(MockMvcResultMatchers.jsonPath("$.resultCode").value("000"));
	}

	@Test
	public void TEST_태양광_발전량_삭제() throws Exception {
		String testURL = "/api/v1/info/581/ls?idList=2";

		mvc.perform(delete(testURL).header("jwt-header",JWT_TOKEN)
				.contentType(MediaType.APPLICATION_JSON_UTF8)
				.accept(MediaType.APPLICATION_JSON_UTF8))
				.andExpect(status().isOk())
				.andDo(print())
				.andExpect(MockMvcResultMatchers.jsonPath("$.resultCode").value("000"));
	}

}