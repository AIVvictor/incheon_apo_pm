package com.sebang.ems.controller.api.v1;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.math.BigDecimal;

import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.sebang.ems.EmsApplication;
import com.sebang.ems.config.MockJwt;
import com.sebang.ems.domain.common.ResultCode;
import com.sebang.ems.domain.common.ResultValue;
import com.sebang.ems.model.Dto.ReqEngPointAmt;
import com.sebang.ems.model.Dto.ReqMonEngClose;

import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.MOCK, classes = { EmsApplication.class,
		ObjectMapper.class })
@AutoConfigureMockMvc(secure = false)
@ContextConfiguration(classes = EmsApplication.class)
@ActiveProfiles("test")
@EnableWebMvc
public class GhgRestContollerTest extends MockJwt{
	@Autowired
	private MockMvc mvc;

	@Autowired
	private ObjectMapper objectMapper;

	@BeforeClass
    public static void beforeClass_createJwtToken() throws Exception {
		createToken();
    }

	@Test
	public void TEST_사용실적_목록_저장() throws Exception {

		String testURL = "/api/v1/ghg/316/ls";
		
		ReqEngPointAmt c1= new ReqEngPointAmt().builder()
	                    .idEngPoint(1l)
	                    .cYear("2019")
	                    .cMon("12")
	                    .nUseAmt(new BigDecimal(1235)).build();
	
		ReqEngPointAmt c2= new ReqEngPointAmt().builder()
	                    .idEngPoint(1l)
	                    .cYear("2019")
	                    .cMon("12")
						.nUseAmt(new BigDecimal(1235)).build();
		ReqEngPointAmt[] c = new  ReqEngPointAmt[2];
		c[0] = c1;
		c[1] =c2;
		
		String memberToJson = objectMapper.writeValueAsString(c);

		ResultValue r = new ResultValue(ResultCode.SUCCESS, null);
		// String resul
		mvc.perform(post(testURL).header("jwt-header",this.JWT_TOKEN)
				.content(memberToJson)
				.contentType(MediaType.APPLICATION_JSON_UTF8)
				.accept(MediaType.APPLICATION_JSON_UTF8))
				.andDo(print())
				.andExpect(status().isOk());

	}

	@Test
	public void TEST_사용실적_목록_조회() throws Exception {

		String testURL = "/api/v1/ghg/316/ls?cYear=2019&cMon=01";
		
		

		// String resul
		mvc.perform(get(testURL).header("jwt-header",this.JWT_TOKEN)
				.contentType(MediaType.APPLICATION_JSON_UTF8)
				.accept(MediaType.APPLICATION_JSON_UTF8))
				.andDo(print())
				.andExpect(status().isOk());

	}

	
	@Test
	public void TEST_월마감_조회() throws Exception {

		String testURL = "/api/v1/ghg/315/ls?cYear=2019&cMon=11&idSite=3";
		
		// String resul
		mvc.perform(get(testURL).header("jwt-header",this.JWT_TOKEN)
				.contentType(MediaType.APPLICATION_JSON_UTF8)
				.accept(MediaType.APPLICATION_JSON_UTF8))
				.andDo(print())
				.andExpect(status().isOk());

	}

	@Test
	public void TEST_월마감_여부() throws Exception {

		String testURL = "/api/v1/ghg/315/close?cYear=2019&cMon=11";
		
		// String resul
		mvc.perform(get(testURL).header("jwt-header",this.JWT_TOKEN)
				.contentType(MediaType.APPLICATION_JSON_UTF8)
				.accept(MediaType.APPLICATION_JSON_UTF8))
				.andDo(print())
				.andExpect(status().isOk());

	}


	@Test
	public void TEST_월마감() throws Exception {

		String testURL = "/api/v1/ghg/315/close?cYear=2019&cMon=11&idSite=3";

		ReqMonEngClose c1= new ReqMonEngClose().builder()
		.cYear("2019")
		.cMon("12")
	//	.idSite(3l)
		.build();

		String memberToJson = objectMapper.writeValueAsString(c1);
		
		mvc.perform(post(testURL).header("jwt-header",this.JWT_TOKEN)
			.content(memberToJson)
			.contentType(MediaType.APPLICATION_JSON_UTF8)
			.accept(MediaType.APPLICATION_JSON_UTF8))
			.andDo(print())
			.andExpect(status().isOk());
	}

	@Test
	public void TEST_월마감_취소() throws Exception {

		String testURL = "/api/v1/ghg/315/close/cancel";

		ReqMonEngClose c1= new ReqMonEngClose().builder()
		.cYear("2019")
		.cMon("12")
	//	.idSite(3l)
		.build();
		String memberToJson = objectMapper.writeValueAsString(c1);
	
		mvc.perform(post(testURL).header("jwt-header",this.JWT_TOKEN)
			.content(memberToJson)
			.contentType(MediaType.APPLICATION_JSON_UTF8)
			.accept(MediaType.APPLICATION_JSON_UTF8))
			.andDo(print())
			.andExpect(status().isOk());
	}

	@Test
	public void Test_목표대비_실적_분석() throws Exception{

		String testURL = "/api/v1/ghg/324/ls?cYear=2019";
		
		mvc.perform(get(testURL).header("jwt-header",this.JWT_TOKEN)
				.contentType(MediaType.APPLICATION_JSON_UTF8)
				.accept(MediaType.APPLICATION_JSON_UTF8))
				.andDo(print())
				.andExpect(status().isOk());

	}

	@Test
	public void TEST_원단위_분석() throws Exception {

		String testURL = "/api/v1/ghg/325/ls?cYear=2019";
		
		mvc.perform(get(testURL).header("jwt-header",this.JWT_TOKEN)
				.contentType(MediaType.APPLICATION_JSON_UTF8)
				.accept(MediaType.APPLICATION_JSON_UTF8))
				.andDo(print())
				.andExpect(status().isOk());

	}


	@Test
	public void TEST_원단위_사이트별_분석() throws Exception {

		String testURL = "/api/v1/ghg/325/ls?cYear=2019&idSite=3";
		
		mvc.perform(get(testURL).header("jwt-header",this.JWT_TOKEN)
				.contentType(MediaType.APPLICATION_JSON_UTF8)
				.accept(MediaType.APPLICATION_JSON_UTF8))
				.andDo(print())
				.andExpect(status().isOk());

	}




}