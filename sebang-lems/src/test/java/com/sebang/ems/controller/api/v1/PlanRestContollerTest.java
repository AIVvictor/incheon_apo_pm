package com.sebang.ems.controller.api.v1;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.sebang.ems.EmsApplication;
import com.sebang.ems.config.MockJwt;
import com.sebang.ems.domain.common.ResultCode;
import com.sebang.ems.domain.common.ResultValue;
import com.sebang.ems.model.Dto.ReqMrvPointGoal;
import com.sebang.ems.model.Dto.ReqSiteMonGoal;
import com.sebang.ems.model.Dto.ReqSiteYearGoal;

import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.MOCK, classes = { EmsApplication.class,
		ObjectMapper.class })
@AutoConfigureMockMvc(secure = false)
@ContextConfiguration(classes = EmsApplication.class)
@ActiveProfiles("test")
@EnableWebMvc
public class PlanRestContollerTest extends MockJwt{
	
	@Autowired
	private MockMvc mvc;

	@Autowired
	private ObjectMapper objectMapper;

	
	@BeforeClass
    public static void beforeClass_createJwtToken() throws Exception {
		createToken();
    }

	@Test
	public void TEST_정부목표_전사목표_목록_저장() throws Exception {

		String testURL = "/api/v1/plan/212/gov/ls";
		
		ReqMrvPointGoal c1= new ReqMrvPointGoal().builder()
	                    .cYear("2019")
						.nEngGoal(new BigDecimal(123456f))
						.nGasGoal(new BigDecimal(45678))
						.build();

		String memberToJson = objectMapper.writeValueAsString(c1);
		
		ResultValue r = new ResultValue(ResultCode.SUCCESS, null);
		// String resul
		mvc.perform(post(testURL).header("jwt-header",JWT_TOKEN)
				.content(memberToJson).contentType(MediaType.APPLICATION_JSON_UTF8)
				.accept(MediaType.APPLICATION_JSON_UTF8)).andExpect(status().isOk());

	}

	@Test
	public void TEST_정부목표_전사목표_목록_삭제() throws Exception {

		String testURL = "/api/v1/plan/212/gov/ls?cYear=2018";

		System.out.println(JWT_TOKEN);

		mvc.perform(delete(testURL).header("jwt-header",JWT_TOKEN)
		.contentType(MediaType.APPLICATION_JSON_UTF8)
		.accept(MediaType.APPLICATION_JSON_UTF8)).andExpect(status().isOk());

	}

	@Test
	public void TEST_사내목표_목록_저장() throws Exception {
		String testURL = "/api/v1/plan/213/ghg/ls";

		ReqSiteMonGoal smg = new ReqSiteMonGoal().builder()
								.idEngPoint(6l)								
								.m1(new BigDecimal(100))
								.m2(new BigDecimal(1))
								.m3(new BigDecimal(1))
								.m4(new BigDecimal(1))
								.m5(new BigDecimal(1))
								.m6(new BigDecimal(1))
								.m7(new BigDecimal(1))
								.m8(new BigDecimal(1))
								.m9(new BigDecimal(1))
								.m10(new BigDecimal(1))
								.m11(new BigDecimal(1))
								.m12(new BigDecimal(1))								
								.build();
		
		List<ReqSiteMonGoal> goals = new ArrayList<ReqSiteMonGoal>();
		goals.add(smg);

		ReqSiteYearGoal c1= new ReqSiteYearGoal().builder()
						.cYear("2019")
						.idSite(2l)
						.nYearEngGoal(new BigDecimal(10))
						.nYearGasGoal(new BigDecimal(20))
						.goals(goals)	
						.build();

		
		// ReqSiteYearGoal c2= new ReqSiteYearGoal().builder()
		// 				.cYear("2019")						 
		// 				.goals(goals) 
		// 				.build();		
		
		ReqSiteYearGoal[] c = new ReqSiteYearGoal[1];
		c[0] = c1;
		//c[1] = c2;

		String memberToJson = objectMapper.writeValueAsString(c);
		System.out.println(memberToJson);
				
		mvc.perform(post(testURL).header("jwt-header",JWT_TOKEN)
				.content(memberToJson).contentType(MediaType.APPLICATION_JSON_UTF8)
				.accept(MediaType.APPLICATION_JSON_UTF8)).andExpect(status().isOk());
	}

	
	@Test
	public void TEST_BAU분석() throws Exception {

		String testURL = "/api/v1/plan/211/ls?cYear=2018&cYearEnd=2019";

		mvc.perform(get(testURL).header("jwt-header",JWT_TOKEN)
		.contentType(MediaType.APPLICATION_JSON_UTF8)
		.accept(MediaType.APPLICATION_JSON_UTF8))
		.andDo(print())
		.andExpect(status().isOk());

	}
}