package com.sebang.ems.controller.api.v1;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.sebang.ems.EmsApplication;
import com.sebang.ems.config.MockJwt;
import com.sebang.ems.domain.common.ResultCode;
import com.sebang.ems.domain.common.ResultValue;

import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.MOCK, classes = { EmsApplication.class,
		ObjectMapper.class })
@AutoConfigureMockMvc(secure = false)
@ContextConfiguration(classes = EmsApplication.class)
@ActiveProfiles("test")
@EnableWebMvc
public class HomeRestContollerTest extends MockJwt{
	
	@Autowired
	private MockMvc mvc;

	@Autowired
	private ObjectMapper objectMapper;

	
	@BeforeClass
    public static void beforeClass_createJwtToken() throws Exception {
		createToken();
    }

	@Test
	public void TEST_온실가스_배출실적() throws Exception {

		String testURL = "/api/v1/home/010/ghg/ls?cYear=2019,2018,2017";
		
		
		ResultValue r = new ResultValue(ResultCode.SUCCESS, null);
		mvc.perform(get(testURL).header("jwt-header",JWT_TOKEN)
				.contentType(MediaType.APPLICATION_JSON_UTF8)
				.accept(MediaType.APPLICATION_JSON_UTF8))
				.andDo(print())
				.andExpect(status().isOk());

	}

	@Test
	public void TEST_에너지_사용실적() throws Exception {

		String testURL = "/api/v1/home/010/eng/ls?cYear=2019,2018,2017";
		
		
		ResultValue r = new ResultValue(ResultCode.SUCCESS, null);
		mvc.perform(get(testURL).header("jwt-header",JWT_TOKEN)
				.contentType(MediaType.APPLICATION_JSON_UTF8)
				.accept(MediaType.APPLICATION_JSON_UTF8))
				.andDo(print())
				.andExpect(status().isOk());

	}

	@Test
	public void TEST_온실가스_에너지현황() throws Exception {

		String testURL = "/api/v1/home/010/ge/dtl?cYear=2019";
		
		
		ResultValue r = new ResultValue(ResultCode.SUCCESS, null);
		mvc.perform(get(testURL).header("jwt-header",JWT_TOKEN)
				.contentType(MediaType.APPLICATION_JSON_UTF8)
				.accept(MediaType.APPLICATION_JSON_UTF8))
				.andDo(print())
				.andExpect(status().isOk());

	}

	@Test
	public void TEST_태양광_발전량() throws Exception {

		String testURL = "/api/v1/home/010/soleng/ls?cYear=2019,2020";
		
		ResultValue r = new ResultValue(ResultCode.SUCCESS, null);
		mvc.perform(get(testURL).header("jwt-header",JWT_TOKEN)
				.contentType(MediaType.APPLICATION_JSON_UTF8)
				.accept(MediaType.APPLICATION_JSON_UTF8))
				.andDo(print())
				.andExpect(status().isOk());

	}

}