package com.sebang.ems.controller.api.v1;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.sebang.ems.EmsApplication;
import com.sebang.ems.config.MockJwt;
import com.sebang.ems.model.Do.TLogisTransMonInfo;
import com.sebang.ems.model.Dto.ReqVehicle;

import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.MOCK, classes = { EmsApplication.class,
		ObjectMapper.class })
@AutoConfigureMockMvc(secure = false)
@ContextConfiguration(classes = EmsApplication.class)
@ActiveProfiles("test")
@EnableWebMvc
public class LemRestContollerTest extends MockJwt {
	@Autowired
	private MockMvc mvc;

	@Autowired
	private ObjectMapper objectMapper;

	@BeforeClass
    public static void beforeClass_createJwtToken() throws Exception {
		createToken();
    }

	@Test
	public void TEST_운송정보_조회() throws Exception {
		String testURL = "/api/v1/lem/411/ls?cYear=2019&cMon=04&transTypeCd=1";	

	 mvc.perform(get(testURL).header("jwt-header",JWT_TOKEN)
			//.content(memberToJson)
			.contentType(MediaType.APPLICATION_JSON_UTF8)
			.accept(MediaType.APPLICATION_JSON_UTF8))
			.andDo(print())
			.andExpect(status().isOk());

	}

	@Test
	public void TEST_운송정보_등록수정_화물차_저장() throws Exception {
		String testURL = "/api/v1/lem/411/ls";

	TLogisTransMonInfo c= new TLogisTransMonInfo();
	c.setIdLogisTransMonInfo(1l);
	c.setDistanceSum(new BigDecimal(123456)); // 영차 운송거리
	c.setVacantDistanceSum(new BigDecimal(654321)); // 공차 운송거리
	c.setFareSum(10000l); // 운임료
	c.setLoadageSum(new BigDecimal(222222)); // 적제량

	List<TLogisTransMonInfo> list = new ArrayList<TLogisTransMonInfo>();
	list.add(c);
		String memberToJson = objectMapper.writeValueAsString(list);

	 mvc.perform(post(testURL).header("jwt-header",JWT_TOKEN)
	 		.content(memberToJson).contentType(MediaType.APPLICATION_JSON_UTF8)
				.accept(MediaType.APPLICATION_JSON_UTF8)).andExpect(status().isOk());

	}

	@Test
	public void TEST_운송정보_등록수정_화물차_저장_운송거리제로() throws Exception {
		String testURL = "/api/v1/lem/411/ls";

	TLogisTransMonInfo c= new TLogisTransMonInfo();
	c.setIdLogisTransMonInfo(1l);
	c.setDistanceSum(BigDecimal.ZERO); // 영차 운송거리
	c.setVacantDistanceSum(new BigDecimal(654321)); // 공차 운송거리
	c.setFareSum(10000l); // 운임료
	c.setLoadageSum(new BigDecimal(222222)); // 적제량

	List<TLogisTransMonInfo> list = new ArrayList<TLogisTransMonInfo>();
	list.add(c);
		String memberToJson = objectMapper.writeValueAsString(list);

	 mvc.perform(post(testURL).header("jwt-header",JWT_TOKEN)
	 		.content(memberToJson).contentType(MediaType.APPLICATION_JSON_UTF8)
				.accept(MediaType.APPLICATION_JSON_UTF8))
				.andExpect(status().isOk());
	}

	@Test
	public void TEST_차량정보_목록_조회() throws Exception {
		String testURL = "/api/v1/lem/461/ls?cYear=2019&transTypeCd=1&idSite=5";	

	 mvc.perform(get(testURL).header("jwt-header",JWT_TOKEN)
			//.content(memberToJson)
			.contentType(MediaType.APPLICATION_JSON_UTF8)
			.accept(MediaType.APPLICATION_JSON_UTF8))
			.andDo(print())
			.andExpect(status().isOk());

	}



	 @Test
	 public void TEST_차량정보_목록_저장() throws Exception {
	 	String testURL = "/api/v1/lem/461/ls";
	
	 	ReqVehicle c= new ReqVehicle().builder()
						.idVehicle(6l)
						.sLineCd("2")
						.nFuelEff(30.2f)
						.build();
				   
		List<ReqVehicle> list = new ArrayList<ReqVehicle>();
		list.add(c);
	 	String memberToJson = objectMapper.writeValueAsString(list);

		 mvc.perform(post(testURL).header("jwt-header",JWT_TOKEN)
		 		.content(memberToJson).contentType(MediaType.APPLICATION_JSON_UTF8)
	 			.accept(MediaType.APPLICATION_JSON_UTF8)).andExpect(status().isOk());

	 }

	 @Test
	 public void TEST_차량정보_목록_삭제() throws Exception {
	 	String testURL = "/api/v1/lem/461/ls?idVehicle=1";
	
	 	 ReqVehicle c= new ReqVehicle().builder()
		 				.idVehicle(1l)
		 				.sLogiYctCd("2")
		 				.nFuelEff(12.3f)
	 	 			   .build();
				   
	
	 	String memberToJson = objectMapper.writeValueAsString(c);

		 mvc.perform(delete(testURL).header("jwt-header",JWT_TOKEN)
		 		.content(memberToJson).contentType(MediaType.APPLICATION_JSON_UTF8)
	 			.accept(MediaType.APPLICATION_JSON_UTF8)).andExpect(status().isOk());

	 }

	 @Test
	 public void TEST_효율화_조회() throws Exception {
	 	String testURL = "/api/v1/lem/452/ls?cYear=2019&sLineCd=2&carType=1&idSite=3&sLogiWeiCd=1";
	
	 	 
		 mvc.perform(get(testURL)
		 		.header("jwt-header",JWT_TOKEN)
				.contentType(MediaType.APPLICATION_JSON_UTF8)
				.accept(MediaType.APPLICATION_JSON_UTF8))
				.andDo(print())
				.andExpect(status().isOk());

	 }
	
	

}