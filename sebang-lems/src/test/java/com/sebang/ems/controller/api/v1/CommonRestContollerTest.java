package com.sebang.ems.controller.api.v1;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.sebang.ems.EmsApplication;
import com.sebang.ems.config.MockJwt;
import com.sebang.ems.domain.common.ResultCode;
import com.sebang.ems.domain.common.ResultValue;
import com.sebang.ems.model.Dto.ReqMenuAuthMap;

import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.MOCK, classes = { EmsApplication.class,
		ObjectMapper.class })
@AutoConfigureMockMvc(secure = false)
@ContextConfiguration(classes = EmsApplication.class)
@ActiveProfiles("test")
@EnableWebMvc
public class CommonRestContollerTest extends MockJwt {

	// private static String JWT_TOKEN = null;

	// private static final String TOKEN_KEY = "sebang.com";

	@Autowired
	private MockMvc mvc;

	@Autowired
	private ObjectMapper objectMapper;

	// private static final String TEST_END_POINT = "/api/v1/info/511/dtl";

	// private void mockRequest(String memberToJson, ResultMatcher matcher, String
	// result) throws Exception {
	// mvc.perform(post(TEST_END_POINT).content(memberToJson).contentType(MediaType.APPLICATION_JSON_UTF8)
	// .accept(MediaType.APPLICATION_JSON_UTF8)).andExpect(matcher).andExpect(content().json(result));
	// }
	@BeforeClass
	public static void beforeClass_createJwtToken() throws Exception {
		createToken();
	}

	@Test
	public void TEST_사용자별_권한_메뉴_목록_등록() throws Exception {

		String testURL = "/api/v1/common/menu/auth";
		ReqMenuAuthMap c = ReqMenuAuthMap.builder()
						.sAuthCd("OUSR")
						.idMenuInfo(41l)
						.build();

		List<ReqMenuAuthMap> list = new ArrayList<ReqMenuAuthMap>();
		list.add(c)		;		
		String memberToJson = objectMapper.writeValueAsString(list);

		ResultValue r = new ResultValue(ResultCode.SUCCESS, null);
		// String resul
		mvc.perform(post(testURL).header("jwt-header", JWT_TOKEN).content(memberToJson)
				.contentType(MediaType.APPLICATION_JSON_UTF8).accept(MediaType.APPLICATION_JSON_UTF8))
				.andDo(print())
				.andExpect(status().isOk());

	}

	@Test
	public void TEST_공통_ERP조직_부서목록() throws Exception {

		String testURL = "/api/v1/common/erp/dept/ls?sDeptId=";

		ResultValue r = new ResultValue(ResultCode.SUCCESS, null);
		// String resul
		mvc.perform(get(testURL).header("jwt-header", JWT_TOKEN).contentType(MediaType.APPLICATION_JSON_UTF8)
				.accept(MediaType.APPLICATION_JSON_UTF8)).andDo(print()).andExpect(status().isOk());

	}

	@Test
	public void TEST_권한별_일반사원_메뉴목록() throws Exception {

		String testURL = "/api/v1/common/menu/auth/IUSR";

		ResultValue r = new ResultValue(ResultCode.SUCCESS, null);
		// String resul
		mvc.perform(get(testURL).header("jwt-header", JWT_TOKEN).contentType(MediaType.APPLICATION_JSON_UTF8)
				.accept(MediaType.APPLICATION_JSON_UTF8)).andDo(print()).andExpect(status().isOk());

	}

	@Test
	public void TEST_권한별_메뉴_목록_조회() throws Exception {

		String testURL = "/api/v1/common/menu/auth";

		ResultValue r = new ResultValue(ResultCode.SUCCESS, null);
		// String resul
		mvc.perform(get(testURL).header("jwt-header", JWT_TOKEN).contentType(MediaType.APPLICATION_JSON_UTF8)
				.accept(MediaType.APPLICATION_JSON_UTF8)).andDo(print()).andExpect(status().isOk());

	}

	@Test
	public void TEST_권한별_메뉴_등록() throws Exception {

		String testURL = "/api/v1/common/menu/auth";

		ReqMenuAuthMap m1 = new ReqMenuAuthMap().builder()
							.sAuthCd("IUSR")
							.idAuthMenu(120l)
							.idMenuInfo(47l)
							.build();

		ReqMenuAuthMap m2 = new ReqMenuAuthMap().builder()
							.sAuthCd("IUSR")
							.idAuthMenu(121l)
							.idMenuInfo(46l)
							.build();
		ReqMenuAuthMap m3 = new ReqMenuAuthMap().builder()
							.sAuthCd("IUSR")
							.idMenuInfo(45l)
							.build();


		List<ReqMenuAuthMap> list =  new ArrayList<ReqMenuAuthMap>();
		list.add(m1);					
		list.add(m2);
		list.add(m3);

		String memberToJson = objectMapper.writeValueAsString(list);

		ResultValue r = new ResultValue(ResultCode.SUCCESS, null);
		mvc.perform(post(testURL)
						.header("jwt-header", JWT_TOKEN)
						.contentType(MediaType.APPLICATION_JSON_UTF8)
						.content(memberToJson)
						.accept(MediaType.APPLICATION_JSON_UTF8))
						.andDo(print()).andExpect(status().isOk());

	}

	@Test
	public void TEST_권한별_메뉴_등록_리스트() throws Exception {

		String testURL = "/api/v1/common/menu/auth/IUSR/46,47";

		// ReqMenuAuthMap menuAuth = new ReqMenuAuthMap().builder()
		// 					.sAuthCd("IUSR")
		// 					.idMenuInfo(46l)
		// 					.build();

		// List<ReqMenuAuthMap> list =  new ArrayList<ReqMenuAuthMap>();
		// list.add(menuAuth);					
		// String memberToJson = objectMapper.writeValueAsString(list);

		ResultValue r = new ResultValue(ResultCode.SUCCESS, null);
		// String resul
		mvc.perform(post(testURL)
						.header("jwt-header", JWT_TOKEN)
						.contentType(MediaType.APPLICATION_JSON_UTF8)
						//.content(memberToJson)
						.accept(MediaType.APPLICATION_JSON_UTF8))
						.andDo(print()).andExpect(status().isOk());

	}

	@Test
	public void TEST_권한별_메뉴_삭제() throws Exception {

		String testURL = "/api/v1/common/menu/auth/IUSR/46,47";

		//ReqMenuAuthMap menuAuth = new ReqMenuAuthMap().builder()
		//					.sAuthCd("IUSR")
		//					.idMenuInfo(46l)
		//					.build();

		//List<ReqMenuAuthMap> list =  new ArrayList<ReqMenuAuthMap>();
	//	list.add(menuAuth);					
	//	String memberToJson = objectMapper.writeValueAsString(list);

	//	ResultValue r = new ResultValue(ResultCode.SUCCESS, null);
		// String resul
		mvc.perform(delete(testURL)
						.header("jwt-header", JWT_TOKEN)
						.contentType(MediaType.APPLICATION_JSON_UTF8)
				//		.content(memberToJson)
						.accept(MediaType.APPLICATION_JSON_UTF8))
						.andDo(print())
						.andExpect(status().isOk());
	}

	

	@Test
	public void TEST_전체_메뉴목록() throws Exception {

		String testURL = "/api/v1/common/menu/all";

		ResultValue r = new ResultValue(ResultCode.SUCCESS, null);
		// String resul
		mvc.perform(get(testURL).header("jwt-header", JWT_TOKEN).contentType(MediaType.APPLICATION_JSON_UTF8)
				.accept(MediaType.APPLICATION_JSON_UTF8)).andDo(print()).andExpect(status().isOk());

	}

}