package com.sebang.ems.util;

import static org.junit.Assert.assertTrue;

import java.io.Reader;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.opencsv.CSVReader;
import com.sebang.ems.config.TestSqlMapperConfig;
import com.sebang.ems.dao.SiteDeptMapper;
import com.sebang.ems.dao.gen.TRawErpDeptMapper;
import com.sebang.ems.model.Dto.ReqErpDept;
import com.sebang.ems.model.Dto.ReqErpUsers;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

//ref https://www.baeldung.com/opencsv

@SpringBootTest
@RunWith(SpringRunner.class)
@ContextConfiguration(classes = TestSqlMapperConfig.class)
@ActiveProfiles("test")
public class OpenCsvTest {

    public List<String[]> readAll(Reader reader) throws Exception {
        CSVReader csvReader = new CSVReader(reader);
        List<String[]> list = new ArrayList<>();
        list = csvReader.readAll();
        reader.close();
        csvReader.close();
        return list;
    }

    // public String readAllExample() throws Exception {
    // Reader reader = Files.newBufferedReader(Paths.get("d:/dept.csv"));
    // return readAll(reader).toString();
    // }

    // public List<String[]> oneByOne(Reader reader) throws Exception {
    // List<String[]> list = new ArrayList<>();
    // CSVReader csvReader = new CSVReader(reader);
    // String[] line;
    // while ((line = csvReader.readNext()) != null) {
    // list.add(line);
    // }
    // reader.close();
    // csvReader.close();
    // return list;
    // }

    // public String oneByOneExample() throws Exception {
    // Reader reader = Files.newBufferedReader(Paths.get("d:/dept.csv"));
    // return oneByOne(reader).toString();
    // }

    // public List<ErpDept> beanBuilderExample(Path path, Class clazz) throws
    // Exception {
    // CsvTransfer<ErpDept> csvTransfer = new CsvTransfer<ErpDept>();
    // ColumnPositionMappingStrategy ms = new ColumnPositionMappingStrategy();
    // ms.setType(clazz);

    // Reader reader = Files.newBufferedReader(path);
    // CsvToBean cb = new
    // CsvToBeanBuilder(reader).withType(clazz).withMappingStrategy(ms).build();

    // csvTransfer.setCsvList(cb.parse());
    // reader.close();
    // return csvTransfer.getCsvList();
    // }

    @Test
    public void readAllTest() throws Exception {

        Path p = Paths.get("deploy/" + "dept.csv");
        Reader reader = Files.newBufferedReader(p);

        String s = p.toAbsolutePath().toString();
        List<String[]> dept = readAll(reader);

        // dept.stream().forEach(d -> {
        //     System.out.println(d[0] + "," + d[1] + "," + d[2]);
        //     return;
        // });
        assertTrue(true);
    }

    // @Test
    // public void oneByOneTest() throws Exception {

    // String d = oneByOneExample();
    // System.out.println(d);

    // }

    // public static void transform(File source, String srcEncoding, File target,
    // String tgtEncoding) throws IOException {
    // BufferedReader br = null;
    // BufferedWriter bw = null;
    // try{
    // br = new BufferedReader(new InputStreamReader(new
    // FileInputStream(source),srcEncoding));
    // bw = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(target),
    // tgtEncoding));
    // char[] buffer = new char[16384];
    // int read;
    // while ((read = br.read(buffer)) != -1)
    // bw.write(buffer, 0, read);
    // } finally {
    // try {
    // if (br != null)
    // br.close();
    // } finally {
    // if (bw != null)
    // bw.close();
    // }
    // }
    // }

    // @Test
    // public void readUtf16Test() throws IOException {

    // File s = new File("deploy/" + "dept.csv");
    // File d = new File("deploy/" + "dept_o.csv");
    // transform(s,"UTF-16",d,"UTF-8");
    // assertTrue(true);
    // }

    @Autowired
    TRawErpDeptMapper rawErpDeptMapper;

    @Autowired
    SiteDeptMapper siteDeptMapper;

    public LocalDate convertToLocalDateViaInstant(Date dateToConvert) {
        return dateToConvert.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
    }

    @Test
    public void getlastRawErpDeptTest() {
        ZoneId defaultZoneId = ZoneId.systemDefault();
        System.out.println("System Default TimeZone : " + defaultZoneId);

        Date date = siteDeptMapper.lastModifiedAtTRawErpDept();

        LocalDate localDate = date.toInstant().atZone(defaultZoneId).toLocalDate();
        System.out.println("localDate : " + localDate);
        System.out.println("localDate : " + date);
    }

    @Test
    public void readDeptCsvTest() throws Exception {
        Path currentRelativePath = Paths.get("deploy/" + "dept.csv");
        String s = currentRelativePath.toAbsolutePath().toString();

        CsvToObject<ReqErpDept> csv = new CsvToObject<ReqErpDept>();
        List<ReqErpDept> dept = csv.build(currentRelativePath, ReqErpDept.class,Charset.forName("euc-kr"));
        // dept.stream().forEach(d -> {
        //     System.out.println("localDate : " + d);
        //    return;
        // });
        assertTrue(true);
    }

    @Test
    public void readPersonCsvTest() throws Exception {
        Path currentRelativePath = Paths.get("deploy/" + "person.csv");
        String s = currentRelativePath.toAbsolutePath().toString();

        CsvToObject<ReqErpUsers> csv = new CsvToObject<ReqErpUsers>();
        List<ReqErpUsers> dept = csv.build(currentRelativePath, ReqErpUsers.class,Charset.forName("EUC-KR"));
        // dept.stream().forEach(d -> {
        //     System.out.println("localDate : " + d);
        //     return;
        // });
        assertTrue(true);
    }

    @Test
    public void readRetireCsvTest() throws Exception {
        Path currentRelativePath = Paths.get("deploy/" + "retire.csv");
        String s = currentRelativePath.toAbsolutePath().toString();

        CsvToObject<ReqErpUsers> csv = new CsvToObject<ReqErpUsers>();
        List<ReqErpUsers> dept = csv.build(currentRelativePath, ReqErpUsers.class,Charset.forName("EUC-KR"));
        // dept.stream().forEach(d -> {
        //     System.out.println("localDate : " + d);
        //     return;
        // });
        assertTrue(true);
    }

//     @Test
//     public void convertEucKrToUtf8() throws IOException
//     {
//         Path currentRelativePath = Paths.get("deploy/" + "person.csv");
//         String s = currentRelativePath.toAbsolutePath().toString();

        
//         BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(s), "MS949"));
//         BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(new FileOutputStream("utf8.csv"), "UTF-8"));
//         String line;
//         while ((line = br.readLine()) != null) {
// //           System.out.println(line);

//             byte[] iso88591bytes = line.getBytes("MS949");
//             for ( byte b : iso88591bytes )
//                 System.out.printf("%02x ", b);

//             String utf8 = new String(line.getBytes(),"UTF-8");
//             byte[] utf8bytes = utf8.getBytes("UTF-8");

//             for ( byte b : utf8bytes )
//                 System.out.printf("%02x ", b);

            
//             System.out.println(utf8);
//             bw.write(utf8);
//             bw.newLine();
//         }
//         bw.flush();
//         br.close();
//         bw.close();
//         System.out.println("Done!"+ "charset:"+ Charset.forName("utf-8"));
//     }

    // @Test
    // public void findPathTest()
    // {
    //     Path p = Paths.get("d:/", "temp");
    //     System.out.println(p.toAbsolutePath().toString());
    // }
}