package com.sebang.ems.util;

import java.util.HashMap;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.sebang.ems.config.TestSqlMapperConfig;
import com.sebang.ems.dao.UsersMapper;
import com.sebang.ems.domain.common.ResultCode;
import com.sebang.ems.domain.common.ResultValue;
import com.sebang.ems.model.Do.TUsers;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

@SpringBootTest
@RunWith(SpringRunner.class)
@ContextConfiguration(classes = TestSqlMapperConfig.class)
@ActiveProfiles("test")
public class JsonTest {

    @Test
    public void convertKeyMapTest() {
        HashMap<String, HashMap<String, String>> result = new HashMap<String, HashMap<String, String>>();

        HashMap<String, String> i = new HashMap<String, String>();
        i.put("1", "1");
        i.put("1", "2");
        i.put("1", "3");
        i.put("1", "4");
        i.put("1", "5");

        result.put("11", i);

        ObjectMapper om = new ObjectMapper();
        try {
            String s = om.writeValueAsString(result);
            System.out.println(s);
        } catch (JsonProcessingException e) {

            e.printStackTrace();
        }

    }

    @Autowired
    UsersMapper usersMappe;

    @Test
    public void convertJsonTest() {
        try {

        TUsers u = usersMappe.findById("70004485");
        
        ResultValue rv = new ResultValue(ResultCode.SUCCESS, u);

        ObjectMapper m = new ObjectMapper();
        String out = m.writeValueAsString(rv);
        System.out.println(out);
        }catch(JsonProcessingException e)
        {
            e.printStackTrace();
        }
    }

}