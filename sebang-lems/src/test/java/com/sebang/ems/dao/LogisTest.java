package com.sebang.ems.dao;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.math.BigDecimal;
import java.util.List;

import com.sebang.ems.config.TestSqlMapperConfig;
import com.sebang.ems.domain.common.SearchParam;
import com.sebang.ems.model.Do.TEngPoint;
import com.sebang.ems.model.Do.TLogisTransInfo;
import com.sebang.ems.model.Do.TLogisTransMonInfo;
import com.sebang.ems.model.Do.TTransCom;
import com.sebang.ems.model.Do.TVehicle;
import com.sebang.ems.model.Do.VLogisDrvLog;
import com.sebang.ems.model.Do.VLogisEff;
import com.sebang.ems.model.Do.VLogisTransYearVhcl;
import com.sebang.ems.model.Do.VVehicle;
import com.sebang.ems.util.DateUtil;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.support.DefaultTransactionDefinition;

@SpringBootTest
@RunWith(SpringRunner.class)
@ContextConfiguration(classes=TestSqlMapperConfig.class)
@ActiveProfiles("test")
public class LogisTest
{

	@Autowired
    LogisMapper logisMapper;

    @Autowired
	private PlatformTransactionManager transactionManager;

	DefaultTransactionDefinition def = null;
    TransactionStatus status = null;
    
    @Transactional
    @Rollback(true)
    @Test
    public void upsertLogisTransInfoTest()
    {
        TLogisTransInfo d = new TLogisTransInfo();
        d.setIdTransCom(1l);
        d.setcDate("2019");
//        d.setsTransComp("세방");
        d.setnWeight(new BigDecimal(111.1f));
        d.setnFuelEff(new BigDecimal(10.1f));
        d.setnToe(new BigDecimal(112.2f));
        
        logisMapper.upsertLogisTransInfo(d);
        assertTrue(true);
    }

    @Transactional
    @Rollback(true)
    @Test
    public void upsertVehicleTest()
    {
        TVehicle v = new TVehicle();
        v.setIdTransCom(1l);
        v.setsVrn("서울7도7777");
        v.setIdErpDept(2l);
        
        logisMapper.upsertVehicle(v);
        assertTrue(true);
    }


    @Transactional
    @Rollback(true)
    @Test
    public void updateVehicleTest()
    {
        TVehicle v = new TVehicle();
        v.setsVrn("대구6도1234");
        v.setIdVehicle(4l);
        v.setIdErpDept(1l);
        v.setIdTransCom(2l);
        v.setsLineCd("1");
    
        
        logisMapper.upsertVehicle(v);
        assertTrue(true);
    }

    @Transactional
    @Rollback(true)
    @Test
    public void upsertTransComTest()
    {
        TTransCom tt = new TTransCom();
        tt.setsTransComNm("세방");

        logisMapper.upsertTransCom(tt);
        System.out.println("idTransCom "+ tt.getIdTransCom());
        assertTrue(tt.getIdTransCom()>0);

        TTransCom t = logisMapper.findTransComByName(tt.getsTransComNm());
        assertNotNull(t);;
    }

    @Transactional
    @Rollback(true)
    @Test
    public void upsertTransComWithIdTest()
    {
        TTransCom tt = new TTransCom();
        tt.setIdTransCom(10l);
        tt.setsTransComNm("세방");

        logisMapper.upsertTransCom(tt);
        System.out.println("idTransCom "+ tt.getIdTransCom());
        assertTrue(tt.getIdTransCom()>0);

        TTransCom t = logisMapper.findTransComByName(tt.getsTransComNm());
        assertNotNull(t);;
    }


    @Test
    public void searchVehicleTest()
    {
        SearchParam p = new SearchParam();
        p.setSVrn("경기2나22227");
        p.setStartDt(DateUtil.convertStringToDate("20190801000000"));
        p.setEndDt(DateUtil.convertStringToDate("20191101000000"));
    
        List<VVehicle> list= logisMapper.searchVehicle(p);
        assertTrue(list.size()>=0);

    }

    @Test
    public void searchVLogisDrvLogTest()
    {
        SearchParam p = new SearchParam();
        p.setCYear("2019");
        p.setCMon("01");     
        p.setTransTypeCd("1");
        p.setSDrvType("1");   
        List<VLogisDrvLog> list = logisMapper.searchVLogisDrvLog(p);
        assertTrue(list.size()>=0);
    }

    @Test
    @Transactional
    @Rollback(true)
    public void upsertLogisTransMonInfoTest()
    {
        TLogisTransMonInfo m = new TLogisTransMonInfo();
        m.setIdLogisTransMonInfo(1l);
        m.setDistanceSum(new BigDecimal(1234));
        m.setIdEngPoint(1l);
        m.setIdVehicle(11l);
        m.setDateMon("201901");
        m.setFuelEff(new BigDecimal(12.2));

        logisMapper.upsertLogisTransMonInfo(m);
        assertTrue(true);
    }

    @Test
    public void searchVLogisTransYearTest()
    {
        SearchParam p = new SearchParam();
        p.setCYear("2019");
        p.setCYearEnd("2020");
        p.setIdSite(3l);  
        
        List<VLogisTransYearVhcl> list = logisMapper.searchVLogisTransYear(p);
        assertTrue(list.size()>=0);

    }

    @Test
    public void searchVLogisEffTest()
    {
        String [] years = {"2019","2018"};
        SearchParam p = new SearchParam();
        p.setCarType("1");
        p.setSLineCd("1");
        p.setSLogiWeiCd("1");
        p.setIdSite(3l);  
        p.setArrYear(years);
        
        List<VLogisEff> list = logisMapper.searchVLogisEff(p);
        assertTrue(list.size()>=0);
    }

    @Test
    public void findTransComByIdTest()
    {
        TTransCom com = logisMapper.findTransComById(null);
        assertTrue(true);

    }
    
    @Test
    public void findEngPointForTransTest()
    {
        TEngPoint  p = logisMapper.findEngPointForVehicle(354l,"전남87바6629");
        assertTrue(true);

    }

    @Test
    public void findEngPointForVehicleTest()
    {
        TEngPoint  p = logisMapper.findEngPointForTrain(400l);
        assertTrue(true);

    }
}