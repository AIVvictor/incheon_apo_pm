package com.sebang.ems.dao;

import static org.junit.Assert.assertTrue;

import java.util.List;

import com.sebang.ems.config.TestSqlMapperConfig;
import com.sebang.ems.model.Menu;
import com.sebang.ems.model.Do.TAuthMenu;
import com.sebang.ems.model.Do.TCat;
import com.sebang.ems.model.Do.TCode;
import com.sebang.ems.model.Do.TMenuInfo;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.TransactionDefinition;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.DefaultTransactionDefinition;

@SpringBootTest
@RunWith(SpringRunner.class)
@ContextConfiguration(classes = TestSqlMapperConfig.class)
@ActiveProfiles("test")
public class CodeTest {

    @Autowired
    CodeMapper codeMapper;

    @Autowired
    private PlatformTransactionManager transactionManager;

    DefaultTransactionDefinition def = null;
    TransactionStatus status = null;

    @Test
    public void findCodeTest() {
        List<TCode> re = codeMapper.find("");
        assertTrue(re.size() >= 0);
    }

    @Test
    public void findCatTest() {
        List<TCat> re = codeMapper.findCat(null);
        assertTrue(re.size() >= 0);
    }

    @Test
    public void upsertCodeTest() {
        TCode code = new TCode();
        code.setsCat("123");
        code.setsCd("123");
        code.setsDesc("dd");

        code.setsRegId("regid");

        try {
            def = new DefaultTransactionDefinition();
            def.setPropagationBehavior(TransactionDefinition.PROPAGATION_REQUIRED);
            status = transactionManager.getTransaction(def);
            codeMapper.upsertCode(code);
            transactionManager.rollback(status);
            assertTrue(true);

        } catch (Exception ex) {
            transactionManager.rollback(status);
            ex.printStackTrace();
            assertTrue(false);

        }
    }

    @Test
    public void upsertCatTest() {
        TCat cat = new TCat();
        cat.setsCat("123");
        cat.setcDelYn("Y");
        cat.setsCatDesc("dd");
        
        cat.setsRegId("regid");
        try {
            def = new DefaultTransactionDefinition();
            def.setPropagationBehavior(TransactionDefinition.PROPAGATION_REQUIRED);
            status = transactionManager.getTransaction(def);
            codeMapper.upsertCat(cat);
            transactionManager.rollback(status);
            assertTrue(true);
        } catch (Exception ex) {
            transactionManager.rollback(status);
            assertTrue(false);
        }
    }

    @Autowired
    MenuMapper menuMapper;

    @Test
    public void getAllMenuTest() {
        List<TMenuInfo> menu = menuMapper.findMst("");
        for (TMenuInfo m : menu) {
            System.out.println("menu:" + m.getsMenuNm());
        }
        assertTrue(true);
    }

    @Test
    public void findMenuTest() {
        List<Menu> menu = menuMapper.find("SYS");
        for (Menu m : menu) {
            System.out.println("menu:" + m.getsMenuNm());
        }
        assertTrue(true);
    }


    @Test
    public void findMenuAuthTest() {
   //     TAuthMenu t =  new TAuthMenu();
      //  t.setsAuthCd("SYS");
        TAuthMenu menu = menuMapper.findMenuAuth(158l);
        if(menu!=null)
            System.out.println("menu:" + menu.getsAuthCd());
        
        assertTrue(true);
    }

    public void upsertMenuAuthTest()
    {
        
    }

}