package com.sebang.ems.dao;

import static org.junit.Assert.assertTrue;

import java.math.BigDecimal;
import java.util.List;

import com.sebang.ems.config.TestSqlMapperConfig;
import com.sebang.ems.domain.common.SearchParam;
import com.sebang.ems.model.EngPointAmt;
import com.sebang.ems.model.SumAmt;
import com.sebang.ems.model.SumAmt4Home;
import com.sebang.ems.model.Do.TEngPointAmt;
import com.sebang.ems.model.Do.TMonEngClose;
import com.sebang.ems.model.Do.VBaseUntAmt;
import com.sebang.ems.model.Do.VMonEngClose;
import com.sebang.ems.model.Do.VMonSiteEngCefAmt;
import com.sebang.ems.model.Do.VSumAmt;
import com.sebang.ems.model.Dto.ReqMonEngClose;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

@SpringBootTest
@RunWith(SpringRunner.class)
@ContextConfiguration(classes = TestSqlMapperConfig.class)
@ActiveProfiles("test")

public class AmtTest {

    @Autowired
    AmtMapper amtMapper;

    @Transactional
    @Rollback(true)
    @Test
    public void upsertEngPointAmtTest() {
        TEngPointAmt a = new TEngPointAmt();
        a.setIdEngPoint(1l);
        a.setIdErpDept(2l);
        a.setcMon("08");
        a.setcYear("2019");
        a.setnCost(new BigDecimal(1.1));
        a.setnUseAmt(new BigDecimal(2.2));
        a.setnGj(new BigDecimal(123));
        a.setnTco2(new BigDecimal(1.2));
        amtMapper.upsertEngPointAmt(a);
        assertTrue(true);
    }

    @Test
    public void searchEngPointAmtTest() {
        SearchParam params = new SearchParam();
        params.setCYear("2019");
        params.setCMon("01");

        // params.setCMon("1");
        List<EngPointAmt> amt = amtMapper.searchEngPointAmt(params);
        amt.stream().forEach(item -> {
            System.out.println(item.getcYear() + ", " + item.getcMon() + " NAME " + item.getsPointNm());
        });
        assertTrue(amt.size() >= 0);
    }

    @Test
    public void getEngConversionTest()
    {
        EngPointAmt amt = amtMapper.findEngConversion(1,new BigDecimal(1234l));
        assertTrue(true);
    }

    @Test
    public void searchMonEngCloseTest() {
        SearchParam p = new SearchParam();
        p.setCYear("2019");
        p.setCDate("1");

        List<VMonEngClose> list = amtMapper.searchMonEngClose(p);

        assertTrue(list.size() >= 0);
    }


    @Test
    public void searchMonSiteEngCefAmtTest() {
        SearchParam p = new SearchParam();
        p.setCYear("2019");
        p.setCDate("1");

        List<VMonSiteEngCefAmt> list = amtMapper.searchMonSiteEngCefAmt(p);

        assertTrue(list.size() >= 0);
    }

    @Test
    public void searchTotalSumAmt() {
        SearchParam p = new SearchParam();
        p.setCYear("2019");
        List<VSumAmt> list = amtMapper.searchTotalSumAmt(p);

        assertTrue(list.size() >= 0);
    }

    // 연도별 분석
    @Test
    public void searchYearAnalysisAmt() {

        SearchParam p = new SearchParam();
        String[] arrYear = { "2019", "2018" };
        p.setArrYear(arrYear);
        p.setSLogiTypeCd("1");
        p.setSBaseUnt("1");
        String[] arrIdSite = { "3" };
        String[] arrIdEngPoint = { "1", "5" };
        p.setArrIdSite(arrIdSite);
        p.setArrIdEngPoint(arrIdEngPoint);
        p.setCYear("2018");
        p.setCYearEnd("2019");

        List<SumAmt> list = amtMapper.searchYearAnalysisAmt(p);
        list.stream().forEach(i -> {
            System.out.println(i.toString());
        });

        assertTrue(list.size() >= 0);
    }

    // 사업부별 분석
    @Test
    public void searchYearAnalysisSiteAmt() {

        SearchParam p = new SearchParam();
        String[] arrYear = { "2019", "2018" };
        p.setArrYear(arrYear);
        p.setSLogiTypeCd("1");
        p.setSBaseUnt("1");
        String[] arrIdSite = { "3" };
        p.setArrIdSite(arrIdSite);
        p.setCYear("2018");
        p.setCYearEnd("2019");

        List<SumAmt> list = amtMapper.searchYearAnalysisSiteAmt(p);
        list.stream().forEach(i -> {
            System.out.println(i.toString());
        });

        assertTrue(list.size() >= 0);
    }

    // 배출시설별 분석
    @Test
    public void searchYearAnalysisPointAmt() {

        SearchParam p = new SearchParam();
        String[] arrYear = { "2019", "2018" };
        p.setArrYear(arrYear);
        p.setSLogiTypeCd("1");
        p.setSBaseUnt("1");
        String[] arrIdEngPoint = { "1", "5" };
        p.setArrIdEngPoint(arrIdEngPoint);
        p.setCYear("2018");
        p.setCYearEnd("2019");

        List<SumAmt> list = amtMapper.searchYearAnalysisPointAmt(p);
        list.stream().forEach(i -> {
            System.out.println(i.toString());
        });

        assertTrue(list.size() >= 0);
    }

    // 월별 분석
    @Test
    public void searchMonAnalysisAmt() {

        SearchParam p = new SearchParam();
        String[] arrYear = { "2019", "2018" };
        p.setArrYear(arrYear);
        String[] arrIdSite = { "3" };
        String[] arrIdEngPoint = { "1", "5" };
        p.setArrIdSite(arrIdSite);
        p.setArrIdEngPoint(arrIdEngPoint);
        p.setCYear("2018");
        p.setCMon("1");

        p.setCYearEnd("2019");
        p.setCMonEnd("2");
        List<SumAmt> list = amtMapper.searchMonAnalysisAmt(p);
        list.stream().forEach(i -> {
            System.out.println(i.toString());
        });

        assertTrue(list.size() >= 0);
    }

    @Test
    public void searchMonAnalysisSiteAmt() {

        SearchParam p = new SearchParam();
        String[] arrYear = { "2019", "2018" };
        p.setArrYear(arrYear);
        String[] arrIdSite = { "3" };
        // String [] arrIdEngPoint={"1","5"};
        p.setArrIdSite(arrIdSite);
        // p.setArrIdEngPoint(arrIdEngPoint);
        p.setCYear("2018");
        p.setCMon("1");

        p.setCYearEnd("2019");
        p.setCMonEnd("2");
        List<SumAmt> list = amtMapper.searchMonAnalysisSiteAmt(p);
        list.stream().forEach(i -> {
            System.out.println(i.toString());
        });

        assertTrue(list.size() >= 0);
    }

    @Test
    public void searchMonAnalysisPointAmt() {

        SearchParam p = new SearchParam();
        String[] arrYear = { "2019", "2018" };
        p.setArrYear(arrYear);
        String[] arrIdSite = { "3" };
        String[] arrIdEngPoint = { "1", "5" };
        // p.setArrIdSite(arrIdSite);
        p.setArrIdEngPoint(arrIdEngPoint);
        // p.setCYear("2018");
        p.setCMon("1");

        p.setCYearEnd("2019");
        p.setCMonEnd("2");
        List<SumAmt> list = amtMapper.searchMonAnalysisPointAmt(p);
        list.stream().forEach(i -> {
            System.out.println(i.toString());
        });

        assertTrue(list.size() >= 0);
    }

    @Test
    public void searchLogisBaseUntAnalysisTest() {
        SearchParam p = new SearchParam();
        String[] arrYear = { "2019", "2018" };
        p.setArrYear(arrYear);
        p.setSLogiTypeCd("1");
        List<VBaseUntAmt> list = amtMapper.searchBaseUntAmtAnalysis(p);
        list.stream().forEach(i -> {
            System.out.println(i.getcYear());
        });

        assertTrue(list.size() >= 0);
    }

    @Test
    public void searchBaseUntAmtAnalysisTest() {
        SearchParam p = new SearchParam();
        String[] arrYear = { "2019", "2018" };
        p.setArrYear(arrYear);
        List<VBaseUntAmt> list = amtMapper.searchBaseUntAmtAnalysis(p);
        list.stream().forEach(i -> {
            System.out.println(i.getcYear());
        });

        assertTrue(list.size() >= 0);
    }

    @Test
    public void searchBaseUntAmtAnalysisByIdSiteTest() {
        SearchParam p = new SearchParam();
        String[] arrYear = { "2019", "2018" };
        p.setArrYear(arrYear);
        p.setIdSite(3l);
        List<VBaseUntAmt> list = amtMapper.searchBaseUntAmtAnalysis(p);
        list.stream().forEach(i -> {
            System.out.println(i.getcYear());
        });

        assertTrue(list.size() >= 0);
    }

    @Test
    public void searchEngSumAmt4HomeTest()
    {
        String[] years = {"2019","2018"};
        SearchParam p = new SearchParam();
        p.setArrYear(years);

        List<SumAmt4Home> list =  amtMapper.searchEngSumAmt4Home(p);
        assertTrue(list.size()>=0);
  
    }

    @Test
    public void searchGasSumAmt4HomeTest()
    {
        String[] years = {"2019","2018"};
        SearchParam p = new SearchParam();
        p.setArrYear(years);

        List<SumAmt4Home> list =  amtMapper.searchGasSumAmt4Home(p);
        assertTrue(list.size()>=0);
  
    }

    @Test
    public void findMonthEndClosingTest()
    {
        Integer count = amtMapper.enableMonthEndClosing("2019","1");
        assertTrue(count>=0);
    }

    @Test
    public void MonthEndClosingTest()
    {
        TMonEngClose c = new TMonEngClose();
        c.setcYear("2019");
        c.setcMon("2");
        amtMapper.upsertMonEngClose(c);
        assertTrue(true);
    }


    @Test
    public void StoreProcedureTest()
    {
       ReqMonEngClose c = new ReqMonEngClose();
       c.setCYear("201911");
    
        amtMapper.callSPDoCloseMonth(c);
        assertTrue(true);
    }
}