package com.sebang.ems.dao;

import static org.junit.Assert.assertTrue;

import com.sebang.ems.config.TestSqlMapperConfig;
import com.sebang.ems.model.Do.TBoard;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

@SpringBootTest
@RunWith(SpringRunner.class)
@ContextConfiguration(classes=TestSqlMapperConfig.class)
@ActiveProfiles("test")

@Transactional
@Rollback(true)   
public class BoardTest
{

	@Autowired
    BoardMapper boardMapper;
    
    @Test
    @Transactional
    @Rollback(true)
    public void insertBoardTest()
    {
        TBoard b = new TBoard();
        b.setsTitle("게시판 테스트");
        b.setsContent("게시판내용");
        b.setsBoardType("1");
        b.setsRegEmpId("tiger");
        boardMapper.insertSelective(b);
        assertTrue(b.getIdBoard()>0);
    }

       
    @Test
    @Transactional
    @Rollback(true)
    public void updateBoardTest()
    {
        TBoard b = new TBoard();
        b.setIdBoard(1l);
        b.setsTitle("게시판 테스트");
        b.setsContent("게시판내용");
        b.setsBoardType("1");
        b.setsRegEmpId("tiger");
        boardMapper.updateByPrimaryKeySelective(b);
        assertTrue(true);   
    }

    @Test
    @Transactional
    @Rollback(true)
    public void deleteBoardTest()
    {

        boardMapper.deleteDetailOneByBoardId("1l");
        assertTrue(true);   
    }
   
}