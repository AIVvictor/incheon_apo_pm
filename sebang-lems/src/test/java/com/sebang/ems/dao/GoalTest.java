package com.sebang.ems.dao;

import static org.junit.Assert.assertTrue;

import java.math.BigDecimal;
import java.util.List;

import com.sebang.ems.config.TestSqlMapperConfig;
import com.sebang.ems.domain.common.SearchParam;
import com.sebang.ems.model.SiteYearGoal4Home;
import com.sebang.ems.model.Do.TMrvPointGoal;
import com.sebang.ems.model.Do.TSiteMonGoal;
import com.sebang.ems.model.Do.TSiteMonSumGoal;
import com.sebang.ems.model.Do.TSiteYearGoal;
import com.sebang.ems.model.Do.VBau;
import com.sebang.ems.model.Do.VGoalResultAnalyAcc;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

@SpringBootTest
@RunWith(SpringRunner.class)
@ContextConfiguration(classes = TestSqlMapperConfig.class)
@ActiveProfiles("test")

@Transactional
@Rollback(false)
public class GoalTest {

    @Autowired
    GoalMapper goalMapper;

    @Test
    @Transactional
    @Rollback(true)
    public void upsertMrvPointGoalTest() {
        String cYear = "2017";
        TMrvPointGoal g = goalMapper.findMrvGoal(cYear);
        if (g == null) {
            g = new TMrvPointGoal();
            g.setcYear(cYear);
        }
        g.setnEngGoal(new BigDecimal(1234));
        g.setnGasGoal(new BigDecimal(6543));
        // goal.setnGoal(88f);
        // goal.setsGasengCd("1");
        goalMapper.upsertMrvPointGoal(g);
        assertTrue(true);
    }

    @Test
    @Transactional
    @Rollback(true)
    public void upsertSiteYearGoalTest() {
        TSiteYearGoal g = goalMapper.findSiteYearGoal(null, 2l);
        if (g == null) {
            g = new TSiteYearGoal();
            g.setcYear("2019");
            g.setIdSite(2l);
        }
        goalMapper.upsertSiteYearGoal(g);
        System.out.println(g.getIdSiteYearGoal());
        assertTrue(g.getIdSiteYearGoal() > 0);
    }

    @Test
    @Transactional
    @Rollback(true)
    public void upsertSiteYearGoalNoIdTest() {
        TSiteYearGoal g = new TSiteYearGoal();
        g = new TSiteYearGoal();
        g.setcYear("2019");
        g.setIdSite(2l);

        goalMapper.upsertSiteYearGoal(g);
        System.out.println(g.getIdSiteYearGoal());
        assertTrue(g.getIdSiteYearGoal() > 0);
    }

    @Test
    @Transactional
    @Rollback(true)
    public void upsertSiteMonSumGoalTest() {
        TSiteMonSumGoal g = goalMapper.findSiteMonSumGoal(1l, 2l);
        if (g == null) {
            g = new TSiteMonSumGoal();
            g.setIdSiteYearGoal(1l);
            g.setIdEngPoint(2l);
            g.setnGoalSum(new BigDecimal(125));
        }
        goalMapper.upsertSiteMonSumGoal(g);
        System.out.println("IdSiteMonSumGoal " + g.getIdSiteMonSumGoal() );
        assertTrue(g.getIdSiteMonSumGoal() > 0);
    }

    @Test
    @Transactional
    @Rollback(true)
    public void upsertSiteMonSumGoalWithNoIdTest() {
        TSiteMonSumGoal g = new TSiteMonSumGoal();
        
        g = new TSiteMonSumGoal();
        g.setIdSiteYearGoal(1l);
        g.setIdEngPoint(2l);
        g.setnGoalSum(BigDecimal.ZERO);
    
        goalMapper.upsertSiteMonSumGoal(g);
        System.out.println("IdSiteMonSumGoal " + g.getIdSiteMonSumGoal() );
        assertTrue(g.getIdSiteMonSumGoal() > 0);
    }

    @Test
    @Transactional
    @Rollback(true)
    public void updateSiteMonSumGoalTest() {
   
        goalMapper.updateSiteMonSumGoal(10l);
        assertTrue(true);
    }

    @Test
    @Transactional
    @Rollback(true)
    public void upsertSiteMonGoalTest() {
        TSiteMonGoal g = goalMapper.findSiteMonGoal(1l, "11");
        if (g == null) {
            g = new TSiteMonGoal();
            g.setIdSiteMonSumGoal(1l);
            g.setcMon("11");
        }
        g.setnMonEngGoal(new BigDecimal(111111));

        goalMapper.upsertSiteMonGoal(g);
        assertTrue(true);
    }

    @Test
    public void searchSiteGoalTest() {
        SearchParam s = new SearchParam();
        s.setIdSite(2l);
        s.setCYear("2019");

        List<?> r = goalMapper.searchSiteGoal(s);
        assertTrue(r.size() >= 0);
    }

    @Test
    public void searchVMrvGoalTest() {
        SearchParam s = new SearchParam();
        s.setCYear("2019");

        List<?> r = goalMapper.searchVMrvGoal(s);
        assertTrue(r.size() >= 0);

    }

    @Test
    public void searchVComponyGoalTest() {
        SearchParam s = new SearchParam();
        s.setCYear("2019");

        List<?> r = goalMapper.searchSiteMonGoal(s);
        assertTrue(r.size() >= 0);

    }

    @Test
    public void searchSiteMonGoalArchRateTest() {
        SearchParam p = new SearchParam();
        p.setCYear("2019");
        p.setIdSite(2l);

        List<VGoalResultAnalyAcc> r = goalMapper.searchSiteMonGoalArchRate(p);
        assertTrue(r.size() >= 0);

        r.stream().forEach(item -> {
            System.out.println("cYear:" + item.getcYear() + ",cMon:" + item.getcMon() + ", idSite:" + item.getIdSite());
        });

        p.setIdEngCef(2l);
        r = goalMapper.searchSiteMonGoalArchRate(p);
        r.stream().forEach(item -> {
            System.out.println("cYear:" + item.getcYear() + ",cMon:" + item.getcMon() + ", idEngCef:" + item.getIdEngCef());
        });
        assertTrue(r.size() >= 0);
    }

    @Test
    public void searchBauTest()
    {
        SearchParam p = new SearchParam();
        p.setCYear("2019");
        p.setCYearEnd("2019");
        List<VBau> r = goalMapper.searchBau(p);
        assertTrue(r.size() >= 0);
    }

    @Test
    public void searchSiteYearGoal4HomeTest()
    {
        SearchParam p = new SearchParam();
        p.setCYear("2019");
        List<SiteYearGoal4Home > r = goalMapper.searchSiteYearGoal4Home(p);
        assertTrue(r.size() >= 0);
    }

}