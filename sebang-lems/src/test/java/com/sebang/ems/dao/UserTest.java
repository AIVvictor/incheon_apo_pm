package com.sebang.ems.dao;

import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.sebang.ems.config.TestSqlMapperConfig;
import com.sebang.ems.domain.common.SearchParam;
import com.sebang.ems.model.Employee;
import com.sebang.ems.model.Do.TErpUsers;
import com.sebang.ems.model.Do.TUsers;
import com.sebang.ems.model.Dto.ReqErpUsers;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.crypto.factory.PasswordEncoderFactories;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

@SpringBootTest
@RunWith(SpringRunner.class)
@ContextConfiguration(classes = TestSqlMapperConfig.class)
@ActiveProfiles("test")
public class UserTest {
    @Autowired
    UsersMapper userMapper;

    private PasswordEncoder passwordEncoder;

    @Test
    @Transactional
    @Rollback(true)
    public void userTest() {
        passwordEncoder = PasswordEncoderFactories.createDelegatingPasswordEncoder();
        String encPassword = passwordEncoder.encode("1234");

        TUsers u = new TUsers();
        u.setsEmpId("admin");
        u.setsEmpNm("");
        
        u.setsEmpPwd(encPassword);
        u.setcAuthCode("ENGA");

        userMapper.insert(u);

        List<TUsers> re = userMapper.select();
        System.out.println(re.size());

        assertTrue(true);
    }

    @Test
    public void findEmployee() {
        TUsers u = userMapper.findById("000");
        assertNull(u);

        SearchParam s = new SearchParam();
        s.setSEmpId("sEmpId");
        s.setIdSite(10l);
        List<Employee> l = userMapper.searchEmployee(s);

        assertTrue(l.size() == 0);
    }

    @Test
    @Transactional
    @Rollback(true)
    public void upsertEmployee() {
        Employee emp = new Employee();
        emp.setsEmpId("test");
        emp.setsEmpNm("test");
        emp.setsEmpPwd("test");

   
        userMapper.upsertEmployee(emp);
        assertTrue(true);
    }

    @Test
    public void lastModifiedAtTErpUsersTest() {
        userMapper.lastModifiedAtTErpUsers();
        assertTrue(true);    
    }


    @Test
    public void findErpUserByEmpIdTest() {
        userMapper.findErpUserByEmpId("123456");
        assertTrue(true);    
    }

    @Test
    @Transactional
    @Rollback(true)
    public void deleteRawErpUsersTest(){

        List<ReqErpUsers> list = new ArrayList<ReqErpUsers>();
        ReqErpUsers user = new ReqErpUsers();
        user.setSEmpId("70004485");
        list.add(user);


        List<TErpUsers> res= userMapper.findDeleteRawErpUsers(list);
        assertTrue(res.size()>=0);

        userMapper.deleteRawErpUsers(list);

        assertTrue(true);    
    }


    @Test
    public void pullErpUsersTest() {
        List<TErpUsers> list = userMapper.pullErpUsers(new Date());
        assertTrue(true);    
    }

    @Test
    @Transactional
    @Rollback(true)
    public void upsertErpUsersTest()
    {
        TErpUsers u =  new TErpUsers();
        u.setsEmpId("1234567");
        u.setsEmpNm("1234567");
        userMapper.upsertErpUsers(u);
    }

}