package com.sebang.ems.dao;

import static org.junit.Assert.assertTrue;

import java.util.List;
import java.util.Map;

import com.sebang.ems.config.TestSqlMapperConfig;
import com.sebang.ems.domain.common.SearchParam;
import com.sebang.ems.model.Do.TCefMod;
import com.sebang.ems.model.Do.VEngCef;
import com.sebang.ems.model.Do.VEngPoint;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.DefaultTransactionDefinition;

@SpringBootTest
@RunWith(SpringRunner.class)
@ContextConfiguration(classes=TestSqlMapperConfig.class)
@ActiveProfiles("test")
public class CefTest
{

	@Autowired
    CefMapper cefMapper;

    @Autowired
	private PlatformTransactionManager transactionManager;

	DefaultTransactionDefinition def = null;
    TransactionStatus status = null;
    
    @Test
    public void findByEmtCdTest()
    {
        List<VEngCef> re = cefMapper.findByEmt("1");
        assertTrue(re.size()>=0);
    }

    @Test
    public void searchModTest()
    {
       
        List<TCefMod> re = cefMapper.findMod(1L);
        assertTrue(re.size()>=0);
    }

    
    @Test
    public void searchPointTest()
    {

        List<VEngPoint> list = cefMapper.findPointInSite(null);
        assertTrue(list.size()>=0); 
        list.stream().forEach(p->{
            System.out.println(String.format("S_SITE_ID %d, S_SITE_NM %s, S_POINT_ID %s, S_POINT_NM %s",p.getIdSite(),p.getsSiteNm(),p.getsPointId(),p.getsPointNm() ));
        });


        SearchParam p = new SearchParam();
        p.setIdSite(3L);
        
        List<VEngPoint> re = cefMapper.searchPoint(p);
        assertTrue(re.size()>=0);  

        // List<Map<String,Object>> o = cefMapper.pointCountInSite(p.getIdSite());
        // assertTrue(o.size()>=0);  
        // o.stream().forEach(map->{

        //     String sSiteId = map.get("S_SITE_ID").toString(); 
        //     Long lastSeq = (Long) map.get("LAST_SEQ"); 

        //     System.out.println("LAST_SEQ:"+ String.format("%05d",lastSeq) + " ID: "+ sSiteId);
        // });

    }

    @Test
    public void generateEngPointIdTest()
    {
       String engPointId= cefMapper.generateEngPointId(2l);
       System.out.print("engPointId:"+ engPointId);
       assertTrue(true);

       String  seq = engPointId.substring(2,8);
       System.out.print("seq:"+ Integer.parseInt(seq));
       System.out.print("lenght:"+ seq.length());

       
    }

}