package com.sebang.ems.dao;

import static org.junit.Assert.assertTrue;

import java.util.List;

import com.sebang.ems.config.TestSqlMapperConfig;
import com.sebang.ems.domain.common.SearchParam;
import com.sebang.ems.model.Do.TSunPower;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

@SpringBootTest
@RunWith(SpringRunner.class)
@ContextConfiguration(classes=TestSqlMapperConfig.class)
@ActiveProfiles("test")
public class SunPowerTest
{

	@Autowired
    SunPowerMapper sunPowerMapper;

    
    @Test
    public void searchSunPowerYearSum()
    {
        SearchParam s =  new SearchParam();
        String arrYear[] =  {"2019"};
        s.setArrYear(arrYear);
        List<TSunPower> re = sunPowerMapper.searchSunPowerYearSum(s);
        assertTrue(re.size()>=0);
    }

}