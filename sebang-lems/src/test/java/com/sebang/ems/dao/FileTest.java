package com.sebang.ems.dao;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.List;

import com.sebang.ems.config.TestSqlMapperConfig;
import com.sebang.ems.model.Gfiles;
import com.sebang.ems.model.Do.TFiles;
import com.sebang.ems.util.Guid;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.TransactionDefinition;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.support.DefaultTransactionDefinition;


@SpringBootTest
@RunWith(SpringRunner.class)
@ContextConfiguration(classes=TestSqlMapperConfig.class)
@ActiveProfiles("test")
public class FileTest
{

	@Autowired
    FilesMapper filesMapper;

    @Autowired
	private PlatformTransactionManager transactionManager;

	DefaultTransactionDefinition def = null;
    TransactionStatus status = null;
    

    @Test
    public void selectGfileTest()
    {
        List<Gfiles> g = filesMapper.findByGid("41469b31d3e944539dec25f698321ddd");
        assertTrue(g.size() >=0);        
     }

     @Test
     public void fineOneTest()
     {
         TFiles f = filesMapper.fineOne(1l);
         assertTrue(true);        
      }

    @Test
    public void upsertFiles()
    {
        Gfiles f = new Gfiles();
        f.setsFileId(Guid.genUUID());
        f.setsFileNm("sFileNm");
        f.setsScreenId("111");
      //  f.setsTableId("t12345");
        f.setsFilePath("sFilePath");
        f.setsFileNm("sFileNm");
        Short seq =0;
     //   f.setnFileSeq(seq);
        try {
			def = new DefaultTransactionDefinition();
			def.setPropagationBehavior(TransactionDefinition.PROPAGATION_REQUIRED);
            status = transactionManager.getTransaction(def);      
            filesMapper.upsertFiles(f);
       //     transactionManager.commit(status);

            System.out.println("fileid =>"+f.getIdFile().toString());

            f.setGId(Guid.genUUID());
            filesMapper.upsertGfiles(f);
            transactionManager.rollback(status);
            assertTrue(true);
        }catch(Exception ex)
        {
            transactionManager.rollback(status);
            ex.printStackTrace();
            assertTrue(false);
        }
    }

    @Test
    @Transactional
    @Rollback(true)
    public void upsertFilesWithidFIle()
    {
        Gfiles f = new Gfiles();
        f.setIdFile(3l);
        f.setsFileId(Guid.genUUID());
        f.setsFileNm("sFileNm");
        f.setsScreenId("111");
      //  f.setsTableId("t12345");
        f.setsFilePath("sFilePath");
        f.setsFileNm("sFileNm");
       // Short seq =0;
        filesMapper.upsertFiles(f);
        Long idFile = f.getIdFile();
        assertTrue(3l == idFile);
    }

    @Test
    public void findDeletedFilesTest()
    {
        List<Gfiles> g = filesMapper.findDeletedFileMarks(10);
        assertTrue(g.size()>=0);
    }
}