package com.sebang.ems.dao;

import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.sebang.ems.config.TestSqlMapperConfig;
import com.sebang.ems.domain.common.SearchParam;
import com.sebang.ems.model.SiteDept;
import com.sebang.ems.model.Do.TErpDept;
import com.sebang.ems.model.Do.TGasDeptMapping;
import com.sebang.ems.model.Do.TSiteInfo;
import com.sebang.ems.model.Dto.ReqErpDept;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.TransactionDefinition;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.support.DefaultTransactionDefinition;

@SpringBootTest
@RunWith(SpringRunner.class)
@ContextConfiguration(classes = TestSqlMapperConfig.class)
@ActiveProfiles("test")
public class SiteTest {
    @Autowired
    SiteDeptMapper siteDeptMapper;

    @Autowired
    private PlatformTransactionManager transactionManager;

    DefaultTransactionDefinition def = null;
    TransactionStatus status = null;

    @Test
    public void findSiteTest() {
        List<?> t = siteDeptMapper.findSiteById(2l);
        assertTrue(t.size() >= 0);      
    }

    @Test
    public void findSiteErpDept()
    {
        TGasDeptMapping t = siteDeptMapper.findErpDeptInSite(2l,1l);
        assertTrue(true);
    }

    @Test
    public void findErErpDept()
    {
        Long t = siteDeptMapper.findErpDeptKeyById("70000000");
        assertTrue(t>=0);
    }


    @Test
    public void findAllDeptTest() {
        // findAllSite
        List<TErpDept> ou = siteDeptMapper.findAllDept();

        assertTrue(ou.size() >= 0);

        List<TSiteInfo> r = siteDeptMapper.findSiteById(2l);
        assertTrue(r.size() >= 0);

        SearchParam p = new SearchParam();
        p.setIdSite(2l);
        List<SiteDept> v = siteDeptMapper.searchSiteDept(p);
        assertTrue(v.size() >= 0);
    }

    @Test
    @Transactional
    @Rollback(true)
    public void upsertSiteTest() {
        TSiteInfo site = new TSiteInfo();
        site.setIdSite(2l);
        site.setsSiteId("00000");
        site.setsSiteNm("test");
        site.setsSiteTel("123-3456");
        site.setsSitePartyCd("1");
        // "sSiteId": "00000",
        // "sSiteNm": "E사업부",
        // "sSiteTel": "123-3456"

        siteDeptMapper.upsertSite(site);
        assertTrue(true);
    }

    @Test
    @Transactional
    @Rollback(true)
    public void upsertDeptTest() {
        TSiteInfo site = new TSiteInfo();
        site.setIdSite(2l);
        site.setsSiteId("00000");
        site.setsSiteNm("가사업부");
        site.setsSitePartyCd("1");

        siteDeptMapper.upsertSite(site);
        site.setsSiteNm("나사업부");
       
        assertTrue(true);
    }

    @Test
    @Transactional
    @Rollback(true)
    public void addDeptToSite() {
        TGasDeptMapping p = new TGasDeptMapping();
        p.setIdSite(0l);
        p.setIdErpDept(2l);
        
        siteDeptMapper.upsertSiteDept(p);
        assertTrue(true);
    }

    TErpDept findCurDept(List<TErpDept> dept, String deptid) {
        TErpDept ret = null;

        for (TErpDept i : dept) {
            if (deptid.equals(i.getsDeptId()) ) {
                 System.out.println("id:"+i.getsDeptId()+ ",Nm:"+ i.getsDeptNm());
                return i;
            }
        }

        return ret;
    }

    String buildFullDeptNm(List<TErpDept> dept, String curDeptId) {
        TErpDept ch = null;
        // String parentId = "10000110";
        // String parentId = "10000110";
        String parentId = curDeptId;
        String deptNm = "";
        StringBuilder sb = new StringBuilder();

        do {

            ch = findCurDept(dept, parentId);
            if (ch != null) {
                parentId = ch.getsUDeptId();
                deptNm = ch.getsDeptNm();
                if (sb.length() == 0)
                    sb.append(deptNm);
                else
                    sb.insert(0, deptNm + " > ");
            }
        } while (ch != null);

        return sb.toString();
    }

    @Test
    public void buildDeptFullNameTest() {
        List<TErpDept> ou = siteDeptMapper.findAllDept();

        String parentId = "70000229";
        System.out.println("full Dept Nm:" + buildFullDeptNm(ou, parentId));
        assertTrue(true);
    }

    // List<TErpDept> findChildDept(List<TErpDept> allDepts, String deptid) {
    // List<TErpDept> ret = new ArrayList<TErpDept>();
    // allDepts.stream().forEach(i -> {
    // if (deptid.compareTo(i.getsUDeptId()) == 0) {
    // System.out.println("id:" + i.getsDeptId() + ",Nm:" + i.getsDeptNm());
    // ret.add(i);
    // }
    // });
    // return ret;
    // }

    List<String> findChildDepts(List<TErpDept> allDepts, String deptid) {
        List<String> ret = new ArrayList<String>();
        if(deptid== null)
            return ret;

        allDepts.stream().forEach(i -> {
            if (deptid.compareTo(i.getsUDeptId()) == 0) {
                // System.out.println("id:" + i.getsDeptId() + ",Nm:" + i.getsDeptNm());
                ret.add(i.getsDeptId());
            }
        });
        return ret;
    }

    List<String> findChildDepts(List<TErpDept> allDepts, List<String> depts, List<String> resultChildDepts) {
        List<String> next = new ArrayList<String>();

        for (String d : depts) {
            List<String> r = findChildDepts(allDepts, d);
            if (r.size() == 0)
            resultChildDepts.add(d);

            next.addAll(r);
        };
        return next;
    }

    List<String> findChildByDeptId(List<TErpDept> allDepts, String sDepId) {
      
        List<String> result = new ArrayList<String>();

        List<String> depts = new ArrayList<String>();
        depts.add(sDepId);

        List<String> childs =null;
        int level = 0;
        do {
            System.out.println("level " + level++ + ", childs size:" + depts.size()+ ", results size:" + result.size());
            childs = findChildDepts(allDepts, depts, result);
            depts.clear();
            depts.addAll(childs);
        }while(childs.size()>0);

        return result;
    }

    @Test
    public void findChildDeptTest() {
        List<TErpDept> ou = siteDeptMapper.findAllDept();
        List<String> result = new ArrayList<String>();

        result = findChildByDeptId(ou,"70000059");
                
        result.stream().forEach(s -> {
            System.out.println("full Dept Nm:" + buildFullDeptNm(ou, s));
        });

        assertTrue(true);

    }

    @Test
    @Transactional
    @Rollback(true)
    public void deleteTRawErpDept()
    {
       // List<String> list = Arrays.asList("WG200","관리부","부서코드","영업부","seb01658","WG000","WG100");
       List<ReqErpDept> list = new ArrayList<ReqErpDept>(); 
       ReqErpDept r = new ReqErpDept();
       r.setSDeptCd("WG000");

       list.add(r);

       List<SiteDept> res = siteDeptMapper.findDeleteListErpDept(list);
       assertTrue(res.size()>=0); 
       
       siteDeptMapper.deleteRawErpDept(list);
    }

    @Test
    @Transactional
    @Rollback(true)
    public void importErpDeptFormRawErpDept()
    {

        Date today = new Date();
        List<SiteDept> list =  siteDeptMapper.findUpsertListErpDept(today);
        assertTrue(list.size()>=0);

        siteDeptMapper.importErpDeptFormRawErpDept(today);
        assertTrue(true);
    }
}