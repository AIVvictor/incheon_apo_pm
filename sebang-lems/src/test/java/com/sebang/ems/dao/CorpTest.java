package com.sebang.ems.dao;

import static org.junit.Assert.assertTrue;

import com.sebang.ems.config.TestSqlMapperConfig;
import com.sebang.ems.model.Do.TCorpInfo;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.TransactionDefinition;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.DefaultTransactionDefinition;

@SpringBootTest
@RunWith(SpringRunner.class)
@ContextConfiguration(classes=TestSqlMapperConfig.class)
@ActiveProfiles("test")
public class CorpTest
{

	@Autowired
    CorpMapper corpMapper;

    @Autowired
	private PlatformTransactionManager transactionManager;

	DefaultTransactionDefinition def = null;
    TransactionStatus status = null;
    
    @Test
    public void findOneTest()
    {
        TCorpInfo re = corpMapper.findOne("");
        assertTrue(true);
    }

    @Test
    public void upsertCorp()
    {
        try {
			def = new DefaultTransactionDefinition();
			def.setPropagationBehavior(TransactionDefinition.PROPAGATION_REQUIRED);
            status = transactionManager.getTransaction(def);      
            TCorpInfo corpInfo = new TCorpInfo();
            corpInfo.setIdCorp(3l);
            corpInfo.setsCorpId("sebang2");
            corpInfo.setsCorpNm("세방2");

            corpMapper.upsert(corpInfo);

            transactionManager.rollback(status);		
            assertTrue(true);
        }catch(Exception e)
        {
            transactionManager.rollback(status);	
            e.printStackTrace();	
            assertTrue(false);
        }
    }


}