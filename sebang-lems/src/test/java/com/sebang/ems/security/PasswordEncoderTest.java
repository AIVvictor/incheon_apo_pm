package com.sebang.ems.security;

import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

import java.util.HashMap;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.factory.PasswordEncoderFactories;
import org.springframework.security.crypto.password.DelegatingPasswordEncoder;
import org.springframework.security.crypto.password.NoOpPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.crypto.password.Pbkdf2PasswordEncoder;
import org.springframework.security.crypto.password.StandardPasswordEncoder;
import org.springframework.test.context.junit4.SpringRunner;

//ref : https://java.ihoney.pe.kr/498
@RunWith(SpringRunner.class)
@SpringBootTest(classes = { PasswordEncoder.class })
public class PasswordEncoderTest {

    private PasswordEncoder passwordEncoder;

    @Before
    public void setUp() throws Exception {
        passwordEncoder = PasswordEncoderFactories.createDelegatingPasswordEncoder();
    }

    @Test
    public void encode() {
        String password = "password";

        String encPassword = passwordEncoder.encode(password);

        System.out.println(encPassword + "length:" + encPassword.length());

        assertTrue(passwordEncoder.matches(password, encPassword));
        assertTrue((encPassword).contains("{bcrypt}"));
    }

    @Test
    public void match() {
        String password = "password";
        String encPassword1 = "{pbkdf2}7a07c208fc2a407fb89cc3b6effb1b759da575a85f65dda9cd426f1ad14b56e6afaeeea6f9269569";
        String encPassword2 = "{bcrypt}$2a$10$Ot44NE6k1kO5bfNHTP0m8ejdpGr8ooHGT90lOD2/LpGIzfiS3p6oq";
        String encPassword3 = "{sha256}fcef9e3f82af42d9059e74a95c633fe99b7aba1c4bfb9ac1cae31dd1b67060da933776fee8baec8f";
        assertTrue(passwordEncoder.matches(password, encPassword1));
        assertTrue(passwordEncoder.matches(password, encPassword2));
        assertTrue(passwordEncoder.matches(password, encPassword3));
    }

    @Test
    public void customDelegatingPasswordEncoder() {
        Map<String, PasswordEncoder> encoders = new HashMap<>();
        String idForEncode = "bcrypt";
        encoders.put(idForEncode, new BCryptPasswordEncoder());
        encoders.put("noop", NoOpPasswordEncoder.getInstance());
        encoders.put("pbkdf2", new Pbkdf2PasswordEncoder());
        encoders.put("sha256", new StandardPasswordEncoder());

        passwordEncoder = new DelegatingPasswordEncoder(idForEncode, encoders);
        String password = "password";
        String encPassword = passwordEncoder.encode(password);
        System.out.println(encPassword);
        assertTrue(passwordEncoder.matches(password, encPassword));

        String encPassword1 = "{pbkdf2}7a07c208fc2a407fb89cc3b6effb1b759da575a85f65dda9cd426f1ad14b56e6afaeeea6f9269569";
        String encPassword2 = "{bcrypt}$2a$10$Ot44NE6k1kO5bfNHTP0m8ejdpGr8ooHGT90lOD2/LpGIzfiS3p6oq";
        String encPassword3 = "{sha256}fcef9e3f82af42d9059e74a95c633fe99b7aba1c4bfb9ac1cae31dd1b67060da933776fee8baec8f";
        assertTrue(passwordEncoder.matches(password, encPassword1));
        assertTrue(passwordEncoder.matches(password, encPassword2));
        assertTrue(passwordEncoder.matches(password, encPassword3));
    }

    @Test(expected = IllegalArgumentException.class) 
    public void 사용한_비밀번호변환기_접두사가없으면오류발생() { 
        String password = "password"; 
        String encPassword1 = "7a07c208fc2a407fb89cc3b6effb1b759da575a85f65dda9cd426f1ad14b56e6afaeeea6f9269569"; 
        // pbkdf2 
        String encPassword2 = "$2a$10$Ot44NE6k1kO5bfNHTP0m8ejdpGr8ooHGT90lOD2/LpGIzfiS3p6oq"; 
        // bcrypt 
        String encPassword3 = "fcef9e3f82af42d9059e74a95c633fe99b7aba1c4bfb9ac1cae31dd1b67060da933776fee8baec8f"; 
        //sha256 
        assertTrue(passwordEncoder.matches(password, encPassword1)); 
        assertTrue(passwordEncoder.matches(password, encPassword2)); 
        assertTrue(passwordEncoder.matches(password, encPassword3)); 
    }



}