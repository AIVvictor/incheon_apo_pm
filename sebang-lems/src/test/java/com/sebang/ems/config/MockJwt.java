package com.sebang.ems.config;

import java.util.Date;

import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.exceptions.JWTCreationException;
import com.sebang.ems.auth.jwt.JwtInfo;
import com.sebang.ems.util.DateUtil;

public class MockJwt
{
	protected static String JWT_TOKEN = null;

    protected static final String TOKEN_KEY = "sebang.com";
    

    public static void createToken()
    {
        Date ex = DateUtil.nowAfterDaysToDate(JwtInfo.EXPIRES_LIMIT);
		
		try {
			JWT_TOKEN = JWT.create()
					.withIssuer("sebang")	
					.withClaim("id", "tester")
					.withClaim("role", "SYS")					
					.withExpiresAt(ex) // 만료일
					.sign(Algorithm.HMAC256(TOKEN_KEY));

		} catch (JWTCreationException createEx) {
            System.out.println(createEx.getMessage());
           
			createEx.printStackTrace();
		}
    }
}