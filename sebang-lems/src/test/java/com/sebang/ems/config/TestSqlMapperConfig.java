package com.sebang.ems.config;

import javax.sql.DataSource;

import org.apache.ibatis.datasource.pooled.PooledDataSource;
import org.apache.ibatis.session.SqlSessionFactory;
import org.mybatis.spring.SqlSessionFactoryBean;
import org.mybatis.spring.SqlSessionTemplate;
import org.mybatis.spring.annotation.MapperScan;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.test.context.ActiveProfiles;

/**
 * SqlMapper Configuration
 *
 * @author
 */
@Configuration
@MapperScan(value = "com.sebang.ems")
@PropertySource(value = "classpath:application-test.properties")
@ActiveProfiles("test")
public class TestSqlMapperConfig {
	public static final Logger logger = LoggerFactory.getLogger(TestSqlMapperConfig.class);

	@Value("${spring.datasource.driver-class-name}")
	private String driverClassName;

	@Value("${spring.datasource.url}")
	private String url;

	@Value("${spring.datasource.username}")
	private String username;

	@Value("${spring.datasource.password}")
	private String password;

	@Bean
	public DataSource testDataSource() {
		// String url =
		// "jdbc:mysql://localhost:3306/lems?characterEncoding=UTF-8&serverTimezone=UTC&createDatabaseIfNotExist=true&autoReconnect=true&useSSL=false&useUnicode=true&characterEncoding=utf8";
		// String url =
		// "jdbc:mysql://13.67.33.98:3306/amr_dodam?autoReconnect=true&useSSL=false&";&amp;characterEncoding=UTF-8&amp;serverTimezone=UTC

		logger.info("Data Source: driverClassName:   {}", driverClassName);
		logger.info("Data Source: Database.url:      {}", url);
		logger.info("Data Source: Username:          {}", username);

		// http://www.mybatis.org/mybatis-3/ko/configuration.html
		// PooledDataSource dataSource = new PooledDataSource(driverClassName, url,
		// username, password);
		PooledDataSource dataSource = new PooledDataSource(driverClassName, url, username, password);

		// dataSource.setPoolMaximumActiveConnections(poolMaxActConnect);
		// dataSource.setPoolMaximumIdleConnections(maximumIdleConnections);
		// dataSource.setPoolMaximumCheckoutTime(poolMaximumCheckoutTime);
		// dataSource.setPoolTimeToWait(poolTimeToWait);
		// dataSource.setPoolPingQuery(poolPingQuery);
		// dataSource.setPoolPingEnabled(poolPingEnabled);
		// dataSource.setPoolPingConnectionsNotUsedFor(poolPingConnectionsNotUsedFor);
		return dataSource;
	}

	@Bean
	public SqlSessionFactory testSqlSessionFactory() throws Exception {

		SqlSessionFactoryBean sessionFactory = new SqlSessionFactoryBean();
		sessionFactory.setDataSource(testDataSource());
		sessionFactory.setConfigLocation(new ClassPathResource("mybatis-config.xml"));// spring boot mybatis settings 부분

		Resource[] res = new PathMatchingResourcePatternResolver().getResources("classpath:mappers/**/*Mapper.xml");
		sessionFactory.setMapperLocations(res);
		return sessionFactory.getObject();
	}

	@Bean
	public SqlSessionTemplate testSqlSessionTemplate() throws Exception {
		return new SqlSessionTemplate(testSqlSessionFactory());
	}

	@Bean
	public DataSourceTransactionManager testDdataSourceTransactionManager() {
		return new DataSourceTransactionManager(testDataSource());
	}

}
