package com.sebang.ems.service;

import static org.junit.Assert.assertTrue;

import java.util.List;

import com.sebang.ems.config.TestSqlMapperConfig;
import com.sebang.ems.model.Do.TMenuInfo;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = { CodeServiceImpl.class })
@ContextConfiguration(classes = TestSqlMapperConfig.class)
@ActiveProfiles("test")
public class CodeServiceImplTest {

    @Autowired
    CodeServiceImpl codeService;

    @Test
    public void buildFullMenuNameTest() {

        List<TMenuInfo> allMenu = codeService.findMenuMst(null);

        String fullmenu = codeService.buildFullMenuName(allMenu,"40002400");
        System.out.println(fullmenu);
        assertTrue(true);

        fullmenu = codeService.buildFullMenuName(allMenu,"40000000");
        System.out.println(fullmenu);
        assertTrue(true);

        fullmenu = codeService.buildFullMenuName(allMenu,"50003000");
        System.out.println(fullmenu);
        assertTrue(true);
    }

    @Test
    public void buildFullMenuNameInvalidMenuIdTest() {

        List<TMenuInfo> allMenu = codeService.findMenuMst(null);

        String fullmenu = codeService.buildFullMenuName(allMenu,null);
        System.out.println(fullmenu);
        assertTrue(true);

        fullmenu = codeService.buildFullMenuName(allMenu,"400");
        System.out.println(fullmenu);
        assertTrue(true);

        fullmenu = codeService.buildFullMenuName(allMenu,"");
        System.out.println(fullmenu);
        assertTrue(true);
    }


}