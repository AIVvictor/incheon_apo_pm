package com.sebang.ems.service;

import static org.junit.Assert.assertTrue;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import com.sebang.ems.config.TestSqlMapperConfig;
import com.sebang.ems.model.Do.TEngPointAmt;
import com.sebang.ems.model.Do.TMonEngClose;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = { AmtServiceImpl.class })
@ContextConfiguration(classes = TestSqlMapperConfig.class)
@ActiveProfiles("test")
public class AmtServiceImplTest {

    @Autowired
    AmtServiceImpl amtService;

    @Test
    public void closeMonEngCloseTest() {

        TMonEngClose close = new TMonEngClose();
        close.setcYear("2019");
        close.setcMon("1");
        close.setIdSite(3l);
        
        amtService.closeMonEngClose(close);
       
        assertTrue(true);
    }

    @Test
    public void cancelMonEngCloseTest() {

        TMonEngClose close = new TMonEngClose();
        close.setcYear("2019");
        close.setcMon("1");
        close.setIdSite(3l);
        
        amtService.cancelMonEngClose(close);
       
        assertTrue(true);
    }

    @Test
    public void upsertEngPointAmtsTest()
    {
        List<TEngPointAmt> arrAmt = new ArrayList<TEngPointAmt>();
        TEngPointAmt a = new TEngPointAmt();
        a.setIdEngPoint(1l);
        a.setIdErpDept(2l);
        a.setcMon("08");
        a.setcYear("2019");
        a.setnCost(new BigDecimal(1.1));
        a.setnUseAmt(new BigDecimal(2.2));
  //      a.setnGj(123f);
  //      a.setnTco2(1.2f);
        arrAmt.add(a);

        amtService.upsertEngPointAmts(arrAmt);
        assertTrue(true);
    }


}