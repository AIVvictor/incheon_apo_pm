package com.sebang.ems.service;

import static org.junit.Assert.assertTrue;

import com.sebang.ems.config.TestSqlMapperConfig;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;


@RunWith(SpringRunner.class)
@SpringBootTest(classes = { CefServiceImpl.class })
@ContextConfiguration(classes=TestSqlMapperConfig.class)
@ActiveProfiles("test")
public class CefServiceImplTest {

    @Autowired
    CefServiceImpl cefService;
    
    @Test
     public void generatePointIdTest()
    {
        // Long subseq = new Long(0l);
        // Map<String,Object> sPointId = cefService.generatePointId(12l);
        // subseq =(Long) sPointId.get("LAST_SEQ");
        // System.out.println("sPointId:"+sPointId.get("S_SITE_ID")+"lastseq: "+ subseq.toString());
        // assertTrue(true);
        cefService.generatePointId(4l);
        assertTrue(true);
    }
    
    
}