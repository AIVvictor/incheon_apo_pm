package com.sebang.ems.service;

import static org.junit.Assert.assertTrue;

import java.util.List;
import java.util.Map;

import com.sebang.ems.config.TestSqlMapperConfig;
import com.sebang.ems.model.Do.TErpDept;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = { SiteServiceImpl.class })
@ContextConfiguration(classes = TestSqlMapperConfig.class)
@ActiveProfiles("test")
public class SiteServiceImplTest {

    @Autowired
    SiteServiceImpl siteService;

    // @Test
    // public void findChildByDeptIdTest() {

    // List<String> list = siteService.findChildFullDeptNmByDeptId("70000202");

    // list.stream().forEach(name -> {

    // System.out.println("full name : " + name);
    // });
    // assertTrue(true);
    // }

    @Test
    public void findChildDeptsByIdTest() {

        List<TErpDept> list = siteService.findChildDeptsByDeptId("70000129");
        list.stream().forEach(name -> {
            System.out.println("full name : " + name.getsFullDeptNm());
        });
        assertTrue(true);

    }

}