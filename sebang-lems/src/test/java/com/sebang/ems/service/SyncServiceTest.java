package com.sebang.ems.service;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.sebang.ems.config.TestSqlMapperConfig;

import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;


@RunWith(SpringRunner.class)
@SpringBootTest(classes = { SyncServiceImpl.class,PasswordEncoder.class })
@ContextConfiguration(classes=TestSqlMapperConfig.class)
@ActiveProfiles("test")
public class SyncServiceTest {

    @Autowired
    SyncServiceImpl syncService;

    
    @Test
    @Ignore

    public void syncErpDeptTest()
    {
        syncService.syncErpDept(false,true);
        assertTrue(true);
    }
    @Test
    @Ignore

    public void syncErpUsersTest()
    {
        syncService.syncErpUsers(false,true);
        assertTrue(true);
    }

    @Test
    @Ignore

    public void syncErpRetireTest()
    {
        syncService.syncErpRetire(false,true);
        assertTrue(true);
    }


    @Test
    public void rexTests()
    {
        String value = "[2018-01-01] [ERROR] [Nesoy Log Time : 50]";

        Pattern infoPattern = Pattern.compile("([A-Z*]{1,5})");
        Matcher infoMatcher = infoPattern.matcher(value);
        if(infoMatcher.find()){ // find가 group보다 선행되어야 합니다.
            System.out.println(infoMatcher.group()); // ERROR
        }  

        boolean result = false;
        Pattern p = Pattern.compile("^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,6}$", Pattern.CASE_INSENSITIVE);
        Matcher m = p.matcher("a@c.com");
        if(m.find())
            result = true;

        assertTrue(result);


        result = false;
        m = p.matcher("a@");
        if(m.find())
            result = true;
        assertFalse(result);

        result = false;
        m = p.matcher("");
        if(m.find())
            result = true;
        assertFalse(result);

    }

    @Test
    public void findErpUserTest()
    {
        String value = "77000162";

        Pattern infoPattern = Pattern.compile("7[0789][0-9]{6}");
        Matcher infoMatcher = infoPattern.matcher(value);
        if(infoMatcher.find()){ // find가 group보다 선행되어야 합니다.
            System.out.println(infoMatcher.group()); // 77000162
        }  
    }
}