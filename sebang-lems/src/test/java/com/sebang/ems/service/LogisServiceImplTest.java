package com.sebang.ems.service;

import static org.junit.Assert.assertTrue;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import com.sebang.ems.config.TestSqlMapperConfig;
import com.sebang.ems.domain.Exception.BaseException;
import com.sebang.ems.model.Do.TLogisTransMonInfo;
import com.sebang.ems.model.Dto.ReqVehicle;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = { LogisServiceImpl.class })
@ContextConfiguration(classes = TestSqlMapperConfig.class)
@ActiveProfiles("test")
public class LogisServiceImplTest {

    @Autowired
    LogisServiceImpl logisService;

    @Test
    public void upsertLogisTransMonInfoTest() {

        TLogisTransMonInfo m =  new TLogisTransMonInfo();
        m.setIdLogisTransMonInfo(1l);
        m.setDistanceSum(new BigDecimal(100));
        m.setLoadageSum(new BigDecimal(2));
        
        logisService.upsertLogisTransMonInfo(m);
        assertTrue(true);
    }

    @Test(expected = BaseException.class)
    public void upsertLogisTransMonInfoExceptionTest()
    {
        TLogisTransMonInfo m =  new TLogisTransMonInfo();

        m.setDistanceSum(BigDecimal.ZERO);
        m.setLoadageSum(new BigDecimal(123));
        
        logisService.upsertLogisTransMonInfo(m);
    }

    @Test
    public void upsertVehicleTest()
    {
        ReqVehicle c= new ReqVehicle().builder()
					   .sVrn("대구06도2227")
					   .sCarStatusCd("1")
					   .sTransComId("S000001")
					   .sErpId("70000059")
					   .sUseTypeCd("2")
					   .sCarMade("197401")
					   .sLogiWeiCd("1")
					   .sCarType("1")
					   .sCarLoanDate("20190501000000")
					   .sModDate("20190501121212")
					   .sCarModel("1")
					   .sRegDate("20190601010010")
					   .sCarCompany("1")
					   .sLogiYctCd("1")
					   .build();

		ReqVehicle c1= new ReqVehicle().builder()
		 	    .sVrn("경북07가1234")
				.sCarStatusCd("1")
				.sTransComId("S000001")
				.sErpId("70000061")
				.sUseTypeCd("1")
				.sCarMade("197510")
				.sLogiWeiCd("1")
				.sCarType("1")
				.sCarLoanDate("20190501000000")
				.sModDate("20190501121212")
				.sCarModel("1")
				.sRegDate("20190601010010")
				.sCarCompany("1")
				.sLogiYctCd("1")
				.build();			   
			
		List<ReqVehicle> list = new ArrayList<ReqVehicle>();
		list.add(c);
		list.add(c1);
        
        logisService.upsertVehicles(list);
        assertTrue(true);
    }
}